const withPlugins = require("next-compose-plugins");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

// ANALYZE=true yarn build

module.exports = withPlugins([
  [withBundleAnalyzer],
  // your other plugins here
  {
    publicRuntimeConfig: {
      REACT_APP_HOST_API_NEXTJS: process.env.REACT_APP_HOST_API_NEXTJS,
      REACT_APP_HOST_API: process.env.REACT_APP_HOST_API,
      REACT_APP_FRONT_END: process.env.REACT_APP_FRONT_END,
      REACT_APP_META_TITLE: process.env.REACT_APP_META_TITLE,
      REACT_APP_META_DESCRIPTION: process.env.REACT_APP_META_DESCRIPTION,
      REACT_APP_META_KEYWORDS: process.env.REACT_APP_META_KEYWORDS,
      REACT_APP_FACEBOOK_APP_ID: process.env.REACT_APP_FACEBOOK_APP_ID,
      REACT_APP_GOOGLE_ANALYTICS_ID: process.env.REACT_APP_GOOGLE_ANALYTICS_ID,
      REACT_APP_GOOGLE_ANALYTICS_ID2: process.env.REACT_APP_GOOGLE_ANALYTICS_ID2,
      REACT_APP_GOOGLE_AUTHEN_API_KEY:
        process.env.REACT_APP_GOOGLE_AUTHEN_API_KEY,
      REACT_APP_GOOGLE_AUTHEN_CLIENT_KEY:
        process.env.REACT_APP_GOOGLE_AUTHEN_CLIENT_KEY,
      REACT_APP_GOOGLE_AUTHEN_SCOPES:
        process.env.REACT_APP_GOOGLE_AUTHEN_SCOPES,
    },
    images: {
      domains: [
        "cdn.mservice.io",
        "static.mservice.io",
        "s3-ap-southeast-1.amazonaws.com",
        "avatars.mservice.io.s3-ap-southeast-1.amazonaws.com",
        "img.mservice.io",
        "www.galaxycine.vn",
        "booking.bhdstar.vn",
        "www.cgv.vn",
        "cinestar.com.vn",
        "i.imgur.com",
      ],
      deviceSizes: [640, 750, 1080, 1200],
      minimumCacheTTL: 259200,
    },
    // webpack5: false,
    assetPrefix: process.env.REACT_APP_CDN_STATIC,
    //Loi next image khi build
    // webpack: (config, { dev, isServer }) => {
    //   // Replace React with Preact only in client production build
    //   if (!dev && !isServer) {
    //     Object.assign(config.resolve.alias, {
    //       react: "preact/compat",
    //       "react-dom/test-utils": "preact/test-utils",
    //       "react-dom": "preact/compat",
    //     });
    //   }
    //   return config;
    // },

    rewrites: async () => [
      // {
      //   source: "/blog",
      //   destination: "/blog/moi-nhat",
      // },
      // {
      //   source: "/blog/:slug-c:cateId(\\d+)dt:detailId(\\d+)",
      //   destination: "/blog/:cateId/:detailId",
      // },
    ],
    redirects: async () => [
      {
        source: "/",
        destination: "https://momo.vn",
        permanent: false,
      },
      {
        source: "/tin-tuc-app",
        destination: "/tin-tuc",
        permanent: false,
      },
      // {
      //   source: "/tim-chuyen-bay",
      //   destination: "https://momo.vn/tim-chuyen-bay",
      //   permanent: false,
      // },
      {
        source: "/song-tot",
        destination: "/trai-tim-momo",
        permanent: false,
      },
      {
        source: "/cong-dong",
        destination: "/trai-tim-momo",
        permanent: false,
      },
    ],
  },
]);
