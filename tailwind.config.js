const { spacing, fontFamily } = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  purge: ["./src/pages/**/*.js", "./src/components/**/*.js", "./src/layouts/**/*.js"],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        gray: colors.trueGray,
      },
      fontFamily: {
        sans: ["Inter", "Arial", "sans-serif"],
        body: ["Inter", "system-ui", "sans-serif"],
      },
      fontSize: {
        'tiny' : "0.8125rem"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/aspect-ratio"),
    require('@tailwindcss/line-clamp'),
  ],
};
