const { spacing, fontFamily } = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  content: [
    "./src/pages/**/*.js",
    "./src/components/**/*.js",
    "./src/layouts/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        gray: colors.neutral,
        indigo: colors.indigo,
        pink: {
          300: "#F9B5C3",
          400: "#f45197",
          500: "#e81f76",
          600: "#a50064",
        },
        pine: {
          500: "#325340",
        },
        momo: "#A50064",
        green: colors.emerald,
        yellow: colors.amber,
        purple: colors.violet,
      },
      fontFamily: {
        sans: ["Nunito", "Arial", "sans-serif"],
        body: ["Nunito", "system-ui", "sans-serif"],
      },
      fontSize: {
        "1.5xl": "1.375rem",
        tiny: "0.8125rem",
        md: "0.9375rem",
      },
    },
  },
  plugins: [
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/forms")({
      // strategy: 'base', // only generate global styles
      strategy: "class", // only generate classes
    }),
    // require('@tailwindcss/forms'),
  ],
};
