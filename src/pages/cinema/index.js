import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Page from "@/components/page";

import Container from "@/components/Container";
// import Hero from "@/components/Hero";
import MovieNow from "@/components/cinema/MovieNow";
import TrailerNow from "@/components/cinema/TrailerNow";

import CinemaMenu from "@/components/cinema/CinemaMenu";

import Heading from "@/components/Heading";

import Link from "next/link";

import SectionGroup from "@/components/section/SectionGroup";

import RapBrandList from "@/components/cinema/RapBrandList";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

const CinemaBookingAll = dynamic(() =>
  import("@/components/cinema/CinemaBookingAll")
);

import { axios } from "@/lib/index";

function Cinema({
  dataMoviesNow,
  dataMoviesSoon,
  dataCinemaMaster,
  dataServiceDetail,
  dataServiceBlocks,
  dataCineplexAll,
  pageMaster,
  isMobile,
}) {
  const dataFilmsNow = dataMoviesNow.Data ? dataMoviesNow.Data.Items : [];
  const dataFilmsSoon = dataMoviesSoon.Data ? dataMoviesSoon.Data.Items : [];

  const dataCineMaster = dataCinemaMaster.Data;
  const dataCineplexList = dataCineplexAll.Data;
  const spDetail = dataServiceDetail.Data;

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadcrumb = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadcrumb = dataServiceBlocks.Data.find(
      (item, index) => item.Type == 23
    );
  }

  return (
    <Page
      title={spDetail?.Meta?.Title}
      description={spDetail?.Meta?.Description}
      image={spDetail?.Meta?.Avatar}
      keywords={spDetail?.Meta?.Keywords}
      scripts={spDetail.Meta.Scripts}
      // robots={spDetail.Meta.Robots}
      // robots={
      //   <>
      //     <meta name="robots" content="noindex, nofollow" />
      //     <meta name="googlebot" content="noindex, nofollow" />
      //   </>
      // }
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={spDetail.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadcrumb &&
        dataServiceBreadcrumb?.Data?.Items?.length > 0 && (
          <LdJson_BreadCrumbs BreadCrumbs={dataServiceBreadcrumb.Data.Items} />
        )}

      {/* <div className="relatice top-0 z-30 bg-white shadow md:sticky">
        <Container>
          <div className="-mx-5 md:mx-0">
            <CinemaMenu />
          </div>
        </Container>
      </div> */}

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName == "AboutH1")}
        isMobile={isMobile}
        startType={3}
      />

      {dataFilmsNow.length > 0 && (
        <section
          className="bg-black bg-contain bg-bottom bg-no-repeat py-8 md:py-10 lg:py-14 "
          style={{
            backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210701105436-637607336767432408.jpg
          )`,
          }}
          id="phimdangchieu"
        >
          <Container>
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Phim đang chiếu"
                color="white"
                isSparkles={true}
              />
            </div>
            <div className="-mx-5 md:mx-0">
              <MovieNow data={dataFilmsNow} isDark={true} isMobile={isMobile} />
            </div>
          </Container>
        </section>
      )}

      {dataFilmsSoon.length > 0 && (
        <Container>
          <section className="pt-6 pb-6 md:py-14 " id="phimsapchieu">
            <div className="mb-5 text-center md:mb-8">
              <Heading title="Phim sắp chiếu" color="pink" />
            </div>
            <div className="-mx-5 md:mx-0 ">
              <MovieNow data={dataFilmsSoon} isMobile={isMobile} />
            </div>

            <div className="mt-5 text-center md:mt-8">
              <Link href="/cinema/phim-chieu#homecinema" scroll={true}>
                <a className="btn-primary medium">Tìm phim chiếu rạp</a>
              </Link>
            </div>
          </section>
        </Container>
      )}

      {dataFilmsNow.length > 0 && (
        <div
          className="bg-cover bg-center bg-no-repeat"
          style={{
            backgroundImage: `url(
              https://static.mservice.io/img/momo-upload-api-211001091715-637686766356856456.jpg
          )`,
          }}
        >
          <Container>
            <section className="py-8 md:py-10 lg:py-14">
              <div className="mb-5 text-center md:mb-8">
                <Heading title="Trailer mới nhất" color="white" />
              </div>
              <div className="-mx-5 md:mx-0">
                <TrailerNow data={dataFilmsNow} isMobile={isMobile} />
              </div>
            </section>
          </Container>
        </div>
      )}

      <section className="] py-8 md:py-10  lg:py-14 " id="cumrap">
        <Container>
          <div className="mb-5 text-center md:mb-8">
            <Heading title=" Lịch chiếu phim" color="pink" />
          </div>

          <CinemaBookingAll master={dataCineMaster} />

          {/* <div className="min-h-[655px]">
            {inViewFilmBooking && (
              <CinemaBookingAll master={dataCineMaster}  />
            )}
          </div>
          <div className="mt-5 text-center md:mt-8">
          </div> */}
          <div className="mt-5 text-center md:mt-8">
            <Link href="/cinema/lich-chieu#homecinema" scroll={true}>
              <a className="btn-primary medium">Xem tất cả</a>
            </Link>
          </div>
        </Container>
      </section>

      {/* <SectionCumRap dataCineMaster={dataCineMaster} isMobile={isMobile} /> */}

      {dataCineplexList && dataCineplexList.Items.length > 0 && (
        <div className="bg-gray-50" id="rapphim">
          <Container>
            <section className="py-8 md:py-10 lg:py-14" id="danhsachrap">
              <div className="mb-5 text-center md:mb-8">
                <Heading
                  title="Hệ thống rạp chiếu phim"
                  subtitle="Danh sách hệ thống rạp chiếu phim lớn có mặt khắp cả nước"
                  color="pink"
                />
              </div>

              <RapBrandList dataCineplexList={dataCineplexList} />

              <div className="mt-5 text-center md:mt-8">
                <Link href="/cinema/rap#homecinema" scroll={true}>
                  <a className="btn-primary medium">Tìm rạp chiếu</a>
                </Link>
              </div>
            </section>
          </Container>
        </div>
      )}

      {/* <FeatureSection /> */}
      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
        isMobile={isMobile}
        startType={2}
      />

      {spDetail.ShowFooter && spDetail.FooterType == 2 && <Footer menuServices={dataServiceMenuFooter} />}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />
    </Page>
  );
}

export async function getStaticProps() {
  //Mac dinh lay theo TP HCM
  const dataMoviesNow = await axios.get(
    `/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataMoviesSoon = await axios.get(
    `/ci-film/loadMore?apiStatus=2&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  const servicePageId = dataCinemaMaster?.Data?.ServicePageIds?.Home;

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  const dataCineplexAll = await axios.get(
    `ci-cinemplex/loadMore?lastIndex=0&sortType=2&sortDir=1`,
    null,
    true
  );

  return {
    props: {
      dataMoviesNow,
      dataMoviesSoon,
      dataCinemaMaster,
      dataServiceDetail,
      dataServiceBlocks,
      dataCineplexAll,
    },
    revalidate: 60 * 1,
  };
}

export default Cinema;
