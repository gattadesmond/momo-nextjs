import { useState, useEffect, useRef } from "react";

import Page from "@/components/page";

import Container from "@/components/Container";

import BreadcrumbSV from "@/components/BreadcrumbSV";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";

import { axios } from "@/lib/index";

import Heading from "@/components/Heading";
import MovieNow from "@/components/cinema/MovieNow";

import SectionGroup from "@/components/section/SectionGroup";

import MovieListFilter from "@/components/cinema/MovieListFilter";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

function Cinema({
  dataMoviesNow,
  dataMoviesSoon,
  dataServiceDetail,
  dataServiceBlocks,
  dataCinemaMaster,
  pageMaster,
  isMobile,
}) {
  const spDetail = dataServiceDetail.Data;
  const dataFilmsNow = dataMoviesNow.Data ? dataMoviesNow.Data.Items : [];
  const dataFilmsSoon = dataMoviesSoon.Data ? dataMoviesSoon.Data.Items : [];

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadCrumb = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadCrumb = dataServiceBlocks
      ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
      : null;
  }

  const boxId = useRef(null);

  return (
    <Page
      title={spDetail?.Meta?.Title}
      description={spDetail?.Meta?.Description}
      image={spDetail?.Meta?.Avatar}
      keywords={spDetail?.Meta?.Keywords}
      scripts={spDetail?.Meta?.Scripts}
      // robots={spDetail.Meta.Robots}
      // robots={
      //   <>
      //     <meta name="robots" content="noindex, nofollow" />
      //     <meta name="googlebot" content="noindex, nofollow" />
      //   </>
      // }
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={spDetail.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadCrumb && dataServiceBreadCrumb?.Data?.Items.length > 0 && (
        <div className="jsx-3479491080 relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadCrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadCrumb.Data.Items}
            />
          </Container>
        </div>
      )}

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName == "AboutH1")}
        isMobile={isMobile}
        startType={3}
      />

      {dataFilmsNow.length > 0 && (
        <section
          className="bg-black bg-contain bg-bottom bg-no-repeat py-8 md:py-10 lg:py-14 "
          style={{
            backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210701105436-637607336767432408.jpg
          )`,
          }}
          id="phimdangchieu"
        >
          <Container>
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Phim đang chiếu"
                color="white"
                isSparkles={true}
              />
            </div>
            <div className="-mx-5 md:mx-0">
              <MovieNow data={dataFilmsNow} isDark={true} isMobile={isMobile} />
            </div>
          </Container>
        </section>
      )}

      {dataFilmsSoon.length > 0 && (
        <Container>
          <section className="pt-6 pb-6 md:py-14 " id="phimsapchieu">
            <div className="mb-5 text-center md:mb-8">
              <Heading title="Phim sắp chiếu" color="pink" />
            </div>
            <div className="-mx-5 md:mx-0 ">
              <MovieNow data={dataFilmsSoon} isMobile={isMobile} />
            </div>
          </section>
        </Container>
      )}

      <section
        className="bg-gray-50 pt-6 pb-6  md:py-14"
        id="timphim"
        ref={boxId}
      >
        <Container>
          <MovieListFilter
            dataCinemaMaster={dataCinemaMaster}
            scrollRef={boxId}
            headingContent="Tìm phim chiếu rạp trên MoMo"
            dataFilmsSoon={dataFilmsSoon}
          />
        </Container>
      </section>

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
        isMobile={isMobile}
      />

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />
    </Page>
  );
}

export async function getStaticProps(context) {
  const dataMoviesNow = await axios.get(
    `/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataMoviesSoon = await axios.get(
    `/ci-film/loadMore?apiStatus=2&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  const servicePageId = dataCinemaMaster?.Data?.ServicePageIds?.ListFilm;

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  return {
    props: {
      dataMoviesNow,
      dataMoviesSoon,
      dataCinemaMaster,
      dataServiceDetail,
      dataServiceBlocks,
    },
    revalidate: 60 * 1,
  };
}

export default Cinema;
