import { useState, useEffect, useRef } from "react";

import Page from "@/components/page";

import Container from "@/components/Container";

import BreadcrumbSV from "@/components/BreadcrumbSV";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";

import { axios } from "@/lib/index";

import MovieListFilter from "@/components/cinema/MovieListFilter";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

function Cinema({
  dataServiceDetail,
  dataServiceBlocks,
  dataCinemaMaster,
  pageMaster,
  dataMoviesSoon,
  servicePageFind,
  isMobile,
}) {
  const theLoaiId = servicePageFind.CateId ?? null;
  const quocGiaId = servicePageFind.CountryId ?? null;

  const spDetail = dataServiceDetail.Data;

  const dataFilmsSoon = dataMoviesSoon.Data ? dataMoviesSoon.Data.Items : [];

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadCrumb = null;
  let dataServiceH1 = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadCrumb = dataServiceBlocks
      ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
      : null;

    dataServiceH1 =
      dataServiceBlocks.Data.filter((item, index) => item.Type == 20)[0] ??
      null;
  }

  const boxId = useRef(null);

  return (
    <Page
      title={spDetail?.Meta?.Title}
      description={spDetail?.Meta?.Description}
      image={spDetail?.Meta?.Avatar}
      keywords={spDetail?.Meta?.Keywords}
      scripts={spDetail?.Meta?.Scripts}
      // robots={spDetail.Meta.Robots}
      // robots={
      //   <>
      //     <meta name="robots" content="noindex, nofollow" />
      //     <meta name="googlebot" content="noindex, nofollow" />
      //   </>
      // }
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={spDetail.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadCrumb && dataServiceBreadCrumb?.Data?.Items.length > 0 && (
        <div className="jsx-3479491080 relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadCrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadCrumb.Data.Items}
            />
          </Container>
        </div>
      )}
      {dataServiceH1 && (
        <section
          className=" relative flex items-center justify-center bg-black bg-contain bg-right bg-no-repeat py-14 md:min-h-[250px] md:py-8 "
          style={{
            backgroundImage:
              "url(https://static.mservice.io/img/momo-upload-api-211001141319-637686943992093264.jpg)",
            backgroundSize: "auto 100%",
          }}
        >
          <div className="absolute inset-0  max-w-6xl bg-gradient-to-r from-black via-black to-transparent">
            {" "}
          </div>
          <Container>
            <div className="z-1 relative max-w-3xl">
              <h1 className="mb-3 text-2xl font-bold text-white md:text-4xl">
                {dataServiceH1?.Data?.Title ?? "Danh sách phim"}
              </h1>
              {/* <h2 className="text-white text-lg text-opacity-70">Thỏa sức thể hiện mọi cảm xúc hỉ nộ ái ố với những bộ phim chính kịch, hài kịch, tình cảm, giật gân này và nhiều hơn nữa, tất cả đều đến từ Thái Lan.</h2> */}
              <h2 className="mt-3 text-sm text-white text-opacity-80 md:text-base">
                {dataServiceH1?.Data?.Description ??
                  "Cùng xem  bộ sưu tập Top phim đã được Ví MoMo tổng hợp dành cho bạn đây! Khám phá ngay để có những phút giây tuyệt vời nhất trên màn ảnh cùng những tựa phim chất lượng cao bạn nhé!"}
              </h2>
            </div>
          </Container>
          ˝
        </section>
      )}
      <section
        className="bg-gray-50 pt-6 pb-6  md:pb-14"
        id="timphim"
        ref={boxId}
      >
        <Container>
          <MovieListFilter
            dataCinemaMaster={dataCinemaMaster}
            scrollRef={boxId}
            headingContent=""
            countryId={quocGiaId}
            theloaiId={theLoaiId}
            dataFilmsSoon={dataFilmsSoon}
          />
        </Container>
      </section>

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />
    </Page>
  );
}

export async function getStaticProps(context) {
  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  function getGenreSlug(post) {
    if (!post) return null;
    let url = "";

    if (post.CateSlug) {
      url += post.CateSlug;
    }
    if (post.CountrySlug) {
      url += post.CateSlug ? `-${post.CountrySlug}` : post.CountrySlug;
    }
    return url;
  }

  const listFilmGenre =
    dataCinemaMaster?.Data?.ServicePageIds?.FilmProjectsCountries;

  const servicePageFind = listFilmGenre.find(
    (post) => getGenreSlug(post) == context?.params?.slug
  );
  const servicePageId = servicePageFind.ServicePageId ?? null;

  if (!servicePageFind) {
    return {
      notFound: true,
    };
  }

  const dataMoviesSoon = await axios.get(
    `/ci-film/loadMore?apiStatus=2&lastIndex=0&sortType=4&sortDir=1`,
    null,
    true
  );

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  return {
    props: {
      dataCinemaMaster,
      dataServiceDetail,
      dataServiceBlocks,
      dataMoviesSoon,
      servicePageFind,
    },
    revalidate: 60 * 1,
  };
}

export async function getStaticPaths() {
  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  const listFilmGenre =
    dataCinemaMaster?.Data?.ServicePageIds?.FilmProjectsCountries;

  if (!listFilmGenre) {
    return {
      notFound: true,
    };
  }

  function getGenreSlug(post) {
    if (!post) return null;
    let url = "";

    if (post.CateSlug) {
      url += post.CateSlug;
    }
    if (post.CountrySlug) {
      url += post.CateSlug ? `-${post.CountrySlug}` : post.CountrySlug;
    }
    return url;
  }

  const paths = listFilmGenre.map((post) => ({
    params: { slug: getGenreSlug(post) },
  }));

  return { paths, fallback: false };
}

export default Cinema;
