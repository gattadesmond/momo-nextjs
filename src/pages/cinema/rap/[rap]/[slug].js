import { useState, useEffect } from "react";

import { axios } from "@/lib/index";

import Page from "@/components/page";

import Breadcrumb from "@/components/Breadcrumb";
import BreadcrumbSV from "@/components/BreadcrumbSV";

import Container from "@/components/Container";

import MovieBookingList from "@/components/cinema/MovieBookingList";

import MovieNow from "@/components/cinema/MovieNow";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";

import Image from "next/image";

import Heading from "@/components/Heading";

import SectionGroup from "@/components/section/SectionGroup";

import MapBox from "@/components/ui/MapBox";

import { useSelector, useDispatch } from "react-redux";

import { setRapId } from "@/slices/filmBooking";

import ReplaceURL from "@/lib/replace-url";
import LdJson_QA from "@/components/LdJson_QA";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

function RapDetail({
  pageMaster,
  isMobile,
  dataServiceDetail,
  dataServiceBlocks,
  dataMoviesNow,
  RapData,
}) {
  const dispatch = useDispatch();

  const data = RapData.Data;
  const spDetail = dataServiceDetail.Data;
  const dataFilmsNow = dataMoviesNow.Data ? dataMoviesNow.Data.Items : [];

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadcrumb = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadcrumb = dataServiceBlocks.Data.find(
      (item, index) => item.Type == 23
    );
  }

  useEffect(() => {
    ReplaceURL(data.Link);
  }, []);

  useEffect(() => {
    dispatch(setRapId(data.ApiCinemaId || null));
    return function cleanup() {
      dispatch(setRapId(null));
    };
  }, []);

  return (
    <Page
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      Trace={{ Id: data.Id, Type: "cinema" }}
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={data.Meta} />
      <LdJson_QA QaData={data.QaData} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadcrumb && dataServiceBreadcrumb?.Data?.Items.length > 0 && (
        <div className="jsx-3479491080 relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadcrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadcrumb.Data.Items}
            />
          </Container>
        </div>
      )}

      <div className="rap__hero relative z-10 flex flex-wrap items-end bg-gray-50  pb-4 text-opacity-95 shadow-sm md:pb-8">
        <div
          className=" cine__cover  rap__hero-bg relative top-0 z-0 h-full w-full overflow-hidden bg-cover bg-center bg-no-repeat md:absolute"
          style={{
            backgroundImage: `url(${data.Background})`,
          }}
        ></div>

        <div className="relative z-10 -mt-6 w-full md:-mt-0">
          <Container>
            <div className="rap-detail flex flex-nowrap items-center ">
              <div className="flex-none ">
                <div className="h-16 w-16 overflow-hidden rounded border border-gray-200 bg-white p-1 shadow-sm md:h-28 md:w-28">
                  <div className="aspect-w-1 aspect-h-1 ">
                    {data.Logo.includes("mservice") ? (
                      <Image
                        src={data.Logo}
                        alt={data.Name}
                        layout="fill"
                        objectFit="cover"
                      />
                    ) : (
                      <Image
                        src={data.Avatar}
                        alt={data.Name}
                        layout="fill"
                        objectFit="cover"
                      />
                    )}
                  </div>
                </div>
              </div>

              <div className="min-w-0 flex-1 pl-4 text-white sm:pl-6">
                <h1 className="mb-0  text-xl font-bold text-white md:text-2xl">
                  {data.Name}
                </h1>
                <div className="sm:text-md text-sm text-white text-opacity-80 ">
                  <div className="">{data.Address}</div>
                </div>

                <div className="text-sm ">
                  <MapBox
                    lat={data.Lat}
                    lon={data.Lon}
                    address={data.Address}
                    name={data.Name}
                  >
                    [ Xem Bản đồ ]
                  </MapBox>
                </div>

                {data.CineplexFeaturedNote && (
                  <div className="mb-2 mt-2 flex items-center text-sm text-pink-400 ">
                    <div className="pr-1">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-4 w-4  rotate-90"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"
                        />
                      </svg>
                    </div>
                    <div className="line-clamp-2 leading-tight">
                      {data.CineplexFeaturedNote}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </Container>
        </div>
      </div>

      <div className="bg-gray-50">
        <Container>
          <section className="py-8 md:py-10 lg:py-14" id="cumrap">
            <div className="mb-5 text-center md:mb-8">
              <Heading title={` Lịch chiếu phim ${data.Name}`} color="pink" />
            </div>

            <div className="-mx-5 max-w-3xl overflow-hidden rounded bg-white md:mx-auto md:border md:border-gray-200">
              <MovieBookingList />
            </div>
          </section>
        </Container>
      </div>

      {dataFilmsNow.length > 0 && (
        <section
          className="bg-black bg-contain bg-bottom bg-no-repeat py-8 md:py-10 lg:py-14 "
          style={{
            backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210701105436-637607336767432408.jpg
          )`,
          }}
          id="phimdangchieu"
        >
          <Container>
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Phim đang chiếu"
                color="white"
                isSparkles={true}
              />
            </div>
            <div className="-mx-5 md:mx-0">
              <MovieNow data={dataFilmsNow} isDark={true} isMobile={isMobile} />
            </div>
          </Container>
        </section>
      )}

      {dataServiceGroup && (
        <SectionGroup
          genLdJson={false}
          data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
          isMobile={isMobile}
        />
      )}

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />

      <style jsx>{`
        .rap__hero {
          min-height: 440px;
        }

        .rap__hero:after {
          content: "";
          z-index: 2;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
          background: linear-gradient(
            to bottom,
            rgba(0, 0, 0, 0) 0,
            rgba(0, 0, 0, 0) 30%,
            rgba(0, 0, 0, 0.8) 100%
          );
        }

        @media screen and (max-width: 767px) {
          .rap__hero {
            min-height: auto;
          }

          .rap__hero:after {
            content: "";
            z-index: 2;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: linear-gradient(
              to top,
              rgba(0, 0, 0, 1) 0,
              rgba(0, 0, 0, 1) 35%,
              rgba(0, 0, 0, 0.1) 60%,
              rgba(0, 0, 0, 0) 100%
            );
          }

          .rap__hero-bg {
            height: 250px;
          }
        }
      `}</style>
    </Page>
  );

  // const data = FilmData.Data;
}

export async function getServerSideProps(context) {
  //Check Category hoac Blog detail
  const slug = context.query.slug;

  const RapID = slug.split("-").slice(-1);

  if (RapID != parseInt(RapID, 10)) {
    return {
      notFound: true,
    };
  }

  let RapData = null;

  RapData = await axios.get(`/ci-cinema/detail?id=${RapID}`, null, true);

  if (RapData.Result == false) {
    return {
      notFound: true,
    };
  }

  const dataMoviesNow = await axios.get(
    `/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  let dataServiceDetail = null;
  let dataServiceBlocks = null;

  const servicePageId = RapData.Data.ServicePageId;

  if (servicePageId != null) {
    dataServiceDetail = await axios.get(
      `/service-page/detail?id=${servicePageId}`,
      null,
      true
    );

    dataServiceBlocks = await axios.get(
      `/service-page/listBlocks?pageId=${servicePageId}`,
      null,
      true
    );

    var dataBreadCrumbs = dataServiceBlocks
      ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
      : null;
    if (
      dataBreadCrumbs &&
      dataBreadCrumbs.Data &&
      dataBreadCrumbs.Data.Items &&
      dataBreadCrumbs.Data.Items.length > 0
    ) {
      dataBreadCrumbs.Data.Items.push({
        Title: RapData.Data.Name,
        Url: `https://momo.vn${RapData.Data.Link}`,
      });
    }
  }

  return {
    props: {
      RapData,
      dataCinemaMaster,
      dataServiceDetail,
      dataServiceBlocks,
      dataMoviesNow,
    }, // will be passed to the page component as props
  };
}

export default RapDetail;
