import { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import Page from "@/components/page";

import Container from "@/components/Container";

import CinemaSuggest from "@/components/cinema/CinemaSuggest";

import Breadcrumb from "@/components/Breadcrumb";
import BreadcrumbSV from "@/components/BreadcrumbSV";
import MovieNow from "@/components/cinema/MovieNow";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";

import SectionGroup from "@/components/section/SectionGroup";

import Heading from "@/components/Heading";

import RapRating from "@/components/cinema/RapRating";

import { axios } from "@/lib/index";
import Image from "next/image";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

import { useSelector, useDispatch } from "react-redux";
import { setRapBrand } from "@/slices/filmBooking";

const CinemaBookingAll = dynamic(() =>
  import("@/components/cinema/CinemaBookingAll")
);

// import CinemaSuggest from "@/components/cinema/CinemaSuggest";

function Cineplex({
  pageMaster,
  isMobile,
  dataMoviesNow,
  dataCinemaMaster,
  dataCineplexDetail,
  dataServiceBlocks,
  dataServiceDetail,
}) {
  const dispatch = useDispatch();

  const dataCineMaster = dataCinemaMaster.Data;
  const dataFilmsNow = dataMoviesNow.Data ? dataMoviesNow.Data.Items : [];

  var dataPage = dataCineplexDetail.Data;

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;

  const spDetail = dataServiceDetail.Data;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;
  }

  const dataServiceBreadCrumb = dataServiceBlocks
    ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
    : null;

  //Check chon rap mac dinh
  useEffect(() => {
    dispatch(setRapBrand(dataPage.Id || null));
    return function cleanup() {
      dispatch(setRapBrand(null));
    };
  }, []);

  // const checkHeaderApp = isLP || isViewApp;

  return (
    <Page
      title={dataPage.Meta.Title}
      description={dataPage.Meta.Description}
      image={dataPage.Meta.Avatar}
      keywords={dataPage.Meta.Keywords}
      scripts={dataPage.Meta.Scripts}
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={dataPage.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadCrumb && dataServiceBreadCrumb?.Data?.Items.length > 0 && (
        <div className="jsx-3479491080 relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadCrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadCrumb.Data.Items}
            />
          </Container>
        </div>
      )}

      <div className="rap__hero relative z-10 flex flex-wrap items-end pb-4  text-opacity-95 shadow-sm md:pb-8">
        <div
          className=" cine__cover  rap__hero-bg relative top-0 z-0 h-full w-full overflow-hidden bg-cover bg-center bg-no-repeat md:absolute"
          style={{
            backgroundImage: `url(${dataPage.Background})`,
          }}
        ></div>

        <div className="relative z-10 -mt-6 w-full md:-mt-0">
          <Container>
            <div className="rap-detail flex flex-nowrap items-center ">
              <div className="flex-none ">
                <div className="h-16 w-16 overflow-hidden rounded border border-gray-200 bg-white p-1 shadow-sm md:h-28 md:w-28">
                  <div className="aspect-w-1 aspect-h-1 ">
                    <Image
                      src={dataPage.Logo}
                      alt={dataPage.Name}
                      layout="fill"
                      objectFit="cover"
                    />
                  </div>
                </div>
              </div>

              <div className="min-w-0 flex-1 pl-4 text-white sm:pl-6">
                <h1 className="mb-0  text-2xl font-bold text-white md:text-3xl">
                  {dataPage.Name}
                </h1>
                <div className="sm:text-md text-sm text-white text-opacity-80 ">
                  <div className="overflow-hidden text-ellipsis whitespace-nowrap">
                    {dataPage.Slogan}
                  </div>
                </div>

                {dataPage.RatingValue && (
                  <RapRating
                    Id={dataPage.Id}
                    Count={dataPage.RatingCount}
                    Value={dataPage.RatingValue}
                  />
                )}

                <div className="sm:text-md flex items-center text-sm text-white text-opacity-60">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                  </svg>
                  <span className="ml-1">
                    {dataPage.TotalCinemas} cửa hàng trong hệ thống
                  </span>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>

      <section className="py-6 md:py-10 lg:py-14 " id="cumrap">
        <Container>
          <div className="mb-5 text-center md:mb-8">
            <Heading
              title={` Lịch chiếu phim ${dataPage.Name}`}
              color="pink"
              // subtitle={`${dataPage.TotalCinemas} cụm rạp chiếu phim thuộc
              // ${dataPage.TotalCities} khu vực.`}
            />
          </div>

          <CinemaBookingAll
            master={dataCineMaster}
            sCineplex={dataPage.Id}
            sAlone={true}
          />
        </Container>
      </section>

      {dataFilmsNow.length > 0 && (
        <section
          className="bg-black bg-contain bg-bottom bg-no-repeat py-8 md:py-10 lg:py-14 "
          style={{
            backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210701105436-637607336767432408.jpg
          )`,
          }}
          id="phimdangchieu"
        >
          <Container>
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Phim đang chiếu"
                color="white"
                isSparkles={true}
              />
            </div>
            <div className="-mx-5 md:mx-0">
              <MovieNow data={dataFilmsNow} isDark={true} isMobile={isMobile} />
            </div>
          </Container>
        </section>
      )}
      {dataServiceGroup && (
        <SectionGroup
          data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
          isMobile={isMobile}
        />
      )}

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />

      <style jsx>{`
        .rap__hero {
          min-height: 440px;
        }

        .rap__hero:after {
          content: "";
          z-index: 2;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
          background: linear-gradient(
            to bottom,
            rgba(0, 0, 0, 0) 0,
            rgba(0, 0, 0, 0) 30%,
            rgba(0, 0, 0, 0.8) 100%
          );
        }

        @media screen and (max-width: 767px) {
          .rap__hero {
            min-height: auto;
          }

          .rap__hero:after {
            content: "";
            z-index: 2;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background: linear-gradient(
              to top,
              rgba(0, 0, 0, 1) 0,
              rgba(0, 0, 0, 1) 35%,
              rgba(0, 0, 0, 0.1) 60%,
              rgba(0, 0, 0, 0) 100%
            );
          }

          .rap__hero-bg {
            height: 250px;
          }
        }
      `}</style>
    </Page>
  );
}

// export async function getStaticPaths() {
//   const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

//   const paths = dataCinemaMaster.Data.Cineplexs.map((item) => ({
//     params: { rap: item.UrlRewrite },
//   }));

//   return { paths, fallback: false };
// }

export async function getServerSideProps(context) {
  const slug = context.query.rap;

  const dataMoviesNow = await axios.get(
    `/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );

  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  const dataCineplexDetail = await axios.get(
    `/ci-cinemplex/detail?urlRewrite=${slug}`,
    null,
    true
  );

  if (dataCineplexDetail.Error != null) {
    return {
      notFound: true,
    };
  }

  const servicePageId = dataCineplexDetail?.Data?.ServicePageId;

  let dataServiceBlocks = null;
  let dataServiceDetail = null;

  if (servicePageId != null) {
    dataServiceDetail = await axios.get(
      `/service-page/detail?id=${servicePageId}&type=3`,
      null,
      true
    );

    dataServiceBlocks = await axios.get(
      `/service-page/listBlocks?pageId=${servicePageId}`,
      null,
      true
    );
  }

  //Tam lay Home
  // Pass post data to the page via props
  return {
    props: {
      dataCinemaMaster,
      dataCineplexDetail,
      dataMoviesNow,
      dataServiceBlocks,
      dataServiceDetail,
    },
  };
}

export default Cineplex;
