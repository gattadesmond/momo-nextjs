import Page from "@/components/page";

import Container from "@/components/Container";

// import CinemaMenu from "@/components/cinema/CinemaMenu";
import MovieNow from "@/components/cinema/MovieNow";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";
import dynamic from "next/dynamic";
import Link from "next/link";

import BreadcrumbSV from "@/components/BreadcrumbSV";

import Heading from "@/components/Heading";

import SectionGroup from "@/components/section/SectionGroup";

import { axios } from "@/lib/index";

import CinemaSuggest from "@/components/cinema/CinemaSuggest";

const RapListData = dynamic(() => import("@/components/cinema/RapListData"));

import RapBrandList from "@/components/cinema/RapBrandList";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

import Image from "next/image";
function Cinema({
  dataMoviesNow,
  dataServiceDetail,
  dataServiceBlocks,
  dataCineplexAll,
  dataCinemaMaster,
  pageMaster,
  isMobile,
}) {
  const dataFilmsNow = dataMoviesNow.Data ? dataMoviesNow.Data.Items : [];

  const spDetail = dataServiceDetail.Data;

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;
  }

  const dataCineplexList = dataCineplexAll.Data;

  const dataCineMaster = dataCinemaMaster.Data;

  const dataServiceBreadCrumb = dataServiceBlocks
    ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
    : null;

  // useEffect(() => {
  //   // ReplaceURL(data.Link);
  //   document.querySelector("body").classList.add("page-cinema");
  //   return function clean() {
  //     document.querySelector("body").classList.remove("page-cinema");
  //   };
  // }, []);

  return (
    <Page
      title={spDetail.Meta.Title}
      description={spDetail.Meta.Description}
      image={spDetail.Meta.Avatar}
      keywords={spDetail.Meta.Keywords}
      scripts={spDetail.Meta.Scripts}
      // robots={spDetail.Meta.Robots}
      // robots={
      //   <>
      //     <meta name="robots" content="noindex, nofollow" />
      //     <meta name="googlebot" content="noindex, nofollow" />
      //   </>
      // }
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={spDetail.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadCrumb && dataServiceBreadCrumb?.Data?.Items.length > 0 && (
        <div className=" relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadCrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadCrumb.Data.Items}
            />
          </Container>
        </div>
      )}

      {/* <div
        className="py-8 text-center bg-gray-900 bg-bottom bg-no-repeat bg-w-1 md:py-14"
        style={{
          backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210702152010-637608360107610767.jpg
          )`,
        }}
      >
        <Container>
          <h1 className="text-2xl font-semibold text-white lg:text-3xl">
            Danh sách rạp
          </h1>
          <div className="text-white md:mt-3 text-opacity-70">
            Danh sách tất cả các rạp chiếu phim tại Việt Nam
          </div>
        </Container>
      </div> */}

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName == "AboutH1")}
        isMobile={isMobile}
        startType={3}
      />

      <section className="py-8 md:py-10 lg:py-14  " id="cumrap">
        <Container>
          <div className="mb-5 text-center md:mb-8">
            <Heading title=" Tìm rạp chiếu phim MoMo" color="pink" />
          </div>

          {/* <CinemaSuggest master={dataCineMaster} /> */}
          <RapListData master={dataCineMaster} />
        </Container>
      </section>

      {dataFilmsNow.length > 0 && (
        <section
          className="bg-black bg-contain bg-bottom bg-no-repeat py-8 md:py-10 lg:py-14 "
          style={{
            backgroundImage: `url(
            https://static.mservice.io/img/momo-upload-api-210701105436-637607336767432408.jpg
          )`,
          }}
          id="phimdangchieu"
        >
          <Container>
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Phim đang chiếu"
                color="white"
                isSparkles={true}
              />
            </div>
            <div className="-mx-5 md:mx-0">
              <MovieNow data={dataFilmsNow} isDark={true} isMobile={isMobile} />
            </div>
          </Container>
        </section>
      )}

      <div className="bg-gray-50">
        <Container>
          <section className="py-8 md:py-10 lg:py-14" id="danhsachrap">
            <div className="mb-5 text-center md:mb-8">
              <Heading
                title="Hệ thống rạp chiếu phim"
                subtitle="Danh sách hệ thống rạp chiếu phim lớn có mặt khắp cả nước"
                color="pink"
              />
            </div>

            <RapBrandList dataCineplexList={dataCineplexList} />
          </section>
        </Container>
      </div>

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
        isMobile={isMobile}
      />

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />
    </Page>
  );
}

export async function getStaticProps() {
  const dataMoviesNow = await axios.get(
    `/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`,
    null,
    true
  );
  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  //Tam lay Home
  const servicePageId = dataCinemaMaster?.Data?.ServicePageIds?.ListCinema;

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  const dataCineplexAll = await axios.get(
    `ci-cinemplex/loadMore?lastIndex=0&sortType=2&sortDir=1`,
    null,
    true
  );

  return {
    props: {
      dataMoviesNow,
      dataCinemaMaster,
      dataServiceDetail,
      dataServiceBlocks,
      dataCineplexAll,
    },
    revalidate: 60 * 1,
  };
}

export default Cinema;
