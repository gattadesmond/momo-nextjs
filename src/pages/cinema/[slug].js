import Link from "next/link";

import { useState, useEffect } from "react";

import { axios } from "@/lib/index";

import Page from "@/components/page";

import { parseISO, format } from "date-fns";

import Breadcrumb from "@/components/Breadcrumb";

import Container from "@/components/Container";

import ReadMore from "@/components/ReadMore";
import ReplaceURL from "@/lib/replace-url";
import ActorList from "@/components/cinema/ActorList";

import MovieGallery from "@/components/cinema/MovieGallery";

import MovieLabel from "@/components/cinema/MovieLabel";

import MovieBlog from "@/components/cinema/MovieBlog";
import MovieBlogFilm from "@/components/cinema/MovieBlogFilm";

import BlogListAjax from "@/components/ui/BlogListAjax";
import MovieBooking from "@/components/cinema/MovieBooking";

import MoviePoster from "@/components/cinema/MoviePoster";

import MovieNowWidget from "@/components/cinema/MovieNowWidget";
import LdJson_Meta_Movie from "@/components/LdJson_Meta_Movie";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";

import MovieRating from "@/components/cinema/MovieRating";

import MovieComments from "@/components/cinema/MovieComments";
import LdJson_QA from "@/components/LdJson_QA";

import { useMediaQuery } from "react-responsive";
import { SM } from "@/lib/breakpoint";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

// import Image from "next/image";

function FilmDetail({
  pageMaster,
  isMobile,
  dataCinemaMaster,
  dataServiceDetail,
  dataServiceBlocks,

  FilmData,
}) {
  const data = FilmData.Data;
  const spDetail = dataServiceDetail.Data;

  const extras = data.Extras ?? [];

  const casts = extras.casts ? JSON.parse(extras.casts) : null;

  const backdrops = extras.backdrops ? JSON.parse(extras.backdrops) : null;

  const dataCineMaster = dataCinemaMaster.Data;

  const isSM = useMediaQuery(SM);

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadcrumb = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadcrumb = dataServiceBlocks.Data.find(
      (item, index) => item.Type == 23
    );
  }

  useEffect(() => {
    ReplaceURL(data.Link);
  }, []);

  return (
    <Page
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      scripts={data.Meta.Scripts}
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
      Trace={{ Id: data.Id, Type: "film" }}
    >
      <LdJson_Meta_Movie Meta={data.Meta} Film={data} />

      <LdJson_QA QaData={data.QaData} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      <Container>
        {data.BreadCrumbs && (
          <div className="py-3 md:py-4">
            <LdJson_BreadCrumbs BreadCrumbs={data.BreadCrumbs} />
            <Breadcrumb data={data.BreadCrumbs} />
          </div>
        )}
      </Container>

      <div className="cine__hero relative z-10 flex items-center justify-center bg-black py-6 text-white text-opacity-95">
        <div
          className="cine__cover absolute top-0 z-0 h-full w-full overflow-hidden bg-cover bg-center bg-no-repeat"
          style={{
            backgroundImage: `url(${data.BannerUrl})`,
          }}
        ></div>

        <Container>
          <div className="flex flex-wrap items-center md:flex-nowrap md:space-x-10 lg:items-start">
            <div className="cine__hero__poster mb-4 w-full md:mb-0 md:w-auto">
              <div className="w-24 md:mx-auto md:w-64">
                <MoviePoster
                  img={data.GraphicUrl}
                  name={data.TitleEn}
                  trailer={data.ApiAutoplayTrailer}
                  trailerYTB={data.TrailerUrl}
                  banner={data.BannerUrl}
                />
              </div>
            </div>

            <div className="relative z-10 w-full md:w-auto">
              <MovieLabel data={data.ApiRating} />

              <h1 className="mt-2 text-3xl font-bold text-white md:text-4xl">
                {data.Title}
              </h1>

              <ul className="mt-2 flex flex-wrap items-center text-white text-opacity-60">
                <li>{data.TitleEn}</li>
                {data.Meta.Date && (
                  <li className="mx-2 text-base font-normal">·</li>
                )}

                <li>
                  {data.Meta.Date && format(parseISO(data.Meta.Date), "yyyy")}
                </li>

                <li className="mx-2 text-base font-normal">·</li>

                <li>{data.Duration} phút</li>
              </ul>

              <div className="flex flex-nowrap space-x-3 overflow-x-auto overflow-y-hidden pt-2 pb-3  ">
                {data.ApiImdb && (
                  <div className="cine__score imdb  flex shrink-0 flex-nowrap  items-center">
                    <div className="">
                      <svg
                        id="home_img"
                        className="w-11"
                        xmlns="http://www.w3.org/2000/svg"
                        width={64}
                        height={32}
                        viewBox="0 0 64 32"
                        version="1.1"
                      >
                        <g fill="#F5C518">
                          <rect x={0} y={0} width="100%" height="100%" rx={4} />
                        </g>
                        <g
                          transform="translate(8.000000, 7.000000)"
                          fill="#000000"
                          fillRule="nonzero"
                        >
                          <polygon points="0 18 5 18 5 0 0 0" />
                          <path d="M15.6725178,0 L14.5534833,8.40846934 L13.8582008,3.83502426 C13.65661,2.37009263 13.4632474,1.09175121 13.278113,0 L7,0 L7,18 L11.2416347,18 L11.2580911,6.11380679 L13.0436094,18 L16.0633571,18 L17.7583653,5.8517865 L17.7707076,18 L22,18 L22,0 L15.6725178,0 Z" />
                          <path d="M24,18 L24,0 L31.8045586,0 C33.5693522,0 35,1.41994415 35,3.17660424 L35,14.8233958 C35,16.5777858 33.5716617,18 31.8045586,18 L24,18 Z M29.8322479,3.2395236 C29.6339219,3.13233348 29.2545158,3.08072342 28.7026524,3.08072342 L28.7026524,14.8914865 C29.4312846,14.8914865 29.8796736,14.7604764 30.0478195,14.4865461 C30.2159654,14.2165858 30.3021941,13.486105 30.3021941,12.2871637 L30.3021941,5.3078959 C30.3021941,4.49404499 30.272014,3.97397442 30.2159654,3.74371416 C30.1599168,3.5134539 30.0348852,3.34671372 29.8322479,3.2395236 Z" />
                          <path d="M44.4299079,4.50685823 L44.749518,4.50685823 C46.5447098,4.50685823 48,5.91267586 48,7.64486762 L48,14.8619906 C48,16.5950653 46.5451816,18 44.749518,18 L44.4299079,18 C43.3314617,18 42.3602746,17.4736618 41.7718697,16.6682739 L41.4838962,17.7687785 L37,17.7687785 L37,0 L41.7843263,0 L41.7843263,5.78053556 C42.4024982,5.01015739 43.3551514,4.50685823 44.4299079,4.50685823 Z M43.4055679,13.2842155 L43.4055679,9.01907814 C43.4055679,8.31433946 43.3603268,7.85185468 43.2660746,7.63896485 C43.1718224,7.42607505 42.7955881,7.2893916 42.5316822,7.2893916 C42.267776,7.2893916 41.8607934,7.40047379 41.7816216,7.58767002 L41.7816216,9.01907814 L41.7816216,13.4207851 L41.7816216,14.8074788 C41.8721037,15.0130276 42.2602358,15.1274059 42.5316822,15.1274059 C42.8031285,15.1274059 43.1982131,15.0166981 43.281155,14.8074788 C43.3640968,14.5982595 43.4055679,14.0880581 43.4055679,13.2842155 Z" />
                        </g>
                      </svg>
                    </div>
                    <div className="px-2 text-xl font-bold">
                      {data.ApiImdb}
                      <span className="text-sm font-normal">/10</span>
                    </div>
                  </div>
                )}

                {data.ApiRottenTomatoes && (
                  <div className="cine__score tomatoes  flex shrink-0 flex-nowrap  items-center">
                    <div className="">
                      <svg
                        width={80}
                        height={80}
                        viewBox="0 0 80 80"
                        className="h-6 w-6"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                      >
                        <defs>
                          <path id="a" d="M0 .247h77.083v63.468H0z" />
                        </defs>
                        <g fill="none" fillRule="evenodd">
                          <g transform="translate(1.328 16.266)">
                            <mask id="b" fill="#fff">
                              <use xlinkHref="#a" />
                            </mask>
                            <path
                              d="M77.014 27.043C76.242 14.674 69.952 5.42 60.488.247c.053.301-.215.678-.52.545-6.19-2.708-16.693 6.056-24.031 1.466.055 1.647-.267 9.682-11.585 10.148-.268.011-.415-.262-.246-.455 1.514-1.726 3.042-6.097.845-8.428-4.706 4.217-7.44 5.804-16.463 3.71C2.711 13.274-.562 21.542.08 31.841c1.311 21.025 21.005 33.044 40.837 31.806 19.83-1.236 37.408-15.58 36.097-36.604"
                              fill="#FA320A"
                              mask="url(#b)"
                            />
                          </g>
                          <path
                            d="M42.2 11.465c4.075-.971 15.796-.095 19.551 4.887.225.299-.092.864-.455.705-6.19-2.708-16.693 6.056-24.032 1.467.056 1.647-.266 9.682-11.585 10.148-.267.01-.414-.262-.245-.455 1.514-1.727 3.042-6.098.845-8.428-5.127 4.594-7.906 6.07-19.032 3.062-.364-.098-.24-.683.147-.83 2.103-.804 6.867-4.324 11.374-5.876a15.308 15.308 0 0 1 2.549-.657c-4.963-.444-7.2-1.134-10.356-.658a.392.392 0 0 1-.367-.627c4.253-5.478 12.088-7.132 16.922-4.222-2.98-3.692-5.314-6.636-5.314-6.636l5.53-3.142 3.948 8.82c4.114-6.078 11.768-6.639 15.001-2.326.192.256-.008.62-.328.613-2.633-.064-4.082 2.33-4.192 4.15l.039.005"
                            fill="#00912D"
                          />
                        </g>
                      </svg>
                    </div>
                    <div className="px-2 text-xl font-bold">
                      {data.ApiRottenTomatoes}
                    </div>
                  </div>
                )}

                {data.ApiMetacritic && (
                  <div className="cine__score tomatoes flex shrink-0 flex-nowrap  items-center">
                    <div className="">
                      <img
                        src="https://static.mservice.io/next-js/_next/static/public/cinema/metacritic.svg"
                        className="img-fluid"
                        alt=""
                        width={24}
                      />
                    </div>
                    <div className="px-2 text-xl font-bold">
                      {data.ApiMetacritic}
                    </div>
                  </div>
                )}

                {data.ApiTmdb && (
                  <div className="cine__score tomatoes flex shrink-0 flex-nowrap  items-center">
                    <div className="">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="w-6"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 185.04 133.4"
                      >
                        <defs>
                          <style
                            dangerouslySetInnerHTML={{
                              __html: ".cls-1{fill:url(#linear-gradient);}",
                            }}
                          />
                          <linearGradient
                            id="linear-gradient"
                            y1="66.7"
                            x2="185.04"
                            y2="66.7"
                            gradientUnits="userSpaceOnUse"
                          >
                            <stop offset={0} stopColor="#90cea1" />
                            <stop offset="0.56" stopColor="#3cbec9" />
                            <stop offset={1} stopColor="#00b3e5" />
                          </linearGradient>
                        </defs>
                        <title>Asset 4</title>
                        <g id="Layer_2" data-name="Layer 2">
                          <g id="Layer_1-2" data-name="Layer 1">
                            <path
                              className="cls-1"
                              d="M51.06,66.7h0A17.67,17.67,0,0,1,68.73,49h-.1A17.67,17.67,0,0,1,86.3,66.7h0A17.67,17.67,0,0,1,68.63,84.37h.1A17.67,17.67,0,0,1,51.06,66.7Zm82.67-31.33h32.9A17.67,17.67,0,0,0,184.3,17.7h0A17.67,17.67,0,0,0,166.63,0h-32.9A17.67,17.67,0,0,0,116.06,17.7h0A17.67,17.67,0,0,0,133.73,35.37Zm-113,98h63.9A17.67,17.67,0,0,0,102.3,115.7h0A17.67,17.67,0,0,0,84.63,98H20.73A17.67,17.67,0,0,0,3.06,115.7h0A17.67,17.67,0,0,0,20.73,133.37Zm83.92-49h6.25L125.5,49h-8.35l-8.9,23.2h-.1L99.4,49H90.5Zm32.45,0h7.8V49h-7.8Zm22.2,0h24.95V77.2H167.1V70h15.35V62.8H167.1V56.2h16.25V49h-24ZM10.1,35.4h7.8V6.9H28V0H0V6.9H10.1ZM39,35.4h7.8V20.1H61.9V35.4h7.8V0H61.9V13.2H46.75V0H39Zm41.25,0h25V28.2H88V21h15.35V13.8H88V7.2h16.25V0h-24Zm-79,49H9V57.25h.1l9,27.15H24l9.3-27.15h.1V84.4h7.8V49H29.45l-8.2,23.1h-.1L13,49H1.2Zm112.09,49H126a24.59,24.59,0,0,0,7.56-1.15,19.52,19.52,0,0,0,6.35-3.37,16.37,16.37,0,0,0,4.37-5.5A16.91,16.91,0,0,0,146,115.8a18.5,18.5,0,0,0-1.68-8.25,15.1,15.1,0,0,0-4.52-5.53A18.55,18.55,0,0,0,133.07,99,33.54,33.54,0,0,0,125,98H113.29Zm7.81-28.2h4.6a17.43,17.43,0,0,1,4.67.62,11.68,11.68,0,0,1,3.88,1.88,9,9,0,0,1,2.62,3.18,9.87,9.87,0,0,1,1,4.52,11.92,11.92,0,0,1-1,5.08,8.69,8.69,0,0,1-2.67,3.34,10.87,10.87,0,0,1-4,1.83,21.57,21.57,0,0,1-5,.55H121.1Zm36.14,28.2h14.5a23.11,23.11,0,0,0,4.73-.5,13.38,13.38,0,0,0,4.27-1.65,9.42,9.42,0,0,0,3.1-3,8.52,8.52,0,0,0,1.2-4.68,9.16,9.16,0,0,0-.55-3.2,7.79,7.79,0,0,0-1.57-2.62,8.38,8.38,0,0,0-2.45-1.85,10,10,0,0,0-3.18-1v-.1a9.28,9.28,0,0,0,4.43-2.82,7.42,7.42,0,0,0,1.67-5,8.34,8.34,0,0,0-1.15-4.65,7.88,7.88,0,0,0-3-2.73,12.9,12.9,0,0,0-4.17-1.3,34.42,34.42,0,0,0-4.63-.32h-13.2Zm7.8-28.8h5.3a10.79,10.79,0,0,1,1.85.17,5.77,5.77,0,0,1,1.7.58,3.33,3.33,0,0,1,1.23,1.13,3.22,3.22,0,0,1,.47,1.82,3.63,3.63,0,0,1-.42,1.8,3.34,3.34,0,0,1-1.13,1.2,4.78,4.78,0,0,1-1.57.65,8.16,8.16,0,0,1-1.78.2H165Zm0,14.15h5.9a15.12,15.12,0,0,1,2.05.15,7.83,7.83,0,0,1,2,.55,4,4,0,0,1,1.58,1.17,3.13,3.13,0,0,1,.62,2,3.71,3.71,0,0,1-.47,1.95,4,4,0,0,1-1.23,1.3,4.78,4.78,0,0,1-1.67.7,8.91,8.91,0,0,1-1.83.2h-7Z"
                            />
                          </g>
                        </g>
                      </svg>
                    </div>
                    <div className="px-2 text-xl font-bold">{data.ApiTmdb}</div>
                  </div>
                )}
              </div>

              <p className="mb-3 italic text-white text-opacity-60">
                {extras.tagline}
              </p>

              <h3 className="text-xl font-bold text-white text-opacity-90">
                Nội dung
              </h3>

              <div className="mt-2 text-sm leading-relaxed text-white text-opacity-70">
                {data.SynopsisEn && (
                  <ReadMore maxLine="4" btnText="... Xem thêm" type="line">
                    {data.SynopsisEn}
                  </ReadMore>
                )}

                {/* <p>{data.SynopsisEn}</p> */}
              </div>

              <div className="mt-3 text-sm text-gray-700 ">
                <div className="mb-2 flex flex-nowrap space-x-4 md:space-x-5">
                  {data.OpeningDate && (
                    <div className="">
                      <div className="whitespace-nowrap text-white text-opacity-50">
                        Ngày chiếu
                      </div>
                      <div className="mt-1 font-bold text-white text-opacity-90">
                        {format(parseISO(data.OpeningDate), "dd/MM/yyyy")}
                      </div>
                    </div>
                  )}

                  <div>
                    <div className="whitespace-nowrap text-white text-opacity-50">
                      Thể loại
                    </div>
                    <div className="mt-1 font-bold text-white text-opacity-90">
                      {data.ApiGenreName}
                    </div>
                  </div>

                  {data?.Countries?.length > 0 && (
                    <div>
                      <div className="whitespace-nowrap text-white text-opacity-50">
                        Quốc gia
                      </div>
                      <div className="mt-1 font-bold text-white text-opacity-90">
                        {data.Countries.map((x) => x.Name).join(", ")}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>

      <Container>
        <div className="grid grid-cols-1 lg:grid-cols-3 lg:gap-12">
          <div className="lg:col-span-2 lg:col-start-1">
            <section className="border-t border-gray-200 py-8 ">
              <div className="mb-2 sm:mb-0">
                <h2 className="text-xl font-bold   md:pr-80">
                  Lịch chiếu {data.Title}
                </h2>
              </div>

              <div className="relative mt-4">
                <MovieBooking
                  filmIdApi={data.ApiFilmId ?? null}
                  master={dataCineMaster}
                  isMobile={isMobile}
                />
              </div>
            </section>
            {/* 
            <section className="py-8 border-t border-gray-200">
              <h3 className="text-xl font-bold">Nội dung</h3>

              <div className="mt-4 leading-relaxed text-gray-500">
                <p>{data.SynopsisEn}</p>
              </div>
            </section> */}

            <section className="border-t border-gray-200 py-8">
              <h3 className="text-xl font-bold">Cộng đồng MoMo nghĩ gì?</h3>

              {!isSM && (
                <div className="mt-3 mb-3">
                  <MovieRating id={data.ApiFilmId} />
                </div>
              )}

              <div className="mt-3">
                <MovieComments id={data.ApiFilmId} />
              </div>
            </section>

            {casts && (
              <section className="border-t border-gray-200 py-8">
                <h3 className="text-xl font-bold">Diễn viên & Đoàn làm phim</h3>
                {casts.length > 0 && (
                  <div className="mt-4">
                    <ActorList data={casts} />
                  </div>
                )}
              </section>
            )}

            {backdrops && backdrops.length > 0 && (
              <section className="border-t border-gray-200 py-8">
                <h3 className="text-xl font-bold">Hình ảnh & Video</h3>

                <MovieGallery data={backdrops} />
              </section>
            )}

            {/* <section className="py-8 border-t border-gray-200">
              <h3 className="text-xl font-bold mb-4">Nội dung</h3>

            </section> */}

            {data.BlogReviewId && <MovieBlogFilm blogId={data.BlogReviewId} />}

            <section className="border-t border-gray-200 py-8">
              <h3 className="mb-4 text-xl font-bold">Blog phim</h3>
              {data.JsonBlog && data.JsonBlog.Projects.length > 0 ? (
                <BlogListAjax
                  item={data.JsonBlog.Projects[0]}
                  isNumber={3}
                  isMobile={isMobile}
                />
              ) : (
                <MovieBlog data={dataServiceBlocks} />
              )}
            </section>
          </div>

          <div className="lg:grid-start-3">
            {isSM && (
              <div className="mt-8">
                <MovieRating id={data.ApiFilmId} />
              </div>
            )}

            <div className="mt-6 mb-6">
              <h3 className="mb-2 flex-1 text-xl font-bold">Phim đang chiếu</h3>
              <MovieNowWidget />
            </div>
          </div>
        </div>
      </Container>

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />

      <style jsx>{`
        .cine__hero {
          min-height: 500px;
        }
        .cine__cover {
          background-position: right -200px top;
        }
        .cine__cover:after {
          content: "";
          z-index: 2;
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;

          background-image: linear-gradient(
            to right,
            rgba(0, 0, 0, 1) 150px,
            rgba(0, 0, 0, 0.6) 100%
          );
        }

        @media screen and (max-width: 767px) {
          .cine__cover {
            background-position: left top;
            background-size: 130% auto;
            height: 250px;
          }

          .cine__cover:after {
            content: "";
            z-index: 2;
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-image: linear-gradient(
              to bottom,
              rgba(0, 0, 0, 0) 0%,
              rgba(0, 0, 0, 0) 50%,
              rgba(0, 0, 0, 0.8) 80%,
              rgba(0, 0, 0, 1) 100%
            );
          }
        }
        .cine__cover__play {
        }
      `}</style>
    </Page>
  );
}

export async function getServerSideProps(context) {
  //Check Category hoac Blog detail
  const slug = context.query.slug;

  if (slug == undefined) {
    return {
      notFound: true,
    };
  }

  const FilmID = slug.split("-").slice(-1);

  if (FilmID != parseInt(FilmID, 10)) {
    return {
      notFound: true,
    };
  }

  let FilmData = null;

  FilmData = await axios.get(`/ci-film/detail?id=${FilmID}`, null, true);

  if (FilmData.Result == false) {
    return {
      notFound: true,
    };
  }

  const dataCinemaMaster = await axios.get(`/common/cinema`, null, true);

  const servicePageId = dataCinemaMaster?.Data?.ServicePageIds?.Home;

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  return {
    props: { FilmData, dataCinemaMaster, dataServiceDetail, dataServiceBlocks }, // will be passed to the page component as props
  };
}

export default FilmDetail;
