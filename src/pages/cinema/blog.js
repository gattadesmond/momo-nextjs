import Page from "@/components/page";

import Container from "@/components/Container";

import SectionGroup from "@/components/section/SectionGroup";

import BreadcrumbSV from "@/components/BreadcrumbSV";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";
import Header from "@/components/service/Header";
import Footer from "@/components/service/Footer";
import FooterCta from "@/components/FooterCta";

import { axios } from "@/lib/index";

function Blog({
  dataServiceDetail,
  dataServiceBlocks,
  pageMaster,
  isMobile,
}) {
  
  const spDetail = dataServiceDetail.Data;

  let dataServiceGroup = null;
  let dataServiceMenu = null;
  let dataServiceMenuFooter = null;
  let dataServiceBreadCrumb = null;

  if (dataServiceBlocks != null) {
    dataServiceGroup = dataServiceBlocks.Data.filter(
      (item, index) =>
        item.Type != 2 && item.Type != 22 && item.Type != 23 && item.Type != 32
    );

    dataServiceMenu = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 32
    )[0];
    dataServiceMenu = dataServiceMenu?.Data;

    dataServiceMenuFooter = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 33
    )[0];
    dataServiceMenuFooter = dataServiceMenuFooter?.Data;

    dataServiceBreadCrumb = dataServiceBlocks
      ? dataServiceBlocks.Data.find((item, index) => item.Type == 23)
      : null;
  }

  return (
    <Page
      title={spDetail.Meta.Title}
      description={spDetail.Meta.Description}
      image={spDetail.Meta.Avatar}
      keywords={spDetail.Meta.Keywords}
      scripts={spDetail?.Meta?.Scripts}
      // robots={spDetail.Meta.Robots}
      // robots={
      //   <>
      //     <meta name="robots" content="noindex, nofollow" />
      //     <meta name="googlebot" content="noindex, nofollow" />
      //   </>
      // }
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={spDetail.HeaderType == 1}
      isShowFooter={spDetail.ShowFooter && spDetail.FooterType == 1}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={spDetail.Meta} />

      {spDetail.HeaderType == 2 && dataServiceMenu && (
        <Header data={dataServiceMenu} isMobile={isMobile} />
      )}

      {dataServiceBreadCrumb && dataServiceBreadCrumb?.Data?.Items.length > 0 && (
        <div className="jsx-3479491080 relatice top-0 z-30 bg-white py-3 shadow-sm  md:py-4">
          <Container>
            <BreadcrumbSV items={dataServiceBreadCrumb.Data.Items} />
            <LdJson_BreadCrumbs
              BreadCrumbs={dataServiceBreadCrumb.Data.Items}
            />
          </Container>
        </div>
      )}

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName == "AboutH1")}
        isMobile={isMobile}
        startType={3}
      />

      <SectionGroup
        data={dataServiceGroup.filter((item) => item.TypeName != "AboutH1")}
        isMobile={isMobile}
      />

      {spDetail.ShowFooter && spDetail.FooterType == 2 && (
        <Footer menuServices={dataServiceMenuFooter} />
      )}

      <FooterCta data={dataServiceGroup} isMobile={isMobile} />
    </Page>
  );
}

export async function getStaticProps() {
  
  const servicePageId = dataCinemaMaster?.Data?.ServicePageIds?.Blog;

  const dataServiceDetail = await axios.get(
    `/service-page/detail?id=${servicePageId}`,
    null,
    true
  );

  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${servicePageId}`,
    null,
    true
  );

  return {
    props: {
      dataServiceDetail,
      dataServiceBlocks,
    },
    revalidate: 60 * 1,
  };
}

export default Blog;
