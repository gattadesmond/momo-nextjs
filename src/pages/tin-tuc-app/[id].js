import dynamic from "next/dynamic";

import { axios } from "@/lib/index";

import PageArticle from "@/components/article/PageArticle";

function ArticleAppDetail({
  dataPostDetail,
  dataCatePost,
  listPrefer,
  listDonation,
  isCateDonation,
  pageMaster,
  isMobile,
}) {
  return (
    <PageArticle
      isReplaceUrl={false}
      dataPostDetail={dataPostDetail}
      dataCatePost={dataCatePost}
      listPrefer={listPrefer}
      listDonation={listDonation}
      isCateDonation={isCateDonation}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  );
}

export async function getServerSideProps(context) {
  const detailId = context.query.id;

  if (!detailId) {
    return {
      notFound: true,
    };
  }

  const dataPostDetail = await axios.get(
    `/article/${detailId}?countRelating=4`,
    null,
    true
  );
  if (!dataPostDetail || !dataPostDetail.Result || !dataPostDetail.Data) {
    return {
      notFound: true,
    };
  }

  const dataCatePost = await axios.get(`/article/cates`, null, true);

  let listPrefer = null;
  let listDonation = null;

  const isCateDonation = dataPostDetail.Data.Category.Id === 118 ? true : false;
  if (isCateDonation) {
    listDonation = {
      detail: null,
      TraiTim: null,
      HeoDat: null
    };
    listDonation.detail = await axios.get(`/donation/list-by-article?articleId=${detailId}`, null, true);
    const excludeIds = `${listDonation.detail.Data.Id ? '&excludeIds=' + listDonation.detail.Data.Id : ''}`;
    listDonation.TraiTim = await axios.get(`/donation/list?type=1&lastIdx=0&count=3&sortType=1&sortDir=2${excludeIds}`, null, true);
    listDonation.HeoDat = await axios.get(`/donation/list?type=2&lastIdx=0&count=3&sortType=1&sortDir=2${excludeIds}`, null, true);
  } else {
    listPrefer = await axios.get(`landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=6`, null, true);
  }

  return {
    props: {
      dataPostDetail,
      dataCatePost,
      listPrefer,
      listDonation,
      isCateDonation
    },
  };
}

export default ArticleAppDetail;
