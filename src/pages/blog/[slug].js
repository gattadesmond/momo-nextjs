import Head from "next/head";

import Page from "@/components/page";

import { useRouter } from "next/router";

import Container from "@/components/Container";

import Link from "next/link";

import Image from "next/image";

function Post({ dataPostDetail }) {
  // const { data } = useSWR(`http://dev.momo.vn/api/blog/list`, fetcher);
  console.log(dataPostDetail.Data);
  const article = dataPostDetail.Data;

  return (
    <Page
    title={`${article.Title} | MoMo`}
    description={`${article.Title} | Descrition`}
    image={article.Avatar}
    >
     {/* <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head> */}

      <Container>
        <article className="  py-12">
          <Image
            src={article.Avatar}
            alt={article.Title}
            layout="responsive"
            width={1900}
            height={600}
          />

          <div className=" max-w-2xl mx-auto mt-10">
            <h1 className="font-bold text-2xl md:text-3xl tracking-tight mb-4 text-black dark:text-white">
              {article.Title}
            </h1>
            <p className="text-gray-700 dark:text-gray-300 italic">{article.Short}</p>
          </div>
          <div
            className="prose mx-auto "
            dangerouslySetInnerHTML={{ __html: article.Content }}
          ></div>
        </article>
      </Container>
    </Page>
  );
}

export async function getServerSideProps(context) {
  const slug = context.query.slug;
  const slugDtIndex = slug.lastIndexOf("dt");
  const idBlog = slug.slice(slugDtIndex + 2);

  // console.log(idBlog);

  // Fetch data from 4 post
  const resPostDetail = await fetch(`http://dev.momo.vn/api/blog/${idBlog}`);
  console.table(resPostDetail);
  const dataPostDetail = await resPostDetail.json();

  // Fetch data from home post

  return {
    props: { dataPostDetail }, // will be passed to the page component as props
  };
}

export default Post;
