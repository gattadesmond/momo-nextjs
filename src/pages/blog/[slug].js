


import dynamic from "next/dynamic";

import { axios } from "@/lib/index";

import { useRouter } from "next/router";

const PageCategory = dynamic(() =>
  import("../../components/blog/PageCategory")
);
const PageArticle = dynamic(() => import("../../components/blog/PageArticle"));

function Soju({
  categoryObj,
  dataPostDetail,
  dataHomePost,
  pageMaster,
  isMobile,
  isArticle,
}) {
  const router = useRouter();

  if (router.isFallback) {
    return <div></div>;
  }

  return (
    <>
      {isArticle ? (
        <PageArticle
          dataPostDetail={dataPostDetail}
          pageMaster={pageMaster}
          isMobile={isMobile}
        />
      ) : (
        <PageCategory
          categoryObj={categoryObj}
          dataHomePost={dataHomePost}
          pageMaster={pageMaster}
          isMobile={isMobile}
        />
      )}
    </>
  );
}

export async function getStaticProps({ params }) {

  const slugDtIndex = params.slug.lastIndexOf("dt");


  let dataPostDetail = null;

  let dataHomePost = null;

  let categoryObj = null;

  let isArticle = true;

  if (slugDtIndex != -1) {
    //Day la article
    const idBlog = params.slug.slice(slugDtIndex + 2);


    if (idBlog != parseInt(idBlog, 10)) {

      return {
        notFound: true,
      };
    }


    dataPostDetail = await axios.get(`/blog/${idBlog}`, null, true);


    if (dataPostDetail.Result == false) {
      return {
        notFound: true,
      };
    }


  } else {
    isArticle = false;
    const category = params.slug;
    const dataCatePost = await axios.get(`/blog/cates`, null, true);
    categoryObj =
      dataCatePost.Data.find((x) => x.Link.includes(category)) || null;

    if (categoryObj) {
      dataHomePost = await axios.get(`/blog/cate/${category}`, null, true);
      // dataFeaturePost = await axios.get(`/blog/list?cateId=${categoryObj.Id}&sortType=1&count=4`);
    } else {
      dataHomePost = await axios.get(`/blog/home`, null, true);
      dataHomePost.isHome = true;
    }

    if (dataHomePost == null)
      return {
        notFound: true,
      };

  }


  return {
    props: { dataPostDetail, categoryObj, dataHomePost, isArticle },
    revalidate: 60 * 1,
  };
}

export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const queryAllPosts = await axios.get(
    `blog/list-route?count=1000`,
    null,
    true
  );

  const allPosts = queryAllPosts?.Data ?? null;

  const queryAllCats = await axios.get(`blog/cates`, null, true);

  const allCats = queryAllCats?.Data ?? null;

  function getPureSlug(url) {
    if (!url) return null;
    return url.split("/").slice(-1).pop();
  }

  const pathPosts =
    allPosts?.map((post) => ({
      params: { slug: getPureSlug(post.Link) },
    })) ?? [];



  const pathCats =
    allCats?.map((cat) => ({
      params: { slug: getPureSlug(cat.Link) },
    })) ?? [];

  const paths = pathCats.concat(pathPosts) ?? [];

  return { paths, fallback: true };
}

export default Soju;
