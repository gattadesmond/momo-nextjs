import { useEffect } from "react";

import dynamic from "next/dynamic";

import { axios } from "@/lib/index";

const PageCategory = dynamic(() =>
  import("../../components/blog/PageCategory")
);

function Soju({ categoryObj, dataHomePost, pageMaster, isMobile }) {
  // const data = dataHomePost.Data;
  return (
    <>
      <PageCategory
        categoryObj={categoryObj}
        dataHomePost={dataHomePost}
        pageMaster={pageMaster}
        isMobile={isMobile}
      />
    </>
  );
}

export async function getStaticProps() {
  
  let dataHomePost = null;
  let categoryObj = null;
  dataHomePost = await axios.get(`/blog/home`, null, true);

  return {
    props: {  categoryObj, dataHomePost },
    // Revalidates the page every minute
    revalidate: 60 * 1,
  };
}

export default Soju;
