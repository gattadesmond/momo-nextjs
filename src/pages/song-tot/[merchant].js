import { axios } from "@/lib/index";

import PageService from "@/components/service/PageService";

const DonationMerchantPage = ({
  servicePage,
  dataServiceBreadcrumbs,
  serviceBlocks,
  pageMaster,
  isMobile
}) => {

  return (
    <PageService 
      dataServiceBreadcrumbs={dataServiceBreadcrumbs}
      dataServiceDetail={servicePage}
      dataServiceListBlock={serviceBlocks}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  )
};
export async function getServerSideProps({ query }) {
  const url = query.merchant;

  const servicePage = await axios.get(`/service-page/detail-by-slug?slug=${url}&type=2`, null, true);
  if (!servicePage || servicePage.Result == false) {
    return {
      notFound: true,
    };
  }

  const serviceId = servicePage.Data.Id;
  const serviceParentId = servicePage.Data.ParentId;

  const dataServiceBreadcrumbs = await axios.get(`/service-page/listBreadcrumbs?id=${serviceId}${serviceParentId ? '&parentId=' + serviceParentId : ''}`, null, true);
  const serviceBlocks = await axios.get(`/service-page/listBlocks?pageId=${serviceId}`, null, true);

  servicePage.Data.Link = '/song-tot' + servicePage.Data.Link;
  
  return {
    props: {servicePage, dataServiceBreadcrumbs, serviceBlocks}
  };
}


export default DonationMerchantPage;