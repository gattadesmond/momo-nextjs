import { useEffect } from "react";

import Page from "@/components/page";
function RedirectCtaUc({ query, isMobile }) {
  var redirect = () => {
    setTimeout(function () {
      debugger
      location.href = query.to;
    }, 2000);
  };

  useEffect(() => {
    redirect();
  }, []);
  return <Page isMobile={isMobile} isViewApp={true}></Page>;
}
export async function getServerSideProps(context) {
  const query = context.query;
  return {
    props: { query },
  };
}

export default RedirectCtaUc;
