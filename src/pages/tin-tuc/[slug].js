import PageArticleCategory from "@/components/article/PageArticleCategory";
import { axios } from "@/lib/index";

function ArticleHome({
  categoryObj,
  dataCatePost,
  dataHomePost,
  listPrefer,
  listDonation,
  isCateDonation,
  pageMaster,
  isMobile
}) {
  return (
    <PageArticleCategory
      categoryObj={categoryObj}
      dataCategory={dataCatePost}
      dataHomePost={dataHomePost}
      listPrefer={listPrefer}
      listDonation={listDonation}
      isCateDonation={isCateDonation}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  )
};
export async function getServerSideProps(context) {
  const slug = context.query.slug;

  const dataCatePost = await axios.get(`/article/cates`, null, true);

  const categoryObj = dataCatePost.Data.find((x) => x.Link.includes(slug)) || null;
  if (!categoryObj) {
    return {
      notFound: true,
    }; 
  }

  if (!categoryObj) {
    return {
      notFound: true,
    };
  }

  const dataHomePost = await axios.get(`/article/cate/${slug}?countFeatured=1&countList=8&countTopView=4`, null, true);

  let listPrefer = null;
  let listDonation = null;

  const isCateDonation = categoryObj.Id === 118 ? true : false;
  if (isCateDonation) {
    listDonation = {
      TraiTim: null,
      HeoDat: null
    };
    listDonation.TraiTim = await axios.get(`/donation/list?type=1&lastIdx=0&count=2&sortType=1&sortDir=2`, null, true);
    listDonation.HeoDat = await axios.get(`/donation/list?type=2&lastIdx=0&count=2&sortType=1&sortDir=2`, null, true);
  } else {
    listPrefer = await axios.get(`landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=4`, null, true);
  }

  return {
    props: { categoryObj, dataCatePost, dataHomePost, listPrefer, listDonation, isCateDonation }, // will be passed to the page component as props
  };
}

export default ArticleHome;