import PageArticleHome from "@/components/article/PageArticleHome";
import { axios } from "@/lib/index";

function ArticleHome({
  categoryObj,
  dataCategory,
  dataHomePost,
  listArticle,
  topViewArticle,
  listPrefer,
  pageMaster,
  isMobile
}) {
  return (
    <PageArticleHome
      categoryObj={categoryObj}
      dataHomePost={dataHomePost}
      dataCategory={dataCategory}
      listArticle={listArticle}
      topViewArticle={topViewArticle}
      listPrefer={listPrefer}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  )
};
export async function getServerSideProps(context) {
  const slug = context.query.slug;

  const dataCategory = await axios.get(`/article/cates`, null, true);

  const categoryObj = dataCategory.Data.find((x) => x.Link.includes(slug)) || null;

  const dataHomePost = await axios.get(`/article/home?countFeatured=3&countPerCategory=1`, null, true);
  const listFeatureId = (dataHomePost.Data?.ListFeatured?.Items || []).map(x => x.Id).join(',');

  const listArticle = await axios.get(`/article/list?sortType=1&count=8&excludeIds=${listFeatureId}`, null, true);

  const listPrefer = await axios.get(`landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=4`, null, true);

  return {
    props: { categoryObj, dataCategory, dataHomePost, listArticle, listPrefer }, // will be passed to the page component as props
  };
}

export default ArticleHome;