import { axios } from "@/lib/index";

import Container from "@/components/Container";
import Page from "@/components/page";
import { useState } from "react";

const PageSearch = (props) => {
  const { q, metaData, pageMaster, isMobile } = props;
  const [query, setQuery] = useState(q);
  const handleSetQuery = (event) => {
    setQuery(event.target.value);
  };
  return (
    <Page
      title={metaData.title}
      description={metaData.description}
      keywords={metaData.keywords}
      header={pageMaster?.Data?.MenuHeaders}
    >
      <div className="header-search bg-cover py-10 md:py-14">
        <div className="max-w-4xl mx-auto w-full px-5 md:px-8 lg:px-8">
          <h1 className=" text-2xl md:text-3xl text-white font-inter font-bold ">
            Tìm kiếm
          </h1>

          <form
            className=" mt-3 md:mt-5 flex shadow-lg rounded-lg overflow-hidden"
            action="/tim-kiem"
          >
            <input
              name="q"
              value={query}
              type="text"
              className=" block w-full  focus:border-indigo border border-transparent  text-black sm:flex-1 px-4 sm:px-6 py-3 lg:py-4 focus:outline-none"
              id=""
              placeholder="Chào bạn, Ví MoMo có thể giúp gì được cho bạn?"
              onChange={(e) => {
                handleSetQuery(e);
              }}
            />
            <button
              type="submit"
              className=" shadow-lg whitespace-nowrap flex flex-nowrap items-center  w-auto  focus:outline-none bg-pink-600 hover:bg-pink-500 focus:bg-pink-500 text-white text-shadow   font-bold px-4 py-3 lg:py-4 "
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 mr-1"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                />
              </svg>{" "}
              <span className="hidden sm:block">Tìm kiếm</span>
            </button>
          </form>
        </div>
      </div>

      <div className="max-w-4xl mx-auto w-full px-1 md:px-6 lg:px-6 min-h-[500px]">
        <script
          async
          src="https://cse.google.com/cse.js?cx=10b37241e3a7b87dd"
        ></script>

        <div
          dangerouslySetInnerHTML={{
            __html: `
            <div class="gcse-searchresults-only"></div>
          `,
          }}
        ></div>
      </div>

      <style jsx>{`
        .header-search {
          width: 100%;
          background-image: url(https://static.mservice.io/styles/desktop/images/hoidap/hoidap-head-bg.jpg);
          background-color: #d68fb7;
        }
       
      `}</style>

      <style jsx global>{`
        .gsc-above-wrapper-area-container  td {
          border: none;
          padding:0;
        }
        .gsc-above-wrapper-area-container{
          margin: 0;
          font-size: 13px;
        }
        .gcsc-find-more-on-google-magnifier{
          display: inline;
        }
       
      `}</style>
    </Page>
  );
};

export async function getServerSideProps(context) {
  const q = context.query.q;
  const metaData = {
    title: "Kết quả tìm kiếm '" + q + "' - Ví MoMo - Ví điện tử số 1 Việt Nam",
    description:
      "Ví MoMo giúp bạn thanh toán hàng trăm tiện ích như ✅Nạp tiền Điện Thoại ✅ Hóa Đơn Điện, Nước, Internet, Truyền hình  ✅Chuyển Nhận Tiền ✅Mua Sắm ... an toàn, bảo mật, tiện lợi‎, miễn phí giao dịch.",
    keywords:
      "Ví MoMo, sự kiện ví MoMo, thanh toán MoMo, đối tác MoMo, chuyển tiền MoMo, dịch vụ MoMo, tiện ích MoMo, thanh toán hóa đơn MoMo, ví điện tử, nạp tiền điện thoại MoMo, nạp tiền điện thoại online",
  };
  return {
    props: { q, metaData }, // will be passed to the page component as props
  };
}

export default PageSearch;
