import { useState, useEffect } from "react";
import Page from "@/components/page";

function PageRedirect() {
  return <Page></Page>;
}

export async function getServerSideProps(context) {
  return {
    redirect: {
      destination: "https://momo.vn/lixi",
      permanent: true,
    },
  };
}
export default PageRedirect;
