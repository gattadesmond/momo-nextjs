import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Page from "@/components/page";

import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

import Container from "@/components/Container";
import Bokeh from "@/components/lacxi/bokeh";
import { Dialog } from "@headlessui/react";
import Rive from "rive-react";
import Link from "next/link";

import { Fade, AttentionSeeker } from "react-awesome-reveal";

import Router from "next/router";

import { axios } from "@/lib/index";
import Image from "next/image";
import Head from "next/head";

import ReadMoreHTML from "@/components/ReadMoreHTML";

import Scrollspy from "react-scrollspy";

import AnchorLink from "react-anchor-link-smooth-scroll";

import ButtonShare from "@/components/ui/ButtonShare";

import FooterCta from "@/components/FooterCta";

function Lacxi({ dataService, dataServiceBlocks, pageMaster, isMobile }) {
  var dataPage = dataService.Data;

  const dataServiceGroup = dataServiceBlocks.Data.filter(
    (item, index) => item.Type != 2
  );

  const offSet = 30;

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);

  useEffect(() => {
    var _isViewApp =
      dataPage.ViewInApp && router?.query?.view == "app" ? true : false;
    if (_isViewApp) setIsViewApp(_isViewApp);
  }, [router]);

  useEffect(() => {
    try {
      window.ReactNativeWebView.postMessage("GetUserInfo");
      if (dataPage.ViewInApp) setIsViewApp(true);
    } catch {}

    // ReplaceURL(data.Link);
  }, []);
  const footerCtaData = [
    {
      TypeName: "CtaFooter",
      Cta: {
        Link: "",
        Text: "TẢI APP MOMO",
        NewTab: true,
        RedirectUC: "lacxi2022",
        QrCodeId: 3,
      },
    },
  ];

  return (
    <Page
      title={dataPage?.Meta?.Title}
      description={dataPage?.Meta?.Description}
      image={dataPage?.Meta?.Avatar}
      keywords={dataPage?.Meta?.Keywords}
      scripts={dataPage?.Meta?.Scripts}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
      isViewApp={isViewApp}
      isLP={true}
    >
      <div
        className="antialiased 
    relative z-10 overflow-x-hidden "
      >  
        <div className="fixed inset-0 -z-10 bg-bottom bg-no-repeat bg-gradient-to-b from-[#FFEBE9] via-[#FFEBE9] to-[#FFEBE9]  "></div>

        <div className="hero-container " id="lacxi">
          <section className=" bg-no-repeat bg-bottom  md:w-full relative overflow-hidden z-10  bg-cover bg-hero pb-[300px] min-h-screen flex items-center justify-center">
            <Bokeh />

            <div className="pt-20 md:pt-16 relative px-4  z-30 ">
              <Fade direction="up" triggerOnce duration="600">
                <div className="border-lx max-w-3xl mx-auto relative z-10">
                  <div className="px-0 py-2 md:py-5 md:px-7 font-lx text-lg text-center md:text-3xl bg-[#ffebe9] text-gray-600 leading-normal md:leading-normal">
                    Nội dung chương trình đang được cập nhật. 
                 

                  </div>
                </div>
              </Fade>
            </div>

          </section>
        </div>

        <style jsx global>
          {`
            .navbar-app {
              background-color: transparent;
              backdrop-filter: none;
              -webkit-backdrop-filter: none;
              box-shadow: none;
              padding-top: max(env(safe-area-inset-top), 22px);
            }

            .font-lx {
              font-family: "UTMColossalis", sans-serif;
            }

            @font-face {
              font-family: "UTMColossalis";
              src: url("https://static.mservice.io/next-js/_next/static/public/lacxi/UTMColossalis.woff2") format("woff2"),
                url("https://static.mservice.io/next-js/_next/static/public/lacxi/UTMColossalis.woff") format("woff");
              font-weight: normal;
              font-style: normal;
              font-display: swap;
            }

            .border-lx {
              border: 30px solid orange;

              border-style: solid;
              border-width: 30px;

              border-image: url(https://static.mservice.io/fileuploads/svg/momo-file-220104173957.svg)
                15 round;
            }

            .border-lx-sm {
              border: 14px solid orange;

              border-style: solid;
              border-width: 14px;

              border-image: url(https://static.mservice.io/fileuploads/svg/momo-file-220104173957.svg)
                14 round;
            }

            .img-mask2 {
              clip-path: ellipse(radius-x, radius-y at x, y);
            }

            .text-lx {
              color: #fcf2c3;
            }

            .text-lx-2 {
              color: #8a0000;
            }

            .heading-lx {
              color: #fcf2c3;
              text-shadow: 0px 2px 1px rgba(0, 0, 0, 0.4);
            }

            .heading-lx-2 {
              text-shadow: 0px 2px 1px rgba(0, 0, 0, 0.4);
            }

            .hero-container {
              filter: drop-shadow(0 5px 5px #0002);
            }

            .hero {
              -webkit-clip-path: url(#my-clip-path);
              clip-path: url(#my-clip-path);
            }

            @media (max-width: 769px) {
              .hero {
                -webkit-clip-path: url(#my-clip-path2);
                clip-path: url(#my-clip-path2);
              }
            }

            .bg-hero {
              background-color: #aa0667;
              background-image: url("https://static.mservice.io/img/momo-upload-api-220106135055-637770738555292214.jpg");
            }

            @media (max-width: 767.98px) {
              .bg-hero {
                background-image: url("https://static.mservice.io/img/momo-upload-api-220108152814-637772524941951679.jpg");
              }
            }

            .img-mask2 {
              -webkit-clip-path: url(#my-clip-path3);
              clip-path: url(#my-clip-path3);
            }
            .img-mask-container2 {
              position: relative;
              filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.15));
            }

            .img-mask-container2:after {
              content: "";
              position: absolute;
              -webkit-clip-path: url(#my-clip-path3);
              clip-path: url(#my-clip-path3);
              left: -4px;
              right: -4px;
              top: -4px;
              bottom: -4px;
              z-index: -1;
              background-color: #8a0000;
            }
            .box-lx {
              background: #ffe0d3;
              box-shadow: inset 0px 1px 3px rgba(226, 146, 133, 0.4);
            }

            .is-current a {
              color: rgb(252, 211, 77);
            }
            .is-current-mobile a {
              color: #8a0000;
            }
          `}
        </style>
      </div>
    </Page>
  );
}

export async function getStaticProps() {
  const dataService = await axios.get(
    `/service-page/detail-by-slug?slug=lixi`,
    null,
    true
  );
  
  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${dataService.Data.Id}`,
    null,
    true
  );

  return {
    props: {
      dataService,
      dataServiceBlocks,
    },
    revalidate: 60 * 1,
  };
}

export default Lacxi;
