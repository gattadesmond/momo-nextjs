import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import MaxApi from "@momo-platform/max-api";

import dynamic from "next/dynamic";
import Page from "@/components/page";

import LdJson_QA from "@/components/LdJson_QA";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

import Container from "@/components/Container";
import { Dialog } from "@headlessui/react";
// import Link from "next/link";

import Reveal from "react-awesome-reveal";
import { keyframes } from "@emotion/react";

import Router from "next/router";

import { axios } from "@/lib/index";
import Image from "next/image";

import CountUp from "react-countup";
// import Head from "next/head";

import ReadMoreHTML from "@/components/ReadMoreHTML";

import Scrollspy from "react-scrollspy";

import AnchorLink from "react-anchor-link-smooth-scroll";

import ButtonShare from "@/components/ui/ButtonShare";

import FooterCta from "@/components/FooterCta";

import ListCustomer from "@/components/lacxi/ListCustomer";

import SearchCustomer from "@/components/lacxi/SearchCustomer";

import Confetti from "react-confetti";

const ProjectArticles = dynamic(() =>
  import("@/components/lacxi/ProjectArticles")
);

const ProjectArticles2 = dynamic(() =>
  import("@/components/section/ProjectArticles")
);

const QuestionList = dynamic(() => import("@/components/lacxi/QuestionList"));
const QuestionList2 = dynamic(() => import("@/components/lacxi/QuestionList2"));

const LineLX = () => {
  return (
    <div className="   mx-auto w-full max-w-6xl px-5 md:px-8 lg:px-8">
      <div className="relative h-3  overflow-hidden">
        <img
          src="https://static.mservice.io/fileuploads/svg/momo-file-220117161804.svg"
          className="absolute left-1/2 mx-auto block w-auto max-w-none -translate-x-1/2"
        />
      </div>
    </div>
  );
};

const customAnimation = keyframes`
  from {
    opacity: 0;
    transform: translateY(40px) translateZ(0px);
  }

  to {
    opacity: 1;
    transform: translateY(0%) translateZ(0px);
  }
`;

const NavPopover = ({ huongdanActive, tintucActive }) => {
  let [isOpen, setIsOpen] = useState(false);
  const offSet = 25;
  useEffect(() => {
    if (!isOpen) return;
    function handleRouteChange() {
      setIsOpen(false);
    }
    Router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      Router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [isOpen]);

  return (
    <div className="">
      <button
        type="button"
        className="flex h-10 w-10 items-center justify-center text-gray-500 hover:text-gray-600 dark:text-gray-400 dark:hover:text-gray-300"
        onClick={() => setIsOpen(true)}
      >
        <span className="sr-only">Navigation</span>

        <img
          src="https://static.mservice.io/fileuploads/svg/momo-file-220108132714.svg"
          className="block h-9 w-9 "
        />
      </button>

      <Dialog
        as="div"
        className="fixed inset-0 z-50"
        open={isOpen}
        onClose={setIsOpen}
      >
        <Dialog.Overlay className="fixed inset-0 bg-black/80 " />

        <div className="fixed top-4 right-4 w-full max-w-[280px]    text-base  text-gray-900   ">
          <div className="px-3  pb-4 pt-14">
            <button
              type="button"
              className="absolute -top-1 right-0 h-8 w-8 "
              onClick={() => setIsOpen(false)}
            >
              <span className="sr-only">Close navigation</span>

              <img
                src="https://static.mservice.io/fileuploads/svg/momo-file-220108135005.svg"
                className="block w-full"
              />
            </button>

            <img
              src="https://static.mservice.io/img/momo-upload-api-220108140840-637772477202614445.png"
              className=" pointer-events-none absolute -bottom-32 right-0 select-none"
              loading="lazy"
              width={100}
            />

            <Scrollspy
              items={[
                "tongket",
                "thuthachthantai",
                "tracuuketqua",
                "thele",
                "tintuc",
                "doitac",
                "hoidap",
              ]}
              currentClassName="is-current-mobile"
              offset={-offSet - 5}
              className="space-y-4 text-right text-lg"
            >
              <li>
                <AnchorLink
                  href="#tongket"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Tổng kết
                </AnchorLink>
              </li>

              <li>
                <AnchorLink
                  href="#thuthachthantai"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Thử thách thần tài
                </AnchorLink>
              </li>

              <li>
                <AnchorLink
                  href="#tracuuketqua"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Tra cứu kết quả
                </AnchorLink>
              </li>

              <li>
                <AnchorLink
                  href="#thele"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Thể lệ
                </AnchorLink>
              </li>
              {tintucActive && (
                <li>
                  <AnchorLink
                    href="#tintuc"
                    onClick={() => setIsOpen(false)}
                    offset={offSet}
                    className="font-lx text-white"
                  >
                    Tin tức
                  </AnchorLink>
                </li>
              )}

              <li>
                <AnchorLink
                  href="#doitac"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Đối tác
                </AnchorLink>
              </li>

              <li>
                <AnchorLink
                  href="#hoidap"
                  onClick={() => setIsOpen(false)}
                  offset={offSet}
                  className="font-lx text-white"
                >
                  Hỏi đáp
                </AnchorLink>
              </li>
            </Scrollspy>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

const ExitApp = () => {
  try {
    MaxApi.goBack();
  } catch (error) {}
};

const HeaderApp = ({ dataPage, isViewApp, children }) => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    let scrolling = false,
      previousTop = 0,
      scrollDelta = 10,
      scrollOffset = 150;

    let selectNav = document.querySelector(".soju-wrapper");

    function handleScrollNav() {
      let currentTop = selectNav.scrollTop;
      if (previousTop - currentTop > scrollDelta) {
        setVisible(false);
      } else if (
        currentTop - previousTop > scrollDelta &&
        currentTop > scrollOffset
      ) {
        setVisible(true);
      }
      previousTop = currentTop;
      scrolling = false;
    }

    // function setScrollVisible() {
    //   window.pageYOffset > 500 ? setVisible(true) : setVisible(false);
    // }
    selectNav.addEventListener("scroll", handleScrollNav, { passive: true });

    return function clean() {
      selectNav.removeEventListener("scroll", handleScrollNav);
    };
  }, []);

  return (
    <header
      className={`navbar-app  fixed top-0 left-0 right-0 z-20 mx-auto flex w-full items-center justify-between px-3  transition-all md:px-8 lg:px-8   ${
        visible ? "-translate-y-10 opacity-0" : "opacity-1 translate-y-0"
      }`}
    >
      <button type="button" className="py-3 px-1" onClick={() => ExitApp()}>
        <img
          src="https://static.mservice.io/fileuploads/svg/momo-file-220109160042.svg"
          className=" rounded-md border-2 border-[#6F1010] bg-[#6F1010]"
        />
      </button>

      <ButtonShare
        link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${dataPage.Link}`}
        isViewApp={isViewApp}
      >
        <button type="button" className="py-3 px-1">
          <img
            src="https://static.mservice.io/fileuploads/svg/momo-file-220109170310.svg"
            className=" rounded-md border-2 border-[#6F1010] bg-[#6F1010]"
          />
        </button>
      </ButtonShare>
    </header>
  );
};

function Lacxi({
  dataService,
  dataServiceBlocks,
  topList,
  pageMaster,
  isMobile,
}) {
  var dataPage = dataService.Data;

  const dataServiceGroup = dataServiceBlocks.Data.filter(
    (item, index) => item.Type != 2
  ).sort((x) => x.DisplayOrder);

  const tintucActive =
    dataServiceGroup.filter(
      (item) => item.TypeName == "ProjectArticles" && item.Template == 1
    ).length > 0;

  const huongdanActive = true;

  const offSet = 30;

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);
  const [ctaConfig, setCtaConfig] = useState(null);

  const [confetti1, setConfetti1] = useState(false);
  const [confetti2, setConfetti2] = useState(false);
  const [confetti3, setConfetti3] = useState(false);
  const [confetti4, setConfetti4] = useState(false);

  const [blockQuestionGroup, setBlockQuestionGroup] = useState(null);
  const [blockRules, setBlockRules] = useState(null);

  const buildCta = (viewApp) => {
    // var ctaBlockConfig = {
    //   Data: [
    //     {
    //       Link: "",
    //       Text: "CHƠI NGAY",
    //       NewTab: true,
    //       RedirectUC: "lacxi2022",
    //       QrCodeId: 533,
    //       Type: 0,
    //     },
    //     {
    //       Link: "",
    //       Text: "CHƠI NGAY",
    //       NewTab: true,
    //       RedirectUC: "lacxi2022",
    //       QrCodeId: 534,
    //       Type: 1,
    //       UtmSource: "facebook",
    //       UtmCampaign: "lacxi2022",
    //       UtmMedium: "ads",
    //     },
    //     {
    //       Link: "",
    //       Text: "CHƠI NGAY",
    //       NewTab: true,
    //       RedirectUC: "lacxi2022",
    //       QrCodeId: 535,
    //       Type: 1,
    //       UtmSource: "google",
    //       UtmCampaign: "lacxi2022",
    //       UtmMedium: "ads",
    //     },
    //     {
    //       Link: "momo://?refId=lixi_2022",
    //       Text: "CHƠI NGAY",
    //       NewTab: false,
    //       Type: 2,
    //       UtmSource: "",
    //       UtmCampaign: "",
    //       UtmMedium: "",
    //     },
    //     {
    //       Link: "http://momo.vn/download#lacxi2022.upapp",
    //       Text: "CẬP NHẬT NGAY",
    //       NewTab: true,
    //       Type: 2,
    //       UtmSource: "in_app",
    //       UtmCampaign: "lacxi2022",
    //       UtmMedium: "noti",
    //     },
    //   ],
    // };

    var ctaBlockConfig = dataServiceBlocks.Data.filter(
      (item, index) => item.Type == 31
    )[0];

    var jsonCtas = ctaBlockConfig?.Data;
    if (!jsonCtas || jsonCtas.length < 1) {
      setCtaConfig(null);
      return;
    }

    var ctas = [];
    var utm_source = router?.query?.utm_source;
    var utm_campaign = router?.query?.utm_campaign;
    var utm_medium = router?.query?.utm_medium;

    if (utm_source) {
      ctas = jsonCtas.filter(
        (x) =>
          x.UtmSource == utm_source.toLocaleLowerCase() &&
          x.UtmMedium == utm_medium.toLocaleLowerCase() &&
          x.Type == (viewApp ? 2 : 1)
      );
    } else {
      ctas = jsonCtas.filter((x) => x.Type == (viewApp ? 2 : 1));
    }

    ctas = ctas.length < 1 ? jsonCtas.filter((x) => x.Type == 0) : ctas;

    setCtaConfig(ctas[0]);
  };

  useEffect(() => {
    var _isViewApp =
      dataPage.ViewInApp && router?.query?.view == "app" ? true : false;
    if (_isViewApp) setIsViewApp(_isViewApp);

    buildCta(_isViewApp);

    setTimeout(() => {
      try {
        window.ReactNativeWebView.postMessage("GetUserInfo");
        if (dataPage.ViewInApp) {
          setIsViewApp(true);
          buildCta(true);
        }
      } catch {}
    }, 1000);
  }, [router]);

  useEffect(() => {
    var dataBlockRules = dataServiceGroup
      .filter((item) => item.TypeName == "QuestionGroup")
      .slice(0, 1);

    setBlockRules(dataBlockRules);

    var dataBlockQuestionGroup = dataServiceGroup
      .filter((item) => item.TypeName == "QuestionGroup")
      .slice(1, 2);

    setBlockQuestionGroup(dataBlockQuestionGroup);

    document.querySelector("body").classList.add("page-lacxi");
    return function clean() {
      document.querySelector("body").classList.remove("page-lacxi");
    };
    // ReplaceURL(data.Link);
  }, []);

  return (
    <Page
      title={dataPage?.Meta?.Title}
      description={dataPage?.Meta?.Description}
      image={dataPage?.Meta?.Avatar}
      keywords={dataPage?.Meta?.Keywords}
      scripts={dataPage?.Meta?.Scripts}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
      isViewApp={isViewApp}
      isLP={true}
      GTM={{
        Disabled: false,
        SwapIdCase: isViewApp ? 2 : 1,
      }}
    >
      <div className="overflow-x-hidden  antialiased ">
        <div className="fixed inset-0 -z-10 bg-gradient-to-b from-[#FFEBE9] via-[#FFEBE9] to-[#FFEBE9] bg-bottom bg-no-repeat  "></div>

        {!isViewApp && (
          <header
            className="  bg-content  fixed top-0 left-0 right-0 z-20 mx-auto h-[63px] w-full  max-w-[1151px] bg-center bg-no-repeat  px-3 md:px-8  lg:px-8"
            style={{
              backgroundImage:
                "url(https://static.mservice.io/img/momo-upload-api-220106134058-637770732580872802.png)",
            }}
          >
            <div className="font-lx relative flex h-[54px] items-center justify-between text-sm leading-6 text-gray-200">
              <div className="logo flex  items-center">
                <a href="https://momo.vn/">
                  <svg
                    viewBox="0 0 72 72"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mx-auto mt-0.5 block h-12 w-12"
                  >
                    <path
                      d="M51.859 10C45.6394 10 40.5057 15.0349 40.5057 21.3533C40.5057 27.5729 45.5407 32.7065 51.859 32.7065C58.0786 32.7065 63.2123 27.6716 63.2123 21.3533C63.2123 15.1337 58.1774 10 51.859 10ZM51.859 26.1908C49.1935 26.1908 47.0215 24.0188 47.0215 21.3533C47.0215 18.6877 49.1935 16.5158 51.859 16.5158C54.5246 16.5158 56.6965 18.6877 56.6965 21.3533C56.6965 24.0188 54.5246 26.1908 51.859 26.1908Z"
                      fill="#fef3c7"
                    />
                    <path
                      d="M28.7576 10C26.8818 10 25.1048 10.5923 23.6239 11.6783C22.2418 10.5923 20.4648 10 18.4903 10C13.7515 10 10 13.8502 10 18.4903V32.7065H16.5158V18.4903C16.5158 17.4043 17.4043 16.6145 18.3915 16.6145C19.4775 16.6145 20.2673 17.503 20.2673 18.4903V32.7065H26.7831V18.4903C26.7831 17.4043 27.6716 16.6145 28.6589 16.6145C29.7448 16.6145 30.5346 17.503 30.5346 18.4903V32.7065H37.0504V18.589C37.2479 13.8502 33.4963 10 28.7576 10Z"
                      fill="#fef3c7"
                    />
                    <path
                      d="M51.859 37.6427C45.6394 37.6427 40.5057 42.6776 40.5057 48.996C40.5057 55.2156 45.5407 60.3492 51.859 60.3492C58.0786 60.3492 63.2123 55.3143 63.2123 48.996C63.2123 42.6776 58.1774 37.6427 51.859 37.6427ZM51.859 53.7347C49.1935 53.7347 47.0215 51.5628 47.0215 48.8972C47.0215 46.2317 49.1935 44.0598 51.859 44.0598C54.5246 44.0598 56.6965 46.2317 56.6965 48.8972C56.6965 51.6615 54.5246 53.7347 51.859 53.7347Z"
                      fill="#fef3c7"
                    />
                    <path
                      d="M28.7576 37.6427C26.8818 37.6427 25.1048 38.235 23.6239 39.321C22.2418 38.235 20.4648 37.6427 18.4903 37.6427C13.7515 37.6427 10 41.4929 10 46.133V60.3492H16.5158V46.0342C16.5158 44.9483 17.4043 44.1585 18.3915 44.1585C19.4775 44.1585 20.2673 45.047 20.2673 46.0342V60.2505H26.7831V46.0342C26.7831 44.9483 27.6716 44.1585 28.6589 44.1585C29.7448 44.1585 30.5346 45.047 30.5346 46.0342V60.2505H37.0504V46.133C37.2479 41.3942 33.4963 37.6427 28.7576 37.6427Z"
                      fill="#fef3c7"
                    />
                  </svg>
                </a>
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220108131606-637772445661806036.png"
                  className="absolute left-1/2 ml-1 block -translate-x-1/2 md:relative md:left-0  md:translate-x-0"
                />
              </div>

              <div className="flex items-center">
                <div className="ml-2 lg:hidden">
                  <NavPopover
                    huongdanActive={huongdanActive}
                    tintucActive={tintucActive}
                  />
                </div>

                <div className="hidden items-center pr-16 lg:flex">
                  <nav>
                    <Scrollspy
                      items={[
                        "tongket",
                        "thuthachthantai",
                        "tracuuketqua",

                        "thele",
                        "tintuc",
                        "doitac",
                        "hoidap",
                      ]}
                      currentClassName="is-current"
                      offset={-offSet - 5}
                      className="flex items-center space-x-6 text-lg"
                    >
                      <li>
                        <AnchorLink
                          href="#tongket"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Tổng kết
                        </AnchorLink>
                      </li>

                      <li>
                        <AnchorLink
                          href="#thuthachthantai"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Thử thách thần tài
                        </AnchorLink>
                      </li>

                      <li>
                        <AnchorLink
                          href="#tracuuketqua"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Tra cứu kết quả
                        </AnchorLink>
                      </li>

                      <li>
                        <AnchorLink
                          href="#thele"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Thể lệ
                        </AnchorLink>
                      </li>
                      {tintucActive && (
                        <li>
                          <AnchorLink
                            href="#tintuc"
                            offset={offSet}
                            className="font-lx text-yellow-100 hover:text-yellow-300"
                          >
                            Tin tức
                          </AnchorLink>
                        </li>
                      )}

                      <li>
                        <AnchorLink
                          href="#doitac"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Đối tác
                        </AnchorLink>
                      </li>

                      {/* <li>
                        <AnchorLink
                          href="#hoidap"
                          offset={offSet}
                          className="font-lx text-yellow-100 hover:text-yellow-300"
                        >
                          Hỏi đáp
                        </AnchorLink>
                      </li> */}
                    </Scrollspy>
                  </nav>
                </div>
              </div>
            </div>
          </header>
        )}

        {isViewApp && <HeaderApp dataPage={dataPage} isViewApp={isViewApp} />}

        <svg className="absolute h-0 w-0">
          <clipPath id="my-clip-path3" clipPathUnits="objectBoundingBox">
            <path d="M0.954,0.037 C0.954,0.017,0.944,0,0.933,0 H0.067 C0.056,0,0.046,0.017,0.046,0.037 C0.046,0.062,0.035,0.081,0.021,0.081 C0.009,0.081,0,0.098,0,0.119 V0.881 C0,0.902,0.009,0.919,0.021,0.919 C0.035,0.919,0.046,0.938,0.046,0.963 C0.046,0.983,0.056,1,0.067,1 H0.933 C0.944,1,0.954,0.983,0.954,0.963 C0.954,0.938,0.965,0.919,0.979,0.919 C0.991,0.919,1,0.902,1,0.881 V0.119 C1,0.098,0.991,0.081,0.979,0.081 C0.965,0.081,0.954,0.062,0.954,0.037" />
          </clipPath>
        </svg>

        <section
          className="vi-[#ffbe84] relative z-10 bg-gradient-to-b from-[#FFD3AD] via-[#FFD3AD] to-[#E9AB62]
            bg-right-bottom bg-no-repeat pt-16 pb-10 md:pt-24 md:pb-20"
          id="tongket"
        >
          <div
            className="absolute inset-0 -z-10 opacity-50 mix-blend-overlay "
            style={{
              backgroundImage:
                "url(https://static.mservice.io/img/momo-upload-api-220217015503-637806597036398858.png)",
            }}
          ></div>

          <div className="absolute top-20   left-10 -z-10">
            <img
              src="https://static.mservice.io/img/momo-upload-api-220117184213-637780417339357688.png"
              className="block max-w-full"
              loading="lazy"
            />
          </div>

          <div className=" absolute  bottom-0 left-0 right-0 ">
            <div className=" relative mx-auto max-w-6xl">
              <div className="absolute -bottom-4  left-0 -z-10 lg:-left-24 ">
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220219140626-637808763862742000.png"
                  className="block w-36 max-w-full md:w-56 lg:w-[280px]"
                  width={280}
                  loading="lazy"
                />
              </div>

              <div className="absolute -bottom-4  right-0 -z-10 lg:-right-24">
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220219140636-637808763968554648.png"
                  className="block w-36 max-w-full  md:w-56  lg:w-[280px]"
                  width={280}
                  loading="lazy"
                />
              </div>
            </div>
          </div>

          <Container>
            <div className="grid  grid-cols-1 gap-8 ">
              <div className=" order-2 sm:order-1">
                <Reveal keyframes={customAnimation} triggerOnce duration="600">
                  <div className="text-center">
                    <Image
                      src="https://static.mservice.io/img/momo-upload-api-220218165244-637807999646578540.png"
                      alt=" Lắc xì MoMo"
                      className="mx-auto block"
                      width={400}
                      height={198}
                    />
                  </div>
                </Reveal>

                <div className=" mx-auto mt-4 max-w-3xl">
                  <Reveal
                    keyframes={customAnimation}
                    triggerOnce
                    duration={500}
                  >
                    {isMobile ? (
                      <h1 className="font-lx text-lx-2  heading-shadow  mb-6  text-center  text-2xl md:text-3xl md:leading-snug ">
                        CẢM ƠN BẠN ĐÃ TRẢI NGHIỆM LẮC XÌ 2022
                        <br />
                        VÀ CÙNG LAN TỎA
                        <br />
                        THÔNG ĐIỆP
                        <span className="block ">
                          “TẾT MÀ, TẾT LÀ PHẢI VUI!”
                        </span>
                      </h1>
                    ) : (
                      <h1 className="font-lx text-lx-2  heading-shadow  mb-6  text-center  text-2xl md:text-3xl md:leading-snug ">
                        CẢM ƠN BẠN ĐÃ TRẢI NGHIỆM LẮC XÌ 2022
                        <br />
                        VÀ CÙNG LAN TỎA THÔNG ĐIỆP
                        <span className="block ">
                          “TẾT MÀ, TẾT LÀ PHẢI VUI!”
                        </span>
                      </h1>
                    )}
                  </Reveal>
                </div>
              </div>
            </div>
          </Container>

          <Container>
            <div className="mx-auto mt-4  grid max-w-4xl  grid-cols-4 gap-4 md:grid-cols-3 lg:gap-8">
              <Reveal
                keyframes={customAnimation}
                triggerOnce
                duration={500}
                cascade={false}
                className="col-span-2 md:col-span-1"
                delay={300}
                onVisibilityChange={(inView) =>
                  inView && setTimeout(() => setConfetti1(true), 100)
                }
              >
                <div className="item relative">
                  <div className="img-mask-container">
                    <div className=" img-mask flex max-w-full overflow-hidden rounded-md border-2 border-[#B0091B] shadow-lg">
                      <Image
                        src="https://static.mservice.io/img/momo-upload-api-220217182722-637807192420552733.jpg"
                        alt="12 TRIỆU + người chơi"
                        width={800}
                        height={450}
                      />
                    </div>
                  </div>
                  <h3 className="font-lx text-lx-2 heading-shadow mt-2 text-center text-lg leading-snug sm:mt-4 sm:text-xl">
                    <CountUp start={0} end={12} duration={1.5} delay={0.2} />{" "}
                    TRIỆU +
                  </h3>

                  <div className="font-lx text-center text-gray-900 text-opacity-80 sm:mt-1  sm:text-lg sm:leading-snug">
                    Người chơi
                  </div>
                  {confetti1 && (
                    <div className="confetti absolute top-1/2 left-1/2">
                      <Confetti
                        width="400px"
                        height="400px"
                        recycle={false}
                        numberOfPieces={20}
                        initialVelocityX={5}
                        initialVelocityY={10}
                        confettiSource={{ x: 150, y: 150, w: 100, h: 100 }}
                        colors={[
                          "#EF8E99",
                          "#F4B43F",
                          "#458B8B",
                          "#a50064",
                          "#8a0000",
                          "#a10203",
                        ]}
                      />
                    </div>
                  )}
                </div>
              </Reveal>
              <Reveal
                keyframes={customAnimation}
                triggerOnce
                duration={500}
                className="col-span-2 md:col-span-1"
                cascade={false}
                delay={800}
                onVisibilityChange={(inView) =>
                  inView && setTimeout(() => setConfetti2(true), 700)
                }
              >
                <div className="item relative">
                  <div className="img-mask-container">
                    <div className=" img-mask flex max-w-full overflow-hidden rounded-md border-2 border-[#B0091B] shadow-lg">
                      <Image
                        src="https://static.mservice.io/img/momo-upload-api-220217181447-637807184877588648.jpg"
                        alt="300 TRIỆU + Lượt chơi"
                        width={800}
                        height={450}
                      />
                    </div>
                  </div>

                  <h3 className="font-lx text-lx-2 heading-shadow mt-2 text-center text-lg leading-snug sm:mt-4 sm:text-xl">
                    <CountUp start={0} end={300} duration={2} delay={0.7} />{" "}
                    TRIỆU +
                  </h3>

                  <div className="font-lx text-center text-gray-900 text-opacity-80 sm:mt-1  sm:text-lg sm:leading-snug">
                    Lượt lắc/quay
                  </div>

                  {confetti2 && (
                    <div className="confetti absolute top-1/2 left-1/2">
                      <Confetti
                        width="400px"
                        height="400px"
                        recycle={false}
                        numberOfPieces={20}
                        initialVelocityX={5}
                        initialVelocityY={10}
                        confettiSource={{ x: 150, y: 150, w: 100, h: 100 }}
                        colors={[
                          "#EF8E99",
                          "#F4B43F",
                          "#458B8B",
                          "#a50064",
                          "#8a0000",
                          "#a10203",
                        ]}
                      />
                    </div>
                  )}
                </div>
              </Reveal>

              <Reveal
                keyframes={customAnimation}
                triggerOnce
                duration={500}
                cascade={false}
                className="col-span-2 col-start-2 md:col-start-3 md:col-span-1"
                delay={1300}
                onVisibilityChange={(inView) =>
                  inView && setTimeout(() => setConfetti3(true), 1300)
                }
              >
                <div className="item relative">
                  <div className="img-mask-container">
                    <div className=" img-mask flex max-w-full  overflow-hidden rounded-md border-2 border-[#B0091B] shadow-lg">
                      <Image
                        src="https://static.mservice.io/img/momo-upload-api-220217181500-637807185006196412.jpg"
                        alt="270 TRIỆU + Thẻ quà và bao lì xì đã gửi đến người chơi"
                        width={800}
                        height={450}
                      />
                    </div>
                  </div>
                  <h3 className="font-lx text-lx-2 heading-shadow mt-2 text-center text-lg leading-snug sm:mt-4 sm:text-xl">
                    <CountUp start={0} end={270} duration={2} delay={1.5} />{" "}
                    TRIỆU +
                  </h3>

                  <div className="font-lx text-center text-gray-900 text-opacity-80 sm:mt-1  sm:text-lg sm:leading-snug">
                    Thẻ quà và bao lì xì đã gửi đến người chơi
                  </div>

                  {confetti3 && (
                    <div className="confetti absolute top-1/2 left-1/2">
                      <Confetti
                        width="400px"
                        height="400px"
                        recycle={false}
                        numberOfPieces={20}
                        initialVelocityX={5}
                        initialVelocityY={10}
                        confettiSource={{ x: 150, y: 150, w: 100, h: 100 }}
                        colors={[
                          "#EF8E99",
                          "#F4B43F",
                          "#458B8B",
                          "#a50064",
                          "#8a0000",
                          "#a10203",
                        ]}
                      />
                    </div>
                  )}
                </div>
              </Reveal>

              {/* <Reveal
                keyframes={customAnimation}
                triggerOnce
                duration={500}
                cascade={false}
                delay={1800}
                onVisibilityChange={(inView) =>
                  inView && setTimeout(() => setConfetti4(true), 1700)
                }
              >
                <div className="item relative">
                  <div className="img-mask-container">
                    <div className=" img-mask flex max-w-full overflow-hidden rounded-md border-2 border-[#B0091B] shadow-lg">
                      <Image
                        src="https://static.mservice.io/img/momo-upload-api-220217181511-637807185114984315.jpg"
                        alt=" 11 TRIỆU + Lượt người chơi
                        gửi lì xì
                        "
                        width={800}
                        height={450}
                      />
                    </div>
                  </div>

                  <h3 className="font-lx text-lx-2 heading-shadow mt-2 text-center text-lg leading-snug sm:mt-4 sm:text-xl">
                    <CountUp start={0} end={11} duration={2} delay={2} /> TRIỆU
                    +
                  </h3>

                  <div className="font-lx text-center text-gray-900 text-opacity-80 sm:mt-1  sm:text-lg sm:leading-snug">
                    Lượt người chơi gửi lì xì
                  </div>

                  {confetti4 && (
                    <div className="confetti absolute top-1/2 left-1/2">
                      <Confetti
                        width="400px"
                        height="400px"
                        recycle={false}
                        numberOfPieces={20}
                        initialVelocityX={5}
                        initialVelocityY={10}
                        confettiSource={{ x: 150, y: 150, w: 100, h: 100 }}
                        colors={[
                          "#EF8E99",
                          "#F4B43F",
                          "#458B8B",
                          "#a50064",
                          "#8a0000",
                          "#a10203",
                        ]}
                      />
                    </div>
                  )}
                </div>
              </Reveal> */}
            </div>
          </Container>
        </section>

        <section
          className="relative z-10  bg-top bg-no-repeat py-10 sm:bg-auto md:py-20 "
          style={{
            backgroundImage:
              "url(https://static.mservice.io/img/momo-upload-api-220118093534-637780953340721843.png)",
          }}
        >
          <Container>
            <Reveal keyframes={customAnimation} triggerOnce duration={500}>
              <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl">
                LẮC XÌ 2022: TẾT VUI TỚI BẾN, HÂN HOAN NỐI KẾT!
              </h2>
            </Reveal>

            <div className="mx-auto max-w-3xl">
              <Reveal
                keyframes={customAnimation}
                triggerOnce
                duration={500}
                delay={200}
              >
                <ReadMoreHTML
                  maxLine="8"
                  btnText="... Xem thêm"
                  type="html"
                  isReadMore={true}
                >
                  <div className=" soju__prose small  text-justify">
                    <p className="mb-2">
                      Một mùa Lắc Xì thành công nữa đã khép lại. MoMo xin chân
                      thành cảm ơn Quý Khách hàng đã tham gia để cùng tạo nên
                      một mùa game nô nức, náo nhiệt và tràn ngập niềm vui. Đó
                      cũng chính là mong muốn của MoMo trong mùa Lắc Xì 2022:
                      Kết nối người chơi bằng những trò chơi đặc sắc, vui nhộn,
                      mang về những giải thưởng hấp dẫn.
                    </p>
                    <p className="mb-2">
                      Thông qua cuộc hành trình thu thập những món đồ trang trí
                      quen thuộc như Bánh chưng - Bánh tét, Tranh thư pháp, Mâm
                      ngũ quả, Tượng Thần Tài, MoMo muốn hướng mọi người về
                      những giá trị tốt đẹp của ngày Tết Việt Nam. Truyền thống
                      và lộng lẫy. Sum vầy và hân hoan. MoMo hy vọng rằng,
                      chương trình đã mang tất cả mọi người xích lại gần nhau để
                      cùng tận hưởng một mùa Tết bình an, tràn ngập tiếng cười
                      như những ngày xưa cũ.
                    </p>

                    <p className="mb-2">
                      MoMo không quên gửi đến Quý Đối tác sự trân trọng và lời
                      cảm ơn vì đã đồng hành cùng chúng tôi tạo nên một mùa Lắc
                      Xì với nhiều đổi mới, sáng tạo, mang đến trải nghiệm đa
                      dạng và quà tặng phong phú cho người dùng.
                    </p>

                    <p className="mb-2">
                      Sự tin yêu của Quý Đối tác đã và luôn là động lực giúp tập
                      thể Ví MoMo hoàn thiện trong quá trình thực hiện Lắc Xì
                      2022 nói riêng và những chương trình khác của MoMo nói
                      chung. Cảm ơn vì đã luôn là Đối tác, Người đồng hành đáng
                      tin cậy của chúng tôi!
                    </p>

                    <p className="mb-2">
                      Cùng chung mục tiêu thúc đẩy nền thanh toán điện tử Việt
                      Nam, MoMo tin rằng, chúng ta sẽ tiếp tục vươn đến những
                      cột mốc mới, kiến tạo những giá trị tốt đẹp cho cộng đồng
                      và người dùng.
                    </p>

                    <p className="mb-2">
                      Cuối cùng, MoMo xin chúc mừng hơn{" "}
                      <b>100.000 người chơi</b> trong tổng số <b>12 triệu</b>{" "}
                      người chơi đã hoàn thành xuất sắc 5 Thử thách Thần Tài để
                      quy đổi Xu thành tiền thưởng.
                    </p>
                    <p className="mb-2">
                      Xuyên suốt Lắc Xì 2022, hơn 270 triệu bao lì xì tiền mặt,
                      thẻ quà thanh toán ứng dụng online, thẻ quà tặng mua sắm
                      tại siêu thị, cửa hàng tiện lợi, thời trang, đặt vé máy
                      bay, ăn uống, nạp tiền điện thoại, mua data 3G/4G, thanh
                      toán hóa đơn điện, nước, bảo hiểm,… cũng đã được gửi đến
                      người chơi.
                    </p>

                    <p className="mb-2">
                      Hy vọng rằng, những món quà nhỏ và lì xì đầu xuân này sẽ
                      là khởi đầu cùng bạn bước vào một năm mới Nhâm Dần đầy
                      hứng khởi, may mắn và gặt hái nhiều thành công!
                    </p>
                    <p className="mb-2">Hẹn gặp lại bạn vào mùa Lắc Xì 2023!</p>
                  </div>
                </ReadMoreHTML>
              </Reveal>
            </div>
          </Container>
        </section>
        <LineLX />
        {topList &&
          topList?.Result == true &&
          topList?.Data?.Items?.length > 0 && (
            <section
              className="relative z-10 pt-10 md:pt-20"
              id="thuthachthantai"
            >
              <Container>
                <Reveal keyframes={customAnimation} triggerOnce duration={500}>
                  <h2 className="font-lx text-lx-2  mb-2 text-center text-2xl md:text-3xl">
                    THỬ THÁCH THẦN TÀI
                  </h2>

                  {/* <h3 className="font-lx mb-4 text-center text-lg text-gray-700 md:mb-8 md:text-xl">
                    Có <strong>1.234.567 </strong> người chơi đã vượt qua 5 Thử
                    thách thần tài
                  </h3> */}
                </Reveal>

                <ListCustomer topList={topList} />
              </Container>
            </section>
          )}

        <section className="relative z-10 py-10 md:py-20 " id="tracuuketqua">
          <Container>
            <Reveal keyframes={customAnimation} triggerOnce duration={500}>
              <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl">
                TRA CỨU KẾT QUẢ
              </h2>
            </Reveal>

            <SearchCustomer />
          </Container>
        </section>
        {blockRules && blockRules.length > 0 && (
          <>
            <LineLX />
            <section className="relative z-10 py-10 md:py-20 " id="thele">
              <Container>
                <Reveal keyframes={customAnimation} triggerOnce duration={500}>
                  <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl">
                    THỂ LỆ
                  </h2>
                </Reveal>

                <div className=" border-lx-sm  -mx-2  max-w-3xl sm:mx-auto">
                  <div className="bg-[#ffebe9] px-2 py-1 sm:px-4">
                    {blockRules.map((item, index) => {
                      if (item.TypeName == "QuestionGroup") {
                        return (
                          <QuestionList
                            key={index}
                            template={item.Template}
                            data={item.Data.Items[0].ListQuestions}
                            isShowTitle={false}
                            isMobile={isMobile}
                          />
                        );
                      }
                    })}
                  </div>
                </div>
              </Container>
            </section>
          </>
        )}

        <LineLX />

        {tintucActive && (
          <>
            <section className="relative z-10 py-10 md:py-20 " id="tintuc">
              <Container>
                <Reveal keyframes={customAnimation} triggerOnce duration={500}>
                  <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl">
                    TIN TỨC
                  </h2>
                </Reveal>
                {dataServiceGroup
                  .filter(
                    (item) =>
                      item.TypeName == "ProjectArticles" && item.Template == 1
                  )
                  .map((item, index) => {
                    if (item.TypeName == "ProjectArticles") {
                      return (
                        <ProjectArticles2
                          key={index}
                          template={item.Template}
                          data={item.Data}
                          isMobile={isMobile}
                        />
                      );
                    }
                  })}
              </Container>
            </section>
            <LineLX />
          </>
        )}

        <section className="relative z-10 py-10 md:py-20 " id="doitac">
          <Container>
            <Reveal
              keyframes={customAnimation}
              triggerOnce
              duration={500}
              className="relative z-10"
            >
              <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl ">
                ĐỐI TÁC <br className="sm:hidden" /> ĐỒNG HÀNH
              </h2>
            </Reveal>
            <Reveal keyframes={customAnimation} triggerOnce duration={500}>
              <div className="relative">
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220108102356-637772342360141956.png"
                  loading="lazy"
                  className="pointer-events-none absolute -top-[50%] left-1/2 mx-auto block max-w-full -translate-x-1/2 select-none md:-top-[120px]"
                />

                <img
                  src="https://static.mservice.io/img/momo-upload-api-220108102750-637772344707523630.png"
                  loading="lazy"
                  className="pointer-events-none absolute -bottom-5 -left-7 z-10 mx-auto block w-24 max-w-full select-none md:-bottom-10 md:-left-16 md:w-40"
                />

                <div className="box-lx z-1 relative mb-10  rounded-2xl px-4  py-2 sm:mb-12 md:px-8 md:py-4">
                  <div className="">
                    <picture>
                      <source
                        media="(min-width: 650px)"
                        srcSet="https://static.mservice.io/img/momo-upload-api-220121110812-637783600926514503.png"
                      />
                      <img
                        src="https://static.mservice.io/img/momo-upload-api-220121110825-637783601050028164.png"
                        alt="Lắc xì MoMo"
                        className="mx-auto block max-w-full"
                        loading="lazy"
                      />
                    </picture>
                  </div>
                </div>
              </div>
            </Reveal>

            <Reveal keyframes={customAnimation} triggerOnce duration={500}>
              <div className="relative">
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220108103037-637772346372812855.png"
                  loading="lazy"
                  className="pointer-events-none absolute -bottom-10 -right-7 z-10 mx-auto block w-24 max-w-full select-none md:-bottom-10 md:-right-16 md:w-40"
                />

                <div className="box-lx mb-10 rounded-2xl px-4  py-2  sm:mb-12  md:px-8 md:py-4">
                  <div className="">
                    <picture>
                      <source
                        media="(min-width: 650px)"
                        srcSet="https://static.mservice.io/img/momo-upload-api-220121110839-637783601196996919.png"
                      />
                      <img
                        src="https://static.mservice.io/img/momo-upload-api-220121111420-637783604606076043.png"
                        alt="Lắc xì MoMo"
                        className="mx-auto block max-w-full"
                        loading="lazy"
                      />
                    </picture>
                  </div>
                </div>
              </div>
            </Reveal>
            <Reveal keyframes={customAnimation} triggerOnce duration={500}>
              <div className="relative">
                <img
                  src="https://static.mservice.io/img/momo-upload-api-220108103049-637772346493647646.png"
                  loading="lazy"
                  className="md:w-34 pointer-events-none absolute -bottom-10 -left-7 z-10 mx-auto block w-24 max-w-full select-none md:-bottom-10 md:-left-16"
                />

                <div className="box-lx mb-10 rounded-2xl px-4  py-2  sm:mb-12 md:px-8 md:py-4">
                  <div className="">
                    <picture>
                      <source
                        media="(min-width: 650px)"
                        srcSet="https://static.mservice.io/img/momo-upload-api-220121110925-637783601653153781.png"
                      />
                      <img
                        src="https://static.mservice.io/img/momo-upload-api-220121110934-637783601747328449.png"
                        alt="Lắc xì MoMo"
                        className="mx-auto block max-w-full"
                        loading="lazy"
                      />
                    </picture>
                  </div>
                </div>
              </div>
            </Reveal>
          </Container>
        </section>

        <LdJson_QA
          QaData={
            dataServiceGroup
              .filter((item) => item.TypeName == "QuestionGroup")
              .slice(1, 2)
              .map((item) => {
                return item.Data;
              })[0]
          }
        />
        {blockQuestionGroup && blockQuestionGroup.length > 0 && (
          <>
            <LineLX />
            <section
              className="relative z-10 py-10 md:py-20 "
              id="hoidap"
            ></section>
            {blockQuestionGroup.map((item, index) => {
              return (
                <Container>
                  <Reveal
                    keyframes={customAnimation}
                    triggerOnce
                    duration={500}
                  >
                    <h2 className="font-lx text-lx-2  mb-8 text-center text-2xl md:text-3xl">
                      {item.Title}
                    </h2>
                  </Reveal>

                  <QuestionList2
                    key={index}
                    template={item.Template}
                    data={item}
                    isShowTitle={false}
                    isMobile={isMobile}
                  />
                </Container>
              );
            })}
          </>
        )}

        <style jsx global>
          {`
            .page-lacxi {
              background: #feeb4f;
            }
            .navbar-app {
              background-color: transparent;
              backdrop-filter: none;
              -webkit-backdrop-filter: none;
              box-shadow: none;
              padding-top: max(env(safe-area-inset-top), 22px);
            }

            .font-lx {
              font-family: "UTMColossalis", sans-serif;
            }

            @font-face {
              font-family: "UTMColossalis";
              src: url("https://static.mservice.io/next-js/_next/static/public/lacxi/UTMColossalis.woff2")
                  format("woff2"),
                url("https://static.mservice.io/next-js/_next/static/public/lacxi/UTMColossalis.woff")
                  format("woff");
              font-weight: normal;
              font-style: normal;
              font-display: swap;
            }

            .border-lx {
              border: 30px solid orange;

              border-style: solid;
              border-width: 30px;

              border-image: url(https://static.mservice.io/fileuploads/svg/momo-file-220104173957.svg)
                15 round;
            }

            .border-lx-sm {
              border: 14px solid orange;

              border-style: solid;
              border-width: 14px;

              border-image: url(https://static.mservice.io/fileuploads/svg/momo-file-220104173957.svg)
                14 round;
            }

            .img-mask2 {
              clip-path: ellipse(radius-x, radius-y at x, y);
            }

            .text-lx {
              color: #fcf2c3;
            }
            .heading-shadow {
              text-shadow: 0px 4px 4px rgba(116, 24, 16, 0.05);
            }
            .text-lx-2 {
              color: #8a0000;
            }

            .heading-lx {
              color: #fcf2c3;
              text-shadow: 0px 2px 1px rgba(0, 0, 0, 0.4);
            }

            .heading-lx-2 {
              text-shadow: 0px 2px 1px rgba(0, 0, 0, 0.4);
            }

            .hero-container {
              filter: drop-shadow(0 5px 5px #0002);
            }

            .hero {
              -webkit-clip-path: url(#my-clip-path);
              clip-path: url(#my-clip-path);
            }

            @media (max-width: 769px) {
              .hero {
                clip-path: view-box;
                clip-path: none;
              }
            }

            .bg-hero {
              background-color: #aa0667;
              background-image: url("https://static.mservice.io/img/momo-upload-api-220106135055-637770738555292214.jpg");
            }

            @media (max-width: 767.98px) {
              .bg-hero {
                background-image: url("https://static.mservice.io/img/momo-upload-api-220108152814-637772524941951679.jpg");
              }
            }

            .img-mask2 {
              -webkit-clip-path: url(#my-clip-path3);
              clip-path: url(#my-clip-path3);
            }
            .img-mask-container2 {
              position: relative;
              filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.15));
            }

            .img-mask-container2:after {
              content: "";
              position: absolute;
              -webkit-clip-path: url(#my-clip-path3);
              clip-path: url(#my-clip-path3);
              left: -4px;
              right: -4px;
              top: -4px;
              bottom: -4px;
              z-index: -1;
              background-color: #8a0000;
            }
            .box-lx {
              background: #ffe0d3;
              box-shadow: inset 0px 1px 3px rgba(226, 146, 133, 0.4);
            }

            .is-current a {
              color: rgb(252, 211, 77);
            }
            .is-current-mobile a {
              color: #ecd79b;
            }

            .bg-grad-1 {
              background: linear-gradient(
                86.8deg,
                #e24078 11.24%,
                #ff4640 94.95%
              );
              box-shadow: 1px 2px 0px #a7002b;
            }

            .confetti canvas {
              transform: translate(-50%, -50%);
            }
          `}
        </style>
      </div>
      {ctaConfig && (
        <FooterCta
          data={[
            {
              TypeName: "CtaFooter",
              Cta: ctaConfig,
            },
          ]}
          isMobile={isMobile}
          isViewApp={isViewApp}
        />
      )}
    </Page>
  );
}

export async function getStaticProps() {
  const dataService = await axios.get(
    `/service-page/detail-by-slug?slug=lixi`,
    null,
    true
  );
  const dataServiceBlocks = await axios.get(
    `/service-page/listBlocks?pageId=${dataService.Data.Id}`,
    null,
    true
  );

  const topList = await axios.get(
    `/lixi/2022/list-player?count=100`,
    null,
    true
  );

  // const dataServiceBlocks = await axios.get(
  //   `/service-page/listBlocks?pageId=13`,
  //   null,
  //   true
  // );

  return {
    props: {
      dataService,
      dataServiceBlocks,
      topList,
    },
    revalidate: 60 * 1,
  };
}

export default Lacxi;
