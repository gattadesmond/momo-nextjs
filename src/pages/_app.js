import App from "next/app";
import { axios } from "@/lib/index";


import NProgress from "../components/NProgress";
import ViewportHeight from "../components/ViewportHeight";

import baseStyles from "../styles/base";
import "@/styles/globals.scss";


import { Provider } from 'react-redux'

import { store } from '../store'

import isMobile from "@/lib/isMobile";
import { AppProvider } from "@/lib/appContext";


// export function reportWebVitals(metric) {
// }


function MyApp({ Component, pageProps, pageMaster }) {
  
  const isMobileDevice = isMobile();

  return (
    <Provider store={store}>
      <AppProvider isMobile={isMobileDevice}>
        <Component
          {...pageProps}
          pageMaster={pageMaster}
          isMobile={isMobileDevice}
        />
      </AppProvider>

      <NProgress />
      <ViewportHeight />

      <style jsx global>
        {baseStyles}
      </style>
    </Provider>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const pageMaster = await axios.get(`/common/master`, null, true);
  
  const appProps = await App.getInitialProps(appContext);

  return { ...appProps, pageMaster };
};

export default MyApp;
