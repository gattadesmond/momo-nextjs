import "@/styles/globals.scss";

import NProgress from "../components/NProgress";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Component {...pageProps} />
      <NProgress />
    </>
  );
}

export default MyApp;
