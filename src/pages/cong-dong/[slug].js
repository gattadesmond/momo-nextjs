import { useState, useEffect } from "react";
import Image from "next/image";
import { appSettings } from "@/configs";
import dynamic from "next/dynamic";

const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import parse, { domToReact } from "html-react-parser";
import { decode } from "html-entities";
import StickyBox from "react-sticky-box";

import { axios } from "@/lib/index";
import { getRootSlug } from "@/lib/url";

import Page from "@/components/page";
import Container from "@/components/Container";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import DonationMenu from "@/components/donation/DonationMenu";
import ButtonShare from "@/components/ui/ButtonShare";
import DonationDetail from "@/components/donation/DonationDetail";
import ListDonation from "@/components/donation/ListDonation";
import DonationItem from "@/components/donation/DonationItem";

import ListTransRank from "@/components/donation/ListTransRank";
import ListTrans from "@/components/donation/ListTrans";
import StickyMenuDetail from "@/components/donation/StickyMenuDetail";
import LdJson_QA from "@/components/LdJson_QA";
import FooterCta from "@/components/FooterCta";

import useBrowserLayoutEffect from "@/lib/useBrowserLayoutEffect";
import ArticleListAjax from "@/components/ui/ArticleListAjax";
import ArticleListByCateAjax from "@/components/ui/ArticleListByCateAjax";

const AlbumImage = ({ images }) => {
  if (!images || images.length === 0) return <></>;

  const [isGallery, setIsGallery] = useState(false);

  let imgList = [];
  let lightGallery = null;

  images.forEach((img, index) => {
    imgList.push({
      src: img.AvatarUrl,
      subHtml: `<div>${img.Description ? img.Description : ""}</div>`,
      downloadUrl: false,
    });
  });

  useEffect(() => {
    const timer = setTimeout(function () {
      setIsGallery(true);
    }, 200);

    return function clean() {
      clearTimeout(timer);
      setIsGallery(false);
    };
  }, [images]);

  const onInitGallery = (e) => {
    lightGallery = e.instance;
  };

  function openLightboxOnSlide(number = 0) {
    const act = number - 1 < 0 ? 0 : number;
    if (lightGallery) {
      lightGallery.openGallery(act);
    }
  }

  return (
    <div className="relative overflow-hidden md:mt-5 md:rounded-lg">
      <div className="grid grid-cols-1 gap-2 md:grid-cols-3">
        {images
          // .sort((a, b) => a.DisplayOrder - b.DisplayOrder)
          .map((img, index) => (
            <div
              key={`album-${img.Id}`}
              className={`aspect-w-4 aspect-h-3 relative cursor-pointer bg-gray-200 
                ${
                  index === 0
                    ? ""
                    : index > 0 && index < 3
                    ? "hidden md:block"
                    : "hidden"
                }
              `}
              onClick={() => {
                openLightboxOnSlide(index);
              }}
            >
              <Image
                className="transition duration-500 hover:scale-105"
                alt={img.Name}
                src={img.AvatarUrl}
                loading={index === 0 ? "eager" : "lazy"}
                layout="fill"
                objectFit="cover"
                priority={index === 0 ? true : false}
              />
            </div>
          ))}
      </div>
      {isGallery && (
        <LightGallery
          speed={500}
          onInit={onInitGallery}
          dynamic={true}
          dynamicEl={imgList}
          downloadUrl={false}
          // plugins={[lgThumbnail]}
        ></LightGallery>
      )}
      <button
        type="button"
        className="absolute bottom-3 right-3 flex rounded border border-gray-700 bg-white py-2 px-3 text-xs"
        onClick={() => openLightboxOnSlide()}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 576 512"
          className=" mr-2 w-5"
        >
          <path d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v208c0 44.112 35.888 80 80 80h336zm96-80V80c0-26.51-21.49-48-48-48H144c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h384c26.51 0 48-21.49 48-48zM256 128c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-96 144l55.515-55.515c4.686-4.686 12.284-4.686 16.971 0L272 256l135.515-135.515c4.686-4.686 12.284-4.686 16.971 0L512 208v112H160v-48z" />
        </svg>
        Xem {images.length} hình
      </button>
    </div>
  );
};

const isEnableDonation = (finishPercent, isEndCampaign, isContDonate) => {
  if (finishPercent >= 100) {
    if (isEndCampaign && isContDonate) {
      return true;
    } else {
      return false;
    }
  } else if (finishPercent < 100 && isEndCampaign) {
    return false;
  } else {
    return true;
  }
};

export default function DonationDetailPage({
  dataDetail,
  listDonationOther,
  listTrans,
  listTransRank,
  dataArticle,
  pageMaster,
  isMobile,
}) {
  const metaData = dataDetail.Data.Meta;
  const menuData = dataDetail.Data.DataJson;
  const data = dataDetail.Data;

  const [enableDonation, setEnableDonation] = useState(false);
  const menuDefault = [
    { Id: "hoancanh", Name: "Hoàn cảnh" },
    { Id: "cauchuyen", Name: "Câu chuyện" },
    { Id: "nhahaotam", Name: "Nhà hảo tâm" },
    { Id: "chuongtrinhkhac", Name: "Chương trình khác" },
    { Id: "tintuc", Name: "Tin tức cộng đồng" },
  ];
  const [menuSticky, setMenuSticky] = useState(menuDefault);

  useEffect(() => {
    document.querySelector("body").classList.add("page-donation", "fix-header");
    return function clean() {
      document
        .querySelector("body")
        .classList.remove("page-donation", "fix-header");
    };
  }, []);

  useBrowserLayoutEffect(() => {
    let menu = menuDefault;
    if (!isShowMenuNhaHaoTam()) {
      menu = menu.filter((x) => x.Id !== "nhahaotam");
    }
    if (!dataArticle.IsShowArticle) {
      menu = menu.filter((x) => x.Id !== "tintuc");
    }
    if (!enableDonation) {
      menu = menu.filter((x) => x.Id !== "chuongtrinhkhac");
    }
    setMenuSticky(menu);

    var isEnabledDonation = isEnableDonation(
      data.FinishPercent,
      data.IsEndCampaign,
      data.IsContDonate
    );
    setEnableDonation(isEnabledDonation);
  }, [dataDetail.Data]);

  const isShowMenuNhaHaoTam = () => {
    if (listTrans?.Data?.Items?.length > 0) return true;
    if (listTransRank?.Data?.Items?.length > 0) return true;

    return false;
  };

  const footerCtaData = [
    {
      TypeName: "CtaFooter",
      Cta: {
        QrCodeId: data.CtaQrCodeId,
        NewTab: data.CtaNewTab,
        Link: data.CtaLink,
        Text: "Quyên góp",
      },
    },
  ];

  return (
    <Page
      className="page-article"
      title={metaData?.Title}
      description={metaData?.Description}
      image={metaData?.Avatar}
      keywords={metaData?.Keywords}
      robots={metaData?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      <DonationMenu
        title={menuData.ServicePageName}
        icon={menuData.ServicePageLogo}
        url={menuData.ServicePageUrlRewrite}
      />
      <Container>
        <div className="flex flex-col" id="hoancanh">
          <div className="order-2 md:order-1">
            <div className="max-w-5xl">
              <h1
                className={`mb-3 pt-4 text-2xl font-bold tracking-tight  text-gray-700 md:pt-6  md:text-2xl md:leading-snug  lg:text-3xl`}
              >
                {data.Title}
              </h1>
              <p className="leading-normal text-gray-500  md:text-lg">
                <img
                  className="mr-1 inline-block max-w-full"
                  alt="heart"
                  src="https://momo.vn/momo2020/img/donation/icon-heart.png"
                  loading="lazy"
                  width={23.3}
                  height={20}
                />
                {data.Short}
              </p>
            </div>
            <div className="mt-2 flex items-center">
              <div className="flex flex-1 flex-col flex-wrap text-sm text-gray-500 md:flex-row md:items-center">
                <span className="relative">{data.PublicDate}</span>
              </div>
              <div className=" shrink-0">
                <ButtonShare
                  link={`${appSettings.AppConfig.FRONT_END}${getRootSlug(
                    data.Link
                  )}`}
                  isViewApp={false}
                />
              </div>
            </div>
          </div>
          <div className="order-1 -mx-5 pb-2 md:order-2 md:mx-0">
            <AlbumImage images={data.Album} />
          </div>
        </div>
      </Container>
      <StickyMenuDetail menuSticky={menuSticky} />
      <Container>
        <div className="mx-auto mt-5 w-full lg:mt-0">
          <div className="lg:grid lg:grid-flow-col lg:grid-cols-5 lg:gap-6">
            <div className="mx-auto w-full lg:col-span-3 lg:pr-4">
              <div className="mb-5 block rounded-lg border-gray-200 md:border md:p-6 md:shadow-lg lg:hidden">
                <h2 className="text-1.5xl mb-5 hidden flex-auto font-semibold text-gray-700 md:block">
                  Thông tin quyên góp
                </h2>
                <DonationDetail
                  data={data}
                  isShowButtonDonation={enableDonation}
                />
              </div>
              <div id="cauchuyen">
                <h2 className="text-1.5xl py-5 font-semibold">Câu chuyện</h2>
                <article className="soju__prose small">
                  {parse(decode(data.Content))}
                </article>
              </div>
              {menuSticky.findIndex((x) => x.Id === "nhahaotam") > -1 && (
                <div id="nhahaotam">
                  {listTrans.Data && listTrans.Data.Items.length > 0 && (
                    <div className="mt-3 mb-5">
                      <h2 className="text-1.5xl my-5 border-t border-gray-200 pt-4 font-semibold">
                        Nhà hảo tâm mới nhất
                      </h2>
                      <ListTrans
                        donationId={data.Id}
                        donationType={data.Type}
                        data={listTrans.Data}
                      />
                    </div>
                  )}
                  {listTransRank.Data && listTransRank.Data.Items.length > 0 && (
                    <div className="mt-3 border-t border-gray-200">
                      <h2 className="text-1.5xl my-5 font-semibold">
                        Nhà hảo tâm hàng đầu
                      </h2>
                      <ListTransRank
                        data={listTransRank.Data}
                        donationType={data.Type}
                      />
                    </div>
                  )}
                </div>
              )}
            </div>
            <div className="relative z-10 hidden w-full lg:col-span-2 lg:block">
              <StickyBox offsetTop={60} offsetBottom={10}>
                <div className="mt-3 rounded-lg bg-white pt-5 lg:mt-0 lg:pl-6">
                  <div className="mb-6 rounded-lg border py-6 px-8 shadow-lg">
                    <h2 className="text-1.5xl mb-5 flex-auto font-semibold text-gray-700">
                      Thông tin quyên góp
                    </h2>
                    <DonationDetail
                      data={data}
                      isShowButtonDonation={enableDonation}
                    />
                  </div>
                  {data.ListOnGoing.length > 0 && (
                    <div>
                      <h2 className="text-1.5xl mb-4 mt-10 font-semibold">
                        Chương trình đang diễn ra
                      </h2>
                      {data.ListOnGoing.map((item, index) => (
                        <DonationItem key={index} data={item} />
                      ))}
                    </div>
                  )}
                </div>
              </StickyBox>
            </div>
          </div>
        </div>
        {enableDonation && (
          <div id="chuongtrinhkhac">
            <div className="mb-4 text-left md:mb-4">
              <h2 className="text-1.5xl pt-10 font-semibold">
                Các chương trình quyên góp khác
              </h2>
            </div>
            {listDonationOther.Data && (
              <ListDonation data={listDonationOther.Data} type={1} />
            )}
          </div>
        )}
        {dataArticle.IsShowArticle && (
          <div id="tintuc">
            <div className="mb-4 text-left md:mb-4">
              <h2 className="text-1.5xl pt-10 font-semibold">
                Tin tức cộng đồng
              </h2>
            </div>
            {enableDonation ? (
              <ArticleListAjax
                item={dataArticle.Data}
                isMobile={isMobile}
                isNumber={dataArticle.ItemsCountPerGroup}
              />
            ) : (
              <ArticleListByCateAjax
                item={dataArticle.Data}
                isMobile={isMobile}
                isNumber={dataArticle.ItemsCountPerGroup}
              />
            )}
          </div>
        )}
        <div className="pb-10" />
      </Container>
      {enableDonation && isMobile && (
        <FooterCta data={footerCtaData} isMobile={isMobile} />
      )}
    </Page>
  );
}

export async function getServerSideProps(context) {
  const urlDetail = context.query.slug;

  const dataDetail = await axios.get(
    `/donation/detail?urlRewrite=${urlDetail}`,
    null,
    true
  );
  if (!dataDetail || !dataDetail.Result) {
    return {
      notFound: true,
    };
  }
  const donationId = dataDetail.Data.Id;
  const listGoingDonationId = dataDetail.Data.ListOnGoing.map((x) => x.Id).join(
    ","
  );
  const listDonationOther = await axios.get(
    `/donation/list?type=${dataDetail.Data.Type}&lastIdx=0&count=3&sortType=1&sortDir=2&excludeIds=${donationId},${listGoingDonationId}`,
    null,
    true
  );
  const listTrans = await axios.get(
    `/donation/listTrans?donationId=${dataDetail.Data.Id}&lastIdx=0&count=10`,
    null,
    true
  );
  const listTransRank = await axios.get(
    `/donation/listTransRank?donationId=${dataDetail.Data.Id}&lastIdx=0&count=10`,
    null,
    true
  );

  const getProjectId = (value) => {
    const array = value.split(",").filter((x) => {
      const value = parseInt(x);
      if (!value) return false;
      if ([14, 15].includes(value)) return false;

      return true;
    });
    return array.join(",");
  };

  const isFinishDonation = !isEnableDonation(
    dataDetail.Data.FinishPercent,
    dataDetail.Data.IsEndCampaign,
    dataDetail.Data.IsContDonate
  );
  const projectId = getProjectId(dataDetail.Data.ProjectIds);
  const count = 4;
  const sortType = 1;
  const sortDir = 2;
  const cateId = 118;
  let listArticle = null;
  if (!isFinishDonation) {
    listArticle = await axios.get(
      `/article/listByProject?projectIds=${projectId}&sortType=${sortType}&sortDir=${sortDir}&count=${count}&lastIdx=0`,
      null,
      true
    );
  } else {
    listArticle = await axios.get(
      `/article/list?cateId=${cateId}&sortType=${sortType}&sortDir=${sortDir}&count=${count}&lastIdx=0`,
      null,
      true
    );
  }
  let dataArticle = {
    IsShowArticle: false,
    ItemsCountPerGroup: count,
    Data: {
      Count: count,
      CtaLink: null,
      CtaNewTab: false,
      CtaQrCodeId: null,
      CtaText: null,
      DisplayOrder: 1,
      SortBy: sortType,
      SortDir: sortDir,
      Status: 1,
      Title: "All",
      ProjectIds: "",
      CateId: cateId,
      LastIdx: -1,
      TotalItems: -1,
      Items: [],
    },
    ShowGroupAll: true,
  };
  if (listArticle) {
    if (listArticle.Success) {
      dataArticle.Data.ProjectIds = (projectId || "")
        .split(",")
        .filter((x) => parseInt(x));
      dataArticle.Data.LastIdx = listArticle.Data.LastIndex;
      dataArticle.Data.TotalItems = listArticle.Data.TotalItems;
      dataArticle.Data.Items = listArticle.Data.Items;
      dataArticle.IsShowArticle =
        listArticle.Data.Items.length > 0 ? true : false;
    } else if (listArticle.Result) {
      dataArticle.Data.ProjectIds = "";
      dataArticle.Data.LastIdx = listArticle.Data.LastIdx;
      dataArticle.Data.TotalItems = listArticle.Data.TotalItems;
      dataArticle.Data.Items = listArticle.Data.Items;
      dataArticle.IsShowArticle =
        listArticle.Data.Items.length > 0 ? true : false;
    } else {
      dataArticle = null;
    }
  } else {
    dataArticle = null;
  }

  return {
    props: {
      dataDetail,
      listDonationOther,
      listTrans,
      listTransRank,
      dataArticle,
    }, // will be passed to the page component as props
  };
}
