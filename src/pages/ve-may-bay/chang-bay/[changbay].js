import { useEffect } from "react";
import dynamic from "next/dynamic";

import { axios } from "@/lib/index";
import ReplaceURL from "@/lib/replace-url";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import OtaMenu from "@/components/ota/OtaMenu";

// const BlockSearchNew = dynamic(() => import("@/components/ota/BlockSearchNew"));

const BlockSearch = dynamic(() => import("@/components/ota/BlockSearch"));

const Heading = dynamic(() => import("@/components/Heading"));
const SectionSpace = dynamic(() => import("@/components/SectionSpace"));
const SectionGroup = dynamic(() => import("@/components/section/SectionGroup"));
const RouterInfo = dynamic(() => import("@/components/ota/RouterInfo"));
const PublicRouterList = dynamic(() =>
  import("@/components/ota/PublicRouterList")
);
const HtmlTitle = dynamic(() => import("@/components/section/HtmlTitle"));
const AboutH1 = dynamic(() => import("@/components/section/AboutH1"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

function VeMayBayChangBay({
  categoryObj,
  dataHomeDetail,
  dataHomeListBlock,
  dataLocation,
  dataListLocation,
  pageMaster,
  isMobile,
}) {
  const metaData = dataHomeDetail.Data;
  const locationData = dataLocation.Data;

  const bannerBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "Banner")[0] || null;
  const aboutBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "AboutH1")[0] || null;
  const ctaFooter = dataHomeListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataHomeListBlock.Data.filter(
    (x) => !["Banner", "AboutH1", "CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const checkCtaFooter = () => {
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };

  const replaceLocationName = (text = "") =>
    text
      .replace("{0}", locationData.Departure.JsCv)
      .replace("{1}", locationData.Destination.JsCv);

  //replace location Name
  metaData.Meta.Title = replaceLocationName(metaData.Meta.Title);
  metaData.Meta.Description = replaceLocationName(metaData.Meta.Description);

  //replace Hồ Chí Minh => Sài Gòn
  metaData.Meta.Title = metaData.Meta.Title.replace("Hồ Chí Minh", "Sài Gòn");
  metaData.Meta.Description = metaData.Meta.Description.replace(
    "Hồ Chí Minh",
    "Sài Gòn"
  );

  aboutBlock.Data.Title = replaceLocationName(aboutBlock.Data.Title);

  const thongTinChangBay = `Thông tin chặng bay từ ${locationData.Departure.JsCv} đến ${locationData.Destination.JsCv}`;

  // Location Matching
  const matching = {
    Title: "",
    Content: "",
    Template: "",
  };
  if (locationData.Matching?.Content) {
    const parseContent = locationData.Matching.Content.split(">");
    const h2Index = parseContent.findIndex((x) => x.includes("h2"));
    matching.Title = parseContent[h2Index + 1].split("<")[0] || "";

    const beginArticle = parseContent.findIndex((x) => x.includes("<article"));
    const endArticle = parseContent.findIndex((x) => x.includes("</article"));
    matching.Content = [
      "<article",
      ...parseContent.filter((x, i) => i > beginArticle && i < endArticle),
      "</article",
    ].join(">");
  } else {
    matching.Title = `Dễ dàng đặt vé máy bay ${locationData.Departure.JsCv} đi ${locationData.Destination.JsCv} trên Ví MoMo`;
    matching.Content = `
      <article>
          <p>
              Chuyến bay từ @Model.Departure.JsCv đến @Model.Destination.JsCv, cất cánh khỏi @Model.Departure.JsCv tại sân bay @Model.Departure.JsNv và hạ cánh tới @Model.Destination.JsCv tại sân bay @Model.Destination.JsNv. Để tìm kiếm vé máy bay @Model.Departure.JsCv đi @Model.Destination.JsCv giá rẻ nhất theo ngày, bạn chỉ cần mở Ví MoMo, đặt vé với tính năng “Du lịch - Đi lại” - mục “Vé máy bay” trên Ví MoMo.
          </p>
          <p>
              Ứng dụng MoMo liên tục cập nhật thông tin hành trình bay, giá tốt và ưu đãi với các hãng hàng không uy tín như Vietnam Airlines, VietJet Air, Bamboo Airways, Pacific Airlines… Vì vậy, khi đặt vé máy bay đi @Model.Departure.JsCv trên Ví MoMo, bạn còn có cơ hội nhận thêm các ưu đãi độc quyền đầy bất ngờ. Việc quản lý hành trình bay của bạn cũng đơn giản hơn khi thông tin vé máy bay @Model.Departure.JsCv đi @Model.Destination.JsCv được lưu ngay trong ứng dụng.
          </p>
          <p>
              Không chỉ có vé đi @Model.Destination.JsCv, MoMo còn có vô vàn lựa chọn chuyến bay từ @Model.Departure.JsCv, cất cánh khám phá nhiều vùng đất khác.
          </p>
          <p>
              Cùng MoMo tận hưởng chuyến bay @Model.Departure.JsCv - @Model.Destination.JsCv giá tốt nhất nhé!
          </p>
      </article>
    `;
    matching.Content = matching.Content.split("@Model.Departure.JsCv").join(
      locationData.Departure.JsCv
    );
    matching.Content = matching.Content.split("@Model.Destination.JsCv").join(
      locationData.Destination.JsCv
    );
    matching.Content = matching.Content.split("@Model.Departure.JsNv").join(
      locationData.Departure.JsNv
    );
    matching.Content = matching.Content.split("@Model.Destination.JsNv").join(
      locationData.Destination.JsNv
    );
  }

  // list DepartureListPopularFlights
  const sectionDeparture = {
    Title: `Chặng bay phổ biến từ ${locationData.Departure.JsCv}`,
    listFlight: [...locationData.DepartureListPopularFlights],
  };
  // list DestinationListPopularFlights
  const sectionDestination = {
    Title: `Chặng bay phổ biến từ ${locationData.Destination.JsCv}`,
    listFlight: [...locationData.DestinationListPopularFlights],
  };

  useEffect(() => {
    ReplaceURL(window.location.href.toLowerCase());

    document.querySelector("body").classList.add("page-ota");
    return function clean() {
      document.querySelector("body").classList.remove("page-ota");
    };
  }, []);

  return (
    <Page
      className=""
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      <OtaMenu
        data={categoryObj}
        pageId={metaData.ParentId}
        isMobile={isMobile}
        pageName={`${locationData.Departure.JsCv} đi ${locationData.Destination.JsCv}`}
      />
      {/* {bannerBlock && (
        <BlockSearchNew
          data={bannerBlock}
          dataListLocation2={dataListLocation}
          isMobile={isMobile}
          codeFrom={locationData.Departure.JsId}
          codeTo={locationData.Destination.JsId}
        />
      )} */}

      {bannerBlock && (
        <BlockSearch
          data={bannerBlock}
          dataListLocation={dataListLocation.Data?.Items}
          isMobile={isMobile}
          codeFrom={locationData.Departure.JsId}
          codeTo={locationData.Destination.JsId}
        />
      )}

      {aboutBlock && (
        <SectionSpace type={2}>
          <AboutH1
            template={aboutBlock.Template}
            data={aboutBlock.Data}
            isMobile={isMobile}
          />
        </SectionSpace>
      )}
      {/* Thông tin điểm đi, điểm đến */}
      <SectionSpace type={3}>
        <RouterInfo title={thongTinChangBay} dataLocation={locationData} />
      </SectionSpace>
      {/* Long Content */}
      <SectionSpace type={1}>
        <div className="mb-5 text-center md:mb-8" id="section-html">
          <Heading title={matching.Title} color="pink" />
        </div>
        <HtmlTitle
          template={matching.Template}
          data={matching.Content}
          isMobile={isMobile}
        />
      </SectionSpace>
      {/* Ưu đãi */}
      <SectionGroup
        data={listBlock.filter((x) => x.TypeName === "ProjectArticles")}
        startType={3}
        isMobile={isMobile}
      />
      {/* Chặng bay phổ biến từ điểm đi */}
      <SectionSpace type={1}>
        <PublicRouterList data={sectionDeparture} />
      </SectionSpace>
      {/* Chặng bay phổ biến từ điểm đến */}
      <SectionSpace type={2}>
        <PublicRouterList data={sectionDestination} />
      </SectionSpace>
      <SectionGroup
        data={listBlock.filter((x) => x.DisplayOrder >= 9)}
        // startType={true}
        isMobile={isMobile}
      />
      {checkCtaFooter() && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
}
export async function getServerSideProps(context) {
  const changBay = context.query.changbay;
  if (!changBay.includes(".")) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }
  const airports = changBay.split(".");
  if (airports.length != 3) {
    return {
      notFound: true,
    };
  }
  const jsIdDept = airports[1] || null;
  const jsIdDest = airports[2] || null;
  if (!jsIdDept || !jsIdDest) {
    return {
      notFound: true,
    };
  }
  const dataLocation = await axios.get(
    `/ota-location/getInfoRoute?jsIdDept=${jsIdDept}&jsIdDest=${jsIdDest}`,
    null,
    true
  );
  if (!dataLocation || !dataLocation.Result) {
    return {
      notFound: true,
    };
  }

  const otaCommon = await axios.get(`/common/ota`, null, true);
  if (!otaCommon || !otaCommon.Result || !otaCommon.Data.ServicePageIds) {
    return {
      notFound: true,
    };
  }
  const pageId = otaCommon.Data.ServicePageIds["Route"] || null;
  if (!pageId) {
    return {
      notFound: true,
    };
  }

  const dataHomeDetail = await axios.get(
    `/service-page/detail?id=${pageId}`,
    null,
    true
  );
  if (!dataHomeDetail || !dataHomeDetail.Result) {
    return {
      notFound: true,
    };
  }
  const pageVeMayBay = otaCommon.Data.ServicePageIds["Home"];
  const pageDuLichDiLai = otaCommon.Data.ServicePageIds["Travel"];

  const categoryObj = await axios.get(
    `/service-page/listBreadcrumbs?id=${pageVeMayBay}&parentId=${pageDuLichDiLai}`,
    null,
    true
  );
  const dataHomeListBlock = await axios.get(
    `/service-page/listBlocks?pageId=${pageId}`,
    null,
    true
  );
  const dataListLocation = await axios.get(
    "/ota-location/loadMore?count=9999&lastIndex=0&status=1",
    null,
    true
  );

  return {
    props: {
      categoryObj,
      dataHomeDetail,
      dataHomeListBlock,
      dataLocation,
      dataListLocation,
    }, // will be passed to the page component as props
  };
}

export default VeMayBayChangBay;
