import { useEffect } from "react";
import dynamic from "next/dynamic";

import { axios } from "@/lib/index";
import ReplaceURL from "@/lib/replace-url";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import OtaMenu from "@/components/ota/OtaMenu";

// const BlockSearchNew = dynamic(() => import("@/components/ota/BlockSearchNew"));

const BlockSearch = dynamic(() => import("@/components/ota/BlockSearch"));

const LocationInfo = dynamic(() => import("@/components/ota/LocationInfo"));
const SectionSpace = dynamic(() => import("@/components/SectionSpace"));
const SectionGroup = dynamic(() => import("@/components/section/SectionGroup"));
const AboutH1 = dynamic(() => import("@/components/section/AboutH1"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

function VeMayBayDiemDen({
  categoryObj,
  dataHomeDetail,
  dataHomeListBlock,
  dataLocation,
  dataListLocation,
  pageMaster,
  isMobile,
}) {
  const metaData = dataHomeDetail.Data;
  const locationData = dataLocation.Data;

  const bannerBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "Banner")[0] || null;
  const aboutBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "AboutH1")[0] || null;
  const ctaFooter = dataHomeListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataHomeListBlock.Data.filter(
    (x) => !["Banner", "AboutH1", "CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const checkCtaFooter = () => {
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };
  const replaceLocationName = (text = "") =>
    text.replace("{0}", locationData?.JsCv);

  //replace location
  metaData.Meta.Title = replaceLocationName(metaData.Meta.Title);
  metaData.Meta.Description = replaceLocationName(metaData.Meta.Description);

  //replace Hồ Chí Minh => Sài Gòn
  metaData.Meta.Title = metaData.Meta.Title.replace("Hồ Chí Minh", "Sài Gòn");
  metaData.Meta.Description = metaData.Meta.Description.replace(
    "Hồ Chí Minh",
    "Sài Gòn"
  );

  aboutBlock.Data.Title = replaceLocationName(aboutBlock.Data.Title);

  const thongTinDiemDen = `Thông tin điểm đến ${locationData?.JsCv}`;

  useEffect(() => {
    ReplaceURL(window.location.href.toLowerCase());

    document.querySelector("body").classList.add("page-ota");
    return function clean() {
      document.querySelector("body").classList.remove("page-ota");
    };
  }, []);

  return (
    <Page
      className="page-ota"
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      <OtaMenu
        data={categoryObj}
        pageId={metaData.ParentId}
        isMobile={isMobile}
        pageName={locationData?.JsCv}
      />
      {/* {bannerBlock &&
        <BlockSearchNew
          data={bannerBlock}
          dataListLocation2={dataListLocation}
          isMobile={isMobile}
          codeTo={locationData?.JsId}
        />
      } */}

      {bannerBlock && (
        <BlockSearch
          data={bannerBlock}
          dataListLocation={dataListLocation.Data?.Items}
          isMobile={isMobile}
          codeTo={locationData?.JsId}
        />
      )}
      {aboutBlock && (
        <SectionSpace type={2}>
          <AboutH1
            template={aboutBlock.Template}
            data={aboutBlock.Data}
            isMobile={isMobile}
          />
        </SectionSpace>
      )}

      <SectionSpace type={3}>
        <LocationInfo title={thongTinDiemDen} dataLocation={locationData} />
      </SectionSpace>
      {/* Ưu đãi */}
      <SectionGroup data={listBlock} startType={true} isMobile={isMobile} />
      {checkCtaFooter() && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
}
export async function getServerSideProps(context) {
  const diemden = context.query.diemden;
  if (!diemden.includes(".")) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }
  const codeLocation = diemden.split(".")[1];
  if (!codeLocation) {
    return {
      notFound: true,
    };
  }

  const dataLocation = await axios.get(
    `/ota-location/detail?jsId=${codeLocation}`,
    null,
    true
  );
  if (!dataLocation || !dataLocation.Result) {
    return {
      notFound: true,
    };
  }

  const otaCommon = await axios.get(`/common/ota`, null, true);
  if (!otaCommon || !otaCommon.Result || !otaCommon.Data.ServicePageIds) {
    return {
      notFound: true,
    };
  }
  const pageId = otaCommon.Data.ServicePageIds["Location"];
  const pageVeMayBay = otaCommon.Data.ServicePageIds["Home"];
  const pageDuLichDiLai = otaCommon.Data.ServicePageIds["Travel"];

  const dataHomeDetail = await axios.get(
    `/service-page/detail?id=${pageId}`,
    null,
    true
  );
  if (!dataHomeDetail || !dataHomeDetail.Result) {
    return {
      notFound: true,
    };
  }

  const categoryObj = await axios.get(
    `/service-page/listBreadcrumbs?id=${pageVeMayBay}&parentId=${pageDuLichDiLai}`,
    null,
    true
  );
  const dataHomeListBlock = await axios.get(
    `/service-page/listBlocks?pageId=${pageId}`,
    null,
    true
  );

  const dataListLocation = await axios.get(
    "/ota-location/loadMore?count=9999&lastIndex=0&status=1",
    null,
    true
  );

  return {
    props: {
      categoryObj,
      dataHomeDetail,
      dataHomeListBlock,
      dataLocation,
      dataListLocation,
    }, // will be passed to the page component as props
  };
}

export default VeMayBayDiemDen;
