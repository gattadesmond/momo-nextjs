import { useEffect } from "react";
import dynamic from "next/dynamic";

import serviceApi from "@/lib/api/service-api";
import otaApi from "@/lib/api/ota-api";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import BlockCheapFlight from "@/components/ota/BlockCheapFlight";

const OtaMenu = dynamic(() => import("@/components/ota/OtaMenu"));


// const BlockSearchNew = dynamic(() => import("@/components/ota/BlockSearchNew"));

const BlockSearch = dynamic(() => import("@/components/ota/BlockSearch"));

const SectionGroup = dynamic(() => import("@/components/section/SectionGroup"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

function VeMayBayHome({
  categoryObj,
  dataHomeDetail,
  dataHomeListBlock,
  dataListLocation,
  dataCheapFlights,
  pageMaster,
  isMobile,
}) {
  const metaData = dataHomeDetail.Data;

  const bannerBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "Banner")[0] || null;
  const ctaFooter = dataHomeListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataHomeListBlock.Data.filter(
    (x) => !["Banner", "CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const checkCtaFooter = () => {
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };

  useEffect(() => {
    document.querySelector("body").classList.add("page-ota");
    return function clean() {
      document.querySelector("body").classList.remove("page-ota");
    };
  }, []);

  return (
    <Page
      className=""
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      <OtaMenu
        isStickyMobile={!isMenuSticky}
        data={categoryObj}
        pageId={metaData.Id}
        isMobile={isMobile}
      />
      {/* {bannerBlock && (
        <BlockSearchNew
          data={bannerBlock}
          dataListLocation2={dataListLocation}
          isMobile={isMobile}
        />
      )} */}

      {bannerBlock && (
        <BlockSearch
          data={bannerBlock}
          dataListLocation={dataListLocation.Data?.Items}
          isMobile={isMobile}
        />
      )}
      <SectionGroup
        data={listBlock.filter((x, i) => i < 2)}
        isMobile={isMobile}
      />
      {/* Giá vé rẻ */}
      <BlockCheapFlight
        data={dataCheapFlights.Data}
        listLocation={dataListLocation.Data?.Items}
      />
      <SectionGroup
        data={listBlock.filter((x, i) => i >= 2)}
        isMobile={isMobile}
      />
      {checkCtaFooter() && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
}
export async function getStaticProps(context) {
  const servicePageId = await otaApi.ssr.getServicePageId("Home");
  if (!servicePageId) {
    return {
      notFound: true,
    };
  }

  const dataHomeDetail = await serviceApi.ssr.getDataDetail(servicePageId);
  if (!dataHomeDetail || !dataHomeDetail.Result) {
    return {
      notFound: true,
    };
  }
  const parentId = dataHomeDetail.Data.ParentId || null;

  const [categoryObj, dataHomeListBlock, dataListLocation, dataCheapFlights] =
    await Promise.all([
      serviceApi.ssr.getBreadcrumb(servicePageId, parentId),
      serviceApi.ssr.getDataBlocks(servicePageId),
      otaApi.ssr.getLocation(),
      otaApi.ssr.getCheapFlights(),
    ]);

  return {
    // will be passed to the page component as props
    props: {
      categoryObj,
      dataHomeDetail,
      dataHomeListBlock,
      dataListLocation,
      dataCheapFlights,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 60 seconds
    revalidate: 60 * 1,
  };
}

export default VeMayBayHome;
