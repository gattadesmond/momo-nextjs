import { useEffect } from "react";
import dynamic from "next/dynamic";

import { axios } from "@/lib/index";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import OtaMenu from "@/components/ota/OtaMenu";
import ServiceBreadCrumb from "@/components/service/ServiceBreadCrumb";

// const BlockSearchNew = dynamic(() => import("@/components/ota/BlockSearchNew"));

const BlockSearch = dynamic(() => import("@/components/ota/BlockSearch"));

const SectionGroup = dynamic(() => import("@/components/section/SectionGroup"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

function VeMayBaySlug({
  categoryObj,
  dataMerchant,
  dataListLocation,
  dataListBlock,
  pageVeMayBayId,
  pageMaster,
  isMobile,
}) {
  const metaData = dataMerchant.Data;

  const breadCrumbBlock =
    dataListBlock.Data.filter((x) => x.TypeName === "BreadCrumb")[0] || null;
  const bannerBlock =
    dataListBlock.Data.filter((x) => x.TypeName === "Banner")[0] || null;
  const ctaFooter = dataListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataListBlock.Data.filter(
    (x) => !["Banner", "CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const checkCtaFooter = () => {
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };

  useEffect(() => {
    document.querySelector("body").classList.add("page-ota");
    return function clean() {
      document.querySelector("body").classList.remove("page-ota");
    };
  }, []);
  return (
    <Page
      className=""
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      {breadCrumbBlock ? (
        <ServiceBreadCrumb
          isStickyMobile={!isMenuSticky}
          data={breadCrumbBlock}
        />
      ) : (
        <OtaMenu
          isStickyMobile={!isMenuSticky}
          data={categoryObj}
          pageId={pageVeMayBayId}
          merchantLogo={metaData.Logo}
          merchantActive={metaData.Name}
          isMobile={isMobile}
        />
      )}
      {/* {bannerBlock &&
        <BlockSearchNew
          data={bannerBlock}
          dataListLocation2={dataListLocation}
          isMobile={isMobile}
        />
      } */}

      {bannerBlock && (
        <BlockSearch
          data={bannerBlock}
          dataListLocation={dataListLocation.Data?.Items}
          isMobile={isMobile}
        />
      )}
      <SectionGroup data={listBlock} isMobile={isMobile} />
      {checkCtaFooter() && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
}
export async function getServerSideProps(context) {
  const merchantUrl = context.query.merchant;

  const dataMerchant = await axios.get(
    `/ota-merchant/detail?urlRewrite=${merchantUrl}`,
    null,
    true
  );
  if (!dataMerchant || !dataMerchant.Result) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }
  const idServicePage = dataMerchant.Data.ServicePageId;
  const dataListBlock = await axios.get(
    `/service-page/listBlocks?pageId=${idServicePage}`,
    null,
    true
  );
  const dataListLocation = await axios.get(
    "/ota-location/loadMore?count=9999&lastIndex=0&status=1",
    null,
    true
  );

  const otaCommon = await axios.get(`/common/ota`, null, true);
  if (!otaCommon || !otaCommon.Result || !otaCommon.Data.ServicePageIds) {
    return {
      notFound: true,
    };
  }
  const pageVeMayBay = otaCommon.Data.ServicePageIds["Home"];
  const pageDuLichDiLai = otaCommon.Data.ServicePageIds["Travel"];

  const categoryObj = await axios.get(
    `/service-page/listBreadcrumbs?id=${pageVeMayBay}&parentId=${pageDuLichDiLai}`,
    null,
    true
  );

  return {
    props: {
      categoryObj,
      pageVeMayBayId: pageVeMayBay,
      dataMerchant,
      dataListLocation,
      dataListBlock,
    }, // will be passed to the page component as props
  };
}

export default VeMayBaySlug;
