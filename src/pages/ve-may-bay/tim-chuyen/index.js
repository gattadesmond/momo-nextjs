import { useCallback, useEffect, useRef, useState } from "react";
import dynamic from "next/dynamic";

const InfiniteScroll = dynamic(
  () => import("react-infinite-scroll-component"),
  { ssr: false }
);
const QRCode = dynamic(() => import("qrcode.react"));
const StickyBox = dynamic(() => import("react-sticky-box"));

import otaApi from "@/lib/api/ota-api";
import serviceApi from "@/lib/api/service-api";

import Page from "@/components/page";
import SectionSpace from "@/components/SectionSpace";
// import BlockSearchNew from "@/components/ota/BlockSearchNew";
import BlockSearch from "@/components/ota/BlockSearch";

import OtaMenu from "@/components/ota/OtaMenu";
import FlightItem from "@/components/ota/FlightItem";
import ModalBFM from "@/components/ota/ModalBFM";
import SelectSortBy from "@/components/ota/SelectSortBy";
import QRModal from "@/components/QRModal";
import ModalConfirm from "@/components/ota/ModalConfirm";
import BlockSortBy from "@/components/ota/BlockSortBy";

import { IconBoLoc, IconSapXep } from "@/components/ota/IconOta";

const ModalSapXep = dynamic(() => import("@/components/ota/ModalSapXep"));
const ModalBoLoc = dynamic(() => import("@/components/ota/ModalBoLoc"));
const LoadingFlight = dynamic(() => import("@/components/ota/LoadingFlight"));

function TimKiemVeMayBay({
  categoryObj,
  dataHomeDetail,
  dataHomeListBlock,
  dataListLocation,
  urlQuery,
  pageMaster,
  isMobile,
}) {
  const metaData = dataHomeDetail.Data;
  const bannerBlock =
    dataHomeListBlock.Data.filter((x) => x.TypeName === "Banner")[0] || null;

  const [locationOut, setLocationOut] = useState(null);
  const [locationIn, setLocationIn] = useState(null);

  const defaultOtaParams = { ...otaApi.otaFlightParams };

  const [isReturn, setIsReturn] = useState(false);
  const [requestId, setRequestId] = useState(null);
  const [flightOut, setFlightOut] = useState(null);
  const [flightIn, setFlightIn] = useState(null);
  const [otaParams, setOtaParams] = useState(defaultOtaParams);

  const [listFlight, setListFlight] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [totalFlight, setTotalFlight] = useState(null);
  const [isError, setIsError] = useState(false);
  const [isNoData, setIsNoData] = useState(false);

  const [step, setStep] = useState(0);
  const [isShowModalBFM, setisShowModalBFM] = useState(false);
  const [dataBFM, setdataBFM] = useState(null);

  const [isShowModalConfirm, setIsShowModalConfirm] = useState(false);
  const [loadingConfirm, setLoadingConfirm] = useState(false);
  const [isShowModalQr, setIsShowModalQr] = useState(false);
  const [qrUrl, setQrUrl] = useState(null);

  const [configMoney, setConfigMoney] = useState([0, 10000000]);
  const [rangeMoney, setRangeMoney] = useState([0, 10000000]);
  const [airlineCodes, setAirlineCodes] = useState([]);
  const [timeTakeOff, setTimeTakeOff] = useState([0, 1439]);
  const [timeLanding, setTimeLanding] = useState([0, 1439]);

  const timeout = useRef(null);

  useEffect(() => {
    // ReplaceURL(window.location.href.toLowerCase());

    document.querySelector("body").classList.add("page-ota");
    return function clean() {
      document.querySelector("body").classList.remove("page-ota");

      setListFlight([]);
      setFlightOut(null);
      setFlightIn(null);
      setHasMore(true);
      setIsError(false);
      setIsNoData(false);
      setIsShowModalConfirm(false);
      setisShowModalBFM(false);
      clearTimeout(timeout.current);
    };
  }, []);

  useEffect(() => {
    setTotalFlight(null);
    setFlightOut(null);
    setFlightIn(null);

    setConfigMoney([0, 10000000]);
    setRangeMoney([0, 10000000]);
    setAirlineCodes([]);
    setTimeTakeOff([0, 1439]);
    setTimeLanding([0, 1439]);

    mapLocation(urlQuery, dataListLocation?.Data?.Items);
    mapOtaParams(urlQuery);
  }, [urlQuery]);

  useEffect(() => {
    setHasMore(listFlight.length < totalFlight);
  }, [listFlight]);

  useEffect(() => {
    if (step > 0 && step <= 2) {
      resetListFlight({
        ...otaParams,
        airlineCodes: step == 1 ? null : flightOut.AirlineCode,
      });
    }
    if (isMobile) {
      if (step >= 2) {
        document.getElementById("scrollToTop").style.display = "none";
      } else {
        document.getElementById("scrollToTop").style.display = "block";
      }
    }
  }, [step]);

  const resetListFlight = (params) => {
    setListFlight([]);
    setHasMore(true);
    setIsError(false);
    setIsNoData(false);

    const resetPageIndex = {
      ...params,
      pageIndex: 0,
    };
    setOtaParams(resetPageIndex);
    getFlight(resetPageIndex);
  };

  const mapLocation = (urlQuery, listLocation = []) => {
    const route = urlQuery.route.split(".");
    setLocationOut(
      listLocation.find((x) => x.JsId.toLowerCase() === route[0].toLowerCase())
    );
    setLocationIn(
      listLocation.find((x) => x.JsId.toLowerCase() === route[1].toLowerCase())
    );
  };

  const mapOtaParams = (urlQuery) => {
    const params = { ...otaApi.otaFlightParams };
    params.departureTime = urlQuery.fromDate;
    params.returnTime = urlQuery.toDate ? urlQuery.toDate : null;

    const pax = (urlQuery.pax || "1").split(".");
    params.adult = pax[0] || 1;
    params.child = pax[1] || 0;
    params.infant = pax[2] || 0;

    const route = urlQuery.route.split(".");
    params.deptCode = route[0];
    params.arriCode = route[1];

    params.pageIndex = 0;

    params.airlineCodes = null;
    params.priceMin = null;
    params.priceMax = null;
    params.takeOffTimeRangeStart = null;
    params.takeOffTimeRangeEnd = null;
    params.landingTimeRangeStart = null;
    params.landingTimeRangeEnd = null;

    setOtaParams(params);
    if (step != 1) {
      setStep(1);
    } else {
      resetListFlight(params);
    }
    setIsReturn(urlQuery.flightType === "roundtrip");
  };

  const callApiGetFlight = (params) => {
    if (step == 1) {
      const sendParams = {
        ...params,
        returnTime: null,
      };
      return otaApi.csr.searchFlightsOut(sendParams);
    } else {
      return otaApi.csr.searchFlightsIn(params);
    }
  };

  const stepMoney = 10000;
  const convertMoney = (value, isMax = true) => {
    let result = 0;
    if (isMax) {
      result = (parseInt(value / stepMoney) + 1) * stepMoney;
    } else {
      result = parseInt(value / stepMoney) * stepMoney;
    }
    return result >= 0 ? result : 0;
  };
  const convertTime = (num) => {
    var hours = Math.floor(num / 60);
    var minutes = num % 60;
    return (
      String(hours).padStart(2, "0") + ":" + String(minutes).padStart(2, "0")
    );
  };

  const getFlight = (params) => {
    if (!!totalFlight && listFlight.length >= totalFlight) return;
    timeout.current = setTimeout(() => {
      callApiGetFlight(params)
        .then((res) => {
          if (!res.Data || !res.Result) {
            console.error(res);
            setHasMore(false);
            setIsError(true);
            return;
          }

          if (params.pageIndex == 0) {
            setTotalFlight(res.Data.Total);
            setIsNoData(res.Data.Items.length === 0);

            const priceMin = convertMoney(res.Data.PriceMin, false);
            const priceMax = convertMoney(res.Data.PriceMax, true);

            setConfigMoney([priceMin, priceMax]);
            setRangeMoney([priceMin, priceMax]);
            const cheapest = res.Data.Cheapest || {};
            setAirlineCodes(
              Object.keys(cheapest).filter((x) => cheapest[x] > 0)
            );
          } else {
            if (res.Data.Items.length === 0) {
              setHasMore(false);
              return;
            }
          }
          setOtaParams((params) => ({
            ...params,
            pageIndex: params.pageIndex + 1,
          }));

          setListFlight((items) => [...items, ...res.Data.Items]);
          setRequestId(res.Data.RequestId);
        })
        .catch((e) => {
          console.error(e);
          setHasMore(false);
          setIsError(true);
        });
    }, 1000);
  };

  const selectFlight = (data) => {
    if (step == 1) {
      setFlightOut(data);
      if (isReturn) {
        scrollToH1();
        setStep(2);
        return;
      }
    } else {
      setFlightIn(data);
      if (!isMobile) {
        scrollToH1();
      }
      setStep(3);
    }
    if (!isMobile && isReturn) return;
    showConfirm();
  };

  const showBFM = (data = null) => {
    if (!data) return;
    setisShowModalBFM(true);
    setdataBFM(data);
  };

  const showConfirm = () => {
    setIsShowModalConfirm(true);
  };

  const submitFlight = () => {
    setQrUrl(null);
    setLoadingConfirm(true);
    otaApi.csr
      .submitFlight(requestId || 0, flightOut.JsDataStr, flightIn?.JsDataStr)
      .then((res) => {
        if (!isMobile) {
          setIsShowModalConfirm(false);
        }
        setLoadingConfirm(false);
        if (!res.Data || !res.Result) {
          console.error(res);
          return;
        }
        if (isMobile) {
          window.location.href = res.Data.ShortLink;
        } else {
          setQrUrl(res.Data.ShortLink);
          setIsShowModalQr(true);
        }
      });
  };

  const getImageOfAirline = (code) => {
    switch (code) {
      case "bamboo":
        return "https://static.mservice.io/img/momo-upload-api-220117013522-637779801224372276.png";
      case "vietjet":
        return "https://static.mservice.io/img/momo-upload-api-220117013545-637779801457988532.png";
      default:
        return "https://static.mservice.io/img/momo-upload-api-220117013601-637779801610052992.png";
    }
  };

  const getNameOfAirline = (code) => {
    switch (code) {
      case "bamboo":
        return "Bamboo Airways";
      case "vietjet":
        return "VietJet Air";
      default:
        return "Vietnam Airlines";
    }
  };

  const getNameOfSortBy = (sortType) => {
    switch (sortType) {
      case 1:
        return "Giá vé tăng dần";
      case 2:
        return "Giá vé giảm dần";
      case 3:
        return "Giờ khởi hành sớm nhất";
      case 4:
        return "Giờ khởi hành muộn nhất";
      case 5:
        return "Thời gian bay nhanh nhất";
      default:
        return "";
    }
  };

  const handleChange = (name, value) => {
    resetListFlight({
      ...otaParams,
      [name]: value,
    });
  };

  const onSelectedAirline = (code) => {
    const listCodes = otaParams.airlineCodes || [];
    const newAirlineCodes = listCodes.includes(code)
      ? listCodes.filter((x) => x !== code)
      : [...listCodes, code];
    setOtaParams((params) => ({
      ...params,
      airlineCodes: newAirlineCodes,
    }));
  };

  const selectAllAirline = () => {
    setOtaParams((params) => ({
      ...params,
      airlineCodes: [...airlineCodes],
    }));
  };

  const resetSort = () => {
    scrollToH1();
    const resetParams = {
      ...otaParams,
      airlineCodes: null,
      priceMin: null,
      priceMax: null,
      takeOffTimeRangeStart: null,
      takeOffTimeRangeEnd: null,
      landingTimeRangeStart: null,
      landingTimeRangeEnd: null,
    };
    resetListFlight(resetParams);
  };

  const applySort = () => {
    scrollToH1();
    const resetParams = {
      ...otaParams,
      priceMin: null,
      priceMax: null,
      takeOffTimeRangeStart: convertTime(timeTakeOff[0]),
      takeOffTimeRangeEnd: convertTime(timeTakeOff[1]),
      landingTimeRangeStart: convertTime(timeLanding[0]),
      landingTimeRangeEnd: convertTime(timeLanding[1]),
    };
    if (!resetParams.airlineCodes || resetParams.airlineCodes.length == 0) {
      resetParams.priceMin = rangeMoney[0];
      resetParams.priceMax = rangeMoney[1];
    }
    resetListFlight(resetParams);
  };

  const getInfo = (step) => {
    return step === 1 ? flightOut : flightIn;
  };

  const changeFlight = (step) => {
    setStep(step);
    if (step === 1) {
      setFlightOut(null);
    }
    setFlightIn(null);
  };

  const [isShowModalSapXep, setIsShowModalSapXep] = useState(false);
  const [isShowModalBoLoc, setIsShowModalBoLoc] = useState(false);

  const scrollToH1 = () => {
    const anchor = document.getElementsByTagName("h1")[0];
    const offsetTop = anchor.offsetTop;

    window.scroll({
      top: offsetTop - 60,
      left: 0,
      behavior: "smooth",
    });
  };

  return (
    <Page
      className="page-ota"
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <OtaMenu data={categoryObj} pageId={metaData.Id} isMobile={isMobile} />
      {/* <BlockSearchNew
        data={bannerBlock}
        dataListLocation2={dataListLocation}
        urlQuery={urlQuery}
        isMobile={isMobile}
      /> */}

      <BlockSearch
        data={bannerBlock}
        dataListLocation={dataListLocation.Data?.Items}
        urlQuery={urlQuery}
        isMobile={isMobile}
      />
      <SectionSpace type={1} className="">
        <div className="mx-auto max-w-5xl lg:px-10">
          <h1 className="text-1.5xl mb-2 text-center font-bold text-pink-500 antialiased md:text-[26px]">
            Chuyến bay {isReturn ? "khứ hồi" : "một chiều"}: {locationOut?.JsCv}{" "}
            ({locationOut?.JsId}) - {locationIn?.JsCv} ({locationIn?.JsId})
          </h1>
          <p className="text-md px-2 text-center text-gray-500 md:px-0">
            Vui lòng chọn vé {isReturn ? "chiều đi và chiều về " : ""} để tiến
            hành đặt chuyến bay của bạn.
          </p>
          <div className="mt-4 grid md:grid-cols-12 md:gap-6">
            <div className="relative col-span-3">
              {!isMobile && (
                <StickyBox offsetTop={60} offsetBottom={20}>
                  <BlockSortBy
                    step={step}
                    otaParams={otaParams}
                    airlineCodes={airlineCodes}
                    rangeMoney={rangeMoney}
                    stepMoney={stepMoney}
                    configMoney={configMoney}
                    setRangeMoney={setRangeMoney}
                    selectAllAirline={selectAllAirline}
                    onSelectedAirline={onSelectedAirline}
                    getImageOfAirline={getImageOfAirline}
                    getNameOfAirline={getNameOfAirline}
                    timeTakeOff={timeTakeOff}
                    setTimeTakeOff={setTimeTakeOff}
                    timeLanding={timeLanding}
                    setTimeLanding={setTimeLanding}
                    convertTime={convertTime}
                    resetSort={resetSort}
                    applySort={applySort}
                  />
                  <div className="sticky bottom-0 left-0 flex items-center justify-around space-x-2 bg-gray-50 py-4 lg:space-x-3 lg:px-1">
                    <button
                      type="button"
                      className="btn-primary-outline w-1/2 !px-0 text-center"
                      disabled={otaParams.pageIndex === 0}
                      onClick={() => resetSort()}
                    >
                      Xóa bộ lọc
                    </button>
                    <button
                      type="button"
                      className="btn-primary w-1/2 !px-0 text-center"
                      disabled={otaParams.pageIndex === 0}
                      onClick={() => applySort()}
                    >
                      Áp dụng
                    </button>
                  </div>
                </StickyBox>
              )}
            </div>
            <div className="col-span-9 min-h-[100px]">
              {isReturn && !isMobile && (
                <div>
                  <div className="relative mb-5 pt-5">
                    <div className="absolute left-0 right-0 top-9 mx-auto h-1.5 w-4/6 rounded-full bg-gray-300" />
                    <div className="relative flex justify-around">
                      {[1, 2, 3].map((item) => (
                        <div
                          key={item}
                          className="flex flex-1 flex-col items-center justify-center"
                        >
                          <div
                            className={`mb-4 flex h-9 w-9 items-center justify-center rounded-full transition-all
                              ${
                                step >= item
                                  ? "bg-pink-400 text-white"
                                  : "bg-gray-300 text-gray-600"
                              }
                            `}
                            style={{
                              boxShadow:
                                step === item ? "0 0 0 8px #fee5f0" : "",
                            }}
                          >
                            {item}
                          </div>
                          <div
                            className={`text-sm font-bold transition-all
                              ${
                                step >= item
                                  ? "text-gray-600"
                                  : "text-gray-500 text-opacity-70"
                              }
                            `}
                          >
                            {item === 1 && <span>Chọn vé chiều đi</span>}
                            {item === 2 && <span>Chọn vé chiều về</span>}
                            {item === 3 && <span>Đặt vé</span>}
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="mb-4 flex justify-between space-x-4">
                    {[1, 2].map((item, index) => (
                      <div
                        key={item}
                        className={`w-1/2 rounded-lg border bg-white p-3.5 shadow-md
                          ${
                            step === item
                              ? "border-pink-400"
                              : "border-gray-100"
                          }
                        `}
                      >
                        <div className="flex flex-1 items-center justify-between">
                          <div
                            className={`flex h-6 w-6 items-center justify-center rounded-full font-bold
                              ${
                                step === item
                                  ? "bg-pink-400 text-white"
                                  : "bg-gray-200 text-gray-500"
                              }
                            `}
                          >
                            {item}
                          </div>
                          <div className="flex-initial pl-3">
                            <div
                              className={`text-sm font-bold text-opacity-80
                                ${
                                  step === item
                                    ? "text-pink-400"
                                    : "text-gray-500"
                                }
                              `}
                            >
                              {item === 1 ? "Vé chiều đi" : "Vé chiều về"}
                            </div>
                            <div className="text-xs font-bold text-gray-400">
                              {item === 1
                                ? otaParams.departureTime
                                : otaParams.returnTime}
                            </div>
                          </div>
                          <div className="flex flex-auto justify-end">
                            {step > item && (
                              <button
                                type="button"
                                className="btn-default border-blue-500 bg-blue-500 !px-3.5 text-white hover:bg-blue-700"
                                onClick={() => changeFlight(item)}
                              >
                                {item === 1 ? "Đổi chuyến đi" : "Đổi chuyến về"}
                              </button>
                            )}
                          </div>
                        </div>
                        <div className="mt-3">
                          {getInfo(item) && (
                            <div className="items-start justify-between lg:flex">
                              <div className="flex flex-initial">
                                <div className="flex h-7 w-7 items-center justify-center rounded-lg border border-gray-200 bg-gray-50">
                                  <img
                                    alt={getInfo(item).AirlineCode}
                                    src={getImageOfAirline(
                                      getInfo(item).AirlineCode
                                    )}
                                    width={26}
                                    height={26}
                                    loading="lazy"
                                  />
                                </div>
                                <div className="flex flex-col justify-center px-3 lg:px-2">
                                  <div className="text-xs font-bold text-gray-600">
                                    {getNameOfAirline(
                                      getInfo(item).AirlineCode
                                    )}
                                  </div>
                                  <span className="text-[10px] font-bold text-gray-400">
                                    {getInfo(item).TicketStr}
                                  </span>
                                </div>
                                {/* 
                                  <div className="flex justify-end items-center">
                                    {getInfo(item) &&
                                      <div className="text-right text-sm font-bold text-red-400">
                                        {getInfo(item).FareStr} đồng/khách
                                      </div>
                                    }
                                  </div> 
                                */}
                              </div>
                              <div className="mt-2 flex flex-1 items-center rounded-md bg-[#f7faff] px-2 py-2 lg:mt-0">
                                {getInfo(item) && (
                                  <>
                                    <div className="flex-initial px-2 text-center lg:px-1">
                                      <div className="text-xs font-bold text-gray-600 text-opacity-90">
                                        {getInfo(item).DepartureTimeStr}
                                      </div>
                                      <div className="text-xs font-bold text-gray-500 text-opacity-80">
                                        {getInfo(item).DepartureCode}
                                      </div>
                                    </div>
                                    <div className="flex-auto px-4 text-center text-gray-600 lg:px-1">
                                      <div className="relative">
                                        <div className="pb-1.5 text-xs font-bold text-gray-500 text-opacity-70">
                                          {getInfo(item).DurationStr}
                                        </div>
                                        <div className="absolute left-0 top-1/2 h-0 w-full -translate-y-1/2 border-t border-dotted border-gray-400"></div>
                                        <div className="absolute left-0 top-1/2 flex w-full -translate-y-1/2 justify-center">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="-rotate-90"
                                            width="11"
                                            height="14"
                                            viewBox="0 0 16 19"
                                            fill="none"
                                          >
                                            <path
                                              d="M10 15.8333L10 12.0633L15.7481 8.59646C15.8247 8.55023 15.8883 8.48348 15.9325 8.40297C15.9767 8.32245 16 8.23103 16 8.13795L16 5.97735C16 5.62671 15.6822 5.3737 15.3628 5.47002L10 7.08734L10 3.69437L11.8 2.26937C11.9259 2.16975 12 2.01306 12 1.84714L12 0.528022C12 0.184635 11.6944 -0.0673796 11.3788 0.0160756L8 1.05547L4.62125 0.0160753C4.30563 -0.0673799 4 0.184635 4 0.528022L4 1.84714C4 2.01339 4.07406 2.16975 4.2 2.26937L6 3.69437L6 7.08734L0.637501 5.47002C0.318125 5.3737 7.07723e-07 5.62671 6.92396e-07 5.97735L5.97953e-07 8.13795C5.89677e-07 8.32729 0.0962502 8.50212 0.251876 8.59613L6 12.0633L6 15.8333C6 16.9994 6.89531 19 8 19C9.10469 19 10 16.9994 10 15.8333Z"
                                              fill="#4F4F4F"
                                            />
                                          </svg>
                                        </div>
                                        <div className="pt-2 text-xs font-bold text-gray-500 text-opacity-70">
                                          {getInfo(item).TotalFlightSegments > 1
                                            ? `${
                                                getInfo(item)
                                                  .TotalFlightSegments - 1
                                              } điểm dừng`
                                            : "Bay thẳng"}
                                        </div>
                                      </div>
                                    </div>
                                    <div className="flex-initial text-center">
                                      <div className="text-xs font-bold text-gray-600 text-opacity-90">
                                        {getInfo(item).ArrivalTimeStr}
                                        {getInfo(item).TotalDays > 0 && (
                                          <small className="pl-1 font-normal text-gray-400">
                                            (+{getInfo(item).TotalDays}d)
                                          </small>
                                        )}
                                      </div>
                                      <div className="text-xs font-bold text-gray-500 text-opacity-80">
                                        {getInfo(item).ArrivalCode}
                                      </div>
                                    </div>
                                  </>
                                )}
                              </div>
                            </div>
                          )}
                          {!getInfo(item) && (
                            <div className="flex h-20 items-center justify-center lg:h-14">
                              <span className="text-gray-400">
                                Vui lòng chọn vé máy bay
                              </span>
                            </div>
                          )}
                        </div>
                      </div>
                    ))}
                  </div>
                  <hr className="mb-4" />
                </div>
              )}
              {!isMobile && step === 3 && (
                <div className="text-center">
                  <p className="mb-3 text-gray-600">
                    Lưu ý: Bạn sẽ phải chọn lại chiều về nếu thay đổi lịch trình
                    chiều đi
                  </p>
                  <button
                    type="button"
                    className="btn-primary w-32"
                    onClick={() => showConfirm()}
                  >
                    Đặt vé
                  </button>
                </div>
              )}
              {(step < 3 || isMobile) && (
                <>
                  <div className="flex items-center justify-between">
                    {!isReturn && (
                      <span className="text-sm font-bold text-pink-500">
                        Mời bạn chọn vé máy bay
                      </span>
                    )}
                    {isReturn && (
                      <span className="flex-initial text-sm font-bold text-pink-500">
                        {step == 1
                          ? "Chọn vé máy bay chiều đi "
                          : "Chọn vé máy bay chiều về "}
                        (
                        {step == 1
                          ? otaParams.departureTime
                          : otaParams.returnTime}
                        )
                      </span>
                    )}
                    {!isMobile && (
                      <div className="hidden items-center md:flex">
                        <span className="text-sm font-bold text-gray-500">
                          Sắp xếp
                        </span>
                        <SelectSortBy
                          sortValue={otaParams.sortType}
                          getTitle={getNameOfSortBy}
                          onChange={(value) => handleChange("sortType", value)}
                        />
                      </div>
                    )}
                  </div>
                  <div id="scrollableDiv" className="py-4">
                    {otaParams.pageIndex === 0 && !isError && !isNoData && (
                      <LoadingFlight />
                    )}
                    <InfiniteScroll
                      dataLength={listFlight.length}
                      next={() => getFlight(otaParams)}
                      hasMore={hasMore}
                      loader={
                        // <p className="py-4 text-gray-400 text-center">
                        //   <span>Đang load ...</span>
                        // </p>
                        <LoadingFlight />
                      }
                    >
                      {listFlight.map((item, index) => (
                        <FlightItem
                          key={index}
                          data={item}
                          logo={getImageOfAirline(item.AirlineCode)}
                          fullName={getNameOfAirline(item.AirlineCode)}
                          onSelect={() => selectFlight(item)}
                          onShowBFM={() => showBFM(item)}
                        />
                      ))}
                    </InfiniteScroll>
                    {(isNoData || isError) && (
                      <p className="mb-3 px-3 text-center font-bold text-gray-600 text-opacity-75">
                        <img
                          src="https://static.mservice.io/img/momo-upload-api-220117054143-637779949039955289.png"
                          className="mx-auto my-3 block"
                        />
                        Không tìm thấy chuyến bay - Quý khách vui lòng thay đổi
                        ngày hoặc hành trình khác
                      </p>
                    )}
                  </div>
                </>
              )}
              {isMobile && (
                <div className="sticky left-0 bottom-0 -mx-4">
                  <div className="px-6 py-2.5">
                    <div className="border-momo flex rounded-full border bg-white">
                      <button
                        type="button"
                        className="flex w-1/2 items-center justify-center space-x-2 py-1.5 text-sm font-bold text-gray-600"
                        onClick={() => setIsShowModalSapXep(true)}
                      >
                        <IconSapXep />
                        <span>Sắp xếp</span>
                      </button>
                      <div className="bg-momo my-1 w-[0.25px]" />
                      <button
                        type="button"
                        className="flex w-1/2 items-center justify-center space-x-2 py-1.5 text-sm font-bold text-gray-600"
                        onClick={() => setIsShowModalBoLoc(true)}
                      >
                        <IconBoLoc />
                        <span>Bộ lọc</span>
                      </button>
                    </div>
                  </div>
                  {flightOut && isReturn && (
                    <div
                      className="rounded-tl rounded-tr bg-white p-4"
                      style={{
                        boxShadow: "0 2px 8px 2px rgb(13 12 34 / 8%)",
                      }}
                    >
                      <div className="flex justify-between text-sm">
                        <div>
                          <div className="font-bold text-pink-400">
                            Vé chiều đi
                          </div>
                          <div className="text-xs font-bold text-gray-400">
                            {otaParams.departureTime}
                          </div>
                        </div>
                        <div>
                          <button
                            type="button"
                            className="btn-default border-blue-500 bg-blue-500 !px-3.5 text-white hover:bg-blue-700"
                            onClick={() => changeFlight(1)}
                          >
                            Đổi chuyến đi
                          </button>
                        </div>
                      </div>
                      <div className="mt-3 flex items-start justify-between">
                        <div className="flex h-7 w-7 flex-initial items-center justify-center rounded-lg border border-gray-200 bg-gray-50">
                          <img
                            alt={flightOut.AirlineCode}
                            src={getImageOfAirline(flightOut.AirlineCode)}
                            width={26}
                            height={26}
                            loading="lazy"
                          />
                        </div>
                        <div className="ml-2 mr-4 flex flex-initial flex-col justify-center">
                          <div className="text-xs font-bold text-gray-700">
                            {getNameOfAirline(flightOut.AirlineCode)}
                          </div>
                          <span className="text-[10px] font-bold text-gray-400">
                            {flightOut.TicketStr}
                          </span>
                        </div>
                        <div className="flex flex-1 items-center rounded-md bg-[#f7faff] px-2 py-2">
                          <div className="flex-initial px-2 text-center">
                            <div className="text-xs font-bold text-gray-600 text-opacity-90">
                              {flightOut.DepartureTimeStr}
                            </div>
                            <div className="text-xs text-gray-500 text-opacity-80">
                              {flightOut.DepartureCode}
                            </div>
                          </div>
                          <div className="flex-auto px-2.5 text-center text-gray-600">
                            <div className="relative">
                              <div className="pb-1 text-[10px] font-bold text-gray-500 text-opacity-70">
                                {flightOut.DurationStr}
                              </div>
                              <div className="absolute left-0 top-1/2 h-0 w-full -translate-y-1/2 border-t border-dotted border-gray-400"></div>
                              <div className="absolute left-0 top-1/2 flex h-2.5 w-full -translate-y-1/2 justify-center">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="-rotate-90"
                                  height="100%"
                                  viewBox="0 0 16 19"
                                  fill="none"
                                >
                                  <path
                                    d="M10 15.8333L10 12.0633L15.7481 8.59646C15.8247 8.55023 15.8883 8.48348 15.9325 8.40297C15.9767 8.32245 16 8.23103 16 8.13795L16 5.97735C16 5.62671 15.6822 5.3737 15.3628 5.47002L10 7.08734L10 3.69437L11.8 2.26937C11.9259 2.16975 12 2.01306 12 1.84714L12 0.528022C12 0.184635 11.6944 -0.0673796 11.3788 0.0160756L8 1.05547L4.62125 0.0160753C4.30563 -0.0673799 4 0.184635 4 0.528022L4 1.84714C4 2.01339 4.07406 2.16975 4.2 2.26937L6 3.69437L6 7.08734L0.637501 5.47002C0.318125 5.3737 7.07723e-07 5.62671 6.92396e-07 5.97735L5.97953e-07 8.13795C5.89677e-07 8.32729 0.0962502 8.50212 0.251876 8.59613L6 12.0633L6 15.8333C6 16.9994 6.89531 19 8 19C9.10469 19 10 16.9994 10 15.8333Z"
                                    fill="#4F4F4F"
                                  />
                                </svg>
                              </div>
                              <div className="pb-1 text-[10px] font-bold text-gray-500 text-opacity-70">
                                {flightOut.TotalFlightSegments > 1
                                  ? `${
                                      flightOut.TotalFlightSegments - 1
                                    } điểm dừng`
                                  : "Bay thẳng"}
                              </div>
                            </div>
                          </div>
                          <div className="flex-initial text-center">
                            <div className="text-xs font-bold text-gray-600 text-opacity-90">
                              {flightOut.ArrivalTimeStr}
                              {flightOut.TotalDays > 0 && (
                                <small className="pl-1 font-normal text-gray-400">
                                  (+{flightOut.TotalDays}d)
                                </small>
                              )}
                            </div>
                            <div className="text-xs text-gray-500 text-opacity-80">
                              {flightOut.ArrivalCode}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </SectionSpace>
      <ModalBFM
        isShow={isShowModalBFM}
        onDismiss={() => setisShowModalBFM(false)}
        data={dataBFM}
      />
      <ModalConfirm
        isOpen={isShowModalConfirm}
        onDismiss={setIsShowModalConfirm}
        locationOut={locationOut?.DisplayName}
        locationIn={locationIn?.DisplayName}
        flightOut={flightOut}
        flightIn={flightIn}
        departureTime={otaParams.departureTime}
        returnTime={otaParams.returnTime}
        imageOfAirline={getImageOfAirline}
        nameOfAirline={getNameOfAirline}
        onSubmit={submitFlight}
        isLoading={loadingConfirm}
      />
      {!isMobile && (
        <QRModal
          isOpen={isShowModalQr}
          onDismiss={() => setIsShowModalQr(false)}
          isDark={true}
          title="Quét mã để tải ứng dụng hoặc đặt vé máy bay ngay"
          shortTitle="Quét mã để tải ứng dụng hoặc đặt vé máy bay ngay"
          qrImage={
            <QRCode
              value={qrUrl}
              size={215}
              imageSettings={{
                src: "https://static.mservice.io/pwa/images/logoMomox50.png",
                x: null,
                y: null,
                height: 50,
                width: 50,
                excavate: true,
              }}
              level="H"
            />
          }
        />
      )}
      {isMobile && (
        <>
          <ModalSapXep
            isOpen={isShowModalSapXep}
            onDismiss={() => setIsShowModalSapXep(false)}
            sortValue={otaParams.sortType}
            getTitle={getNameOfSortBy}
            onChange={(value) => {
              handleChange("sortType", value);
              scrollToH1();
            }}
          />
          <ModalBoLoc
            isOpen={isShowModalBoLoc}
            onDismiss={() => setIsShowModalBoLoc(false)}
            step={step}
            otaParams={otaParams}
            airlineCodes={airlineCodes}
            rangeMoney={rangeMoney}
            stepMoney={stepMoney}
            configMoney={configMoney}
            setRangeMoney={setRangeMoney}
            selectAllAirline={selectAllAirline}
            onSelectedAirline={onSelectedAirline}
            getImageOfAirline={getImageOfAirline}
            getNameOfAirline={getNameOfAirline}
            timeTakeOff={timeTakeOff}
            setTimeTakeOff={setTimeTakeOff}
            timeLanding={timeLanding}
            setTimeLanding={setTimeLanding}
            convertTime={convertTime}
            resetSort={() => {
              resetSort();
              scrollToH1();
            }}
            applySort={() => {
              applySort();
              scrollToH1();
            }}
          />
        </>
      )}
    </Page>
  );
}
export async function getServerSideProps(context) {
  const urlQuery = context.query;
  //check query
  if (
    !urlQuery ||
    !urlQuery.flightType ||
    !urlQuery.route ||
    !urlQuery.fromDate
  ) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }
  //check route
  const route = urlQuery.route;
  if (!route.includes(".")) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }

  const servicePageId = await otaApi.ssr.getServicePageId("Home");
  if (!servicePageId) {
    return {
      notFound: true,
    };
  }

  const dataHomeDetail = await serviceApi.ssr.getDataDetail(servicePageId);
  if (!dataHomeDetail || !dataHomeDetail.Result) {
    return {
      notFound: true,
    };
  }
  const parentId = dataHomeDetail.Data.ParentId || null;

  const categoryObj = await serviceApi.ssr.getBreadcrumb(
    servicePageId,
    parentId
  );
  const dataListLocation = await otaApi.ssr.getLocation();
  if (!dataListLocation || !dataListLocation.Result) {
    return {
      notFound: true,
    };
  }

  // trường hợp không có danh sách các sân bay
  const listLocation = dataListLocation.Data.Items;
  if (!listLocation || listLocation.length === 0) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }
  // trường hợp sai code sân bay nơi đi hoặc nơi đến
  const parseRoute = route.split(".");
  const routeIn = listLocation.find(
    (x) => x.JsId.toLowerCase() === parseRoute[0].toLowerCase()
  );
  const routeOut = listLocation.find(
    (x) => x.JsId.toLowerCase() === parseRoute[1].toLowerCase()
  );
  if (!routeIn || !routeOut) {
    return {
      redirect: {
        destination: "/ve-may-bay",
        permanent: false,
      },
    };
  }

  const dataHomeListBlock = await serviceApi.ssr.getDataBlocks(servicePageId);

  return {
    props: {
      categoryObj,
      dataHomeDetail,
      dataHomeListBlock,
      dataListLocation,
      urlQuery,
    }, // will be passed to the page component as props
  };
}

export default TimKiemVeMayBay;
