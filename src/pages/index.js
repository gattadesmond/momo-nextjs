import Head from "next/head";
import styles from "../styles/Home.module.css";

import Page from "@/components/page";

import Container from "@/components/Container";

import Link from "next/link";

import fetcher from "@/lib/fetcher";

import useSWR from "swr";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

import SectionFeature from "@/components/blog/SectionFeature";
import SectionList from "@/components/blog/SectionList";
import PopularList from "@/components/blog/PopularList";

function Home({ dataFeaturePost, dataHomePost }) {
  // const { data } = useSWR(`http://dev.momo.vn/api/blog/list`, fetcher);
  // console.log(dataFeaturePost.Data.Items);

  return (
    <Page
      title="Momo - Blog"
      description="My personal blog, built with Next.js API routes deployed as serverless functions."
    >
      {/* <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head> */}

      <Container>
        <div className="my-10 pt-10">
          <h1 className="text-5xl font-bold mb-4 text-gray-900">Blog</h1>
          <h4 className="text-xl text-gray-500">
            Chia sẻ thông tin, kiến thức có giá trị dành cho bạn
          </h4>
        </div>

        <div className="flex border-b hidden">
          <div className="py-2 mr-6">
            <Link href="/">
              <a className="text-gray-700 hover:text-pink-700">Mới nhất</a>
            </Link>
          </div>
          {dataHomePost.Data.Categories.map((item, index) => (
            <div className="py-2 mr-6" key={item.id}>
              <Link href={item.Link}>
                <a className="text-gray-700 hover:text-pink-700">{item.Name}</a>
              </Link>
            </div>
          ))}
        </div>

        <SectionFeature data={dataHomePost.Data.ListBlogFeatured.Data.Items} />

        <div className="border-t border-gray-200 pt-6 mt-6" />

        <div className="grid  grid-cols-1 md:grid-cols-3 gap-6">
          
          <div className="col-span-2 md:pr-4">
            <h2 className="text-xl text-gray-800 font-semibold mb-0">
              
            </h2>

            <div className="grid grid-cols-1 divide-y divide-gray-300 ">
              <SectionList data={dataHomePost.Data.ListBlogs.Data.Items} />
            </div>
          </div>

          <div className="">
            <h2 className="text-xl text-gray-800 font-semibold mb-0">
              Xem nhiều nhất
            </h2>
            <div className="grid grid-cols-1 mt-4  ">
              <PopularList data={dataHomePost.Data.ListBlogTopView.Data.Items} />
            </div>
          </div>
        </div>
      </Container>
    </Page>
  );
}

export async function getServerSideProps(context) {
  // Fetch data from 4 post
  const resFeaturePost = await fetch(`http://dev.momo.vn/api/blog/list`);
  const dataFeaturePost = await resFeaturePost.json();

  // Fetch data from home post

  const resHomePost = await fetch(`http://dev.momo.vn/api/blog/home`);
  const dataHomePost = await resHomePost.json();

  // if (!data) {
  //   return {
  //     notFound: true,
  //   }
  // }

  return {
    props: { dataFeaturePost, dataHomePost }, // will be passed to the page component as props
  };
}

export default Home;
