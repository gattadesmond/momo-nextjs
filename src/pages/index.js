
import Page from "@/components/page";

import Container from "@/components/Container";
import Link from "next/link";

function Home({ dataHomePost, pageMaster, isMobile }) {

  const data = dataHomePost.Data;

  return (
    <Page
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      scripts={data.Meta.Scripts}
      robots={data.Meta.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <Container>
        <Link  href="/blog" >
          <a className="block px-10 py-6 m-5 text-2xl rounded-md text-gray-900  border border-gray-200">
            Blog
          </a>
        </Link>

        <Link href="/cinema" >
          <a className="block px-10 py-6 m-5 text-2xl rounded-md text-gray-900  border border-gray-200">
            Cinema
          </a>
        </Link>

        <Link href="/article" >
          <a className="block px-10 py-6 m-5 text-2xl rounded-md text-gray-900  border border-gray-200">
            Article
          </a>
        </Link>

      </Container>
    </Page>
  );
}

export async function getServerSideProps(context) {
  const dataHomePost = {}; //await axios.get(`/blog/home`);

  return {
    props: { dataHomePost }, // will be passed to the page component as props
  };
}

export default Home;
