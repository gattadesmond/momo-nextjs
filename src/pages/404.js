import Page from "@/components/page";

import Container from "@/components/Container";

import isMobile from "@/lib/isMobile";
import { axios } from "@/lib/index";

export default function Page404({ pageMaster1 }) {
  const isMobileDevice = isMobile();
  return (
    <Page header={pageMaster1.Data.MenuHeaders} isMobile={isMobileDevice}>
      <div className="flex items-center justify-center py-32 md:py-44">
        <Container>
          <div className="mb-4 font-bold text-center text-gray-200 text-8xl md:text-9xl">
            404
          </div>
          <h1 className="mb-4 text-3xl font-bold text-center md:text-4xl">
            Page Not Found
          </h1>
          <div className="max-w-2xl mx-auto text-sm text-center text-gray-500 md:text-base">
            Xin lỗi, trang bạn đang tìm kiếm không tồn tại!
          </div>

          <div className="mx-auto text-center mt-7 ">
            <a
              className="inline-flex items-center px-5 py-2 text-white bg-pink-700 rounded "
              href="https://momo.vn/"
            >
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-4 h-4 mr-1"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M10 19l-7-7m0 0l7-7m-7 7h18"
                />
              </svg>{" "}
              Quay lại trang chủ
            </a>
          </div>
        </Container>
      </div>
    </Page>
  );
}

export async function getStaticProps() {
  const pageMaster1 = await axios.get(`/common/master`, null, true);
  return {
    props: { pageMaster1 },
  };
}
