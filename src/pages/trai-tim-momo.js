import { useEffect } from "react";
import dynamic from "next/dynamic";

import { axios } from "@/lib/index";
import { getRootSlug } from "@/lib/url";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import ServiceMenu from "@/components/service/ServiceMenu";

const SectionGroup = dynamic(() => import("@/components/section/SectionGroup"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

const ServicePages = ({
  dataServiceBreadcrumbs,
  dataServiceDetail,
  dataServiceListBlock,
  pageMaster,
  isMobile,
}) => {
  const metaData = dataServiceDetail.Data;

  const ctaFooter = dataServiceListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataServiceListBlock.Data.filter(
    (x) => !["CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const checkCtaFooter = () => {
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };

  useEffect(() => {
    document.querySelector("body").classList.add("fix-header");
    return function clean() {
      document.querySelector("body").classList.remove("fix-header");
    };
  }, []);

  return (
    <Page
      className=""
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      {dataServiceBreadcrumbs && (
        <ServiceMenu
          data={dataServiceBreadcrumbs}
          pageId={metaData.Id}
          isMobile={isMobile}
        />
      )}
      <SectionGroup data={listBlock} isMobile={isMobile} startType={2} />
      {checkCtaFooter() && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
};
export async function getStaticProps(context) {
  const url = "trai-tim-momo"; //context.url || context.resolvedUrl;
  let queryAllServicePage = null,
    servicePage = null,
    dataServiceDetail = null,
    dataServiceListBlock = null,
    dataServiceBreadcrumbs = null;

  dataServiceDetail = await axios.get(
    `/service-page/detail-by-slug?slug=${url}&type=1`,
    null,
    true
  );

  if (dataServiceDetail && dataServiceDetail.Result) {
    servicePage = dataServiceDetail.Data;
    dataServiceBreadcrumbs = await axios.get(
      `/service-page/listBreadcrumbs?id=${servicePage.Id}${
        servicePage.ParentId ? "&parentId=" + servicePage.ParentId : ""
      }`,
      null,
      true
    );
    dataServiceListBlock = await axios.get(
      `/service-page/listBlocks?pageId=${servicePage.Id}`,
      null,
      true
    );
  } else {
    return {
      notFound: true,
    };
  }

  // queryAllServicePage = await axios.get(`service-page/list-route?count=1000`, null, true);
  // if (queryAllServicePage.Result == false) {
  //   return {
  //     notFound: true,
  //   };
  // }

  // servicePage = queryAllServicePage.Data.find(page => page.Link === getRootSlug(url)) || null;
  // if (servicePage) {
  //   dataServiceDetail = await axios.get(`/service-page/detail?id=${servicePage.Id}`, null, true);
  //   if (dataServiceDetail.Result == false) {
  //     return {
  //       notFound: true,
  //     };
  //   }

  //   dataServiceBreadcrumbs = await axios.get(`/service-page/listBreadcrumbs?id=${servicePage.Id}${servicePage.ParentId ? '&parentId=' + servicePage.ParentId : ''}`, null, true);
  //   dataServiceListBlock = await axios.get(`/service-page/listBlocks?pageId=${servicePage.Id}`, null, true);
  // } else {
  //   return {
  //     notFound: true
  //   }
  // }

  return {
    props: { dataServiceBreadcrumbs, dataServiceDetail, dataServiceListBlock },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 60 seconds
    revalidate: 60 * 1, // In seconds
  };
}

export default ServicePages;
