const data = {
  "Result": true,
  "Data": {
    "Category": {
      "Id": 101,
      "Name": "Du lịch",
      "Description": "Trải nghiệm du lịch, kinh nghiệm đặt phòng khách sạn, vé máy bay, tàu xe.",
      "Link": "/blog/du-hi"
    },
    "ListBlogs": {
      "Items": [
        {
          "Link": "/blog/kinh-nghiem-du-lich-quang-ninh-c101dt222",
          "Title": "Kinh nghiệm du lịch Quảng Ninh không thể bỏ lỡ từ Ví MoMo",
          "ShortContent": "Vịnh Hạ Long, Quan Lạn, Cô Tô, Bình Liêu… còn nơi nào hội tụ đủ vẻ đẹp rừng - biển như Quảng Ninh! Xem ngay kinh nghiệm du lịch Quảng Ninh chi tiết nhất cùng Ví MoMo và lên đường khám phá ngay thôi.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604103224-637583995443173534.jpg",
          "Id": 222,
          "TotalView": 25,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 10:39:00",
          "TotalWords": 4440,
          "TimeReadAvg": 16
        },
        {
          "Link": "/blog/khach-san-ha-giang-c101dt228",
          "Title": "Cùng Ví MoMo lạc lối ở Cao nguyên đá với những khách sạn Hà Giang siêu đẹp",
          "ShortContent": "Hà Giang luôn thu hút các vị khách phương xa với núi non trùng điệp, thiên nhiên hoang sơ và văn hoá các dân tộc đa dang. Qua bài viết dưới đây Ví MoMo sẽ đưa bạn đi tìm những khách sạn Hà Giang tốt nhất cho chuyến du lịch sắp tới của mình!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210607090916-637586537563296168.jpg",
          "Id": 228,
          "TotalView": 2,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "03/06/2021",
          "Date": "2021-06-03 11:40:00",
          "TotalWords": 3355,
          "TimeReadAvg": 12
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-con-dao-c101dt234",
          "Title": "Kinh nghiệm du lịch Côn Đảo tự túc siêu tiết kiệm cùng Ví MoMo",
          "ShortContent": "Cùng khám phá kinh nghiệm du lịch Côn Đảo cực chi tiết từ A-Z với Ví MoMo. Một kỳ nghỉ tuyệt vời và vô cùng thú vị đang chờ đón bạn tại nơi đây.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210609120733-637588372535790640.jpg",
          "Id": 234,
          "TotalView": 2,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "02/06/2021",
          "Date": "2021-06-02 12:06:00",
          "TotalWords": 4947,
          "TimeReadAvg": 18
        },
        {
          "Link": "/blog/top-khach-san-da-nang-c101dt223",
          "Title": "Khám phá những khách sạn Đà Nẵng hot nhất 2021",
          "ShortContent": "Nếu bạn đang lên kế hoạch cho chuyến vi vu “thành phố đáng sống nhất\" thì đừng bỏ lỡ danh sách khách sạn Đà Nẵng hot nhất tại Ví MoMo để có chuyến đi trọn vẹn hơn nhé!\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210528101139-637577934992242601.jpg",
          "Id": 223,
          "TotalView": 91,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "02/06/2021",
          "Date": "2021-06-02 09:44:59",
          "TotalWords": 3442,
          "TimeReadAvg": 13
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-vung-tau-c101dt216",
          "Title": "Du lịch Vũng Tàu tự túc siêu tiết kiệm cùng Ví MoMo",
          "ShortContent": "Những bí kíp du lịch Vũng Tàu sắp được Ví MoMo bật mí không sót chi tiết nào trong bài viết dưới đây. Nếu bạn là một \"fan\" biển chính hiệu và cần bổ sung gấp \"vitamin sea\" thì đừng bỏ lỡ nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210518145032-637569462320736581.jpg",
          "Id": 216,
          "TotalView": 264,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "25/05/2021",
          "Date": "2021-05-25 16:57:29",
          "TotalWords": 4660,
          "TimeReadAvg": 17
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-phu-yen-c101dt218",
          "Title": "Cùng Ví MoMo bỏ túi kinh nghiệm du lịch Phú Yên, phá đảo xứ hoa vàng",
          "ShortContent": "Với tất tần tật kinh nghiệm du lịch Phú Yên từ Ví MoMo, bạn còn chần chừ gì mà chưa đặt ngay vé bay, khách sạn và lên đường để tận hưởng vẻ đẹp của “xứ hoa vàng cỏ xanh” trong kỳ nghỉ sắp tới!\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210521094635-637571871958515419.jpg",
          "Id": 218,
          "TotalView": 197,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "25/05/2021",
          "Date": "2021-05-25 10:28:58",
          "TotalWords": 4694,
          "TimeReadAvg": 17
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-quy-nhon-c101dt221",
          "Title": "Kinh nghiệm du lịch Quy Nhơn siêu hot với Ví MoMo",
          "ShortContent": "Hè này, đừng bỏ lỡ trọn bộ kinh nghiệm du lịch Quy Nhơn cùng Ví MoMo. Bạn sẽ có ngay bí quyết khám phá thiên đường biển đẹp vạn người mê, tựa đảo Jeju (Hàn Quốc) đó. ",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210525091944-637575311843138136.jpg",
          "Id": 221,
          "TotalView": 57,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "25/05/2021",
          "Date": "2021-05-25 09:13:52",
          "TotalWords": 4733,
          "TimeReadAvg": 17
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-sapa-c101dt217",
          "Title": "Trọn bộ kinh nghiệm du lịch Sapa từ Ví MoMo",
          "ShortContent": "Trải nghiệm vùng Tây Bắc hùng vĩ, nên thơ với kinh nghiệm du lịch Sapa cùng Ví MoMo, tất tần tật mọi bí kíp đặt vé bay, khách sạn, vui chơi hấp dẫn đang chờ bạn.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210520162807-637571248878205956.jpg",
          "Id": 217,
          "TotalView": 41,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "24/05/2021",
          "Date": "2021-05-24 15:35:15",
          "TotalWords": 4146,
          "TimeReadAvg": 15
        },
        {
          "Link": "/blog/top-khach-san-phu-quoc-voi-trai-nghiem-dinh-nhat-c101dt215",
          "Title": "Top 17 khách sạn Phú Quốc có trải nghiệm tốt nhất",
          "ShortContent": "Việc lựa chọn khách sạn Phú Quốc phù hợp sẽ giúp chuyến du lịch của bạn thêm phần trọn vẹn. Tham khảo kinh nghiệm từ Ví MoMo để chọn được khách sạn “đỉnh\" nhất với giá ưu đãi nhất bạn nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210518105230-637569319506172216.jpg",
          "Id": 215,
          "TotalView": 169,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "18/05/2021",
          "Date": "2021-05-18 10:49:51",
          "TotalWords": 3062,
          "TimeReadAvg": 11
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-da-lat-moi-nhat-a-den-z-c101dt209",
          "Title": "Bật mí kinh nghiệm du lịch Đà Lạt mới nhất 2021",
          "ShortContent": "Khám phá ngay trọn bộ kinh nghiệm du lịch Đà Lạt cùng Ví Momo, với hàng loạt những bí kíp siêu hay để bạn trải nghiệm “thả ga\" thành phố tình yêu.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210507155611-637559997715493700.jpg",
          "Id": 209,
          "TotalView": 564,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "12/05/2021",
          "Date": "2021-05-12 10:50:01",
          "TotalWords": 5271,
          "TimeReadAvg": 19
        }
      ],
      "TotalItems": 29,
      "Count": 10,
      "LastIdx": 10,
      "ExcludeIds": [
        235,
        227,
        229
      ]
    },
    "ListBlogFeatured": {
      "Items": [
        {
          "Link": "/blog/top-mon-ngon-phu-quoc-c101dt235",
          "Title": "Top 12 món ăn Phú Quốc không thử sẽ “tiếc hùi hụi\"",
          "ShortContent": "Du lịch Phú Quốc không chỉ nổi tiếng bởi các bãi biển xanh mát mà còn là những món ăn vô cùng tươi ngon. Cùng Ví MoMo “bỏ bụng” top 12 món ăn nhất định phải thử khi đến Phú Quốc nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210609123553-637588389532026697.jpg",
          "Id": 235,
          "TotalView": 13,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "1 giờ trước",
          "Date": "2021-06-09 12:40:00",
          "TotalWords": 1986,
          "TimeReadAvg": 7
        },
        {
          "Link": "/blog/top-khach-san-nha-trang-c101dt227",
          "Title": "Top 15 khách sạn Nha Trang cực xịn sò cùng Ví MoMo",
          "ShortContent": "Muốn chinh phục phố biển nhưng chưa biết bắt đầu từ đâu, khám phá 15 khách sạn Nha Trang “đỉnh của chóp” cùng Ví MoMo để có kì nghỉ tuyệt vời nhất nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210601140328-637581530087916026.jpg",
          "Id": 227,
          "TotalView": 9,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "2 giờ trước",
          "Date": "2021-06-09 11:44:54",
          "TotalWords": 2820,
          "TimeReadAvg": 10
        },
        {
          "Link": "/blog/kinh-nghiem-du-lich-tam-dao-c101dt229",
          "Title": "Trọn bộ kinh nghiệm du lịch Tam Đảo chi tiết nhất từ Ví MoMo",
          "ShortContent": "Được mệnh danh là Đà Lạt của miền Bắc với làn sương mờ ảo và tiết trời se lạnh, Tam Đảo là địa điểm “đi trốn” yêu thích của nhiều tín đồ du lịch. Bỏ túi kinh nghiệm du lịch Tam Đảo từ A tới Z cùng Ví MoMo và sẵn sàng tận hưởng một kỳ nghỉ tuyệt vời!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210609110157-637588333176049192.jpg",
          "Id": 229,
          "TotalView": 9,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "3 giờ trước",
          "Date": "2021-06-09 11:00:56",
          "TotalWords": 4574,
          "TimeReadAvg": 17
        }
      ],
      "TotalItems": 32,
      "Count": 3,
      "LastIdx": 3,
      "ExcludeIds": null
    },
    "ListBlogTopView": {
      "Items": [
        {
          "Link": "/blog/10-dia-diem-du-lich-dang-di-tai-viet-nam-trong-dip-tet-c101dt36",
          "Title": "10 địa điểm du lịch đáng đi tại Việt Nam trong dịp Tết",
          "ShortContent": "\"Tết nên đi du lịch ở đâu\" là câu hỏi thường gặp khi Tết 2021 đến gần. Đây là thời điểm chúng ta nghỉ ngơi sau một năm làm việc căng thẳng. Ngoài việc về quê thăm hỏi họ hàng thì thời gian còn lại mọi người sẽ chọn những nơi để du lịch cùng gia đình. Ví MoMo giới thiệu những địa điểm du lịch Tết Tân Sửu trong nước để bạn cùng gia đình khám phá nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201215170523-637436487231314598.jpg",
          "Id": 36,
          "TotalView": 34987,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "01/11/2020",
          "Date": "2020-11-01 11:37:00",
          "TotalWords": 1189,
          "TimeReadAvg": 4
        },
        {
          "Link": "/blog/danh-sach-10-bai-bien-dep-nhat-viet-nam-khong-the-bo-qua-c101dt145",
          "Title": "Danh sách 10 bãi biển đẹp nhất Việt Nam không thể bỏ qua",
          "ShortContent": "Với đường bờ biển dài hơn 3200 km, không có gì là ngạc nhiên khi Việt Nam có nhiều bãi biển đẹp cùng nhiều vịnh nhỏ với những bãi cát trắng xóa và làn nước trong xanh rất phù hợp cho những buổi ngắm bình minh, tiếp nạp “vitamin sea” hoặc ngắm cảnh hoàng hôn lúc chiều muộn, thật không có gì tuyệt vời hơn.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-200612174641-637275808017222444.jpg",
          "Id": 145,
          "TotalView": 33984,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "08/03/2021",
          "Date": "2021-03-08 14:18:30",
          "TotalWords": 1780,
          "TimeReadAvg": 6
        },
        {
          "Link": "/blog/top-dia-diem-vui-choi-dip-tet-2020-o-sai-gon-c101dt47",
          "Title": "Top địa điểm vui chơi dịp Tết Tân Sửu 2021 ở Sài Gòn",
          "ShortContent": "Nếu bạn muốn ngắm Sài Gòn với một không khí khác hẳn ngày thường thì hãy đến vào dịp Tết nhất định bạn sẽ không thất vọng.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-191225110008-637128684087001128.jpg",
          "Id": 47,
          "TotalView": 18689,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "02/01/2021",
          "Date": "2021-01-02 16:46:00",
          "TotalWords": 1513,
          "TimeReadAvg": 6
        },
        {
          "Link": "/blog/trai-nghiem-nhung-resort-sang-trong-lang-man-nhat-tai-da-lat-c101dt162",
          "Title": "Resort Đà Lạt đẹp lãng mạn, sang trọng để bạn nghỉ dưỡng và \"sống ảo\"",
          "ShortContent": "Review to resort Đà Lạt với view cực đẹp và thiết kế lãng mạn, sang trọng bật nhất ở thành phố ngàn hoa. Nếu mục đích của bạn là nghỉ dưỡng hay đơn giản chỉ là muốn trải nghiệm cảm giác bình yên bên những người thân yêu, thì ngay bây giờ hãy cùng Ví MoMo xem qua danh sách resort ấn tượng này để chuẩn bị cho chuyến du lịch sắp tới nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201215170605-637436487654876407.jpg",
          "Id": 162,
          "TotalView": 7030,
          "CategoryName": "Du lịch",
          "CategoryLink": "/blog/du-hi",
          "CategoryId": 101,
          "PublicDate": "01/04/2021",
          "Date": "2021-04-01 12:00:00",
          "TotalWords": 1666,
          "TimeReadAvg": 6
        }
      ],
      "TotalItems": 32,
      "Count": 4,
      "LastIdx": 4,
      "ExcludeIds": null
    },
    "Categories": [
      {
        "Name": "Phim ảnh",
        "Id": 99,
        "Link": "/blog/di-quay"
      },
      {
        "Name": "Ăn uống",
        "Id": 100,
        "Link": "/blog/them-an"
      },
      {
        "Name": "Du lịch",
        "Id": 101,
        "Link": "/blog/du-hi"
      },
      {
        "Name": "Mua sắm",
        "Id": 102,
        "Link": "/blog/mua-gi"
      },
      {
        "Name": "Chọn MoMo",
        "Id": 103,
        "Link": "/blog/chon-momo"
      },
      {
        "Name": "Game",
        "Id": 115,
        "Link": "/blog/game"
      }
    ],
    "Meta": {
      "Title": "Tổng hợp các kinh nghiệm du lịch từ A đến Z",
      "Description": "Chia sẻ các kinh nghiệm du lịch các địa điểm nổi tiếng. Danh sách các khách sạn, món ăn nổi tiếng để bạn trải nghiệm du lịch trọn vẹn cùng Ví MoMo.",
      "Keywords": "du lịch",
      "Url": null,
      "Avatar": "https://static.mservice.io/img/momo-upload-api-210429102306-637552885865683751.jpeg",
      "Scripts": null,
      "Robots": null,
      "RatingValue": 4.3,
      "RatingCount": 26
    }
  },
  "Error": null
};

export default (reg, res) => {
  res.status(200).json(data)
};