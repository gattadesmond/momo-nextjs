// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = {
  Result: true,
  Data: {
    Id: 245,
    Title: "Review The Boys: Khi siêu anh hùng là phản diện!",
    Content:
      '<div class="ckemm_ads d-none type1" data-ads-id="2" data-ads-type="1">\n\tAds Id:2 -> VÉ XEM PHIM CỰC RẺ TRÊN MOMO\n</div>\n\n<p>\n\tNếu trước đây bạn từng thắc mắc sẽ ra sao nếu siêu anh hùng thực ra không hề tốt bụng như vẻ bề ngoài và là các kẻ ác từ trong thâm tâm, The Boys sẽ là câu trả lời hoàn hảo cho việc đó.\n</p>\n\n<p>\n\tCâu chuyện trong The Boys bắt đầu bằng cái chết của Robin, bạn gái của anh chàng Hughie bị siêu anh hùng A-Train tông chết khi đang đứng sát trên lề đường, Hughie thậm chí còn chưa biết gì cho đến khi thấy mình đang nắm hai bàn tay “còn lại” của bạn gái. Hughie cảm thấy rất căm hận và khó lòng vượt qua được nỗi đau cho đến khi được Billy Butcher đến rủ rê tham gia một kế hoạch nhằm lật tẩy bọn siêu anh hùng đang có sức ảnh hưởng rất lớn tới nước Mỹ. Gia nhập chung nhóm với Butcher là anh chàng buôn súng người Pháp Frenchie và cựu đặc vụ FBI cơ bắp M.M (Mother’s Milk). \n</p>\n\n<p>\n\tBước ngoặt xảy ra khi cả nhóm bắt giữ được Translucent,  một thành viên trong The Seven-nhóm siêu anh hùng mạnh nhất nước Mỹ hiện nay được dẫn đầu bởi Homelander. Hughie thì lại có mối quan hệ với Starlight, thành viên nữ mới của The Seven được đắp vào thay thế Translucent khiến cho câu chuyện của phim càng trở nên rắc rối và phức tạp.\n</p>\n\n<h2 class="ckemm_title_spy" id="anh-hung-duoc-ca-tung">Mặt trái của những người hùng được cả thế giới ca tụng </h2>\n\n<p>\n\tĐiểm nhấn lớn nhất của The Boys chính là những màn đá xoáy siêu anh hùng được thể hiện rất rõ rệt. Siêu anh hùng giết người, siêu anh hùng đi giảng đạo theo Kinh Thánh tôn vinh các giá trị truyền thống thì đi đàn đúm với người đồng tính, siêu anh hùng dụ lính mới phục vụ tình dục… Ngay cả nhóm The Seven, chỉ cần nhìn sơ qua cũng biết là phim đang nhái lại các thành viên trong Justice League.Rõ rệt nhất là Homelander và Queen Maeve, hai phiên bản khác của Superman và Wonder Woman. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617102350-637595222309800896.jpg" style="width: 800px; height: 450px;"/>\n\t<strong>7 thành viên của The Seven.</strong>\n</p>\n\n<p>\n\t \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617102402-637595222426977934.jpg" style="width: 800px; height: 450px;"/>\n\t<em>Nhóm 5 thành viên The Boys.</em>\n</p>\n\n<p>\n\tThe Boys là câu trả lời cho thắc mắc rất lâu của nhiều fan siêu anh hùng là sẽ ra sao nếu siêu anh hùng thực ra chỉ là bọn biến chất và hoạt động vì tư lợi? Cách The Boys khai thác khía cạnh này rất độc đáo và xuất sắc, chưa một bộ phim nào tính tới thời điểm hiện tại có thể làm được. Toàn bộ các siêu anh hùng trên nước Mỹ đều hoạt động dưới trướng công ty Vought và thông qua đó, chúng ta sẽ thấy bọn siêu anh hùng đều làm mọi thứ để đạt được mục đích duy nhất: thu về lợi nhuận và tăng độ phủ sóng truyền thông của công ty. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617102411-637595222510829832.jpg" style="width: 800px; height: 594px;"/>\n\t<em>A Train sau khi tông bạn gái của Hughies.</em>\n</p>\n\n<p>\n\tTừ những khía cạnh đạo đức căn bản của một con người đến vấn đề chính trị, quốc phòng, an ninh và khủng bố đều được đem ra mổ xẻ trong The Boys. Mọi thứ ban đầu chỉ từ cái chết của Robin đã phát triển theo nhiều hướng không ngờ. Kịch bản của phim mang đến nhiều cú ngoặt bất ngờ đến tận tập cuối cùng, một nút thắt không thể sốc hơn. Tất cả đều gói gọn là đơn giản là những sự dối trá chồng dối trá nhằm đạt được tiền bạc và danh vọng.  \n</p>\n\n<h2 class="ckemm_title_spy" id="ca-tinh-nhan-vat">Cá tính nhân vật thú vị, đa chiều </h2>\n\n<p>\n\tCác thành viên trong nhóm The Boys và The Seven mỗi người có một tính cách riêng và được khắc họa rất rõ nét gây chú ý tới người xem. Trước tiên là bộ tứ nhân vật chính diện. \n</p>\n\n<p>\n\tHughie hiền lành, chân thật nhưng càng về sau càng cáo già hơn do được các anh em trong nhóm dạy dỗ tới nơi tới chốn. Anh vừa có thể chém gió, đi tống tiền và thậm chí là giết người (tình thế bắt buộc phải giết thôi).\n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617102449-637595222894473055.jpg" style="width: 800px; height: 533px;"/>\n\t<em>M.M và Frenchie xung đột.</em>\n</p>\n\n<p>\n\tBilly Butcher thì đúng chuẩn một thanh niên cục súc, bất cần, sẵn sàng thí anh em ra làm tốt để đạt được mục đích và nhìn đời bằng cặp kính râm không thể nào đen hơn. Từng câu, từng chữ Butcher thốt ra là đầy sự thù hận và mỉa mai những người xung quanh. Tất nhiên, vì sao Butcher lại căm ghét siêu anh hùng và biến thành con người thù hận như thế là có lý do cả, một bí ẩn chờ đón người xem khám phá vào những tập cuối cùng của mùa đầu. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617104221-637595233410030169.jpg" style="width: 800px; height: 534px;"/>\n\t<em>Ăn đường bằng mũi - Frenchie.</em>\n</p>\n\n<p>\n\tCòn cặp đôi M.M và Frenchie là cây hài đỉnh trong phim. M.M vốn bị mắc bệnh OCD nên làm gì cũng phải theo nguyên tắc còn Frenchie là trùm làm theo cảm tính. Thành ra khi cả hai đi chung thế nào cũng như chó với mèo chuẩn bị đồ sát lẫn nhau. Tuy nhiên, họ lại sống rất tình cảm và nâng đỡ tinh thần của Frenchie sau mỗi bị Butcher lên cơn sỉ vả. \n</p>\n\n<p>\n\tCòn với các thành viên của The Seven, đa phần chúng là tập hợp của một lũ ô hợp, chỉ sống trên danh vọng của bản thân, đầu óc đồi trụy. \n</p>\n\n<p>\n\tA-Train (Flash ởi vũ trụ nào đó), siêu anh hùng nhanh nhất hành tinh thì nghiện chất kích thích để có thể đạt được thành tích. \n</p>\n\n<p>\n\tTranslucent (Phiên bản nam của bộ tứ nào đó nhà M), khả năng tàng hình chuyên dùng để vô nhà vệ sinh nữ nhìn trộm. \n</p>\n\n<p>\n\tThe Deep (Aquaman phiên bản yếu đuối), gần như vô dụng nhất trong nhóm vì đầu óc chỉ nghĩ tới chuyện cứu cá heo bị nhốt trong sở thú và khi lên cạn thì coi như phế vật. \n</p>\n\n<p>\n\tQueen Maeve (Wonder Woman của vũ trụ D) còn lại một chút lương tri nhưng cô cũng không thể nào chống đối được mệnh lệnh từ Vought nhưng đặc biệt nguy hiểm nhất là Homelander. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617104240-637595233602397771.jpg" style="width: 800px; height: 450px;"/>\n\t<em>Homelander: bản sao gần hoàn hảo của Sup.</em>\n</p>\n\n<p>\n\tHắn là phiên bản độc ác của Superman, một tên giả nhân giả nghĩa bị rất nhiều người căm ghét nhưng không một ai có thể giết được hắn vì sức mạnh vô song của mình. Từng phân cảnh hắn xuất hiện cảm tưởng như hắn đã đạt tới đỉnh cao của sự nhẫn nhịn và sắp giết ai đó. Tất nhiên là hắn có giết người và khán giả sẽ cực kỳ ghê tởm nhân vật này vì sự hai mặt, xảo trá của hắn khi gieo niềm tin cho những người dân vô tội rồi để họ phải chết trong đau đớn và những tiếng gào thét tuyệt vọng. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617104354-637595234343860444.jpg" style="width: 800px; height: 421px;"/>\n\t<em>Starlight là siêu anh hùng duy nhất không dính tới hợp chất V.</em>\n</p>\n\n<p>\n\tStarlight là thành viên duy nhất trong The Seven không bị biến chất và có mối quan hệ khăng khít với Hughie. Chuyện tình của cặp đôi gặp phải nhiều trắc trở nhưng lại là động lực để cả hai phát triển tâm lý tốt hơn. Hughie không còn bị ám ảnh bởi cái chết của Robin quá nhiều nữa và anh tìm được cách mạnh mẽ, đứng lên chống lại những điều sai trái. Còn Starlight, cô không còn nhìn bọn siêu anh hùng với đôi mắt ngưỡng mộ năm nào nữa mà đã nếm trải sự cay đắng của cuộc đời để có thể trở thành một chiến binh thật sự, đấu tranh vì lẽ phải mà ở đây là chống lại chính đồng nghiệp của mình. \n</p>\n\n<h2 class="ckemm_title_spy" id="canh-bao-luc-dam-mau">Những cảnh phim bạo lực, đẫm máu trải dài xuyên suốt các tập phim </h2>\n\n<p>\n\tThe Boys có xếp loại độ tuổi R nên không ngại xài hết các chiêu trò máu me trong phim. Đủ thể loại giết người bạn khó có thể tưởng tượng nổi như nhét bom vào hậu môn, dùng mấy đứa bé siêu nhân để bắn laze cắt đôi kẻ thù, bóp nát tim… Kinh hoàng nhất là phân đoạn của nữ quái Kimiko. Đây là phiên bản nữ của Wolverine trong phim, trừ móng vuốt ra thì sức mạnh của cô gần như tương đồng. Kimiko giết người rất kinh khủng và dễ dàng hạ gục được 4 người đàn ông khỏe mạnh chỉ bằng tay không. \n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617104417-637595234576400265.jpg" style="width: 800px; height: 400px;"/>\n\t<em>Kimiko nhìn khá giống X-23.</em>\n</p>\n\n<p>\n\tĐiểm thú vị nhất trong phim là xem cách The Boys phải đối phó với bọn siêu anh hùng thế nào. Mỗi tên lại có đặc điểm riêng, sức mạnh riêng khiến cho cuộc chơi rất khó cân sức, có thể ví von như điển tích David và Goliath. Việc họ giành được chiến thắng thật sự là không tưởng và đầy bất ngờ. \n</p>\n\n<p>\n\tTuy nhiên, The Boys vẫn còn tồn đọng nhiều khuyết điểm trong câu chuyện. Nửa đầu khi đối đầu với Translucent thì mọi thứ vẫn còn hợp lý nhưng về sau, quy mô câu chuyện được mở rộng hơn khiến cho nhiều phân đoạn trở nên khá vô lý. Một vài nhân vật như Queen Mave, The Noir không đóng góp gì nhiều trong câu chuyện hay nhân vật Madelyn Stillwell có một kết cục không tương xứng với những gì bà đã thể hiện từ đầu season. Khó hiểu nhất là The Deep, đây chỉ là nhân vật có tính chất tấu hài nhưng không hiểu lại xuất hiện rất rất nhiều lần và thậm chí chiếm nhiều thời lượng ở tập cuối cùng. Có thể những nhân vật này sẽ là chủ chốt trong Mùa 2 này?\n</p>\n\n<p style="text-align: center;">\n\t<img src="https://static.mservice.io/blogscontents/momo-upload-api-210617104446-637595234862863083.png" style="width: 800px; height: 342px;"/>\n\t<em>Đây là cảnh đã trở thành meme huyền thoại của Mùa 1.</em>\n</p>\n\n<p>\n\tVới cách kể chuyện độc lạ, đoạn kết đầy bất ngờ thì The Boys chắc chắn là hiện tượng đột biến trong dòng phim siêu anh hùng. Mọi thứ trong phim đang dừng lại với một mớ hỗn độn chưa thể giải quyết được, hi vọng chúng ta sẽ được giải đáp hết trong Mùa 2 này. Nhưng phải nói, The Boys quả là một món ăn xuất sắc và một bộ phim đạt được đầy đủ điều kiện thiên thời-địa lợi-nhân hòa, khi khán giả sắp thế giới đã sắp bội thực vì những siêu anh hùng cứu người bằng cách phá hủy mọi thứ hay bọn phản diện với âm mưu hủy diệt Trái Đất.\n</p>\n\n<div class="ckemm_ads d-none type2" data-ads-id="2" data-ads-type="2">\n\tAds Id:2 -> VÉ XEM PHIM CỰC RẺ TRÊN MOMO\n</div>\n\n<p>\n\t<strong>Xem thêm:</strong>\n</p>\n\n<ul>\n\t<li><a href="https://momo.vn/blog/review-loki-2021-hanh-trinh-cua-vi-than-lua-loc-bac-nhat-dien-anh-marvel-c99dt239">Review Loki 2021: Hành trình của vị thần lừa lọc bậc nhất điện ảnh Marvel</a></li>\n\t<li><a href="https://momo.vn/blog/fast-9-va-nhung-diem-dac-sac-lam-nen-thanh-cong-series-phim-nay-c99dt230">Fast 9 và những điểm đặc sắc làm nên thành công series phim này</a></li>\n</ul>\n',
    Short:
      "Mùa 1 và 2 của The Boys đã ra được ra mắt trên Amazon Prime, nhưng sẽ là thiếu sót nếu bạn không xem từ Mùa 1 của bộ phim này. Cùng trở lại Mùa 1 để xem tại sao series này lại gây được tiếng vang trong mảng phim siêu anh hùng vốn đã bão hòa nhé!",
    CreatedBy: 0,
    PublishDate: "17/06/2021",
    Avatar:
      "https://static.mservice.io/blogscontents/momo-upload-api-210617102319-637595221993020525.jpg",
    Link: "/blog/review-the-boys-khi-sieu-anh-hung-la-phan-dien-c99dt245",
    TotalViews: 428,
    TotalWords: 1795,
    Type: 1,
    MenuItems: [
      {
        Id: "anh-hung-duoc-ca-tung",
        Title: "Mặt trái của những người hùng được cả thế giới ca tụng ",
      },
      {
        Id: "ca-tinh-nhan-vat",
        Title: "Cá tính nhân vật thú vị, đa chiều ",
      },
      {
        Id: "canh-bao-luc-dam-mau",
        Title:
          "Những cảnh phim bạo lực, đẫm máu trải dài xuyên suốt các tập phim ",
      },
    ],
    TimeReadAvg: 7.0,
    RelatingBlogs: {
      Items: [
        {
          Link: "/blog/review-loki-2021-hanh-trinh-cua-vi-than-lua-loc-bac-nhat-dien-anh-marvel-c99dt239",
          Title:
            "Review Loki 2021: Hành trình của vị thần lừa lọc bậc nhất điện ảnh Marvel",
          ShortContent:
            "Đối với các Fan của MCU thì TV Series Loki 2021 đang được mong chờ nhất trên nền tảng Streaming phim Disney+. Hãy cùng Ví MoMo trải nghiệm từng tập phim của Loki trong bài viết này được tóm gọn nhất nhé.",
          Avatar:
            "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210614135140-637592755005802668.jpg",
          Id: 239,
          TotalView: 1144,
          CategoryName: "Phim ảnh",
          CategoryLink: "/blog/di-quay",
          CategoryId: 99,
          PublicDate: "14/06/2021",
          Date: "2021-06-14 13:52:43",
          TotalWords: 3935,
          TimeReadAvg: 14.0,
        },
        {
          Link: "/blog/top-phim-kiem-hiep-xem-nhieu-nhat-c99dt233",
          Title: "Top phim kiếm hiệp Trung Quốc được nhiều người xem nhất",
          ShortContent:
            "Ví MoMo xin phép giới thiệu cho bạn danh sách phim kiếm hiệp hay nhất hiện nay mà chúng tôi đã thưởng thức, đánh giá và muốn chia sẻ đến các bạn dưới đây.",
          Avatar:
            "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210609142130-637588452902108365.jpg",
          Id: 233,
          TotalView: 1523,
          CategoryName: "Phim ảnh",
          CategoryLink: "/blog/di-quay",
          CategoryId: 99,
          PublicDate: "09/06/2021",
          Date: "2021-06-09 13:42:29",
          TotalWords: 1379,
          TimeReadAvg: 5.0,
        },
        {
          Link: "/blog/loat-phim-toi-hai-hay-nhat-tren-netflix-hien-nay-c99dt225",
          Title: "Loạt phim hài hay nhất trên Netflix hiện nay",
          ShortContent:
            "Ví MoMo xin phép giới thiệu cho bạn danh sách phim hài hay nhất trên Netflix mà chúng tôi đã trải nghiệm cũng như tham khảo khắp nơi.",
          Avatar:
            "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210531160440-637580738802237846.jpg",
          Id: 225,
          TotalView: 829,
          CategoryName: "Phim ảnh",
          CategoryLink: "/blog/di-quay",
          CategoryId: 99,
          PublicDate: "08/06/2021",
          Date: "2021-06-08 09:56:03",
          TotalWords: 1373,
          TimeReadAvg: 5.0,
        },
      ],
      TotalItems: 94,
      Count: 3,
      LastIdx: 3,
      ExcludeIds: null,
    },
    QaData: [],
    QaGroupData: [],
    AdsData: [
      {
        Id: 2,
        Name: "ĐẶT VÉ XEM PHIM TRÊN VÍ MOMO",
        Content: "<p>\n\tĐặt vé trực tiếp, thanh toán xong ngay.\n</p>\n",
        BtnName: "ĐẶT VÉ XEM PHIM NGAY",
        BtnLink: "/mua-ve-xem-phim-tai-vi-momo",
        BtnNewTab: true,
        Avatar:
          "https://static.mservice.io/img/momo-upload-api-210618140725-637596220453237033.png",
      },
    ],
    CtaData: [],
    Category: {
      Id: 99,
      Name: "Phim ảnh",
      Slug: "di-quay",
      Link: "/blog/di-quay",
    },
    Meta: {
      Title: "Review The Boys: Khi siêu anh hùng là phản diện!",
      Description:
        "Mùa 1 và 2 của The Boys đã ra được ra mắt trên Amazon Prime. Cùng trở lại Mùa 1 để xem tại sao series này lại gây được tiếng vang trong mảng phim siêu anh hùng vốn đã bão hòa nhé!",
      Keywords: "Review The Boys: Khi siêu anh hùng là phản diện!",
      Url: "/blog/review-the-boys-khi-sieu-anh-hung-la-phan-dien-c99dt245",
      Avatar:
        "https://static.mservice.io/blogscontents/momo-upload-api-210617102319-637595221993020525.jpg",
      Scripts: null,
      Robots: null,
      RatingValue: 5.0,
      RatingCount: 16,
      Date: "2021-06-17 10:21:22",
    },
    BreadCrumbs: [
      {
        Title: "MoMo",
        Url: "https://momo.vn",
      },
      {
        Title: "Blog",
        Url: "https://momo.vn/blog",
      },
      {
        Title: "Phim ảnh",
        Url: "https://momo.vn/blog/di-quay",
      },
      {
        Title: "Review The Boys: Khi siêu anh hùng là phản diện!",
        Url: "https://momo.vn/blog/review-the-boys-khi-sieu-anh-hung-la-phan-dien-c99dt245",
      },
    ],
  },
  Error: null,
};

export default (req, res) => {
  res.status(200).json(data);
};
