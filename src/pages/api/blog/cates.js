const data = {
  "Result": true,
  "Data": [
    {
      "Name": "Phim ảnh",
      "Id": 99,
      "Link": "/blog-test/di-quay"
    },
    {
      "Name": "Ăn uống",
      "Id": 100,
      "Link": "/blog-test/them-an"
    },
    {
      "Name": "Du lịch",
      "Id": 101,
      "Link": "/blog-test/du-hi"
    },
    {
      "Name": "Mua sắm",
      "Id": 102,
      "Link": "/blog-test/mua-gi"
    },
    {
      "Name": "Chọn MoMo",
      "Id": 103,
      "Link": "/blog-test/chon-momo"
    },
    {
      "Name": "Game",
      "Id": 115,
      "Link": "/blog-test/game"
    }
  ],
  "Error": null
};

export default (reg, res) => {
  res.status(200).json(data)
};