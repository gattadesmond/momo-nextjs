// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = {
  "Result": true,
  "Data": {
    "ServicePageIds": {
      "Home": 13
    },
    "Cities": [
      {
        "Id": 50,
        "ApiId": 50,
        "Name": "TP.HCM",
        "UrlRewrite": "tp-ho-chi-minh",
        "Code": "HCM",
        "Lat": 10.823099,
        "Lon": 106.629664,
        "DisplayOrder": 1
      },
      {
        "Id": 1,
        "ApiId": 1,
        "Name": "Hà Nội",
        "UrlRewrite": "ha-noi",
        "Code": "HAN",
        "Lat": 21.027764,
        "Lon": 105.83416,
        "DisplayOrder": 2
      },
      {
        "Id": 57,
        "ApiId": 57,
        "Name": "An Giang",
        "UrlRewrite": "an-giang",
        "Code": "AGG",
        "Lat": 10.521584,
        "Lon": 105.125896,
        "DisplayOrder": 10
      },
      {
        "Id": 49,
        "ApiId": 49,
        "Name": "Bà Rịa - Vũng Tàu",
        "UrlRewrite": "ba-ria-vung-tau",
        "Code": "BVT",
        "Lat": 10.54174,
        "Lon": 107.242998,
        "DisplayOrder": 10
      },
      {
        "Id": 15,
        "ApiId": 15,
        "Name": "Bắc Giang",
        "UrlRewrite": "bac-giang",
        "Code": "BGG",
        "Lat": 21.301495,
        "Lon": 106.62913,
        "DisplayOrder": 10
      },
      {
        "Id": 5,
        "ApiId": 5,
        "Name": "Bắc Kạn",
        "UrlRewrite": "bac-kan",
        "Code": "BKN",
        "Lat": 22.303292,
        "Lon": 105.876004,
        "DisplayOrder": 10
      },
      {
        "Id": 62,
        "ApiId": 62,
        "Name": "Bạc Liêu",
        "UrlRewrite": "bac-lieu",
        "Code": "BLU",
        "Lat": 9.251556,
        "Lon": 105.513647,
        "DisplayOrder": 10
      },
      {
        "Id": 18,
        "ApiId": 18,
        "Name": "Bắc Ninh",
        "UrlRewrite": "bac-ninh",
        "Code": "BNH",
        "Lat": 21.121444,
        "Lon": 106.11105,
        "DisplayOrder": 10
      },
      {
        "Id": 53,
        "ApiId": 53,
        "Name": "Bến Tre",
        "UrlRewrite": "ben-tre",
        "Code": "BTE",
        "Lat": 10.108155,
        "Lon": 106.440587,
        "DisplayOrder": 10
      },
      {
        "Id": 35,
        "ApiId": 35,
        "Name": "Bình Định",
        "UrlRewrite": "binh-dinh",
        "Code": "BDH",
        "Lat": 14.166532,
        "Lon": 108.902683,
        "DisplayOrder": 10
      },
      {
        "Id": 47,
        "ApiId": 47,
        "Name": "Bình Dương",
        "UrlRewrite": "binh-duong",
        "Code": "BDG",
        "Lat": 11.325402,
        "Lon": 106.477017,
        "DisplayOrder": 10
      },
      {
        "Id": 45,
        "ApiId": 45,
        "Name": "Bình Phước",
        "UrlRewrite": "binh-phuoc",
        "Code": "BPC",
        "Lat": 11.751189,
        "Lon": 106.723464,
        "DisplayOrder": 10
      },
      {
        "Id": 39,
        "ApiId": 39,
        "Name": "Bình Thuận",
        "UrlRewrite": "binh-thuan",
        "Code": "BTN",
        "Lat": 11.09037,
        "Lon": 108.072078,
        "DisplayOrder": 10
      },
      {
        "Id": 63,
        "ApiId": 63,
        "Name": "Cà Mau",
        "UrlRewrite": "ca-mau",
        "Code": "CMU",
        "Lat": 9.152673,
        "Lon": 105.196079,
        "DisplayOrder": 10
      },
      {
        "Id": 59,
        "ApiId": 59,
        "Name": "Cần Thơ",
        "UrlRewrite": "can-tho",
        "Code": "CTO",
        "Lat": 10.045162,
        "Lon": 105.746854,
        "DisplayOrder": 10
      },
      {
        "Id": 3,
        "ApiId": 3,
        "Name": "Cao Bằng",
        "UrlRewrite": "cao-bang",
        "Code": "CBG",
        "Lat": 22.635689,
        "Lon": 106.252214,
        "DisplayOrder": 10
      },
      {
        "Id": 32,
        "ApiId": 32,
        "Name": "Đà Nẵng",
        "UrlRewrite": "da-nang",
        "Code": "DAN",
        "Lat": 16.054407,
        "Lon": 108.202167,
        "DisplayOrder": 10
      },
      {
        "Id": 42,
        "ApiId": 42,
        "Name": "Đắk Lắk",
        "UrlRewrite": "dak-lak",
        "Code": "DLC",
        "Lat": 12.710012,
        "Lon": 108.237752,
        "DisplayOrder": 10
      },
      {
        "Id": 43,
        "ApiId": 43,
        "Name": "Đắk Nông",
        "UrlRewrite": "dak-nong",
        "Code": "DNG",
        "Lat": 12.264648,
        "Lon": 107.609806,
        "DisplayOrder": 10
      },
      {
        "Id": 7,
        "ApiId": 7,
        "Name": "Điện Biên",
        "UrlRewrite": "dien-bien",
        "Code": "DBN",
        "Lat": 21.804231,
        "Lon": 103.107652,
        "DisplayOrder": 10
      },
      {
        "Id": 48,
        "ApiId": 48,
        "Name": "Đồng Nai",
        "UrlRewrite": "dong-nai",
        "Code": "DNI",
        "Lat": 11.06863,
        "Lon": 107.167598,
        "DisplayOrder": 10
      },
      {
        "Id": 56,
        "ApiId": 56,
        "Name": "Đồng Tháp",
        "UrlRewrite": "dong-thap",
        "Code": "DTP",
        "Lat": 10.493799,
        "Lon": 105.688179,
        "DisplayOrder": 10
      },
      {
        "Id": 41,
        "ApiId": 41,
        "Name": "Gia Lai",
        "UrlRewrite": "gia-lai",
        "Code": "GLI",
        "Lat": 13.807894,
        "Lon": 108.109375,
        "DisplayOrder": 10
      },
      {
        "Id": 2,
        "ApiId": 2,
        "Name": "Hà Giang",
        "UrlRewrite": "ha-giang",
        "Code": "HAG",
        "Lat": 22.766206,
        "Lon": 104.938885,
        "DisplayOrder": 10
      },
      {
        "Id": 23,
        "ApiId": 23,
        "Name": "Hà Nam",
        "UrlRewrite": "ha-nam",
        "Code": "HNM",
        "Lat": 20.58352,
        "Lon": 105.92299,
        "DisplayOrder": 10
      },
      {
        "Id": 28,
        "ApiId": 28,
        "Name": "Hà Tĩnh",
        "UrlRewrite": "ha-tinh",
        "Code": "HTH",
        "Lat": 18.294378,
        "Lon": 105.674525,
        "DisplayOrder": 10
      },
      {
        "Id": 19,
        "ApiId": 19,
        "Name": "Hải Dương",
        "UrlRewrite": "hai-duong",
        "Code": "HDG",
        "Lat": 20.937341,
        "Lon": 106.314554,
        "DisplayOrder": 10
      },
      {
        "Id": 20,
        "ApiId": 20,
        "Name": "Hải Phòng",
        "UrlRewrite": "hai-phong",
        "Code": "HPG",
        "Lat": 20.844911,
        "Lon": 106.688084,
        "DisplayOrder": 10
      },
      {
        "Id": 58,
        "ApiId": 58,
        "Name": "Hậu Giang",
        "UrlRewrite": "hau-giang",
        "Code": "HGG",
        "Lat": 9.757898,
        "Lon": 105.641253,
        "DisplayOrder": 10
      },
      {
        "Id": 11,
        "ApiId": 11,
        "Name": "Hòa Bình",
        "UrlRewrite": "hoa-binh",
        "Code": "HBH",
        "Lat": 20.686127,
        "Lon": 105.313118,
        "DisplayOrder": 10
      },
      {
        "Id": 21,
        "ApiId": 21,
        "Name": "Hưng Yên",
        "UrlRewrite": "hung-yen",
        "Code": "HYN",
        "Lat": 20.852571,
        "Lon": 106.016997,
        "DisplayOrder": 10
      },
      {
        "Id": 37,
        "ApiId": 37,
        "Name": "Khánh Hòa",
        "UrlRewrite": "khanh-hoa",
        "Code": "KHA",
        "Lat": 12.25851,
        "Lon": 109.052608,
        "DisplayOrder": 10
      },
      {
        "Id": 60,
        "ApiId": 60,
        "Name": "Kiên Giang",
        "UrlRewrite": "kien-giang",
        "Code": "KGG",
        "Lat": 9.824959,
        "Lon": 105.125896,
        "DisplayOrder": 10
      },
      {
        "Id": 40,
        "ApiId": 40,
        "Name": "Kon Tum",
        "UrlRewrite": "kon-tum",
        "Code": "KTM",
        "Lat": 14.34974,
        "Lon": 108.000461,
        "DisplayOrder": 10
      },
      {
        "Id": 9,
        "ApiId": 9,
        "Name": "Lai Châu",
        "UrlRewrite": "lai-chau",
        "Code": "LCU",
        "Lat": 22.368661,
        "Lon": 103.311909,
        "DisplayOrder": 10
      },
      {
        "Id": 44,
        "ApiId": 44,
        "Name": "Lâm Đồng",
        "UrlRewrite": "lam-dong",
        "Code": "LDG",
        "Lat": 11.575279,
        "Lon": 108.142867,
        "DisplayOrder": 10
      },
      {
        "Id": 12,
        "ApiId": 12,
        "Name": "Lạng Sơn",
        "UrlRewrite": "lang-son",
        "Code": "LSN",
        "Lat": 21.853708,
        "Lon": 106.761519,
        "DisplayOrder": 10
      },
      {
        "Id": 6,
        "ApiId": 6,
        "Name": "Lào Cai",
        "UrlRewrite": "lao-cai",
        "Code": "LCI",
        "Lat": 22.338086,
        "Lon": 104.148706,
        "DisplayOrder": 10
      },
      {
        "Id": 51,
        "ApiId": 51,
        "Name": "Long An",
        "UrlRewrite": "long-an",
        "Code": "LAN",
        "Lat": 10.695572,
        "Lon": 106.243121,
        "DisplayOrder": 10
      },
      {
        "Id": 25,
        "ApiId": 25,
        "Name": "Nam Định",
        "UrlRewrite": "nam-dinh",
        "Code": "NDH",
        "Lat": 20.27918,
        "Lon": 106.205148,
        "DisplayOrder": 10
      },
      {
        "Id": 27,
        "ApiId": 27,
        "Name": "Nghệ An",
        "UrlRewrite": "nghe-an",
        "Code": "NAN",
        "Lat": 19.234249,
        "Lon": 104.920036,
        "DisplayOrder": 10
      },
      {
        "Id": 24,
        "ApiId": 24,
        "Name": "Ninh Bình",
        "UrlRewrite": "ninh-binh",
        "Code": "NBH",
        "Lat": 20.250615,
        "Lon": 105.974454,
        "DisplayOrder": 10
      },
      {
        "Id": 38,
        "ApiId": 38,
        "Name": "Ninh Thuận",
        "UrlRewrite": "ninh-thuan",
        "Code": "NTN",
        "Lat": 11.673877,
        "Lon": 108.862957,
        "DisplayOrder": 10
      },
      {
        "Id": 16,
        "ApiId": 16,
        "Name": "Phú Thọ",
        "UrlRewrite": "phu-tho",
        "Code": "PTO",
        "Lat": 21.268443,
        "Lon": 105.204557,
        "DisplayOrder": 10
      },
      {
        "Id": 36,
        "ApiId": 36,
        "Name": "Phú Yên",
        "UrlRewrite": "phu-yen",
        "Code": "PYN",
        "Lat": 13.088186,
        "Lon": 109.092876,
        "DisplayOrder": 10
      },
      {
        "Id": 29,
        "ApiId": 29,
        "Name": "Quảng Bình",
        "UrlRewrite": "quang-binh",
        "Code": "QBH",
        "Lat": 17.610271,
        "Lon": 106.348747,
        "DisplayOrder": 10
      },
      {
        "Id": 33,
        "ApiId": 33,
        "Name": "Quảng Nam",
        "UrlRewrite": "quang-nam",
        "Code": "QNM",
        "Lat": 15.539354,
        "Lon": 108.019102,
        "DisplayOrder": 10
      },
      {
        "Id": 34,
        "ApiId": 34,
        "Name": "Quảng Ngãi",
        "UrlRewrite": "quang-ngai",
        "Code": "QNI",
        "Lat": 15.539354,
        "Lon": 108.019102,
        "DisplayOrder": 10
      },
      {
        "Id": 14,
        "ApiId": 14,
        "Name": "Quảng Ninh",
        "UrlRewrite": "quang-ninh",
        "Code": "QNH",
        "Lat": 21.006382,
        "Lon": 107.292514,
        "DisplayOrder": 10
      },
      {
        "Id": 30,
        "ApiId": 30,
        "Name": "Quảng Trị",
        "UrlRewrite": "quang-tri",
        "Code": "QTI",
        "Lat": 16.794347,
        "Lon": 106.963409,
        "DisplayOrder": 10
      },
      {
        "Id": 61,
        "ApiId": 61,
        "Name": "Sóc Trăng",
        "UrlRewrite": "soc-trang",
        "Code": "STG",
        "Lat": 9.600369,
        "Lon": 105.959954,
        "DisplayOrder": 10
      },
      {
        "Id": 8,
        "ApiId": 8,
        "Name": "Sơn La",
        "UrlRewrite": "son-la",
        "Code": "SLA",
        "Lat": 21.102228,
        "Lon": 103.728917,
        "DisplayOrder": 10
      },
      {
        "Id": 46,
        "ApiId": 46,
        "Name": "Tây Ninh",
        "UrlRewrite": "tay-ninh",
        "Code": "TNH",
        "Lat": 11.335155,
        "Lon": 106.109885,
        "DisplayOrder": 10
      },
      {
        "Id": 22,
        "ApiId": 22,
        "Name": "Thái Bình",
        "UrlRewrite": "thai-binh",
        "Code": "TBH",
        "Lat": 20.538694,
        "Lon": 106.393478,
        "DisplayOrder": 10
      },
      {
        "Id": 13,
        "ApiId": 13,
        "Name": "Thái Nguyên",
        "UrlRewrite": "thai-nguyen",
        "Code": "TNN",
        "Lat": 21.567156,
        "Lon": 105.825204,
        "DisplayOrder": 10
      },
      {
        "Id": 26,
        "ApiId": 26,
        "Name": "Thanh Hóa",
        "UrlRewrite": "thanh-hoa",
        "Code": "THA",
        "Lat": 19.806692,
        "Lon": 105.785182,
        "DisplayOrder": 10
      },
      {
        "Id": 31,
        "ApiId": 31,
        "Name": "Thừa Thiên Huế",
        "UrlRewrite": "thua-thien-hue",
        "Code": "TTH",
        "Lat": 16.467397,
        "Lon": 107.590533,
        "DisplayOrder": 10
      },
      {
        "Id": 52,
        "ApiId": 52,
        "Name": "Tiền Giang",
        "UrlRewrite": "tien-giang",
        "Code": "TGG",
        "Lat": 10.449332,
        "Lon": 106.34205,
        "DisplayOrder": 10
      },
      {
        "Id": 54,
        "ApiId": 54,
        "Name": "Trà Vinh",
        "UrlRewrite": "tra-vinh",
        "Code": "TVH",
        "Lat": 9.812741,
        "Lon": 106.299291,
        "DisplayOrder": 10
      },
      {
        "Id": 4,
        "ApiId": 4,
        "Name": "Tuyên Quang",
        "UrlRewrite": "tuyen-quang",
        "Code": "TQG",
        "Lat": 22.172671,
        "Lon": 105.313118,
        "DisplayOrder": 10
      },
      {
        "Id": 55,
        "ApiId": 55,
        "Name": "Vĩnh Long",
        "UrlRewrite": "vinh-long",
        "Code": "VLG",
        "Lat": 10.239574,
        "Lon": 105.957193,
        "DisplayOrder": 10
      },
      {
        "Id": 17,
        "ApiId": 17,
        "Name": "Vĩnh Phúc",
        "UrlRewrite": "vinh-phuc",
        "Code": "VPC",
        "Lat": 21.360881,
        "Lon": 105.547437,
        "DisplayOrder": 10
      },
      {
        "Id": 10,
        "ApiId": 10,
        "Name": "Yên Bái",
        "UrlRewrite": "yen-bai",
        "Code": "YBI",
        "Lat": 21.683792,
        "Lon": 104.455136,
        "DisplayOrder": 10
      }
    ],
    "Cineplexs": [
      {
        "Id": 1,
        "Name": "LOTTE_VOUCHER",
        "Code": "LOTTE_VOUCHER",
        "Code2": "booking_lotte",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/blogscontents/momo-upload-api-210604170617-637584231772974269.png"
      },
      {
        "Id": 2,
        "Name": "GALAXY",
        "Code": "GALAXY",
        "Code2": "booking_galaxy",
        "DisplayOrder": null,
        "Logo": "https://img.mservice.io/momo_app_v2/new_version/img/icon/GalaxyCinema.png"
      },
      {
        "Id": 3,
        "Name": "BHD",
        "Code": "BHD",
        "Code2": "booking_bhd",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/blogscontents/momo-upload-api-210604170453-637584230934981809.png"
      },
      {
        "Id": 4,
        "Name": "MEGA_GS",
        "Code": "MEGA_GS",
        "Code2": "booking_megags",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/blogscontents/momo-upload-api-210604170511-637584231119272266.png"
      },
      {
        "Id": 5,
        "Name": "CINESTAR",
        "Code": "CINESTAR",
        "Code2": "booking_cinestar",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/blogscontents/momo-upload-api-210604170530-637584231309495829.png"
      },
      {
        "Id": 6,
        "Name": "CGV",
        "Code": "CGV",
        "Code2": "booking_cgv",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/img/momo-upload-api-210724114327-637627238075191078.png"
      },
      {
        "Id": 7,
        "Name": "DCINE",
        "Code": "DCINE",
        "Code2": "booking_dcine",
        "DisplayOrder": null,
        "Logo": "https://img.mservice.io/momo_app_v2/new_version/img/THAO.MAI/DcineLogo.png"
      },
      {
        "Id": 8,
        "Name": "LOTTE_TICKET",
        "Code": "LOTTE_TICKET",
        "Code2": "booking_lotte_ticket",
        "DisplayOrder": null,
        "Logo": "https://static.mservice.io/blogscontents/momo-upload-api-210604170617-637584231772974269.png"
      },
      {
        "Id": 9,
        "Name": "GALAXY_V2",
        "Code": "GALAXY_V2",
        "Code2": "booking_galaxy_v2",
        "DisplayOrder": null,
        "Logo": "https://img.mservice.io/momo_app_v2/new_version/img/icon/GalaxyCinema.png"
      }
    ]
  },
  "Error": null
}

export default (req, res) => {
  res.status(200).json(data);
};
