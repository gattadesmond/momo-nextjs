const data = { "Result": true, "Data": { "Id": 319, "Type": 1, "Stars": 4.0, "Count": 10, "Value": 4.5 }, "Error": null };

export default (req, res) => {
  res.status(200).json(data);
};
