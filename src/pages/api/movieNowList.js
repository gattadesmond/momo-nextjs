// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

const data = {
  user: "",
  momoUser: "",
  result: true,
  errorCode: 0,
  errorDesc: "Giao dịch thành công",
  data: {
    time: 1622109898900,
    statusCode: 200,
    success: true,
    message: "Success!",
    data: {
      filmsShowing: [
        {
          id: 9,
          Link: "/cinema/phim/cgv-the-doorman-9",
          createDate: 0,
          lastUpdate: 1622045530090,
          status: 3,
          filmData:
            '[{"filmId":"20014600","cineplex":6,"title":"The Doorman","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20014300","cineplex":6,"title":"TIEC TRANG MAU","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20014400","cineplex":6,"title":"QUAI DAN","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20008500","cineplex":6,"title":"Rom","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20004000","cineplex":6,"title":"8 - A South African Horror Story","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20013600","cineplex":6,"title":"The Pawn","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20010600","cineplex":6,"title":"Tenet","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"21000400","cineplex":6,"title":"Phim moi James ","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20013700","cineplex":6,"title":"Honest Thief","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20014700","cineplex":6,"title":"The Croods: A New Age","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20014200","cineplex":6,"title":"Tsunami At Haeundae (Rerun)","captionType":-1,"versionType":-1,"isClosed":false}]',
          title: "[CGV] The Doorman",
          viTitle: "[CGV] Chiến Binh Hồi Sinh",
          rating: "C16",
          synopsis:
            "Chiến Binh Hồi Sinh bắt đầu khi nữ quân nhân Ali trở về quê hương New York để hồi phục những chấn thương trong thời gian phục vụ quân ngũ. Được người thân nhờ làm gác cửa tại một căn biệt thự chứa nhiều tài sản quý giá, Ali vô tình trở thành người bảo vệ nơi này trước kế hoạch tấn công được tính toán kỹ lưỡng của một nhóm trộm chuyên nghiệp.",
          viSynopsis: null,
          duration: 97,
          openingDate: 1621962000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17447126216413349-agQxKYlHe81iIbpmJHkaQcuB2kE.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17447126324480652-9pHxv7TX0jOKNgnGMDP6RJ2m1GL.jpg",
          filmType: "2D",
          genreName: "Gây Cấn, Hành Động",
          rating: "C16",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":1,"sneakShowDate":1621530000000,"tmdbId":"737568","imdbId":"tt6222118","imdbScore":"4.4","metacriticScore":"41","tagline":"Truy tận gốc, giết tận cùng.","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/23154971466761492-CGV_750x420.mp4","posters":"[]","backdrops":"[]","trailers":"[]","casts":"[{\\"id\\":\\"5f771374d18fb900388dba87\\",\\"name\\":\\"Rupert Evans\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/27326618002061426-YrsdSUjinm0NN6qd8Y2znPf8Cg.jpg\\",\\"character\\":\\"Jon Stanton\\"},{\\"id\\":\\"5f7713a2d18fb900358e9387\\",\\"name\\":\\"Julian Feder\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/27326618018495308-dfhAZETTKwyqGuKxhOTU5JjS8EA.jpg\\",\\"character\\":\\"Max\\"},{\\"id\\":\\"5f78dd27d4d50900340fccdc\\",\\"name\\":\\"David Sakurai\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/27326618018972119-kC0o7PEqRLb6iciL7Y3MjMxh1dw.jpg\\",\\"character\\":\\"Andre\\"}]","crew":"[{\\"id\\":\\"5f496256f6596f0035b12a33\\",\\"name\\":\\"Ryûhei Kitamura\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/27326618042013340-iitzR6lbdD1sCK2LOoG8Li1Cg30.jpg\\",\\"job\\":\\"Director\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","totalMedal":4,"total":5,"per":80},"cityNowShowing":[]}',
        },
        {
          id: 10,
          Link: "/cinema/phim/godzilla-vs-kong-10",
          createDate: 0,
          lastUpdate: 1621498549575,
          status: 3,
          filmData:
            '[{"filmId":"10445","momoFilmId":"","cineplex":8,"captionType":-1,"versionType":2,"title":"LẬT MẶT 48H","cineplexEnum":"LOTTE_TICKET","isClosed":false},{"filmId":"HO00002270","momoFilmId":"","cineplex":3,"captionType":2,"versionType":2,"title":"GODZILLA VS. KONG","cineplexEnum":"BHD","isClosed":true},{"filmId":"20014400","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"Freaky","cineplexEnum":"CGV","isClosed":false},{"filmId":"HO00002238","momoFilmId":"","cineplex":2,"captionType":2,"versionType":2,"title":"Godzilla Đại Chiến Kong","cineplexEnum":"GALAXY","isClosed":true},{"filmId":"HO00001993","momoFilmId":"","cineplex":2,"captionType":2,"versionType":2,"title":"Josee: Khi Nàng Thơ Yêu","cineplexEnum":"GALAXY","isClosed":true}]',
          title: "Godzilla vs. Kong",
          viTitle: "Godzilla Đại Chiến Kong",
          rating: "C13",
          synopsis:
            "Khi hai kẻ thù truyền kiếp gặp nhau trong một trận chiến ngoạn mục, số phận của cả thế giới vẫn còn bị bỏ ngỏ… Bị đưa khỏi Đảo Đầu Lâu, Kong cùng Jia, một cô bé mồ côi có mối liên kết mạnh mẽ với mình và đội bảo vệ đặc biệt hướng về mái nhà mới. Bất ngờ, nhóm đụng độ phải Godzilla hùng mạnh, tạo ra một làn sóng hủy diệt trên toàn cầu. Thực chất, cuộc chiến giữa hai kẻ khổng lồ dưới sự thao túng của các thế lực vô hình mới chỉ là điểm khởi đầu để khám phá những bí ẩn nằm sâu trong tâm Trái đất.",
          viSynopsis: null,
          duration: 113,
          openingDate: 1622394000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457412438188335-ayKhR40ClDEivfmubfcXeyNPo2r.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=yFpuUGFS1Kg",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457413162597355-iopYFB1b6Bh7FWZh3onQhph1sih.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Khoa Học Viễn Tưởng, Hành Động",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":1,"sneakShowDate":1621530000000,"keywords":"creature feature, giant ape, monsterverse, giant monster, king kong, kaiju, godzilla","featuredNote":"N/A","tmdbId":"399566","imdbId":"tt5034838","rottenTomatoesScore":"92%","tagline":"Quyết phân thắng bại.","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457411294572663-8iiu1VQjcH7fjKgfzVd0hlOlHXQ.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457411294482829-wWqTMWkEw6HouLd1zPZbZWxtAPr.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457411294551345-pBscXLwZg9rmYCp6HfiIF19WGH0.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457411294587196-yOnWwGjRJsb4tHHzk6t5TuiOE3L.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=yFpuUGFS1Kg\\"]","casts":"[{\\"id\\":\\"5bd27583c3a3687437003310\\",\\"name\\":\\"Alexander Skarsgård\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457411050607660-aVAPqwoElxk9O8QID96l6qG4x2E.jpg\\",\\"character\\":\\"Dr. Nathan Lind\\"},{\\"id\\":\\"5b154f5dc3a368533000d528\\",\\"name\\":\\"Millie Bobby Brown\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457412093534405-yzfxLMcBMusKzZp9f1Z9Ags8WML.jpg\\",\\"character\\":\\"Madison Russell\\"},{\\"id\\":\\"5bd9b264925141156c00e024\\",\\"name\\":\\"Rebecca Hall\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457412094379322-cVZaQrUY7F5khCBYdKDlEppHnQi.jpg\\",\\"character\\":\\"Dr. Ilene Andrews\\"},{\\"id\\":\\"5bbeafae9251413d51006008\\",\\"name\\":\\"Brian Tyree Henry\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457412096399468-1h4sYFAc1inxcV0Ljrl5v2mMskI.jpg\\",\\"character\\":\\"Bernie Hayes\\"},{\\"id\\":\\"5be890c90e0a263c0a031146\\",\\"name\\":\\"Shun Oguri\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/22457412094329119-4tfrhvqp3IGHPATor0lYE9X9UD3.jpg\\",\\"character\\":\\"Ren Serizawa\\"}]","cityNowShowing":[50]}',
        },
        {
          id: 11,
          Link: "/cinema/phim/cgv-cuc-no-hoa-cuc-cung-11",
          createDate: 0,
          lastUpdate: 1619499041343,
          status: 3,
          filmData:
            '[{"filmId":"20013600","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"Pawn","cineplexEnum":"CGV","isClosed":true},{"filmId":"20014400","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"Freaky","cineplexEnum":"CGV","isClosed":true}]',
          title: "[CGV] Cục Nợ Hóa Cục Cưng",
          viTitle: "[CGV] Cục Nợ Hóa Cục Cưng",
          rating: "P",
          synopsis:
            'Cục Nợ Hóa Cục Cưng là câu chuyện về một gia đình “kì lạ” lấy bối cảnh vào năm 1993 ở Incheon. Du-seok và Jong-bae là hai gã chuyên đòi nợ thuê có máu mặt. Để uy hiếp một con nợ, cả hai đã giữ Seung-yi - một bé gái 9 tuổi làm vật thế chấp cho số nợ của mẹ cô bé. Tuy nhiên, mẹ của Seung-yi lại bị trục xuất, và hai ông chú đành nhận trách nhiệm trông chừng Seung-yi đến khi cô bé được một gia đình giàu có và tử tế nhận nuôi. Khi phát hiện ra Seung-yi nhỏ bé bị bán đi làm công cho một bà chủ vô trách nhiệm, Du-seok và Jong-bae đã tìm đến để chuộc lại cô. Mặc dù Seung-yi vốn là "cục nợ" Du-seok và Jong-bae không hề mong muốn, cô bé dần trở thành cục cưng yêu quý và cả 3 sống bên nhau như một gia đình.',
          viSynopsis: null,
          duration: 113,
          openingDate: 1619802000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17447190361605245-w5r3T9sGpg9WIdJkqzUX66VDACN.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17447190229185875-htyMJhFpau8goMMLQoUXyLZf6cs.jpg",
          filmType: null,
          genreName: "Hài",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":1,"sneakShowDate":1619456400000,"keywords":"loan shark, money-lender, 1990s, incheon","featuredNote":"N/A","tmdbId":"589174","imdbId":"tt13273172","imdbScore":"7.4","metacriticScore":"N/A","posters":"[]","backdrops":"[]","trailers":"[]","rating":{"id":"54d3f158-e29c-4e6d-8e8c-af6044ab3bba","name":"Thích Tet","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15809675890551732-like.png","totalMedal":4,"total":4,"per":100},"cityNowShowing":[]}',
        },
        {
          id: 12,
          Link: "/cinema/phim/mega-gs-avengers-infinity-war-12",
          createDate: 0,
          lastUpdate: 1620287607173,
          status: 3,
          filmData:
            '[{"filmId":"HO00000066","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D - HUNGER GAME: MOCKING JAY P.2","cineplexEnum":"MEGA_GS","isClosed":true},{"filmId":"HO00000128","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D ALVIN AND THE CHIPMUNKS: ROAD CHIP","cineplexEnum":"MEGA_GS","isClosed":true},{"filmId":"HO00000164","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D BATMAN VS. SUPERMAN [NC16]","cineplexEnum":"MEGA_GS","isClosed":true},{"filmId":"HO00000189","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D CAPTAIN AMERICA: CIVIL WAR [NC16]","cineplexEnum":"MEGA_GS","isClosed":true},{"filmId":"HO00000211","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D DORAEMON: NOBITA AND THE BIRTH OF JAPAN 16","cineplexEnum":"MEGA_GS","isClosed":true}]',
          title: "[Mega GS] Avengers: Infinity War",
          viTitle: "[GLX - V2] Biệt Đội Siêu Anh Hùng: Cuộc Chiến Vô Cực",
          rating: "C13",
          synopsis:
            "Sau chuyến hành trình độc nhất vô nhị không ngừng mở rộng và phát triển vụ trũ điện ảnh Marvel, bộ phim Avengers: Cuộc Chiến Vô Cực sẽ mang đến màn ảnh trận chiến cuối cùng khốc liệt nhất mọi thời đại. Biệt đội Avengers và các đồng minh siêu anh hùng của họ phải chấp nhận hy sinh tất cả để có thể chống lại kẻ thù hùng mạnh Thanos trước tham vọng hủy diệt toàn bộ vũ trụ của hắn.",
          viSynopsis: null,
          duration: 149,
          openingDate: 0,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17002222114715836-333.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=O4XbKp6kN2k",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980414394188183-lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
          filmType: "2D, 3D",
          genreName: "Khoa Học Viễn Tưởng, Phiêu Lưu, Hành Động",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":2,"keywords":"magic, marvel cinematic universe, cosmic, based on comic, aftercreditsstinger, superhero, genocide, sacrifice, team, space, magical object, super power, battlefield","featuredNote":"Nominated for 1 Oscar, Another 46 wins & 75 nominations","tmdbId":"299536","imdbId":"tt4154756","imdbScore":"8.4","rottenTomatoesScore":"85%","metacriticScore":"68","tagline":"Avengers: Infinity War","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=O4XbKp6kN2k\\"]","casts":"[{\\"id\\":\\"54a9cfa29251414d5b00553d\\",\\"name\\":\\"Robert Downey Jr.\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980413330925878-im9SAqJPZKEbVZGmjXuLI4O7RvM.jpg\\",\\"character\\":\\"Tony Stark / Iron Man\\"},{\\"id\\":\\"54a9d012c3a3680c29005762\\",\\"name\\":\\"Chris Hemsworth\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980413336409384-qMIay6aytplhE1J8aeGnfYtrtRi.jpg\\",\\"character\\":\\"Thor Odinson\\"},{\\"id\\":\\"54a9cfc0c3a3680c2900575e\\",\\"name\\":\\"Chris Evans\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980413402156045-3bOGNsHlrswhyW79uvIHH1V43JI.jpg\\",\\"character\\":\\"Steve Rogers / Captain America\\"},{\\"id\\":\\"581f354cc3a36860b9005213\\",\\"name\\":\\"Scarlett Johansson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980413344743382-6NsMbJXRlDZuDzatN2akFdGuTvx.jpg\\",\\"character\\":\\"Natasha Romanoff / Black Widow\\"},{\\"id\\":\\"5921f8ab92514149f305d2d0\\",\\"name\\":\\"Benedict Cumberbatch\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15980413350607178-fBEucxECxGLKVHBznO0qHtCGiMO.jpg\\",\\"character\\":\\"Stephen Strange / Doctor Strange\\"}]","cityNowShowing":[]}',
        },
        {
          id: 13,
          Link: "/cinema/phim/cgv-wonder-woman-1984-13",
          createDate: 0,
          lastUpdate: 1619164492527,
          status: 3,
          filmData: "[]",
          title: "[CGV] Wonder Woman 1984",
          viTitle: "[CGV] Wonder Woman 1984",
          rating: "C13",
          synopsis:
            "Lấy bối cảnh năm 1984, 66 năm sau sự kiện diễn ra Thế Chiến thứ I (1918) ở phần phim đầu tiên, Wonder Woman tái hợp với người yêu tưởng chừng đã qua đời Steve Trevor, đồng thời đương đầu với hai kẻ thù mới là Max Lord và The Cheetah.",
          viSynopsis: null,
          duration: 151,
          openingDate: 1608310800000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533323886757020-sNUCQ07nnIhZWUoUssT67uvfyD7.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=BIhNsAtPbPI",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533324009109028-srYya1ZlI97Au4jUYAktDe3avyA.jpg",
          filmType: "2D, 3D, IMAX",
          genreName: "Giả Tượng,Phiêu Lưu,Hành Động",
          averageScore: null,
          extras:
            '{"bigBanner":true,"featuredFilm":true,"bigBannerPrior":3,"typeface":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533323176406572-3625955253366630-candyman-5e57f618689b9.png","sneakShowDate":1607533200000,"keywords":"based on comic, action hero, 1980s, dc extended universe, dc comics","featuredNote":"N/A","tmdbId":"464052","imdbId":"tt7126948","imdbScore":"N/A","rottenTomatoesScore":"70%","metacriticScore":"N/A","tagline":"A new era of wonder begins.","blurredPoster":"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3626042978545847-t9Tt3MpfG3HZUcv9CtP5NPrzf3b.jpg","autoplayTrailer":"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3626078637195269-y2meta.com%20-%20S%C3%A1t%20Nh%C3%A2n%20Trong%20G%C6%B0%C6%A1ng%20official%20trailer%20-%20Moveek_%20L%E1%BB%8Bch%20chi%E1%BA%BFu%20%26%20Mua%20v%C3%A9%20phim%20to%C3%A0n%20qu%E1%BB%91c.mp4","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=BIhNsAtPbPI\\",\\"https://www.youtube.com/watch?v=vw2FOYjCz38\\"]","casts":"[{\\"id\\":\\"5657775bc3a3681970009dfa\\",\\"name\\":\\"Daniel Craig\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032474837875-12703600164030614-12703589986515121-12703522302963826-11756125889108113-11669141445375946-11409458788137388-10778055747346039-10189749176289180-9136981390554597-9052482674528521-7945809775928105-7938893221457215-iFerDZUmC5Fu26i4qI8xnUVEHc7.jpg\\",\\"character\\":\\"James Bond\\"},{\\"id\\":\\"5cc1a8150e0a26105af8741b\\",\\"name\\":\\"Rami Malek\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032534376297-12703600164396790-12703589965397032-12703522303063380-11756125883679865-11669141433888467-11409458790551839-10778055740215969-10189749181505425-9136981391888689-9052482642232825-7945809780147756-7938893228975600-zvBCjFmedqXRqa45jlLf6vBd9Nt.jpg\\",\\"character\\":\\"Lyutsifer Safin\\"},{\\"id\\":\\"5c09b51092514147950299e0\\",\\"name\\":\\"Léa Seydoux\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032591519951-12703600243186978-12703590033401568-12703522303104828-11756125946042452-11669141466202247-11409458807074207-10778055764088792-10189749248446237-9136981402440941-9052482662055741-7945809791434734-7938893228411419-7JAUieStGsHZAy6ed2WuFy4CJjm.jpg\\",\\"character\\":\\"Dr. Madeleine Swann\\"},{\\"id\\":\\"5cc1a7f7c3a368145886aa8d\\",\\"name\\":\\"Lashana Lynch\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032558816921-12703600227952194-12703589975629233-12703521966152288-11756125880585875-11669141438984063-11409458786941643-10778055743479827-10189749228878629-9136981388710681-9052482647469310-7945809761361593-7938893228469173-8KueSCFErJEx5gsNCdyQlk3s32d.jpg\\",\\"character\\":\\"Nomi\\"},{\\"id\\":\\"5cc1a7e2c3a368776c8d2cab\\",\\"name\\":\\"Ana de Armas\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032599923756-12703600232262229-12703589987364763-12703522303088050-11756125877099319-11669141434644730-11409458802034369-10778055772462563-10189749231911668-9136981383509981-9052482650245609-7945809784677597-7938893481801520-8F6ziFZaK71L3rFeDIc4aAkPZPv.jpg\\",\\"character\\":\\"Paloma\\"},{\\"id\\":\\"5ac753160e0a264965026141\\",\\"name\\":\\"Ben Whishaw\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032617945467-12703600243090143-12703590059032238-12703522383293048-11756125975477263-11669141534145845-11409458860222406-10778055874797296-10189749311294328-9136981475200426-9052482729169812-7945809834986590-7938893249765863-2GBtQ6scGeSHkX1urOP1EJbmksx.jpg\\",\\"character\\":\\"Q\\"}]","crew":"[{\\"id\\":\\"5e1493fed8af670016c1d287\\",\\"name\\":\\"Hans Zimmer\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032650774107-12703600251882298-12703590062562093-12703522459786454-11756125963977334-11669141513579181-11409458870648504-10778055866435000-10189749272656530-9136981472023484-9052482729884332-7945809847478974-7938893248834655-tpQnDeHY15szIXvpnhlprufz4d.jpg\\",\\"job\\":\\"Original Music Composer\\"},{\\"id\\":\\"5e5d3f5c9b8616000ffcc91b\\",\\"name\\":\\"Daniel Craig\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032644318567-12703600296650738-12703590062933564-12703522399689561-11756125973058389-11669141523060556-11409458858507937-10778055828768148-10189749366452798-9136981454279509-9052482786862816-7945809846846001-7938893246496702-iFerDZUmC5Fu26i4qI8xnUVEHc7.jpg\\",\\"job\\":\\"Co-Producer\\"},{\\"id\\":\\"5f05f28ddbf144003509f1dd\\",\\"name\\":\\"Ian Fleming\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13315032690565183-12703600297030551-12703590071529808-12703522380966369-11756126015214989-11669141524295960-11409458879210088-10778055829593800-10189749331604216-9136981465303059-9052482740526780-7945809846179799-7938893252266108-94Dq1VuPjv66wYwu78rtvMDuCgn.jpg\\",\\"job\\":\\"Novel\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":22,"per":86,"totalMedal":19},"cityNowShowing":[]}',
        },
        {
          id: 14,
          Link: "/cinema/phim/glx-v2-mat-biec-14",
          createDate: 0,
          lastUpdate: 1620618338653,
          status: 3,
          filmData:
            '[{"filmId":"77eabc6c-d038-4a5b-87d4-5f23b110dbd2","cineplex":9,"title":"PHÚT KINH HOÀNG TẠI EL ROYALE","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"14b5e620-8ad4-4877-9c1b-7945c95c5397","cineplex":9,"title":"VÙNG ĐẤT CÂM LẶNG","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"3e4be638-af62-4b77-b7a3-4473e65944fc","cineplex":9,"title":"100% WOLF (2D - VIET.DUB)","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"80f6c95d-af3f-42f3-aec9-14964be725d7","cineplex":9,"title":"Mắt Biếc","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"53bbb4c6-3556-4e7e-8765-049e012af211","cineplex":9,"title":"AVENGERS: CUỘC CHIẾN VÔ CỰC","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"3ca9a5ef-a9e8-4ab1-bf0d-94d866fc409e","cineplex":9,"title":"DOLITTLE","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"dc9ae43d-0375-44fb-b8a8-474933eb8366","cineplex":9,"title":"8: A SOUTH AFRICAN HORROR STORY","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"5d8778a5-be00-4fdd-a9e6-9a7ce847e5e9","cineplex":9,"title":"ANH TRAI YEU QUAI","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"e552e65c-1a3b-4eea-b207-d6202d49fe79","cineplex":9,"title":"ANGEL HAS FALLEN","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"9ea75ade-3c66-4155-bf07-d0fbb83e75e7","cineplex":9,"title":"GHOST WRITER","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"fc3618d6-a731-477c-9831-d09583f6fd8f","cineplex":9,"title":"BANG CHUNG VO HINH","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"c9043289-8d73-4d51-95b4-99e7e8052fea","cineplex":9,"title":"BLACK CHRISTMAS","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"2d2e964c-ee14-4a32-9f09-dba06121b78a","cineplex":9,"title":"A QUIET PLACE PART II","captionType":-1,"versionType":-1,"isClosed":false}]',
          title: "[GLX - V2] Mắt Biếc",
          viTitle: "[GLX - V2] Mắt Biếc",
          rating: "P",
          synopsis:
            "Đạo diễn Victor Vũ trở lại với một tác phẩm chuyển thể từ truyện ngắn cùng tên nổi tiếng của nhà văn Nguyễn Nhật Ánh: Mắt Biếc. Phim kể về chuyện tình đơn phương của chàng thanh niên Ngạn dành cho cô bạn từ thuở nhỏ Hà Lan.",
          viSynopsis: null,
          duration: 117,
          openingDate: 1620752400000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17002013490480116-111.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15275812656190231-DwTyJZKMH7rcJFDpQ5X3fZs6K9.jpg",
          filmType: "2D",
          genreName: "Chính Kịch, Lãng Mạn",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":3,"sneakShowDate":1620579600000,"featuredNote":"N/A","tmdbId":"618010","imdbId":"tt10622410","imdbScore":"N/A","metacriticScore":"N/A","tagline":"Tình đầu, một thời cứ ngỡ một đời","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24884688889653207-Trangti_750x420%20%281%29.mp4","posters":"[]","backdrops":"[]","trailers":"[]","casts":"[{\\"id\\":\\"5d38583eab68492d5784bade\\",\\"name\\":\\"Trúc Anh\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15275812567871495-i4ngGFyXaF3uR154cB3Q8fj4HIp.jpg\\",\\"character\\":\\"Hà Lan\\"},{\\"id\\":\\"5d385857ab68497d7d87da51\\",\\"name\\":\\"Trần Nghĩa\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15275812556867890-7IhXofvkbDCO4THRm4NLxQgbjqq.jpg\\",\\"character\\":\\"Ngạn\\"},{\\"id\\":\\"5df90aa8609750001230a6cd\\",\\"name\\":\\"Đỗ Khánh Vân\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15275812549174400-v2GMkhOem8J7YKUaSjrLyTSvLnF.jpg\\",\\"character\\":\\"Trà Long\\"},{\\"id\\":\\"5df90b0c65686e001589cd82\\",\\"name\\":\\"Trần Phong\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15275812551358426-yyo21x77snrp3YtWvSABubTD5R0.jpg\\",\\"character\\":\\"Dũng\\"}]","rating":{"id":"54d3f158-e29c-4e6d-8e8c-af6044ab3bba","name":"Thích Tet","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15809675890551732-like.png","total":4,"per":100,"totalMedal":4},"cityNowShowing":[]}',
        },
        {
          id: 15,
          Link: "/cinema/phim/glxv1-dragon-rider-15",
          createDate: 0,
          lastUpdate: 1620632574232,
          status: 3,
          filmData:
            '[{"filmId":"HO00002252","cineplex":2,"title":"Ong Nhí Phiêu Lưu Kí: Giải Cứu Công Chúa Kiến","captionType":1,"versionType":2,"isClosed":false},{"filmId":"80f6c95d-af3f-42f3-aec9-14964be725d7","cineplex":9,"title":"Mắt Biếc","captionType":-1,"versionType":-1,"isClosed":false}]',
          title: "[GLX-V1] Dragon Rider",
          viTitle: "[GLX-V1] Dragon Rider",
          rating: "P",
          synopsis:
            "An unlikely trio of heroes – a dragon, a boy and a forest brownie – embark on an epic adventure to find the “Rim of Heaven” - the mythological safe haven for all dragons.",
          viSynopsis: null,
          duration: 100,
          openingDate: 1607533200000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533886934358326-7HFnV5JFTcOBr38zmxQjQQ8RATa.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=9ihTMGouLms",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533887273756871-494bVeWKJ3DlCRHQu7kaPyskou9.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Hoạt Hình, Phiêu Lưu, Gia Đình",
          averageScore: null,
          extras:
            '{"bigBanner":true,"featuredFilm":true,"bigBannerPrior":4,"keywords":"anime","featuredNote":"N/A","tmdbId":"523366","imdbId":"tt7080422","imdbScore":"7.1","metacriticScore":"N/A","blurredPoster":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14498273240262784-test.jpg","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=9ihTMGouLms\\"]","casts":"[{\\"id\\":\\"595686e4c3a368382e050da4\\",\\"name\\":\\"Gal Gadot\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512840820014-14253833550729535-13991006085016422-13990951857480941-13494855143244633-901564823841054-901087626042461-901017526514796-896655509763669-896496068456129-895732346639369-12873908995539276-12871963288024456-12871934868224133-aDWrs64fKhIyqeIMPTAlK4UlDbZ.jpg\\",\\"character\\":\\"Diana Prince / Wonder Woman\\"},{\\"id\\":\\"5b0b4526c3a3684adc0097a5\\",\\"name\\":\\"Chris Pine\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512833108249-14253833550697719-13991006088399215-13990951846479935-13494855138852388-901564845813960-901087633824135-901017523894904-896655516051558-896496073641792-895732342602605-12873909047240051-12871963307924471-12871934882788716-ipG3BMO8Ckv9xVeEY27lzq975Qm.jpg\\",\\"character\\":\\"Steve Trevor\\"},{\\"id\\":\\"5a975236c3a36861510077f1\\",\\"name\\":\\"Kristen Wiig\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512832412824-14253833550637371-13991006089633059-13990951848769605-13494855143019449-901564821360175-901087641516353-901017546005909-896655593320100-896496069696757-895732349663496-12873909078441496-12871963343468824-12871934893846420-oddvykQHx71hEZlvKinCzB3Vcfh.jpg\\",\\"character\\":\\"Barbara Ann Minerva / Cheetah\\"},{\\"id\\":\\"5abc31e99251411ea701aa27\\",\\"name\\":\\"Pedro Pascal\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512839467407-14253833555974756-13991006086644736-13990951846247587-13494855143820633-901564831274824-901087639765818-901017517353424-896655520947542-896496076894463-895732350600139-12873909056018579-12871963355326633-12871934883379436-wROJBhRvazeFl1SIWfzwMcKrYYn.jpg\\",\\"character\\":\\"Maxwell Lord\\"},{\\"id\\":\\"5b8ad4080e0a261d6801d975\\",\\"name\\":\\"Connie Nielsen\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512834845450-14253833548961784-13991006092142905-13990951841765087-13494855147520787-901564832082746-901087651279695-901017524400528-896655519810267-896496052425185-895732363852560-12873909093544494-12871963384699988-12871934886200036-gSQ3O3PJ6ly6nT63joOtfZyscFP.jpg\\",\\"character\\":\\"Queen Hippolyta\\"},{\\"id\\":\\"5b8ad4190e0a261d6801d99e\\",\\"name\\":\\"Robin Wright\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512894283733-14253833628357640-13991006151779558-13990951935173806-13494855226720426-901564892124507-901087702270542-901017607875706-896655622044539-896496131854051-895738359343717-12873909102194361-12871963358741935-12871934892609807-bU0IKL6kX4xiCQmsZNhVq8hEy6A.jpg\\",\\"character\\":\\"Antiope\\"},{\\"id\\":\\"5b5b80039251415234003cb9\\",\\"name\\":\\"Gabriella Wilde\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512894951864-14253833619331105-13991006156211771-13990951938849410-13494855332960798-901564888761261-901087712290677-901017599151241-896655587700701-896496138237363-895738368995115-12873909127870508-12871963368011476-12871934907788242-4ElIAtrcM1kY1ieWgH9T3dUUBA6.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5b57d2edc3a3685c9103e642\\",\\"name\\":\\"Natasha Rothwell\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512895715176-14253833628490180-13991006159067898-13990951926663211-13494855282461172-901564894597363-901087759380570-901017605948783-896655596354869-896496162477958-895738362977539-12873909179630363-12871963414152328-12871934904381523-x5KdL3QoS4YuozVpfuPsu3MLwwf.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5b5b7fe5c3a36842270047d3\\",\\"name\\":\\"Ravi Patel\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512898902975-14253833629747410-13991006165162402-13990951942705747-13494855275196880-901564901273484-901087722393157-901017617254400-896655598547638-896496152579731-895738345832943-12873909137329061-12871963420721533-12871934902233638-94GutLAPx72fqxe6XUQ3HWxANxz.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5bcb66b792514155a1001186\\",\\"name\\":\\"Oakley Bull\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512924039043-14253833632044200-13991006158241997-13990951983072093-13494855297496526-901564918081714-901087730042254-901017615615794-896655665473970-896496164934611-895738352005566-12873909194238047-12871963447184069-12871934912342334-dr2gXPMavXaJ9At9QuvCeh2Ifyn.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5e6fca282f3b17001446607d\\",\\"name\\":\\"Bern Collaço\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512955914775-14253833711805381-13991006210617740-13990952014157716-13494855303528538-901570407908648-901087798763031-901017681357983-896655661111207-896496209736483-895738517795913-12873909165707196-12871963435016683-12871934908711088-ziLGGjo5GWzYDL8H4MUquoFj8r0.jpg\\",\\"character\\":\\"Gala Guest\\"},{\\"id\\":\\"5ece332ee4b57600202a42f5\\",\\"name\\":\\"Chuck Taber\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14413512953732683-14253833986860246-13991006219868534-13990952068305486-13494855359566037-901570398233754-901087815764913-901017705378305-896655671713962-896496214505253-895738512090392-12873909211644037-12871963446260463-12871934924721917-3Da2e0pDetKApfWkWr1R7bZmVhQ.jpg\\",\\"character\\":\\"Rioter / Driver\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":8,"per":88,"totalMedal":7},"cityNowShowing":[]}',
        },
        {
          id: 16,
          Link: "/cinema/phim/glx-v2-bad-times-at-the-el-royale-16",
          createDate: 0,
          lastUpdate: 1619083898673,
          status: 3,
          filmData:
            '[{"filmId":"10444","cineplex":8,"title":"Trạng Tí","captionType":-1,"versionType":2,"isClosed":false},{"filmId":"10445","cineplex":8,"title":"LẬT MẶT 48H","captionType":-1,"versionType":2,"isClosed":false},{"filmId":"10318","cineplex":8,"title":"NẮNG 3: LỜI HỨA CỦA CHA","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"20014200","cineplex":6,"title":"Tsunami At Haeundae (Rerun)","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"53bbb4c6-3556-4e7e-8765-049e012af211","cineplex":9,"title":"AVENGERS: CUỘC CHIẾN VÔ CỰC","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"80f6c95d-af3f-42f3-aec9-14964be725d7","cineplex":9,"title":"Mắt Biếc","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"77eabc6c-d038-4a5b-87d4-5f23b110dbd2","cineplex":9,"title":"PHÚT KINH HOÀNG TẠI EL ROYALE","captionType":-1,"versionType":-1,"isClosed":false}]',
          title: "[GLX - V2] Bad Times at the El Royale",
          viTitle: "[GLX - V2] Phút Kinh Hoàng Tại El Royale",
          rating: "C16",
          synopsis:
            "Bad Times at the El Royale lấy bối cảnh El Royale – một khách sạn sang trọng và lâu đời tại nơi giao nhau giữa California và Nevada. Một ngày nọ, chàng lễ tân trẻ tuổi chào đón cùng lúc sáu vị khách bí ẩn. Họ gồm mục sư Daniel Flynn, nữ ca sĩ Darlene Sweet, hai chị em nhà Summerspring , tay bán máy hút hút bụi Laramie Seymour Sullivan và thủ lĩnh giáo phái bí ẩn Billy Lee . Song, tất cả chỉ là danh tính giả khi mỗi người đều có một bí mật đen tối muốn chôn giấu.  Họ dường như đều có liên quan đến một vụ cướp xe chở tiền và án mạng bí ẩn. Không những thế, kịch tính càng được đẩy lên cao trào khi Laramie Seymour Sullivan có thể quan sát tất cả các phòng thông qua một con đường hầm bí mật và hé lộ rằng hắn ta đang làm việc với một thế lực có tên “Ban quản lý”. Bảy kẻ xa lạ tìm kiếm theo dõi và giết hại lẫn nhau, ai sẽ người chiến thắng? Tổ chức bí ẩn kia là gì? Danh tính thật sự của kẻ thủ ác là ai? Vụ cướp kia và âm mưu của từng người là gì?",
          viSynopsis: null,
          duration: 142,
          openingDate: 1611162000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/17001892869809129-30797927443946348-Untitled-1%20%282%29%20%281%29.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/16493066091878312-uyM09BML2F05VpJoXtf0tza44A4.jpg",
          filmType: "2D",
          genreName: "Gây Cấn, Chính Kịch, Bí Ẩn",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":5,"sneakShowDate":1611162000000,"keywords":"ruse, storm, jukebox, vacuum cleaner, lake tahoe, hippie, casino, richard nixon, j edgar hoover, audio tape, motel, motel room, neo-noir, payphone, vietnam war, post traumatic stress  disorder, slot machine, 1960s, concierge","featuredNote":"2 wins & 9 nominations","tmdbId":"446021","imdbId":"tt6628394","imdbScore":"7.1","rottenTomatoesScore":"74%","metacriticScore":"60","tagline":"All roads lead here","blurredPoster":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21079165586677278-fGHE8bvRz1yxI7nZDEq5RVYCjng.jpg","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/20136550881057806-yt1s.com%20-%20Godzilla%20vs%20Kong%20%20Official%20Trailer.mp4","posters":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21079196154445696-fGHE8bvRz1yxI7nZDEq5RVYCjng.jpg\\"]","backdrops":"[]","trailers":"[]","cityNowShowing":[50]}',
        },
        {
          id: 17,
          Link: "/cinema/phim/bhd-the-crucified-lovers-17",
          createDate: 0,
          lastUpdate: 1620654812658,
          status: 3,
          filmData:
            '[{"filmId":"HO00001371","momoFilmId":"","cineplex":3,"captionType":2,"versionType":2,"title":"A STORY FROM CHIKAMATSU","cineplexEnum":"BHD","isClosed":true}]',
          title: "[BHD] The Crucified Lovers",
          viTitle: "[BHD] The Crucified Lovers",
          rating: "C16",
          synopsis:
            "In 17th century Kyoto, Osan is married to Ishun, a wealthy miserly scroll-maker. When Osan is falsely accused of having an affair with the best worker, Mohei, the pair flee the city and declare their love for each other. Ishun orders his men to find them, and separate them to avoid public humiliation.",
          viSynopsis: null,
          duration: 102,
          openingDate: 1619802000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533646183552001-2FQkHxqdXVD7B02XLaI62UZhWnK.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533645963189131-aiEMx1ND6Gq5uFYWRHektj5CrSs.jpg",
          filmType: "2D",
          genreName: "Chính Kịch, Lãng Mạn",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":6,"typeface":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15533645208622117-3611173059258165-train-to-busan-581c8fd4496d6.png","sneakShowDate":1619456400000,"keywords":"love, kyoto japan, extramarital affair, attempted suicide, lover, locked in, social class, bankruptcy, escape, 17th century","featuredNote":"1 win & 1 nomination","tmdbId":"35861","imdbId":"tt0046851","imdbScore":"8.0","rottenTomatoesScore":"100%","certifiedFresh":true,"metacriticScore":"N/A","tagline":"The tortured heart behind the cultivated image.","awards":"Oscar","autoplayTrailer":"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3611273609527681-y2meta.com%20-%20CHUY%E1%BA%BEN%20T%C3%80U%20SINH%20T%E1%BB%AC%20-%20Train%20To%20Busan%20-%20Trailer%20Ch%C3%ADnh%20Th%E1%BB%A9c%20%28Kh%E1%BB%9Fi%20chi%E1%BA%BFu%20t%E1%BB%AB%2012_8_2016%29.mp4","posters":"[]","backdrops":"[]","trailers":"[]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":18,"per":61,"totalMedal":11},"cityNowShowing":[50]}',
        },
        {
          id: 18,
          Link: "/cinema/phim/lotte-nang-3-loi-hua-cua-cha-18",
          createDate: 0,
          lastUpdate: 1619601632159,
          status: 3,
          filmData:
            '[{"filmId":"10318","cineplex":8,"title":"NẮNG 3: LỜI HỨA CỦA CHA","captionType":-1,"versionType":-1,"isClosed":false},{"filmId":"10444","cineplex":8,"title":"Trạng Tí","captionType":-1,"versionType":2,"isClosed":false},{"filmId":"10445","cineplex":8,"title":"LẬT MẶT 48H","captionType":-1,"versionType":2,"isClosed":false},{"filmId":"10442","cineplex":8,"title":"Black Widow","captionType":-1,"versionType":2,"isClosed":false}]',
          title: "[LOTTE] Nắng 3: Lời Hứa Của Cha",
          viTitle: "[LOTTE] Nắng 3: Lời Hứa Của Cha",
          rating: "P",
          synopsis:
            "Lời Hứa Của Cha kể về cuộc sống của 2 mẹ con lừa đảo Quế Phương và bé Hồng Ân. Mọi thứ trở nên đảo lộn khi 2 mẹ con vô tình lừa phải bác sĩ Tùng Sơn, người có khả năng là cha mà bé Hồng Ân bấy lâu đang tìm kiếm. Hành trình chinh phục người cha bất đắc dĩ của 2 mẹ con không hề suôn sẻ khi gặp phải chướng ngại đáng gờm là Thùy Linh, người yêu hiện tại của Sơn. Số phận nghiệt ngã còn trêu đùa và thử thách tình cha con hơn nữa khi đặt Hồng Ân vào tình huống hiểm nghèo.",
          viSynopsis: null,
          duration: 90,
          openingDate: 1620666000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622359314741556-nn9daCeo4XF9upAAcNvFRhfgjmc.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=0hAL7emClFM",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/16595319728873341-1BRdu3b7f40psgkXHZeXIXTVAuD.jpg",
          filmType: "2D, 3D, IMAX",
          genreName: "Hài, Gia Đình",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":8,"sneakShowDate":1619456400000,"tmdbId":"676834","tagline":"Cha tính không bằng con tính.","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=0hAL7emClFM\\"]","casts":"[{\\"id\\":\\"5a70e6059251413757015d2f\\",\\"name\\":\\"Keanu Reeves\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15024680276951633-5522374399933602-d9HyjGMCt4wgJIOxAGlaYWhKsiN.jpg\\",\\"character\\":\\"Ted Logan\\"},{\\"id\\":\\"5a70e60e9251413742016149\\",\\"name\\":\\"Alex Winter\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15024680300891764-5522374405158950-3kZFRlc8zvfvziJIc7HxY803HXi.jpg\\",\\"character\\":\\"Bill S. Preston\\"},{\\"id\\":\\"5d958e4c35818f0174d64b96\\",\\"name\\":\\"Kristen Schaal\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15024680300843807-5522374400754597-s3LSHVTx8gxHP2twYsXEGa8JbLl.jpg\\",\\"character\\":\\"Kelly\\"},{\\"id\\":\\"5d0ebaca0e0a2627fecd03bc\\",\\"name\\":\\"Samara Weaving\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15024680300913251-5522374408908861-5MzsIWtOKnTRYQ8fBpFCDgwMqNF.jpg\\",\\"character\\":\\"Thea Preston\\"},{\\"id\\":\\"5d0ebade925141779dbbb2de\\",\\"name\\":\\"Brigette Lundy-Paine\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15024680300929142-5522374403324929-88nq2n9o1e9Qjfz6rmbvfi184W.jpg\\",\\"character\\":\\"Billie Logan\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":12,"per":75,"totalMedal":9},"cityNowShowing":[50]}',
        },
        {
          id: 19,
          Link: "/cinema/phim/freaky-19",
          createDate: 0,
          lastUpdate: 1617641575954,
          status: 3,
          filmData:
            '[{"filmId":"20014400","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"Freaky","cineplexEnum":"CGV","isClosed":true}]',
          title: "Freaky",
          viTitle: "[CGV] Quái Đản",
          rating: "C16",
          synopsis:
            "Bộ phim có nội dung nói về một nữ sinh trung học đã hoán đổi thân xác với một tên giết người hàng loạt. Trong vòng 24 giờ, sự thay đổi này sẽ trở thành vĩnh viễn, liệu cô gái phải làm thế nào?",
          viSynopsis: null,
          duration: 102,
          openingDate: 1615827600000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170797306153191-8xC6QSyxrpm0D5A6iyHNemEWBVe.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=_6bSFQex-DY",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170797318053131-eShw0LB5CkoEfYtpUcXPD85oz5Q.jpg",
          filmType: "2D",
          genreName: "Hài, Kinh Dị, Giả Tượng, Gây Cấn",
          averageScore: null,
          extras:
            '{"featuredFilm":true,"keywords":"body-swap, high school, murder, chainsaw, serial killer, gore, high school student, stabbed, dead father","featuredNote":"1 win & 6 nominations","tmdbId":"551804","imdbId":"tt10919380","imdbScore":"6.3","metacriticScore":"66","tagline":"Basic switch. Killer new look.","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=_6bSFQex-DY\\"]","casts":"[{\\"id\\":\\"5d6876201f33190010376b20\\",\\"name\\":\\"Vince Vaughn\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170796315380129-A9fJ88dfXZGpgFTnQIK0bOtjMrj.jpg\\",\\"character\\":\\"The Butcher\\"},{\\"id\\":\\"5d6876291f33190014376b0c\\",\\"name\\":\\"Kathryn Newton\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170796320464530-josGaRCnTXIKQWSU09RxqKE5S8F.jpg\\",\\"character\\":\\"Millie Kessler\\"},{\\"id\\":\\"5db237c34b0c630017e944b3\\",\\"name\\":\\"Celeste O\'Connor\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170796522936087-6vHIQYhuqry02eVaKOlxb5XwJu0.jpg\\",\\"character\\":\\"Nyla Chones\\"},{\\"id\\":\\"5db237e14b0c630014e921f5\\",\\"name\\":\\"Misha Osherovich\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170796321531909-i1jkFPxcr4IAELlIhcTlkuLI3VC.jpg\\",\\"character\\":\\"Josh Detmer\\"},{\\"id\\":\\"5fb62548bd101e003f325953\\",\\"name\\":\\"Uriah Shelton\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/21170796429799901-xsoajGHj9gonoR5qp3MgVazcYnt.jpg\\",\\"character\\":\\"Booker Strode\\"}]","cityNowShowing":[]}',
        },
        {
          id: 20,
          Link: "/cinema/phim/megags-the-hunger-games-mockingjay-part-2-20",
          createDate: 0,
          lastUpdate: 1620287607161,
          status: 3,
          filmData:
            '[{"filmId":"HO00000066","momoFilmId":"","cineplex":4,"captionType":2,"versionType":2,"title":"2D - HUNGER GAME: MOCKING JAY P.2","cineplexEnum":"MEGA_GS","isClosed":true}]',
          title: "[MEGAGS] The Hunger Games: Mockingjay - Part 2",
          viTitle: "[MEGAGS] Đấu Trường Sinh Tử: Húng Nhại 2",
          rating: "C13",
          synopsis:
            "Sau khi được tôn vinh như “Húng Nhại” - biểu tượng cho niềm hy vọng và khát khao tự do, Katniss cùng những người dân ở Quận 13 quyết tâm đứng lên thực hiện cuộc khởi nghĩa nhằm vào Capitol. Cách duy nhất để dành phần thắng chính là trừ khử tổng thống Snow - kẻ chủ mưu đứng đằng sau tất cả những nỗi đau thương mà Panem phải chịu đựng suốt hơn 70 năm qua. Luôn đi trước một bước, mọi thứ với Snow dường như luôn nằm trong dự định, đặc biệt là khi âm mưu của lão được vén màn: Capitol chính là Đấu Trường Sinh Tử lần thứ 76. Một lần nữa, Katniss cùng nhóm của mình bất đắc dĩ sẽ phải tham gia vào trò chơi man rợ này để chấm dứt tất cả.",
          viSynopsis: null,
          duration: 137,
          openingDate: 1608051600000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622516513061177-lImKHDfExAulp16grYm8zD5eONE.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=PqNGHKLyPD0",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622516357888091-vPToLA075RgDEKllFYAq5BaMxKU.jpg",
          filmType: "2D",
          genreName: "Khoa Học Viễn Tưởng, Phiêu Lưu, Hành Động",
          averageScore: null,
          extras:
            '{"featuredFilm":true,"bigBannerPrior":1,"sneakShowDate":1607533200000,"keywords":"game of death, dystopia, strong woman, based on novel or book, revolution, based on young adult novel","featuredNote":"16 wins & 34 nominations","tmdbId":"131634","imdbId":"tt1951266","imdbScore":"6.6","rottenTomatoesScore":"69%","metacriticScore":"65","tagline":"The fire will burn forever.","autoplayTrailer":"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/7170600637939688-y2meta.com%20-%20TI%E1%BB%86C%20TR%C4%82NG%20M%C3%81U%20-%20MAIN%20TRAILER%20_%20KC_23.10.2020.mp4","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=PqNGHKLyPD0\\"]","casts":"[{\\"id\\":\\"5eace3e966469a00245c9e75\\",\\"name\\":\\"NSƯT Đức Thịnh\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622515262080142-7170622565690751-3zRVqDjr94R6cKz6WNuPtf7jGM9.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5eace3fd0c3ec8001dcb14f6\\",\\"name\\":\\"Hồng Ánh\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622515262594690-7170622567342853-2RODT9UiJhVvAraOKFkyIuKERKL.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5eace40c5a7884001ac34b32\\",\\"name\\":\\"Hứa Vĩ Văn\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622515263522390-7170622574036877-pJ8XHYq86Tq5sGQDMBEuy5vxTmq.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5eace41466469a001f5c97f3\\",\\"name\\":\\"Thu Trang\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622515258456488-7170622569964908-oSpHBUKu9vNcmQTJD9w936jJD7R.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5eace42366469a001c5ca95f\\",\\"name\\":\\"Kiều Minh Tuấn\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/15622515270795904-7170622574653566-bcbOUKabmDtiRjZlNL2UpJvxJzz.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"0.egt62n7vmvt\\",\\"name\\":\\"12312\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/9595150299299380-9594979489557763-9593751322994645-9591429355879529-9582985768555345-175696534960451-174365805685950-173816869302603-173744135447795-35896.png\\",\\"character\\":\\"123\\"},{\\"id\\":\\"0.0czu7djqntq\\",\\"name\\":\\"Minh Ho\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/9595150320341057-9594979509403325-9593751355220350-9591429350677583-9582985763305004-175696540120493-174365841268947-173816897659360-173752186300540-visual-reverse-image-search-v2_297x176.jpg\\",\\"character\\":\\"2313123\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":23,"per":65,"totalMedal":15},"cityNowShowing":[]}',
        },
        {
          id: 21,
          Link: "/cinema/phim/tenet-21",
          createDate: 0,
          lastUpdate: 1620654812658,
          status: 3,
          filmData:
            '[{"filmId":"20014300","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"Tiec Trang Mau","cineplexEnum":"CGV","isClosed":true},{"filmId":"20014700","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"The Croods: A New Age","cineplexEnum":"CGV","isClosed":true},{"filmId":"20009900","momoFilmId":"","cineplex":6,"captionType":-1,"versionType":-1,"title":"The Elfkins Baking A Difference","cineplexEnum":"CGV","isClosed":true}]',
          title: "TENET",
          viTitle: "TENET",
          rating: "C16",
          synopsis:
            "Armed with only one word - Tenet - and fighting for the survival of the entire world, the Protagonist journeys through a twilight world of international espionage on a mission that will unfold in something beyond real time.\nTenet là câu chuyện nói về một điệp viên CIA với biệt danh The Protagonist (nhân vật chính) được một tổ chức bí mật chiêu mộ cho nhiệm vụ giải cứu thế giới thoát khỏi thế chiến thứ III. Đồng hành cùng với anh trong nhiệm vụ này là Neil – một điệp viên da trắng có tung tích mờ ám. Để bắt đầu nhiệm vụ của mình, họ tìm đến những tên buôn vũ khí nguy hiểm nhất thế giới nhằm khám phá ra nguồn gốc của những hiện tượng kỳ lạ đã xảy ra gần đây.\nTenet là câu chuyện nói về một điệp viên CIA với biệt danh The Protagonist (nhân vật chính) được một tổ chức bí mật chiêu mộ cho nhiệm vụ giải cứu thế giới thoát khỏi thế chiến thứ III. Đồng hành cùng với anh trong nhiệm vụ này là Neil – một điệp viên da trắng có tung tích mờ ám. Để bắt đầu nhiệm vụ của mình, họ tìm đến những tên buôn vũ khí nguy hiểm nhất thế giới nhằm khám phá ra nguồn gốc của những hiện tượng kỳ lạ đã xảy ra gần đây.",
          viSynopsis: null,
          duration: 150,
          openingDate: 1603818000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176770388785-5521294880659723-g50V0IpNnA9aO2KzUsWx1IOGbl7.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=L3pk_TBkihU",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176791294923-6923771737922667-g5hIWYC46G1eVYPNkwaX5AisIcc.jpg",
          filmType: "2D, IMAX, Onyx, Dolby Atmos",
          genreName: "Khoa Học Viễn Tưởng, Gây Cấn, Hành Động",
          averageScore: null,
          extras:
            '{"featuredFilm":true,"sneakShowDate":1604336400000,"keywords":"CIA, Nghịch đảo, WWIII, sea, car chase, language, espionage, work, terrorist attack, swat team, crime scene","tmdbId":"577922","imdbId":"tt6723592","imdbScore":"7.9","rottenTomatoesScore":"74%","certifiedFresh":true,"metacriticScore":"69","tagline":"Đừng hiểu, hãy cảm nhận!","awards":"Golden Globe","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175800276257-6923771214194446-1vudnDRaxOURqLmNFBXwqB8krhA.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175827014994-6923771206396578-sAQLL9fJOBWMjvlHghlB47yZCEL.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175817543530-6923771212155715-6TB7E8xvlCqAWqPdS2fPkdvCVM5.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175803642619-6923771213745606-7BYtnXUfweV6EryNYDpZFeS31Ia.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175834695035-6923771212768592-86moT8cxSXjyWotA500cmNUV8Ze.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175906393024-6923771333629409-ppj2uRyxxhEASqSW0wCRRLls8I8.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175903193889-6923771312094878-gPucRIw0n3MPv7r1FEdMpngNXr7.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175956786347-6923771312588982-l7ve4N8ZdWUzxLNTpsLO2cBBJAl.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175960230327-6923771320598298-dmLMz5BYUW7MzApVbckD1aiqBEW.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579175958021168-6923771315944812-2JrpCnV3ntIKm9h7Iw768SMTZO9.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176002445649-6923771390632156-axSEiEvYRrHwaGn9HzFHjWVyRF7.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176039436479-6923771416453674-bImDTzVitwcKZXCF1naWRGI5W0g.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176089469336-6923771447321856-ouslWxr71CR2jO9HmAZ8ZboWkxQ.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176056502256-6923771423372064-3GwYDxFCzPCEGVM2VvP9DLldaUz.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176077693531-6923771438464059-d67rRfWFgux5ZA0a6zwMj4yZCKO.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176155806107-6923771485823346-edARj5QbThiHvuXDvHPIi1s08UW.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176227397261-6923771525675455-s2i74aEODBPUWO9xY4ZpUAzP6ug.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176196753169-6923771526481454-p2vgoQXLEUfTdpLn2Ob7L7sHZkT.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176199917213-6923771551221119-8NTRWfYOWj66JfxBhGoaAO42aVX.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176203980129-6923771557609231-51aFCM5cqaNF0DGXkJ6zj713vHy.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176272867555-6923771592290850-tPPH9fdQmCtXi2Gz6lQrmL2vjos.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176305289911-6923771629910596-oKBWimQG5r5eo8VNWlebB1MIbv9.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176346362339-6923771629294091-bgQUBbZ14hfeb975yZ2YzTnAwen.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176324826101-6923771656821185-hfg0NYSNNtIHeMv05R4sPk2nNbW.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176377160499-6923771655538691-1g7n02DNWOyrYl8WzXViloEHdSC.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176378812790-6923771694821855-wzJRB4MKi3yK138bJyuL9nx47y6.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=L3pk_TBkihU\\",\\"https://www.youtube.com/watch?v=AZGcmvrTX9M\\",\\"https://www.youtube.com/watch?v=LdOM0x0XDMo\\"]","casts":"[{\\"id\\":\\"5cab21c90e0a264c88f63209\\",\\"name\\":\\"Elizabeth Debicki\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176392656402-5521294573678088-nXXbGG1vCrHlscwqD55EGI9aHpA.jpg\\",\\"character\\":\\"Kat\\"},{\\"id\\":\\"5ce577b00e0a26381ccfa79f\\",\\"name\\":\\"Kenneth Branagh\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176401976351-5521294869117907-AbCqqFxNi5w3nDUFdQt0DGMFh5H.jpg\\",\\"character\\":\\"Andrei Sator\\"},{\\"id\\":\\"5cebdd4bc3a3682e93239fd3\\",\\"name\\":\\"Dimple Kapadia\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176453170243-5521294573744271-hnFAuyLpo0f20S1GbAY7cYBNFAn.jpg\\",\\"character\\":\\"Priya\\"},{\\"id\\":\\"5d657c14e2102300130d65d3\\",\\"name\\":\\"Himesh Patel\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176472138299-6635879029104310-fC6diZ0i3Epot9dRl7b2SSegf4L.jpg\\",\\"character\\":\\"Mahir\\"},{\\"id\\":\\"5cab21b70e0a264c80f63b9c\\",\\"name\\":\\"Robert Pattinson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176497695686-6635879028156777-3qZ09UE7lN6AtorfXFRYpEtSY93.jpg\\",\\"character\\":\\"Neil\\"},{\\"id\\":\\"5ce577c5925141479bbb7d78\\",\\"name\\":\\"Clémence Poésy\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176474487358-6635879029454512-jB16viYaToZvJvbX8mlUtTo3Gsw.jpg\\",\\"character\\":\\"Barbara\\"},{\\"id\\":\\"5dc60faea14e1000135af48a\\",\\"name\\":\\"Andrew Howard\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176474172064-6635879029764998-lKuZYelO5zUzJ3AqkjV9us1mZLa.jpg\\",\\"character\\":\\"Driver\\"},{\\"id\\":\\"5ce57790c3a36802d21fa74e\\",\\"name\\":\\"Aaron Taylor-Johnson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176529681299-6635879033140709-ldIRyeraVFHwprWhe8BK441UJYn.jpg\\",\\"character\\":\\"Ives\\"}]","crew":"[{\\"id\\":\\"5da45809e860170014b22c31\\",\\"name\\":\\"Emma Thomas\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176548153025-6657109468071427-6GemtNCy856iLho6WRsFASxQTAp.jpg\\",\\"job\\":\\"Producer\\"},{\\"id\\":\\"5dc6115b9c24fc0018bfbc8d\\",\\"name\\":\\"John Papsidera\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176559755728-6657109467475030-egwEVyrAmdWhtuLqE5fcThZf41E.jpg\\",\\"job\\":\\"Casting\\"},{\\"id\\":\\"5da45992e860170012b1bd3e\\",\\"name\\":\\"Hoyte van Hoytema\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176561464805-6657109470840163-y2HXvac1oPzciwxfdyWc5syThRk.jpg\\",\\"job\\":\\"Director of Photography\\"},{\\"id\\":\\"5da459e1cb3084001393fb3e\\",\\"name\\":\\"Ludwig Göransson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176579673544-6657109473581775-nY2ZxvjlPS5jYE7wOWrNT1rQ02m.jpg\\",\\"job\\":\\"Original Music Composer\\"},{\\"id\\":\\"5dee94d085da12001a4b64de\\",\\"name\\":\\"Douglas Shamburger\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176648631787-6657109470773996-qgsKCoORvf0kjfEzYum8B7f8q0J.jpg\\",\\"job\\":\\"Boom Operator\\"},{\\"id\\":\\"5da45a04e860170017b2738f\\",\\"name\\":\\"Nathan Crowley\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176634661989-6657109485104517-djrPBaXkKLdt8JM5wWGTIXRh5AP.jpg\\",\\"job\\":\\"Production Design\\"},{\\"id\\":\\"5da459b31967570017ace5bf\\",\\"name\\":\\"Jennifer Lame\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176648191659-6657109491527923-nwiKUZoGn298QWYgICPDbhMUSgc.jpg\\",\\"job\\":\\"Editor\\"},{\\"id\\":\\"5dee9530688cd0001720397f\\",\\"name\\":\\"George Cottle\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176670200074-6657109488768287-jwQmBfZCJk7V9W96r7fXo3JGyMs.jpg\\",\\"job\\":\\"Stunt Coordinator\\"},{\\"id\\":\\"5dc6117ca14e1000175af710\\",\\"name\\":\\"Toby Britton\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176705463102-6657109490253365-zdzCR1xmS1rGiUyK94KUvbl8Ym1.jpg\\",\\"job\\":\\"Supervising Art Director\\"},{\\"id\\":\\"5e8a9a39ffd44d00131a2296\\",\\"name\\":\\"Jackson Spidell\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14579176740183831-6657109486900730-7cozKHcmO4MOOViAMqE4FXd0Bmf.jpg\\",\\"job\\":\\"Fight Choreographer\\"},{\\"id\\":\\"5dc611ae6f8d950017c3cfaa\\",\\"name\\":\\"Rory Bruen\\",\\"picture\\":\\"\\",\\"job\\":\\"Supervising Art Director\\"},{\\"id\\":\\"5dee944a85da1200144bccde\\",\\"name\\":\\"Robert Aguirre\\",\\"picture\\":\\"\\",\\"job\\":\\"Assistant Art Director\\"},{\\"id\\":\\"5dc611fa6f8d950010c40928\\",\\"name\\":\\"Letizia Santucci\\",\\"picture\\":\\"\\",\\"job\\":\\"Set Decoration\\"},{\\"id\\":\\"5dc611d099259c00137ef386\\",\\"name\\":\\"Emmanuel Delis\\",\\"picture\\":\\"\\",\\"job\\":\\"Set Decoration\\"},{\\"id\\":\\"5f55d87af5f1c500368ba730\\",\\"name\\":\\"Patrick Zapata\\",\\"picture\\":\\"\\",\\"job\\":\\"Set Production Assistant\\"},{\\"id\\":\\"5f4044f63bd26e003580f296\\",\\"name\\":\\"Andrew Jackson\\",\\"picture\\":\\"\\",\\"job\\":\\"Visual Effects Supervisor\\"},{\\"id\\":\\"5dee947f02576400145569ad\\",\\"name\\":\\"Willie D. Burton\\",\\"picture\\":\\"\\",\\"job\\":\\"Sound Mixer\\"},{\\"id\\":\\"5dee950e025764001655313d\\",\\"name\\":\\"Kevin Bitters\\",\\"picture\\":\\"\\",\\"job\\":\\"Pyrotechnician\\"}]","cityNowShowing":[]}',
        },
        {
          id: 22,
          Link: "/cinema/phim/break-the-silence-the-movie-22",
          createDate: 0,
          lastUpdate: 1621569366256,
          status: 3,
          filmData:
            '[{"filmId":"HO00001305","momoFilmId":"","cineplex":3,"captionType":2,"versionType":2,"title":"AMERICAN FIGHTER ","cineplexEnum":"BHD","isClosed":true}]',
          title: "Break the Silence: The Movie ",
          viTitle: "Break the Silence: The Movie ",
          rating: "P",
          synopsis:
            "With unprecedented access, Break the Silence: The Movie travels with BTS throughout the Love Yourself: Speak Yourself world tour, exploring each band member behind the curtain. Off stage, we see another side of BTS. The seven members begin to candidly tell personal stories they have never voiced before.",
          viSynopsis: null,
          duration: 101,
          openingDate: 1621875600000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780320040430333-6651212156663308-gLNZdWq7r8ZAyWuUxoke7qC0gy8.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=gZCTwV2xq5Y",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780320014262684-6651212153173255-8vDzqxZvrp7IdyAI5F8NePWGyEP.jpg",
          filmType: "2D",
          genreName: "Tài Liệu, Nhạc",
          averageScore: null,
          extras:
            '{"sneakShowDate":1621530000000,"keywords":"k-pop","tmdbId":"730647","imdbId":"tt12850582","imdbScore":"7.6","tagline":"Worldwide handesome","posters":"[]","backdrops":"[]","trailers":"[\\"https://www.youtube.com/watch?v=gZCTwV2xq5Y\\"]","casts":"[{\\"id\\":\\"5f2c8186c51acd00349c461d\\",\\"name\\":\\"Kim Nam-joon\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319785385649-6651212130155375-cJmaUOIuibsC0DPnUMStN4rfAwr.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81ada2e6020037c1efcc\\",\\"name\\":\\"Kim Seok-jin\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319605868039-6651212131336830-cothCnRmZXC63olRZ816lZoGMDc.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81cb9c24fc0037a4c988\\",\\"name\\":\\"Jung Ho-seok\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319785321648-6651212136560265-2xZu36Z6JFT95AIzazyRHW51dC1.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81bea14bef0037312622\\",\\"name\\":\\"Min Yoon-gi\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319785374959-6651212137207292-83xf9EkLK3k7CJSaXe3iVgBC6S3.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81e3f54836003484dbfe\\",\\"name\\":\\"Kim Tae-hyung\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319785358347-6651212139273344-jPCklfz8cAXfvRptmeNeYBa9Myf.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81d822df2e00373da30a\\",\\"name\\":\\"Park Ji-min\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780319988864710-6651212144193839-ydSiyzoGcjvNLXwXm6dhIFxAewN.jpg\\",\\"character\\":\\"Himself\\"},{\\"id\\":\\"5f2c81ef22df2e00373da497\\",\\"name\\":\\"Jeon Jung-kook\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/24780320000632149-6651212149223148-2O7wrPFZtdfNJ5w6LGlIri3Xd0.jpg\\",\\"character\\":\\"Himself\\"}]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","total":3,"per":67},"cityNowShowing":[50]}',
        },
        {
          id: 23,
          Link: "/cinema/phim/jumanji-welcome-to-the-jungle-23",
          createDate: 0,
          lastUpdate: 1620654812657,
          status: 3,
          filmData:
            '[{"filmId":"HO00001371","momoFilmId":"","cineplex":3,"captionType":2,"versionType":2,"title":"\\"EM\\" LÀ CỦA EM","cineplexEnum":"BHD","isClosed":true}]',
          title: "Jumanji: Welcome to the Jungle",
          viTitle: "Jumanji: Trò Chơi Kỳ Ảo",
          rating: "C13",
          synopsis:
            "Jumanji: Trò Chơi Kỳ Ảo theo chân một nhóm 4 học sinh phổ thông bị phạt dọn dẹp tầng hầm trường học. Tại đó, họ phát hiện ra phiên bản game cổ điển mang tên Jumanji. Cả 4 tiến hành chọn nhân vật nhập vai và bắt đầu chơi, nhưng nào ngờ họ bị cuốn vào thế giới bên trong game và sống với ngoại hình, đặc tính nhân vật đã chọn trước đó.Họ buộc phải tham gia vào những chuyến phiêu nguy hiểm nhưng vô cùng thú vị và ẩn chứa nhiều bất ngờ. Trailer hé lộ thế giới kỳ ảo đầy mê hoặc, những pha hành động nghẹt thở xen lẫn các tình tiết dở khóc dở cười. Như vậy, khác với phần phim năm 1995, trò chơi ma thuật sẽ không được giải phóng ra thế giới bên ngoài mà người bị hút vào trong game. Liệu họ sẽ phải làm gì để trở về với thực tại?",
          viSynopsis: null,
          duration: 119,
          openingDate: 1598806800000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/16410750876929461-3545451925593604-22hqf97LadMvkd4zDi3Bq25xSqD.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/16410750825587397-3545451932903387-rz3TAyd5kmiJmozp3GUbYeB5Kep.jpg",
          filmType: null,
          genreName: "Hài, Giả Tượng, Phiêu Lưu, Hành Động",
          averageScore: null,
          extras:
            '{"sneakShowDate":1598634000000,"keywords":"game, video game, explorer, jungle, zoologist, sequel, based on young adult novel","featuredNote":"5 wins & 14 nominations","tmdbId":"353486","imdbId":"tt2283362","imdbScore":"6.9","rottenTomatoesScore":"76%","metacriticScore":"58","tagline":"The game has evolved.","posters":"[]","backdrops":"[]","trailers":"[]","rating":{"id":"32f2bb50-72ab-406f-9d3c-1b3e117c0b06","name":"Thích","image":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/11471406250392379-like.png","totalMedal":2,"total":2,"per":100},"cityNowShowing":[50]}',
        },
        {
          id: 24,
          Link: "/cinema/phim/the-invisible-man-24",
          createDate: 0,
          lastUpdate: 1620654812658,
          status: 3,
          filmData:
            '[{"filmId":"718b7b5b-8e58-40f5-b421-96797e3add9f","momoFilmId":"","cineplex":5,"captionType":2,"versionType":2,"title":"KẺ VÔ HÌNH - THE INVISIBLE MAN (C18)","cineplexEnum":"CINESTAR","isClosed":true},{"filmId":"718b7b5b-8e58-40f5-b421-96797e3add9f","momoFilmId":"","cineplex":7,"captionType":2,"versionType":2,"title":"KẺ VÔ HÌNH - THE INVISIBLE MAN (C18)","cineplexEnum":"DCINE","isClosed":true}]',
          title: "The Invisible Man",
          viTitle: "Kẻ Vô Hình",
          rating: "C16",
          synopsis:
            "Bị kẹt trong một mối quan hệ bạo lực và kiểm soát với một nhà khoa học giàu có và thông minh, Cecilia Kass chạy trốn vào nửa đêm cùng với sự giúp đỡ của người thân. Nhưng khi người yêu cũ của Cecilia tự vẫn và để lại khối tài sản lớn cho cô, Cecilia nghi ngờ cái chết của anh ta không có thật. Một loạt các sự kiện trùng hợp chết người diễn ra, đe dọa mạng sống bản thân cô và những người xung quanh.",
          viSynopsis: null,
          duration: 125,
          openingDate: 1598547600000,
          graphicUrl:
            "http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584389839822-rNpPKC6vVT0G8nP1pd2FgWU5naI.jpg",
          trailerUrl: null,
          bannerUrl:
            "http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584397657846-uZMZyvarQuXLRqf3xdpdMqzdtjb.jpg",
          filmType: "2D",
          genreName: "Kinh Dị,Khoa Học Viễn Tưởng,Gây Cấn",
          averageScore: null,
          extras:
            '{"keywords":"murder, mental hospital, stalker, death, invisible person, scientist, architect, pregnancy, car crash, woman in peril, domestic abuse, fake suicide, police detective, based on novel or book, violence","featuredNote":"Giật gân, Hành động, Tâm lý","tmdbId":"570670","imdbId":"tt1051906","imdbScore":"7.1","certifiedFresh":true,"metacriticScore":"72","tagline":"Những gì bạn không thấy có thể làm hại bạn.","autoplayTrailer":"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605340586039418-y2meta.com%20-%20THE%20INVISIBLE%20MAN%20_%20K%E1%BA%BA%20V%C3%94%20H%C3%8CNH%20_%20D%E1%BB%B0%20KI%E1%BA%BEN_%2028.02.2020.mp4","posters":"[]","backdrops":"[]","trailers":"[]","casts":"[{\\"id\\":\\"5c7bb553c3a36841dc1b2bbc\\",\\"name\\":\\"Elisabeth Moss\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584373341319-3r39W0bpiC2lkCh8nJMqJoLWrH.jpg\\",\\"character\\":\\"Cecilia Kass\\"},{\\"id\\":\\"5d0e4ff40e0a2602a1cc1712\\",\\"name\\":\\"Aldis Hodge\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584376419072-jPpnaAGFXaIeOrRNUHIHxk3fIJL.jpg\\",\\"character\\":\\"James Lanier\\"},{\\"id\\":\\"5cd9e835c3a3681443c24cc3\\",\\"name\\":\\"Storm Reid\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584385278450-vQLNtoNcKGWs1bWRFIxUbGm3pL9.jpg\\",\\"character\\":\\"Sydney Lanier\\"},{\\"id\\":\\"5e5534f4a76ac50011a8bb08\\",\\"name\\":\\"Michael Dorman\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584376487218-624CKam4fVBcHATuOfsOEvPzCCh.jpg\\",\\"character\\":\\"Tom Griffin\\"},{\\"id\\":\\"5d0e501a0e0a263dddceb9ea\\",\\"name\\":\\"Harriet Dyer\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584373911027-pxiSf8sPDUsNWZz488FME3TKH5u.jpg\\",\\"character\\":\\"Emily Kass\\"},{\\"id\\":\\"5d28eebacaab6d0f71992473\\",\\"name\\":\\"Oliver Jackson-Cohen\\",\\"picture\\":\\"http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/uploads/cinema-2.0/3605584386437652-ucPJbjWXqrpBg8jSAqhaZob9ReF.jpg\\",\\"character\\":\\"Adrian Griffin\\"}]","cityNowShowing":[]}',
        },
      ],
      filmsComing: [
        {
          id: 1,
          Link: "/cinema/phim/raya-and-the-last-dragon-1",
          createDate: 0,
          lastUpdate: 1608890052325,
          status: 2,
          filmData: "[]",
          title: "Raya and the Last Dragon",
          viTitle: "Raya và Rồng Thần Cuối Cùng",
          rating: "P",
          synopsis:
            "Raya và Rồng Thần Cuối Cùng kể về một vương quốc huyền bí có tên là Kumandra – vùng đất mà loài rồng và con người sống hòa thuận với nhau. Nhưng rồi một thế lực đen tối bỗng đe dọa bình yên nơi đây, loài rồng buộc phải hi sinh để cứu lấy loài người. 500 năm sau, thế lực ấy bỗng trỗi dậy và một lần nữa, Raya là chiến binh duy nhất mang trong mình trọng trách đi tìm Rồng Thần cuối cùng trong truyền thuyết nhằm hàn gắn lại khối ngọc đã vỡ để cứu lấy vương quốc. Thông qua cuộc hành trình, Raya nhận ra loài người cần nhiều hơn những gì họ nghĩ, đó chính là lòng tin và sự đoàn kết.",
          viSynopsis: null,
          duration: 101,
          openingDate: 1614877200000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455250042979-fujXIXkQPBOaWn6SPkR4P8ha4xv.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=A-5RUmopVqA",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455253875209-jDhBdOotIMIQlgrpGgymr2LmTHe.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Hoạt Hình, Giả Tượng, Phiêu Lưu, Hành Động, Gia Đình",
          averageScore: null,
          extras:
            '{"bigBanner":true,"keywords":"kung fu, dragon, anime","tmdbId":"527774","imdbId":"tt5109280","tagline":"A quest to save her world.","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995435305504007-RAYA%20V%C3%80%20R%E1%BB%92NG%20TH%E1%BA%A6N%20CU%E1%BB%90I%20C%C3%99NG%20-%20TEASER%20TRAILER%20-%20KH%E1%BB%9EI%20CHI%E1%BA%BEU%20TH%C3%81NG%203.2021.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455209369618-w8YjgPiafMEAUdX8Y0MR0YgLm67.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455214052822-ghLVV2rxVIb1s8Mp5DokBGBxKGU.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455207718983-yqRzIVKXoopoqK5ShWonAq0eru7.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455213442021-erkLagk4HW4ickU8gQeRgBghtUA.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455210843384-A69pNa8HuVatufLKEkLxZ5gyIpg.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455235238857-rLJaTJSuRHhRiLIKMMedfHM8KQR.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455231718217-rONChBDQ19NSuBNoneRRJVrHAB9.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455230687085-xvtUgDeNbgNUP71HzkDgRpwFzBW.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455230899196-rLBTZMjRRqukn0Oi3RINBumM7uq.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=A-5RUmopVqA\\"]","casts":"[{\\"id\\":\\"5f47db6f813cb600358c77aa\\",\\"name\\":\\"Kelly Marie Tran\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455231775080-v2daUrk7hZryH6vtCWK9ESf6gAG.jpg\\",\\"character\\":\\"Raya (voice)\\"},{\\"id\\":\\"5d618b1f7f6c8d1cfbed05da\\",\\"name\\":\\"Awkwafina\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995455245849197-9JGZ85VoRTwNRVYiWPVVu1gXETf.jpg\\",\\"character\\":\\"Sisu (voice)\\"}]"}',
        },
        {
          id: 2,
          Link: "/cinema/phim/dragon-rider-2",
          createDate: 0,
          lastUpdate: 1608890055819,
          status: 2,
          filmData: "[]",
          title: "Dragon Rider",
          viTitle: "Kỵ Sĩ Cưỡi Rồng",
          rating: "P",
          synopsis:
            "An unlikely trio of heroes – a dragon, a boy and a forest brownie – embark on an epic adventure to find the “Rim of Heaven” - the mythological safe haven for all dragons.",
          viSynopsis: null,
          duration: 100,
          openingDate: 1609434000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874462095598-6A7PbpKHKumu8EJ3OUCkbPQcPN3.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=H0jn6R1bpcw",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874463618522-494bVeWKJ3DlCRHQu7kaPyskou9.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Hoạt Hình, Phiêu Lưu, Gia Đình",
          averageScore: null,
          extras:
            '{"bigBanner":true,"keywords":"anime","tmdbId":"523366","imdbId":"tt7080422","imdbScore":"7.1","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992731390327221-K%E1%BB%B4%20S%C4%A8%20C%C6%AF%E1%BB%A0I%20R%E1%BB%92NG%20%28DRAGON%20RIDER%29%20-%20KH%E1%BB%9EI%20CHI%E1%BA%BEU_%2001.01.2021%20-%20SU%E1%BA%A4T%20CHI%E1%BA%BEU%20%C4%90%E1%BA%B6C%20BI%E1%BB%86T%20SAU%2018H00%20%2031.12.2020.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874443690716-4bu5OLGfBhXphfR67yLlZChMz48.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=H0jn6R1bpcw\\"]","casts":"[{\\"id\\":\\"5af34df10e0a26397100150d\\",\\"name\\":\\"Patrick Stewart\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874442307405-wEy5qSDT5jT3ZASc2hbwi59voPL.jpg\\",\\"character\\":\\"(voice)\\"},{\\"id\\":\\"5af34e0e0e0a26394300156b\\",\\"name\\":\\"Felicity Jones\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874442982601-35KdWSfTldNEdsn4MUGFIRoxJEu.jpg\\",\\"character\\":\\"(voice)\\"},{\\"id\\":\\"5af34f3892514178ef00124b\\",\\"name\\":\\"Thomas Brodie-Sangster\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874448850095-eFqJ1RbGChKJYRpd9iiwzQrgCED.jpg\\",\\"character\\":\\"(voice)\\"},{\\"id\\":\\"5af34f4292514178ef00124e\\",\\"name\\":\\"Freddie Highmore\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992874456795741-berpjuXALltPnJSkISZfQEDGnYQ.jpg\\",\\"character\\":\\"(voice)\\"}]"}',
        },
        {
          id: 3,
          Link: "/cinema/phim/monster-hunter-3",
          createDate: 0,
          lastUpdate: 1608890043771,
          status: 2,
          filmData: "[]",
          title: "Monster Hunter",
          viTitle: "Thợ Săn Quái Vật",
          rating: "C16",
          synopsis:
            "Monster Hunter được chuyển thể từ series game nổi tiếng cùng tên của Capcom. Trong phim, đội trưởng Artemis của nữ diễn viên Milla Jovovich (Resident Evil) và đồng đội đã vô tình bước qua một cánh cửa bí ẩn dẫn tới thế giới khác. Tại đây, họ phải chiến đấu với nhiều loài quái vật khổng lồ trong hành trình trở về thế giới. Đồng hành với họ trong trận chiến là nhân vật “Thợ săn” của nam diễn viên Tony Jaa (Ong Bak). Monster Hunter hứa hẹn sẽ là bom tấn hành động với những màn săn quái vật khổng lồ hoành tráng nhất năm 2020.",
          viSynopsis: null,
          duration: 104,
          openingDate: 1609434000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281556058712-oLX8fwtV2ryOh0qLUMwZYCGi6Ku.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=puQyZsaTtqY",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281878761241-z8TvnEVRenMSTemxYZwLGqFofgF.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Giả Tượng, Phiêu Lưu, Hành Động",
          averageScore: null,
          extras:
            '{"bigBanner":true,"bigBannerPrior":1,"keywords":"based on video game, giant monsters, dimensional travel, another dimension, female soldier","tmdbId":"458576","imdbId":"tt6475714","imdbScore":"5.1","metacriticScore":"40","tagline":"Behind our world, there is another","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992230083055086-TH%E1%BB%A2%20S%C4%82N%20QU%C3%81I%20V%E1%BA%ACT%20%28Monster%20Hunter%29%20Official%20Trailer%20_%20KC_%2030.12.2020.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281511905873-3ghQMf9ZQZcoR8WZaErXzjk2MyL.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281509014115-1BRdu3b7f40psgkXHZeXIXTVAuD.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281508246218-bJICB3iNqaRezqXVqQlT0BxduhL.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281515817472-6NPIyzbDgLgUfn9LykVeL6Q7DJG.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281516820516-qdSH5gVrSDoVooWlaYHUtvxaoj1.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281533063280-tARcMWpWO2qP24HDNLgeEQX3Zf9.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281532181557-c1l1pCm4GQOMSRLi39JwfH90zT4.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=puQyZsaTtqY\\"]","casts":"[{\\"id\\":\\"5af8e2900e0a263ed4003f3f\\",\\"name\\":\\"Milla Jovovich\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281536995645-usWnHCzbADijULREZYSJ0qfM00y.jpg\\",\\"character\\":\\"Artemis\\"},{\\"id\\":\\"5baab051c3a3683aaf00c798\\",\\"name\\":\\"T.I.\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281799160013-dHnR4QavU2ILjvbr3SXBh4cJSdJ.jpg\\",\\"character\\":\\"Link\\"},{\\"id\\":\\"5bb3b6990e0a263df7012ce1\\",\\"name\\":\\"Diego Boneta\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281536161062-uACJGIdsVhuxEoLjVjRapVB1g1T.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5baab075c3a3683a9700cd6f\\",\\"name\\":\\"Ron Perlman\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281556699169-34matYg8O1gsB24AAEssjao73yb.jpg\\",\\"character\\":\\"Admiral\\"},{\\"id\\":\\"5bc052b292514179ae01179c\\",\\"name\\":\\"Tony Jaa\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281834419143-anBSs0nh4n4cpiwRl5QmjT5zRrt.jpg\\",\\"character\\":\\"The Hunter\\"},{\\"id\\":\\"5edfcf25dbf14400208fbf63\\",\\"name\\":\\"Meagan Good\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13992281566243407-iwSNBQKKDhEbtNe8I4ROYoYmWqE.jpg\\",\\"character\\":\\"\\"}]"}',
        },
        {
          id: 4,
          Link: "/cinema/phim/a-quiet-place-part-ii-4",
          createDate: 0,
          lastUpdate: 1608715663781,
          status: 2,
          filmData: "[]",
          title: "A Quiet Place Part II",
          viTitle: "Vùng Đất Câm Lặng 2",
          rating: "C16",
          synopsis:
            "Phần hai tiếp nối các sự kiện xảy ra trong phần một, khi gia đình Abbot gồm người mẹ Evelyn (do Emily Blunt thủ vai) cùng ba con chạy trốn đến một thành phố tưởng như an toàn. Tuy nhiên, cả gia đình không ngờ rằng ở thế giới bên ngoài cũng đã bị những sinh vật ngoài hành tinh thâu tóm. Những sinh vật này khiếm khuyết về thị giác nhưng có thính giác siêu nhạy để săn mồi bằng cách lần theo âm thanh. “Vùng đất câm lặng” lúc này đã trở thành “thế giới câm lặng” khi những người sống sót tiếp tục phải lẩn trốn, không được tạo ra tiếng động mỗi khi di chuyển hay giao tiếp với nhau. Nhưng càng bước ra ngoài thế giới, gia đình Abbot sớm nhận ra rằng hiểm họa duy nhất không chỉ đến từ những sinh vật ngoài hành tinh. Những bí ẩn xung quanh cuộc đổ bộ của các giống loài này dần được hé lộ.",
          viSynopsis: null,
          duration: 97,
          openingDate: 1619110800000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751409204049-80x0HZxqkbLwktqMlJxUUewYb0U.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=OObI02klU6E",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751560780597-z2UtGA1WggESspi6KOXeo66lvLx.jpg",
          filmType: "2D, IMAX, Onyx, Dolby Atmos",
          genreName: "Khoa Học Viễn Tưởng, Gây Cấn, Chính Kịch",
          averageScore: null,
          extras:
            '{"keywords":"psychological thriller, alien monster, survival horror, deaf, alien life-form, human vs alien, alien invasion, alien, alien attack, sign languages, post-apocalyptic future, child in peril, parenting","tmdbId":"520763","imdbId":"tt8332922","tagline":"Silence is not enough.","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996735834411674-A%20QUIET%20PLACE%20II%20_%20V%C3%99NG%20%C4%90%E1%BA%A4T%20C%C3%82M%20L%E1%BA%B6NG%20II%20_%20TRAILER%20_%20KH%E1%BB%9EI%20CHI%E1%BA%BEU_%2020.03.2020.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750701699930-rd2lIZcsE3Fdhh9xmmkccByBjFF.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750635893516-9JtYKRnYMexueLcIFRx9bkAA0ls.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750631745072-hVNpYYuck09ktHtrXC7fByOXPiQ.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750664540131-kUQf8BhY2tInhyK3wA21kUdxb3W.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750651449922-AsqUSUqXrK8JfH8WEQnCXVbIAv6.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750975805004-gadXdEuFkc3PMy5i03TDfr8qbRa.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750962685403-am782sUaTOGcEPEdUUjybwUZP1f.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751111859610-55W4OmZx1tH73KOtYHMJU7uGpy0.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996750936410947-3YJk145WsQz0T9UTHjl3cxE1I4I.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751010250888-ojtkat19816bUNfpvCjn6KqWn5G.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=OObI02klU6E\\"]","casts":"[{\\"id\\":\\"5c70a0a5c3a3685a4911580d\\",\\"name\\":\\"Emily Blunt\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751225655316-nPJXaRMvu1vh3COG16GzmdsBySQ.jpg\\",\\"character\\":\\"Evelyn Abbott\\"},{\\"id\\":\\"5ca267b5c3a3687355860709\\",\\"name\\":\\"Cillian Murphy\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751273576460-rm4jns47kaqh7oC7RYiXrESlVbV.jpg\\",\\"character\\":\\"Emmett\\"},{\\"id\\":\\"5ca267e69251415bbe06f81e\\",\\"name\\":\\"Millicent Simmonds\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751255400697-vn7hejb0IRFvSrZxpxqY9RbBxMe.jpg\\",\\"character\\":\\"Regan Abbott\\"},{\\"id\\":\\"5ca26800c3a3687355860795\\",\\"name\\":\\"Noah Jupe\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751300602672-oledwip7hBHyRA43OBDUlDDhxhm.jpg\\",\\"character\\":\\"Marcus Abbott\\"},{\\"id\\":\\"5d51c6910102c96f1076083c\\",\\"name\\":\\"Djimon Hounsou\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996751127712060-hco0KMbrxACYTmBfAkSzCf23CXV.jpg\\",\\"character\\":\\"\\"}]"}',
        },
        {
          id: 5,
          Link: "/cinema/phim/no-time-to-die-5",
          createDate: 0,
          lastUpdate: 1608715484504,
          status: 2,
          filmData: "[]",
          title: "No Time to Die",
          viTitle: "Điệp Viên 007: Không Phải Lúc Chết",
          rating: "C13",
          synopsis:
            "Phần 25 của bộ phim điệp viên huyền thoại 007 sẽ tiếp nối câu chuyện về James Bond cùng những pha hành động táo bạo và hoành tráng hơn bao giờ hết. Sau sự kiện đầy ám ảnh trong Spectre, Bond lui về ở ẩn tại đất nước Jamaica, sống một cuộc đời cô độc nhưng bình lặng. Bỗng một người bạn cũ từ CIA xuất hiện, cầu xin anh giúp đỡ. Bond bất đắc dĩ phải tái xuất, nhưng không hề biết mình sẽ đối đầu với thế lực nào. Chi tiết đáng chú ý nhất là chiếc mặt nạ trắng vỡ nửa, đánh dấu sự xuất hiện của tên ác nhân kì quái bậc nhất trong cả series 007. Màn chạm trán giữa Bond và kẻ thù nguy hiểm này sẽ vén màn những bí ẩn còn để ngỏ và tiếp theo đó, có thể là một cuộc đối đầu “sinh tử”.",
          viSynopsis: null,
          duration: 163,
          openingDate: 1617296400000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996572095771578-6obsiTBb1D6Yc32q0BgAIFKOABu.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=P7-J0HKSCUc",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996572065733928-kcvKEvTWqIGMjpVJdbDVRHdIt4C.jpg",
          filmType: "2D, 3D, IMAX, Onyx, Dolby Atmos",
          genreName: "Gây Cấn, Phiêu Lưu, Bí Ẩn, Hành Động",
          averageScore: null,
          extras:
            '{"keywords":"british secret service, spy","tmdbId":"370172","imdbId":"tt2382320","tagline":"The mission that changes everything begins…","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996549347042819-KH%C3%94NG%20PH%E1%BA%A2I%20L%C3%9AC%20CH%E1%BA%BET%20_%20NO%20TIME%20TO%20DIE%20_%20Trailer%20_%20KC_%2013.11.2020.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571424625690-uy6l4Y8EZiNlNN5BGHnWfUNtVOy.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571395297721-zQMKeWU7kqcmSq9RXfrsvmgkubG.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571379538637-tPsDrUjguOS7bxssz3TqvEzBn3F.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571379472047-jiXWozMk854sAHYpLj2pujAQPp6.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571374748885-uoMygXRAXg1SOS1dqHUSi6yAVQA.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571741198315-u5Fp9GBy9W8fqkuGfLBuuoJf57Z.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571688562549-9QkYmsucJGWWTtQrDNDvqvRYp8t.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571667845374-hMWwdbI3MKsMOGRljMZrUqpNjqi.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571710731231-sYSVT2eblD4yApNrSqrHvlUxU0j.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571792065292-64oEFJusPULAkC9RA53hSwZUAYq.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=P7-J0HKSCUc\\"]","casts":"[{\\"id\\":\\"5657775bc3a3681970009dfa\\",\\"name\\":\\"Daniel Craig\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571946297323-iFerDZUmC5Fu26i4qI8xnUVEHc7.jpg\\",\\"character\\":\\"James Bond\\"},{\\"id\\":\\"5cc1a8150e0a26105af8741b\\",\\"name\\":\\"Rami Malek\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571705320614-zvBCjFmedqXRqa45jlLf6vBd9Nt.jpg\\",\\"character\\":\\"Lyutsifer Safin\\"},{\\"id\\":\\"5c09b51092514147950299e0\\",\\"name\\":\\"Léa Seydoux\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571998055067-tUfoM95x70Qsb1ElClPSpSrZQ2v.jpg\\",\\"character\\":\\"Dr. Madeleine Swann\\"},{\\"id\\":\\"5cc1a7f7c3a368145886aa8d\\",\\"name\\":\\"Lashana Lynch\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996571726218912-9bThcInfgzeXFk1Uicy2773Zul0.jpg\\",\\"character\\":\\"Nomi\\"},{\\"id\\":\\"5cc1a7e2c3a368776c8d2cab\\",\\"name\\":\\"Ana de Armas\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13996572013462447-14uxt0jH28J9zn4vNQNTae3Bmr7.jpg\\",\\"character\\":\\"Paloma\\"}]"}',
        },
        {
          id: 6,
          Link: "/cinema/phim/the-kings-man-6",
          createDate: 0,
          lastUpdate: 1614310936775,
          status: 2,
          filmData: "[]",
          title: "The King's Man",
          viTitle: "Kingsman: Khởi Nguồn",
          rating: "C18",
          synopsis:
            "Kingsman: Khởi Nguồn là phần tiền truyện của loạt phim Mật vụ Kingsman, xoay quanh quá trình hình thành và phát triển của tổ chức mật vụ nổi tiếng bậc nhất màn ảnh rộng này. Đứng trước một kế hoạch tiêu diệt hàng triệu người do một nhóm những bậc thầy ác nhân và bạo chúa khởi xướng; một điệp viên đầy kinh nghiệm, cùng với sự giúp đỡ của con trai và các cộng sự đáng tin cậy của mình, buộc phải chạy đua với thời gian ngăn chặn âm mưu đó.",
          viSynopsis: null,
          duration: 100,
          openingDate: 1615482000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064687014571280-13995812901280090-509rj2kjXyf69B31cfXjPizIkgt.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=G0AvPz_ElhQ",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064687014672728-13995812915725830-2OZyQtwn4N1o6uE5oOXJgxkk7Km.jpg",
          filmType: "2D, IMAX, Onyx, Dolby Atmos",
          genreName: "Hài, Phiêu Lưu, Hành Động",
          averageScore: null,
          extras:
            '{"keywords":"1910s, based on comic, secret service, world war i, london, england, spy, world domination","tmdbId":"476669","imdbId":"tt6856242","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995789516392404-THE%20KING%27S%20MAN%20_%20%27Ruthless%27%20Trailer.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686855857002-13995812883925623-4OTYefcAlaShn6TGVK33UxLW9R7.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686855891150-13995812881483180-oQPbZ5e6J9fuAyv4Gl0mMZMIyPI.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686855476250-13995812880801923-beRsk7Q5yqnHHvTC5RtIEUMEYc6.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=G0AvPz_ElhQ\\"]","casts":"[{\\"id\\":\\"5c74689a0e0a262c1f825c0b\\",\\"name\\":\\"Harris Dickinson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686855877208-13995812897175131-grbjZDzxBHqHYc0V0ic0Y1M38s.jpg\\",\\"character\\":\\"Conrad\\"},{\\"id\\":\\"5c7468400e0a262c1c828ae5\\",\\"name\\":\\"Ralph Fiennes\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686855797867-13995812884370873-tJr9GcmGNHhLVVEH3i7QYbj6hBi.jpg\\",\\"character\\":\\"Duke of Oxford\\"},{\\"id\\":\\"5c746831c3a3685a3e1aaa5f\\",\\"name\\":\\"Aaron Taylor-Johnson\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686944272811-13995812898724816-vkph4FcqFrOnVbrIQD7MbJXz4Uf.jpg\\",\\"character\\":\\"Lee Unwin\\"},{\\"id\\":\\"5c74682092514119e1b26661\\",\\"name\\":\\"Gemma Arterton\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686931768403-13995812901476242-Alfm28oyHNiYweNCUaMmrVjVMAT.jpg\\",\\"character\\":\\"Polly\\"},{\\"id\\":\\"5d2ca546caab6d208695ee8d\\",\\"name\\":\\"Tom Hollander\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/14064686928225416-13995812899785458-fs5EyDuxcE1s3hFijTSnVhpwYPO.jpg\\",\\"character\\":\\"George V / Wilhelm II / Nicholas II\\"}]"}',
        },
        {
          id: 7,
          Link: "/cinema/phim/nobody-7",
          createDate: 0,
          lastUpdate: 1608714206018,
          status: 2,
          filmData: "[]",
          title: "Nobody",
          viTitle: "Kẻ Vô Danh",
          rating: "C16",
          synopsis:
            'Hutch Mansell, a suburban dad, overlooked husband, nothing neighbor — a "nobody." When two thieves break into his home one night, Hutch\'s unknown long-simmering rage is ignited and propels him on a brutal path that will uncover dark secrets he fought to leave behind.',
          viSynopsis: null,
          duration: 101,
          openingDate: 1614272400000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293560881957-4QIe8vtKVNgD7QChden2Xrw4hT0.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=65pavfi1XTs",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293550793271-sBwGOfJtSF6hlXaEgvFfBfeLqMk.jpg",
          filmType: "2D, IMAX, Dolby Atmos, Onyx",
          genreName: "Gây Cấn, Hình Sự, Hành Động",
          averageScore: null,
          extras:
            '{"keywords":"thief, home invasion, family","featuredNote":"Another John Wick.","tmdbId":"615457","imdbId":"tt7888964","tagline":"Never underestimate a nobody","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995273056102226-K%E1%BA%BA%20V%C3%94%20DANH%20_%20D%E1%BB%B0%20KI%E1%BA%BEN%20KH%E1%BB%9EI%20CHI%E1%BA%BEU_%2026.02.2021.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293515689840-y6i3vFMOlpPS8gymSYNak9Z4u6G.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293518243313-pmwESzKvCCAVY06mX5f9eL7ABTk.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293527319602-bbsj4F5ioUD49eIut1uMpU8cyeR.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=65pavfi1XTs\\"]","casts":"[{\\"id\\":\\"5d2b2910be4b3612f5ab00f6\\",\\"name\\":\\"Bob Odenkirk\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293531812561-rF0Lb6SBhGSTvjRffmlKRSeI3jE.jpg\\",\\"character\\":\\"Hutch Mansell\\"},{\\"id\\":\\"5da4fb761967570013ae5963\\",\\"name\\":\\"Christopher Lloyd\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293545254058-nxVjpyb3UrfbPZnEyDNlQVlFAs5.jpg\\",\\"character\\":\\"Hutch\'s Father\\"},{\\"id\\":\\"5da4fb801967570013ae5970\\",\\"name\\":\\"Connie Nielsen\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293535611598-gSQ3O3PJ6ly6nT63joOtfZyscFP.jpg\\",\\"character\\":\\"Becca Mansell\\"},{\\"id\\":\\"5e8d14013434300017d4cc91\\",\\"name\\":\\"J.P. Manoux\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293541477314-lLEfizV970QfahQSBssUvj8Exs7.jpg\\",\\"character\\":\\"Darren\\"},{\\"id\\":\\"5e8d1412ce997a0015b8252b\\",\\"name\\":\\"Humberly Gonzalez\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13995293547132013-9tsJlFkKMZP02w4GPjlQedegWxf.jpg\\",\\"character\\":\\"Lupita\\"}]"}',
        },
        {
          id: 8,
          Link: "/cinema/phim/news-of-the-world-8",
          createDate: 0,
          lastUpdate: 1608712359925,
          status: 2,
          filmData: "[]",
          title: "News of the World",
          viTitle: "Chuyến Đi Định Mệnh",
          rating: "C13",
          synopsis:
            "Năm năm sau khi Nội chiến kết thúc, Thuyền trưởng Jefferson Kyle Kidd, một cựu chiến binh của ba cuộc chiến, di chuyển từ thị trấn này sang thị trấn khác với tư cách là một người kể những câu chuyện hư cấu, chia sẻ tin tức về các tổng thống và nữ hoàng, những mối thù hiển hách, những thảm họa tàn khốc , và những cuộc phiêu lưu hấp dẫn từ những vùng xa của thế giới.",
          viSynopsis: null,
          duration: 118,
          openingDate: 1612458000000,
          graphicUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447611665653-v9gOSS3cQvUel5MJW6qzGF6eQLh.jpg",
          trailerUrl: "https://www.youtube.com/watch?v=bl8vUxTcnEU",
          bannerUrl:
            "http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447609835396-ugoims6PfRgcAcIgTRoK2uvjhOu.jpg",
          filmType: "2D, IMAX, Dolby Atmos",
          genreName: "Miền Tây, Chính Kịch",
          averageScore: null,
          extras:
            '{"keywords":"american civil war, based on novel or book, civil war veteran, kiowa","tmdbId":"581032","imdbId":"tt6878306","metacriticScore":"71","tagline":"Find where you belong.","autoplayTrailer":"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993423648600526-CHUY%E1%BA%BEN%20%C4%90I%20%C4%90%E1%BB%8ANH%20M%E1%BB%86NH%20_%20TOM%20HANKS%20_%20D%E1%BB%B1%20ki%E1%BA%BFn_%2005.02.2021.mp4","posters":"[]","backdrops":"[\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447591563479-fJyHzq0ZWmXj2HJZWqSxEu4rh3v.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447578683697-lNWRDRPP4cFhCxbx7WQ9qWPJGeP.jpg\\",\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447587011412-aRxE0gUVC4T5bwCQexJjmgVmTsB.jpg\\"]","trailers":"[\\"https://www.youtube.com/watch?v=bl8vUxTcnEU\\"]","casts":"[{\\"id\\":\\"5c5f525ac3a3683cc68eab43\\",\\"name\\":\\"Tom Hanks\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447584503585-xndWFsBlClOJFRdhSt4NBwiPq2o.jpg\\",\\"character\\":\\"Captain Jefferson Kyle Kidd\\"},{\\"id\\":\\"5d647662813cb6026ef8ab97\\",\\"name\\":\\"Helena Zengel\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447579298522-yBICpAAWPm5QFJyQ3p5dno8nQ8e.jpg\\",\\"character\\":\\"Johanna Leonberger\\"},{\\"id\\":\\"5d65c9e8b3e62766ecfd8041\\",\\"name\\":\\"Fred Hechinger\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447596806569-n9cfoNKpyKsB3Jckec5IOIlyV2e.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5d69d8726f53e10015f0756a\\",\\"name\\":\\"Michael Angelo Covino\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447597974890-a3TAoOHa7XTE9ZEidLrNRX9cxrr.jpg\\",\\"character\\":\\"\\"},{\\"id\\":\\"5d8ab241d9f4a6001850692b\\",\\"name\\":\\"Thomas Francis Murphy\\",\\"picture\\":\\"http://s3-ap-southeast-1.amazonaws.com/avatars.mservice.io/uploads/cinema-2.0/13993447612359399-xC895xJVAAjoFtZQlGZqA1yil3E.jpg\\",\\"character\\":\\"Merritt Farley\\"}]"}',
        },
      ],
    },
  },
};
export default (req, res) => {
  res.status(200).json(data);
};
