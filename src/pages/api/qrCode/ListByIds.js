// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = {

  "Result": true,
  "Data": [
      {
          "Id": 2,
          "IsNewTab": true,
          "QrLink": "https://momoapp.page.link/bi9jFZjZ21sej9gr5",
          "ShortTitle": "Đặt vé ngay",
          "Title": "Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay",
          "BtnText": null,
          "QrImage": "https://static.mservice.io/img/momo-upload-api-201027104805-637393924852326436.jpeg"
      }
  ],
  "Error": null

};

export default (req, res) => {
  res.status(200).json(data);
};
