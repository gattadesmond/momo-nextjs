// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = {
  Result: true,
  Data: ["https://momo.page.link/aRHzpkyjNSixJSRk8"],
  Error: null,
};

export default (req, res) => {
  res.status(200).json(data);
};
