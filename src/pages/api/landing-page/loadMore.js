const data = { 
  "Result": true, 
  "Data": { 
    "Count": 4, 
    "LastIndex": 4, 
    "PageCount": 2, 
    "TotalItems": 6, 
    "Items": [
      { 
        "Title": "Rinh quà 500K cho khách hàng mới", 
        "Description": "Nhập HELLOMOMO và liên kết ngân hàng để nhận gói quà đến 500K", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-210719095859-637622855390110653.jpg", 
        "Link": "/lienkettaikhoan", 
        "OffDate": null 
      }, 
      { 
        "Title": "Giới Thiệu MoMo: Nhận quà đến 600K", 
        "Description": "Giới thiệu bạn bè nhận quà đến 600K, tham gia Thành Phố MoMo cùng chia 10 tỷ", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-210726121748-637628986685254908.jpg", 
        "Link": "/gioi-thieu-momo", 
        "OffDate": null 
      }, 
      { 
        "Title": "Đặt vé xem phim, mở ngay Ví MoMo", 
        "Description": "Đặt vé xem phim nhanh chóng, ưu đãi ngập tràn với Ví MoMo", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-200731093755-637317850750707761.jpg", 
        "Link": "/mua-ve-xem-phim-tai-vi-momo", 
        "OffDate": null 
      }, 
      { 
        "Title": "Nhận ưu đãi tiền lời 8%/năm và gói quà 1 triệu", 
        "Description": "Nhập TPMOMO \u0026 liên kết ngân hàng nhận quà khổng lồ 1 triệu và tiền lời 8%/năm", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-210712123113-637616898733307791.jpg", 
        "Link": "/tpmomo", 
        "OffDate": null 
      },
      { 
        "Title": "Đặt vé xem phim, mở ngay Ví MoMo", 
        "Description": "Đặt vé xem phim nhanh chóng, ưu đãi ngập tràn với Ví MoMo", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-200731093755-637317850750707761.jpg", 
        "Link": "/mua-ve-xem-phim-tai-vi-momo1", 
        "OffDate": null 
      }, 
      { 
        "Title": "Nhận ưu đãi tiền lời 8%/năm và gói quà 1 triệu", 
        "Description": "Nhập TPMOMO \u0026 liên kết ngân hàng nhận quà khổng lồ 1 triệu và tiền lời 8%/năm", 
        "Avatar": "https://static.mservice.io/img/momo-upload-api-210712123113-637616898733307791.jpg", 
        "Link": "/tpmomo1", 
        "OffDate": null 
      }
    ] 
  }, 
  "Error": null 
};

export default (req, res) => {
  res.status(200).json(data);
};
