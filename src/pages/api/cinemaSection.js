// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const master = {
  Result: true,
  Data: [
    {
      Id: 145,
      PageId: 13,
      Type: 2,
      TypeName: "Banner",
      Template: 1,
      DisplayOrder: 1,
      Title: "Banner",
      Short: null,
      ShortTitle: null,
      Content: "",
      BgColor: null,
      Data: {
        Items: [
          {
            Guid: "9726df85-2a4d-41c2-8b59-5ce33b299fb8",
            Name: "Main Banner",
            Description: null,
            DateShow: null,
            DateOff: null,
            Status: 1,
            Avatar:
              "https://static.mservice.io/img/momo-upload-api-210414100944-637539917842267830.jpg",
            AvatarMobile:
              "https://static.mservice.io/img/momo-upload-api-210414103358-637539932383195826.jpg",
            Url: null,
            IsNewTab: false,
            DisplayOrder: null,
          },
        ],
      },
    },
    {
      Id: 146,
      PageId: 13,
      Type: 1,
      TypeName: "Html",
      Template: 1,
      DisplayOrder: 2,
      Title: "Lịch chiếu theo địa điểm",
      Short: null,
      ShortTitle: null,
      Content:
        '<section class="bg-white">\n<div class="container ">\n\t<div class="row align-items-center no-gutters ">\n\t\t<div class="col-12 col-md-6 order-1 order-md-0 pr-lg-5 pt-4 pt-md-0">\n\t\t\t<h1 class="text-pink-momo">Vé Phim - Đặt vé trên Ví MoMo</h1>\n\n\t\t\t<div class="row">\n\t\t\t\t<div class="col-12 d-flex align-items-center mb-1">\n\t\t\t\t\t<img alt="" class="img-fluid" src="/momo2020/img/service/small-circle-icon.png"/>\n\t\t\t\t\t<h6 class="fw-400 text-gray-900 pl-2 pt-2">Dẫn đầu với tất cả các cụm rạp lớn trên cả nước</h6>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="col-12 d-flex align-items-center mb-1">\n\t\t\t\t\t<img alt="" class="img-fluid" src="/momo2020/img/service/small-circle-icon.png"/>\n\t\t\t\t\t<h6 class="fw-400 text-gray-900 pl-2 pt-2">Ưu đãi ngập tràn, luôn có khuyến mãi</h6>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="col-12 d-flex align-items-center mb-1">\n\t\t\t\t\t<img alt="" class="img-fluid" src="/momo2020/img/service/small-circle-icon.png"/>\n\t\t\t\t\t<h6 class="fw-400 text-gray-900 pl-2 pt-2">Quản lý thông tin chi tiết vé tiện lợi</h6>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="col-12 d-flex align-items-center mb-1">\n\t\t\t\t\t<img alt="" class="img-fluid" src="/momo2020/img/service/small-circle-icon.png"/>\n\t\t\t\t\t<h6 class="fw-400 text-gray-900 pl-2 pt-2">Thanh toán cực nhanh không phải chờ đợi</h6>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="mt-4 text-center text-md-left">\n\t\t\t\t<a class="d-none d-md-inline-block btn btn-primary-2 btn-sm py-1 font-weight-bold mr-2 bg-pink-momo fs-15 text-uppercase py-2" data-qrcode-id="2" data-target="#qrcode-modal-2" data-toggle="modal" href="javascript:void(0)">ĐẶT VÉ NGAY</a>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="col-12 col-md-6 order-0 order-md-1">\n\t\t\t<div class="position-relative align-items-center justify-content-center d-md-flex">\n\t\t\t\t<img alt="" class="rounded img-fluid shadow-lg" src="https://static.mservice.io/blogscontents/momo-upload-api-201223171230-637443403504267297.jpg"/>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n</section>\n',
    },
    {
      Id: 208,
      PageId: 13,
      Type: 1,
      TypeName: "Html",
      Template: 1,
      DisplayOrder: 100,
      Title: "USP",
      Short: null,
      ShortTitle: null,
      Content:
        '<section class="bg-pink-light bg-cover pb-0">\n<div class="container overflow-hidden">\n\t<div class="row justify-content-center">\n\t\t<div class="col-12 col-md-10 col-lg-8">\n\t\t\t<div class="text-center section__title">\n\t\t\t\t<h2 class="h2 font-weight-bold text-primary-2">Trải nghiệm thế giới điện ảnh màu sắc trên Ví MoMo</h2>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div class="row justify-content-center sm-gutters reason__service ">\n\t\t<div class="col-12 col-md-6 col-lg-4 order-1 order-md-1 reason__item__left">\n\t\t\t<div class="reason__item remove_maxwith align-items-start aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">\n\t\t\t\t<div class="reason__item__avatar">\n\t\t\t\t\t<img alt="" src="https://static.mservice.io/blogscontents/momo-upload-api-201221170623-637441671835547961.png" style="height: 100px; width: 100px;"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="reason__item__content">\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<h3 class="text-pink-hot fw-700 h6">Đặt vé cực nhanh</h3>\n\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tChọn loại vé và cụm rạp yêu thích một cách nhanh chóng và trực quan trên Ví MoMo. Không cần mất nhiều thao tác chỉ cần ra rạp và xem phim.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="reason__item remove_maxwith align-items-start aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">\n\t\t\t\t<div class="reason__item__avatar">\n\t\t\t\t\t<img alt="" src="https://static.mservice.io/blogscontents/momo-upload-api-201221170629-637441671894985967.png" style="height: 100px; width: 100px;"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="reason__item__content">\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<h3 class="text-pink-hot fw-700 h6">Thêm bắp nước</h3>\n\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tChọn ngay combo phù hợp để những buổi xem phim thêm cảm xúc. Dù là một mình hay cùng hội bạn, Ví MoMo cũng có đầy đủ các loại combo bắp nước cho bạn nha.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="col-12 col-md-4 order-3 order-md-2 col-xl-auto align-self-center d-none d-lg-block">\n\t\t\t<div class="position-relative mt-md-5 mt-lg-0 reason__item__phone">\n\t\t\t\t<img alt="Đặt mua vé máy bay trên Ví MoMo Siêu nhanh - Siêu tiện - Siêu ưu đãi" class="img-fluid d-block mx-auto lazyloaded" src="https://static.mservice.io/blogscontents/momo-upload-api-201222135613-637442421730723240.png" style="width: 330px; height: 480px;"/>\n\t\t\t\t<div class="reason__cta">\n\t\t\t\t\t<a class="hocvien-btn" href="https://momo.vn/download#service" target="_blank">\n\t\t\t\t\t<img alt="" class="img-fluid d-block mx-auto" src="https://static.mservice.io/fileuploads/svg/momo-file-201201152229.svg" width="142"/>\n\t\t\t\t\t</a>\n\t\t\t\t\t<style type="text/css">.hocvien-btn {\n                display: inline-block;\n                transition: all 0.2s ease-in;\n                opacity: 0.9;\n              }\n\n              .hocvien-btn:hover {\n                opacity: 1;\n                transform: translate3d(0, -3px, 0);\n              }\n\n              @media (max-width: 767.98px) {\n                .hocvien-btn {\n                  width: 130px;\n                }\n              }\n\t\t\t\t\t</style>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class="col-12 col-md-6 col-lg-4 order-2 order-md-3 reason__item__right">\n\t\t\t<div class="reason__item remove_maxwith align-items-start aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">\n\t\t\t\t<div class="reason__item__avatar">\n\t\t\t\t\t<img alt="" src="https://static.mservice.io/blogscontents/momo-upload-api-201221170641-637441672015903155.png" style="width: 100px; height: 100px;"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="reason__item__content">\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<h3 class="text-pink-hot fw-700 h6">Đánh giá - Nhận xét phim</h3>\n\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tĐưa ra cảm nghĩ của bạn về những bộ phim đang chiếu ngoài rạp và trở thành một reviewer thực thụ. Đây cũng là nơi để bạn tham khảo về những bộ phim một cách thực tế chính xác nhất.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class="reason__item remove_maxwith align-items-start aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">\n\t\t\t\t<div class="reason__item__avatar">\n\t\t\t\t\t<img alt="" src="https://static.mservice.io/blogscontents/momo-upload-api-201221170647-637441672079250614.png" style="width: 100px; height: 100px;"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class="reason__item__content">\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<h3 class="text-pink-hot fw-700 h6">Ưu đãi ngập tràn</h3>\n\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tRạp nào cũng có giá vé hời từ trong tuần đến cuối tuần. Đặc biệt còn dành cho cả bạn cũ và bạn mới, chỉ cần đặt vé ưu đãi sẽ tự động áp dụng.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n</section>\n',
    },
  ],
  Error: null,
};

export default (req, res) => {
  res.status(200).json(master);
};
