// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data =
{
  "Result": true,
  "Data": {
    "Id": 586,
    "Title": "1 triệu suất ăn nóng hổi và thức uống mát lạnh tại CircleK",
    "Content": "<h2 style=\"text-align: center;\">\n<img alt=\"\" src=\"https://static.mservice.io/blogscontents/momo-upload-api-770x370-181022215652.jpg\" style=\"width: 770px; height: 370px;\"/>\n</h2>\n\n<h2><strong>Thông tin chương trình</strong></h2>\n\n<p>\n\t- Thời gian: 22/10 – 29/10/2018.<br/>\n\t- Đối tượng: 1 triệu khách hàng may mắn từ thành viên Bạc trở lên tại khu vực Hồ Chí Minh, Hà Nội.<br/>\n\t- Giá trị voucher: 30.000đ/voucher.\n</p>\n\n<h2><strong>Nội dung khuyến mãi</strong></h2>\n\n<p>\n\t- Ví MoMo bất ngờ dành tặng khách hàng 1 TRIỆU suất ăn nóng hổi và nước uống mát lạnh bất kỳ với giá chỉ 1 ĐỒNG.<br/>\n\t- Thẻ quà tặng trị giá 30.000đ.\n</p>\n\n<h2><strong>Quy định thẻ quà tặng</strong></h2>\n\n<p>\n\t- Áp dụng cho tất cả khách hàng có Ví MoMo và may mắn nhận được thẻ quà.<br/>\n\t- Chỉ áp dụng cho các suất ăn/ nước uống tại CircleK (tối đa 30.000đ) và thanh toán bằng Ví MoMo.<br/>\n\t- Ví MoMo sẽ bị trừ 1 ĐỒNG với các hóa đơn có giá trị nhỏ hơn hoặc bằng giá trị thẻ quà.<br/>\n\t- Hạn sử dụng: 7 ngày kể từ ngày nhận. Sau thời gian trên, hệ thống sẽ tự động thu hồi thẻ quà.<br/>\n\t- Có thể sử dụng đồng thời với các mã khuyến mãi khác của CircleK.<br/>\n\t- Không được chuyển nhượng và không có giá trị quy đổi thành tiền mặt.<br/>\n\t- Chỉ có giá trị thanh toán 01 lần.<br/>\n\t<help-article data-helpid=\"142\" data-link=\"#\" data-title=\"Hướng Dẫn Nhận và Sử Dụng Thẻ Quà Tặng CircleK\" data-type=\"2\" style=\"color:red\" title=\"Hướng dẫn:Hướng dẫn sử dụng thẻ quà tặng ăn uống tại Circle K giá chỉ 1Đ\">Hướng dẫn:Hướng dẫn sử dụng thẻ quà tặng ăn uống tại Circle K giá chỉ 1Đ</help-article>\n</p>\n\n<div class=\"text-center mb-3 ckemm_cta\">\n\t<a class=\"ckemm_cta_btn desktop d-none d-md-inline-block btn btn-primary-2 btn-pulse\" data-qrcode-id=\"10\" data-target=\"#qrcode-modal-10\" data-toggle=\"modal\" href=\"javascript:void(0)\" target=\"_blank\">Xem ngay</a>\n\n\t<div class=\"ckemm_cta_des d-none\">\n\t\t<b>Quét mã để quyên góp</b> <a href=\"https://momoapp.page.link/SWQ3QXcWrygU7EAN8\">https://momoapp.page.link/SWQ3QXcWrygU7EAN8</a>\n\t</div>\n</div>\n\n<h2><strong>Hình ảnh sản phẩm</strong></h2>\n\n<p>\n\tNếu đã quá đau đầu với câu chuyện “ăn gì cũng được”, hãy để MoMo và menu món-nào-cũng-ngon của CircleK giải quyết giúp bạn.\n</p>\n\n<div class=\"row no-gutters mt-4 mb-4\">\n\t<div class=\"col-12\">\n\t\t<div class=\"swiper-container gallery-top\">\n\t\t\t<div class=\"swiper-wrapper\">\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-lonxanh-do-181023140240.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"post-slider-caption\">\n\t\t\t\t\t\tTrời nóng? – Mua cho nàng một lon nước ngọt mát lạnh sảng khoái tinh thần.\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-xucxich-6-181023140313.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"post-slider-caption\">\n\t\t\t\t\t\tTrời mưa? – Tặng nàng xúc xích chính chủ CircleK vừa dai vừa dai, vừa đủ chất thơm ngon.\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-banh-bao-181023140337.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"post-slider-caption\">\n\t\t\t\t\t\tThưởng thức một cặp bánh bao nóng hổi, giá chỉ 1 đồng tại CircleK.\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-bia-nuocngot-181023140431.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"post-slider-caption\">\n\t\t\t\t\t\tThưởng thức bia - nước ngọt, giá chỉ 1 đồng tại CircleK.\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-nuoc-suoi-181023140447.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"post-slider-caption\">\n\t\t\t\t\t\t\"Chống đuối\" với nước suối, giá chỉ 1 đồng tại CircleK.\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-chuoi3-181023140551.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-banhgio-181023140617.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img class=\"swiper-lazy \" data-src=\"https://static.mservice.io/blogscontents/momo-upload-api-banhmiopla-181023140645.jpg\"/>\n\t\t\t\t\t<div class=\"swiper-lazy-preloader \">\n\t\t\t\t\t\t \n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"swiper-button-next swiper-button-white\">\n\t\t\t\t \n\t\t\t</div>\n\n\t\t\t<div class=\"swiper-button-prev swiper-button-white\">\n\t\t\t\t \n\t\t\t</div>\n\t\t</div>\n\n\t\t<div class=\"swiper-container gallery-thumbs\">\n\t\t\t<div class=\"swiper-wrapper\">\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-lonxanh-do-181023140240.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-xucxich-6-181023140313.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-banh-bao-181023140337.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-bia-nuocngot-181023140431.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-nuoc-suoi-181023140447.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-chuoi3-181023140551.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-banhgio-181023140617.jpg\"/>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"swiper-slide\">\n\t\t\t\t\t<img src=\"https://static.mservice.io/blogscontents/momo-upload-api-banhmiopla-181023140645.jpg\"/>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n\n<p>\n\t<help-article data-helpid=\"1491\" data-link=\"www\" data-title=\"Nhóm Hướng dẫn\" data-type=\"1\" style=\"color:red\" title=\"Nhóm hướng dẫn:Hướng Dẫn Liên Kết Ví Với Tài Khoản Ngân Hàng SeABank\">Nhóm hướng dẫn:Hướng Dẫn Liên Kết Ví Với Tài Khoản Ngân Hàng SeABank</help-article>\n</p>\n\n<p>\n\t<question-article data-questid=\"1329\" data-title=\"Hỏi đáp\" data-type=\"2\" style=\"color:red\" title=\"Hỏi Đáp:Cách liên kết MoMo với tài khoản ngân hàng Indovina\">Hỏi Đáp:Cách liên kết MoMo với tài khoản ngân hàng Indovina</question-article>\n</p>\n\n<p>\n\t<question-article data-questid=\"237\" data-title=\"Nhóm hỏi đáp\" data-type=\"1\" style=\"color:red\" title=\"Nhóm Hỏi Đáp:Hỏi đáp về Ví Trả Sau\">Nhóm Hỏi Đáp:Hỏi đáp về Ví Trả Sau</question-article>\n</p>\n",
    "Short": "Nằm trong khuôn khổ “Lễ hội thanh toán không tiền mặt”, từ nay đến hết ngày 29/10/2018, Ví MoMo và CircleK tặng ngay 1 triệu suất ăn nóng hổi và thức uống mát lạnh cho khách hàng là thành viên Bạc của MoMo trở lên tại TP. HCM, Hà Nội trong suốt một tuần vàng ưu đãi.",
    "CreatedBy": 0,
    "PublishDate": "21/10/2018",
    "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-770x370-181022215709.jpg",
    "Link": "/tin-tuc/khuyen-mai/1-trieu-suat-an-nong-hoi-va-thuc-uong-mat-lanh-tai-circlek-586",
    "TotalViews": 12384,
    "Type": 0,
    "MenuItems": [],
    "Tags": [
      {
        "Id": 19,
        "Name": "khuyến mãi MoMo",
        "Link": "/tags/khuyen-mai-momo"
      }
    ],
    "Latest": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/combo-4-the-qua-ahamove-giam-den-50-dang-doi-ban-1921",
          "Title": "Combo 4 thẻ quà AhaMove giảm đến 50% đang đợi bạn",
          "ShortContent": "Tham gia ngay Thành phố MoMo giải đố những câu hỏi vui, bổ ích cùng tranh giải thưởng 10 TỶ đồng và nhận siêu combo 4 thẻ quà AhaMove đặt giao hàng giá cực hời.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210728115602-637630701626269092.jpg",
          "Id": 1921,
          "TotalView": 32,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "21 giờ trước",
          "Date": "2021-07-28 11:53:34",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/giai-do-cuc-vui-chat-day-tui-trieu-the-qua-hoan-tien-tu-tui-than-tai-va-vi-tra-sau-1916",
          "Title": "Giải đố cực vui, chất đầy túi triệu thẻ quà hoàn tiền từ Túi Thần Tài và Ví Trả Sau",
          "ShortContent": "Hàng triệu thẻ quà Túi Thần Tài đang chờ bạn tại Thành phố MoMo. Tranh tài giải đố cực vui, nhận tiền thưởng đầy Túi. Để MoMo bật mí những phần quà hot từ Túi Thần Tài, Ví Trả Sau và hướng dẫn bạn cách sử dụng nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210723173554-637626585547995252.jpg",
          "Id": 1916,
          "TotalView": 237,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "26/07/2021",
          "Date": "2021-07-26 15:08:45",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/deal-doi-hap-dan-an-tam-luu-tru-1915",
          "Title": "Deal đôi hấp dẫn, an tâm lưu trú",
          "ShortContent": "MoMo hỗ trợ bạn 2 deal đặt phòng hấp dẫn với duy nhất 1 mã, thêm an tâm lưu trú!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210723161741-637626538616089389.jpg",
          "Id": 1915,
          "TotalView": 155,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "23/07/2021",
          "Date": "2021-07-23 16:26:22",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/dang-ky-hoi-vien-bong-sen-vang-tang-the-bac-tang-ngan-dam-thuong-tan-huong-uu-dai-momo-1914",
          "Title": "Đăng ký hội viên Bông Sen Vàng: Tặng thẻ bạc, Tặng ngàn dặm thưởng - Tận hưởng ưu đãi MoMo",
          "ShortContent": "Không chỉ được tận hưởng đặc quyền trong chuyến bay Vietnam Airlines, trở thành hội viên Bông Sen Vàng còn mang tới cho bạn nhiều ưu đãi ngay khi đặt vé trên ứng dụng MoMo. Đăng ký hội viên và cất cánh ngay nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210723111904-637626359443294854.jpg",
          "Id": 1914,
          "TotalView": 221,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "23/07/2021",
          "Date": "2021-07-23 12:51:14",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 685,
      "Count": 4,
      "LastIdx": 4,
      "ExcludeIds": [
        586
      ]
    },
    "Relatings": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/ve-tau-truc-tuyen-di-chuyen-an-toan-ho-tro-35000d-1913",
          "Title": "Vé tàu trực tuyến - Di chuyển an toàn: Hỗ trợ 35.000đ",
          "ShortContent": "Hạn chế tiếp xúc bằng cách đặt vé tàu trực tuyến và nhận thêm hỗ trợ từ Ví MoMo. Nhập mã “TAU35” để nhận ưu đãi 35.000đ cho lần đầu đặt vé.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210723104920-637626341601239902.jpg",
          "Id": 1913,
          "TotalView": 55,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "23/07/2021",
          "Date": "2021-07-23 12:49:09",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/quet-vi-tra-sau-di-cho-tien-loi-hoan-them-20000d-tai-bach-hoa-xanh-va-7eleven-1911",
          "Title": "Quét Ví Trả Sau đi chợ tiện lợi, hoàn thêm 20.000đ tại Bách Hóa Xanh và 7-Eleven",
          "ShortContent": "Tiện ích Ví Trả Sau giúp bạn chi tiêu trước, thanh toán sau cực kỳ tiện lợi và thiết thực trong mùa dịch. Kích hoạt Ví ngay hôm nay để mua sắm nhu yếu phẩm tại Bách Hóa Xanh hoặc 7-Eleven. Đặc biệt: hoàn ngay 20.000đ cho khách hàng lần đầu dùng Ví từ nay đến hết 17/08/2021.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210722130806-637625560864110279.jpg",
          "Id": 1911,
          "TotalView": 353,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "22/07/2021",
          "Date": "2021-07-22 14:03:41",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/giai-ngan-cao-nhan-qua-khung-toi-500k-cho-khach-hang-oncredit-tren-momo-1910",
          "Title": "Giải ngân cao – nhận quà khủng tới 500.000đ cho khách hàng OnCredit trên MoMo",
          "ShortContent": "Bạn có biết, có rất nhiều khuyến mãi và phần thưởng đang đợi khi bạn chọn giải ngân khoản vay OnCredit qua MoMo! Cơ hội nhận thẻ quà tới 500.000 đồng mỗi tuần. Tham gia ngay!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210722124831-637625549115860921.jpg",
          "Id": 1910,
          "TotalView": 497,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "22/07/2021",
          "Date": "2021-07-22 14:02:58",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/mien-phi-giao-dich-khi-lien-ket-the-quoc-te-voi-momo-1906",
          "Title": "Miễn phí giao dịch khi liên kết thẻ quốc tế với MoMo",
          "ShortContent": "Chung tay phòng dịch, nhận quà thật thích cùng MoMo! Từ ngày 21/07, chỉ cần liên kết thẻ Visa/Master/JCB với Ví MoMo, bạn sẽ nhận ngay ưu đãi MIỄN PHÍ 5 LẦN TRONG THÁNG khi Nạp tiền vào Ví hoặc Thanh toán dịch vụ trên MoMo. Liên kết để nhận ưu đãi ngay.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210720111019-637623762197187900.png",
          "Id": 1906,
          "TotalView": 224,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "21/07/2021",
          "Date": "2021-07-21 18:14:14",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 681,
      "Count": 4,
      "LastIdx": 4,
      "ExcludeIds": [
        1921,
        1916,
        1915,
        1914,
        586
      ]
    },
    "CtaData": [
      {
        "Id": 10,
        "IsNewTab": true,
        "QrLink": "https://momoapp.page.link/SWQ3QXcWrygU7EAN8",
        "ShortTitle": "Quyên góp",
        "Title": "Quét mã để quyên góp",
        "BtnText": "Xem ngay",
        "QrImage": "https://static.mservice.io/img/momo-upload-api-201112160002-637407936029836406.png"
      }
    ],
    "QaData": [
      {
        "Id": 1329,
        "Title": "Hỏi đáp",
        "Data": {
          "Id": 1329,
          "Title": "Cách liên kết MoMo với tài khoản ngân hàng Indovina",
          "Short": null,
          "Content": "&lt;p&gt;Để li&#234;n kết ng&#226;n h&#224;ng, bạn vui l&#242;ng thao t&#225;c những bước sau:&lt;/p&gt;&lt;p&gt;Bước 1: Chọn &quot;V&#237; của t&#244;i&quot;&lt;/p&gt;&lt;p&gt;Bước 2: Chọn &quot;Li&#234;n kết t&#224;i khoản&quot;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Nhấn chọn v&#224;o &quot;Li&#234;n kết t&#224;i khoản&quot; tr&#234;n m&#224;n h&#236;nh quản l&#253;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Bước 3: Chọn &quot;Indovina&quot;&lt;br/&gt;Nhấn chọn logo Indovina tr&#234;n m&#224;n h&#236;nh&lt;/p&gt;&lt;p&gt;Bước 4: Nhập th&#244;ng tin thẻ/ t&#224;i khoản&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Nhập c&#225;c th&#244;ng tin bao gồm:&#160;&lt;ul&gt;&lt;li&gt;Số thẻ/ t&#224;i khoản&lt;/li&gt;&lt;li&gt;Họ t&#234;n chủ thẻ/t&#224;i khoản&lt;/li&gt;&lt;li&gt;CMND/CCCD/Hộ chiếu&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Lưu &#253;: C&#225;c th&#244;ng tin tr&#234;n v&#224; số V&#237; MoMo cần tr&#249;ng với số điện thoại đăng k&#237; tại Indovina&lt;/p&gt;&lt;p&gt;Bước 5: Nhập m&#227; OTP được gửi đến điện thoại của bạn v&#224;o &#244;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;M&#227; OTP được gửi đến qua tin nhắn ng&#226;n h&#224;ng, vui l&#242;ng kiểm tra v&#224; nhập m&#227; v&#224;o &#244;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&#160;&lt;/p&gt;",
          "MobileContent": null,
          "Link": "/hoi-dap/cach-lien-ket-momo-voi-tai-khoan-ngan-hang-indovina"
        }
      }
    ],
    "QaGroupData": [
      {
        "Id": 237,
        "Title": "Nhóm hỏi đáp",
        "Data": [
          {
            "Id": 237,
            "Name": "Hỏi đáp về Ví Trả Sau",
            "DisplayOrder": 5,
            "Items": [
              {
                "Id": 1317,
                "Title": "Ví Trả Sau là gì?",
                "Short": "&lt;p&gt;V&#237; Trả Sau l&#224; sản phẩm cho vay ti&#234;u d&#249;ng kh&#244;ng c&#243; t&#224;i sản bảo đảm, do TPBank cung cấp với h&#236;nh thức cấp hạn mức vay cho Kh&#225;ch H&#224;ng theo Hợp Đồng T&#237;n Dụng.&lt;/p&gt;&lt;p&gt;Kh&#225;ch H&#224;ng c&#243; thể sử dụng hạn mức vay của V&#237; Trả Sau để thanh to&#225;n trước một số dịch vụ c&#243; tr&#234;n ứng dụng V&#237; MoMo v&#224; trả lại v&#224;o th&#225;ng sau. Ngo&#224;i ra Kh&#225;ch H&#224;ng c&#242;n c&#243; những lựa chọn trả g&#243;p hằng th&#225;ng ph&#249; hợp với khả năng chi trả của mỗi người.&lt;/p&gt;",
                "Content": "&lt;p&gt;V&#237; Trả Sau l&#224; sản phẩm cho vay ti&#234;u d&#249;ng kh&#244;ng c&#243; t&#224;i sản bảo đảm, do TPBank cung cấp với h&#236;nh thức cấp hạn mức vay cho Kh&#225;ch H&#224;ng theo Hợp Đồng T&#237;n Dụng.&lt;/p&gt;&lt;p&gt;Kh&#225;ch H&#224;ng c&#243; thể sử dụng hạn mức vay của V&#237; Trả Sau để thanh to&#225;n trước một số dịch vụ c&#243; tr&#234;n ứng dụng V&#237; MoMo v&#224; trả lại v&#224;o th&#225;ng sau. Ngo&#224;i ra Kh&#225;ch H&#224;ng c&#242;n c&#243; những lựa chọn trả g&#243;p hằng th&#225;ng ph&#249; hợp với khả năng chi trả của mỗi người.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/vi-tra-sau-la-gi",
                "GenerateSeo": true
              },
              {
                "Id": 1318,
                "Title": "Cách đăng ký / kích hoạt Ví Trả Sau",
                "Short": "&lt;p&gt;Để k&#237;ch hoạt V&#237; Trả Sau, bạn mở app MoMo v&#224; thực hiện c&#225;c bước sau:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; T&#236;m kiếm &quot;V&#237; Trả Sau&quot; tr&#234;n thanh t&#236;m kiếm, chọn &quot;V&#237; Trả Sau&quot;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Điền th&#244;ng tin đăng k&#253; theo mẫu, sau đ&#243; nhấn &quot;Tiếp Tục&quot;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Nhập m&#227; OTP để x&#225;c thực k&#253; kết Hợp Đồng về hạn mức vay. M&#227; OTP sẽ được gửi bởi TPBank đến số điện thoại bạn đăng k&#253; qua SMS&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Bạn tham khảo th&#234;m hướng dẫn chi tiết bằng h&#236;nh ảnh tại đ&#226;y nh&#233;: &lt;a href=&quot;http://momo.vn/huong-dan/huong-dan-dang-ky-va-su-dung-vi-tra-sau-tren-momo#servicepage&quot; target=&quot;_blank&quot;&gt;Hướng dẫn k&#237;ch hoạt V&#237; Trả Sau&lt;/a&gt;&lt;/p&gt;",
                "Content": "&lt;p&gt;Để k&#237;ch hoạt V&#237; Trả Sau, bạn mở app MoMo v&#224; thực hiện c&#225;c bước sau:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; T&#236;m kiếm &quot;V&#237; Trả Sau&quot; tr&#234;n thanh t&#236;m kiếm, chọn &quot;V&#237; Trả Sau&quot;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Điền th&#244;ng tin đăng k&#253; theo mẫu, sau đ&#243; nhấn &quot;Tiếp Tục&quot;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Nhập m&#227; OTP để x&#225;c thực k&#253; kết Hợp Đồng về hạn mức vay. M&#227; OTP sẽ được gửi bởi TPBank đến số điện thoại bạn đăng k&#253; qua SMS&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Bạn tham khảo th&#234;m hướng dẫn chi tiết bằng h&#236;nh ảnh tại đ&#226;y nh&#233;: &lt;a href=&quot;http://momo.vn/huong-dan/huong-dan-dang-ky-va-su-dung-vi-tra-sau-tren-momo#servicepage&quot; target=&quot;_blank&quot;&gt;Hướng dẫn k&#237;ch hoạt V&#237; Trả Sau&lt;/a&gt;&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/cach-dang-ky-kich-hoat-vi-tra-sau",
                "GenerateSeo": true
              },
              {
                "Id": 1319,
                "Title": "Ví Trả Sau có bao nhiêu loại hạn mức?",
                "Short": "&lt;p&gt;Hiện tại, V&#237; Trả Sau đang cấp cho Kh&#225;ch H&#224;ng hạn mức từ 1.000.000đ - 5.000.000đ.&lt;/p&gt;&lt;p&gt;Cho lần đầu k&#237;ch hoạt V&#237; Trả Sau, hạn mức được ph&#234; duyệt tự động trong khoảng từ 1.000.000đ - 5.000.000đ. Kh&#225;ch H&#224;ng sẽ được x&#233;t duyệt tăng hạn mức nếu c&#243; lịch sử giao dịch tốt.&lt;/p&gt;",
                "Content": "&lt;p&gt;Hiện tại, V&#237; Trả Sau đang cấp cho Kh&#225;ch H&#224;ng hạn mức từ 1.000.000đ - 5.000.000đ.&lt;/p&gt;&lt;p&gt;Cho lần đầu k&#237;ch hoạt V&#237; Trả Sau, hạn mức được ph&#234; duyệt tự động trong khoảng từ 1.000.000đ - 5.000.000đ. Kh&#225;ch H&#224;ng sẽ được x&#233;t duyệt tăng hạn mức nếu c&#243; lịch sử giao dịch tốt.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/vi-tra-sau-co-bao-nhieu-loai-han-muc",
                "GenerateSeo": true
              },
              {
                "Id": 1320,
                "Title": "Cách thanh toán bằng Ví Trả Sau trên Ứng dụng Ví MoMo",
                "Short": "&lt;p&gt;&lt;strong&gt;Để sử dụng V&#237; Trả Sau l&#224;m phương thức thanh to&#225;n&lt;/strong&gt; tr&#234;n V&#237; MoMo bạn thực hiện c&#225;c bước đơn giản như sau:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; Chọn dịch vụ bạn cần thanh to&#225;n v&#224; chọn thanh to&#225;n qua V&#237; MoMo (kh&#244;ng &#225;p dụng cho c&#225;c dịch vụ thanh to&#225;n khoản vay v&#224; nạp tiền t&#224;i xế v&#224; một số dịch vụ kh&#225;c chưa hỗ trợ nguồn tiền n&#224;y)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Ở m&#224;n h&#236;nh thanh to&#225;n an to&#224;n, thay đổi nguồn tiền th&#224;nh V&#237; Trả Sau&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Bấm x&#225;c nhận để ho&#224;n tất thanh to&#225;n dịch vụ&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Bạn tham khảo th&#234;m hướng dẫn chi tiết bằng h&#236;nh ảnh tại đ&#226;y nh&#233;: &lt;a href=&quot;http://momo.vn/huong-dan/huong-dan-dang-ky-va-su-dung-vi-tra-sau-tren-momo#servicepage&quot; target=&quot;_blank&quot;&gt;Hướng dẫn thanh to&#225;n bằng V&#237; Trả Sau&lt;/a&gt;&lt;/p&gt;",
                "Content": "&lt;p&gt;&lt;strong&gt;Để sử dụng V&#237; Trả Sau l&#224;m phương thức thanh to&#225;n&lt;/strong&gt; tr&#234;n V&#237; MoMo bạn thực hiện c&#225;c bước đơn giản như sau:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt; Chọn dịch vụ bạn cần thanh to&#225;n v&#224; chọn thanh to&#225;n qua V&#237; MoMo (kh&#244;ng &#225;p dụng cho c&#225;c dịch vụ thanh to&#225;n khoản vay v&#224; nạp tiền t&#224;i xế v&#224; một số dịch vụ kh&#225;c chưa hỗ trợ nguồn tiền n&#224;y)&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Ở m&#224;n h&#236;nh thanh to&#225;n an to&#224;n, thay đổi nguồn tiền th&#224;nh V&#237; Trả Sau&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Bấm x&#225;c nhận để ho&#224;n tất thanh to&#225;n dịch vụ&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Bạn tham khảo th&#234;m hướng dẫn chi tiết bằng h&#236;nh ảnh tại đ&#226;y nh&#233;: &lt;a href=&quot;http://momo.vn/huong-dan/huong-dan-dang-ky-va-su-dung-vi-tra-sau-tren-momo#servicepage&quot; target=&quot;_blank&quot;&gt;Hướng dẫn thanh to&#225;n bằng V&#237; Trả Sau&lt;/a&gt;&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/cach-thanh-toan-bang-vi-tra-sau-tren-ung-dung-vi-momo",
                "GenerateSeo": true
              },
              {
                "Id": 1310,
                "Title": "Tôi sẽ thanh toán dư nợ Ví Trả Sau như thế nào?",
                "Short": "&lt;p&gt;&lt;strong&gt;Hướng dẫn thanh to&#225;n dư nợ khoản vay V&#237; Trả Sau:&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt;&#160;Bạn c&#243; thể chọn “Thanh to&#225;n ngay” ở m&#224;n h&#236;nh trang chủ của V&#237; Trả Sau hoặc chọn “Danh s&#225;ch h&#243;a đơn” ở m&#224;n h&#236;nh trang chủ V&#237; Trả Sau để xem c&#225;c h&#243;a đơn cần thanh to&#225;n rồi bấm “Thanh to&#225;n”&#160;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Kiểm tra th&#244;ng tin h&#243;a đơn, chỉnh sửa số tiền cần thanh to&#225;n. Bạn c&#243; thể thanh to&#225;n nhiều lần cho 1 khoản dư nợ. Số tiền thanh to&#225;n tối thiểu l&#224; 1.000đ. Sau đ&#243;, chọn “Tiếp tục”&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Nhận “X&#225;c nhận” để thanh to&#225;n dư nợ. Lưu &#253;: Ph&#237; dịch vụ chỉ ph&#225;t sinh 1 lần duy nhất trong th&#225;ng khi bạn sử dụng V&#237; Trả Sau để thanh to&#225;n giao dịch.&lt;/p&gt;",
                "Content": "&lt;p&gt;&lt;strong&gt;Hướng dẫn thanh to&#225;n dư nợ khoản vay V&#237; Trả Sau:&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt;&#160;Bạn c&#243; thể chọn “Thanh to&#225;n ngay” ở m&#224;n h&#236;nh trang chủ của V&#237; Trả Sau hoặc chọn “Danh s&#225;ch h&#243;a đơn” ở m&#224;n h&#236;nh trang chủ V&#237; Trả Sau để xem c&#225;c h&#243;a đơn cần thanh to&#225;n rồi bấm “Thanh to&#225;n”&#160;&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt; Kiểm tra th&#244;ng tin h&#243;a đơn, chỉnh sửa số tiền cần thanh to&#225;n. Bạn c&#243; thể thanh to&#225;n nhiều lần cho 1 khoản dư nợ. Số tiền thanh to&#225;n tối thiểu l&#224; 1.000đ. Sau đ&#243;, chọn “Tiếp tục”&lt;/p&gt;&lt;p&gt;&lt;strong&gt;Bước 3:&lt;/strong&gt; Nhận “X&#225;c nhận” để thanh to&#225;n dư nợ. Lưu &#253;: Ph&#237; dịch vụ chỉ ph&#225;t sinh 1 lần duy nhất trong th&#225;ng khi bạn sử dụng V&#237; Trả Sau để thanh to&#225;n giao dịch.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/toi-se-thanh-toan-du-no-vi-tra-sau-nhu-the-nao",
                "GenerateSeo": true
              },
              {
                "Id": 1312,
                "Title": "Có thể thanh toán nhiều lần cho khoản vay Ví Trả Sau được không?",
                "Short": "&lt;p&gt;Kh&#225;ch H&#224;ng c&#243; thể &lt;strong&gt;thanh to&#225;n khoản vay bất kỳ l&#250;c n&#224;o&lt;/strong&gt;.&lt;/p&gt;",
                "Content": "&lt;p&gt;Kh&#225;ch H&#224;ng c&#243; thể &lt;strong&gt;thanh to&#225;n khoản vay bất kỳ l&#250;c n&#224;o&lt;/strong&gt;.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/co-the-thanh-toan-nhieu-lan-cho-khoan-vay-vi-tra-sau-duoc-khong",
                "GenerateSeo": true
              },
              {
                "Id": 1313,
                "Title": "Tôi có bị phạt khi thanh toán quá hạn Ví Trả Sau không?",
                "Short": "&lt;p&gt;Nếu thanh to&#225;n qu&#225; hạn, Kh&#225;ch H&#224;ng sẽ chịu th&#234;m phần Ph&#237; chậm trả (Ph&#237; chậm trả sẽ được t&#237;nh dựa theo số ng&#224;y qu&#225; hạn của Kh&#225;ch H&#224;ng).&lt;/p&gt;&lt;p&gt;Trong trường hợp h&#243;a đơn bị qu&#225; hạn 5 ng&#224;y, V&#237; Trả Sau sẽ bị kh&#243;a t&#237;nh năng thanh to&#225;n. Kh&#225;ch H&#224;ng cần thanh to&#225;n to&#224;n bộ dư nợ của th&#225;ng qu&#225; hạn để tiếp tục sử dụng dịch vụ.&#160;&lt;/p&gt;&lt;p&gt;Lưu &#253;: Thời hạn kh&#243;a V&#237; Trả Sau (Qu&#225; hạn 5 ng&#224;y) c&#243; thể thay đổi theo từng thời kỳ.&lt;/p&gt;&lt;p&gt;Ngo&#224;i ra, chi tiết quy định về xử l&#253; c&#225;c khoản vay qu&#225; hạn xem tại Hợp đồng t&#237;n dụng.&lt;/p&gt;",
                "Content": "&lt;p&gt;Nếu thanh to&#225;n qu&#225; hạn, Kh&#225;ch H&#224;ng sẽ chịu th&#234;m phần Ph&#237; chậm trả (Ph&#237; chậm trả sẽ được t&#237;nh dựa theo số ng&#224;y qu&#225; hạn của Kh&#225;ch H&#224;ng).&lt;/p&gt;&lt;p&gt;Trong trường hợp h&#243;a đơn bị qu&#225; hạn 5 ng&#224;y, V&#237; Trả Sau sẽ bị kh&#243;a t&#237;nh năng thanh to&#225;n. Kh&#225;ch H&#224;ng cần thanh to&#225;n to&#224;n bộ dư nợ của th&#225;ng qu&#225; hạn để tiếp tục sử dụng dịch vụ.&#160;&lt;/p&gt;&lt;p&gt;Lưu &#253;: Thời hạn kh&#243;a V&#237; Trả Sau (Qu&#225; hạn 5 ng&#224;y) c&#243; thể thay đổi theo từng thời kỳ.&lt;/p&gt;&lt;p&gt;Ngo&#224;i ra, chi tiết quy định về xử l&#253; c&#225;c khoản vay qu&#225; hạn xem tại Hợp đồng t&#237;n dụng.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/toi-co-bi-phat-khi-thanh-toan-qua-han-vi-tra-sau-khong",
                "GenerateSeo": true
              },
              {
                "Id": 1321,
                "Title": "Sử dụng Ví Trả Sau có an toàn không?",
                "Short": "&lt;p&gt;Th&#244;ng tin bạn khoản vay sẽ &lt;strong&gt;được bảo mật v&#224; kh&#244;ng tiết lộ cho c&#225;c b&#234;n thứ 3&lt;/strong&gt; trong qu&#225; tr&#236;nh đăng k&#253; vay.&lt;/p&gt;&lt;p&gt;Mọi giao dịch nhận tiền giải ng&#226;n v&#224; thanh to&#225;n khoản vay được đều &lt;strong&gt;được m&#227; h&#243;a an to&#224;n (bảo mật SSL/TLS)&lt;/strong&gt; bởi V&#237; điện tử MoMo.&lt;/p&gt;",
                "Content": "&lt;p&gt;Th&#244;ng tin bạn khoản vay sẽ &lt;strong&gt;được bảo mật v&#224; kh&#244;ng tiết lộ cho c&#225;c b&#234;n thứ 3&lt;/strong&gt; trong qu&#225; tr&#236;nh đăng k&#253; vay.&lt;/p&gt;&lt;p&gt;Mọi giao dịch nhận tiền giải ng&#226;n v&#224; thanh to&#225;n khoản vay được đều &lt;strong&gt;được m&#227; h&#243;a an to&#224;n (bảo mật SSL/TLS)&lt;/strong&gt; bởi V&#237; điện tử MoMo.&lt;/p&gt;",
                "MobileContent": null,
                "Link": "/hoi-dap/su-dung-vi-tra-sau-co-an-toan-khong",
                "GenerateSeo": true
              }
            ]
          }
        ]
      }
    ],
    "GuideData": [
      {
        "Id": 142,
        "Title": "Hướng Dẫn Nhận và Sử Dụng Thẻ Quà Tặng CircleK",
        "Link": "#",
        "Data": {
          "Id": 142,
          "Title": "Hướng dẫn sử dụng thẻ quà tặng ăn uống tại Circle K giá chỉ 1Đ",
          "Blocks": [
            {
              "Id": 631,
              "Title": "Đăng nhập Ví MoMo",
              "Content": null,
              "Index": 1,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-1-181217110450.jpg"
            },
            {
              "Id": 632,
              "Title": "Chọn 'Ưu đãi'",
              "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Tại m&amp;agrave;n h&amp;igrave;nh của V&amp;iacute; MoMo, chọn &amp;#39;Ưu đ&amp;atilde;i&amp;#39;.&lt;/span&gt;&lt;/p&gt;",
              "Index": 2,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-2-181217110454.jpg"
            },
            {
              "Id": 633,
              "Title": "Chọn 'Quà của tôi'",
              "Content": "&lt;p&gt;Chọn &amp;#39;Qu&amp;agrave; của t&amp;ocirc;i&amp;#39;, để kh&amp;aacute;m ph&amp;aacute; thẻ qu&amp;agrave; ăn uống gi&amp;aacute; chỉ 1Đ&amp;nbsp;của Circle K.&lt;/p&gt;",
              "Index": 3,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-3-181217110459.jpg"
            },
            {
              "Id": 634,
              "Title": "Chọn thẻ quà Circle K",
              "Content": null,
              "Index": 4,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-4-181217110504.jpg"
            },
            {
              "Id": 635,
              "Title": "Chọn 'Sử dụng'",
              "Content": null,
              "Index": 5,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-5-181217110508.jpg"
            },
            {
              "Id": 636,
              "Title": "Tiến hành thanh toán",
              "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Vui l&amp;ograve;ng đưa m&amp;atilde; thanh to&amp;aacute;n cho thu ng&amp;acirc;n để tiến h&amp;agrave;nh giao dịch.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;",
              "Index": 6,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-6-181217110512.jpg"
            },
            {
              "Id": 637,
              "Title": "Thanh toán thành công",
              "Content": null,
              "Index": 7,
              "Avatar": "https://static.mservice.io/img/momo-upload-api-cc-7-181217110516.jpg"
            }
          ]
        }
      }
    ],
    "GuideGroupData": [
      {
        "Id": 1491,
        "Title": "Nhóm Hướng dẫn",
        "Link": "www",
        "Data": {
          "Id": 1491,
          "Name": "Hướng Dẫn Liên Kết Ví Với Tài Khoản Ngân Hàng SeABank",
          "Description": null,
          "Groups": [
            {
              "Id": 1493,
              "Name": "Tải Và Đăng Ký ",
              "Description": null,
              "Items": [
                {
                  "Id": 52,
                  "Title": null,
                  "IconUrl": "https://static.mservice.io/default/s/no-image-momo.jpg",
                  "Blocks": [
                    {
                      "Id": 211,
                      "Title": "Tải ứng dụng Ví MoMo",
                      "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Tải ứng dụng miễn ph&amp;iacute; bằng c&amp;aacute;ch t&amp;igrave;m kiếm&amp;nbsp;từ kh&amp;oacute;a &amp;ldquo;vi momo&amp;rdquo;&amp;nbsp;tr&amp;ecirc;n &lt;a href=&quot;https://apps.apple.com/vn/app/id918751511?utm_source=website&amp;amp;utm_medium=download&amp;amp;utm_campaign=guideline&quot;&gt;App Store&lt;/a&gt; hoặc &lt;a href=&quot;https://play.google.com/store/apps/details?id=com.mservice.momotransfer&amp;amp;utm_source=website&amp;amp;utm_medium=download&amp;amp;utm_campaign=guideline&quot;&gt;Google Play Store&lt;/a&gt;.&lt;/span&gt;&lt;/p&gt;",
                      "Index": 1,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-tai-vi-1-190103160624.jpg"
                    },
                    {
                      "Id": 212,
                      "Title": "Nhập số điện thoại",
                      "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Mở ứng dụng v&amp;agrave; nhập số điện thoại bạn muốn đăng k&amp;yacute; V&amp;iacute; MoMo.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;em&gt;Lưu &amp;yacute;:&amp;nbsp;&lt;/em&gt;&lt;/span&gt;&lt;em&gt;Số điện&amp;nbsp;thoại đăng k&amp;iacute; V&amp;iacute; phải tr&amp;ugrave;ng với số điện thoại đăng k&amp;iacute; Internet Banking của ng&amp;acirc;n h&amp;agrave;ng bạn muốn li&amp;ecirc;n kết.&lt;/em&gt;&lt;/p&gt;",
                      "Index": 2,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-2-190103153118.jpg"
                    },
                    {
                      "Id": 213,
                      "Title": "Nhập mã xác thực ",
                      "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Một tin nhắn chứa m&amp;atilde; x&amp;aacute;c thực sẽ&amp;nbsp;gửi trực tiếp đến số điện thoại của bạn.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Vui l&amp;ograve;ng kiểm tra v&amp;agrave; nhập ch&amp;iacute;nh x&amp;aacute;c m&amp;atilde; x&amp;aacute;c thực v&amp;agrave;o &amp;ocirc; &amp;quot;Nhập m&amp;atilde; x&amp;aacute;c thực&amp;quot; rồi nhấn &amp;quot;Tiếp tục&amp;quot;.&lt;/span&gt;&lt;/p&gt;",
                      "Index": 3,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-3-190103153122.jpg"
                    },
                    {
                      "Id": 214,
                      "Title": "Tạo mật khẩu đăng nhập ",
                      "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Thiết lập mật khẩu để bảo vệ t&amp;agrave;i khoản V&amp;iacute; MoMo của bạn gồm 6 chữ số v&amp;agrave; mật khẩu ở 2 &amp;ocirc; phải ho&amp;agrave;n to&amp;agrave;n giống nhau.&lt;br /&gt;&lt;br /&gt;Sau đ&amp;oacute; bấm &amp;quot;X&amp;aacute;c nhận&amp;quot;.&lt;/span&gt;&lt;/p&gt;",
                      "Index": 4,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-4-190103153127.jpg"
                    },
                    {
                      "Id": 293,
                      "Title": "Nhập thông tin",
                      "Content": "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;Sau khi tạo mật khẩu đăng nhập th&amp;agrave;nh c&amp;ocirc;ng, bạn nhập th&amp;ocirc;ng tin c&amp;aacute; nh&amp;acirc;n theo y&amp;ecirc;u cầu v&amp;agrave; nhấn &amp;quot;X&amp;aacute;c nhận&amp;quot;&amp;nbsp;để ho&amp;agrave;n tất đăng k&amp;yacute;.&lt;/span&gt;&lt;/p&gt;",
                      "Index": 5,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-5-190103153131.jpg"
                    }
                  ]
                }
              ]
            },
            {
              "Id": 1494,
              "Name": "Liên Kết Ngân Hàng ",
              "Description": null,
              "Items": [
                {
                  "Id": 749,
                  "Title": null,
                  "IconUrl": "https://static.mservice.io/default/s/no-image-momo.jpg",
                  "Blocks": [
                    {
                      "Id": 3538,
                      "Title": "Chọn liên kết tài khoản",
                      "Content": "&#160;- Nhấn chọn &quot;Li&#234;n kết t&#224;i khoản&quot; tr&#234;n m&#224;n h&#236;nh ch&#237;nh hoặc nhấn v&#224;o &quot;Li&#234;n kết ngay&quot; tr&#234;n một m&#224;n h&#236;nh bất k&#236;.",
                      "Index": 1,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-b1-181011102900.jpg"
                    },
                    {
                      "Id": 3543,
                      "Title": "Chọn thêm thẻ/ tài khoản",
                      "Content": "&#160;- Nhấn chọn &quot;Th&#234;m thẻ/ t&#224;i khoản&quot; tr&#234;n m&#224;n h&#236;nh",
                      "Index": 2,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519135156-637570291163819088.png"
                    },
                    {
                      "Id": 3539,
                      "Title": "Chọn \"SeABank\"",
                      "Content": "&#160;- Nhấn chọn logo SeABank tr&#234;n m&#224;n h&#236;nh",
                      "Index": 3,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210518172501-637569555018289462.png"
                    },
                    {
                      "Id": 3540,
                      "Title": "Nhập thông tin tài khoản",
                      "Content": "Nhập đầy đủ c&#225;c th&#244;ng tin li&#234;n kết, bao gồm:&lt;br/&gt;&#160;- Số thẻ/ t&#224;i khoản&lt;br/&gt;&#160;- Họ v&#224; t&#234;n chủ thẻ/ t&#224;i khoản&lt;br/&gt;&#160;- CMND/CCCD/Hộ chiếu&lt;br/&gt;&#160;Lưu &#253;: c&#225;c th&#244;ng tin tr&#234;n phải tr&#249;ng khớp với th&#244;ng tin đăng k&#253; tại ng&#226;n h&#224;ng, số điện thoại đăng k&#237; nhận OTP tại ng&#226;n h&#224;ng phải tr&#249;ng khớp với số V&#237; MoMo&quot;",
                      "Index": 4,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210518172848-637569557286591605.png"
                    },
                    {
                      "Id": 3542,
                      "Title": "Nhập mã xác thực",
                      "Content": "Nhập m&#227; x&#225;c thực OTP được gửi đến điện thoại qu&#253; kh&#225;ch để ho&#224;n tất li&#234;n kết V&#237;",
                      "Index": 5,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210518172941-637569557810240293.png"
                    }
                  ]
                }
              ]
            },
            {
              "Id": 1492,
              "Name": "Nạp Tiền Vào Ví ",
              "Description": null,
              "Items": [
                {
                  "Id": 750,
                  "Title": null,
                  "IconUrl": "https://static.mservice.io/default/s/no-image-momo.jpg",
                  "Blocks": [
                    {
                      "Id": 3544,
                      "Title": "Tại màn hình chính, chọn “Nạp Tiền”",
                      "Content": "",
                      "Index": 1,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-201208113755-637430242752289373.jpg"
                    },
                    {
                      "Id": 3545,
                      "Title": "Nhập số tiền cần nạp và chọn nguồn tiền",
                      "Content": "&#160;- Số tiền cần nạp tối thiểu 10.000đ đến 20.000.000đ/giao dịch (Hạn mức: 50.000.000đ/ng&#224;y)&lt;br/&gt;&#160;- Lưu &#253;: Số dư t&#224;i khoản ng&#226;n h&#224;ng của bạn sau khi nạp tiền cần lớn hơn 50.000đ",
                      "Index": 2,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519135655-637570294154523155.png"
                    },
                    {
                      "Id": 3546,
                      "Title": "Xác nhận giao dịch",
                      "Content": "&#160;- Kiểm tra th&#244;ng tin v&#224; bấm &quot;X&#225;c nhận&quot; để ho&#224;n tất giao dịch",
                      "Index": 3,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519135714-637570294348964415.png"
                    },
                    {
                      "Id": 3547,
                      "Title": "Nhập mã xác thực",
                      "Content": "- Nhập m&#227; x&#225;c thực để ho&#224;n tất giao dịch&lt;br/&gt;Lưu &#253;: Nhập m&#227; x&#225;c thực với số tiền giao dịch &gt; 1.000.000đ",
                      "Index": 4,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519140857-637570301372888906.png"
                    }
                  ]
                }
              ]
            },
            {
              "Id": 1495,
              "Name": "Rút Tiền Khỏi Ví ",
              "Description": null,
              "Items": [
                {
                  "Id": 751,
                  "Title": null,
                  "IconUrl": "https://static.mservice.io/default/s/no-image-momo.jpg",
                  "Blocks": [
                    {
                      "Id": 3548,
                      "Title": "Chọn mục \"Rút tiền\"",
                      "Content": "&#160;- Mở ứng dụng V&#237; MoMo, chọn mục &quot;R&#250;t tiền&quot;",
                      "Index": 1,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-201208114819-637430248998980497.jpg"
                    },
                    {
                      "Id": 3549,
                      "Title": "Chọn hình thức rút tiền",
                      "Content": "&#160;- Trong mục r&#250;t tiền, vui l&#242;ng chọn h&#236;nh thức &quot;Về ng&#226;n h&#224;ng li&#234;n kết&quot;&lt;br/&gt;&#160;- Nhập số tiền cần r&#250;t tối thiểu l&#224; 50.000đ đến 20.000.000đ/giao dịch",
                      "Index": 2,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519140235-637570297555737685.png"
                    },
                    {
                      "Id": 3550,
                      "Title": "Xác nhận thông tin",
                      "Content": "&#160;- Chọn &quot;X&#225;c nhận&quot; sau khi kiểm tra ch&#237;nh x&#225;c th&#244;ng tin giao dịch r&#250;t tiền",
                      "Index": 3,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519140245-637570297654812499.png"
                    },
                    {
                      "Id": 3551,
                      "Title": "Nhập mật khẩu Ví",
                      "Content": "&#160;- Nhập mật khẩu V&#237; để ho&#224;n tất giao dịch",
                      "Index": 4,
                      "Avatar": "https://static.mservice.io/img/momo-upload-api-210519140329-637570298097500060.png"
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    ],
    "Category": {
      "Id": 18,
      "Name": "Khuyến mãi",
      "Slug": "khuyen-mai",
      "Link": "/tin-tuc/khuyen-mai"
    },
    "Meta": {
      "Title": "1 triệu suất ăn nóng hổi và thức uống mát lạnh tại CircleK",
      "Description": "Ví MoMo và Circle K tặng ngay 1 triệu suất ăn nóng hổi và thức uống mát lạnh cho những thành viên may mắn có tài khoản MoMo từ hạng Bạc trở lên trong suốt tuần lễ hội thanh toán không tiền mặt.",
      "Keywords": "1 triệu suất ăn nóng hổi và thức uống mát lạnh tại CircleK",
      "Url": "/tin-tuc/khuyen-mai/1-trieu-suat-an-nong-hoi-va-thuc-uong-mat-lanh-tai-circlek-586",
      "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-770x370-181022215709.jpg",
      "Scripts": null,
      "Robots": null,
      "RatingValue": 5,
      "RatingCount": 42,
      "Date": null
    },
    "BreadCrumbs": [
      {
        "Title": "MoMo",
        "Url": "https://momo.vn"
      },
      {
        "Title": "Tin tức",
        "Url": "https://momo.vn/tin-tuc"
      },
      {
        "Title": "Khuyến mãi",
        "Url": "https://momo.vn/tin-tuc/khuyen-mai"
      },
      {
        "Title": "1 triệu suất ăn nóng hổi và thức uống mát lạnh tại CircleK",
        "Url": "https://momo.vn/tin-tuc/khuyen-mai/1-trieu-suat-an-nong-hoi-va-thuc-uong-mat-lanh-tai-circlek-586"
      }
    ]
  },
  "Error": null
};

export default (req, res) => {
    res.status(200).json(data);
};
