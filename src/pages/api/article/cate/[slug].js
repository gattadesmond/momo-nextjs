const data = {
  "Result": true,
  "Data": {
    "Category": {
      "Id": 18,
      "Name": "Khuyến mãi",
      "Description": null,
      "Link": "/tin-tuc/khuyen-mai"
    },
    "Lists": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/dai-tiec-deal-an-uong-gia-chi-1000d-hot-uu-dai-bo-gio-mang-ve-1815",
          "Title": "Đại tiệc deal ăn uống giá chỉ 1.000Đ, rinh ngay ưu đãi những ngày giãn cách",
          "ShortContent": "Từ nay đến 10/6 Chợ deal MoMo sẽ bùng nổ đại tiệc thịnh soạn, hàng loạt deal ăn uống lên sàn giá chỉ 1 NGÀN ĐỒNG. Đừng bỏ lỡ, vô số ưu đãi hấp dẫn đang đợi bạn săn về đấy.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608093516-637587417161702639.jpg",
          "Id": 1815,
          "TotalView": 169,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "hôm qua",
          "Date": "2021-06-08 09:29:35",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/an-tam-cat-canh-momo-ho-tro-100000d-voi-ma-bayantam-1814",
          "Title": "An tâm cất cánh, MoMo hỗ trợ 100.000đ với mã “BAYANTAM”",
          "ShortContent": "Nhận ngay hỗ trợ từ Ví MoMo: Đặt vé máy bay và sử dụng mã “BAYANTAM”, có ngay 2 thẻ quà ưu đãi, cho bạn tiết kiệm tới 100.000đ. Đồng hành cùng MoMo để có muôn chuyến đi thật an toàn với chi phí cực “hời”. \n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608092855-637587413354755184.jpg",
          "Id": 1814,
          "TotalView": 44,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "hôm qua",
          "Date": "2021-06-08 09:28:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/nap-dien-thoai-tren-momo-nhan-qua-nap-game-sieu-thich-1804",
          "Title": "Nạp điện thoại trên MoMo, nhận quà nạp game siêu thích!",
          "ShortContent": "Ví MoMo tặng bạn ưu đãi thẻ quà nạp game trị giá 10.000 đồng khi nạp tiền hoặc mua thẻ điện thoại trên MoMo. Áp dụng cho dịch vụ nạp tiền điện thoại hoặc mua mã thẻ nạp trả trước qua Ví MoMo.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210607105844-637586603245520331.jpg",
          "Id": 1804,
          "TotalView": 208,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "hôm qua",
          "Date": "2021-06-07 17:09:13",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/dung-vi-tra-sau-nhan-qua-da-nang-20000d-thanh-toan-dich-vu-momo-tha-ga-1801",
          "Title": "Dùng Ví Trả Sau, nhận quà đa năng 20.000Đ thanh toán dịch vụ MoMo thả ga",
          "ShortContent": "Từ nay đến hết 30/06/2021, khách hàng kích hoạt và sử dụng nguồn tiền Ví Trả Sau cho việc thanh toán sẽ nhận được 1 thẻ quà trị giá 20.000đ áp dụng cho tất cả các dịch vụ có trên Ví MoMo. ",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604140606-637584123669411207.jpg",
          "Id": 1801,
          "TotalView": 1370,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 16:53:24",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/phong-dich-tai-nha-rinh-qua-gioi-thieu-momo-nhan-khong-gioi-han-goi-qua-400000d-1803",
          "Title": "Phòng dịch tại nhà, rinh quà giới thiệu MoMo: Nhận không giới hạn gói quà 400.000đ",
          "ShortContent": "An tâm ở nhà, chốt bộn quà “phòng dịch\" từ MoMo: Mỗi lần giới thiệu bạn bè thành công, gói quà 400.000đ sẽ thuộc về bạn, trong đó có 100.000đ quà Đa Năng thanh toán mọi dịch vụ. Nhân đôi niềm vui khi bạn bè được giới thiệu cũng nhận gói quà 400.000đ trải nghiệm MoMo. Tham gia ngay!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608133258-637587559787078838.jpg",
          "Id": 1803,
          "TotalView": 550,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 14:40:34",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/gioi-thieu-momo-hot-qua-mang-ve-goi-qua-giam-500000d-tiet-kiem-mua-covid-1802",
          "Title": "Giới thiệu MoMo, hốt quà mang về: Gói quà giảm 500.000đ tiết kiệm mùa Covid",
          "ShortContent": "Giãn cách vẫn có cách “làm giàu\". Ngay lần đầu tiên giới thiệu thành công, nhận ngay gói quà 500.000đ, trong đó có 200.000đ quà Đa Năng thanh toán mọi dịch vụ. Ở nhà phòng dịch, thanh toán an toàn và tiết kiệm cùng MoMo. Chương trình dành riêng cho khách hàng chưa từng giới thiệu MoMo thành công. Tham gia ngay!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608133001-637587558019645524.jpg",
          "Id": 1802,
          "TotalView": 455,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 14:17:02",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/nhap-ma-lazada-nhan-200k-san-sale-he-tha-ga-1800",
          "Title": "Nhập mã “LAZADA”, nhận 200K săn sale hè thả ga",
          "ShortContent": "Mùa hè đã hot nay còn hot hơn với quà tặng cho bạn mới từ MoMo. Nhập mã LAZADA ngay hôm nay để nhận trọn combo giảm giá 200K mua sắm trên Lazada áp dụng cho khách hàng mới nhé!\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604140427-637584122672637434.jpg",
          "Id": 1800,
          "TotalView": 1178,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 14:04:55",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/nhap-ma-banmoioi-momo-tang-ngay-the-qua-sieu-thi-20000d-1799",
          "Title": "Nhập mã “BANMOIOI” MoMo tặng ngay thẻ quà siêu thị 20.000Đ",
          "ShortContent": "Từ 08/6 đến 30/6/2021 khách hàng lần đầu sử dụng MoMo thanh toán tại quầy, chỉ cần nhập mã “BANMOIOI” MoMo tặng liền tay thẻ quà tiết kiệm trị giá 20.000Đ mua sắm tại các hệ thống siêu thị. Dùng MoMo an toàn mùa dịch và tiết kiệm hơn, mở Ví lấy quà liền liền kẻo hết.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604103234-637583995544222821.jpg",
          "Id": 1799,
          "TotalView": 810,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 10:31:43",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/nhap-ma-hibanmoi-nhan-ngay-the-qua-giam-15000d-tai-cua-hang-tien-loi-1798",
          "Title": "Nhập mã “HIBANMOI” nhận ngay thẻ quà giảm 15.000Đ tại Cửa hàng tiện lợi",
          "ShortContent": "Chào bạn mới, MoMo gửi tới Ví bạn một phần quà đây. Nhập ngay mã: “HIBANMOI” nhận liền thẻ quà GIẢM 15.000Đ (hóa đơn từ 40.000Đ) khi thanh toán MoMo tại các hệ thống cửa hàng tiện lợi.\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604103712-637583998324432311.jpg",
          "Id": 1798,
          "TotalView": 451,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 10:17:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/an-tam-bay-momo-tang-toi-200000d-dat-phong-khach-san-1797",
          "Title": "An tâm bay, MoMo tặng tới 200.000đ đặt phòng khách sạn",
          "ShortContent": "Đặt vé máy bay, nhận ngay thẻ quà đặt phòng khách sạn! Chuyến đi thật tiết kiệm khi có Ví MoMo - Nhớ thực hiện các quy tắc an toàn để đảm bảo sức khỏe nhé!\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210604093519-637583961198926312.jpg",
          "Id": 1797,
          "TotalView": 48,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "04/06/2021",
          "Date": "2021-06-04 09:32:39",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 620,
      "Count": 10,
      "LastIdx": 10,
      "ExcludeIds": [
        1818,
        1817,
        1816
      ]
    },
    "ListFeatured": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/nap-dien-thoai-100000d-nhan-ngay-the-qua-app-store-ve-vi-1818",
          "Title": "Nạp điện thoại 100.000Đ nhận ngay thẻ quà App Store về Ví",
          "ShortContent": "Thẻ quà 50% tối đa 20.000Đ thanh toán các dịch vụ Apple dành riêng tặng bạn. Chỉ cần nạp điện thoại/mua mã thẻ từ 100.000Đ trên Ví MoMo thẻ quà App Store sẽ “bay” về Ví. Ưu đãi tháng 6 xua tan nắng hè.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608142811-637587592913025802.jpg",
          "Id": 1818,
          "TotalView": 137,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "20 giờ trước",
          "Date": "2021-06-08 14:28:13",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/thoa-suc-xem-phim-ca-nam-tren-galaxy-play-khi-nap-dien-thoai-tu-100000d-1817",
          "Title": "Thỏa sức xem phim cả năm trên Galaxy Play khi nạp điện thoại từ 100.000Đ",
          "ShortContent": "Cơ hội xem phim 1 năm trên Galaxy Play Mobile đang trong tầm tay. Chỉ cần nạp điện thoại/mua mã thẻ mệnh giá từ 100.000Đ trên MoMo bạn sẽ có cơ hội nhận thẻ quà siêu xịn này. Mùa dịch giãn cách, xem phim tại nhà là thượng sách!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608141850-637587587301658891.jpg",
          "Id": 1817,
          "TotalView": 40,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "20 giờ trước",
          "Date": "2021-06-08 14:18:51",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/co-hoi-nhan-hoan-tien-len-den-100000d-qua-hot-mua-he-1816",
          "Title": "Cơ hội nhận hoàn tiền lên đến 100.000Đ - Quà hot mùa hè",
          "ShortContent": "Đến Co.opsmile và Co.op Food mua sắm và thanh toán Ví MoMo liền tay, chắc chắn nhận hoàn tiền ngẫu nhiên lên đến 100.000Đ. Giãn cách mùa dịch, mua sắm an toàn, thanh toán nhanh gọn cùng MoMo.\n",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210608102102-637587444622147303.jpg",
          "Id": 1816,
          "TotalView": 257,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "hôm qua",
          "Date": "2021-06-08 10:20:46",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 623,
      "Count": 3,
      "LastIdx": 3,
      "ExcludeIds": null
    },
    "ListTopView": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/chi-9000d-ve-cgv-cung-momo-dot-chay-rap-he-500",
          "Title": "Đặt vé CGV chỉ với 9.000đ/vé! Nhập mã “CGVMOMO” nhận combo cực CHẤT",
          "ShortContent": "Chương trình siêu HOT của Ví MoMo dành cho tín đồ xem phim đã quay trở lại! Nhập mã “CGVMOMO” và lần đầu liên kết ngân hàng với MoMo, bạn sẽ nhận ngay 2 vé xem phim CGV chỉ 9,000Đ và combo quà cực CHẤT trị giá 200,000Đ tha hồ nạp điện thoại siêu rẻ, mua sắm thỏa thích hay nhâm nhi trà sữa thả ga.",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201022144130-637389744908681162.jpg",
          "Id": 500,
          "TotalView": 539315,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "08/05/2020",
          "Date": "2020-05-08 14:59:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/cuoi-tuan-xem-phim-tha-ga-chi-79000d-ve-542",
          "Title": "Ưu đãi 79.000đ/vé 2D CGV cuối tuần",
          "ShortContent": "Xem phim CGV thả ga chỉ 79.000đ/vé 2D khi mua vé trực tiếp trên Ví MoMo! Chỉ cần bạn là khách hàng MoMo là Thành viên Bạc hoặc Vàng thôi đó. ",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-200310085044-637194270441781599.jpg",
          "Id": 542,
          "TotalView": 496878,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "31/12/2018",
          "Date": "2018-12-31 19:00:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/cung-momo-san-voucher-500000d-tiki-mien-phi-tikinow-619-619",
          "Title": "Cùng MoMo săn voucher 500.000đ Tiki & miễn phí TikiNOW",
          "ShortContent": "Cuối năm lại đến, mùa sale lại về. Các tín đồ sale đã sẵn sàng cho cuộc chiến săn hàng giá khủng chưa? Đừng quên chọn thanh toán bằng MoMo để có cơ hội nhận ngay 500.000đ thả ga mua sắm và miễn phí TikiNOW nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-770-370-resize-181227093526.jpg",
          "Id": 619,
          "TotalView": 176108,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "22/11/2018",
          "Date": "2018-11-22 08:18:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/tang-nguoi-moi-goi-500000d-khi-lien-ket-bidv-smartbanking-voi-vi-momo-359",
          "Title": "Tặng “người mới” gói quà 500.000đ khi liên kết ngân hàng BIDV với Ví MoMo",
          "ShortContent": "Ưu đãi hấp dẫn dành tặng “ma mới”: Cứ nhập mã “BIDV2020” và liên kết ngân hàng với Ví MoMo, bạn nhận ngay gói quà trị giá 500.000đ dùng thanh toán hóa đơn, nạp tiền điện thoại, mua vé máy bay… thỏa thích. Đừng quên đăng ký BIDV SmartBanking để liên kết Ví MoMo dễ dàng hơn nhé!",
          "Avatar": "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201023114830-637390505103770549.png",
          "Id": 359,
          "TotalView": 148058,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "20/10/2020",
          "Date": "2020-10-20 11:57:00",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 623,
      "Count": 4,
      "LastIdx": 4,
      "ExcludeIds": null
    },
    "Categories": [
      {
        "Name": "Tin Tức - Sự Kiện",
        "Id": 17,
        "Link": "/tin-tuc/tin-tuc-su-kien"
      },
      {
        "Name": "Khuyến mãi",
        "Id": 18,
        "Link": "/tin-tuc/khuyen-mai"
      },
      {
        "Name": "Thông cáo báo chí",
        "Id": 19,
        "Link": "/tin-tuc/thong-cao-bao-chi"
      },
      {
        "Name": "Hình ảnh - Video",
        "Id": 20,
        "Link": "/tin-tuc/hinh-anh-video"
      }
    ],
    "Meta": {
      "Title": "Các chương trình khuyến mãi hấp dẫn từ Ví MoMo",
      "Description": "Các khuyến mãi hấp dẫn nhất, quà tặng giá trị nhất từ Ví MoMo, ứng dụng ✅Nạp tiền Điện Thoại ✅ Hóa Đơn Điện, Nước, Internet, Truyền hình ✅Chuyển Nhận Tiền.",
      "Keywords": "Ví MoMo, tin tức khuyến mãi, khuyến mãi hot, khuyến mãi ví momo, chương trình khuyến mãi, quà tặng ví MoMo, momo khuyen mai, vi momo khuyen mai",
      "Url": null,
      "Avatar": "https://static.mservice.io/img/momo-upload-api-tin-khuyen-mai-181015180205.jpg",
      "Scripts": null,
      "Robots": null,
      "RatingValue": 4.5,
      "RatingCount": 17
    }
  },
  "Error": null
};

export default (reg, res) => {
  res.status(200).json(data)
};