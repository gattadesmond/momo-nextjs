const data = {
  "Result": true,
  "Data": {
    "ListFeatured": {
      "Items": [
        {
          "Link": "/tin-tuc/khuyen-mai/tang-ban-the-qua-toi-50000d-luu-ve-vi-di-xe-an-tam-1950",
          "Title": "Tặng bạn thẻ quà tới 50.000đ đi lại: Lưu ngay về Ví",
          "ShortContent": "Lưu thẻ quà đặt vé xe khách với hạn sử dụng 30 ngày, sẵn sàng dùng ngay khi cần đi lại bạn nhé.",
          "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210809161522-637641225221306105.jpg",
          "Id": 1950,
          "TotalView": 0,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "8 phút trước",
          "Date": "2021-08-10 09:51:02",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/mua-dich-thanh-toan-tai-gia-nhan-bo-3-the-qua-giam-40k-nhe-ganh-hoa-don-1949",
          "Title": "Mùa dịch thanh toán tại gia - Nhận bộ 3 thẻ quà giảm 40K “nhẹ gánh” hóa đơn",
          "ShortContent": "[Từ 09/08 đến 09/09/2021] Tặng riêng cho khách hàng nhận thông báo trên ứng dụng MoMo combo 3 thẻ quà tổng 40.000đ thanh toán nhiều loại hóa đơn Điện/Nước/Internet/Truyền hình/Chung cư/Di động trả sau/Học phí. Có MoMo vừa tiện, vừa lợi, quét hóa đơn thôi!",
          "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210809111000-637641042005632075.jpg",
          "Id": 1949,
          "TotalView": 153,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "19 giờ trước",
          "Date": "2021-08-09 14:23:16",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/tin-tuc-su-kien/dong-long-vuot-dich-cung-viet-nam-yeu-thuong-1948",
          "Title": "Đồng lòng vượt dịch cùng \"Việt Nam Yêu Thương\" nhận vòng quay tri ân",
          "ShortContent": "Chiến dịch \"Việt Nam Yêu Thương\" do MoMo triển khai nhằm vận động quyên góp cho 3 mặt trận: Tiếp sức tuyến đầu, Cứu trợ đồng bào và Tiêu diệt virus. Nhằm thể hiện lòng cảm ơn các mạnh thường quân, MoMo mở Vòng quay tri ân với nhiều phần quà ý nghĩa gửi đến những tấm lòng vàng quyên góp trên MoMo.",
          "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806183009-637638714095443800.png",
          "Id": 1948,
          "TotalView": 1287,
          "CategoryName": "Tin Tức - Sự Kiện",
          "CategoryLink": "/tin-tuc/tin-tuc-su-kien",
          "CategoryId": 17,
          "PublicDate": "06/08/2021",
          "Date": "2021-08-06 20:27:23",
          "TotalWords": null,
          "TimeReadAvg": 0
        },
        {
          "Link": "/tin-tuc/khuyen-mai/qua-tiet-kiem-20000d-cho-lan-dau-thanh-toan-cuoc-truyen-hinh-internet-vtvcab-1947",
          "Title": "Quà tiết kiệm 20.000đ cho lần đầu thanh toán cước Truyền hình/Internet VTVcab",
          "ShortContent": "Ưu đãi hấp dẫn mùa giãn cách MoMo dành tặng khách hàng đang sử dụng dịch vụ truyền hình cáp/internet VTVcab. Với lần đầu thanh toán cước VTVcab qua MoMo được giảm ngay 20.000đ. Quà tặng làm quen, tiết kiệm mùa dịch, MoMo thôi!",
          "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806180948-637638701888676762.jpg",
          "Id": 1947,
          "TotalView": 132,
          "CategoryName": "Khuyến mãi",
          "CategoryLink": "/tin-tuc/khuyen-mai",
          "CategoryId": 18,
          "PublicDate": "06/08/2021",
          "Date": "2021-08-06 18:07:28",
          "TotalWords": null,
          "TimeReadAvg": 0
        }
      ],
      "TotalItems": 1496,
      "Count": 4,
      "LastIdx": 4,
      "ExcludeIds": null
    },
    "Categories": [
      {
        "Name": "Tin Tức - Sự Kiện",
        "Id": 17,
        "Link": "/tin-tuc/tin-tuc-su-kien",
        "Articles": {
          "Items": [
            {
              "Link": "/tin-tuc/tin-tuc-su-kien/than-tai-thuong-khung-den-888888d-khi-tham-gia-quay-thuong-moi-ngay-1946",
              "Title": "Mừng triệu người dùng - Thần Tài thưởng khủng đến 888.888Đ khi tham gia quay thưởng mỗi ngày",
              "ShortContent": "Từ 08/08 – 20/08/2021, khách hàng tham gia Vòng quay Thần Tài – Nhận quà phát tài có cơ hội được nhận đến 888.888đ về Túi Thần Tài. Ngoài ra, hàng triệu bao lì xì và thẻ quà đa năng khác đang chờ bạn mang về. Quay thưởng mỗi ngày, ai cũng có quà. Đăng nhập Túi Thần Tài nhận lượt quay ngay!",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806165111-637638654712901245.jpg",
              "Id": 1946,
              "TotalView": 591,
              "CategoryName": "Tin Tức - Sự Kiện",
              "CategoryLink": "/tin-tuc/tin-tuc-su-kien",
              "CategoryId": 17,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 17:43:59",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/tin-tuc-su-kien/3-khong-voi-vi-tra-sau-khong-phi-kich-hoat-khong-lai-suat-khong-phi-duy-tri-1945",
              "Title": "3 KHÔNG với Ví Trả Sau: không phí kích hoạt, không lãi suất, không phí dịch vụ",
              "ShortContent": "Ví Trả Sau - sản phẩm được cung cấp bởi TPBank với ưu điểm đăng ký và phê duyệt nhanh trong vòng 1 phút - giúp người dùng có thể tiếp cận dịch vụ tài chính hiện đại, tiện lợi và thông minh. Đặc biệt, sử dụng Ví Trả Sau bạn hoàn toàn an tâm không lo các vấn đề về phí kích hoạt, lãi suất hay phí dịch vụ",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806135010-637638546106985665.jpg",
              "Id": 1945,
              "TotalView": 175,
              "CategoryName": "Tin Tức - Sự Kiện",
              "CategoryLink": "/tin-tuc/tin-tuc-su-kien",
              "CategoryId": 17,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 16:32:37",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/tin-tuc-su-kien/duy-nhat-6-8-dai-hoi-dau-nhom-dua-top-giat-thuong-khung-den-100-trieu-tu-tiger-collab-1942",
              "Title": "Duy nhất 6/8: Đại hội Đấu Nhóm - Đua TOP giật thưởng khủng đến 100 triệu từ Tiger Collab",
              "ShortContent": "Loa! Loa! Giờ Vàng Thi Đấu Nhóm cuối cùng đã điểm. Hẹn khung Giờ Vàng 15h - 16h & 19h - 20h ngày 6/8 vào Thành Phố MoMo “tham chiến”, giành lấy giải thưởng khủng lên đến 100 triệu đồng từ Tiger Collab. ",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210805160748-637637764686671437.jpg",
              "Id": 1942,
              "TotalView": 75,
              "CategoryName": "Tin Tức - Sự Kiện",
              "CategoryLink": "/tin-tuc/tin-tuc-su-kien",
              "CategoryId": 17,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 12:25:14",
              "TotalWords": null,
              "TimeReadAvg": 0
            }
          ],
          "TotalItems": 739,
          "Count": 3,
          "LastIdx": 3,
          "ExcludeIds": null
        }
      },
      {
        "Name": "Khuyến mãi",
        "Id": 18,
        "Link": "/tin-tuc/khuyen-mai",
        "Articles": {
          "Items": [
            {
              "Link": "/tin-tuc/khuyen-mai/su-kien-cuc-hot-hoan-ngay-10000d-cho-game-thu-lan-dau-mua-the-tren-napthengayvn-1360",
              "Title": "Nhận quà 10k, Nạp game cực đã!",
              "ShortContent": "Quà cho 500 anh em lần đầu thanh toán trên Napthengay.vn. Nhanh tay vào ví nhận thẻ quà giảm 10.000đ khi mua thẻ game và thanh toán qua Ví MoMo. Cứ nạp là có quà, nạp thẻ ngay! \n",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806154429-637638614695290302.jpg",
              "Id": 1360,
              "TotalView": 70235,
              "CategoryName": "Khuyến mãi",
              "CategoryLink": "/tin-tuc/khuyen-mai",
              "CategoryId": 18,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 15:45:01",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/khuyen-mai/dat-ve-vietnam-airlines-chi-tu-49000d-99000d-nhe-canh-vi-vu-cung-momo-1944",
              "Title": "Đặt vé Vietnam Airlines chỉ từ 49.000đ - 99.000đ -  \"Nhẹ cánh\" vi vu cùng MoMo",
              "ShortContent": "Yên tâm đặt vé Vietnam Airlines thật tiết kiệm với chính sách hoàn, đổi linh hoạt cùng MoMo: Giá vé không hành lý ký gửi chỉ từ 49.000đ và 99.000đ. Lên lịch khám phá sau dịch thôi!",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806095223-637638403439611681.jpg",
              "Id": 1944,
              "TotalView": 44,
              "CategoryName": "Khuyến mãi",
              "CategoryLink": "/tin-tuc/khuyen-mai",
              "CategoryId": 18,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 12:21:26",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/khuyen-mai/sai-canh-an-tam-momo-tiep-suc-30000d-khi-ban-dat-ve-may-bay-1943",
              "Title": "“Sải cánh” an tâm - MoMo \"tiếp sức\" 30.000đ khi bạn đặt vé máy bay",
              "ShortContent": "Di chuyển an toàn - Tiết kiệm ngay 30.000đ khi đặt vé máy bay qua MoMo. Ưu đãi đặc biệt này nhằm hỗ trợ cộng đồng trong mùa dịch, nhanh tay lấy thẻ quà nhé bạn ơi!",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210806092425-637638386658459171.jpg",
              "Id": 1943,
              "TotalView": 21,
              "CategoryName": "Khuyến mãi",
              "CategoryLink": "/tin-tuc/khuyen-mai",
              "CategoryId": 18,
              "PublicDate": "06/08/2021",
              "Date": "2021-08-06 12:21:01",
              "TotalWords": null,
              "TimeReadAvg": 0
            }
          ],
          "TotalItems": 694,
          "Count": 3,
          "LastIdx": 3,
          "ExcludeIds": null
        }
      },
      {
        "Name": "Thông cáo báo chí",
        "Id": 19,
        "Link": "/tin-tuc/thong-cao-bao-chi",
        "Articles": {
          "Items": [
            {
              "Link": "/tin-tuc/thong-cao-bao-chi/nguoi-dung-co-the-chuyen-tien-bang-vi-momo-ngay-tren-viber-1896",
              "Title": "MoMo và Rakuten Viber hợp tác chiến lược: Người dùng có thể chuyển tiền bằng Ví MoMo ngay trên ứng dụng chat Viber",
              "ShortContent": "Ngày 15/7/2021, Ví MoMo và Rakuten Viber, công ty sở hữu ứng dụng chat hàng đầu thế giới (Viber) công bố hợp tác chiến lược, giúp người dùng có thể thực hiện các thao tác chuyển tiền, chia tiền hay nạp tiền điện thoại ngay trên ứng dụng chat Viber. ",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210716084027-637620216274191485.jpg",
              "Id": 1896,
              "TotalView": 140,
              "CategoryName": "Thông cáo báo chí",
              "CategoryLink": "/tin-tuc/thong-cao-bao-chi",
              "CategoryId": 19,
              "PublicDate": "16/07/2021",
              "Date": "2021-07-16 08:40:26",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/thong-cao-bao-chi/vi-momo-khoac-ao-moi-cho-icon-chuyen-tien-momo-lien-1838",
              "Title": "Ví MoMo khoác áo mới cho icon “Chuyển tiền - MoMo liền”",
              "ShortContent": "Giữa tháng 6/2021, Ví MoMo “khoác áo mới\" cho icon “Chuyển tiền - MoMo liền\", ngay trên màn hình ứng dụng với màu hồng nổi bật. Hình ảnh mới mẻ của icon này nhanh chóng được người dùng bình luận và thích thú.\n",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210618083432-637596020724280333.jpg",
              "Id": 1838,
              "TotalView": 555,
              "CategoryName": "Thông cáo báo chí",
              "CategoryLink": "/tin-tuc/thong-cao-bao-chi",
              "CategoryId": 19,
              "PublicDate": "18/06/2021",
              "Date": "2021-06-18 08:33:49",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/thong-cao-bao-chi/chung-tay-cung-vi-momo-va-bao-nguoi-lao-dong-dong-gop-kinh-phi-phong-chong-dich-1769",
              "Title": "\"Tổ quốc cần, cả nước chung tay\": Cùng Ví MoMo và Báo Người Lao Động đóng góp kinh phí phòng chống dịch",
              "ShortContent": "Từ ngày 5/5, Báo Người Lao Động phát động Chương trình “Tổ quốc cần, cả nước chung tay”, phối hợp cùng Ví MoMo kêu gọi sự chung sức của tất cả nguồn lực trong xã hội để hỗ trợ tài chính cho các hoạt động phòng chống dịch Covid-19. Mọi khoản tiền quyên góp của chương trình sẽ được Báo Người Lao Động sử dụng cho công tác phòng chống dịch. ",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210517093246-637568407665778506.jpg",
              "Id": 1769,
              "TotalView": 397,
              "CategoryName": "Thông cáo báo chí",
              "CategoryLink": "/tin-tuc/thong-cao-bao-chi",
              "CategoryId": 19,
              "PublicDate": "17/05/2021",
              "Date": "2021-05-17 09:32:17",
              "TotalWords": null,
              "TimeReadAvg": 0
            }
          ],
          "TotalItems": 39,
          "Count": 3,
          "LastIdx": 3,
          "ExcludeIds": null
        }
      },
      {
        "Name": "Hình ảnh - Video",
        "Id": 20,
        "Link": "/tin-tuc/hinh-anh-video",
        "Articles": {
          "Items": [
            {
              "Link": "/tin-tuc/hinh-anh-video/tien-linh-momo-lien-tra-tien-ao-hao-phong-tip-them-cho-giong-ca-cua-van-toan-1928",
              "Title": "Đẹp trai không nợ dai, Tiến Linh “MoMo liền” trả tiền áo, hào phóng “tip thêm” cho giọng ca của Văn Toàn",
              "ShortContent": "Lỡ thiếu tiền chiếc áo It’s Real, Tiến Linh được \"Chủ tịch\" Văn Toàn tâm huyết \"đòi nợ\" đích danh hẳn hoi bằng giọng ca \"việt… vị\". Tình anh em chắc có bền lâu?\n",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210730140544-637632507444814087.jpg",
              "Id": 1928,
              "TotalView": 45,
              "CategoryName": "Hình ảnh - Video",
              "CategoryLink": "/tin-tuc/hinh-anh-video",
              "CategoryId": 20,
              "PublicDate": "30/07/2021",
              "Date": "2021-07-30 14:03:58",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/hinh-anh-video/bien-cang-chu-tich-van-toan-doi-tien-linh-tien-mua-ao-tinh-anh-em-chac-co-ben-lau-1927",
              "Title": "Biến căng: \"Chủ tịch\" Văn Toàn đòi Tiến Linh tiền mua áo, tình anh em chắc có bền lâu?",
              "ShortContent": "Là anh em thân thiết, thường xuyên có những màn cà khịa nhau trên mạng xã hội, mới đây Văn Toàn đã “đầu tư” quay hẳn clip để có màn “đòi tiền” cực bá đạo đến cậu em Tiến Linh. Không biết Tiến Linh sau khi xem xong video này sẽ phản ứng thế nào nhưng fan của đôi bạn này lại được một phen cười nghiêng ngả.\n",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210730134453-637632494937278764.jpg",
              "Id": 1927,
              "TotalView": 38,
              "CategoryName": "Hình ảnh - Video",
              "CategoryLink": "/tin-tuc/hinh-anh-video",
              "CategoryId": 20,
              "PublicDate": "30/07/2021",
              "Date": "2021-07-30 13:38:00",
              "TotalWords": null,
              "TimeReadAvg": 0
            },
            {
              "Link": "/tin-tuc/hinh-anh-video/tam-long-cua-nguoi-tre-voi-sai-gon-nghia-tinh-1924",
              "Title": "Tấm lòng của người trẻ với Sài Gòn nghĩa tình",
              "ShortContent": "Những ngày này, thương Sài Gòn “ốm nặng” bao nhiêu lại thương những phận đời vất vả mưu sinh trên mảnh đất ấy bấy nhiêu. Không ai bảo ai, từ khắp mọi miền đất nước, nhiều nhóm bạn trẻ đã quyên góp từng bó rau, vỉ trứng hay những suất quà gửi đến người dân khu phong tỏa và những người bị mất thu nhập. Món quà như cái siết tay, mong họ có thêm tinh thần vượt qua lúc khốn khó.",
              "Avatar": "https://static.mservice.io/blogscontents/momo-upload-api-210729085633-637631457935837861.jpg",
              "Id": 1924,
              "TotalView": 33,
              "CategoryName": "Hình ảnh - Video",
              "CategoryLink": "/tin-tuc/hinh-anh-video",
              "CategoryId": 20,
              "PublicDate": "29/07/2021",
              "Date": "2021-07-29 09:00:38",
              "TotalWords": null,
              "TimeReadAvg": 0
            }
          ],
          "TotalItems": 18,
          "Count": 3,
          "LastIdx": 3,
          "ExcludeIds": null
        }
      }
    ],
    "Meta": {
      "Title": "Trang thông tin chính thức Ví MoMo - Ví điện tử số 1 Việt Nam",
      "Description": "Trang thông tin chính thức các tin tức sự kiện, thông tin khuyến mãi, video -hình ảnh, sự kiện mới nhất của Ví MoMo - Ví điện tử số 1 Việt Nam.",
      "Keywords": "Ví MoMo, tin tức khuyến mãi, tin tức sự kiện, khuyến mãi hot, khuyến mãi ví momo, chương trình khuyến mãi, tin tức, quà tặng ví MoMo, thông cáo báo chí",
      "Url": "/tin-tuc",
      "Avatar": "https://static.mservice.io/img/momo-upload-api-tin-tuc-181015180136.jpg",
      "Scripts": null,
      "Robots": null,
      "RatingValue": 4.5,
      "RatingCount": 22,
      "Date": null
    }
  },
  "Error": null
}


export default (reg, res) => {
  res.status(200).json(data)
};