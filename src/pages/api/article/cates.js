const data = {
  "Result": true,
  "Data": [
    {
      "Name": "Tin Tức - Sự Kiện",
      "Id": 17,
      "Link": "/tin-tuc/tin-tuc-su-kien"
    },
    {
      "Name": "Khuyến mãi",
      "Id": 18,
      "Link": "/tin-tuc/khuyen-mai"
    },
    {
      "Name": "Thông cáo báo chí",
      "Id": 19,
      "Link": "/tin-tuc/thong-cao-bao-chi"
    },
    {
      "Name": "Hình ảnh - Video",
      "Id": 20,
      "Link": "/tin-tuc/hinh-anh-video"
    }
  ],
  "Error": null
};

export default (reg, res) => {
  res.status(200).json(data)
};