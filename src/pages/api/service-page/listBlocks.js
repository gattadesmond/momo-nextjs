// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = {
  Result: true,
  Data: [
    {
      Id: 145,
      PageId: 13,
      Type: 2,
      TypeName: "Banner",
      Template: 1,
      DisplayOrder: 1,
      Title: "Banner",
      Short: null,
      ShortTitle: null,
      Content: "",
      BgColor: null,
      Data: {
        Items: [
          {
            Guid: "9726df85-2a4d-41c2-8b59-5ce33b299fb8",
            Name: "Main Banner",
            Description: null,
            DateShow: null,
            DateOff: null,
            Status: 1,
            Avatar:
              "https://static.mservice.io/img/momo-upload-api-210414100944-637539917842267830.jpg",
            AvatarMobile:
              "https://static.mservice.io/img/momo-upload-api-210414103358-637539932383195826.jpg",
            Url: null,
            IsNewTab: false,
            DisplayOrder: null,
          },
        ],
      },
    },
    {
      Id: 211,
      PageId: 13,
      Type: 8,
      TypeName: "HelpArticleGroup",
      Template: 1,
      DisplayOrder: 7,
      Title: "Hướng dẫn đặt vé xem phim trên Ví MoMo",
      Short: "Những câu hỏi thường gặp khi Mua vé xem phim trên ví MoMo",
      ShortTitle: null,
      Content:
        'Không tìm thấy câu hỏi của bạn. Vui lòng xem thêm <a href="/hoi-dap/cach-mua-ve-xem-phim" target="_blank">tại đây</a>',
      BgColor: null,
      Data: {
        Maps: [
          {
            Title: "Hướng dẫn đặt vé xem phim CGV",
            GroupItems: [
              {
                Id: 761,
                Title: "Hướng dẫn đặt vé xem phim CGV tại Ví MoMo",
                Items: [
                  {
                    Id: 398,
                    Title: null,
                    Image:
                      "https://static.mservice.io/default/s/no-image-momo.jpg",
                    Description: "Hướng dẫn đặt vé xem phim CGV tại Ví MoMo",
                    Blocks: [
                      {
                        Id: 1755,
                        Index: 1,
                        Title: "Vào Ví MoMo >> Vào mục “Mua vé xem phim”",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103408-637341212485820849.png",
                      },
                      {
                        Id: 1756,
                        Index: 2,
                        Title: "Tại mục “Chọn rạp” >> Chọn rạp CGV",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103422-637341212629123676.png",
                      },
                      {
                        Id: 1757,
                        Index: 3,
                        Title: "Chọn phim yêu thích",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103435-637341212750688394.png",
                      },
                      {
                        Id: 1758,
                        Index: 4,
                        Title: "Chọn chỗ ngồi và bấm “mua vé”",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103446-637341212866269581.png",
                      },
                      {
                        Id: 1759,
                        Index: 5,
                        Title: "Kiểm tra thông tin người đặt và bấm “tiếp tục”",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103456-637341212968369200.png",
                      },
                      {
                        Id: 1760,
                        Index: 6,
                        Title:
                          "Bấm “xác nhận” thanh toán để hoàn tất giao dịch",
                        Content: "",
                        Image:
                          "https://static.mservice.io/img/momo-upload-api-200827103506-637341213069565957.png",
                      },
                    ],
                  },
                ],
              },
            ],
            Guid: "32d6ae6f-4e67-4767-a0aa-922c87c1d9f6",
            Id: 760,
            Status: 1,
            DisplayOrder: 1,
          },
        ],
      },
      Config: {
        TitleColor: null,
        TitleClass: null,
        ShortColor: null,
        ShortClass: null,
      },
      Cta: {
        Text: "Mua ngay",
        NewTab: false,
        Link: null,
        RedirectUC: null,
        QrCodeId: 2,
        QrCodeData: {
          Id: 2,
          IsNewTab: true,
          QrLink: "https://momoapp.page.link/bi9jFZjZ21sej9gr5",
          ShortTitle: "Đặt vé ngay",
          Title: "Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay",
          BtnText: null,
          QrImage:
            "https://static.mservice.io/img/momo-upload-api-201027104805-637393924852326436.jpeg",
        },
      },
    },
    {
      Id: 212,
      PageId: 13,
      Type: 6,
      TypeName: "QuestionGroup",
      Template: 1,
      DisplayOrder: 8,
      Title: "Bạn hỏi, Ví MoMo trả lời",
      Short: null,
      ShortTitle: null,
      Content: null,
      BgColor: null,
      BtnMore: {
        Content: null,
        Enabled: false,
      },
      Data: {
        Items: [
          {
            Name: "Hỏi đáp dịch vụ Đặt vé xem phim",
            Description: null,
            ListQuestions: [
              {
                Title: "Cách mua vé xem phim trên Ví MoMo",
                ShortContent:
                  '<p>\n\tĐể đặt vé xem phim trên Ví MoMo, các bạn cần làm theo các bước sau đây:\n</p>\n\n<ul>\n\t<li><strong>Bước 1: </strong>Chọn mục ‘Mua vé xem phim’.</li>\n\t<li><strong>Bước 2: </strong>Chọn phim, chọn suất chiếu.</li>\n\t<li><strong>Bước 3: </strong>Chọn ghế ngồi.</li>\n\t<li><strong>Bước 4: </strong>Chọn combo bắp nước (nếu có nhu cầu).</li>\n\t<li><strong>Bước 5: </strong>Kiểm tra lại thông tin đặt vé, xác nhận và thanh toán.</li>\n\t<li><strong>Bước 6:</strong> Quét mã code QR/Barcode tại quầy vé.</li>\n</ul>\n\n<div class="text-center">\n\t<p style="text-align: left;">\n\t\t<em>Sau khi giao dịch thành công, bạn sẽ nhận được thông báo trong ứng dụng có lưu mã vé. Bạn cũng có thể xem lại mã vé khi vào xem chi tiết giao dịch trong mục "<strong>Lịch sử giao dịch</strong>"</em>\n\t</p>\n\n\t<div class="text-center mb-3 ckemm_cta">\n\t\t<a class="ckemm_cta_btn desktop d-none d-md-inline-block btn btn-primary-2 btn-pulse" data-qrcode-id="2" data-target="#qrcode-modal-2" data-toggle="modal" href="javascript:void(0)" target="_blank">ĐẶT VÉ NGAY</a>\n\n\t\t<div class="ckemm_cta_des d-none">\n\t\t\t<b>Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay</b> <a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">https://momoapp.page.link/bi9jFZjZ21sej9</a>\n\t\t</div>\n\n\t\t<div class="ckemm_cta_des d-none">\n\t\t\t<a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">r5</a>\n\t\t</div>\n\t</div>\n</div>\n\n<p>\n\t<strong>Xem thêm:</strong>\n</p>\n\n<ul>\n\t<li><em><a href="https://momo.vn/mua-ve-xem-phim-tai-vi-momo">Khuyến mãi khi đặt vé xem phim trên MoMo</a></em></li>\n\t<li><em><a href="https://momo.vn/mua-ve-xem-phim-tai-vi-momo">Mua vé CGV giá chỉ 9.000đ và combo bắp nước</a></em></li>\n</ul>\n',
                Content:
                  '<p>\n\tĐể đặt vé xem phim trên Ví MoMo, các bạn cần làm theo các bước sau đây:\n</p>\n\n<ul>\n\t<li><strong>Bước 1: </strong>Chọn mục ‘Mua vé xem phim’.</li>\n\t<li><strong>Bước 2: </strong>Chọn phim, chọn suất chiếu.</li>\n\t<li><strong>Bước 3: </strong>Chọn ghế ngồi.</li>\n\t<li><strong>Bước 4: </strong>Chọn combo bắp nước (nếu có nhu cầu).</li>\n\t<li><strong>Bước 5: </strong>Kiểm tra lại thông tin đặt vé, xác nhận và thanh toán.</li>\n\t<li><strong>Bước 6:</strong> Quét mã code QR/Barcode tại quầy vé.</li>\n</ul>\n\n<div class="text-center">\n\t<p style="text-align: left;">\n\t\t<em>Sau khi giao dịch thành công, bạn sẽ nhận được thông báo trong ứng dụng có lưu mã vé. Bạn cũng có thể xem lại mã vé khi vào xem chi tiết giao dịch trong mục "<strong>Lịch sử giao dịch</strong>"</em>\n\t</p>\n\n\t<div class="text-center mb-3 ckemm_cta">\n\t\t<a class="ckemm_cta_btn desktop d-none d-md-inline-block btn btn-primary-2 btn-pulse" data-qrcode-id="2" data-target="#qrcode-modal-2" data-toggle="modal" href="javascript:void(0)" target="_blank">ĐẶT VÉ NGAY</a>\n\n\t\t<div class="ckemm_cta_des d-none">\n\t\t\t<b>Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay</b> <a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">https://momoapp.page.link/bi9jFZjZ21sej9</a>\n\t\t</div>\n\n\t\t<div class="ckemm_cta_des d-none">\n\t\t\t<a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">r5</a>\n\t\t</div>\n\t</div>\n</div>\n\n<p>\n\t<strong>Xem thêm:</strong>\n</p>\n\n<ul>\n\t<li><em><a href="https://momo.vn/mua-ve-xem-phim-tai-vi-momo">Khuyến mãi khi đặt vé xem phim trên MoMo</a></em></li>\n\t<li><em><a href="https://momo.vn/thanh-toan-momo-cgv">Mua vé CGV giá chỉ 9.000đ và combo bắp nước</a></em></li>\n</ul>\n',
                MobileContent:
                  "Bước 1: Từ m&agrave;n h&igrave;nh ch&iacute;nh của ứng dụng chọn &quot;Đặt v&eacute;&quot; -&gt; chọn &quot;V&eacute; xem phim&quot;<br />\r\n<br />\r\nBước 2: Chọn phim muốn xem<br />\r\n<br />\r\nBước 3: Chọn suất chiếu, số lượng v&eacute;, chọn chỗ ngồi<br />\r\n<br />\r\nBước 4: Kiểm tra th&ocirc;ng tin người nhận v&eacute;<br />\r\n<br />\r\nBước 5: Chọn nguồn tiền, x&aacute;c nhận th&ocirc;ng tin v&agrave; ho&agrave;n tất giao dịch<br />\r\n<br />\r\nSau khi giao dịch th&agrave;nh c&ocirc;ng, bạn sẽ nhận được th&ocirc;ng b&aacute;o trong ứng dụng c&oacute; lưu m&atilde; v&eacute;. Bạn cũng c&oacute; thể xem lại m&atilde; v&eacute; khi v&agrave;o xem chi tiết giao dịch trong mục &quot;Lịch sử giao dịch&quot;",
                UrlRewrite: "cach-mua-ve-xem-phim",
                GenerateSeo: false,
                Guid: null,
                Id: 80,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title:
                  "Tôi chưa nhận được mã vé xem phim/không biết xem vé ở đâu?",
                ShortContent:
                  "<p>Trường hợp đã thanh toán thành công cho giao dịch <strong>‘Mua vé xem phim’</strong> nhưng không nhận được mã vé, bạn vui lòng kiểm tra lại Mã vé theo 3 cách sau:</p>\n\n<ul>\n\t<li>Cách 1: Tại màn hình chính Ví MoMo > Mua vé xem phim > Biểu tượng vé góc phải trên cùng của ứng dụng.</li>\n\t<li>Cách 2: Tại màn hình chính Ví MoMo > VÍ CỦA TÔI > Quản lý vé và thông tin hành khách > Quản lý vé và thẻ.</li>\n\t<li>Cách 3: Tại màn hình chính Ví MoMo > Lịch sử giao dịch > Chọn giao dịch mua vé xem phim.</li>\n</ul>\n",
                Content:
                  "<p>Trường hợp đã thanh toán thành công cho giao dịch <strong>‘Mua vé xem phim’</strong> nhưng không nhận được mã vé, bạn vui lòng kiểm tra lại Mã vé theo 3 cách sau:</p>\n\n<ul>\n\t<li>Cách 1: Tại màn hình chính Ví MoMo > Mua vé xem phim > Biểu tượng vé góc phải trên cùng của ứng dụng.</li>\n\t<li>Cách 2: Tại màn hình chính Ví MoMo > VÍ CỦA TÔI > Quản lý vé và thông tin hành khách > Quản lý vé và thẻ.</li>\n\t<li>Cách 3: Tại màn hình chính Ví MoMo > Lịch sử giao dịch > Chọn giao dịch mua vé xem phim.</li>\n</ul>\n",
                MobileContent: null,
                UrlRewrite:
                  "toi-chua-nhan-duoc-ma-ve-xem-phim-khong-biet-xem-ve-o-dau",
                GenerateSeo: false,
                Guid: null,
                Id: 1140,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Vé xem phim có được đổi trả, hoàn hủy không?",
                ShortContent:
                  "Các vé xem phim đặt tại Ví MoMo hiện tại không hỗ trợ đổi trả hay hoàn hủy vé.",
                Content:
                  "Các vé xem phim đặt tại Ví MoMo hiện tại không hỗ trợ đổi trả hay hoàn hủy vé.",
                MobileContent: null,
                UrlRewrite: "ve-xem-phim-co-duoc-doi-tra-hoan-huy-khong",
                GenerateSeo: false,
                Guid: null,
                Id: 1141,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Có thể mua vé xem phim kèm bắp nước hay không?",
                ShortContent:
                  "Hiện tại Ví MoMo có hỗ trợ mua bắp nước các cụm rạp CGV Cinemas, Lotte Cinema, BHD Star, Dcine ngay khi đặt vé. Nếu bạn muốn tăng size, đổi mùi vị bắp nước chỉ cần đến quầy và trả thêm tiền cho thu ngân. Bắp nước đã mua không hỗ trợ đổi trả.",
                Content:
                  "Hiện tại Ví MoMo có hỗ trợ mua bắp nước các cụm rạp CGV Cinemas, Lotte Cinema, BHD Star, Dcine ngay khi đặt vé. Nếu bạn muốn tăng size, đổi mùi vị bắp nước chỉ cần đến quầy và trả thêm tiền cho thu ngân. Bắp nước đã mua không hỗ trợ đổi trả.",
                MobileContent: null,
                UrlRewrite: "co-the-mua-ve-xem-phim-kem-bap-nuoc-hay-khong",
                GenerateSeo: true,
                Guid: null,
                Id: 1142,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Có thể mua vé xem phim những rạp nào trên Ví MoMo?",
                ShortContent:
                  "Hiện tại bạn có thể đặt vé các rạp sau: CGV Cinemas, Lotte Cinema, Galaxy Cinema, BHD Star, Mega GS, Dcine, Cinestar.\n",
                Content:
                  '<p>\n\tHiện tại bạn có thể đặt vé các rạp sau: CGV Cinemas, Lotte Cinema, Galaxy Cinema, BHD Star, Mega GS, Dcine, Cinestar.\n</p>\n\n<div class="text-center mb-3 ckemm_cta">\n\t<a class="ckemm_cta_btn desktop d-none d-md-inline-block btn btn-primary-2 btn-pulse" data-qrcode-id="2" data-target="#qrcode-modal-2" data-toggle="modal" href="javascript:void(0)" target="_blank">ĐẶT VÉ NGAY</a>\n\n\t<div class="ckemm_cta_des d-none">\n\t\t<b>Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay</b> <a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">https://momoapp.page.link/bi9jFZjZ21sej9gr5</a>\n\t</div>\n</div>\n',
                MobileContent:
                  "Hiện tại, khi thanh to&aacute;n tr&ecirc;n MoMo, bạn c&oacute; thể mua v&eacute; xem phim của c&aacute;c rạp BHD, Galaxy, Mega GS, Cinestar.<br />\r\n<br />\r\nNgo&agrave;i ra, bạn c&oacute; thể chọn MoMo l&agrave; nguồn tiền thanh to&aacute;n khi mua v&eacute; xem phim qua ứng dụng hoặc website của c&aacute;c rạp CGV tr&ecirc;n to&agrave;n quốc.",
                UrlRewrite: "co-the-mua-ve-xem-phim-tai-cac-rap-nao",
                GenerateSeo: false,
                Guid: null,
                Id: 81,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Lợi ích của việc mua vé xem phim trên Ví MoMo?",
                ShortContent:
                  "Nhanh chóng, trực quan không cần ra mua vé trực tiếp tại rạp. Tiết kiệm thời gian và tiện lợi. Có nhiều chương trình khuyến mãi với giá vé vô cùng hấp dẫn.",
                Content:
                  '<p>\n\tNhanh chóng, trực quan không cần ra mua vé trực tiếp tại rạp. Tiết kiệm thời gian và tiện lợi. Có nhiều chương trình khuyến mãi với giá vé vô cùng hấp dẫn.\n</p>\n\n<div class="text-center mb-3 ckemm_cta">\n\t<a class="ckemm_cta_btn desktop d-none d-md-inline-block btn btn-primary-2 btn-pulse" data-qrcode-id="2" data-target="#qrcode-modal-2" data-toggle="modal" href="javascript:void(0)" target="_blank">ĐẶT VÉ NGAY</a>\n\n\t<div class="ckemm_cta_des d-none">\n\t\t<b>Quét mã để tải ứng dụng hoặc Đặt vé xem phim ngay</b> <a href="https://momoapp.page.link/bi9jFZjZ21sej9gr5">https://momoapp.page.link/bi9jFZjZ21sej9gr5</a>\n\t</div>\n</div>\n',
                MobileContent: null,
                UrlRewrite: "loi-ich-cua-viec-mua-ve-xem-phim-tren-vi-momo",
                GenerateSeo: true,
                Guid: null,
                Id: 1143,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Có thể đặt tối thiểu và tối đa bao nhiêu vé xem phim?",
                ShortContent:
                  "Có thể đặt tối thiểu 1 vé và tối đa 8 vé xem phim.",
                Content: "Có thể đặt tối thiểu 1 vé và tối đa 8 vé xem phim.",
                MobileContent: null,
                UrlRewrite:
                  "co-the-dat-toi-thieu-va-toi-da-bao-nhieu-ve-xem-phim",
                GenerateSeo: false,
                Guid: null,
                Id: 1144,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title:
                  "Mua vé xem phim tại MoMo có đắt hơn mua trực tiếp tại rạp không?",
                ShortContent:
                  "Giá vé xem phim trên Ví MoMo không thu thêm phí dịch vụ và bán bằng giá tại rạp, nhưng luôn có CTKM vào các ngày trong tuần và cuối tuần để MoMo-er có thể mua với giá vé luôn tốt hơn khi mua trực tiếp tại rạp.",
                Content:
                  "Giá vé xem phim trên Ví MoMo không thu thêm phí dịch vụ và bán bằng giá tại rạp, nhưng luôn có CTKM vào các ngày trong tuần và cuối tuần để MoMo-er có thể mua với giá vé luôn tốt hơn khi mua trực tiếp tại rạp.",
                MobileContent: null,
                UrlRewrite:
                  "mua-ve-xem-phim-tai-momo-co-dat-hon-mua-truc-tiep-tai-rap-khong",
                GenerateSeo: true,
                Guid: null,
                Id: 1145,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title: "Mã QR/Barcode khi mua vé xem phim là gì?",
                ShortContent:
                  "<ul>\n\t<li>Là dạng thông tin đặt vé xem phim đã được mã hóa thành hình ảnh có hình vuông (QR) hoặc dãy các ký tự sọc tạo thành hình chữ nhật (Barcode) xuất hiện trong phần thông tin vé. Khi đến quầy vé tại rạp bạn chỉ cần đưa cho nhân viên tại quầy mã này để vào xem phim bạn đã đặt vé.</li>\n\t<li>Đối với rạp CGV và Galaxy, bạn có thể dùng mã này để tới thẳng phòng chiếu, tại đây sẽ có nhân viên hỗ trợ soát vé bằng mã QR Code, không cần phải đổi tại quầy mua vé.</li>\n</ul>\n",
                Content:
                  "<ul>\n\t<li>Là dạng thông tin đặt vé xem phim đã được mã hóa thành hình ảnh có hình vuông (QR) hoặc dãy các ký tự sọc tạo thành hình chữ nhật (Barcode) xuất hiện trong phần thông tin vé. Khi đến quầy vé tại rạp bạn chỉ cần đưa cho nhân viên tại quầy mã này để vào xem phim bạn đã đặt vé.</li>\n\t<li>Đối với rạp CGV và Galaxy, bạn có thể dùng mã này để tới thẳng phòng chiếu, tại đây sẽ có nhân viên hỗ trợ soát vé bằng mã QR Code, không cần phải đổi tại quầy mua vé.</li>\n</ul>\n",
                MobileContent: null,
                UrlRewrite: "ma-qr-barcode-khi-mua-ve-xem-phim-la-gi",
                GenerateSeo: false,
                Guid: null,
                Id: 1147,
                Status: 0,
                DisplayOrder: null,
              },
              {
                Title:
                  "Phải làm gì khi giao dịch mua vé xe phim “Đang chờ xử lý”?",
                ShortContent:
                  "Vui lòng chờ từ 5 đến 15 phút và kiểm tra lại trạng thái của giao dịch. Sau thời gian trên nếu chưa nhận được mã vé, vui lòng liên hệ trực tiếp tổng đài chăm sóc khách hàng 1900 545441 hoặc nút ‘Trợ giúp’ trên ứng dụng hoặc tại màn hình chính Ví MoMo > Lịch sử giao dịch > Giao dịch mua vé xem phim đang chờ xử lý.",
                Content:
                  "Vui lòng chờ từ 5 đến 15 phút và kiểm tra lại trạng thái của giao dịch. Sau thời gian trên nếu chưa nhận được mã vé, vui lòng liên hệ trực tiếp tổng đài chăm sóc khách hàng 1900 545441 hoặc nút ‘Trợ giúp’ trên ứng dụng hoặc tại màn hình chính Ví MoMo > Lịch sử giao dịch > Giao dịch mua vé xem phim đang chờ xử lý.",
                MobileContent: null,
                UrlRewrite:
                  "phai-lam-gi-khi-giao-dich-mua-ve-xe-phim-dang-cho-xu-ly",
                GenerateSeo: false,
                Guid: null,
                Id: 1146,
                Status: 0,
                DisplayOrder: null,
              },
            ],
            Guid: null,
            Id: 208,
            Status: 0,
            DisplayOrder: null,
          },
        ],
      },
      Config: {
        TitleColor: null,
        TitleClass: null,
        ShortColor: null,
        ShortClass: null,
      },
      Cta: null,
    },
    {
      Id: 213,
      PageId: 13,
      Type: 9,
      TypeName: "GroupPromotionArticles",
      Template: 1,
      DisplayOrder: 95,
      Title: "Mua vé ưu đãi",
      Short:
        "Mỗi khuyến mãi áp dụng cho từng cụm rạp riêng, chọn cụm rạp yêu thích để xem chi tiết",
      ShortTitle: null,
      Content: null,
      BgColor: null,
      Data: {
        Groups: [
          {
            Items: [
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201210143318-637432075982125092.jpg",
                Title:
                  "Cuối tuần đặt vé CGV trực tiếp trên MoMo nhận Thẻ quà giảm 30% My Combo",
                Code: null,
                Short:
                  "Cuối tuần này đi xem phim tại CGV được nhận Thẻ quà ưu đãi bắp nước, giảm 30% My Combo (tối đa 25.000đ). Ưu đãi đặc biệt này chỉ có trên Ví MoMo. Chọn phim yêu thích, thanh toán trên MoMo và nhận quà liền tay nhé!",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2020-12-10 14:32:51",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/khuyen-mai/cuoi-tuan-dat-ve-cgv-truc-tiep-tren-momo-nhan-the-qua-giam-30-my-combo-1529",
                Guid: null,
                Id: 1529,
                Status: 1,
                DisplayOrder: 3,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201007090350-637376582301611766.jpg",
                Title:
                  "Cuối tuần xem phim Lotte chỉ 75K/vé khi đặt trực tiếp trên Ví MoMo!",
                Code: null,
                Short:
                  "Từ 12/10 này, bạn có thể mua vé ưu đãi cụm rạp Lotte Cinema chỉ 75K/vé vào cuối tuần trên Ví MoMo. Đặc biệt số lượng vé sẽ không giới hạn, chỉ cần mở Ví MoMo và chọn ngay cụm rạp Lotte Cinema thôi!",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2020-10-09 09:13:01",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/khuyen-mai/cuoi-tuan-xem-phim-lotte-chi-75k-ve-khi-dat-truc-tiep-tren-vi-momo-1424",
                Guid: null,
                Id: 1424,
                Status: 1,
                DisplayOrder: 2,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-201223083149-637443091094376452.jpg",
                Title:
                  "Thông báo kết quả Minigame “Review có tâm trúng quà xứng tầm”",
                Code: null,
                Short:
                  "MoMo chúc mừng 25 bạn sau đây đã trúng thưởng vé xem phim miễn phí trong mini game Review phim nhân dịp ra mắt tính năng Đánh giá phim trong mục “Mua vé xem phim” của Ví MoMo. Các bạn có tên trong danh sách vui lòng phản hồi lại địa chỉ nhận quà theo email hướng dẫn BTC nhé!",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2020-12-17 10:45:14",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/tin-tuc-su-kien/mini-game-review-co-tam-trung-qua-xung-tam-cung-momo-1544",
                Guid: null,
                Id: 1544,
                Status: 1,
                DisplayOrder: 1,
                IsNewTab: false,
              },
            ],
            Guid: null,
            Title: "Rạp",
            DisplayOrder: 1,
            Status: 1,
          },
        ],
        ShowGroupAll: false,
        ItemsCountPerGroup: 4,
      },
      Config: {
        TitleColor: null,
        TitleClass: null,
        ShortColor: null,
        ShortClass: null,
      },
      Cta: null,
    },
    {
      Id: 214,
      PageId: 13,
      Type: 14,
      TypeName: "CateBlog",
      Template: 1,
      DisplayOrder: 96,
      Title: "Review phim - Blog phim",
      Short: null,
      ShortTitle: null,
      Content: null,
      BgColor: null,
      Data: {
        Cates: [
          {
            Items: [
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210617102319-637595221993020525.jpg",
                Title: "Review The Boys: Khi siêu anh hùng là phản diện!",
                Code: null,
                Short:
                  "Mùa 1 và 2 của The Boys đã ra được ra mắt trên Amazon Prime, nhưng sẽ là thiếu sót nếu bạn không xem từ Mùa 1 của bộ phim này. Cùng trở lại Mùa 1 để xem tại sao series này lại gây được tiếng vang trong mảng phim siêu anh hùng vốn đã bão hòa nhé!",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-06-17 10:21:22",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/review-the-boys-khi-sieu-anh-hung-la-phan-dien-245",
                IsMap: false,
                TotalView: 428,
                Guid: null,
                Id: 245,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210614135140-637592755005802668.jpg",
                Title:
                  "Review Loki 2021: Hành trình của vị thần lừa lọc bậc nhất điện ảnh Marvel",
                Code: null,
                Short:
                  "Đối với các Fan của MCU thì TV Series Loki 2021 đang được mong chờ nhất trên nền tảng Streaming phim Disney+. Hãy cùng Ví MoMo trải nghiệm từng tập phim của Loki trong bài viết này được tóm gọn nhất nhé.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-06-14 13:52:43",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/review-loki-2021-hanh-trinh-cua-vi-than-lua-loc-bac-nhat-dien-anh-marvel-239",
                IsMap: false,
                TotalView: 1143,
                Guid: null,
                Id: 239,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210609142130-637588452902108365.jpg",
                Title:
                  "Top phim kiếm hiệp Trung Quốc được nhiều người xem nhất",
                Code: null,
                Short:
                  "Ví MoMo xin phép giới thiệu cho bạn danh sách phim kiếm hiệp hay nhất hiện nay mà chúng tôi đã thưởng thức, đánh giá và muốn chia sẻ đến các bạn dưới đây.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-06-09 13:42:29",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/top-phim-kiem-hiep-xem-nhieu-nhat-233",
                IsMap: false,
                TotalView: 1522,
                Guid: null,
                Id: 233,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-210531160440-637580738802237846.jpg",
                Title: "Loạt phim hài hay nhất trên Netflix hiện nay",
                Code: null,
                Short:
                  "Ví MoMo xin phép giới thiệu cho bạn danh sách phim hài hay nhất trên Netflix mà chúng tôi đã trải nghiệm cũng như tham khảo khắp nơi.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-06-08 09:56:03",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/loat-phim-toi-hai-hay-nhat-tren-netflix-hien-nay-225",
                IsMap: false,
                TotalView: 829,
                Guid: null,
                Id: 225,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
            ],
            Title: "Mới nhất",
            DisplayOrder: 1,
            Status: 1,
            SortBy: 1,
            SortDir: 2,
            CateId: 99,
            CtaText: "Xem thêm",
            CtaLink: "/blog/di-quay",
            CtaQrCodeId: null,
            CtaNewTab: true,
          },
          {
            Items: [
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-200213144103-637172016639933742.jpg",
                Title: "Danh sách phim hay 2020 gây ấn tượng",
                Code: null,
                Short:
                  "Ví MoMo xin phép liệt kê hàng loạt phim hay 2020 đáng chờ được đan xen nhiều thể loại phim khác với những tựa phim hấp dẫn hơn bao giờ hết.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-03-01 00:00:00",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/phim-bom-tan-2020-top-15-phim-dien-anh-dang-mong-cho-nhat-nam-84",
                IsMap: false,
                TotalView: 809648,
                Guid: null,
                Id: 84,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-200603161145-637267975050546961.jpg",
                Title:
                  "Phim TVB Hong Kong đáng xem nhất đã trở thành huyền thoại",
                Code: null,
                Short:
                  "Phim Bộ Hong Kong (TVB)  từ trước đến nay vốn luôn được mặc định là thương hiệu số một dành cho những ai những ai yêu thích phim Hong Kong nói riêng và khán giả phim truyền hình nói chung. Những bộ phim này dù chiếu đi chiếu lại nhiều lần vẫn nhận được nhiều tình cảm của người hâm mộ trong nhiều năm.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-02-23 09:27:00",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/top-nhung-bo-phim-tvb-dang-xem-nhat-da-tro-thanh-huyen-thoai-139",
                IsMap: false,
                TotalView: 312829,
                Guid: null,
                Id: 139,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-200304105650-637189162109030736.jpg",
                Title:
                  "Danh sách phim Hàn Quốc hay được yêu thích nhất tại Việt Nam",
                Code: null,
                Short:
                  "Cơn sốt phim truyền hình Hàn Quốc chưa bao giờ hạ nhiệt ở Việt Nam. Dưới đây là danh sách Hàn Quốc hay và được yêu thích tại Việt Nam trong thời gian gần đây.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2021-02-21 10:58:00",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/top-10-phim-truyen-hinh-han-quoc-duoc-yeu-thich-nhat-tai-viet-nam-91",
                IsMap: false,
                TotalView: 274088,
                Guid: null,
                Id: 91,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
              {
                Avatar:
                  "https://static.mservice.io/blogscontents/s770x370/momo-upload-api-191227144525-637130547250159561.jpg",
                Title: "Top phim phim ma, kinh dị hay và đáng sợ nhất năm 2019",
                Code: null,
                Short:
                  "Năm 2019 hẳn là một năm bùng nổ của dòng phim ma cũng như phim kinh dị với hàng loạt tác phẩm đắt giá.",
                Content: null,
                DateStart: null,
                DateEnd: null,
                DateShow: "2019-12-27 14:57:00",
                DateOff: null,
                DateFormat: "",
                IsExpired: false,
                IsActive: true,
                Url: "/tin-tuc/di-quay/top-5-phim-kinh-di-dang-so-nhat-nam-2019-51",
                IsMap: false,
                TotalView: 201626,
                Guid: null,
                Id: 51,
                Status: 0,
                DisplayOrder: null,
                IsNewTab: false,
              },
            ],
            Title: "Xem nhiều nhất",
            DisplayOrder: 2,
            Status: 1,
            SortBy: 3,
            SortDir: 2,
            CateId: 99,
            CtaText: "Xem thêm",
            CtaLink: "/blog/di-quay",
            CtaQrCodeId: null,
            CtaNewTab: true,
          },
        ],
        ShowGroupAll: false,
        ItemsCountPerGroup: 4,
      },
      Config: {
        TitleColor: null,
        TitleClass: null,
        ShortColor: null,
        ShortClass: null,
      },
      Cta: null,
    },
  ],
  Error: null,
};

export default (req, res) => {
  res.status(200).json(data);
};
