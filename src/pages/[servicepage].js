import { useRouter } from "next/router";

import { axios } from "@/lib/index";
import { getPureSlug, getUrlHasRoot } from "@/lib/url";
import { appSettings } from "@/configs";

import PageService from "@/components/service/PageService";

const ServicePages = ({
  dataServiceBreadcrumbs,
  dataServiceDetail,
  dataServiceListBlock,
  pageMaster,
  isMobile,
}) => {
  const router = useRouter();
  if (router.isFallback) {
    return <div></div>;
  }

  return (
    <PageService
      dataServiceBreadcrumbs={dataServiceBreadcrumbs}
      dataServiceDetail={dataServiceDetail}
      dataServiceListBlock={dataServiceListBlock}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  );
};

export async function getStaticProps({ params }) {
  let dataServiceDetail = null,
    dataServiceListBlock = null,
    dataServiceBreadcrumbs = null;

  dataServiceDetail = await axios.get(
    `/service-page/detail-by-slug?slug=${params.servicepage}&type=1`,
    null,
    true
  );

  if (!dataServiceDetail) {
    return {
      notFound: true,
    };
  }
  
  if (dataServiceDetail.Error) {
    if (dataServiceDetail.Error.Code == 2) {
      var urlRedirect = getUrlHasRoot(`/${params.servicepage}`);

      return {
        redirect: {
          destination: urlRedirect,
          permanent: false,
        },
      };
    }
    return {
      notFound: true,
    };
  }

  dataServiceBreadcrumbs = await axios.get(
    `/service-page/listBreadcrumbs?id=${dataServiceDetail.Data.Id}${
      dataServiceDetail.Data.ParentId
        ? "&parentId=" + dataServiceDetail.Data.ParentId
        : ""
    }`,
    null,
    true
  );
  dataServiceListBlock = await axios.get(
    `/service-page/listBlocks?pageId=${dataServiceDetail.Data.Id}`,
    null,
    true
  );

  return {
    props: { dataServiceBreadcrumbs, dataServiceDetail, dataServiceListBlock },
    revalidate: 60 * 1,
  };
}

// This function gets called at build time
export async function getStaticPaths() {
  // Call an external API endpoint to get page
  const queryAllServicePage = await axios.get(
    `service-page/list-route?count=1000`,
    null,
    true
  );

  const allPages = queryAllServicePage.Data || [];

  // Get the paths we want to pre-render based on posts
  const paths =
    allPages
      //.filter(page => ['ve-may-bay'].includes(page.Link))
      .filter(page => ['heo-dat-momo','test-lixi'].includes(page.Link))
      .map((page) => ({
        params: { servicepage: getPureSlug(page.Link) },
      })) ?? [];
  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: true };
}

export default ServicePages;
