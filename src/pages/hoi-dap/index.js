import { useState } from "react";
import { useRouter } from "next/router";

import { axios } from "@/lib/index";

import Page from "@/components/page";
import SectionSpace from "@/components/SectionSpace";
import Heading from "@/components/Heading";
import Container from "@/components/Container";

import BlockTabs from "@/components/question/BlockTabs";
import QuestionMenu from "@/components/question/QuestionMenu";
import BlockCategories from "@/components/question/BlockCategories";
import BlockBotQuestion from "@/components/question/BlockBotQuestion";
import BlockLienHe from "@/components/question/BlockLienHe";
import QuestionSearch from "@/components/question/QuestionSearch";
import LdJson_QA_Home from "@/components/question/LdJson_QA_Home";

function HoiDapHome({ dataHome, pageMaster, isMobile }) {
  const router = useRouter();
  const metaData = dataHome.Data.Meta;
  const [query, setQuery] = useState("");

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    router.push("/hoi-dap/tim-kiem?q=" + query);
  };

  const handleSearchChange = (e) => {
    setQuery(e.target.value);
  };

  const handleSearchKeyDown = (e) => {
    if (e.key === "Enter") {
      handleSearchSubmit(e);
    }
  };

  return (
    <Page
      className=""
      title={metaData?.Title}
      description={metaData?.Description}
      image={metaData?.Avatar}
      keywords={metaData?.Keywords}
      robots={metaData?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_QA_Home />
      <QuestionMenu />
      <div
        className="py-12"
        style={{
          background:
            "url(https://static.mservice.io/styles/desktop/images/hoidap/hoidap-head-bg.jpg) #d68fb7 no-repeat center center",
        }}
      >
        <Container>
          <div className="flex justify-center">
            <h1
              className={`mb-4 text-2xl font-bold text-white md:mb-6 md:text-3xl lg:text-4xl`}
            >
              TRUNG TÂM TRỢ GIÚP
            </h1>
          </div>
          <div className="flex justify-center">
            <QuestionSearch
              className="w-full md:w-3/5 "
              PlaceHolder="Chào bạn, Ví MoMo có thể giúp gì được cho bạn?"
            />
          </div>
        </Container>
      </div>
      <SectionSpace type={2}>
        <div className="mb-3 text-center md:mb-5" id="section-help">
          <h2 className="text-xl font-bold  text-pink-600 md:text-2xl">
            CÂU HỎI THƯỜNG GẶP
          </h2>
        </div>
        <BlockTabs data={dataHome.Data.TopQuestions} />
      </SectionSpace>

      <SectionSpace type={1}>
        <div className="mb-3 text-center md:mb-5" id="section-help">
          <h2 className="text-xl font-bold  text-pink-600 md:text-2xl">
            CÂU HỎI THEO CHỦ ĐỀ
          </h2>

          <div className=" text-gray-600">
            Bấm vào chủ đề để xem danh sách các câu hỏi theo chủ đề
          </div>
        </div>
        <BlockCategories data={dataHome.Data.Categories} />
      </SectionSpace>
      <SectionSpace type={2}>
        <div className="mb-3 text-center md:mb-5" id="section-help">
          <h2 className="text-xl font-bold  text-pink-600 md:text-2xl">
            CÂU HỎI QUAN TÂM NHIỀU NHẤT
          </h2>
        </div>
        <BlockBotQuestion data={dataHome.Data.BotQuestion} />
        <BlockLienHe />
      </SectionSpace>
    </Page>
  );
}

export async function getStaticProps(context) {
  const dataHome = await axios.get("/question-article/home", null, true);
  return {
    props: { dataHome }, // will be passed to the page component as props
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 60 seconds
    revalidate: 60 * 1, // In seconds
  };
}

export default HoiDapHome;
