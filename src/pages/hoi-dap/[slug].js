import { axios } from "@/lib/index";

import PageCategory from "@/components/question/PageCategory";
import PageDetail from "@/components/question/PageDetail";

export default function QuestionSlug({
  dataCategories,
  isPageCategory,
  dataDetail,
  dataCates,
  pageMaster,
  isMobile
}) {
  return (
    <>
      {isPageCategory
        ?
        <PageCategory
          dataCategories={dataCategories}
          data={dataCates}
          pageMaster={pageMaster}
          isMobile={isMobile}
        />
        :
        <PageDetail
          dataCategories={dataCategories}
          data={dataDetail}
          pageMaster={pageMaster}
          isMobile={isMobile}
        />
      }
    </>
  )
}

export async function getServerSideProps(context) {
  const slug = context.query.slug;
  if (!slug) {
    return {
      notFound: true,
    };
  }
  let isPageCategory = false;

  const dataCategories = await axios.get('/question-article/cates', null, true);

  let dataDetail = null,
    dataCates = null,
    cateId = null;

  const slugCateIndex = slug.lastIndexOf("ctgr");
  if (slugCateIndex > 0) {
    cateId = slug.slice(slugCateIndex + 4);
    if (cateId == parseInt(cateId, 10)) {
      isPageCategory = true;
    }
  } 

  if (!isPageCategory) {
    dataDetail = await axios.get(`/question-article/detail?urlRewrite=${slug}`, null, true);
    if (!dataDetail || !dataDetail.Result) {
      return {
        notFound: true,
      };
    }
  } else {
    const cateSlug = slug.slice(0, slugCateIndex);
    dataCates = await axios.get(`/question-article/cate/${cateSlug}?id=${cateId}`, null, true);
    if (!dataCates || !dataCates.Result) {
      return {
        notFound: true,
      };
    }
  }

  return {
    props: {
      dataCategories,
      isPageCategory,
      dataDetail,
      dataCates,
    }, // will be passed to the page component as props
  };
}