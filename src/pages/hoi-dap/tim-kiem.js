import { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import { axios } from "@/lib/index";

import Page from "@/components/page";
import Container from "@/components/Container";

import QuestionMenu from "@/components/question/QuestionMenu";
import BlockDetailCategory from "@/components/question/BlockDetailCategory";
import QuestionSearch from "@/components/question/QuestionSearch";
import InfiniteScroll from "react-infinite-scroll-component";

export default function QuestionTimKiem({
  dataCategories,
  pageMaster,
  isMobile
}) {
  const router = useRouter();

  const [query, setQuery] = useState(router.query.q || "");
  const [data, setData] = useState([]);
  const [pageIndex, setPageIndex] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [total, setTotal] = useState(null);

  const [isError, setIsError] = useState(false);
  const [isNoData, setIsNoData] = useState(false);

  const timeout = useRef(null);

  useEffect(() => {
    fetchData();
    () => {
      resetPage();
    }
  }, [])

  useEffect(() => {
    setQuery(router.query.q);
    resetPage();
    fetchData();
  }, [router.query.q])

  useEffect(() => {
    setHasMore(data.length < total);
  }, [data])

  const resetPage = () => {
    setData([]);
    setPageIndex(0);
    setHasMore(true);
    setTotal(null);
    setIsError(false);
    setIsNoData(false);
  }

  const fetchData = () => {
    timeout.current = setTimeout(() => {
      axios
        .post('/search', {
          data: {
            q: router.query.q,
            t: 2,
            s: 1,
            pi: pageIndex,
            c: 50,
          },
        })
        .then(res => {
          if (!res || !res.Result) {
            console.error(res);
            setHasMore(false);
            setIsError(true);
            return;
          }
          if (pageIndex === 0) {
            setTotal(res.Data.Paging.Total);
          }
          if (res.Data.Items.length === 0) {
            setIsNoData(true);
          } else {
            setPageIndex(pIndex => pIndex + 1);
            setData((items) => [...items, ...res.Data.Items]);
          }
        })
        .catch(e => {
          console.error(e);
          setHasMore(false);
          setIsError(true);
        })
    }, 500);
  }

  return (
    <Page
      className=""
      title="Trung tâm trợ giúp - Câu hỏi thường gặp khi dùng Ví MoMo"
      description="Giải đáp chi tiết tất cả các vấn đề thường gặp khi sử dụng Ví MoMo - Ví điện tử số 1 Việt Nam với gần 10 triệu người tin dùng."
      image="https://static.mservice.io/img/momo-upload-api-hoi-dap-su-dung-vi-momo-facebook-181019162228.png"
      keywords="hỏi đáp momo, hướng dẫn sử dụng momo, momo là gì, cách nạp tiền vào momo, thanh toán hoá đơn momo, khoá tài khoản momo, bảo mật, momo có an toàn không"
      // robots={metaData?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <QuestionMenu
        title={'Tìm kiếm'}
      />
      <Container>
        <div
          className="grid grid-cols-1 lg:grid-cols-12 -mx-5 md:mx-0 md:border md:border-gray-200 mb-8"
        >
          <div className="lg:col-span-3 border-r border-gray-200">
            <BlockDetailCategory
              data={dataCategories.Data}
            />
          </div>
          <div className="lg:col-span-9">
            <div className="bg-gray-200 justify-between px-10 py-2 hidden lg:flex">
              <div></div>
              <QuestionSearch
                className="w-2/5 flex-row-reverse pl-4"
                PlaceHolder="Tìm theo câu hỏi, chủ đề"
              />
            </div>
            <div className="py-8 px-4 md:px-10 lg:px-12 mx-1">
              {(isError || isNoData)
                ?
                <>
                  <h1 class="text-lg md:text-xl   mb-6 text-gray-600 text-center">
                    Từ khóa  <strong>"{query}"</strong> - không tìm thấy kết quả phù hợp
                  </h1>
                  <div class="text-left mb-4">
                    <h3 class=" lg:text-lg font-semibold text-gray-600">Để tìm được kết quả chính xác hơn, bạn vui lòng:</h3>
                    <ul class="list-disc text-sm lg:text-md text-gray-500 pl-5  pt-2.5">
                      <li>Kiểm tra lỗi chính tả của từ khóa đã nhập</li>
                      <li>Thử lại bằng từ khóa khác</li>
                      <li>Thử lại bằng những từ khóa tổng quát hơn</li>
                      <li>Thử lại bằng những từ khóa ngắn gọn hơn</li>
                    </ul>
                  </div>
                  <div class="text-center">
                    <a href="/" class="btn btn-primary text-center">
                      Về trang chủ
                    </a>
                  </div>
                </>
                :
                <InfiniteScroll
                  dataLength={data.length}
                  next={() => fetchData()}
                  hasMore={hasMore}
                  loader={
                    <p className="py-4 text-gray-400 text-center">
                      <span>Đang load ...</span>
                    </p>
                  }
                  className="grid grid-cols-1 lg:grid-cols-2 gap-x-8"
                >
                  {data &&
                    data.map((question, qIndex) => (
                      <div
                        key={'question' + question.Id}
                        className="border-b border-gray-200 pb-2 mb-2 lg:pb-2.5 lg:mb-2.5"
                      >
                        <Link href={question.UrlRewrite}>
                          <a className="text-sm lg:text-md font-medium text-gray-500 text-opacity-90 hover:text-momo">
                            <span>{question.Name}</span>
                          </a>
                        </Link>
                      </div>
                    ))
                  }
                </InfiniteScroll>
              }
            </div>
          </div>
        </div>
      </Container>
    </Page>
  )
}

export async function getServerSideProps(context) {
  const urlQuery = context.query.q || null;

  const dataCategories = await axios.get('/question-article/cates', null, true);
  return {
    props: {
      dataCategories
    }, // will be passed to the page component as props
  };
}