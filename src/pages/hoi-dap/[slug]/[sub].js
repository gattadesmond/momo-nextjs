import React from 'react';

import { axios } from "@/lib/index";
import PageCategory from '@/components/question/PageCategory';

export default function QuestionSubCate({
  dataCategories,
  dataCates,
  pageMaster,
  isMobile
}) {
  return (
    <PageCategory
      dataCategories={dataCategories}
      data={dataCates}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  )
}

export async function getServerSideProps(context) {
  const slug = context.query.slug;
  const sub = context.query.sub;

  if (!slug || !sub) {
    return {
      notFound: true,
    };
  }

  const dataCategories = await axios.get('/question-article/cates', null, true);

  const slugCateIndex = slug.lastIndexOf("ctgr");
  if (slugCateIndex < 0) {
    return {
      notFound: true,
    };
  }
  const cateId = slug.slice(slugCateIndex + 4);
  if (cateId != parseInt(cateId, 10)) {
    return {
      notFound: true,
    };
  }
  const cateSlug = slug.slice(0, slugCateIndex);
  const dataCates = await axios.get(`/question-article/cate/${cateSlug}?id=${cateId}&urlCategory=${sub}`, null, true);
  if (!dataCates || !dataCates.Result) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      dataCategories,
      dataCates
    }, // will be passed to the page component as props
  };
}