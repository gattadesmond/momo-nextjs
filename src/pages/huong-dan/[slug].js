import { axios } from "@/lib/index";
import dynamic from "next/dynamic";

const PageGuide = dynamic(() => import("../../components/guide/PageGuide"));
const PageGuideCategory = dynamic(() =>
  import("../../components/guide/PageGuideCategory")
);
function GuideHome({
  dataPostDetail,
  categoryObj,
  dataCatePost,
  dataHomePost,
  listPrefer,
  pageMaster,
  isMobile,
  isDetail,
}) {
  return (
    <>
      {isDetail ? (
        <PageGuide
          categoryObj={categoryObj}
          dataPostDetail={dataPostDetail}
          dataCatePost={dataCatePost}
          listPrefer={listPrefer}
          pageMaster={pageMaster}
          isMobile={isMobile}
          isReplaceUrl={false}
        />
      ) : (
        <PageGuideCategory
          categoryObj={categoryObj}
          dataCategory={dataCatePost}
          dataHomePost={dataHomePost}
          listPrefer={listPrefer}
          pageMaster={pageMaster}
          isMobile={isMobile}
        />
      )}
    </>
  );
}
export async function getServerSideProps(context) {
  const slug = context.query.slug;

  const slugCtIndex = slug.lastIndexOf("-ctgr");

  let dataPostDetail = null;

  let dataHomePost = null;
  let categoryObj = null;
  let dataCatePost = null;
  let listPrefer = null;
  let isDetail = false;

  if (slugCtIndex == -1) {
    //detail
    isDetail = true;
    dataPostDetail = await axios.get(`/guide/detail?slug=${slug}`, null, true);
    if (!dataPostDetail || !dataPostDetail.Result || !dataPostDetail.Data) {
      return {
        notFound: true,
      };
    }

    dataCatePost = await axios.get(`/guide/cates`, null, true);
    listPrefer = await axios.get(
      `landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=6`,
      null,
      true
    );
  } //cate
  else {
    dataCatePost = await axios.get(`/guide/cates`, null, true);
    var cateId = slug.substr(slugCtIndex + 5);
    
    categoryObj = dataCatePost.Data.find((x) => x.Id == cateId) || null;
    if (!categoryObj) {
      return {
        notFound: true,
      };
    }

    if (!categoryObj) {
      return {
        notFound: true,
      };
    }
    dataHomePost = await axios.get(
      `/guide/cate/${cateId}?countList=8&countFeatured=1`,
      null,
      true
    );
    listPrefer = await axios.get(
      `landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=6`,
      null,
      true
    );
  }
  return {
    props: {
      categoryObj,
      dataCatePost,
      dataHomePost,
      listPrefer,
      dataPostDetail,
      isDetail,
    }, // will be passed to the page component as props
  };
}

export default GuideHome;
