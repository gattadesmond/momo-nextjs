import PageGuideHome from "@/components/Guide/PageGuideHome";
import { axios } from "@/lib/index";

function GuideHome({
  categoryObj,
  dataCatePost,
  dataHomePost,
  listPrefer,
  pageMaster,
  isMobile,
}) {
  return (
    <PageGuideHome
      categoryObj={categoryObj}
      dataHomePost={dataHomePost}
      dataCatePost={dataCatePost}
      listPrefer={listPrefer}
      pageMaster={pageMaster}
      isMobile={isMobile}
    />
  );
}
export async function getServerSideProps(context) {


  let dataHomePost = null;
  let dataCatePost = null;
  let listPrefer = null;


  dataCatePost = await axios.get(`/guide/cates`, null, true);

  dataHomePost = await axios.get(
    `/guide/home?countList=8&countFeatured=1`,
    null,
    true
  );
  listPrefer = await axios.get(
    `landing-page/loadMore?isFeatured=true&sortType=2&sortDir=2&count=6`,
    null,
    true
  );

  return {
    props: { dataCatePost, dataHomePost, listPrefer }, // will be passed to the page component as props
  };
}

export default GuideHome;
