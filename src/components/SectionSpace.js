import Container from "@/components/Container";

import classNames from "classnames";

const SectionSpace = ({ className, type = 1, children }) => {
  const sectionClass = classNames(
    {
      "py-8 md:py-10 lg:py-14": true,
      "bg-gray-50": type == 1,
      "bg-white": type == 2,
      "bg-pink-50": type == 3,
    },
    className
  );


  const sectionGradColor = [
    "250 250 250",
    "255 255 255",
    "253 242 248"
  ]

  const selectGradColor = type >=1 && type <=3 ? sectionGradColor[type - 1] : sectionGradColor[0];

  return (
    <>
      <section
        className={sectionClass}
        style={{
          "--grad-rgb-color": selectGradColor,
        }}
      >
        <Container>{children}</Container>
      </section>
    </>
  );
};

export default SectionSpace;
