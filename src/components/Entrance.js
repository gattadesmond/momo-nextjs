import React from "react";
import { useState, useEffect, useRef } from "react";

const Entrance = ({ delay = 300, split = false, children }) => {
  const elementRef = useRef();

  // state for the array of lines found after running Splitting
  const [lines, setLines] = useState([]);

  const [visible, setVisible] = useState(false);
  const [wasVisible, setWasVisible] = useState(false);

  useEffect(() => {
    if (!elementRef.current) {
      return null;
    }

    const observer = new window.IntersectionObserver((entries, observer) => {
      const [entry] = entries;
      if (entry.intersectionRatio > 0) {
        setVisible(true);
      }
      setWasVisible(entry.intersectionRatio > 0);
    });

    observer.observe(elementRef.current);

    return () => {
      observer.disconnect();
    };
  }, []);

  return (
    <div
      ref={elementRef}
      className={` entrance  ${visible ? "was-shown" : ""} ${
        wasVisible ? "is-visible " : "was-visible"
      }`}
    >
      {children}

      <style jsx>{`
        .entrance {
          --entrance-animation-delay: ${delay}ms;
        }
      `}</style>

      <style jsx global>{`
        @keyframes fadeInUp {
          0% {
            opacity: 0;
            transform: translateY(2rem);
          }

          100% {
            opacity: 1;
            transform: translateY(0);
          }
        }

        .entrance:not([data-entrance-split-value]) {
          animation-duration: 750ms;
          animation-fill-mode: both;
          animation-delay: var(--entrance-animation-delay);
          animation-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
        }
        .entrance {
          opacity: 0;
        }

        .entrance.was-shown {
          opacity: 1;
          animation-name: fadeInUp;
          animation-fill-mode: both;
        }

      
      `}</style>
    </div>
  );
};

export default Entrance;
