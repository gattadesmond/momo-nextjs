import Link from "next/link";
import Image from "next/image";

import { getRootSlug } from "@/lib/url";

import CssCarousel from "@/components/CssCarousel";
import Container from "@/components/Container";

export function BreadcrumbItem({
  title,
  icon,
  url = null,
  isShowIcon = true,
  isActive = false,
  isCatItem = false,
  className
}) {

  const renderItem = (title, icon, url, isShowIcon = true) => (
    <>
      {url && url.length > 0 && (
        url.substr(0, 4) === 'http'
          ?
          <a href={url} className={`stretched-link`}>
            <span className="hidden">{title}</span>
          </a>
          :
          <Link href={getRootSlug(url)}>
            <a className={`stretched-link`}>
              <span className="hidden">{title}</span>
            </a>
          </Link>
        )
      }
      <span className="beardcrumb-content">
        {isShowIcon
          ? <span className="relative flex items-center space-x-2">
            {icon &&
              <Image
                alt={title}
                src={icon}
                width={22.5}
                height={22.5}
                loading="eager"
                layout="intrinsic"
                unoptimized={true}
              />
            }
            <span>{title}</span>
          </span>
          : <span>{title}</span>
        }
      </span>
    </>
  )

  return (
    <li
      className={`breadcrumb-item relative whitespace-nowrap flex items-center shrink-0 text-sm px-3 md:px-3.5 first:pl-0
        ${isActive ? "active is-active text-momo font-bold" : "text-gray-800 hover:text-pink-700"}
        ${isCatItem ? "breadcrumb-cat" : ''}
        ${className ? className : ""}
      `}
    >
      {(url && (url === "https://momo.vn" || url === "https://momo.vn/"))
        ?
        <a href="https://momo.vn/">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-4 h-4 block -mt-px beardcrumb-content"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
          </svg>
        </a>
        :
        renderItem(title, icon, url, isShowIcon)
      }
    </li>
  )
}

export default function BreadcrumbContainer({
  className,
  children,
  id="menu-0"
}) {
  return (
    <div className={`block bg-white shadow ${className ? className : ''}`}>
      <Container className="px-0">
        <CssCarousel>
          <ul
            key={id}
            className="breadcrumb flex flex-nowrap overflow-scroll w-full py-3 lg:py-4 pl-4 md:pl-0"
          >
            {children}
            <li className="">
              <span className="block h-px w-2">&nbsp;</span>
            </li>
          </ul>
        </CssCarousel>
      </Container>
    </div>
  )
}