import Link from "next/link";

import { useState, useEffect, useRef } from "react";

import { Dialog } from "@reach/dialog";

const Modal = ({
  isOpen,
  onDismiss = false,
  isFull = false,
  isFullMobile = false,
  title = false,
  isDark = false,
  isBig = false,
  isShowHeaderClose = true,
  cssSize = null,
  children,
}) => {
  // const [showDialog, setShowDialog] = useState(false);

  // const open = () => setShowDialog(true);
  // const close = () => setShowDialog(false);

  // useLockBodyScroll();

  const hmmRef = useRef();

  const getSize = () => {
    if (isBig) {
      return " md:max-w-4xl ";
    } else if (cssSize) {
      return ` ${cssSize} `;
    } else {
      return " md:max-w-3xl ";
    }
  };

  return (
    <>
      {/* <button onClick={open}>Open Dialog</button> */}
      <Dialog
        isOpen={isOpen}
        onDismiss={onDismiss}
        initialFocusRef={hmmRef}
        className="modal fixed inset-0 flex max-h-full flex-row items-end justify-center bg-transparent p-0 pt-3 text-gray-800 md:items-center  md:p-9 "
        aria-labelledby="modal-title"
        role="dialog"
        aria-modal="true"
      >
        <div
          className={`modalFadeUp modal-cluster relative flex max-h-full w-full   max-w-full flex-col rounded-t-xl bg-white shadow-xl  md:rounded-xl 
            ${isFull ? "h-full " : "md:h-auto "} 
            ${isFullMobile ? "h-full md:h-auto  " : ""}
            ${getSize()}
          `}
        >
          {isShowHeaderClose && (
            <button
              className={`absolute top-0 left-0 right-auto z-20 mt-1 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full hover:bg-gray-50 hover:bg-opacity-20 md:left-auto md:right-3 
                ${isDark == true ? "text-white text-opacity-70" : ""}
              `}
              onClick={onDismiss}
              ref={hmmRef}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className=" block h-6 w-6"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>

              {/* <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 block md:hidden"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M10 19l-7-7m0 0l7-7m-7 7h18"
              />
            </svg> */}
            </button>
          )}
          {children}
        </div>

        <div
          className="modal-overlay fixed inset-0 overflow-y-auto bg-black bg-opacity-50"
          onClick={onDismiss}
        ></div>
      </Dialog>

      <style jsx global>{`
        .modalFadeUp {
          animation-duration: 400ms;
          animation-iteration-count: 1;
          animation-fill-mode: both;
          animation-name: keyframe_d37zz3;
        }

        @keyframes keyframe_d37zz3 {
          from {
            opacity: 0;
            transform: translate3d(0, 100%, 0);
          }

          to {
            opacity: 1;
            transform: translate3d(0, 0, 0);
          }
        }

        .modal {
          z-index: 200;
        }

        .modal-cluster {
          z-index: 10;
        }

        .modal-header {
          flex-shrink: 0;
        }

        .modal-body {
          -webkit-overflow-scrolling: auto;
        }

        .modal-overlay {
          z-index: 5;
        }

        .modal-footer {
          flex: 0 0 auto;
        }
      `}</style>
    </>
  );
};

export default Modal;
