import Link from "next/link";

import { useState, useEffect } from "react";

const ModalHeader = ({ children, isBorder = true }) => {
  return (
    <>
      <header
        className={`modal-header  pt-4 pb-4 pl-12 pr-12  font-semibold flex items-center justify-center ${isBorder ? "border-b border-gray-200" : ""}`}
      >
        <h3 className="overflow-hidden  text-ellipsis whitespace-nowrap">{children}</h3>
      </header>
    </>
  );
};

export default ModalHeader;
