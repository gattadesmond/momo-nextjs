import Link from "next/link";

import { useState, useEffect } from "react";

const ModalBody = ({ children, css = false }) => {
  // useLockBodyScroll();

  return (
    <>
      <div className={`modal-body  h-full overflow-auto ${css ? css : "pt-2 pb-2 pl-12 pr-12"}`}>
        {children}
      </div>

    </>
  );
};

export default ModalBody;
