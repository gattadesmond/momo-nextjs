import Link from "next/link";

import { useState, useEffect } from "react";

const ModalFooter = ({ children, className }) => {
  return (
    <>
      <footer className={`modal-footer flex border-t border-gray-200 py-2 md:py-4 px-5 items-center justify-end ${className}`}>
        {children}
      </footer>
    </>
  );
};

export default ModalFooter;
