import Link from "next/link";
import { useRouter } from "next/router";

const BreadcrumbItems = ({ url, title, path, icon }) => {
  if (url == "/" || url == "https://momo.vn") {
    return (
      <a
        className=" whitespace-nowrap"
        aria-label="MoMo"
        href="https://momo.vn/"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
        </svg>
      </a>
    );
  }
  if (url == `https://momo.vn${path}`) {
    return (
      <span className="text-gray-500  whitespace-nowrap  flex items-center space-x-2">
        {icon && <img src={icon} className="w-4 h-4  flex-none hidden md:block" />}{" "}
        <span>{title}</span>
      </span>
    );
  }
  return (
    <Link href={url}>
      <a className="font-semibold whitespace-nowrap flex items-center space-x-2">
        {icon && <img src={icon} className="w-4 h-4 hidden md:block flex-none" />}
        <span>{title}</span>
      </a>
    </Link>
  );
};

const Breadcrumb = ({ data }) => {
  const router = useRouter();
  const routerPath = router.asPath;

  return (
    <>
      <nav aria-label="breadcrumb" className="soju-carousel">
        <ol className="flex items-center list-reset flex-nowrap ">
          {data.map((item, index) => (
            <li
              className={`breadcrumb-item shrink-0 px-3 md:px-3 text-gray-800 text-sm relative hover:text-pink-700  ${
                index == 0 ? "pl-0" : ""
              }`}
              key={index}
            >
              <BreadcrumbItems
                url={item.Url}
                title={item.Title}
                path={routerPath}
                icon={item.Icon ?? null}
              />
            </li>
          ))}
        </ol>
      </nav>

      <style jsx>{`
        .breadcrumb-item + .breadcrumb-item:before {
          content: "";
          opacity: 0.3;
          width: 1.5rem;
          height: 1rem;
          background-image: url("data:image/svg+xml,%0A%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-right'%3E%3Cpolyline points='9 18 15 12 9 6'%3E%3C/polyline%3E%3C/svg%3E");
          background-repeat: no-repeat;

          left: -6px;
          top: 2px;
          position: absolute;
        }
      `}</style>
    </>
  );
};

export default Breadcrumb;
