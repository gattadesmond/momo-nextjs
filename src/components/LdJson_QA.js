const LdJson_QA = (props) => {
  const { QaData } = props;
  var ldJson = "";
  if (QaData && QaData.Items && QaData.Items.length > 0) {
    QaData.Items.forEach((x) => {
      x.ListQuestions.forEach((y) => {
        if (y.GenerateSeo) {
          ldJson +=
            (ldJson ? "," : "") +
            `
            {
              "@type": "Question",
              "name": "${y.Title}",
              "acceptedAnswer":
              {
                  "@type": "Answer",
                  "text": "${y.ShortContent}"
              }
            }
          `;
        }
      });
    });
  }

  return ldJson ? (
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{
        __html: `{
            "@context": "http://schema.org",
            "@type": "FAQPage",
            "mainEntity":
            [
            ${ldJson}
            ]
        }`,
      }}
    ></script>
  ) : (
    <></>
  );
};

export default LdJson_QA;
