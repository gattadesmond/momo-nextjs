import Link from "next/link";

const QRCodeScan = ({ img = "", children }) => {
  return (
    <>
      <div className="qrcode__scan__container">
        <div className="qrcode__scan">
          
          <div className="qrcode__gradient">
            <img
              alt=""
              className="img-fluid"
              src="https://static.mservice.io/jk/momo2020/img/qrcode/qrcode-gradient.png"
              loading="lazy"
            />
          </div>
          <div className="qrcode__border">
            <img
              alt=""
              className="img-fluid"
              src="https://static.mservice.io/jk/momo2020/img/qrcode/border-qrcode.svg"
            />
          </div>

          <div className="p-4 qrcode__image">
            {img ? (
              <img
                alt=""
                className="mx-auto img-fluid d-block"
                src={img}
                loading="lazy"
              />
            ) : (
              children
            )}
          </div>
        </div>
      </div>

      <div className="mt-4 text-sm text-white">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="inline w-6 h-6 mr-1"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M12 4v1m6 11h2m-6 0h-2v4m0-11v3m0 0h.01M12 12h4.01M16 20h4M4 12h4m12 0h.01M5 8h2a1 1 0 001-1V5a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1zm12 0h2a1 1 0 001-1V5a1 1 0 00-1-1h-2a1 1 0 00-1 1v2a1 1 0 001 1zM5 20h2a1 1 0 001-1v-2a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1z"
          />
        </svg>
        Sử dụng App MoMo hoặc
        <br />
        ứng dụng Camera hỗ trợ QR code để quét mã.
      </div>

      <style jsx>{`
        .qrcode__scan__container {
          padding: 15px;
          background-color: white;
          width: 280px;
          height: 280px;
          border-radius: 20px;
          border-radius: 15px;
          margin: 0 auto;
        }

        .qrcode__image {
          width: 100%;
        }

        .qrcode__image img {
          width: 100%;
        }

        .qrcode__scan {
          width: 250px;
          height: 250px;
          margin: 0 auto;
          overflow: hidden;
          background-color: white;
          position: relative;
          display: flex;
          align-items: center;
          justify-content: center;
        }

        .qrcode__border {
          position: absolute;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
          z-index: 5;
          opacity: 0.9;
          pointer-events: none;
        }

        .qrcode__gradient {
          position: absolute;
          opacity: 0.6;
          width: 98%;
          height: 98%;
          top: 1%;
          left: 1%;
          z-index: 6;
          // pointer-events: none;
          transform: translate3d(0, -110%, 0);
          animation: qrCodeScan 3s infinite cubic-bezier(0.45, 0.03, 0.81, 0.63);

          backface-visibility: hidden;
        }

        @keyframes qrCodeScan {
          0% {
            transform: translate3d(0, -110%, 0);
          }

          90% {
            transform: translate3d(0, 30%, 0);
          }
          100% {
            transform: translate3d(0, 30%, 0);
          }
        }
      `}</style>
    </>
  );
};

export default QRCodeScan;
