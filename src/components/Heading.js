import Link from "next/link";
import Sparkles from "@/components/Sparkles";
import parse, { domToReact } from "html-react-parser";

const Heading = ({
  title,
  subtitle,
  type = 1,
  color,
  isSparkles = false,
  config,
}) => {
  if (type == 1) {
    return (
      <>
        {isSparkles ? (
          <Sparkles>
            <>
              {config != null && config.TitleHtmlElement == 1 ? (
                <h1
                  className={`text-2xl font-bold lg:text-3xl  ${
                    color == "pink" ? "text-pink-600" : ""
                  } ${color == "white" ? "text-white" : ""}`}
                >
                  {parse(title)}
                </h1>
              ) : config != null && config.TitleHtmlElement == 3 ? (
                <h3
                  className={`text-2xl font-bold lg:text-3xl  ${
                    color == "pink" ? "text-pink-600" : ""
                  } ${color == "white" ? "text-white" : ""}`}
                >
                  {parse(title)}
                </h3>
              ) : (
                <h2
                  className={`text-2xl font-bold lg:text-3xl  ${
                    color == "pink" ? "text-pink-600" : ""
                  } ${color == "white" ? "text-white" : ""}`}
                >
                  {parse(title)}
                </h2>
              )}
            </>
          </Sparkles>
        ) : (
          <>
            {config != null && config.TitleHtmlElement == 1 ? (
              <h1
                className={`text-2xl font-bold lg:text-3xl  ${
                  color == "pink" ? "text-pink-600" : ""
                } ${color == "white" ? "text-white" : ""}`}
              >
                {parse(title)}
              </h1>
            ) : config != null && config.TitleHtmlElement == 3 ? (
              <h3
                className={`text-2xl font-bold lg:text-3xl  ${
                  color == "pink" ? "text-pink-600" : ""
                } ${color == "white" ? "text-white" : ""}`}
              >
                {parse(title)}
              </h3>
            ) : (
              <h2
                className={`text-2xl font-bold lg:text-3xl  ${
                  color == "pink" ? "text-pink-600" : ""
                } ${color == "white" ? "text-white" : ""}`}
              >
                {parse(title)}
              </h2>
            )}
          </>
        )}

        {subtitle && (
          <h3 className="text-md mx-auto mt-2 max-w-3xl text-gray-500 lg:text-lg">
            {parse(subtitle)}
          </h3>
        )}
      </>
    );
  }

  return (
    <>
      <h2 className="text-3xl font-bold text-gray-800">{title}</h2>
      {subtitle && <h3 className="text-md md:text-xl">{subtitle}</h3>}
    </>
  );
};

export default Heading;
