const LdJson_Meta_NewsArticle = (props) => {
  var {
    Title,
    Description,
    Keywords,
    Avatar,
    RatingCount,
    RatingValue,
    Robots,
    Scripts,
    Url,
    Date,
  } = props.Meta;
  return (
    <>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `
        {
            "@context": "http://schema.org",
            "@type": "NewsArticle",
            "mainEntityOfPage": 
            {
                "@type": "WebPage",
                "@id": "https://momo.vn/"
            },
            "headline": "${Title}",
            "image": ["${Avatar}"],
            ${Date?"\"datePublished\": \""+Date+"\",":""}
            ${Date?"\"dateModified\": \""+Date+"\",":""}
            "author":
            {
                "@type": "Organization",
                "name": "Ví MoMo"
            },
            "publisher": 
            {
                "@type": "Organization",
                "name": "Ví MoMo",
                "logo": 
                {
                    "@type": "ImageObject",
                    "url": "https://static.mservice.io/img/momo-upload-api-logo-momo-181015105508.png"
                }
            },
            "description": "${Description}"
        }`
        }}
      ></script>

      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `
          {
            "@context": "https://schema.org/",
            "@type": "CreativeWorkSeries",
            "name": "${Title}",
            "description": "${Description}",
            "aggregateRating": 
            {
                "@type": "AggregateRating",
                "ratingValue": "${RatingValue}",
                "bestRating": "5",
                "ratingCount": "${RatingCount}",
                "worstRating": "0"
            }
          }`
        }}
      ></script>
    </>
  );
};

export default LdJson_Meta_NewsArticle;
