export default function MaxWidth({ children }) {
  return (
    <div className="max-w-5xl mx-auto w-full px-6 lg:px-8 ">{children}</div>
  );
}
