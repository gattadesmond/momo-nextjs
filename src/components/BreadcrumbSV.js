import Link from "next/link";
import { useRouter } from "next/router";

import Breadcrumb from "@/components/Breadcrumb";


const BreadcrumbSV = ({ items }) => {

  const router = useRouter();
  const routerPath = router.asPath;

  return (
    <>
      <Breadcrumb data={items} />
    </>
  );
};

export default BreadcrumbSV;
