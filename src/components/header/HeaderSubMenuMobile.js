import Link from "next/link";
import { appSettings } from "@/configs";

import { getUrlHasRoot } from "@/lib/url";

import useLockBodyScroll from "@/lib/useLockBodyScroll";

import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";

const HeaderSubMenuMobile = ({ menu }) => {
  useLockBodyScroll();

  const getMenuItem = (tar) =>
    menu
      .filter((item) => item.ParentId == tar)
      .sort((a, b) => (a.OrderView > b.OrderView ? -1 : 1));

  const menuParent = getMenuItem(null);

  return (
    <>
      <div className="relative z-40 bg-gray-100 shadow-sm mobile-menu__content fadeInDown">
        <div style={{ marginTop: "53px" }} className="mb-10">
          <Accordion collapsible className="bg-white divide-y divide-gray-200">
            {menuParent.map((item, index) => (
              <AccordionItem
                className="block text-gray-600 cursor-pointer hover:text-gray-900 "
                key={item.Id}
              >
                <AccordionButton className="block w-full text-left focus:outline-none">
                  <div className="relative block px-4 py-3 ">
                    {item.Name}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="absolute inline w-5 h-5 text-gray-500  icon right-4 top-4"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                </AccordionButton>

                <AccordionPanel>
                  {getMenuItem(item.Id).length > 0 && (
                    <div className="grid grid-cols-2 px-3 pt-1 bg-gray-100 shadow-inner gap-x-2 mobile-menu__collapse">
                      {getMenuItem(item.Id).map((child, index) => (
                        <Link href={getUrlHasRoot(child.Url)} key={child.Id}>
                          <a
                            // href={getUrlHasRoot(child.Url)}
                            // key={child.Id}
                            className="flex flex-row items-center py-3 border-b border-gray-200 flex-nowrap hover:bg-pink-100"
                          >
                            <div className="flex-none pr-1 ">
                              <img src={child.Icon} className="w-5 " />
                            </div>

                            <div className="flex-1 ">
                              <div className="text-gray-700 whitespace-nowrap text-tiny group-hover:text-pink-700 ">
                                {child.Name}
                              </div>
                            </div>
                          </a>
                        </Link>
                      ))}
                    </div>
                  )}
                </AccordionPanel>
              </AccordionItem>
            ))}
          </Accordion>
          <div className="p-4 pt-3 pb-5">
            <h4 className="mt-5 mb-2 text-sm font-semibold text-gray-700 ">
              TẢI VÀ ĐĂNG KÝ
            </h4>

            <div className="flex flex-nowrap">
              <div className="flex-initial">
                <Link href="https://itunes.apple.com/vn/app/id918751511?utm_source=website-momo&utm_medium=download&utm_campaign=momo-1dong">
                  <a className="link-white" target="_blank">
                    <img
                      src="https://static.mservice.io/img/momo-upload-api-210724113855-637627235353131497.jpg"
                      width={130}
                      loading="lazy"
                      className="img-fluid"
                      alt=""
                    />
                  </a>
                </Link>
              </div>
              <div className="flex-initial ml-3">
                <Link href="https://play.google.com/store/apps/details?id=com.mservice.momotransfer&utm_source=website-momo&utm_medium=download&utm_campaign=momo-1dong">
                  <a className="link-white" target="_blank">
                    <img
                      src="https://static.mservice.io/img/momo-upload-api-210724113959-637627235994410679.jpg"
                      width={130}
                      loading="lazy"
                      className="img-fluid"
                      alt=""
                    />
                  </a>
                </Link>
              </div>
            </div>

            <h4 className="mt-5 mb-2 font-semibold text-gray-700 text-tiny">
              HỖ TRỢ KHÁCH HÀNG
            </h4>

            <ul className="mb-2 text-gray-500 text-tiny">
              <li className="mb-1">
                Hotline :
                <Link href={`tel:${appSettings.MoMoText.Support.Hotline}`}>
                  <a className="text-gray-600">
                    {appSettings.MoMoText.Support.Hotline_Format}
                  </a>
                </Link>
              </li>
              <li className="mb-1">
                Email:
                <Link href={`mailto:${appSettings.MoMoText.Support.Email}`}>
                  <a className="text-gray-600">
                    {appSettings.MoMoText.Support.Email}
                  </a>
                </Link>
              </li>
            </ul>
            <a
              href="https://momo.vn/huong-dan/huong-dan-gui-yeu-cau-ho-tro-bang-tinh-nang-tro-giup"
              rel="noreferrer"
              target="_blank"
            >
              <div className="relative inline-block py-1 pl-10 pr-2 overflow-hidden bg-white border border-gray-300 rounded hover:bg-gray-100">
                <div
                  className="absolute left-1 top-1 "
                  style={{ paddingTop: "3px" }}
                >
                  <svg
                    className="text-pink-700 w-7 h-7 "
                    fill="currentColor"
                    stroke="currentColor"
                    viewBox="0 0 345.1 512"
                  >
                    <g>
                      <title>Asset 1</title>
                      <path
                        d="M279.4,23.7H30.8C14.5,23.7,0,38.2,0,56.3v401.8c0,16.3,14.5,30.8,30.8,30.8H76h23.8L76,449.4H34.5V96.2h243.1v152l34.5,22
                          V56.3C312,38.2,297.5,23.7,279.4,23.7z M226.8,77.1H86.1c-8.1,0-13.5-5.4-13.5-13.5c0-8.1,5.4-13.5,13.5-13.5h140.8
                          c5.4,0,10.8,5.4,10.8,13.5C237.7,71.7,232.3,77.1,226.8,77.1z"
                      ></path>
                      <path
                        d="M189.4,200.7c-14.4,0-25.9,11.6-25.9,25.9v155.7l-17.3-34.6c-14.2-26.3-28.1-23.6-38.9-17.3c-12.5,8.3-17.2,17-8.6,38.9
                          c19.6,48.2,49.8,105.6,82.2,142.7h116.7c41-30.4,74-175,17.3-181.6c-5.2,0-13.5,0.8-17.3,4.3c0-17.3-15.1-21.7-21.6-21.6
                          c-7.5,0.1-13,4.3-17.3,13c0-17.3-14.1-21.6-21.6-21.6c-8.3,0-17.9,5.2-21.6,13v-90.8C215.4,212.3,203.8,200.7,189.4,200.7z"
                      ></path>
                    </g>
                  </svg>
                </div>
                <div className="text-xs text-gray-500 ">
                  Hướng dẫn trợ giúp trên
                </div>
                <div className="font-semibold text-gray-800 text-tiny">
                  Ứng dụng Ví MoMo
                </div>
              </div>
            </a>
            <h4 className="mt-5 mb-2 font-semibold text-gray-700 text-tiny">
              HỢP TÁC DOANH NGHIỆP
            </h4>

            <ul className="mb-2 text-gray-500 text-tiny">
              <li className="mb-1">
                Hotline :
                <Link href={`tel:${appSettings.MoMoText.Business.Phome}`}>
                  <a className="text-gray-600">
                    {appSettings.MoMoText.Business.Phome_Format}
                  </a>
                </Link>
              </li>
              <li className="mb-1">
                Email :
                <Link href={`mailto:${appSettings.MoMoText.Business.Email}`}>
                  <a className="text-gray-600">
                    {" "}
                    {appSettings.MoMoText.Business.Email}
                  </a>
                </Link>
              </li>
            </ul>

            <a
              href="https://business.momo.vn/#menutop"
              rel="noreferrer"
              target="_blank"
            >
              <div className="relative inline-block py-1 pl-12 pr-2 overflow-hidden bg-white border border-gray-300 rounded hover:bg-gray-100">
                <div className="absolute left-1 top-1 ">
                  <img
                    src="https://static.mservice.io/fileuploads/svg/momo-file-210724114053.svg"
                    className="w-10"
                    loading="lazy"
                  />
                </div>
                <div className="text-xs text-gray-500 ">
                  Hợp tác doanh nghiệp
                </div>
                <div className="font-semibold text-gray-800 text-tiny">
                  Đăng ký hợp tác
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>

      <style jsx>{`
        .mobile-menu__collapse {
          margin-bottom: -1px;
        }

        .mobile-menu__content {
          -webkit-overflow-scrolling: auto;
          top: 0;
          left: 0px;
          bottom: 0px;
          right: 0px;
          position: fixed;
          height: 100vh;
          overflow-y: auto;
          pointer-events: auto;
        }
      `}</style>
    </>
  );
};

export default HeaderSubMenuMobile;
