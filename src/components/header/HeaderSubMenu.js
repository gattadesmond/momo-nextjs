import Link from "next/link";

import { useState, useEffect, useCallback } from "react";

import { getUrlHasRoot } from "@/lib/url";

const HeaderSubMenu = ({ data }) => {
  return (
    <>
      {data.length > 8 ? (
        <div
          // className={`absolute   ${
          //   activeMenu == item.Id
          //     ? "is-active translate-y-0 visible opacity-100"
          //     : "translate-y-4 invisible opacity-0"
          // }`}
          className="absolute opacity-0 submenu fadeInUp"
          style={{ width: "890px", opacity: "0" }}
        >
          <div className="grid grid-cols-3 gap-2 p-3 bg-white border border-gray-200 rounded shadow-xl">
            {data.map((child, index) => (
              <Link href={getUrlHasRoot(child.Url)} key={child.Id}>
                <a
                  // href={getUrlHasRoot(child.Url)}
                  // key={child.Id}
                  className="flex flex-row items-center px-2 py-2 rounded-md flex-nowrap group hover:bg-opacity-50 hover:bg-pink-100 "
                >
                  <div className="flex-none pr-3 ">
                    <img
                      src={child.Icon}
                      className="h-9 w-9"
                      width={36}
                      height={36}
                      loading="lazy"
                    />

                    {/* <div
                      className="bg-cover h-9 w-9 "
                      style={{
                        backgroundImage: `url(${child.Icon})`,
                      }}
                    >
                      {" "}
                    </div> */}
                  </div>

                  <div className="flex-1 ">
                    <div className="font-bold text-gray-700 whitespace-nowrap group-hover:text-pink-700 ">
                      {child.Name}
                    </div>
                    <div className="text-xs opacity-50 ">
                      {child.Description}
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      ) : (
        <div
          className="absolute opacity-0 submenu fadeInUp"
          style={{ width: "340px", opacity: "0" }}
        >
          <div className="grid grid-cols-1 gap-2 p-3 bg-white border border-gray-200 rounded shadow-xl">
            {data.map((child, index) => (
              <Link href={getUrlHasRoot(child.Url)} key={child.Id}>
                <a
                  // href={getUrlHasRoot(child.Url)}
                  // key={child.Id}
                  className="flex flex-row items-center px-2 py-2 rounded-md flex-nowrap group hover:bg-opacity-50 hover:bg-pink-100 "
                >
                  <div className="flex-none pr-3 ">
                    <img
                      src={child.Icon}
                      className="h-9 w-9"
                      width={36}
                      height={36}
                      loading="lazy"
                    />
                  </div>

                  <div className="flex-1 ">
                    <div className="font-bold text-gray-700 whitespace-nowrap group-hover:text-pink-700 ">
                      {child.Name}
                    </div>
                    <div className="text-xs opacity-50 ">
                      {child.Description}
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default HeaderSubMenu;
