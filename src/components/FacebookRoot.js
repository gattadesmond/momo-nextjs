import { appSettings } from "@/configs";

const FacebookRoot = (props) => {
  if (!appSettings.AppConfig.FACEBOOK_APP_ID) return <></>;

  return (
  <>
    <div id="fb-root"></div>
    <script
      dangerouslySetInnerHTML={{
        __html: `
          (function (d, s, id, appId) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.async = true; js.defer = true; js.crossorigin = "anonymous";
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=' + appId + '&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk', '${appSettings.AppConfig.FACEBOOK_APP_ID}'));
        `,
      }}
    >
    </script>
  </>
  );
}

export default FacebookRoot;