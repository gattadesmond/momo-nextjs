
import { memo } from "react";

import dynamic from 'next/dynamic';
import useInView from "react-cool-inview";


const FooterContent = dynamic(() => import('../components/footer/FooterContent'));

const Footer = () => {

  const { observe, inView } = useInView({
    threshold: 0.25,
    onEnter: ({ unobserve }) => unobserve(),
  });


  return (
    <footer className="px-0 py-5 bg-gray-800 md:px-5 min-h-[834px] md:min-h-[367px]" ref={observe}>
      {inView && <FooterContent />}
    </footer>
  );
};

export default memo(Footer);
