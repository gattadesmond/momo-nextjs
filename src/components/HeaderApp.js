import { memo, useState, useEffect } from "react";
import useCopyToClipboard from "@/lib/useCopyToClipboard";
import MaxApi from "@momo-platform/max-api";

import {
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
} from "@reach/menu-button";

const HeaderApp = ({ link, theme = "light" }) => {

  const [copied, copy] = useCopyToClipboard(link);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
   


    MaxApi.init({
      appId: "vn.momo.web.quyengop",
      name: "vn.momo.web.quyengop",
      displayName: "Quyên Góp",
      client: {
        web: {
          hostId: "vn.momo.web.quyengop",
          accessToken:
            "U2FsdGVkX1/GbSqeoCZwUA4RlSDjsqVFWWtqL6Re9Rxc1LEGiZHMg7VgjxY89CBf3KsQngY3gW0fU7fQMJsmiNK2fNu60l6rxnNrGtAuE3s=",
        },
      },
      configuration_version: 1,
    });

    let scrolling = false,
      previousTop = 0,
      scrollDelta = 10,
      scrollOffset = 150;

    let selectNav = document.querySelector(".soju-wrapper");

    function handleScrollNav() {
  
      let currentTop = selectNav.scrollTop;
      if (previousTop - currentTop > scrollDelta) {
        setVisible(false);
      } else if (
        currentTop - previousTop > scrollDelta &&
        currentTop > scrollOffset
      ) {
        setVisible(true);
      }
      previousTop = currentTop;
      scrolling = false;
    }


    // function setScrollVisible() {
    //   window.pageYOffset > 500 ? setVisible(true) : setVisible(false);
    // }
    selectNav.addEventListener("scroll", handleScrollNav, { passive: true });

    return function clean() {
      selectNav.removeEventListener("scroll", handleScrollNav);
    };
  }, []);

  const color = {
    light: {
      color: "#b1b0b6",
      bg: "#585858",
    },
    dark: {
      color: "#717173",
      bg: "#c3c3c5",
    },
  };


  function shareFacebook() {
    try {
      MaxApi.shareFacebook({ link: link });
      var objShare = { action: "shareFacebookUrl", value: link };
      window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
    } catch (error) { }
  }

  function shareOther() {
    try {
      MaxApi.share({ message: link });
      var objShare = { action: "shareContent", value: link };
      window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
    } catch (error) { }
  }

  function copyToClipboard() {
    copy();
  }

  return (
    <nav
      className={` fixed top-0 left-0 right-0 px-4 pt-4 z-30 flex items-center pointer-events-none nav-viewapp overflow-x-hidden ${visible ? "is-active" : ""
        } `}
    >
      {/* <div className="pointer-events-auto cursor-pointer" onClick={() => closeApp()}>


        <svg fill="none" height={33} viewBox="0 0 33 33" width={33} xmlns="http://www.w3.org/2000/svg"> <rect fill={color[theme].bg} height="32.9317" rx="16.4659" width="32.9317" /> <path d="M22.25 10.75L10.75 22.25" stroke={color[theme].color} strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} /> <path d="M10.75 10.75L22.25 22.25" stroke={color[theme].color} strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} /> </svg>



      </div> */}

      <div className="flex-1 "></div>

      <div className="pointer-events-auto cursor-pointer flex">
        <Menu>
          <MenuButton className="">
            <svg
              fill="none"
              height={30}
              style={{ background: color[theme].color, borderRadius: "3px" }}
              viewBox="0 0 31 30"
              width={31}
              xmlns="http://www.w3.org/2000/svg"
            >
              {" "}
              <path
                d="M31 3.21429V26.7857C31 28.5609 29.513 30 27.6786 30H3.32143C1.48703 30 0 28.5609 0 26.7857V3.21429C0 1.43906 1.48703 0 3.32143 0H27.6786C29.513 0 31 1.43906 31 3.21429ZM21.0357 17.6786C20.0281 17.6786 19.1105 18.051 18.4213 18.661L13.7188 15.9305C13.8794 15.3199 13.8794 14.68 13.7188 14.0694L18.4213 11.3389C19.1105 11.949 20.0281 12.3214 21.0357 12.3214C23.1758 12.3214 24.9107 10.6425 24.9107 8.57143C24.9107 6.50036 23.1758 4.82143 21.0357 4.82143C18.8956 4.82143 17.1607 6.50036 17.1607 8.57143C17.1607 8.89266 17.2026 9.20444 17.2811 9.50203L12.5787 12.2325C11.8895 11.6224 10.9719 11.25 9.96428 11.25C7.82418 11.25 6.08929 12.9289 6.08929 15C6.08929 17.0711 7.82418 18.75 9.96428 18.75C10.9719 18.75 11.8895 18.3776 12.5787 17.7676L17.2812 20.498C17.201 20.8021 17.1606 21.1148 17.1608 21.4286C17.1608 23.4997 18.8957 25.1786 21.0358 25.1786C23.1759 25.1786 24.9108 23.4997 24.9108 21.4286C24.9107 19.3575 23.1758 17.6786 21.0357 17.6786Z"
                fill={color[theme].bg}
              />{" "}
            </svg>
          </MenuButton>



          <MenuPopover className="dropdown-filter-mobile fadeInDown  w-[240px] absolute z-10 mt-1 rounded-lg px-3 py-2 overflow-auto focus:outline-none shadow-level3 bg-white  ">
            <div className="arbitrary-element">
              <MenuItems className="grid grid-cols-1 gap-x-2 gap-y-1 divide-y">
                <MenuItem
                  className="cursor-pointer select-none relative py-2 pl-10 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                  onSelect={() => copyToClipboard()}
                >
                  {" "}
                  Copy link{" "}
                  <span className="absolute left-1 top-2.5">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1" />
                    </svg>
                  </span>
                </MenuItem>

                <MenuItem
                  className="cursor-pointer select-none relative py-2 pl-10 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                  onSelect={() => shareFacebook()}
                >
                  {" "}
                  Chia sẻ qua Facebook{" "}
                  <span className="absolute left-1 top-2.5">
                    <img
                      src="https://momo.vn/momo2020/img/lacxi/i-fb.png"
                      className="w-5"
                    />
                  </span>
                </MenuItem>

                <MenuItem
                  className="cursor-pointer select-none relative py-2 pl-10 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                  onSelect={() => shareOther()}
                >
                  {" "}
                  Chia sẻ khác{" "}
                  <span className="absolute left-1 top-2">
                    <img
                      src="https://static.mservice.io/pwa/images/social/share.svg"
                      className="w-6"
                    />
                  </span>
                </MenuItem>
              </MenuItems>
            </div>
          </MenuPopover>
        </Menu>
      </div>



      {/* {<svg onClick={() => copy()} className="" xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 md:flex" fill="none" viewBox="0 0 24 24" stroke="currentColor">
           <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 5H6a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2v-1M8 5a2 2 0 002 2h2a2 2 0 002-2M8 5a2 2 0 012-2h2a2 2 0 012 2m0 0h2a2 2 0 012 2v3m2 4H10m0 0l3-3m-3 3l3 3" />
        </svg>}

        <svg onClick={() => copy()}  xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
           <path d="M15 8a3 3 0 10-2.977-2.63l-4.94 2.47a3 3 0 100 4.319l4.94 2.47a3 3 0 10.895-1.789l-4.94-2.47a3.027 3.027 0 000-.74l4.94-2.47C13.456 7.68 14.19 8 15 8z" />
        </svg> */}

      <style jsx>{`
        .nav-viewapp {
          transform: translateZ(0);
          will-change: transform;
          transition: transform 0.5s, background-color 0.2s;

          padding-top: 1rem;
          padding-top: Max(env(safe-area-inset-top), 1rem);
        }
        .nav-viewapp.is-active {
          transform: translateY(-100%);
        }

    
      `}</style>
    </nav>
  );
};

export default memo(HeaderApp);
