export default function Container({ children, className }) {
  return (
    <div
      className={`max-w-6xl mx-auto w-full px-5 md:px-8 lg:px-8 ${className ? className : ""}`}
    >
      {children}
    </div>
  );
}
