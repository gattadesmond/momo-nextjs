export default function Container({ children }) {
  return (
    <div className="max-w-6xl mx-auto w-full px-6 lg:px-8 ">{children}</div>
  );
}
