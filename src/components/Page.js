import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
// import { useTheme } from "next-themes";
import Header from "./Header";
import HeaderApp from "./HeaderApp";
import ScrollToTop from "@/components/ScrollToTop";
// import GoogleAnalytics from "@/components/GoogleAnalytics";
import { appSettings } from "@/configs";
import parse, { domToReact } from "html-react-parser";

import smoothscroll from "smoothscroll-polyfill";

import TagManager from "react-gtm-module";

import Footer from "./Footer";

export default function Page(props) {
  const {
    Trace,
    className,
    children,
    link = "",
    isViewApp = false,
    isLP = false,
    isShowHeader = true,
    isShowFooter = true,
    GTM = {
      Disabled: false,
      SwapIdCase: 1,
    },
    ...customMeta
  } = props;

  useEffect(() => {
    smoothscroll.polyfill();

    if (Trace) {
      var img = document.createElement("img");
      img.setAttribute(
        "src",
        `${appSettings.AppConfig.HOST_API}/trace/${Trace.Type}/${Trace.Id}`
      );
      img.setAttribute(
        "style",
        "position: absolute;display: block;z-index: -10000"
      );
      document.body.appendChild(img);
    }
  }, []);
  useEffect(() => {
    window.clearTimeout(window._timeoutGTM);
    window._timeoutGTM = window.setTimeout(() => {
      if (!GTM.Disabled) {
        const tagManagerArgs = {
          gtmId:
            GTM.SwapIdCase == 2
              ? appSettings.AppConfig.GOOGLE_ANALYTICS_ID2
              : appSettings.AppConfig.GOOGLE_ANALYTICS_ID,
        };
        TagManager.initialize(tagManagerArgs);
      }
    }, 2000);
  }, [isViewApp]);

  const router = useRouter();
  const meta = {
    title: "Ví Điện Tử MoMo - Siêu Ứng Dụng Thanh Toán số 1 Việt Nam",
    description: `Ví Điện Tử MoMo - Siêu Ứng Dụng Thanh Toán với hàng trăm tiện ích với hơn 23 triệu người tin dùng ✅An toàn ✅Bảo mật ✅Tiện lợi ✅Nhanh chóng.`,
    image:
      "https://static.mservice.io/img/momo-upload-api-210413152735-637539244553562967.jpg",
    type: "website",
    ...customMeta,
    header: [],
  };
  //Header data
  const headerData = props.header;

  const isMobile = props.isMobile;

  const clearMetaUrl = (url) => {
    if (!url) return url;
    var idx = url.indexOf("?");

    if (idx > 0) url = url.substr(0, idx);

    idx = url.indexOf("#");
    if (idx > 0) url = url.substr(0, idx);

    return url;
  };

  const renderMetaRobots = (robots) => {
    if (robots) {
      if (robots.includes("noindex")) {
        return (
          <>
            <meta name="robots" content="noindex, nofollow" />
            <meta name="googlebot" content="noindex, nofollow" />
          </>
        );
      } else {
        return robots;
      }
    } else {
      return (
        <>
          <meta name="robots" content="index, follow" />
          <meta name="googlebot" content="index, follow" />
        </>
      );
    }
  };

  const checkHeaderApp = isLP || isViewApp;
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, user-scalable=no,  initial-scale=1.0, viewport-fit=cover"
        />
        <title>{meta.title}</title>
        <meta content={meta.description} name="description" />

        {meta.keywords && <meta name="keywords" content={meta.keywords} />}

        <meta
          property="og:url"
          content={`https://momo.vn${clearMetaUrl(router.asPath)}`}
        />
        <link
          rel="canonical"
          href={`https://momo.vn${clearMetaUrl(router.asPath)}`}
        />
        <meta property="og:type" content={meta.type} />
        <meta property="og:site_name" content="Vi MoMo" />
        <meta property="og:description" content={meta.description} />
        <meta property="og:title" content={meta.title} />
        <meta property="og:image" content={meta.image} />

        {renderMetaRobots(meta.robots)}

        <meta
          name="abstract"
          content="Ví điện tử MoMo - Siêu ứng dụng thanh toán số 1 Việt Nam"
        />
        <meta name="distribution" content="Global" />
        <meta name="author" content="Ví MoMo" />

        <meta property="fb:app_id" content="320653355376196" />
        <meta property="fb:pages" content="138010322921640" />
        <link
          rel="SHORTCUT ICON"
          href="https://static.mservice.io/jk/momo.ico"
        />

        {meta.date && (
          <meta property="article:published_time" content={meta.date} />
        )}

        <meta name="language" content="vietnamese" />
        <meta name="copyright" content="Copyright © 2019 by MOMO.VN" />
        <meta name="REVISIT-AFTER" content="1 DAYS" />
        <meta name="RATING" content="GENERAL" />
        <meta httpEquiv="x-dns-prefetch-control" content="on" />
        <link rel="dns-prefetch" href="//www.google-analytics.com" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//www.googletagservices.com" />
        <link rel="dns-prefetch" href="//www.googletagmanager.com" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//static.mservice.io" />
        <link rel="dns-prefetch" href="//www.google.com" />
        <link rel="dns-prefetch" href="//www.google.com.vn" />
        <link rel="dns-prefetch" href="//www.googleadservices.com" />
        <link rel="preconnect" href="//connect.facebook.net" />
        <link rel="preconnect" href="//www.googletagmanager.com" />
        <link rel="preconnect" href="//www.google-analytics.com" />
        <link rel="preconnect" href="//googleads.g.doubleclick.net" />
        <link rel="preconnect" href="//static.mservice.io" />
        <link rel="preconnect" href="//www.google.com" />
        <link rel="preconnect" href="//www.google.com.vn" />
        <link rel="preconnect" href="//www.googleadservices.com" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `{
            "@context": "https://schema.org/",
            "@type": "WebSite",
            "@id":"https://momo.vn/#website",
            "name": "Ví MoMo",
            "url": "https://momo.vn",
            "potentialAction": {
            "@type": "SearchAction",
            "target": "https://momo.vn/tim-kiem?q={search_term_string}",
            "query-input": "required name=search_term_string"
            }}`,
          }}
        ></script>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `{
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "Ví Điện Tử MoMo",
            "alternateName": "Ví Điện Tử MoMo - Siêu Ứng Dụng Thanh Toán số 1 Việt Nam",
            "legalName" : "ONLINE MOBILE SERVICES JSC (M_Service)",
            "url": "https://momo.vn",
            "logo": "https://static.mservice.io/img/logo-momo.png",
            "foundingDate": "2007",
            "founders": [
            {
            "@type": "Person",
            "name": "Phạm Thành Đức"
            }],
            "address": {
            "@type": "PostalAddress",
            "streetAddress": "12 Tân Trào, Phường Tân Phú, Quận 7",
            "addressLocality": "Hồ Chí Minh",
            "addressRegion": "VN",
            "postalCode": "700000",
            "addressCountry": "VN"
            },
            "contactPoint": {
            "@type": "ContactPoint",
            "contactType": "customer support",
            "telephone": "+841900545441",
            "email": "hotro@momo.vn"
            },
            "sameAs": [
            "https://www.facebook.com/vimomo/",
            "https://www.youtube.com/channel/UCKHHW-qL2JoZqcSNm1jPlqw",
            "https://www.linkedin.com/company/momo-mservice/",
            "https://github.com/momo-wallet"
            ]
            }`,
          }}
        ></script>

        {/* <GoogleAnalytics Type={1} /> */}
      </Head>

      {/* {!isViewApp ? (
        <Header data={headerData} isMobile={isMobile} />
      ) : (
        <HeaderApp link={link}></HeaderApp>
      )} */}

      {!checkHeaderApp && isShowHeader && (
        <Header data={headerData} isMobile={isMobile} />
      )}

      <main className={`soju-wrapper  lg:mt-0 ${className}`}>
        {/* <GoogleAnalytics Type={2} /> */}
        {children}
      </main>

      <ScrollToTop />

      {!checkHeaderApp && isShowFooter && <Footer />}

      {meta.scripts && parse(meta.scripts)}
      {/* .page-blog a {
           user-select: none;
           cursor: auto;
           pointer-events: none;   
         }
           .page-article a {
          user-select: none;
          cursor: auto;
          pointer-events: none;   
        } */}
      {isViewApp && (
        <style jsx global>
          {`
            html,
            body {
              position: relative;
              height: 100%;
              width: 100%;
              overflow-x: hidden;
            }
            body {
              overflow: hidden;
            }

            #__next {
              box-sizing: border-box;
              position: absolute;
              left: 0;
              top: 0;
              width: 100%;
              height: 100%;
              transform: none;
              z-index: 1;
            }

            .soju-wrapper {
              height: 100%;
              position: relative;
              overflow: auto;
              -webkit-overflow-scrolling: touch;
              padding-top: Max(env(safe-area-inset-top), 0);
              padding-bottom: 0;
            }

            .footer-cta {
              opacity: 1 !important;
              user-select: auto !important;
              pointer-events: auto !important;
            }

            .page-blog .ckemm_ads,
            .page-blog .ckemm_cta,
            .page-blog .btn-primary-2,
            .page-blog .breadcrumb,
            .page-blog .social,
            .page-blog .blogs-relative,
            .page-blog .rating {
              display: none;
            }

            .page-blog {
            }
            .page-blog .toc-mobile {
              display: none;
            }

            .page-article .ckemm_ads,
            .page-article .ckemm_cta,
            .page-article .btn-primary-2,
            .page-article .header-mobile,
            .page-article .header-desktop,
            .page-article .social,
            .page-article .rating,
            .page-article .short-info {
              display: none;
            }
          `}
        </style>
      )}
    </>
  );
}
