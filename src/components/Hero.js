import dynamic from "next/dynamic";

const Desktop = dynamic(() => import("../components/hero/HeroDesktop"));
const Mobile = dynamic(() => import("../components/hero/HeroMobile"));

const Hero = (props) => {
  return (
    <>
      {props.isMobile == true ? (
        <Mobile data={props.data} svgColor={props.svgColor}/>
      ) : (
        <Desktop data={props.data} svgColor={props.svgColor}/>
      )}
    </>
  );
};

export default Hero;
