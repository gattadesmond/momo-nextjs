import Image from "next/image";

import SwiperCore, { Navigation, Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Navigation, Pagination]);

const HeroMobile = ({ data ,svgColor = "text-white" }) => {
  return (
    <div className="slide-mobile relative">
      <Swiper
        spaceBetween={0}
        pagination={{ clickable: true }}
        navigation
        slidesPerView={1}
        className="bg-black "
      >
        {data.map((item, index) => (
          <SwiperSlide key={index}>
            <div className="aspect-mobile">
              {item.Url ? (
                <Link href={item.Url}>
                  <a target={item.IsNewTab ? "_blank" : ""}>
                    <Image
                      src={item.AvatarMobile}
                      alt="momo"
                      layout="fill"
                      objectFit="cover"
                    />
                  </a>
                </Link>
              ) : (
                <Image
                  src={item.AvatarMobile}
                  alt="momo"
                  layout="fill"
                  objectFit="cover"
                />
              )}
            </div>
          </SwiperSlide>
        ))}
      </Swiper>

      <div className={`absolute svg-line z-2 left-0 right-0 select-none pointer-events-none ${svgColor}`}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          preserveAspectRatio="none"
          viewBox="0 0 1680 40"
          className="sud-curve"
        >
          <path d="M0 40h1680V30S1340 0 840 0 0 30 0 30z" fill="currentColor" />
        </svg>
      </div>

      <style jsx>{`
        .svg-line{
          bottom: -1px;
        }
        .aspect-mobile {
          position: relative;
          padding-bottom: 120%;
        }

        .slide-mobile {
          --swiper-navigation-size: 34px;
        }
      `}</style>
    </div>
  );
};

export default HeroMobile;
