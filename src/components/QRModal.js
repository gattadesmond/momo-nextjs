

import QRCodeScan from "@/components/QrCodeScan";
import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";

const QRModal = ({ isOpen, onDismiss, isDark, img, qrImage, title, shortTitle }) => {
  return (
    <>
      <Modal isOpen={isOpen} onDismiss={onDismiss} isDark={isDark}>
        <ModalBody css="p-0 bg-white rounded">
          <div className="flex flex-row flex-wrap ">
            <div
              className="flex-initial w-full h-auto md:w-7/12 md:order-2"
              style={{
                background:
                  "url(https://static.mservice.io/jk/momo2020/img/intro/qrcode-pattern.png) 10px 10px no-repeat,linear-gradient(to top,#c1177c,#e11b90)",
              }}
            >
              <div className="px-5 py-10 text-center ">
                <h4 className="mb-5 text-base text-white ">{title}</h4>
                <QRCodeScan img={img}>
                  {qrImage}
                </QRCodeScan>
              </div>
            </div>

            <div className="flex-none w-full md:w-5/12 md:order-1">
              <div className="px-5 py-8 text-sm ">
                <div className="flex items-center flex-nowrap">
                  <div className="flex-none">
                    <img
                      className="w-20 "
                      loading="lazy"
                      src="https://static.mservice.io/images/s/momo-upload-api-200917091602-637359309621891617.png"
                    />
                  </div>

                  <div className="flex-1 pl-4">
                    <div className="font-semibold text-gray-800 step ">
                      Bước 1 :
                    </div>

                    <div className="mt-1 text-gray-500 ">
                      Mở ứng dụng camera mặc định hoặc ứng dụng hỗ trợ QR code
                      của bạn
                    </div>
                  </div>
                </div>

                <div className="flex items-center mt-5 flex-nowrap">
                  <div className="flex-none">
                    <img
                      className="w-20 "
                      loading="lazy"
                      src="https://static.mservice.io/images/s/momo-upload-api-200917091443-637359308837905996.png"
                    />
                  </div>

                  <div className="flex-1 pl-4">
                    <div className="font-semibold text-gray-800 step ">
                      Bước 2 :
                    </div>

                    <div className="mt-1 text-gray-500 ">
                      Quét mã QR Code theo hình bên phải
                    </div>
                  </div>
                </div>

                <div className="flex items-center mt-5 flex-nowrap">
                  <div className="flex-none">
                    <img
                      className="w-20 "
                      loading="lazy"
                      src="https://static.mservice.io/images/s/momo-upload-api-200917090146-637359301062519803.png"
                    />
                  </div>

                  <div className="flex-1 pl-4">
                    <div className="font-semibold text-gray-800 step ">
                      Bước 3 :
                    </div>

                    <div className="mt-1 text-gray-500 ">
                      Bấm vào thông báo hiển thị để tải ứng dụng hoặc {shortTitle}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default QRModal;
