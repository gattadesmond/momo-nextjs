// import HTMLEllipsis from "react-lines-ellipsis/lib/html";
import LinesEllipsis from "react-lines-ellipsis";

import { useState, useEffect } from "react";
import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

const ReadMoreHTML = ({
  children,
  maxLine,
  btnText,
  type = "line",
  isReadMore = true,
}) => {
  const [useEllipsis, setUseEllipsis] = useState(true);

  const handleTextClick = (e, act) => {
    e.preventDefault();
    if (act == false) {
      setUseEllipsis(true);
      return;
    }

    setUseEllipsis(false);
  };

  useEffect(() => {
    setUseEllipsis(true);
  }, [children]);

  if (isReadMore == false) {
    return { children };
  }

  return (
    <>
      {useEllipsis ? (
        <>
          {type == "line" && (
            <div className="pointer-events-none" onClick={handleTextClick}>
              <LinesEllipsis
                className="ellipsis-text"
                text={children}
                maxLine={maxLine}
                ellipsis={btnText}
              />
            </div>
          )}

          {type == "html" && (
            <>
              <div className="is-truncated relative">{children}</div>

              <div className="text-center  mt-4">
                <button
                  type="button"
                  className="bg-[#FFE0D3] text-lx-2 font-bold inline-flex items-center px-4 py-2 rounded-md shadow-inner transition-colors hover:bg-[#f7d1c0]"
                  onClick={handleTextClick}
                >
                  Xem thêm{" "}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 inline-block relative 
                    ml-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M19 9l-7 7-7-7"
                    />
                  </svg>{" "}
                </button>
              </div>
            </>
          )}
        </>
      ) : (
        <>
          {type == "line" && <div className="relative">{children}</div>}

          {type == "html" && (
            <>
              {children}

              <div className="text-center  mt-4">
                <button
                  type="button"
                  className="bg-[#FFE0D3] text-lx-2 font-bold inline-flex items-center px-4 py-2 rounded-md shadow-inner transition-colors hover:bg-[#f7d1c0]"
                  onClick={(e) => handleTextClick(e, false)}
                >
                  Thu gọn {`  `} 
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 inline-block relative ml-2 rotate-180 transform"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M19 9l-7 7-7-7"
                    />
                  </svg>{" "}
                </button>
              </div>
            </>
          )}
        </>
      )}
      <style jsx global>{`
        .LinesEllipsis-ellipsis {
          pointer-events: auto;
        }

        .is-truncated {
          height: 220px;
          max-height: 220px;
          overflow: hidden;
        }

        .is-truncated:after {
          background: linear-gradient(
            0deg,
            rgba(255, 235, 233, 1),
            rgba(255, 255, 255, 0) 100%
          );
          content: "";
          position: absolute;
          bottom: 0;
          left: 0;
          height: 60px;
          width: 100%;
        }
      `}</style>
    </>
  );
};

export default ReadMoreHTML;
