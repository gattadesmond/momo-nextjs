const LdJson_QAContent = (props) => {
  const { QaGroupData, QaData } = props;
  
  var ldJson = "";
  if (QaGroupData && QaGroupData.length > 0) {
    QaGroupData.forEach((x) => {
      x.Data.forEach((y) => {
        y.Items.forEach((z) => {
          ldJson +=
            (ldJson ? "," : "") +
            `
          {
            "@type": "Question",
            "name": "${z.Title}",
            "acceptedAnswer":
            {
                "@type": "Answer",
                "text": "${z.Short}"
            }
          }
          `;
        });
      });
    });
  }

  if (QaData && QaData.length > 0) {
    QaData.forEach((x) => {
      ldJson +=
        (ldJson ? "," : "") +
        `
        {
          "@type": "Question",
          "name": "${x.Data.Title}",
          "acceptedAnswer":
          {
              "@type": "Answer",
              "text": "${x.Data.Short}"
          }
        }
        `;
    });
  }

  return ldJson ? (
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{
        __html: `{
            "@context": "http://schema.org",
            "@type": "FAQPage",
            "mainEntity":
            [
            ${ldJson}
            ]
        }`,
      }}
    ></script>
  ) : (
    <></>
  );
};

export default LdJson_QAContent;
