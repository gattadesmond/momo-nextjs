import { forwardRef, useCallback, useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
// import dynamic from "next/dynamic";

import add from "date-fns/add";
import isBefore from "date-fns/isBefore";
import format from "date-fns/format";
import parse from "date-fns/parse";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// import { registerLocale } from "react-datepicker";
import vi from "date-fns/locale/vi";

import Container from "@/components/Container";
import Heading from "@/components/Heading";

import MenuCustomer from "@/components/ota/MenuCustomer";
import MenuAirport from "@/components/ota/MenuAirport";

import MenuCheapFlight from "@/components/ota/MenuCheapFlight";

import MenuLocationFeature from "@/components/ota/MenuLocationFeature";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

import { SwitchHorizontalIcon, SearchIcon } from "@heroicons/react/outline";

import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";

const BlockSearchNew = ({
  className,
  data = null,
  dataListLocation2 = [],
  urlQuery = null,
  airlines = null,
  isMobile,
  ...rest
}) => {
  const backgroundImg = {
    desktop:
      data?.Data?.Items[0].Avatar ||
      "https://static.mservice.io/blogscontents/momo-upload-api-210811175315-637643011953054096.png",
    mobile:
      data?.Data?.Items[0].AvatarMobile ||
      "https://static.mservice.io/blogscontents/momo-upload-api-210811175328-637643012080882941.jpg",
  };

  const listLocation = dataListLocation2?.Data?.Items;

  const dataListLocation = dataListLocation2?.Data?.Items;

  const router = useRouter();
  registerLocale("vi", vi);

  const [tabIndex, setTabIndex] = useState(0);
  const [isReturn, setIsReturn] = useState(false);

  const [showError, setShowError] = useState(false);

  const handleTabsChange = (index) => {
    setTabIndex(index);
  };

  const handleInputChange = (e) => {
    if (e.target.value == 1) {
      setIsReturn(false);
    }
    if (e.target.value == 2) {
      setIsReturn(true);
    }
  };

  ///////////////////

  const minDate = add(new Date(), { days: 1 });

  const defaultFormState = {
    isValid: false,
    values: {
      fromAirport: "",
      toAirport: "",
      customer: {
        adult: 1,
        child: 0,
        infat: 0,
      },
      fromDate: add(new Date(), { days: 1 }),
      toDate: null,
    },
    touched: {},
    errors: {},
    helperText: {},
  };

  const [formState, setFormState] = useState({ ...defaultFormState });

  useEffect(() => {
    let fromAirport = dataListLocation[0];
    let toAirport = dataListLocation[1];

    if (rest.codeTo) {
      const findItem = dataListLocation.find((x) => x.JsId === rest.codeTo);
      if (findItem) {
        toAirport = findItem;
      }
      if (!rest.codeFrom && findItem.JsId === fromAirport.JsId) {
        fromAirport = dataListLocation.filter(
          (x) => x.JsId !== findItem.JsId
        )[0];
      }
    }
    if (rest.codeFrom) {
      const findItem = dataListLocation.find((x) => x.JsId === rest.codeFrom);
      if (findItem) {
        fromAirport = findItem;
      }
    }

    if (urlQuery) {
      const pax = (urlQuery.pax || "1.0.0").split(".");
      const code = urlQuery.route.split(".");
      const fintItem0 = dataListLocation.find((x) => x.JsId === code[0]);
      if (fintItem0) {
        fromAirport = fintItem0;
      }
      const fintItem1 = dataListLocation.find((x) => x.JsId === code[1]);
      if (fintItem1) {
        toAirport = fintItem1;
      }

      setIsReturn(urlQuery.flightType === "roundtrip");
      setFormState((_state) => ({
        ..._state,
        values: {
          ..._state.values,
          fromAirport: fromAirport,
          toAirport: toAirport,
          customer: {
            adult: parseInt(pax[0] || 1),
            child: parseInt(pax[1] || 0),
            infat: parseInt(pax[2] || 0),
          },
          fromDate: parse(urlQuery.fromDate, "dd-MM-yyyy", new Date()),
          toDate: urlQuery.todate
            ? parse(urlQuery.toDate, "dd-MM-yyyy", new Date())
            : null,
        },
      }));
    } else {
      setFormState((_state) => ({
        ..._state,
        values: {
          ..._state.values,
          fromAirport: fromAirport,
          toAirport: toAirport,
        },
      }));
    }
  }, []);

  const handleChange = useCallback(
    (name, value) => {
      setFormState((_state) => ({
        ..._state,
        values: {
          ..._state.values,
          [name]: value,
        },
        touched: {
          ..._state.touched,
          [name]: true,
        },
      }));
    },
    [setFormState]
  );

  const convertHanhKhach = (customer) => {
    let response = `${customer?.adult || 1} Người lớn`;
    if (customer?.child && customer.child > 0) {
      response += `, ${customer?.child} trẻ em`;
    }
    if (customer?.infat && customer.infat > 0) {
      response += `, ${customer?.infat} em bé`;
    }
    return (
      <span className={`${customer.infat > 0 ? "text-sm" : ""}`}>
        {response}
      </span>
    );
  };

  useEffect(() => {
    if (isReturn) {
      handleChange("toDate", add(formState.values.fromDate, { days: 3 }));
    } else {
      handleChange("toDate", null);
    }
  }, [isReturn]);

  const changeLocation = (e) => {
    e.preventDefault();
    setFormState((_state) => ({
      ..._state,
      values: {
        ..._state.values,
        fromAirport: _state.values.toAirport,
        toAirport: _state.values.fromAirport,
      },
    }));
  };

  const handleChangeFromDate = (date) => {
    handleChange("fromDate", date);
    const _isBefore = isBefore(date, formState.values.toDate);
    if (isReturn && !_isBefore) {
      handleChange("toDate", add(date, { days: 3 }));
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (formState.values.fromAirport.JsId == formState.values.toAirport.JsId) {
      setShowError(true);

      // setTimeout(() => {
      //   setShowError(false)
      // }, 2000)

      return;
    }

    const fligtType = isReturn ? "roundtrip" : "oneway";
    const pax = `${formState.values.customer.adult}.${formState.values.customer.child}.${formState.values.customer.infat}`;
    const routeCode = `${formState.values.fromAirport.JsId}.${formState.values.toAirport.JsId}`;
    const fromDate = format(formState.values.fromDate, "dd-MM-yyyy");

    let href = "";
    if (!isReturn) {
      href = `/ve-may-bay/tim-chuyen?flightType=${fligtType}&route=${routeCode}&fromDate=${fromDate}&pax=${pax}${
        airlines ? `&airlines=${airlines}` : ""
      }`;
    } else {
      const toDate = format(formState.values.toDate, "dd-MM-yyyy");
      href = `/ve-may-bay/tim-chuyen?flightType=${fligtType}&route=${routeCode}&fromDate=${fromDate}&toDate=${toDate}&pax=${pax}${
        airlines ? `&airlines=${airlines}` : ""
      }`;
    }
    router.push(href);
  };

  useEffect(() => {
    // On first render, counter will be 0
    // The condition will be false and setTimeout() won't start
    if (showError == true) {
      var timer = setTimeout(() => setShowError(false), 3000);
    }
    return () => clearTimeout(timer);
  }, [showError]);

  const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
    <div className="stretched-link cursor-pointer" onClick={onClick}>
      <input
        readOnly={true}
        ref={ref}
        value={value}
        className=" w-full cursor-pointer bg-transparent font-bold text-gray-900"
      />
    </div>
  ));

  // console.log("formState", formState);

  return (
    <>
      <div
        className="block-form-ota relative z-10 bg-gray-50"
        id={`block-${data.Id}`}
      >
        <div
          className="ota-bg absolute inset-0 -z-10 h-[200px] bg-[#fff9fb] bg-top bg-no-repeat sm:h-[400px]"
          style={{
            backgroundImage: `url(${backgroundImg.desktop})`,
          }}
        ></div>

        {/* <div className="bg-ota absolute top-0 left-0 right-0 bottom-4">
          <div className="relative block h-full w-full md:hidden">
            <Image
              alt="background"
              src={backgroundImg.mobile}
              layout="intrinsic"
              width={500}
              height={534}
              objectFit="cover"
              priority={true}
            />
          </div>
          <div className="relative hidden h-full w-full md:block">
            <Image
              alt="background"
              src={backgroundImg.desktop}
              layout="fill"
              objectFit="cover"
              unoptimized={true}
              loading="lazy"
            />
          </div>
        </div> */}

        <div className="z-1 ota-widget-search sm:pt-42 relative pt-32   md:pt-60">
          <Container className="relative z-10">
            <Tabs index={tabIndex} onChange={handleTabsChange}>
              <TabList className="z-2 relative -mx-5  flex   flex-nowrap overflow-x-auto rounded-t-md bg-white shadow sm:inline-flex md:mx-0 ">
                <Tab className="flex flex-shrink-0 items-center space-x-2 px-3 pb-2 pt-3 sm:pl-4 sm:pr-5  ">
                  <img
                    src="https://static.mservice.io/fileuploads/svg/momo-file-211118130931.svg"
                    alt=""
                    className=" w-7"
                  />{" "}
                  <span className=" whitespace-nowrap">Tìm chuyến bay</span>
                </Tab>
                <Tab className="flex flex-shrink-0 items-center space-x-2 px-3 pb-2 pt-3 sm:pl-4 sm:pr-5  ">
                  <img
                    src="https://static.mservice.io/fileuploads/svg/momo-file-211118130922.svg"
                    alt=""
                    className=" w-7"
                  />{" "}
                  <span className="whitespace-nowrap">Chặng bay giá rẻ</span>
                </Tab>
                <Tab className="flex flex-shrink-0 items-center space-x-2 px-3 pb-2 pt-3 sm:pl-4 sm:pr-5  ">
                  {" "}
                  <img
                    src="https://static.mservice.io/fileuploads/svg/momo-file-211118130903.svg"
                    alt=""
                    className=" w-7"
                  />{" "}
                  <span className="whitespace-nowrap">Điểm đến nổi bật</span>
                </Tab>
              </TabList>

              <TabPanels className="z-1 relative -mx-5 rounded-b-md bg-white px-3 py-3 shadow-md md:mx-0 md:px-6">
                <TabPanel>
                  <form onSubmit={handleSubmit}>
                    <div className="mt-2 flex  space-x-4">
                      <label className="inline-flex cursor-pointer items-center">
                        <input
                          className="form-radio text-pink-600 focus:shadow-none focus:outline-none focus:ring-0"
                          type="radio"
                          checked={isReturn == false}
                          name="radio-direct"
                          defaultValue={1}
                          onChange={(e) => handleInputChange(e)}
                        />
                        <span className="ml-2">Một chiều</span>
                      </label>
                      <label className="inline-flex cursor-pointer items-center">
                        <input
                          className="form-radio text-pink-600   focus:shadow-none focus:outline-none focus:ring-0"
                          type="radio"
                          name="radio-direct"
                          checked={isReturn == true}
                          defaultValue={2}
                          onChange={(e) => handleInputChange(e)}
                        />
                        <span className="ml-2">Khứ hồi</span>
                      </label>
                    </div>

                    <div className="mt-5 rounded-md border border-gray-300 bg-white  shadow-lg md:shadow-none">
                      <div className="relative  grid  grid-cols-[minmax(100px,_1fr)] md:grid-cols-[minmax(100px,_1fr)_minmax(100px,_1fr)] lg:grid-cols-[400px_250px_230px_minmax(100px,_1fr)]">
                        <div className="relative flex  flex-wrap items-center md:col-span-2 md:flex-nowrap lg:col-span-1">
                          <div className="   flex-grow-1 relative w-full min-w-0 cursor-pointer rounded-l-md  px-4 py-3 transition-colors hover:bg-gray-100">
                            <div className=" text-tiny mb-0.5 flex items-center space-x-1 text-gray-400">
                              <svg
                                className=" h-4 w-4"
                                fill="currentColor"
                                focusable="false"
                                viewBox="0 0 24 24"
                                aria-hidden="true"
                                data-testid="FlightTakeoffIcon"
                              >
                                <path d="M2.5 19h19v2h-19v-2zm19.57-9.36c-.21-.8-1.04-1.28-1.84-1.06L14.92 10l-6.9-6.43-1.93.51 4.14 7.17-4.97 1.33-1.97-1.54-1.45.39 2.59 4.49s7.12-1.9 16.57-4.43c.81-.23 1.28-1.05 1.07-1.85z" />
                              </svg>
                              <span>Nơi đi</span>
                            </div>
                            <div className="  font-bold text-gray-900">
                              <MenuAirport
                                listLocation={listLocation}
                                formState={formState}
                                onChangeItem={handleChange}
                                type={1}
                              />
                            </div>
                          </div>

                          <div className="absolute right-3 top-1/2 z-10 -mx-1 flex-shrink-0 -translate-y-1/2 md:relative md:top-0 md:right-0 md:translate-y-0">
                            <div
                              className="flex h-7 w-7 cursor-pointer items-center justify-center rounded-full border-2 border-pink-200  bg-white ring-8 ring-white hover:bg-pink-50"
                              onClick={(e) => changeLocation(e)}
                            >
                              <SwitchHorizontalIcon className="h-4 w-4 text-pink-600" />
                            </div>
                          </div>

                          <div className="flex-grow-1 relative  mx-4 w-full min-w-0  border-t border-gray-200 md:hidden"></div>

                          <div className="  flex-grow-1 relative w-full  min-w-0 cursor-pointer bg-white px-4  py-3 transition-colors hover:bg-gray-100 ">
                            {showError && (
                              <div className=" fadeInDown  tooltip absolute  bottom-full z-10 inline-block rounded-lg bg-gray-900 py-2 px-3 text-sm font-medium text-white  shadow-sm transition-opacity duration-300 dark:bg-gray-700">
                                Điểm đi và điểm đến phải khác nhau
                              </div>
                            )}

                            <div className=" text-tiny mb-0.5 flex items-center space-x-1 text-gray-400">
                              <svg
                                className=" h-4 w-4"
                                fill="currentColor"
                                focusable="false"
                                viewBox="0 0 24 24"
                                aria-hidden="true"
                                data-testid="FlightTakeoffIcon"
                              >
                                <path d="M2.5 19h19v2h-19v-2zm16.84-3.15c.8.21 1.62-.26 1.84-1.06.21-.8-.26-1.62-1.06-1.84l-5.31-1.42-2.76-9.02L10.12 2v8.28L5.15 8.95l-.93-2.32-1.45-.39v5.17l16.57 4.44z" />
                              </svg>
                              <span>Nơi đến</span>
                            </div>
                            <div className=" font-bold text-gray-900">
                              <MenuAirport
                                listLocation={listLocation}
                                formState={formState}
                                onChangeItem={handleChange}
                                type={2}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="flex-grow-1 relative col-span-2  mx-4  hidden  min-w-0 border-t border-gray-200 md:block lg:hidden"></div>

                        <div className=" flex flex-wrap items-center md:flex-nowrap">
                          <div className=" hidden h-8 w-0 border-l border-gray-200 md:block"></div>

                          <div className="flex-grow-1 relative  mx-4 w-full min-w-0  border-t border-gray-200 md:hidden"></div>

                          <div className="relative w-1/2 flex-grow cursor-pointer bg-white px-4 py-3 transition-colors hover:bg-gray-100">
                            <div className=" text-tiny mb-0.5 flex items-center space-x-1 text-gray-400">
                              <svg
                                className=" h-4 w-4"
                                fill="currentColor"
                                focusable="false"
                                viewBox="0 0 24 24"
                                aria-hidden="true"
                                data-testid="FlightTakeoffIcon"
                              >
                                <path d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z" />
                              </svg>
                              <span>Ngày đi</span>
                            </div>
                            <div className=" font-bold text-gray-900">
                              <DatePicker
                                id="inputNgaydi"
                                customInput={<ExampleCustomInput />}
                                dateFormat="dd-MM-yyyy"
                                selected={formState.values.fromDate}
                                onChange={(date) => handleChangeFromDate(date)}
                                locale="vi"
                                selectsStart
                                startDate={formState.values.fromDate}
                                endDate={formState.values.toDate}
                                minDate={minDate}
                                popperPlacement="bottom-start"
                                popperModifiers={[
                                  {
                                    name: "flip",
                                    options: {
                                      fallbackPlacements: ["bottom"],
                                    },
                                  },
                                ]}
                              />
                            </div>
                          </div>

                          {isReturn && (
                            <div className="relative w-1/2 flex-grow cursor-pointer bg-white  px-4 py-3 transition-colors hover:bg-gray-100">
                              <div className=" text-tiny mb-0.5 flex items-center space-x-1 text-gray-400">
                                <span>Ngày về</span>
                              </div>
                              <div className="  font-bold text-gray-900">
                                <DatePicker
                                  id="inputNgayve"
                                  customInput={<ExampleCustomInput />}
                                  dateFormat="dd-MM-yyyy"
                                  selected={formState.values.toDate}
                                  onChange={(date) =>
                                    handleChange("toDate", date)
                                  }
                                  locale="vi"
                                  selectsEnd
                                  startDate={formState.values.fromDate}
                                  endDate={formState.values.toDate}
                                  minDate={formState.values.fromDate}
                                  popperPlacement="bottom-end"
                                  popperModifiers={[
                                    {
                                      name: "flip",
                                      options: {
                                        fallbackPlacements: ["bottom"],
                                      },
                                    },
                                  ]}
                                />
                              </div>
                            </div>
                          )}
                        </div>

                        <div className=" flex flex-wrap items-center md:flex-nowrap">
                          <div className=" hidden h-8 w-0 border-l border-gray-200 md:block"></div>

                          <div className="flex-grow-1 relative  mx-4 w-full min-w-0  border-t border-gray-200 md:hidden"></div>

                          <Menu>
                            <MenuButton className="w-full text-left">
                              <div className="flex-1 cursor-pointer bg-white px-4 py-3 transition-colors hover:bg-gray-100">
                                <div className=" text-tiny mb-0.5 flex items-center space-x-1 text-gray-400">
                                  <svg
                                    className=" h-4 w-4"
                                    fill="currentColor"
                                    focusable="false"
                                    viewBox="0 0 24 24"
                                    aria-hidden="true"
                                    data-testid="FlightTakeoffIcon"
                                  >
                                    <path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z" />
                                  </svg>
                                  <span>Hành khách</span>
                                </div>
                                <div className="  truncate font-bold text-gray-900">
                                  {convertHanhKhach(formState.values.customer)}
                                </div>
                              </div>
                            </MenuButton>

                            <MenuPopover className="dropdown-filter-mobile fadeInDown   shadow-level3 absolute z-20 mt-1 max-w-[320px] overflow-auto rounded-lg bg-white p-2 text-sm focus:outline-none ">
                              <div className="arbitrary-element">
                                <MenuItems className="p-3">
                                  <MenuCustomer
                                    formState={formState}
                                    onChangeItem={handleChange}
                                  />
                                </MenuItems>
                              </div>
                            </MenuPopover>
                          </Menu>
                        </div>

                        <div className="flex items-center justify-end px-3 pb-4 pt-2 md:col-span-2 md:pt-0 md:pb-0 lg:col-span-1">
                          <button
                            className=" flex w-full items-center justify-center whitespace-nowrap rounded  bg-pink-600 py-3  pr-6 pl-4 font-bold text-white transition-colors hover:bg-pink-500 focus:bg-pink-500"
                            type="submit"
                          >
                            <SearchIcon className="mr-2 h-5 w-5" />{" "}
                            <span>Tìm kiếm</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>

                  <div className="mt-8 grid gap-3 md:grid-cols-3 md:gap-6 lg:px-14">
                    <div className="flex flex-nowrap">
                      <div className=" mr-3 flex-shrink-0">
                        <img
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124101438.svg"
                          width={42}
                          loading="lazy"
                        />
                      </div>
                      <div className=" flex-grow-1">
                        <h5 className=" text-sm font-semibold text-gray-700">
                          Ngập tràn ưu đãi
                        </h5>
                        <div className="text-tiny  mt-1 text-gray-500">
                          Đặt vé, đặt phòng trên Ví MoMo với giá cạnh tranh
                          nhất.
                        </div>
                      </div>
                    </div>

                    <div className="flex flex-nowrap ">
                      <div className=" mr-3 flex-shrink-0">
                        <img
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124101415.svg"
                          width={36}
                          loading="lazy"
                        />
                      </div>
                      <div className=" flex-grow-1">
                        <h5 className=" text-sm font-semibold text-gray-700">
                          Thanh toán siêu bảo mật
                        </h5>
                        <div className="text-tiny  mt-1 text-gray-500">
                          Thanh toán siêu bảo mật Ví MoMo đáp ứng các tiêu chuẩn
                          thanh toán với chứng chỉ bảo mật quốc tế PCI DSS.
                        </div>
                      </div>
                    </div>

                    <div className="flex flex-nowrap ">
                      <div className=" mr-3 flex-shrink-0">
                        <img
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124101240.svg"
                          width={38}
                          loading="lazy"
                        />
                      </div>
                      <div className=" flex-grow-1">
                        <h5 className=" text-sm font-semibold text-gray-700">
                          Hỗ trợ khách hàng 24/7
                        </h5>
                        <div className="text-tiny  mt-1 text-gray-500">
                          Đội ngũ nhân viên hỗ trợ khách hàng luôn sẵn sàng giúp
                          đỡ bạn.
                        </div>
                      </div>
                    </div>
                  </div>
                </TabPanel>
                <TabPanel>
                  <div className="px-2 py-2 md:px-0 md:py-2">
                    <p>
                      Giá vé máy bay rẻ nhất, tự động cập nhật từ ứng dụng MoMo.
                    </p>

                    <div className="py-3">
                      <MenuCheapFlight />
                    </div>

                    <div className=" text-sm italic text-gray-500">
                      * Giá vé có thể thay đổi theo thời điểm đặt mua. Vui lòng
                      kiểm tra tại chức năng Tìm kiếm vé hoặc App MoMo.
                    </div>
                  </div>
                </TabPanel>
                <TabPanel>
                  <div className="px-2 py-2 md:px-0 md:py-2">
                    <p>Điểm đến phổ biến</p>
                    <div className="py-3">
                      <MenuLocationFeature />
                    </div>
                  </div>
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Container>
        </div>
      </div>

      <style jsx>{`
        .svg-line {
          bottom: -1px;
        }
        .ota-bg {
        }

        @media screen and (max-width: 1280px) {
          .ota-bg {
            background-size: 140% auto;
          }
        }

        @media screen and (max-width: 640px) {
          .ota-bg {
            background-size: 140% auto;
          }
        }

        @media screen and (max-width: 414px) {
          .ota-bg {
            background-size: 220% auto;
          }
        }

        .tooltip:before {
          position: absolute;
          left: 20px;
          top: 100%;
          border: 9px solid transparent;
          border-top-color: var(--gray-800);
          content: "";
        }

        .ota-widget-search :global(.react-datepicker__input-container) {
          position: initial;
        }
        .ota-widget-search :global([data-reach-tab]) {
          border-bottom: 2px solid transparent;
        }
        .ota-widget-search :global([data-reach-tab] img) {
          filter: grayscale(100%);
        }

        .ota-widget-search :global([data-reach-tab][data-selected]) {
          border-bottom: 2px solid var(--pinkmomo);
          color: var(--pinkmomo);
        }

        .ota-widget-search :global([data-reach-tab][data-selected] img) {
          filter: grayscale(0);
        }
      `}</style>
    </>
  );
};
export default BlockSearchNew;
