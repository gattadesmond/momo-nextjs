import Heading from "../Heading";
import PlaceBrand from "../section/PlaceBrand";
import SectionSpace from "../SectionSpace";

const BlockMerchant = ({ data = null, isMobile }) => {
  return (
    <SectionSpace type={3}>
      <div className="mb-5 text-center md:mb-8" id="section-brand">
        <Heading
          title={"Đối tác Vé Máy Bay"}
          subtitle={"Những đối tác hàng không toàn cầu sẽ chắp cánh đưa bạn đến mọi địa điểm trên thế giới."}
          color="pink"
        />
      </div>
      <PlaceBrand
        data={data.Data}
        isMobile={isMobile}
      />
    </SectionSpace>
  )
};

export default BlockMerchant;