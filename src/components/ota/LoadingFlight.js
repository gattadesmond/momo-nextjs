import ContentLoader from "react-content-loader";

const LoadingFlight = ({}) => {
  return (
    Array(5).fill(0).map((v, i) => (
      <ContentLoader
        key={i}
        className="bg-white rounded-xl border border-gray-200 p-5 mb-5 shadow"
      >
      </ContentLoader>
    ))
  )
};

export default LoadingFlight;