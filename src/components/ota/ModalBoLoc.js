
import Modal from "../modal/Modal";
import ModalBody from "../modal/ModalBody";
import ModalFooter from "../modal/ModalFooter";
import ModalHeader from "../modal/ModalHeader";
import BlockSortBy from "./BlockSortBy";
import { IconBoLoc } from "./IconOta";

const ModalBoLoc = ({
  isOpen,
  onDismiss = () => { },
  step = 1,
  otaParams,
  airlineCodes,
  rangeMoney,
  stepMoney,
  configMoney,
  setRangeMoney = () => {},
  selectAllAirline = () => {},
  onSelectedAirline = () => {},
  getImageOfAirline = () => {},
  getNameOfAirline = () => {},
  timeTakeOff,
  setTimeTakeOff = () => {},
  timeLanding,
  setTimeLanding = () => {},
  convertTime = () => {},
  resetSort = () => {},
  applySort = () => {}
}) => {


  return (
    <Modal
      isOpen={isOpen}
      onDismiss={onDismiss}
      isFull={false}
      isBig={false}
    >
      <ModalHeader>
        <div className="flex items-center space-x-2">
          <IconBoLoc />
          <span className="text-lg">Bộ lọc</span>
        </div>
      </ModalHeader>
      <ModalBody css="bg-gray-50 py-2 px-6">
        <BlockSortBy
          step={step}
          otaParams={otaParams}
          airlineCodes={airlineCodes}
          rangeMoney={rangeMoney}
          stepMoney={stepMoney}
          configMoney={configMoney}
          setRangeMoney={setRangeMoney}
          selectAllAirline={selectAllAirline}
          onSelectedAirline={onSelectedAirline}
          getImageOfAirline={getImageOfAirline}
          getNameOfAirline={getNameOfAirline}
          timeTakeOff={timeTakeOff}
          setTimeTakeOff={setTimeTakeOff}
          timeLanding={timeLanding}
          setTimeLanding={setTimeLanding}
          convertTime={convertTime}
        />
      </ModalBody>
      <ModalFooter>
        <div className="flex justify-around items-center space-x-3 w-full">
          <button
            type="button"
            className="btn-primary-outline w-1/2"
            disabled={otaParams.pageIndex === 0}
            onClick={() => {
              resetSort();
              onDismiss();
            }}
          >
            Xóa bộ lọc
          </button>
          <button
            type="button"
            className="btn-primary w-1/2"
            disabled={otaParams.pageIndex === 0}
            onClick={() => {
              applySort();
              onDismiss();
            }}
          >
            Áp dụng
          </button>
        </div>
      </ModalFooter>
    </Modal>
  );
};

export default ModalBoLoc;