import { useEffect, useState } from "react";

import otaApi from "@/lib/api/ota-api";
import useBrowserLayoutEffect from "@/lib/useBrowserLayoutEffect";

import Modal from "../modal/Modal";
import ModalBody from "../modal/ModalBody";
import ModalHeader from "../modal/ModalHeader";

const IconCheck = () => (<img className="w-6 h-6" src="https://static.mservice.io/img/momo-upload-api-220117010649-637779784098284307.png" />)
const IconClose = () => (<img className="w-6 h-6" src="https://static.mservice.io/img/momo-upload-api-220117010818-637779784988174325.png" />)
const IconMoney = () => (<img className="w-6 h-6" src="https://static.mservice.io/img/momo-upload-api-220117010844-637779785246307886.png" />)

const ModalBFM = ({ isShow, onDismiss, data = null }) => {

  const [jsData, setJsData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useBrowserLayoutEffect(() => {
    if (data?.JsDataStr) {
      getBFM(data?.JsDataStr);
    }
  }, [data])

  const getBFM = (JsDataStr) => {
    setIsLoading(true);
    otaApi.csr.getBFM(JsDataStr)
      .then(res => {
        setIsLoading(false);
        if (!res.Data || !res.Success) {
          console.error(res);
          return;
        }
        setJsData(res.Data);
      })
  }

  return (
    <Modal
      isOpen={isShow}
      onDismiss={onDismiss}
      isFull={false}
      isBig={false}
      cssSize="md:max-w-xl"
    >
      <ModalHeader>
        <span className="text-lg">Điều kiện vé</span>
      </ModalHeader>
      <ModalBody css="p-0">
        <div className="flex justify-center space-x-5 py-2.5">
          <div className="flex items-center space-x-2">
            <IconCheck />
            <span className="font-extrabold">Được phép</span>
          </div>
          <div className="flex items-center space-x-2">
            <IconClose />
            <span className="font-extrabold">Không được phép</span>
          </div>
          <div className="flex items-center space-x-2">
            <IconMoney />
            <span className="font-extrabold">Thu phí</span>
          </div>
        </div>
        <div className="border-t border-gray-200 px-5 py-5">
          {jsData?.Content &&
            <div className="flex justify-between text-sm">
              <div className="font-semibold text-gray-400 text-opacity-80">Hạng vé</div>
              <div className="max-w-[45%]">
                <div className="flex items-center justify-end mb-2">
                  <span className="mr-2 text-right font-semibold">{jsData.Content.Title}</span>
                </div>
              </div>
            </div>
          }
          {jsData?.Content &&
            jsData.Content.Items.map((item, index) => (
              <div key={'item' + index} className="flex justify-between text-sm">
                <div className="font-semibold text-gray-400 text-opacity-80">{item.Key}</div>
                <div className="max-w-[45%]">
                  {item.Values && item.Values
                    .sort((a, b) => a.Order - b.Order)
                    .map((value, vIndex) => (
                      <div key={'value' + vIndex} className="flex items-center justify-end mb-2">
                        <span className="mr-2 text-right font-semibold">{value.Content}</span>
                        {value.Icon == "yes" && <IconCheck />}
                        {value.Icon == "no" && <IconClose />}
                        {value.Icon == "price" && <IconMoney />}
                      </div>
                    ))}
                </div>
              </div>
            ))
          }
        </div>
      </ModalBody>
    </Modal>
  );
};

export default ModalBFM;