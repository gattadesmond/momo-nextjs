import { forwardRef, useCallback, useEffect, useRef, useState } from "react";
import { useRouter } from 'next/router';
import Image from "next/image";
// import dynamic from "next/dynamic";

import add from 'date-fns/add';
import isBefore from 'date-fns/isBefore';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// import { registerLocale } from "react-datepicker";
import vi from 'date-fns/locale/vi';

import Container from "@/components/Container";
import {
  IconDateTime, IconFlightFrom, IconFlightTo,
  IconHanhKhach, IconNoiDen, IconNoiDi,
  IconSearch, IconTransfer
} from "./IconOta";

import AirportModal from "./AirportModal";
import CustomerModal from "./CustomerModal";


const BlockSearch = ({ className, data = null, dataListLocation = [], urlQuery = null, airlines = null, isMobile, ...rest }) => {
  const router = useRouter();
  registerLocale('vi', vi);
  
  const backgroundImg = {
    desktop: data?.Data?.Items[0].Avatar || 'https://static.mservice.io/blogscontents/momo-upload-api-210811175315-637643011953054096.png',
    mobile: data?.Data?.Items[0].AvatarMobile || 'https://static.mservice.io/blogscontents/momo-upload-api-210811175328-637643012080882941.jpg',
  };

  const [isReturn, setIsReturn] = useState(false);

  const minDate = add(new Date(), { days: 1 });

  const defaultFormState = {
    isValid: false,
    values: {
      fromAirport: '',
      toAirport: '',
      customer: {
        adult: 1,
        child: 0,
        infat: 0
      },
      fromDate: add(new Date(), { days: 1 }),
      toDate: null,
    },
    touched: {},
    errors: {},
    helperText: {}
  };

  const [formState, setFormState] = useState({ ...defaultFormState });

  useEffect(() => {
    let fromAirport = dataListLocation[0];
    let toAirport = dataListLocation[1];

    if (rest.codeTo) {
      const findItem = dataListLocation.find(x => x.JsId === rest.codeTo);
      if (findItem) {
        toAirport = findItem;
      }
      if (!rest.codeFrom && findItem.JsId === fromAirport.JsId) {
        fromAirport = dataListLocation.filter(x => x.JsId !== findItem.JsId)[0];
      }
    }
    if (rest.codeFrom) {
      const findItem = dataListLocation.find(x => x.JsId === rest.codeFrom);
      if (findItem) {
        fromAirport = findItem;
      }
    }

    if (urlQuery) {
      const pax = (urlQuery.pax || '1.0.0').split('.');
      const code = urlQuery.route.split('.');
      const fintItem0 = dataListLocation.find(x => x.JsId === code[0]);
      if (fintItem0) {
        fromAirport = fintItem0;
      }
      const fintItem1 = dataListLocation.find(x => x.JsId === code[1]);
      if (fintItem1) {
        toAirport = fintItem1;
      }
      
      setIsReturn(urlQuery.flightType === 'roundtrip');
      setFormState((_state) => ({
        ..._state,
        values: {
          ..._state.values,
          fromAirport: fromAirport,
          toAirport: toAirport,
          customer: {
            adult: parseInt(pax[0] || 1),
            child: parseInt(pax[1] || 0),
            infat: parseInt(pax[2] || 0)
          },
          fromDate: parse(urlQuery.fromDate, 'dd-MM-yyyy', new Date()),
          toDate: urlQuery.todate ? parse(urlQuery.toDate, 'dd-MM-yyyy', new Date()) : null,
        }
      }));
    } else {
      setFormState((_state) => ({
        ..._state,
        values: {
          ..._state.values,
          fromAirport: fromAirport,
          toAirport: toAirport
        }
      }));
    }
  }, []);

  const handleChange = useCallback((name, value) => {
    setFormState((_state) => ({
      ..._state,
      values: {
        ..._state.values,
        [name]: value
      },
      touched: {
        ..._state.touched,
        [name]: true
      }
    }));
  }, [setFormState]);

  const convertHanhKhach = (customer) => {
    let response = `${customer?.adult || 1} Người lớn`;
    if (customer?.child && customer.child > 0) {
      response += `, ${customer?.child} trẻ em`;
    }
    if (customer?.infat && customer.infat > 0) {
      response += `, ${customer?.infat} em bé`;
    }
    return response;
  };

  const fromAirportRef = useRef();
  const toAirportRef = useRef();
  const customerRef = useRef();

  const openAirPortPopup = (type = 1) => {
    if (type === 1) {
      fromAirportRef.current.Show();
    } else {
      toAirportRef.current.Show();
    }
  }

  const openCustomerPopup = () => {
    customerRef.current.Show();
  }

  const handleIsReturn = () => {
    setIsReturn((_isReturn) => !_isReturn);
  };

  useEffect(() => {
    if (isReturn) {
      handleChange('toDate', add(formState.values.fromDate, { days: 3 }));
    } else {
      handleChange('toDate', null);
    } 
  }, [isReturn])

  const changeLocation = () => {
    setFormState((_state) => ({
      ..._state,
      values: {
        ..._state.values,
        fromAirport: _state.values.toAirport,
        toAirport: _state.values.fromAirport
      }
    }));
  }

  const handleChangeFromDate = (date) => {
    handleChange("fromDate", date);
    const _isBefore = isBefore(
      date,
      formState.values.toDate
    );
    if (isReturn && !_isBefore) {
      handleChange("toDate", add(date, { days: 3 }));
    }
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    const fligtType = isReturn ? 'roundtrip' : 'oneway';
    const pax = `${formState.values.customer.adult}.${formState.values.customer.child}.${formState.values.customer.infat}`;
    const routeCode = `${formState.values.fromAirport.JsId}.${formState.values.toAirport.JsId}`;
    const fromDate = format(formState.values.fromDate, 'dd-MM-yyyy');
    
    let href = '';
    if (!isReturn) {
      href = `/ve-may-bay/tim-chuyen?flightType=${fligtType}&route=${routeCode}&fromDate=${fromDate}&pax=${pax}${airlines ? `&airlines=${airlines}` : ''}`;
    } else {
      const toDate = format(formState.values.toDate, 'dd-MM-yyyy');
      href = `/ve-may-bay/tim-chuyen?flightType=${fligtType}&route=${routeCode}&fromDate=${fromDate}&toDate=${toDate}&pax=${pax}${airlines ? `&airlines=${airlines}` : ''}`;
    }
    router.push(href);
  }

  const inputClass = `input pl-8 py-2 pr-2 w-full rounded-md bg-blue-100 transition focus:bg-white focus:ring-0 border border-transparent focus:border-blue-500`;

  const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
    <input
      className={inputClass}
      readOnly={true}
      onClick={onClick}
      ref={ref}
      value={value}
    />
  ));
  return (
    <>
      <div className="block-form-ota relative bg-gray-50 z-10" id={`block-${data.Id}`}>
        <div className="bg-ota absolute top-0 left-0 right-0 bottom-4">
          <div className="block md:hidden relative w-full h-full">
            <Image
              alt="background"
              src={backgroundImg.mobile}
              layout="intrinsic"
              width={500}
              height={534}
              objectFit="cover"
              priority={true}
            />
          </div>
          <div className="hidden md:block relative w-full h-full">
            <Image
              alt="background"
              src={backgroundImg.desktop}
              layout="fill"
              objectFit="cover"
              unoptimized={true}
              loading="lazy"
            />
          </div>
        </div>
        <Container className="relative z-10">
          <form className="md:mx-14 lg:mx-20 bg-white rounded-lg shadow p-4" onSubmit={handleSubmit}>
            <div className="grid md:grid-cols-3 gap-x-6">
              <div className="block relative z-20 order-1 mb-4">
                <div className="w-10/12 md:w-full">
                  <label className="text-gray-700" htmlFor="inputNoiDi">Nơi đi</label>
                  <div className="group-input relative">
                    <IconNoiDi />
                    <input type="text" id="inputNoiDi"
                      className={inputClass}
                      readOnly
                      value={formState.values?.fromAirport?.DisplayName || ''}
                      onClick={() => openAirPortPopup(1)}
                    />
                    <AirportModal
                      ref={fromAirportRef}
                      classInput={inputClass}
                      dataLocation={dataListLocation}
                      formState={formState}
                      type={1}
                      onChangeItem={handleChange}
                      isMobile={isMobile}
                    />
                  </div>
                </div>
                <div className="block-doichuyenbay">
                  <div className="icon-noidi relative md:hidden">
                    <IconFlightFrom />
                  </div>
                  
                  <button type="button"
                    className="btn-transfer rounded-full h-8 w-8 bg-momo flex justify-center items-center z-10"
                    onClick={changeLocation}
                  >
                    <span className="hidden">Đổi chuyến bay</span>
                    <div className="md:rotate-90">
                      <IconTransfer />
                    </div>
                  </button>


                  <div className="icon-noiden relative md:hidden">
                    <IconFlightTo />
                  </div>
                </div>
              </div>
              <div className="block relative order-2 mb-4">
                <div className="w-10/12 md:w-full">
                  <label className="text-gray-700" htmlFor="inputNoiDen">Nơi đến</label>
                  <div className="group-input relative">
                    <IconNoiDen />
                    <input type="text" id="inputNoiDen"
                      className={inputClass}
                      readOnly
                      value={formState.values?.toAirport?.DisplayName || ''}
                      onClick={() => openAirPortPopup(2)}
                    />
                    <AirportModal
                      ref={toAirportRef}
                      classInput={inputClass}
                      dataLocation={dataListLocation}
                      formState={formState}
                      type={2}
                      onChangeItem={handleChange}
                      isMobile={isMobile}
                    />
                  </div>
                </div>
              </div>
              <div className="block order-6 md:order-3 mb-4">
                <label className="text-gray-700" htmlFor="inputHanhkhach">Hành khách</label>
                <div className="group-input relative">
                  <IconHanhKhach />
                  <input type="text" id="inputHanhkhach"
                    className={inputClass}
                    readOnly
                    value={convertHanhKhach(formState.values.customer)}
                    onClick={() => openCustomerPopup()}
                  />
                  <CustomerModal
                    ref={customerRef}
                    classInput={inputClass}
                    formState={formState}
                    onChangeItem={handleChange}
                    isMobile={isMobile}
                  />
                </div>
              </div>
              <div className="flex items-end relative order-4 mb-3 md:mb-0">
                <div className="w-8/12 md:w-full">
                  <label className="text-gray-700" htmlFor="inputNgaydi">Ngày đi</label>
                  <div className="group-input relative">
                    <IconDateTime />
                    <DatePicker
                      id="inputNgaydi"
                      className={inputClass}
                      customInput={<ExampleCustomInput />}
                      dateFormat="dd-MM-yyyy"
                      selected={formState.values.fromDate}
                      onChange={(date) => handleChangeFromDate(date)}
                      locale="vi"
                      selectsStart
                      startDate={formState.values.fromDate}
                      endDate={formState.values.toDate}
                      minDate={minDate}
                      popperPlacement="bottom"
                      popperModifiers={[
                        {
                          name: 'flip',
                          options: {
                            fallbackPlacements: ['bottom'],
                          },
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              <div className={`block relative order-5 ${isReturn ? "mb-3" : 'mb-0'} md:mb-0`}>
                <label className="khuhoi-ota inline-flex items-center text-gray-700 cursor-pointer">
                  <input type="checkbox"
                    className="h-5 w-5 md:h-4 md:w-4 rounded transition bg-white border border-blue-600 focus:border-transparent text-blue-600 checked:bg-blue-600"
                    onChange={handleIsReturn} checked={isReturn}
                  />
                  <span className="ml-2">Khứ hồi</span>
                </label>
                <div className={`block transition ${isReturn ? "visible" : 'invisible overflow-hidden h-0 md:h-auto'}`}>
                  <label className="text-gray-700 md:hidden" htmlFor="inputNgayve">Ngày về</label>
                  <div className="group-input relative">
                    <IconDateTime />
                    <DatePicker
                      id="inputNgayve"
                      className={inputClass}
                      customInput={<ExampleCustomInput />}
                      dateFormat="dd-MM-yyyy"
                      selected={formState.values.toDate}
                      onChange={(date) => handleChange("toDate", date)}
                      locale="vi"
                      selectsEnd
                      startDate={formState.values.fromDate}
                      endDate={formState.values.toDate}
                      minDate={formState.values.fromDate}
                      popperPlacement="bottom"
                      popperModifiers={[
                        {
                          name: 'flip',
                          options: {
                            fallbackPlacements: ['bottom'],
                          },
                        },
                      ]}
                    />
                  </div>
                </div>
              </div>
              <div className="flex items-end order-7">
                <button type="submit"
                  className="btn-primary w-full flex items-center justify-center"
                >
                  <IconSearch />
                  <span className="">Tìm chuyến bay</span>
                </button>
              </div>
            </div>
          </form>
        </Container>
        <style jsx>{`
        .btn-transfer {
          background-color: #e81f76;
        }
        .btn-primary {
          display: flex !important;
          font-size: 16px;
        }
        .block-form-ota {
          position: relative;
        }
        .block-form-ota .bg-ota {
          z-index: 1;
        }
        .block-form-ota:after {
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
          pointer-events: none;
          content: "";
          width: 100%;
          height: 80%;
          top: 0;
          left: 0;
          position: absolute;
          background-repeat: no-repeat;
          background-color: #fca4ca;
        }
        @media (min-width: 501px) {
          .block-form-ota {
            padding-top: 160px;
          }
        }
        @media (min-width: 1200px) {
          .block-form-ota {
            padding-top: 220px;
          }
          .block-form-ota::after {
            background-size: auto 100%;
          }
        }
        @media (min-width: 1400px) {
          .block-form-ota {
            padding-top: 250px;
          }
          .block-form-ota::after {
            background-size: 100% auto;
          }
        }
        @media (max-width: 500px) {
          .block-form-ota {
            padding-top: 70px;
          }
          .khuhoi-ota {
            position: absolute;
            top: -45px;
            right: 0;
          }
        }
        @media (min-width: 425px) and (max-width: 499px) {
          .block-form-ota {
            padding-top: 90px;
          }
        }
        .block-doichuyenbay {
          position: absolute;
          right: 0;
          top: 0;
          width: 34px;
          height: 140px;
          display: flex;
          flex-direction: column;
          justify-content: space-evenly;
          align-items: center;
          padding-top: 18px;
          z-index: 10;
        }
        @media (min-width: 768px) {
          .block-doichuyenbay {
            width: auto;
            height: auto;
            padding-top: 0;
            flex-direction: row;
            bottom: 4px;
            top: auto;
            right: -26px;
          }
        }
        .block-doichuyenbay .icon-noidi::after {
          content: "";
          width: 0;
          height: 20px;
          border-right: 1px dashed #4f4f4f;
          display: block;
          position: absolute;
          bottom: -20px;
          left: calc(50% - 1px);
        }
        .block-doichuyenbay .icon-noiden::before {
          content: "";
          width: 0;
          height: 20px;
          border-right: 1px dashed #4f4f4f;
          display: block;
          position: absolute;
          top: -20px;
          left: calc(50% - 1px);
        }
      `}</style>
      </div>
    </>
  );
}
export default BlockSearch;