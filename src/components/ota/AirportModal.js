import { forwardRef, useImperativeHandle, useRef, useState } from "react";

import useOutsideClick from "@/lib/useOutsideClick";

import { IconNoiDen, IconNoiDi, IconSearch } from '@/components/ota/IconOta';

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

const AirportModal = forwardRef((props, ref) => {
  const { classInput, dataLocation = [], formState = null, type = 1, onChangeItem, isMobile } = props;

  useImperativeHandle(ref, () => ({
    Show() {
      if (!isMobile) {
        if (type === 1) {
          setInputStr(formState.values?.fromAirport?.DisplayName);
        } else {
          setInputStr(formState.values?.toAirport?.DisplayName);
        }
      }
      setModalShow(true);
      setTimeout(() => {
        inputFrom.current.focus();
      });
    },
    Hide() {
      setModalShow(false);
    }
  }));

  const containRef = useRef();
  const inputFrom = useRef(null);
  const [inputStr, setInputStr] = useState('');
  const [modalShow, setModalShow] = useState(false);

  const handleChangeInputStr = (event) => {
    setInputStr(event.target.value);
  };

  useOutsideClick(containRef, () => {
    if (modalShow === true) {
      setModalShow(false);
    }
  });

  const selectAirport = (item, type = 1) => {
    setModalShow(false);
    if (isItemSelected(item)) return;

    if (type === 1) {
      onChangeItem('fromAirport', item);
    } else {
      onChangeItem('toAirport', item);
    }
  }

  const sortAirport = (item, type = 1) => {
    if (type == 1 && item.Id === formState?.values?.toAirport.Id) return false;
    if (type == 2 && item.Id === formState?.values?.fromAirport.Id) return false;

    if (!inputStr) return true;

    if (inputStr === formState?.values?.fromAirport?.DisplayName) return true;
    if (inputStr === formState?.values?.toAirport?.DisplayName) return true;

    if (item.DisplayName.toLowerCase().includes(inputStr.toLowerCase())) {
      return true;
    }
    if (item.JsId.toLowerCase().includes(inputStr.toLowerCase())) {
      return true;
    }
    if (item.JsCi.toLowerCase().includes(inputStr.toLowerCase())) {
      return true;
    }
    return false;
  }

  const isItemSelected = (item) => {
    if (item.Id === formState.values.fromAirport.Id) {
      return true;
    }
    if (item.Id === formState.values.toAirport.Id) {
      return true;
    }
    return false;
  }
  return (
    <>
      {isMobile &&
        <Modal isOpen={modalShow} onDismiss={() => setModalShow(false)} isFull={true} isBig={true}>
          <ModalHeader> Chọn điểm {type === 1 ? "đi" : "đến"} </ModalHeader>

          <ModalBody css="p-0 bg-white rounded block-form-ota">
            <div className="py-2 px-4">
              <div className="group-input relative">
                <IconSearch color="#737373" />
                <input type="text" id="airport-values"
                  ref={inputFrom}
                  className={classInput}
                  value={inputStr}
                  onChange={handleChangeInputStr}
                  placeholder="Tìm địa điểm"
                />
              </div>
            </div>
            <div className={`relative bg-white`}>
              {dataLocation.length > 0 && dataLocation
                .filter(item => sortAirport(item, type))
                .map((item, index) => (
                  <div key={item.Id}
                    className={`ota-box-item px-4 py-2 border-b border-gray-200 cursor-pointer hover:bg-gray-100 
                    ${isItemSelected(item) ? 'bg-gray-100' : ''}`}
                    onClick={() => selectAirport(item, type)}
                  >
                    <div className="tt text-gray-700">{item.JsCv}, {item.JsCov}</div>
                    <div className="tb text-tiny text-gray-400">{item.JsId} - {item.JsNv}</div>
                  </div>
                ))}
            </div>
          </ModalBody>

          <ModalFooter className="flex">
          </ModalFooter>
        </Modal>
      }
      {!isMobile &&
        <div className={`popup-ota absolute top-0 z-10 bg-white rounded-md ${modalShow ? 'block' : 'hidden'}`} ref={containRef}>
          <div className="group-input relative">
            {type === 1 &&
              <IconNoiDi />
            }
            {type === 2 &&
              <IconNoiDen />
            }
            <input type="text" id="airport-values"
              ref={inputFrom}
              className={classInput}
              value={inputStr}
              onChange={handleChangeInputStr}
            />
          </div>
          <div className={`relative overflow-y-auto max-h-96 bg-white`}>
            {dataLocation.length > 0 && dataLocation
              .filter(item => sortAirport(item, type))
              .map((item, index) => (
                <div key={item.Id}
                  className={`ota-box-item px-4 py-2 border-b border-gray-200 cursor-pointer hover:bg-gray-100 ${isItemSelected(item) ? 'bg-gray-100' : ''}`}
                  onClick={() => selectAirport(item, type)}
                >
                  <div className="tt font-bold text-gray-700">{item.JsCv}, {item.JsCov}</div>
                  <div className="tb text-tiny text-gray-400">{item.JsId} - {item.JsNv}</div>
                </div>
              ))}
          </div>
        </div>
      }
    </>
  )
});

export default AirportModal;