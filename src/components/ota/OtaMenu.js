import BreadcrumbContainer, { BreadcrumbItem } from "../BreadcrumbContainer";

const OtaMenu = ({ data, pageId, merchantLogo = null, merchantActive = null, pageName = null, isMobile, isStickyMobile = true }) => {
  const menuData = data.Data || null;
  if (!menuData) return;

  let isMerchant = !!merchantActive;
  let navItems = [...menuData.ListChilds];
  if (merchantActive || pageName) {
    navItems = menuData.ListChilds.filter(x => x.Id === pageId);
  }

  return (
    <BreadcrumbContainer
      className={`${isStickyMobile ? "sticky" : "block"} md:sticky z-30 -top-px`}
      id={`${pageId}-${merchantActive || ''}`}
    >
      <BreadcrumbItem url="https://momo.vn/" />

      {menuData.ShowBreadcrumb &&
        <BreadcrumbItem
          icon={menuData.Icon}
          title={menuData.Title}
          url={menuData.Url}
          isShowIcon={false}
          isActive={menuData.Id === pageId}
        />
      }

      {navItems
        .filter(x => x.ShowBreadcrumb)
        .sort((a, b) => a.Id - b.Id)
        .sort((a, b) => a.Id === pageId ? -1 : 1)
        .map((item, index) => (
          <BreadcrumbItem
            key={item.Id}
            icon={item.Icon}
            title={item.Title}
            url={item.Url}
            isActive={item.Id === pageId && !isMerchant && !pageName}
            isCatItem={!isMerchant && !pageName}
          />
        ))}
      {isMerchant &&
        <BreadcrumbItem
          icon={merchantLogo}
          title={merchantActive}
          isActive={true}
        />
      }
      {pageName &&
        <BreadcrumbItem
          title={pageName}
          isActive={true}
        />
      }
    </BreadcrumbContainer>
  );
};

export default OtaMenu;
