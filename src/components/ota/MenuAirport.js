import { useEffect, useState, useMemo, useRef } from "react";
import { useDebounce, useThrottledCallback } from "use-debounce";
import Downshift from "downshift";

import { matchSorter } from "match-sorter";
const MenuAirport = ({
  listLocation,
  formState = null,
  onChangeItem,
  type = null,
}) => {
  const [term, setTerm] = useState("");

  const [selection, setSelection] = useState("");

  const results = useCityMatch(term);

  const inputS1 = useRef(null);

  const handleSelect = (item) => {
    inputS1.current.blur();
  };

  const handleStateChange = (changes) => {
    if (changes.hasOwnProperty("selectedItem")) {
      setSelection(changes.selectedItem);
      setTerm("");

      if (type === 1) {
        onChangeItem("fromAirport", changes.selectedItem);
      } else {
        onChangeItem("toAirport", changes.selectedItem);
      }
    } else if (
      changes.hasOwnProperty("inputValue") &&
      changes.type == "__autocomplete_change_input__"
    ) {
      setTerm(changes.inputValue);
    }

    if (changes.hasOwnProperty("isOpen")) {
      if (changes.isOpen == false) {
        inputS1.current.blur();
        setTerm("");
      }
    }
  };

  function useCityMatch(term) {
    const [debounceTerm] = useDebounce(term, 200);

    return useMemo(
      () =>
        term.trim() === ""
          ? listLocation.slice(0, 9)
          : matchSorter(listLocation, term, {
              keys: [
                (item) =>
                  `${item.DisplayName}, ${item.JsCv}, ${item.JsNv}, ${item.JsCi}, ${item.JsId}, ${item.JsNa}`,
              ],
            }).slice(0, 9),
      [debounceTerm]
    );
  }

  const handleFocus = (event) => event.target.select();

  const initialSelect = () => {
    if (type === 1) {
      return listLocation.find(
        (item) => item.Id == formState.values.fromAirport.Id
      );
    }
    return listLocation.find(
      (item) => item.Id == formState.values.toAirport.Id
    );
  };

  return (
    <Downshift
      defaultIsOpen={false}
      onChange={(selection) => handleSelect(selection)}
      onStateChange={handleStateChange}
      itemToString={(item) => (item ? item.DisplayName : "")}
      selectedItem={initialSelect() ?? null}
      // selectedItem={listLocation[1]}
    >
      {({
        getInputProps,
        getItemProps,
        getMenuProps,
        getLabelProps,
        inputValue,
        highlightedIndex,
        openMenu,
        isOpen,
      }) => (
        <div>
          <label
            {...getLabelProps()}
            className=" z-1 absolute inset-0 cursor-pointer"

          >
            &nbsp;
          </label>
          <input
            {...getInputProps({
              onFocus: (e) => {
                if (!isOpen) {
                  openMenu();
                  handleFocus(e);
                }
              },
            })}
            ref={inputS1}
            autoCapitalize="off"
            autoCorrect="off"
            role="combobox"
            autoComplete="off"
            aria-expanded="true"
            className="z-2 relative bg-transparent font-bold text-gray-900"
          />

          {isOpen && results.length > 0 && (
            <ul
              {...getMenuProps()}
              className="  shadow-level3 fadeInDown absolute top-full left-4 right-4 z-20 mt-1  sm:w-96  sm:max-w-[300px] overflow-auto rounded-lg   bg-white p-2 text-sm focus:outline-none"
            >
              {isOpen
                ? results.map((item, index) => (
                    <li
                      className="rounded-md py-2 px-2"
                      {...getItemProps({
                        key: `${item.DisplayName}${index}`,
                        item,
                        index,
                        style: {
                          backgroundColor:
                            highlightedIndex === index ? "#f3f4f6" : "white",
                          // backgroundColor:
                          //   selectedItem === item ? "#f3f4f6" : "white",
                        },
                      })}
                    >
                      <span className="block truncate font-bold text-gray-800">
                        {item.DisplayName}
                      </span>
                      <span className="truncate text-sm font-normal text-gray-500">
                        {item.JsNv}
                      </span>
                    </li>
                  ))
                : null}
            </ul>
          )}
        </div>
      )}
    </Downshift>
  );
};
export default MenuAirport;
