
import { memo } from "react";
import Modal from "../modal/Modal";
import ModalBody from "../modal/ModalBody";
import ModalHeader from "../modal/ModalHeader";
import { IconSapXep } from "./IconOta";

const ModalSapXep = ({
  isOpen,
  onDismiss = () => { },
  sortValue,
  getTitle = () => { },
  onChange = () => { }
}) => {

  const handleChange = (value) => {
    if (value === sortValue) return;
    onChange(value);
    onDismiss();
  }

  return (
    <Modal
      isOpen={isOpen}
      onDismiss={onDismiss}
      isFull={false}
      isBig={false}
    >
      <ModalHeader>
        <div className="flex items-center space-x-2">
          <IconSapXep />
          <span className="text-lg">Sắp xếp</span>
        </div>
      </ModalHeader>
      <ModalBody>
        <div className="flex flex-col">
          {([1, 2, 3, 4, 5]).map((value) => (
            <div
              key={value}
              className={`text-md py-2 flex items-center justify-start
                font-bold text-opacity-90 text-gray-500 bg-white group
                ${sortValue === value ? '' : 'cursor-pointer '}
              `}
              onClick={() => handleChange(value)}
            >
              <div 
                className="w-4 h-4 rounded-full border border-green-500 mr-3
                  flex justify-center items-center
                "
              >
                <div 
                  className={`w-2.5 h-2.5 rounded-full
                    ${sortValue === value ? 'bg-green-500' : 'group-hover:bg-green-500'}
                  `}
                />
              </div>
              {getTitle(value)}
            </div>
          ))}
        </div>
      </ModalBody>
    </Modal>
  );
};

export default memo(ModalSapXep);