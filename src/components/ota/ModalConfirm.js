import Modal from "../modal/Modal";
import ModalBody from "../modal/ModalBody";
import ModalFooter from "../modal/ModalFooter";
import ModalHeader from "../modal/ModalHeader";

const ConfirmItem = ({
  data,
  time,
  title,
  airlineLogo,
  airlineName
}) => {

  return (
    <div className="mt-2 md:mt-3">
      <div className="text-momo font-bold text-sm mb-2">
        {time}
      </div>
      <div
        className="rounded-lg bg-white p-3 pb-6 shadow"
      >
        <div className="text-sm font-bold text-blue-500 text-opacity-90">
          {title}
        </div>
        <div className="flex my-3">
          <div className="rounded-lg border border-gray-200 w-12 h-12 flex items-center justify-center">
            <img
              alt={data?.AirlineCode}
              src={airlineLogo}
              width={46}
              height={46}
              loading="lazy"
            />
          </div>
          <div className="pl-5 text-sm font-bold">
            <h5 className="text-gray-700">{airlineName}</h5>
            <span className="text-gray-400">{data.TicketStr}</span>
          </div>
        </div>
        <div className="rounded-md bg-[#f7faff] flex items-center px-4 py-2">
          <div className="flex-initial text-center md:px-2">
            <div className="font-bold">{data.DepartureTimeStr}</div>
            <div className="font-bold text-sm text-gray-500 text-opacity-80">{data.DepartureCode}</div>
          </div>
          <div className="flex-auto text-center text-gray-600 px-4 md:px-10">
            <div className="relative">
              <div className="font-bold text-xs text-gray-500 text-opacity-80 pb-1.5">
                {data.DurationStr}
              </div>
              <div className="absolute left-0 top-1/2 -translate-y-1/2 w-full h-0 border-t border-dotted border-gray-400"></div>
              <div className="absolute left-0 top-1/2 -translate-y-1/2 w-full flex justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" className="-rotate-90" width="16" height="14" viewBox="0 0 16 19" fill="none">
                  <path d="M10 15.8333L10 12.0633L15.7481 8.59646C15.8247 8.55023 15.8883 8.48348 15.9325 8.40297C15.9767 8.32245 16 8.23103 16 8.13795L16 5.97735C16 5.62671 15.6822 5.3737 15.3628 5.47002L10 7.08734L10 3.69437L11.8 2.26937C11.9259 2.16975 12 2.01306 12 1.84714L12 0.528022C12 0.184635 11.6944 -0.0673796 11.3788 0.0160756L8 1.05547L4.62125 0.0160753C4.30563 -0.0673799 4 0.184635 4 0.528022L4 1.84714C4 2.01339 4.07406 2.16975 4.2 2.26937L6 3.69437L6 7.08734L0.637501 5.47002C0.318125 5.3737 7.07723e-07 5.62671 6.92396e-07 5.97735L5.97953e-07 8.13795C5.89677e-07 8.32729 0.0962502 8.50212 0.251876 8.59613L6 12.0633L6 15.8333C6 16.9994 6.89531 19 8 19C9.10469 19 10 16.9994 10 15.8333Z" fill="#4F4F4F" />
                </svg>
              </div>
              <div className="font-bold text-xs text-gray-500 text-opacity-80 pt-1.5">
                {data.TotalFlightSegments > 2 ? `${data.TotalFlightSegments - 2} điểm dừng` : "Bay thẳng"}
              </div>
            </div>
          </div>
          <div className="flex-initial text-center md:px-2">
            <div className="font-bold">
              {data.ArrivalTimeStr}
              {data.TotalDays > 0 &&
                <small className="pl-1 font-normal text-gray-400">(+{data.TotalDays}d)</small>
              }
            </div>
            <div className="font-bold text-sm text-gray-500 text-opacity-80">{data.ArrivalCode}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

const ModalConfirm = ({
  isOpen = false,
  onDismiss = () => { },
  locationOut,
  locationIn,
  flightOut = null,
  flightIn = null,
  departureTime = null,
  returnTime = null,
  imageOfAirline = () => { },
  nameOfAirline = () => { },
  onSubmit = () => { },
  isLoading = false,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      onDismiss={() => onDismiss(false)}
      isFull={false}
      isBig={false}
      isShowHeaderClose={false}
      cssSize="md:max-w-lg"
    >
      <ModalHeader>
        <span>Thông tin đặt vé</span>
      </ModalHeader>
      <ModalBody
        css="bg-gray-100 px-4 md:px-6 pt-2 pb-6"
      >
        {flightOut &&
          <ConfirmItem
            data={flightOut}
            time={departureTime}
            title={`Chiều đi: ${locationOut} - ${locationIn}`}
            airlineLogo={imageOfAirline(flightOut.AirlineCode)}
            airlineName={nameOfAirline(flightOut.AirlineCode)}
          />
        }
        {flightIn &&
          <ConfirmItem
            data={flightIn}
            time={returnTime}
            title={`Chiều về: ${locationIn} - ${locationOut}`}
            airlineLogo={imageOfAirline(flightIn.AirlineCode)}
            airlineName={nameOfAirline(flightIn.AirlineCode)}
          />
        }
      </ModalBody>
      <ModalFooter
        className="space-x-3"
      >
        <button type="button"
          className="flex-initial btn-primary-outline"
          onClick={() => onDismiss(false)}
          disabled={isLoading}
        >
          Quay lại
        </button>
        <button type="button"
          className="flex-1 btn-primary"
          onClick={() => onSubmit()}
          disabled={isLoading}
        >
          Đặt vé
        </button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalConfirm;