import Image from "next/image";

import { decode } from "html-entities";
import parse from "html-react-parser";

import Heading from "../Heading";


const RouterInfo = ({ title, dataLocation }) => {
  return (
    <>
      <div className="mb-5 text-center md:mb-8" id="section-changbay">
        <Heading title={title} color="pink" />
      </div>
      <div className="grid gap-6 md:grid-cols-2 lg:px-24">
        <div className="flex flex-col">
          <div className="img-city relative overflow-hidden rounded-lg">
            <div className="img-city-overlay">
              <div>
                <div className="text-white">Điểm đi</div>
                <h2 className="text-white text-2xl md:text-3xl font-semibold md:font-normal">{dataLocation.Departure.JsCv}</h2>
              </div>
            </div>
            <div className="absolute inset-0">
              <Image
                src={dataLocation.Departure.Avatar}
                alt={dataLocation.Departure.JsCv || ""}
                layout="responsive"
                width={335}
                height={168}
                loading="lazy"
              />
            </div>
          </div>
          <div className="flex flex-col mt-6">
            {parse(decode(dataLocation.Departure.Content))}
          </div>
        </div>
        <div className="flex flex-col mt-6 md:mt-0">
          <div className="img-city relative overflow-hidden rounded-lg">
            <div className="img-city-overlay">
              <div>
                <div className="text-white">Điểm đến</div>
                <h2 className="text-white text-2xl md:text-3xl font-semibold md:font-normal">{dataLocation.Destination.JsCv}</h2>
              </div>
            </div>
            <div className="absolute inset-0">
              <Image
                src={dataLocation.Destination.Avatar}
                alt={dataLocation.Destination.JsCv || ""}
                layout="responsive"
                width={335}
                height={168}
                loading="lazy"
              />
            </div>
          </div>
          <div className="flex flex-col mt-6">
            {parse(decode(dataLocation.Destination.Content))}
          </div>
        </div>
      </div>
      <style jsx>{`
          .img-city::before {
            content: "";
            display: block;
            padding-bottom: 48%;
          }
          .img-city-overlay {
            position: absolute;
            padding: 10px 20px;
            bottom: 0;
            left: 0;
            color: white;
            background-image: linear-gradient(to top, rgba(0,0,0,0.6) 0%, rgba(0,0,0,0.4) 30%, rgba(0,0,0,0.01) 50%, rgba(0,0,0,0) 100%);
            height: 100%;
            width: 100%;
            text-align: left;
            display: flex;
            align-items: flex-end;
            z-index: 5;
          }
        `}</style>
    </>
  )
};

export default RouterInfo;