
import useOutsideClick from "@/lib/useOutsideClick";
import { memo, useRef, useState } from "react";

const SelectSortBy = ({ sortValue, getTitle = () => {}, onChange = () => {} }) => {

  const containRef = useRef();
  const [isOpen, setIsOpen] = useState(false);

  useOutsideClick(containRef, () => {
    if (isOpen === true) {
      setIsOpen(false);
    }
  });

  const handleChange = (value) => {
    if (value === sortValue) return;
    setIsOpen(false);
    onChange(value);
  }

  return (
    <div
      ref={containRef}
      className="relative pl-3"
    >
      <button
        type="button"
        className="selected shadow-md rounded-full bg-white text-sm text-gray-500 border border-gray-200 pl-4 pr-8 py-1 overflow-hidden max-w-[152px] max-h-7"
        onClick={() => setIsOpen(value => !value)}
      >
        {getTitle(sortValue)}
      </button>
      <div className={`relative w-full ${isOpen ? 'block' : 'hidden'}`}>
        <div
          className={`absolute left-1/2 -translate-x-1/2 translate-y-1 top-full
          bg-white py-3 z-10 rounded overflow-hidden min-w-[13rem]
            border border-gray-200 shadow
          `}
        >
          {([1,2,3,4,5]).map((value) => (
            <div 
              key={value}
              className={`text-sm hove px-4 py-2 font-bold text-opacity-90
                ${sortValue === value ? 'bg-blue-600 text-white' : 'cursor-pointer text-gray-500 bg-white hover:text-gray-700 hover:bg-gray-100'}
              `}
              onClick={() => handleChange(value)}
            >
              {getTitle(value)}
            </div>
          ))}
        </div>
      </div>
      <style jsx>{`
        .selected {
          text-overflow: ellipsis;
          white-space: nowrap;
        }
        .selected::after {
          position: absolute;
          top: 50%;
          right: 10px;
          transform: translate(0,-50%);
          display: inline-block;
          margin-left: 0.255em;
          vertical-align: 0.255em;
          content: "";
          border-top: 0.3em solid #f45197;
          border-right: 0.3em solid transparent;
          border-bottom: 0;
          border-left: 0.3em solid transparent;
      }
      `}</style>
    </div>
  )
};

export default memo(SelectSortBy);