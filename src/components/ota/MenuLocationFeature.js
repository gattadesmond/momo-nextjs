import { useEffect, useState } from "react";
import { axios } from "@/lib/index";
const MenuLocationFeature = ({}) => {
  const [list, setList] = useState(null);

  useEffect(() => {
    axios.get(`/ota/getPopularLocations`).then(function (response) {
      const res = response.Data ?? null;
      setList(res ?? null);
    });
  }, []);
  const getUrlDay = (item) => {
    return `https://momo.vn/ve-may-bay/diem-den/${item.UrlRewrite}.${item.JsId}`;
  };

  return (
    <div className="grid grid-cols-1 gap-3 md:grid-cols-2 lg:grid-cols-4">
      {list &&
        list.map((item, index) => (
          <div
            className=" relative overflow-hidden rounded-md  hover:shadow-md"
            key={index}
          >
            <a href={getUrlDay(item)} className="z-2 absolute inset-0 bg-transparent"></a>
            <div className="  relative aspect-[21/9] w-full">
              <img
                src={item.Avatar}
                loading="lazy"
                className="absolute inset-0 h-full w-full object-cover"
              />

              <div className="z-1 absolute bottom-0 h-full w-full px-3 py-2 text-white bg-gradient-to-b  from-transparent  via-transparent  to-black/40 flex items-end">
              
                <div className="text-lg font-bold  ">
                  {item.JsCv}
                </div>
              </div>
            </div>
          </div>
        ))}

      <style jsx>{``}</style>
    </div>
  );
};
export default MenuLocationFeature;
