import { useCallback, useEffect, useState } from "react";

const MenuCustomer = (props) => {
  const { formState = null, onChangeItem } = props;

  const [customer, setCustomer] = useState(
    formState.values.customer || {
      adult: 1,
      child: 0,
      infat: 0,
    }
  );

  useEffect(() => {
    onChangeItem("customer", customer);
  }, [customer]);

  useEffect(() => {
    setCustomer(formState.values.customer);
  }, [formState.values.customer]);

  const handleChange = useCallback(
    (name, value) => {
      if (value < 0) return;
      setCustomer((_state) => ({
        ..._state,
        [name]: parseInt(value),
      }));
    },
    [setCustomer]
  );

  const DecreaseAdult = () =>
    handleChange("adult", parseInt(customer.adult) - 1);
  const IncreaseAdult = () =>
    handleChange("adult", parseInt(customer.adult) + 1);

  const DecreaseChild = () =>
    handleChange("child", parseInt(customer.child) - 1);
  const IncreaseChild = () =>
    handleChange("child", parseInt(customer.child) + 1);

  const DecreaseInfat = () =>
    handleChange("infat", parseInt(customer.infat) - 1);
  const IncreaseInfat = () =>
    handleChange("infat", parseInt(customer.infat) + 1);

  return (
    <>
      <p className="mb-5 text-gray-500">
        Vui lòng chọn số lượng hành khách chính xác để xem được mức giá tốt
        nhất.
      </p>

      <div className="flex w-full items-center justify-between ">
        <div className="flex ">
          <span className="">Người lớn</span>
          <span className="ml-2 text-xs text-gray-500">&gt; 12 tuổi</span>
        </div>

        <div className=" ml-5 flex w-20 items-center justify-between">
          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-50 disabled:hover:border-pink-200"
            type="button"
            disabled={customer.adult == 1}
            onClick={DecreaseAdult}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M20 12H4"
              />
            </svg>
          </button>

          <span> {customer.adult}</span>

          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 hover:text-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-40 disabled:hover:border-pink-600"
            type="button"
            disabled={
              customer.adult == 9 || customer.adult + customer.child == 9
            }
            onClick={IncreaseAdult}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M12 4v16m8-8H4"
              />
            </svg>
          </button>
        </div>
      </div>

      <div className="mt-5 flex w-full items-center justify-between">
        <div className="  align-baseline ">
          <span className="">Trẻ em</span>
          <span className="ml-2 text-xs text-gray-500">2-11 tuổi</span>
        </div>

        <div className=" ml-5 flex w-20 items-center justify-between">
          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-50 disabled:hover:border-pink-200"
            type="button"
            disabled={customer.child == 0}
            onClick={DecreaseChild}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M20 12H4"
              />
            </svg>
          </button>

          <span>{customer.child}</span>

          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 hover:text-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-40 disabled:hover:border-pink-600"
            type="button"
            disabled={
              customer.child == 8 || customer.adult + customer.child == 9
            }
            onClick={IncreaseChild}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M12 4v16m8-8H4"
              />
            </svg>
          </button>
        </div>
      </div>

      <div className="mt-5 flex w-full items-center justify-between">
        <div className="  align-baseline ">
          <span className="">Em bé</span>
          <span className="ml-2 text-xs text-gray-500">&lt; 2 tuổi</span>
        </div>
        <div className=" ml-5 flex w-20 items-center justify-between">
          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-50 disabled:hover:border-pink-200"
            type="button"
            disabled={customer.infat == 0}
            onClick={DecreaseInfat}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M20 12H4"
              />
            </svg>
          </button>

          <span>{customer.infat}</span>

          <button
            className="flex h-5 w-5 items-center justify-center rounded-full  border-2 border-pink-600 bg-white  text-pink-600  hover:border-pink-500 hover:text-pink-500 focus:outline-none disabled:cursor-default  disabled:opacity-40 disabled:hover:border-pink-600"
            type="button"
            disabled={customer.infat == 4 || customer.adult == customer.infat}
            onClick={IncreaseInfat}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-3 w-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={3}
                d="M12 4v16m8-8H4"
              />
            </svg>
          </button>
        </div>
      </div>
    </>
  );
};
export default MenuCustomer;
