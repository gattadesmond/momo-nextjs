import ContentLoader from "react-content-loader";
import { Range, getTrackBackground } from 'react-range';

import { MoneyFormat } from "@/lib/int-format";

const IconCheckBox = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 23 23" fill="none">
    <rect x="0.75" y="0.75" width="21.5" height="21.5" rx="1.25" stroke="#27AE60" strokeWidth="1.5" />
  </svg>
)
const IconCheckBoxChecked = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 23 23" fill="none">
    <rect x="0.75" y="0.75" width="21.5" height="21.5" rx="1.25" stroke="#27AE60" strokeWidth="1.5" />
    <path d="M9.41538 16.8036L5.19037 12.4453C4.93654 12.1835 4.93654 11.759 5.19037 11.4971L6.10959 10.5489C6.36342 10.287 6.775 10.287 7.02883 10.5489L9.875 13.4848L15.9712 7.19638C16.225 6.93454 16.6366 6.93454 16.8904 7.19638L17.8096 8.14461C18.0635 8.40645 18.0635 8.83098 17.8096 9.09285L10.3346 16.8036C10.0808 17.0655 9.66921 17.0655 9.41538 16.8036Z" fill="#27AE60" />
  </svg>
)

const BlockSortBy = ({
  step,
  otaParams,
  airlineCodes,
  rangeMoney,
  stepMoney,
  configMoney,
  setRangeMoney = () => { },
  selectAllAirline = () => { },
  onSelectedAirline = () => { },
  getImageOfAirline = () => { },
  getNameOfAirline = () => { },
  timeTakeOff,
  setTimeTakeOff = () => { },
  timeLanding,
  setTimeLanding = () => { },
  convertTime = () => { },
  resetSort = () => { },
  applySort = () => { }
}) => {
  return (
    <>
      <div className="mb-3">
        <h4 className="font-extrabold text-sm mb-3 pt-1 text-gray-600">Khoảng giá</h4>
        <div className="bg-white rounded-xl px-2 py-3 block-shadow">
          {otaParams.pageIndex >= 1 &&
            <>
              <div
                className="flex justify-around text-gray-500 text-xs font-bold"
              >
                <div className="text-center">
                  Từ <span className="block text-momo md:inline-block">{MoneyFormat(rangeMoney[0])} ₫</span>
                </div>
                <div>-</div>
                <div className="text-center">
                  Đến <span className="block text-momo md:inline-block">{MoneyFormat(rangeMoney[1])} ₫</span>
                </div>
              </div>
              <div
                className="flex justify-center flex-wrap px-2 h-8"
              >
                {otaParams.pageIndex >= 1 &&
                  <Range
                    allowOverlap
                    values={rangeMoney}
                    step={stepMoney}
                    min={configMoney[0]}
                    max={configMoney[1]}
                    onChange={(values) => { setRangeMoney(values) }}
                    renderTrack={({ props, children }) => (
                      <div
                        onMouseDown={props.onMouseDown}
                        onTouchStart={props.onTouchStart}
                        style={{
                          ...props.style,
                          height: "36px",
                          display: "flex",
                          width: "100%"
                        }}
                      >
                        <div
                          ref={props.ref}
                          style={{
                            height: "3px",
                            width: "100%",
                            borderRadius: "4px",
                            background: getTrackBackground({
                              values: rangeMoney,
                              colors: ['#ccc', '#f45197', '#ccc'],
                              min: configMoney[0],
                              max: configMoney[1]
                            }),
                            alignSelf: "center"
                          }}
                        >
                          {children}
                        </div>
                      </div>
                    )}
                    renderThumb={({ props, isDragged }) => (
                      <div
                        {...props}
                        style={{
                          ...props.style,
                          height: "22px",
                          width: "22px",
                          borderRadius: "100%",
                          backgroundColor: "rgba(244,81,151,.25)",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <div
                          style={{
                            height: "10px",
                            width: "10px",
                            borderRadius: '100%',
                            backgroundColor: isDragged ? "#f45197" : "#f45197"
                          }}
                        />
                      </div>
                    )}
                  />
                }
              </div>
              <div className="flex justify-between text-xs text-gray-500 text-opacity-70 font-bold pt-1.5 mb-1">
                <div>
                  {MoneyFormat(configMoney[0])} ₫
                </div>
                <div>
                  {MoneyFormat(configMoney[1])} ₫
                </div>
              </div>
            </>
          }
          {otaParams.pageIndex === 0 &&
            <ContentLoader
              viewBox="0 0 270 80"
              title="Loading giá..."
              className="w-full"
            >
              <rect x="0" y="0" rx="5" ry="5" width="130" height="14" />
              <rect x="140" y="0" rx="5" ry="5" width="130" height="14" />
              <rect x="0" y="20" rx="5" ry="5" width="280" height="28" />
              <rect x="0" y="56" rx="5" ry="5" width="280" height="20" />
            </ContentLoader>
          }
        </div>
      </div>
      {step < 2 &&
        <div className="mb-3">
          <div className="flex justify-between items-center mb-3">
            <h4 className="font-extrabold text-sm text-gray-600">Hãng bay</h4>
            {otaParams.pageIndex >= 1 &&
              <button
                type="button"
                className="text-xs text-blue-600 text-opacity-90 font-bold"
                onClick={() => selectAllAirline()}
              >
                Chọn tất cả
              </button>
            }
            {otaParams.pageIndex === 0 &&
              <ContentLoader
                viewBox="0 0 64 16"
                title="Loading hãng bay..."
                className="w-16 h-4"
              >
                <rect x="0" y="0" rx="10" ry="10" width="60" height="16" />
              </ContentLoader>
            }
          </div>
          <div className="bg-white rounded-xl px-2 py-3 block-shadow">
            {otaParams.pageIndex >= 1 &&
              airlineCodes && airlineCodes.map((code) => (
                <div
                  key={code}
                  className="flex items-center p-1 cursor-pointer"
                  onClick={() => onSelectedAirline(code)}
                >
                  <div
                    className="w-3.5 h-3.5 mr-2.5"
                  >
                    {otaParams.airlineCodes?.includes(code)
                      ? <IconCheckBoxChecked />
                      : <IconCheckBox />
                    }
                  </div>
                  <img src={getImageOfAirline(code)} width={36} height={36} />
                  <span className="pl-2.5 text-sm text-gray-600 text-opacity-70 font-bold">{getNameOfAirline(code)}</span>
                </div>
              ))
            }
            {otaParams.pageIndex === 0 &&
              [1, 2, 3].map(v => (
                <ContentLoader
                  key={v.toString()}
                  viewBox="0 0 250 44"
                  title="Loading airline..."
                >
                  <rect x="4" y="10" width="16" height="16" />
                  <rect x="28" y="0" rx="5" ry="5" width="36" height="30" />
                  <rect x="70" y="10" rx="5" ry="5" width="120" height="12" />
                </ContentLoader>
              ))
            }
          </div>
        </div>
      }
      <div className="mb-3">
        <h4 className="font-extrabold text-sm mb-3 pt-1 text-gray-600">Giờ cất cánh</h4>
        <div className="bg-white rounded-xl px-2 py-3 block-shadow">
          <div className="flex justify-between items-center pt-5">
            <div className="text-xs text-gray-500 text-opacity-70 font-bold pt-1.5 pr-2">00:00</div>
            <div
              className="flex-auto flex justify-center flex-wrap px-2 h-8"
            >
              <Range
                allowOverlap
                values={timeTakeOff}
                step={1}
                min={0}
                max={1439}
                onChange={(values) => { setTimeTakeOff(values) }}
                renderTrack={({ props, children }) => (
                  <div
                    onMouseDown={props.onMouseDown}
                    onTouchStart={props.onTouchStart}
                    style={{
                      ...props.style,
                      height: "36px",
                      display: "flex",
                      width: "100%"
                    }}
                  >
                    <div
                      ref={props.ref}
                      style={{
                        height: "3px",
                        width: "100%",
                        borderRadius: "4px",
                        background: getTrackBackground({
                          values: timeTakeOff,
                          colors: ['#ccc', '#f45197', '#ccc'],
                          min: 0,
                          max: 1439
                        }),
                        alignSelf: "center"
                      }}
                    >
                      {children}
                    </div>
                  </div>
                )}
                renderThumb={({ index, props, isDragged }) => (
                  <div
                    {...props}
                    className="relative"
                    style={{
                      ...props.style,
                      height: "22px",
                      width: "22px",
                      borderRadius: "100%",
                      backgroundColor: "rgba(244,81,151,.25)",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <div
                      style={{
                        height: "10px",
                        width: "10px",
                        borderRadius: '100%',
                        backgroundColor: isDragged ? "#f45197" : "#f45197"
                      }}
                    />
                    <div
                      className={`bg-pink-400 text-gray-100 text-xs py-0.5 px-1.5 rounded-sm absolute
                          -translate-x-1/2
                        `}
                      style={{
                        bottom: '140%',
                        left: index == 0 ? '-10px' : 'calc(100% + 10px)'
                      }}
                    >
                      {convertTime(timeTakeOff[index])}
                      <div
                        className={`absolute w-0 h-0 ${index == 0 ? "right-0" : "left-0"}`}
                        style={{
                          top: '19px',
                          borderLeft: index == 0 ? '10px solid transparent' : '0 solid transparent',
                          borderRight: index == 0 ? '0 solid transparent' : '10px solid transparent',
                          borderTop: '20px solid #f45197'
                        }}
                      />
                    </div>
                  </div>
                )}
              />
            </div>
            <div className="text-xs text-gray-500 text-opacity-70 font-bold pt-1.5 pl-2">23:59</div>
          </div>
        </div>
      </div>
      <div className="mb-3">
        <h4 className="font-extrabold text-sm mb-3 pt-1 text-gray-600">Giờ hạ cánh</h4>
        <div className="bg-white rounded-xl px-2 py-3 block-shadow">
          <div className="flex justify-between items-center pt-5">
            <div className="text-xs text-gray-500 text-opacity-70 font-bold pt-1.5 pr-2">00:00</div>
            <div
              className="flex-auto flex justify-center flex-wrap px-2 h-8"
            >
              <Range
                allowOverlap
                values={timeLanding}
                step={1}
                min={0}
                max={1439}
                onChange={(values) => { setTimeLanding(values) }}
                renderTrack={({ props, children }) => (
                  <div
                    onMouseDown={props.onMouseDown}
                    onTouchStart={props.onTouchStart}
                    style={{
                      ...props.style,
                      height: "36px",
                      display: "flex",
                      width: "100%"
                    }}
                  >
                    <div
                      ref={props.ref}
                      style={{
                        height: "3px",
                        width: "100%",
                        borderRadius: "4px",
                        background: getTrackBackground({
                          values: timeLanding,
                          colors: ['#ccc', '#f45197', '#ccc'],
                          min: 0,
                          max: 1439
                        }),
                        alignSelf: "center"
                      }}
                    >
                      {children}
                    </div>
                  </div>
                )}
                renderThumb={({ index, props, isDragged }) => (
                  <div
                    {...props}
                    style={{
                      ...props.style,
                      height: "22px",
                      width: "22px",
                      borderRadius: "100%",
                      backgroundColor: "rgba(244,81,151,.25)",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <div
                      style={{
                        height: "10px",
                        width: "10px",
                        borderRadius: '100%',
                        backgroundColor: isDragged ? "#f45197" : "#f45197"
                      }}
                    />
                    <div
                      className={`bg-pink-400 text-gray-100 text-xs py-0.5 px-1.5 rounded-sm absolute
                              -translate-x-1/2
                            `}
                      style={{
                        bottom: '140%',
                        left: index == 0 ? '-10px' : 'calc(100% + 10px)'
                      }}
                    >
                      {convertTime(timeLanding[index])}
                      <div
                        className={`absolute w-0 h-0 ${index == 0 ? "right-0" : "left-0"}`}
                        style={{
                          top: '19px',
                          borderLeft: index == 0 ? '10px solid transparent' : '0 solid transparent',
                          borderRight: index == 0 ? '0 solid transparent' : '10px solid transparent',
                          borderTop: '20px solid #f45197'
                        }}
                      />
                    </div>
                  </div>
                )}
              />
            </div>
            <div className="text-xs text-gray-500 text-opacity-70 font-bold pt-1.5 pl-2">23:59</div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .block-shadow {
          box-shadow: 0 2px 4px rgb(0 0 0 / 10%);
          border: 0.5px solid #f2f2f2;
        }
        @media (min-width: 768px) {
          .block-shadow:focus,
          .block-shadow:hover {
              box-shadow:13px 13px 36px -18px rgba(0,0,0,.3),0 0 50px -12px rgba(50,50,93,.13)
          }
        }
      `}</style>
    </>
  );
};

export default BlockSortBy;