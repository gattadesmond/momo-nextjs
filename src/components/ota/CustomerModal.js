import { forwardRef, useCallback, useEffect, useImperativeHandle, useRef, useState } from "react";

import useOutsideClick from "@/lib/useOutsideClick";

import { IconAdult, IconChild, IconInfat } from '@/components/ota/IconOta';

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";
import isMobile from "@/lib/isMobile";

const CustomerModal = forwardRef((props, ref) => {
  const { classInput, formState = null, onChangeItem, isMobile } = props;

  useImperativeHandle(ref, () => ({
    Show() {
      setModalShow(true);
    },
    Hide() {
      setModalShow(false);
    }
  }));
  const containRef = useRef();
  const [modalShow, setModalShow] = useState(false);

  useOutsideClick(containRef, () => {
    if (modalShow === true) {
      setModalShow(false);
    }
  });

  const [customer, setCustomer] = useState(formState.values.customer || {
    adult: 1,
    child: 0,
    infat: 0
  });

  useEffect(() => {
    onChangeItem('customer', customer);
  }, [customer])

  useEffect(() => {
    setCustomer(formState.values.customer);
  }, [formState.values.customer])

  const handleChange = useCallback((name, value) => {
    if (value < 0) return;
    setCustomer((_state) => ({
      ..._state,
      [name]: parseInt(value)
    }));
  }, [setCustomer]);

  const DecreaseAdult = () => handleChange('adult', parseInt(customer.adult) - 1);
  const IncreaseAdult = () => handleChange('adult', parseInt(customer.adult) + 1);

  const DecreaseChild = () => handleChange('child', parseInt(customer.child) - 1);
  const IncreaseChild = () => handleChange('child', parseInt(customer.child) + 1);

  const DecreaseInfat = () => handleChange('infat', parseInt(customer.infat) - 1);
  const IncreaseInfat = () => handleChange('infat', parseInt(customer.infat) + 1);

  return (
    <>
      {isMobile &&
        <Modal isOpen={modalShow} onDismiss={() => setModalShow(false)} isFull={true} isBig={true}>
          <ModalHeader> Thêm hành khách </ModalHeader>

          <ModalBody css="p-0 bg-white rounded block-form-ota">
            <div className="modal-hanhkhach pt-10 pb-6 px-7">
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconAdult />
                </div>
                <div className="col-span-5 pr-0 hk-name">
                  Người lớn
                  <small>(từ 12 tuổi)</small>
                </div>
                <div className="col-span-2 hk-count">{customer.adult}</div>
                <div className="col-span-4 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.adult == 1}
                      onClick={DecreaseAdult}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.adult == 9 || customer.adult + customer.child == 9}
                      onClick={IncreaseAdult}
                    >+</button>
                  </div>
                </div>
              </div>
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconChild />
                </div>
                <div className="col-span-5 pr-0 hk-name">
                  Trẻ em<br />
                  <small>(2-12 tuổi)</small>
                </div>
                <div className="col-span-2 hk-count">{customer.child}</div>
                <div className="col-span-4 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.child == 0}
                      onClick={DecreaseChild}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.child == 8 || customer.adult + customer.child == 9}
                      onClick={IncreaseChild}
                    >+</button>
                  </div>
                </div>
              </div>
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconInfat />
                </div>
                <div className="col-span-5 pr-0 hk-name">
                  Em bé<br />
                  <small>(dưới 2 tuổi)</small>
                </div>
                <div className="col-span-2 hk-count">{customer.infat}</div>
                <div className="col-span-4 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.infat == 0}
                      onClick={DecreaseInfat}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.infat == 4 || customer.adult == customer.infat}
                      onClick={IncreaseInfat}
                    >+</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="text-center px-8">
              <button type="button" className="btn-primary w-full"
                onClick={() => setModalShow(false)}
              >
                Xác nhận
              </button>
            </div>
          </ModalBody>

          <ModalFooter className="flex">
          </ModalFooter>
        </Modal>
      }
      {!isMobile &&
        <div className={`popup-ota ota-customer absolute top-full z-10 bg-white rounded-md ${modalShow ? 'block' : 'hidden'}`}
          ref={containRef}
        >
          <div className={`relative overflow-y-auto bg-white`}>
            <div className="modal-hanhkhach p-3 pb-1">
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconAdult />
                </div>
                <div className="col-span-5 lg:col-span-4 pr-0 hk-name">
                  Người lớn
                  <small>(từ 12 tuổi)</small>
                </div>
                <div className="col-span-1 lg:col-span-2 hk-count text-center">{customer.adult}</div>
                <div className="col-span-5 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.adult == 1}
                      onClick={DecreaseAdult}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.adult == 9 || customer.adult + customer.child == 9}
                      onClick={IncreaseAdult}
                    >+</button>
                  </div>
                </div>
              </div>
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconChild />
                </div>
                <div className="col-span-5 lg:col-span-4 pr-0 hk-name">
                  Trẻ em<br />
                  <small>(2-12 tuổi)</small>
                </div>
                <div className="col-span-1 lg:col-span-2 hk-count text-center">{customer.child}</div>
                <div className="col-span-5 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.child == 0}
                      onClick={DecreaseChild}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.child == 8 || customer.adult + customer.child == 9}
                      onClick={IncreaseChild}
                    >+</button>
                  </div>
                </div>
              </div>
              <div className="grid grid-cols-12 mb-3 items-center">
                <div className="col-span-1 hk-icon">
                  <IconInfat />
                </div>
                <div className="col-span-5 lg:col-span-4 pr-0 hk-name">
                  Em bé<br />
                  <small>(dưới 2 tuổi)</small>
                </div>
                <div className="col-span-1 lg:col-span-2 hk-count text-center">{customer.infat}</div>
                <div className="col-span-5 text-center">
                  <div className="btn-group" role="group">
                    <button type="button" className="btn"
                      disabled={customer.infat == 0}
                      onClick={DecreaseInfat}
                    >-</button>
                    <button type="button" className="btn"
                      disabled={customer.infat == 4 || customer.adult == customer.infat}
                      onClick={IncreaseInfat}
                    >+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <style jsx>{`
            .ota-customer {
              width: 125%;
            }
            @media (min-width: 992px) {
              .ota-customer {
                width: 110%;
              }
            }
          `}</style>
        </div>
      }
    </>
  )
});
export default CustomerModal;