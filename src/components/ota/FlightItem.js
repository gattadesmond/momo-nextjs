
export default function FlightItem({ data, logo, fullName, onSelect, onShowBFM }) {

  const getNote = (airline) => {
    switch (airline) {
      case 'VN':
        return 'Khai thác bởi Vietnam Airlines';
      case '0V':
        return 'Khai thác bởi Vietnam Air Service';
      case 'BL':
        return 'Khai thác bởi Pacific Airlines';
      case 'QH':
        return 'Khai thác bởi Bamboo Airways';
      case 'VJ':
        return 'Khai thác bởi VietJet Air';
      default:
        return '';
    }
  }

  return (
    <div
      className={`item-vemaybay group bg-white rounded-xl border border-gray-200 p-5 mb-5 shadow-md
        transition-all hover:border-blue-400 hover:shadow-lg
      `}
    >
      <div className="flex flex-wrap items-stretch">
        <div className="flex order-1 mr-4">
          <div className="rounded-lg border border-gray-200 w-12 h-12 flex items-center justify-center">
            <img
              alt={data.AirlineCode}
              src={logo}
              width={46}
              height={46}
              loading="lazy"
            />
          </div>
        </div>
        <div className="order-3 flex md:block md:order-2 pt-3 md:pt-0 md:mr-4">
          <h5 className="text-gray-500 text-sm font-bold mr-4 md:mr-0">{fullName}</h5>
          <span className="text-gray-400 text-sm font-bold">{data.TicketStr}</span>
        </div>
        <div className="flex-auto order-2 md:order-3 flex justify-end">
          <div className="grow rounded-md bg-[#f7faff] flex items-center max-w-[400px]">
            <div className="flex-initial text-right px-2">
              <div className="font-bold text-gray-600 text-opacity-90">{data.DepartureTimeStr}</div>
              <div className="font-bold text-sm text-gray-500 text-opacity-80">{data.DepartureCode}</div>
            </div>
            <div className="flex-auto text-center text-gray-600 px-2">
              <div className="relative">
                <div className="text-xs font-bold pb-1.5 text-gray-500 text-opacity-70">
                  {data.DurationStr}
                </div>
                <div className="absolute left-0 top-1/2 -translate-y-1/2 w-full h-0 border-t 
                  border-dotted border-gray-400"></div>
                <div className="absolute left-0 top-1/2 -translate-y-1/2 w-full flex justify-center h-3 md:h-4">
                  <svg xmlns="http://www.w3.org/2000/svg" className="-rotate-90" height="100%" viewBox="0 0 16 19" fill="none">
                    <path d="M10 15.8333L10 12.0633L15.7481 8.59646C15.8247 8.55023 15.8883 8.48348 15.9325 8.40297C15.9767 8.32245 16 8.23103 16 8.13795L16 5.97735C16 5.62671 15.6822 5.3737 15.3628 5.47002L10 7.08734L10 3.69437L11.8 2.26937C11.9259 2.16975 12 2.01306 12 1.84714L12 0.528022C12 0.184635 11.6944 -0.0673796 11.3788 0.0160756L8 1.05547L4.62125 0.0160753C4.30563 -0.0673799 4 0.184635 4 0.528022L4 1.84714C4 2.01339 4.07406 2.16975 4.2 2.26937L6 3.69437L6 7.08734L0.637501 5.47002C0.318125 5.3737 7.07723e-07 5.62671 6.92396e-07 5.97735L5.97953e-07 8.13795C5.89677e-07 8.32729 0.0962502 8.50212 0.251876 8.59613L6 12.0633L6 15.8333C6 16.9994 6.89531 19 8 19C9.10469 19 10 16.9994 10 15.8333Z" fill="#4F4F4F" />
                  </svg>
                </div>
                <div className="text-xs font-bold pt-1.5 text-gray-500 text-opacity-70">
                  {data.TotalFlightSegments > 1 ? `${data.TotalFlightSegments - 1} điểm dừng` : "Bay thẳng"}
                </div>
              </div>
            </div>
            <div className="flex-initial text-left px-2">
              <div className="font-bold text-gray-600 text-opacity-90">
                {data.ArrivalTimeStr}
                {data.TotalDays > 0 &&
                  <small className="pl-1 font-normal text-gray-400">(+{data.TotalDays}d)</small>
                }
              </div>
              <div className="font-bold text-sm text-gray-500 text-opacity-80">{data.ArrivalCode}</div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex items-center">
        <div className="flex-initial">
          <div className="my-2.5 text-red-400 font-semibold">
            {data.FareStr} đồng/khách
          </div>
          {data.ParentTagText
            ?
            <span 
              className="text-[10px] font-semibold text-center rounded-full py-1.5 px-4"
              style={{
                backgroundColor: data.ParentTagBgColor ? data.ParentTagBgColor : '#cbbfdb',
                color: data.ParentTagColor ? data.ParentTagColor : '#31006f'
              }}
            >
              {data.ParentTagText}
            </span>
            :
            <span className="text-[10px] font-semibold text-center rounded-full py-1.5 px-4 
          bg-[#cbbfdb] text-[#31006f]">
              {getNote(data.Airline)}
            </span>
          }
        </div>
        <div className="flex-auto grid grid-row-2 justify-items-end pt-2 md:pt-4">
          <button type="button"
            className="block font-extrabold text-md text-center text-[#a50064] py-1.5 px-6 
            rounded-full bg-white border border-[#a50064] hover:bg-[#a50064] hover:text-white"
            onClick={onSelect}
          >
            Chọn
          </button>
          <button type="button"
            className="flex items-start pt-3 font-bold text-tiny text-blue-700 
            md:hover:text-momo"
            onClick={() => onShowBFM()}
          >
            <svg xmlns="http://www.w3.org/2000/svg" className="mr-1 w-4 h-4" viewBox="0 0 512 512">
              <path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path>
            </svg>
            <span className="underline">Điều kiện vé</span>
          </button>
        </div>
      </div>
      <style jsx>{`
      
      `}</style>
    </div>
  )
}