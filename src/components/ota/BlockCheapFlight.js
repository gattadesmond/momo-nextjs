import isFuture from 'date-fns/isFuture';
import parse from 'date-fns/parse';
import compareAsc from 'date-fns/compareAsc';

import Heading from "../Heading";
import SectionSpace from "../SectionSpace"

const BlockCheapFlight = ({ data }) => {
  if (!data) return (<></>);
  if (data.length === 0) return (<></>);

  const renderRow = (key, item) => {
    const getUrlDay = (item, day) => {
      return `https://momo.vn/ve-may-bay/tim-chuyen/?flightType=oneway&route=${item.DeptJsId}.${item.DestJsId}&fromDate=${day.Day}&pax=1.0.0`;
    }
    item.Days.forEach(day => {
      day.Date = parse(day.Day, 'dd-MM-yyyy', new Date())
    });

    const fistFlight = item.Days
      .sort((a, b) => compareAsc(a.Date, b.Date))
      .filter(x => isFuture(x.Date))
      [0];
    
    if (!fistFlight) return (<></>);
    
    const formatMoney = (money) => {
      return new Intl.NumberFormat('vi-VN').format(money);
    }

    return (
      <tr key={key} className={key % 2 ? "bg-gray-100" : ""}>
        <td className="md:w-72 border-0 font-bold text-gray-800 text-tiny">
          {item.DeptJsCv}{/* */} &rarr; <br className="md:hidden" />{/* */}{item.DestJsCv}
        </td>
        <td className="border-0 py-3 text-center">
          <a
            href={getUrlDay(item, fistFlight)}
            className="inline-block bg-pink-50 border border-pink-200 rounded-md px-3 py-2 hover:bg-pink-100 text-center"
          >
            <strong className="block text-pink-600 leading-3">{formatMoney(fistFlight.Price)}</strong>
          </a>
        </td>
        <td className="w-20 sm:w-auto border-0 text-tiny text-center">
          {fistFlight.Day}
        </td>
      </tr>
    );
  }

  return (
    <div id={`block-${data?.Id || -1}`}>
      <SectionSpace type={2}>
        <div className="mb-5 text-center md:mb-8" id="section-catblog">
          <Heading
            title={"Vé máy bay giá rẻ"}
            subtitle={`
              Giá vé máy bay rẻ nhất, tự động cập nhật từ ứng dụng MoMo. 
              <br class="hidden md:block"/>
              Bạn có thể đi muôn nơi với chi phí tiết kiệm, an toàn, nhanh chóng!
            `}
            color="pink"
          />
        </div>
        <div className="table-responsive my-10 max-w-3xl mx-auto">
          <table id="tb-vegiare" className="table m-0 table-auto">
            <caption className="hidden">Vé máy bay giá rẻ</caption>
            <tbody>
              <tr className="font-bold text-tiny uppercase text-gray-500 border-b">
                <td className="border-0 py-3">Chặng bay</td>
                <td className="border-0 py-3 text-center">Giá vé (VND)</td>
                <td className="border-0 py-3 text-center">Ngày đi</td>
              </tr>
              {data.length > 0 && data
                .map((item, index) =>
                  renderRow(index, item)
              )}
            </tbody>
          </table>
          <p className="mt-5 text-xs italic text-gray-500">
            * Giá vé có thể thay đổi theo thời điểm đặt mua. Vui lòng kiểm tra tại chức năng Tìm kiếm vé hoặc App MoMo.
          </p>
        </div>
        <style jsx>{`
          #tb-vegiare :global(td) {
            min-width: 90px;
          }
        `}</style>
      </SectionSpace>
    </div>
  )
}

export default BlockCheapFlight;