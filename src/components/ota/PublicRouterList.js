import Link from "next/link";

import Heading from "../Heading";

const PublicRouterList = ({ data }) => {
  return (
    <>
      <div className="mb-5 text-center md:mb-8" id="section-changbay">
        <Heading title={data.Title} color="pink" />
      </div>
      <div className="grid gap-2 md:grid-cols-2 lg:grid-cols-3">
        {data.listFlight.map((item, index) => (
          <div key={item.Id}>
            <Link href={item.UrlRewrite}>
              <a className="text-md text-blue-600 hover:text-momo">Chặng bay từ {item.DepartureJsCv} đến {item.DestinationJsCv}</a>
            </Link>
          </div>
        ))}
      </div>
    </>
  )
};

export default PublicRouterList;