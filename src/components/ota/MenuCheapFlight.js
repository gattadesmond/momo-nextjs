import { useEffect, useState } from "react";
import { axios } from "@/lib/index";
const MenuCheapFlight = ({}) => {
  const [list, setList] = useState(null);

  useEffect(() => {
    axios.get(`/ota/getCheapFlights?isHome=true`).then(function (response) {
      const res = response.Data ?? null;
      setList(res ?? null);
    });
  }, []);


  const formatMoney = (money) => {
    return new Intl.NumberFormat("vi-VN").format(money);
  };

  const getUrlDay = (item, day) => {
    return `https://momo.vn/ve-may-bay/tim-chuyen/?flightType=oneway&route=${item.DeptJsId}.${item.DestJsId}&fromDate=${day.Day}&pax=1.0.0`;
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-3">
      {list &&
        list.map((item, index) => (
          <div
            className=" relative overflow-hidden rounded-md border border-gray-200 hover:shadow-md"
            key={index}
          >
            <div className="cheap-item-image  relative aspect-[16/9] w-full">
              <img
                src={item.Avatar}
                loading="lazy"
                className="absolute inset-0 h-full w-full object-cover"
              />

              <div className="z-2 absolute bottom-0 w-full px-3 py-2 text-white">
                <div className=" md:text-tiny text-white text-opacity-80">
                  Vé máy bay
                </div>
                <div className="text-lg md:text-base font-bold ">
                  {item.DeptJsCv} → {item.DestJsCv}
                </div>
              </div>
            </div>

            {item?.Days?.length > 0 && (
              <div className="flex flex-nowrap items-center p-3">
                <div className=" flex-1">
                  <div className="text-tiny mb-1 text-gray-500">
                    Ngày đi : <strong>{item?.Days?.[0].Day}</strong>
                  </div>

                  <div className="text-tiny text-gray-600">
                    Chỉ từ :{" "}
                    <strong className="text-md">
                      {formatMoney(item?.Days?.[0].Price)} đ
                    </strong>
                  </div>
                </div>

                <a
                  href={getUrlDay(item, item?.Days?.[0])}
                  className=" z-10 stretched-link flex-shrink-0 text-tiny rounded border border-pink-600 py-1 px-2 font-semibold text-pink-700 transition-all hover:bg-pink-50 hover:text-pink-800 md:opacity-0 md:w-0"
                >
                  Xem ngay
                </a>
              </div>
            )}
          </div>
        ))}

      <style jsx>{`
        .cheap-item-image:after {
          content: "";
          position: absolute;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 55%;
          background-color: rgba(16, 16, 16, 0);
          background: linear-gradient(
            180deg,
            rgba(255, 255, 255, 0) 0,
            #268f7b
          );
          z-index: 1;
        }
      `}</style>
    </div>
  );
};
export default MenuCheapFlight;
