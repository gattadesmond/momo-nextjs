import Image from "next/image";

import parse from "html-react-parser";
import { decode } from "html-entities";

import Heading from "../Heading";

const LocationInfo = ({ title, dataLocation }) => {
  return (
    <>
      <div className="mb-5 text-center md:mb-8" id="section-diemden">
        <Heading title={title} color="pink" />
      </div>
      <div className="grid gap-6 md:grid-cols-12">
        <div className="md:col-span-4 aspect-w-16 aspect-h-9 aspect-77-37">
          <Image
            className="rounded-lg"
            src={dataLocation.Avatar}
            alt={title || ""}
            layout="fill"
            objectFit="cover"
            loading="lazy"
          />
        </div>
        <div className="md:col-span-8 flex flex-col mt-6 md:mt-0">
          {parse(decode(dataLocation.Content))}
        </div>
      </div>
    </>
  )
};

export default LocationInfo;