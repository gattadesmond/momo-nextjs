import Link from "next/link";

import AnchorLink from "react-anchor-link-smooth-scroll";

import { useState, useEffect, useRef } from "react";

import CssCarousel from "@/components/CssCarousel";

function CinemaMenu() {
  return (
    <>
      <CssCarousel>
        <nav
          aria-label="breadcrumb "
          className="py-3 lg:py-4 bg-white overflow-scroll w-full  items-center pl-4 md:pl-0 flex-nowrap hidden md:flex"
        >
          <div className="relative px-3 pl-0 text-sm text-gray-800 breadcrumb-item  hover:text-pink-700 shrink-0">
            <a
              className=" whitespace-nowrap"
              aria-label="MoMo"
              href="https://momo.vn/"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-4 h-4"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
              </svg>
            </a>
          </div>

          <div className="relative px-3 text-sm text-gray-800 breadcrumb-item  hover:text-pink-700 flex items-center shrink-0">

            <img src="https://static.mservice.io/fileuploads/svg/momo-file-210817143558.svg" className="w-4 mr-2" />

            <span className="font-bold text-gray-800 whitespace-nowrap ">  Cinema</span>
          </div>





          <div className="relative px-3 text-sm text-gray-800 breadcrumb-item shrink-0 hover:text-pink-700  ">
            <div className="flex divide-x flex-nowrap whitespace-nowrap">


              <AnchorLink href="#cumrap" offset={50} className="shrink-0">
                <span className="flex items-center px-3 space-x-2 text-sm font-semibold text-gray-500 no-underline hover:text-pink-600 whitespace-nowrap">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-4 h-4 text-pink-600 "
                  >
                    <path
                      d="M13.06 14.84H2.94a1.79 1.79 0 01-1.79-1.79v-8.9a1.79 1.79 0 011.79-1.79H3v.73a.83.83 0 001.65 0v-.73h2.52v.73a.83.83 0 001.66 0v-.73h2.51v.73a.83.83 0 001.65 0v-.73h.07a1.79 1.79 0 011.79 1.79v8.9a1.79 1.79 0 01-1.79 1.79zm.64-9.71H2.3v7.78a.77.77 0 00.78.77h9.84a.77.77 0 00.78-.77zm-1.54-1.58a.47.47 0 01-.46-.46V1.62a.47.47 0 01.46-.46.46.46 0 01.46.46v1.47a.46.46 0 01-.46.46zM8 3.55a.47.47 0 01-.46-.46V1.62a.46.46 0 01.92 0v1.47a.47.47 0 01-.46.46zm-4.16 0a.46.46 0 01-.46-.46V1.62a.46.46 0 01.46-.46.47.47 0 01.46.46v1.47a.47.47 0 01-.46.46z"
                      id="m2"
                      fill="currentColor"
                    />
                  </svg>
                  <div>Lịch chiếu</div>
                </span>
              </AnchorLink>


              <AnchorLink href="#rapphim" offset={50} className="shrink-0">
                <span className="flex items-center px-3 space-x-2 text-sm font-semibold text-gray-500 no-underline hover:text-pink-600 whitespace-nowrap">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-4 h-4 text-pink-600 "
                  >
                    <path
                      d="M15.25 9.63H.75a.58.58 0 01-.6-.57v-7.5A.59.59 0 01.75 1h14.5a.59.59 0 01.6.57v7.5a.58.58 0 01-.6.56zm-.59-7.49H1.34v6.35h13.32zM1.39 13.06h1.24a1.15 1.15 0 011.18 1.12V15H2.68v-.83H1.39V15H.21v-.83a1.15 1.15 0 011.18-1.11zm.82-.71v-.83a1.15 1.15 0 011.18-1.12h1.23a1.15 1.15 0 011.19 1.12v.83H4.67v-.83H3.34v.83zm3.18.71h1.23a1.16 1.16 0 011.19 1.12V15H6.67v-.83H5.39V15H4.2v-.83a1.15 1.15 0 011.19-1.11zm.81-.71v-.83a1.15 1.15 0 011.18-1.12h1.24a1.15 1.15 0 011.18 1.12v.83H8.67v-.83H7.38v.83zm3.18.71h1.23a1.15 1.15 0 011.19 1.12V15h-1.14v-.83H9.33V15H8.2v-.83a1.15 1.15 0 011.18-1.11zm.81-.71v-.83a1.15 1.15 0 011.19-1.12h1.23a1.15 1.15 0 011.18 1.12v.83h-1.13v-.83h-1.33v.83zm3.18.71h1.24a1.15 1.15 0 011.18 1.12V15h-1.14v-.83h-1.28V15h-1.18v-.83a1.15 1.15 0 011.18-1.11z"
                      id="m3"
                      fill="currentColor"
                    />
                  </svg>
                  <div>Rạp phim</div>
                </span>
              </AnchorLink>

              <AnchorLink href="#section-blog" offset={90} className="shrink-0">
                <span className="flex items-center px-3 space-x-2 text-sm font-semibold text-gray-500 no-underline hover:text-pink-600 whitespace-nowrap">
                  <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4 text-pink-600 " fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                  </svg>


                  <div>Blog Phim</div>
                </span>
              </AnchorLink>
            </div>
          </div>
        </nav>
      </CssCarousel>




      <style jsx>{`
        .breadcrumb-item + .breadcrumb-item:before {
          content: "";
          opacity: 0.3;
          width: 1.5rem;
          height: 1rem;
          background-image: url("data:image/svg+xml,%0A%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-chevron-right'%3E%3Cpolyline points='9 18 15 12 9 6'%3E%3C/polyline%3E%3C/svg%3E");
          background-repeat: no-repeat;

          left: -6px;
          top: 2px;
          position: absolute;
       
        }

     
      `}</style>
    </>
  );
}

export default CinemaMenu;
