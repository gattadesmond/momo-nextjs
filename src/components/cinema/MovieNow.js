
import dynamic from "next/dynamic";

const Desktop = dynamic(() => import("../../components/cinema/MovieNowDesktop"));
const Mobile = dynamic(() => import("../../components/cinema/MovieNowMobile"));

const MovieNow = (props) => {
  
  return (
    <>
      {props.isMobile == true  ? (
        <Mobile data={props.data} isDark={props.isDark} />
      ) : (
        <Desktop data={props.data} isDark={props.isDark} />
      )}
    </>
  );
};

export default MovieNow;
