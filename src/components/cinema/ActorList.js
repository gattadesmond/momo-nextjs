import Link from "next/link";

import Image from "next/image";

import SojuSlider from "@/components/SojuSlider";

import ActorItem from "@/components/cinema/ActorItem";

const ActorList = ({ data }) => {
  return (
    <>
      <SojuSlider>
        <div className="flex w-full overflow-auto flex-nowrap ">
          {data.map((item, index) => (
            <div className="actor-col" key={item.id}>
              <ActorItem data={item} />
            </div>
          ))}
        </div>
      </SojuSlider>

      <style jsx>{`
        .actor-col {
          flex: 0 0 ${(1 / 6) * 100}%;
        }

        @media screen and (max-width: 600px) {
          .actor-col {
            flex : 0 0 100px;
          }
        }
      `}</style>
    </>
  );
};

export default ActorList;
