import Link from "next/link";

import { useEffect, useState } from "react";

import Image from "next/image";

import MovieBookingRapList from "@/components/cinema/MovieBookingRapList";

import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

import { axios } from "@/lib/index";

import { useSelector } from "react-redux";

const BookingRapSelectMobile = ({ master, sAlone = false }) => {
  const [modal, setModal] = useState(false);
  const filmStore = useSelector((state) => state.filmBooking);

  const { rapId } = filmStore;
  const [rapChieu, setRapChieu] = useState(null);

  const close = () => {
    setModal(false);
  };
  useEffect(() => {
    if (modal == true) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.removeProperty("overflow");
    }
    return function clean() {
      document.body.style.removeProperty("overflow");
    };
  }, [modal]);

  useEffect(() => {
    if (!rapId) return;
    axios
      .get(`/ci-cinema/detail-by-apiCinemaId/${rapId}`)
      .then(function (response) {
        const res = response.Data ?? null;

        if (res) {
          setRapChieu(res);
        } else {
          setRapChieu(null);
        }
      });

    return function cleanup() {
      setModal(false);
      setRapChieu(null);
    };
  }, [rapId]);

  return (
    <>
      <div
        className="relative mt-5 cursor-pointer  rounded border  border-pink-600 hover:bg-gray-50 "
        onClick={() => setModal(true)}
      >
        <div className="z-1 absolute -top-2.5 left-2 mb-2 inline-block bg-white px-1 text-sm font-semibold text-pink-600">
          Chọn rạp xem lịch chiếu
        </div>
        <div className=" flex flex-nowrap items-center space-x-3 px-3 pb-2 pt-4">
          <div className="bg-gray-5  relative aspect-square h-10 w-10 flex-none overflow-hidden rounded border border-gray-200">
            {rapChieu?.Avatar && (
              <Image
                src={rapChieu.Logo}
                alt={rapChieu.Name}
                layout="fill"
                objectFit="cover"
              />
            )}
          </div>

          <div className="min-w-0 flex-1 ">
            <div className="text-md mb-0 font-bold leading-tight text-gray-800">
              {rapChieu?.Name && rapChieu.Name}
            </div>
            <div className="text-tiny truncate text-gray-500">
              {rapChieu?.Address && rapChieu.Address}
            </div>
          </div>

          <div className="flex-none  ">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4 opacity-50"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M19 9l-7 7-7-7"
              />
            </svg>
          </div>
        </div>
      </div>

      <div
        className={`modal  fixed inset-0 flex max-h-full flex-row items-end justify-center bg-transparent p-0 pt-3 text-gray-800 ${
          modal ? " visible " : "invisible  select-none"
        }`}
        role="dialog"
        aria-modal="true"
      >
        <div
          className={` modal-cluster relative flex h-full max-h-full   w-full max-w-full flex-col rounded-t-xl bg-white  shadow-xl  md:rounded-xl  ${
            modal ? "modalFadeUp  " : ""
          }`}
        >
          <button
            className="absolute top-0 left-0 right-auto z-20 mt-1 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full hover:bg-gray-50 hover:bg-opacity-20 md:left-auto md:right-3 "
            onClick={close}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className=" block h-6 w-6"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>

          <ModalHeader isBorder={true}>Danh sách rạp</ModalHeader>

          <ModalBody css="p-0 bg-white rounded relative">
            {!sAlone && (
              <div className="sticky top-0 z-30 block border-b border-gray-200 bg-white pt-3 pb-2 shadow-sm  ">
                <MovieBookingRap data={master.Cineplexs} />
              </div>
            )}

            <div className=" relative divide-y divide-gray-100  ">
              <MovieBookingRapList />
            </div>
          </ModalBody>

          <ModalFooter className="flex">
            <div className="flex">
              <a
                onClick={close}
                className="btn flex cursor-pointer items-center rounded-md bg-pink-700 px-5 py-2 text-center text-sm font-bold uppercase text-white text-opacity-90 transition-all hover:bg-pink-800"
              >
                Đóng
              </a>
            </div>
          </ModalFooter>
        </div>

        <div
          className="modal-overlay fixed inset-0 overflow-y-auto bg-black bg-opacity-50"
          onClick={close}
        ></div>
      </div>

      <style jsx>
        {`
          .modalFadeUp {
            animation-duration: 400ms;
            animation-iteration-count: 1;
            animation-fill-mode: both;
            animation-name: keyframe_d37zz3;
          }

          @keyframes keyframe_d37zz3 {
            from {
              opacity: 0;
              transform: translate3d(0, 100%, 0);
            }

            to {
              opacity: 1;
              transform: translate3d(0, 0, 0);
            }
          }
        `}
      </style>
    </>
  );
};
export default BookingRapSelectMobile;
