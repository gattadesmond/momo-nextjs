import Link from "next/link";

import { useEffect, useState } from "react";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";

import MovieType from "@/components/cinema/MovieType";

import MovieItem2 from "@/components/cinema/MovieItem2";

import QRCodeScan from "@/components/QrCodeScan";

import QRCode from "qrcode.react";

import Loader from "@/components/ui/Loader";

import { parseISO, format } from "date-fns";

import { axios } from "@/lib/index";

import { useRouter } from "next/router";

import { Media, MediaContextProvider } from "@/lib/mediaFresnel";

const MovieBookingStep = ({ gioChieu, isOpen, dissmiss }) => {
  const router = useRouter();

  const [qrUrl, setQrUrl] = useState(null);

  const [data, setData] = useState(null);

  const [rapChieu, setRapChieu] = useState(null);


  const close = () => {
    dissmiss();
  };

  useEffect(() => {
    axios
      .get(
        `/ci-film/deepLink/${gioChieu.ApiFilmId}?apiCinemaId=${gioChieu.ApiCinemaId}&cineplex=${gioChieu.Cineplex}&showDateTime=${gioChieu.ShowDateTime}&id=${gioChieu.Id}&cityId=${gioChieu.CityId}`
      )
      .then(function (response) {
        const res = response.Data ? response.Data[0] : null;

        if (res) {
          setQrUrl(res);
        } else {
          setQrUrl(null);
        }
      });

    return function cleanup() {
      setQrUrl(null);
    };
  }, []);

  useEffect(() => {
    axios
      .get(`ci-film/detail-by-apiFilmId/${gioChieu.ApiFilmId}`)
      .then(function (response) {
        const res = response.Data ?? null;

        if (res) {
          setData(res);
        } else {
          setData(null);
        }
      });

    return function cleanup() {
      setData(null);
    };
  }, []);

  useEffect(() => {
    axios
    .get(`/ci-cinema/detail-by-apiCinemaId/${gioChieu.ApiCinemaId}`)
      .then(function (response) {
        const res = response.Data ?? null;

        if (res) {
          setRapChieu(res);
        } else {
          setRapChieu(null);
        }
      });

    return function cleanup() {
      setRapChieu(null);

    };
  }, []);

  return (
    <div>
      <MediaContextProvider>
        <Media lessThan="md">
          <Modal isOpen={isOpen} onDismiss={close} isFull={false}>
            <ModalHeader isBorder={true}>Thông tin suất chiếu</ModalHeader>
            <ModalBody css="p-0 bg-white rounded">
              <div className="px-5 py-5 text-sm ">
                {data && <MovieItem2 data={data} />}

                <div className="mt-5 border-t border-gray-200 pt-5">
                  <div className="mb-3 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">Độ tuổi :</span>{" "}
                    {data && data.ApiRating}
                  </div>
                  <div className="mb-3 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">
                      Thời gian :
                    </span>{" "}
                    {gioChieu.ShowTimeDuration}
                  </div>

                  <div className="mb-3 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">Ngày :</span>{" "}
                    {format(parseISO(gioChieu.ShowTime), "dd-MM-yyyy")}
                  </div>

                  <div className="mb-3 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">
                      Loại suất :
                    </span>{" "}
                    <MovieType
                      vtype={gioChieu.VersionType}
                      ctype={gioChieu.CaptionType}
                    />
                  </div>

                  <div className="mb-2 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">
                      Rạp chọn :
                    </span>{" "}
                   {rapChieu && rapChieu.Name}
                  </div>
                  <div className="mb-3 font-semibold text-gray-900">
                    <span className="font-normal text-gray-500">
                    {rapChieu && rapChieu.Address}
                    </span>
                  </div>
                </div>

                <div className="py-2">
                  <button
                    disabled={!qrUrl ? true : false}
                    className="text-md w-100 block w-full rounded-md bg-pink-600 px-3 py-3 font-semibold text-white"
                    onClick={() => router.push(qrUrl)}
                  >
                    {qrUrl ? (
                      <span>Chọn chỗ ngồi và Đặt vé</span>
                    ) : (
                      <span>Lấy thông tin</span>
                    )}
                  </button>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </Media>
        <Media greaterThan="sm">
          <Modal isOpen={isOpen} onDismiss={close} isDark={true}>
            <ModalBody css="p-0 bg-white rounded">
              <div className="flex flex-row flex-wrap ">
                <div
                  className="w-full flex-initial md:order-2 md:w-7/12"
                  style={{
                    background:
                      "url(https://static.mservice.io/jk/momo2020/img/intro/qrcode-pattern.png) 10px 10px no-repeat,linear-gradient(to top,#c1177c,#e11b90)",
                  }}
                >
                  <div className="px-5 py-10 text-center ">
                    <h4 className="mb-5 text-base text-white ">
                      Quét mã QR bằng MoMo để chọn ghế
                    </h4>

                    <QRCodeScan>
                      {!qrUrl && (
                        <div className="fadeIn absolute inset-0 z-10 flex items-center justify-center bg-white bg-opacity-80">
                          <Loader />
                        </div>
                      )}
                      {qrUrl && (
                        <QRCode
                          value={qrUrl}
                          size={215}
                          imageSettings={{
                            src: "https://static.mservice.io/pwa/images/logoMomox50.png",
                            x: null,
                            y: null,
                            height: 50,
                            width: 50,
                            excavate: true,
                          }}
                          level="H"
                        />
                      )}
                    </QRCodeScan>
                  </div>
                </div>
                <div className="w-full flex-none md:order-1 md:w-5/12">
                  <div className="px-5 py-8 text-sm ">
                    {data && <MovieItem2 data={data} />}

                    <div className="mt-5 border-t border-gray-200 pt-5">
                      <div className="mb-3 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                          Độ tuổi :
                        </span>{" "}
                        {data && data.ApiRating}
                      </div>
                      <div className="mb-3 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                          Thời gian :
                        </span>{" "}
                        {gioChieu.ShowTimeDuration}
                      </div>

                      <div className="mb-3 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                          Ngày :
                        </span>{" "}
                        {format(parseISO(gioChieu.ShowTime), "dd-MM-yyyy")}
                      </div>

                      <div className="mb-3 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                          Loại suất :
                        </span>{" "}
                        <MovieType
                          vtype={gioChieu.VersionType}
                          ctype={gioChieu.CaptionType}
                        />
                      </div>

                      <div className="mb-2 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                          Rạp chọn :
                        </span>{" "}
                        {rapChieu && rapChieu.Name}
                      </div>
                      <div className="mb-3 font-semibold text-gray-900">
                        <span className="font-normal text-gray-500">
                        {rapChieu && rapChieu.Address}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </Media>
      </MediaContextProvider>

    </div>
  );
};

export default MovieBookingStep;
