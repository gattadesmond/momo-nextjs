function oneCharName(input) {
  if (!input) {
    return input;
  }
  input = input.replace('/\r/g', '').replace('/\n/g', '');

  // var buff = input.split(' ');
  // if (buff.length == 1)
  //     return buff[0][0].toUpperCase();
  // if (buff.length > 1) {
  //     var first = buff[buff.length - 1][0].toUpperCase();
  //     //var second = buff[buff.length - 2][0].toUpperCase();
  //     //return second + first;
  //     return first;
  // }
  return input[0];
};

const UserAvatar = ({ avatarUrl = "", showName = "" }) => {
  return (
    <>
      {avatarUrl ? (
        <>
          <img
            src={avatarUrl}
            onError={(e) => {
              //e.target.style.display = 'none';
              e.target.className = "hidden error-image";
            }}
            className="w-11 h-11 rounded-full overflow-hidden object-cover"
          />

          <div className="name-firstcase w-11 h-11 rounded-full overflow-hidden bg-gray-200 flex text-xl font-bold text-gray-500 items-center justify-center">{oneCharName(showName)}</div>
        </>
      ) : (
        <img
          src="https://static.mservice.io/next-js/_next/static/public/cinema/momo-avatar.svg"
          className="w-11 h-11 rounded-full overflow-hidden "
        />
      )}

      <style jsx global>{`

          .name-firstcase{
              display: none;
          }
          .error-image + .name-firstcase{
            display: flex;
          }

      `}</style>
    </>
  );
};

export default UserAvatar;

