import Link from "next/link";

import { useEffect, useState } from "react";

import { useDebouncedCallback } from "use-debounce";

import MovieBookingDate from "@/components/cinema/MovieBookingDate";

import Loader from "@/components/ui/Loader";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import MovieLabel from "@/components/cinema/MovieLabel";

import MovieType from "@/components/cinema/MovieType";

import MovieBookingTime from "@/components/cinema/MovieBookingTime";

import Image from "next/image";

import { axios } from "@/lib/index";

import { useSelector, useDispatch } from "react-redux";

const MovieBookingList = ({ isMobile, type = null }) => {
  const filmStore = useSelector((state) => state.filmBooking);
  const rapId = filmStore.rapId ?? null;

  const showDay = filmStore.showDay ?? null;

  const [filmsList, setFilmsList] = useState(null);

  const [loading, setLoading] = useState(false);

  const debouncedFetch = useDebouncedCallback((query) => {
    axios
      .get(`/ci-cinema/session/${query}`)
      .then(function (response) {
        const res = response.Data;
        setLoading(false);
        if (res) {
          setFilmsList(res);
        }
      })
      .catch(function (error) {
        setFilmsList(null);
        return;
      });
  }, 500);

  const getQuery = () => {
    let query = "";
    query += `${rapId}`;
    if (showDay) {
      query += `?&date=${showDay}`;
    }
    return query;
  };

  const [rapBooking, setRapBooking] = useState(null);

  useEffect(() => {
    if (rapId == null) {
      return;
    }

    setLoading(true);
    debouncedFetch(getQuery());
  }, [rapId, showDay]);

  if (rapId == null || !rapId) return <></>;

  if (filmsList == null) {
    return (
      <NotFoundCinema
      title="Úi, Suất chiếu không tìm thấy."
      content="Bạn hãy thử tìm ngày khác nhé"
    />
    );
  }


  return (
    <>
        <div className="box-nav  relative z-10 border-b border-gray-200 bg-white py-3">
          {filmsList && (
            <div className="">
              <MovieBookingDate
                data={filmsList.ShowTimes}
                rapID={rapId}
                onSelect={(e) => handleSelectDay(e)}
              />
            </div>
          )}
        </div>

        <div className="booking-list-height relative">
          {loading && (
            <div className="z-1 fadeIn absolute top-0 left-0 flex h-full w-full justify-center bg-white bg-opacity-80 pt-20">
              <Loader />
            </div>
          )}

          {filmsList && (
            <div className="normal-accordion divide-y divide-gray-200">
              {filmsList.Films &&
                filmsList.Films.map((item, index) => (
                  <div key={item.Id} className="grid ">
                    <div className="block w-full px-4 py-4 text-left">
                      <div className="film-show grid gap-y-0  gap-x-4 md:gap-x-6">
                        <div className="col-start-1 row-span-2 row-start-1 ">
                          <Link href={item.Link}>
                            <a className="group block ">
                              <div className="background-gray-100  relative overflow-hidden rounded">
                                <div className="flex bg-gray-200">
                                  <Image
                                    src={item.GraphicUrl}
                                    alt={item.TitleEn}
                                    layout="intrinsic"
                                    width={200}
                                    height={300}
                                    className="scale-100 transition-transform  duration-300 group-hover:scale-105 "
                                  />
                                </div>
                              </div>
                            </a>
                          </Link>
                        </div>

                        <div className="col-start-2 ">
                          <div className="mb-1 flex origin-left scale-90 flex-row  flex-nowrap space-x-2">
                            <MovieLabel data={item.ApiRating} />
                          </div>

                          <div className="font-semibold leading-tight text-gray-800">
                            <Link href={item.Link}>
                              <a className="">{item.Title}</a>
                            </Link>
                          </div>

                          <div className="text-tiny mt-1 leading-tight text-gray-400">
                            {item.ApiGenreName}
                          </div>
                        </div>

                        <div className=" col-span-2 col-start-1 md:col-start-2">
                          {item.VersionsCaptions.map((child, index) => (
                            <div key={index} className="pt-4 pb-4">
                              <div className="mb-2 text-sm font-semibold ">
                                <MovieType
                                  vtype={child.VersionType}
                                  ctype={child.CaptionType}
                                />
                              </div>
                              <div className="grid grid-cols-3 gap-3 sm:grid-cols-4 ">
                                {child.ShowTimes.map((i, index) => (
                                  // <MovieBookingTime
                                  //   text={i.ShowTimeDuration}
                                  // />

                                  <MovieBookingTime
                                    gioChieu={i}
                                    key={index}
                                  />
                                ))}
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          )}

          {filmsList &&
            filmsList.Films != null &&
            filmsList.Films.length == 0 && (
              <NotFoundCinema
              title="Úi, Suất chiếu không tìm thấy."
                content="Bạn hãy thử tìm ngày khác nhé"
              />
            )}
        </div>

      <style jsx global>
        {`
          .box-nav {
            box-shadow: 0 2px 5px 0 rgb(0, 0, 0, 0.05);
          }
          .booking-list-height {
            min-height: 240px;
          }

          .film-show {
            grid-template: auto 1fr / 120px minmax(0, 1fr);
          }
          @media screen and (max-width: 768px) {
            .film-show {
              grid-template: auto 1fr / 70px minmax(0, 1fr);
            }
          }
        `}
      </style>
    </>
  );
};
export default MovieBookingList;
