import MovieBookingDate from "@/components/cinema/MovieBookingDate";
import MovieBookingLocation from "@/components/cinema/MovieBookingLocation";
import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import Loader from "@/components/ui/Loader";

import { useSelector, useDispatch } from "react-redux";

import { axios } from "@/lib/index";

import { useEffect, useState, useRef } from "react";
import { useDebouncedCallback } from "use-debounce";

import RapBookingList from "@/components/cinema/RapBookingList";

const MovieBooking = ({ filmIdApi, master, isMobile }) => {
  const dispatch = useDispatch();

  const filmStore = useSelector((state) => state.filmBooking);

  const { cityId, isNearby, rapBrand, showDay, geo } = filmStore;

  const [rapList, setRapList] = useState(null);

  const [listShowDay, setListShowDay] = useState(null);

  const [loading, setLoading] = useState(false);

  const debouncedFetch = useDebouncedCallback((filmIdApi, query) => {
    axios
      .get(`/ci-film/session/${filmIdApi}?${query}`)
      .then(function (response) {
        if (!response.Result) {
          setRapList(null);
          setListShowDay(null);
          return;
        }
        const result = response.Data;
        setLoading(false);
        setRapList(result);
        result.ShowTimes && setListShowDay(result.ShowTimes);
      })
      .catch(function (error) {
        setNotFound(true);
        return;
      });
  }, 500);

  const getQuery = () => {
    let query = "";

    if (isNearby == true) {
      query += `&lat=${geo.lat}&lon=${geo.lng}&sortType=4&sortDir=1`;
    } else {
      query += `&sortType=1&sortDir=1`;
    }

    if (rapBrand != null) {
      query += `&cineplex=${rapBrand}`;
    }

    if (cityId != null) {
      query += `&apiCityId=${cityId}`;
    }

    if (showDay != null) {
      query += `&date=${showDay}`;
    }
    return query;
  };

  useEffect(() => {
    if (filmIdApi == null) {
      return;
    }

    let query = getQuery();

    setLoading(true);
    debouncedFetch(filmIdApi, query);
  }, [cityId, isNearby, rapBrand, showDay, geo, filmIdApi]);


  const [notFound, setNotFound] = useState(false);

  const boxId = useRef(null);

  if (!filmIdApi) return null;

  return (
    <>
      {notFound && (
        <div className="boder-gray-200 rounded border pb-5">
          <NotFoundCinema
            title="Úi, phim đã hết suất chiếu."
            content="Bạn hãy thử lại với phim khác hoặc rạp khác nha !"
            button="Phim đang chiếu"
            link="/cinema/#phimdangchieu"
          />
        </div>
      )}

      <div
        className={`z-10 sm:absolute sm:right-0 sm:-top-12 ${
          notFound ? "hidden" : ""
        }`}
      >
        <MovieBookingLocation data={master.Cities} />
      </div>

      <div
        className={`rounded md:border md:border-gray-200  ${
          notFound ? "hidden" : ""
        }`}
      >
        <div className="relative z-20 -mx-5 bg-white pt-2 md:mx-0">
          {listShowDay && (
            <div className="border-b border-gray-100 px-0  pb-2 ">
              <MovieBookingDate data={listShowDay} />
            </div>
          )}
        </div>

        {rapList && (
          <div
            className="box-nav top-sticky-menu cinema-scroll-margin sticky z-20 -mx-5 border-b border-gray-200  bg-white md:top-0 md:mx-0 lg:relative"
            ref={boxId}
          >
            <div className="pt-3 pb-2 ">
              <MovieBookingRap data={master.Cineplexs} />
            </div>
          </div>
        )}

        <div className="booking-list-height relative">
          {loading && (
            <div className="z-12 fadeIn absolute top-0 left-0 flex h-full w-full justify-center bg-white bg-opacity-80 pt-20">
              <Loader />
            </div>
          )}
          {rapList && (
            <RapBookingList rapList={rapList?.Cinemas?.Items ?? null} />
          )}

          {rapList &&
            rapList.Cinemas != null &&
            rapList.Cinemas.Items.length == 0 && (
              <NotFoundCinema
                title="Úi, hôm nay chưa có suất chiếu."
                content="Bạn hãy thử tìm ngày khác nhé"
              />
            )}
        </div>
      </div>

      <style jsx>{`
        .box-nav {
          box-shadow: 0 2px 5px 0 rgb(0 0 0 / 5%);
        }

        .booking-list-height {
          min-height: 240px;
        }
      `}</style>
    </>
  );
};
export default MovieBooking;
