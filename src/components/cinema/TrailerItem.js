import Link from "next/link";

import { useEffect, useState } from "react";

import Image from "next/image";

// import videojs from 'video.js';
// import videojs from "https://vjs.zencdn.net/7.10.2/video.min.js";

// import { LightGallerySettings } from 'lightgallery/lg-settings';

// import lgAutoplay from "lightgallery/plugins/autoplay";

const MovieItem = ({ data }) => {
  const { Title, BannerUrl, genreName = "Gây Cấn, Hành Động" } = data;

  const convertGenre = genreName.split(", ").slice(0, 2).join(", ");
  return (
    <>
      <div className="relative z-10  cursor-pointer group">
        <div className="absolute z-10 w-10 h-10 pt-2 pb-2 pl-3 pr-2 transition-transform  bg-black border-2 border-white rounded-full border-opacity-70 bottom-3 left-3 group-hover:scale-110 bg-opacity-40">

          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 31" className="w-full h-full" style={{ fill: "white" }}>
            <path d="M22.79 17.02L3.934 29.436a2.327 2.327 0 01-3.607-1.944V2.662A2.327 2.327 0 013.934.718L22.79 13.133a2.327 2.327 0 010 3.888v-.001z" />
          </svg>
        </div>
        <div className="overflow-hidden rounded flex" >
          <Image
            src={BannerUrl}
            alt={Title}
            layout="intrinsic"
            objectFit="cover"
            width={400}
            height={225}
            className="transition-transform duration-300  scale-100 group-hover:scale-105 bg-gray-200"
          />
        </div>
      </div>

      <div className="mt-4">
        <div className="font-semibold leading-tight text-gray-800">
          <div className="text-white text-opacity-90">{Title}</div>
        </div>
        <div className="mt-1 leading-tight text-white text-tiny text-opacity-60">
          {convertGenre}
        </div>
      </div>
    </>
  );
};

export default MovieItem;

