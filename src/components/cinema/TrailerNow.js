
import dynamic from "next/dynamic";

const Desktop = dynamic(() => import("../../components/cinema/TrailerNowDesktop"));
const Mobile = dynamic(() => import("../../components/cinema/TrailerNowMobile"));

const TrailerNow = (props) => {
  return (
    <>
      {props.isMobile == true ? (
        <Mobile data={props.data} />
      ) : (
        <Desktop data={props.data} />
      )}
    </>
  );
};

export default TrailerNow;
