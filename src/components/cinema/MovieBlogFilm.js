import Link from "next/link";

import { useEffect, useState } from "react";


import { axios } from "@/lib/index";
import ViewFormat from "@/lib/view-format";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

import BlogAjaxLoad from "@/components/ui/BlogAjaxLoad";


import Image from "next/image";
const MovieBlogFilm = ({ blogId }) => {

  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  const [blog, setBlog] = useState(null);

  useEffect(() => {
    axios
      .get(`/blog/base/${blogId}`)
      .then(function (res) {
        setBlog(res.Data);
      });
  }, [blogId]);


  return (
    <>
    {blog &&
      <section className="py-8 border-t border-gray-200" key={blog.Id}>
        <h3 className="text-xl font-bold mb-4">Review phim</h3>
        <div className="overflow-hidden rounded  md:rounded-lg relative z-1 group">
          {/* <Link href={item.Url ?? "/"}>
            <a className=" absolute inset-0 z-2 cursor-pointer  bg-gray-900 bg-opacity-0 transition-colors group-hover:bg-opacity-10" target="_blank">
            </a>
          </Link> */}

          <div className=" absolute inset-0 z-2 cursor-pointer  bg-gray-900 bg-opacity-0 transition-colors group-hover:bg-opacity-10 " onClick={() => setModal(true)}>

          </div>

          <div className="relative h-52 sm:h-60">
            <Image
              src={blog.Avatar}
              alt={blog.Title}
              layout="fill"
              className=" object-cover"
            />
          </div>


          <div className="absolute pl-4 pr-5 pt-4 pb-4 md:pl-5 md:pr-20 md:pb-4 md:pt-6 left-0 right-0 bottom-0 z-1 bg-gradient-to-t from-black  to-transparent">
            {/* <div className="text-tiny font-semibold mb-0 text-gray-200 inline-block relative z-2">Phim ảnh</div> */}

            <div className="text-xs text-gray-300 mb-1">
              {blog.DateShow && <>{blog.DateShow} - </>}
              {/* {blog.DateShow && <>{format(parseISO(blog.DateShow), "dd/MM/yyyy")} - </>} */}
              <ViewFormat data={blog.TotalViews} /> lượt xem
            </div>

            <div className="text-white font-inter  md:text-lg font-bold  leading-tight group-hover:underline">{blog.Title}</div>


          </div>
        </div>

        <Modal isOpen={modal} onDismiss={close} isFull={true} isBig={true}>
          <ModalHeader>Review Phim</ModalHeader>

          <ModalBody css="p-0 bg-white rounded">
            <div className="p-5 md:p-7">
              <BlogAjaxLoad idBlog={blog.Id} />
            </div>
          </ModalBody>
          <ModalFooter className="flex">
            <div>
              <a
                onClick={close}
                className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
              >
                Đóng
              </a>
            </div>
          </ModalFooter>
        </Modal>
      </section>
    }
    </>
  );
};

export default MovieBlogFilm;
