import Link from "next/link";

import { useState, useEffect, useRef, useCallback } from "react";
import RatingView from "@/components/RatingView";

import Image from "next/image";


const RapBrandList = ({ dataCineplexList }) => {

  return (
    <div className="grid grid-cols-1 gap-4 md:gap-4 md:grid-cols-2 lg:grid-cols-2">
      {dataCineplexList.Items.filter((item) => item.Id != -1).map(
        (item, index) => (
          <Link href={item.Link} key={item.Id}>
            <a className=" group">
              <div
                key={item.Id}
                className="flex h-full items-center px-2 py-3 bg-white border border-gray-200 rounded-lg shadow-sm group-hover:border-gray-300 group-hover:shadow-md md:px-4"
              >
                <div className=" w-14 h-14 lg:w-20 lg:h-20  shrink-0">

                  <Image
                    src={item.Logo ?? ''}
                    alt={item.Name}
                    layout="intrinsic"
                    width={80}
                    height={80}
                  />

                </div>
                
                <div className="pl-4 min-w-0 flex-1 ">
                  <div className="font-bold text-pink-600 group-hover:text-pink-500 text-base md:text-lg">
                    {item.Name}
                  </div>
                  {item.Slogan && (
                    <div className=" text-sm text-gray-400 line-clamp-1">
                      {item.Slogan}
                    </div>
                  )}

                  {item.FeaturedNote && (
                    <div className="mb-0 text-sm text-pink-600 flex items-center mt-1 ">
                      <div className="pr-1">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-4 w-4  rotate-90"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"
                          />
                        </svg>
                      </div>
                      <div className="leading-tight ">
                        {item.FeaturedNote}
                      </div>
                    </div>
                  )}

                  {item.RatingValue && (
                    <div className="mt-1 flex items-center">
                      <RatingView
                        data={item.RatingValue}
                        isSmall={true}
                      />
                      <div className="ml-1 text-tiny text-gray-500 flex items-center">
                        <div>{item.RatingCount} đánh giá</div>
                      </div>
                    </div>
                  )}

                  <div className="text-sm text-gray-900 text-opacity-50 mt-1 flex items-center">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-4 w-4 inline-block mr-1"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                    <div>{item.TotalCinemas} rạp</div>
                  </div>
                </div>
              </div>
            </a>
          </Link>
        )
      )}
    </div>
  );
};

export default RapBrandList;
