import Link from "next/link";

import { useEffect, useState } from "react";

const MovieLabel = ({ data }) => {
  const color = {
    P: "cinema-rate-green",
    C13: "cinema-rate-yellow",
    C16: "cinema-rate-orange",
    C18: "cinema-rate-red",
  };

  if (!color[data]) return <></>;
  return (
    <>
      <div
        className={`${color[data]} bg-opacity-80 text-white text-opacity-95 text-xs font-semibold px-2 h-5  items-center rounded-sm text-center inline-flex `}
      >
        {data}
      </div>

      <style jsx global>{`
        .cinema-rate-green {
          background-color: #579b41;
        }

        .cinema-rate-yellow {
          background-color: #ddbc3f;
        }

        .cinema-rate-orange {
          background-color: #e88021;
        }

        .cinema-rate-red {
          background-color: #9b2020;
        }
      `}</style>
    </>
  );
};

export default MovieLabel;
