import { useEffect, useState } from "react";

import SwiperCore, { Navigation } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Navigation]);

import dynamic from "next/dynamic";
const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import lgVideo from "lightgallery/plugins/video";
import lgThumbnail from "lightgallery/plugins/thumbnail";

import TrailerItem from "./TrailerItem";

let lightGallery = null;

const TrailerNow = ({ data }) => {
  const [swiperObject, setSwiperObject] = useState(null);
  const [swiperLeft, setSwiperLeft] = useState(false);
  const [swiperRight, setSwiperRight] = useState(false);


  const clickRight = () => {
    swiperObject.slideNext();
  };
  const clickLeft = () => {
    swiperObject.slidePrev();
  };

  const slideChange = () => {
    swiperObject.isEnd ? setSwiperRight(false) : setSwiperRight(true);
    swiperObject.isBeginning ? setSwiperLeft(false) : setSwiperLeft(true);
  };

  useEffect(() => {
    if (swiperObject) {
      slideChange();
    }
  }, [swiperObject]);

  const onInitGallery = (e) => {
    lightGallery = e.instance;
  };

  const onOpenGallery = (number) => {
    lightGallery.openGallery(number);
  };

  let video = [];

  data.map((item, index) => {
    let hmm = {
      video: {
        source: [
          {
            src: item.ApiAutoplayTrailer,
            type: "video/mp4",
          },
        ],
        attributes: {
          preload: true,
          controls: true,
        },
      },
      src:item.TrailerUrl,
      thumb: item.BannerUrl,
      poster: item.BannerUrl,
      subHtml: `<h4>${item.Title}</h4>`,
    };
    video.push(hmm);
  });

  return (
    <div className="relative swiper-cinema">
      <Swiper
        spaceBetween={20}
        slidesPerView={4}
        speed={700}
        threshold="10px"
        slidesPerGroup={4}
        onSwiper={setSwiperObject}
        onSlideChange={() => slideChange()}
      >
        {data.map((item, index) => (
          <SwiperSlide key={item.Id} onClick={() => onOpenGallery(index)}>
            <TrailerItem data={item} />
          </SwiperSlide>
        ))}
      </Swiper>

      {swiperObject && swiperRight && (
        <div
          onClick={() => clickRight()}
          className="absolute z-20 flex items-center justify-center text-black transition-all  -translate-y-1/2 bg-white border border-gray-200 rounded-full shadow cursor-pointer button-next button-swiper w-11 h-11 -right-6 top-1/2 hover:opacity-90"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 5l7 7-7 7"
            />
          </svg>
        </div>
      )}
      {swiperObject && swiperLeft && (
        <div
          onClick={() => clickLeft()}
          className="absolute z-20 flex items-center justify-center text-black transition-all  -translate-y-1/2 bg-white border border-gray-200 rounded-full shadow cursor-pointer button-prev button-swiper w-11 h-11 -left-6 top-1/2 hover:opacity-90"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 19l-7-7 7-7"
            />
          </svg>
        </div>
      )}

      <LightGallery
        speed={500}
        autoplayFirstVideo={true}
        onInit={onInitGallery}
        dynamic={true}
        dynamicEl={video}
        download={false}
        // onInit={onInitGallery}
        plugins={[lgVideo, lgThumbnail]}
      />

      <style jsx>{`
        .swiper-cinema :global(.button-prev) {
          top: 40%;
        }
        .swiper-cinema :global(.button-next) {
          top: 40%;
        }
      `}</style>
    </div>
  );
};

export default TrailerNow;
