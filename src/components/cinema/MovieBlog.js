import Link from "next/link";

import { useEffect, useState } from "react";

import BlogListAjax from "@/components/ui/BlogListAjax";

import { axios } from "@/lib/index";

const MovieBlog = ({ data, isMobile = false }) => {
  const dataFilmsBlog = data.Data.find(
    (item, index) => item.TypeName == "ProjectBlogs"
  );

  if (!dataFilmsBlog || dataFilmsBlog.Data.Projects[0] == undefined) {
    return null;
  }

  const List = dataFilmsBlog.Data.Projects[0];

  return (
    <>
      <BlogListAjax item={List} isNumber={3} isMobile={isMobile} />
    </>
  );
};

export default MovieBlog;
