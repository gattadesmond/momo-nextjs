import SwiperCore, { Navigation } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";
import { getDate, getDay, parseISO } from "date-fns";
// install Swiper modules
SwiperCore.use([Navigation]);

import { useEffect, useState } from "react";


import { useSelector, useDispatch } from "react-redux";
import { setShowDay } from "@/slices/filmBooking";


const DayConvert = ({ day, date }) => {
  var today = new Date();
  var inputDate = new Date(date);
  if (inputDate.setHours(0, 0, 0, 0) == today.setHours(0, 0, 0, 0)) {
    return "Hôm nay";
  }

  if (day == 0) return "Chủ nhật";
  if (day == 6) return "Thứ 7";
  return `Thứ ${day + 1}`;
};

const MovieBookingDate = ({ data, onSelect, filmId, rapID }) => {
  const dispatch = useDispatch();

  
  const [convertDate, setConvertDate] = useState(null);

  const [swiperDate, setSwiperDate] = useState(null);

  const [selectedDate, setSelectedDate] = useState(0); //Active tab index

  useEffect(() => {
    const changeDate = data.map((item, index) => ({
      id: index,
      date: getDate(parseISO(item)),
      day: getDay(parseISO(item)),
      original: item.split(" ")[0],
    }));
    setConvertDate(changeDate);

  

  }, [data]);



  useEffect(() => {
    // gallerySwiperRef.current.swiper.slideTo(4);
    if (swiperDate != null) {
      swiperDate.slideTo(selectedDate - 2);
    }
  }, [selectedDate]);

  useEffect(() => {
    

    return function clean() {
      setSelectedDate(0);

      dispatch(setShowDay(null));
    };
  }, [rapID])



  const handleSelectDate = (number, ori) => {

    dispatch(setShowDay(ori || 0));

    setSelectedDate(number);

  };

  return (
    <>
      <Swiper
        spaceBetween={8}
        slidesPerView={"auto"}
        onSwiper={setSwiperDate}
        className="px-5"
        navigation={{
          nextEl: ".button-next",
          prevEl: ".button-prev",
        }}
      >
        {convertDate &&
          convertDate.map((item, index) => (
            <SwiperSlide
              className={`w-16 ${item.id == selectedDate ? "is-active" : ""}`}
              onClick={() => handleSelectDate(item.id, item.original)}
              key={item.id}
            >
              <div
                className={`w-16 py-0  bg-white overflow-hidden  rounded border   text-center cursor-pointer transition-all  ${item.id == selectedDate
                  ? "border-pink-700"
                  : "border-gray-300 hover:border-gray-400 "
                  }`}
              >
                <div
                  className={`mx-auto py-1 font-semibold text-lg  justify-center  ${item.id == selectedDate
                    ? "bg-pink-600 text-white"
                    : "bg-gray-100 "
                    }`}
                >
                  {item.date}
                </div>
                <div
                  className={`text-xs text-nowrap  h-6 flex items-center justify-center ${item.id == selectedDate ? "text-pink-600" : "text-gray-400 "
                    }`}
                >
                  <DayConvert day={item.day} date={item.original} />
                </div>
              </div>
            </SwiperSlide>
          ))}

        <div className="absolute top-0 right-0 z-10 flex items-center justify-end w-12 h-full pr-1 text-gray-500 cursor-pointer button-next button-swiper ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 5l7 7-7 7"
            />
          </svg>
        </div>

        <div className="absolute top-0 left-0 z-10 flex items-center w-12 h-full pl-1 text-gray-500 cursor-pointer button-prev button-swiper ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 19l-7-7 7-7"
            />
          </svg>
        </div>
      </Swiper>

      <style jsx>{`
        .button-next {
          background: linear-gradient(
            90deg,
            rgba(255, 255, 255, 0) 0,
            rgba(255, 255, 255, 0.9) 30%,
            rgba(255, 255, 255, 1) 50%,
            #fff 100%
          );
        }

        .button-prev {
          background: linear-gradient(
            -90deg,
            rgba(255, 255, 255, 0) 0,
            rgba(255, 255, 255, 0.9) 30%,
            rgba(255, 255, 255, 1) 50%,
            #fff 100%
          );
        }
        .button-swiper.swiper-button-disabled {
          display: none;
        }
      `}</style>
    </>
  );
};

export default MovieBookingDate;
