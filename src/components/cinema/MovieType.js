const MovieType = ({ vtype, ctype }) => {
  const VersionList = {
    "-1": "",
    1: "Digital ",
    2: "2D ",
    3: "3D ",
    4: " ",
    5: "IMAX ",
  };
  let name = "";

  if (vtype) {
    name = name.concat(VersionList[parseInt(vtype)]);
  }

  const CaptionList = {
    "-1": "",
    1: "Lồng tiếng",
    2: "Phụ đề",
    3: "",
  };

  if (ctype) {
    name = name.concat(CaptionList[parseInt(ctype)]);
  }
  return name;
};

export default MovieType;
