
import { useEffect, useState } from "react";
import SwiperCore, { Navigation } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

import MovieItemGenre from "./MovieItemGenre";


const MovieGenreFeature = ({ data, isDark = false }) => {
  const [swiperObject, setSwiperObject] = useState(null);
  const [swiperLeft, setSwiperLeft] = useState(false);
  const [swiperRight, setSwiperRight] = useState(false);

  const clickRight = () => {
    swiperObject.slideNext();
  };
  const clickLeft = () => {
    swiperObject.slidePrev();
  };

  const slideChange = () => {
    swiperObject.isEnd ? setSwiperRight(false) : setSwiperRight(true);
    swiperObject.isBeginning ? setSwiperLeft(false) : setSwiperLeft(true);
  };

  useEffect(() => {
    if (swiperObject) {
      slideChange();
    }
  }, [swiperObject]);

  return (
    <div className="relative swiper-cinema">
      <Swiper
        spaceBetween={20}
        slidesPerView={1}
        speed={700}
        threshold="10px"
        slidesPerGroup={1}
        onSwiper={setSwiperObject}
        onSlideChange={() => slideChange()}
        breakpoints={{
          "640": {
            "slidesPerView": "auto",
            "spaceBetween": 20,
            "slidesPerGroup":3
          }
        }}
        
      >
        {data.map((item, index) => (
          <SwiperSlide key={index} className="h-auto w-[420px]">
            <>
              <MovieItemGenre data={item} key={item.id} isDark={isDark} isNumber={index + 1} />
            </>
          </SwiperSlide>
        ))}
      </Swiper>
      {swiperObject && swiperRight && (
        <div
          onClick={() => clickRight()}
          className="absolute z-20 flex items-center justify-center text-black transition-all  -translate-y-1/2 bg-white border border-gray-200 rounded-full shadow cursor-pointer button-next button-swiper w-11 h-11 -right-6 top-1/2 hover:opacity-90"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 5l7 7-7 7"
            />
          </svg>
        </div>
      )}
      {swiperObject && swiperLeft && (
        <div
          onClick={() => clickLeft()}
          className="absolute z-20 flex items-center justify-center text-black transition-all  -translate-y-1/2 bg-white border border-gray-200 rounded-full shadow cursor-pointer button-prev button-swiper w-11 h-11 -left-6 top-1/2 hover:opacity-90"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-6 h-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 19l-7-7 7-7"
            />
          </svg>
        </div>
      )}

      <style jsx>{`
        .swiper-cinema :global(.button-prev) {
          top: 50%;
        }
        .swiper-cinema :global(.button-next) {
          top: 50%;
        }
      `}</style>
    </div>
  );
};

export default MovieGenreFeature;
