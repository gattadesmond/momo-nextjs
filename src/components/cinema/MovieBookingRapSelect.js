import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

import { useSelector } from "react-redux";

import RapRowItem from "@/components/cinema/RapRowItem";

const MovieBookingRapSelect = ({ data, sCineplex = null }) => {
  const filmStore = useSelector((state) => state.filmBooking);

  const { rapId } = filmStore;

  const [rapChieu, setRapChieu] = useState(null);

  useEffect(() => {
    if (!rapId) return;
    axios
      .get(`/ci-cinema/detail-by-apiCinemaId/${rapId}`)
      .then(function (response) {
        const res = response.Data ?? null;

        if (res) {
          setRapChieu(res);
        } else {
          setRapChieu(null);
        }
      });

    return function cleanup() {
      setRapChieu(null);
    };
  }, [rapId]);

  // if (!rapId) return <></>;

  return (
    <>
      <div className=" h-[65px] bg-gray-50 px-4 pb-2.5 pt-2.5  flex items-center ">
        {rapChieu && <RapRowItem data={rapChieu} type={4} />}
      </div>
    </>
  );
};

export default MovieBookingRapSelect;
