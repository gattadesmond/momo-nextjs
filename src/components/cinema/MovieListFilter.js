import { useEffect, useState } from "react";

import { axios } from "@/lib/index";
import MovieItem from "@/components/cinema/MovieItem";

import { useDebouncedCallback } from "use-debounce";

import Pagination from 'rc-pagination';

import { Menu, MenuList, MenuButton, MenuItem, MenuItems, MenuPopover } from "@reach/menu-button";


import Loader from "@/components/ui/Loader";
import { useGetFilmsQuery } from '@/slices/services/getFilms';

const filterYear = [
  {
    Id: 0,
    Name: "Tất cả",
  },
  {
    Id: 2019,
    Name: "Năm 2019",
  },
  {
    Id: 2020,
    Name: "Năm 2020",
  },
  {
    Id: 2021,
    Name: "Năm 2021",
  },
];


const MovieListFilter = ({ dataFilmsSoon, dataCinemaMaster, scrollRef, headingContent = false, countryId = null, theloaiId = null }) => {


  const [theLoai, setTheLoai] = useState(null);
  const [quocGia, setQuocGia] = useState(null);

  const [selectTheLoai, setSelectTheLoai] = useState(null);
  const [selectQuocGia, setSelectQuocGia] = useState(null);

  const [stext, setStext] = useState("");

  const [selectYear, setSelectYear] = useState(null);

  const [skip, setSkip] = useState(true);

  const [loading, setLoading] = useState(false);

  const [current, setCurrent] = useState(1);


  useEffect(() => {
    if (countryId && !theloaiId) {
      setQuocGia(dataCinemaMaster?.Data?.Countries ? dataCinemaMaster?.Data?.Countries.filter((item) => item.Id == countryId) : null);

      axios
        .get(`/ci-film/params?countryId=${countryId}`)
        .then(function (response) {
          const res = response.Data ?? null;
          if (!res) return;

          const filterRes = res?.Categories?.filter((item) => item.Enabled == true);
          setTheLoai(filterRes ?
            [{
              Id: 0,
              Name: "Tất cả",
              Enabled: true,
            }, ...filterRes]
            : null);
        });
    }

    if (theloaiId && !countryId) {
      setTheLoai(dataCinemaMaster?.Data?.Categories ? dataCinemaMaster?.Data?.Categories.filter((item) => item.Id == theloaiId) : null);
      axios
        .get(`/ci-film/params?projectIds=${theloaiId}`)
        .then(function (response) {
          const res = response.Data ?? null;
          if (!res) return;
          const filterRes = res?.Countries?.filter((item) => item.Enabled == true);
          setQuocGia(filterRes ? [{
            Id: 0,
            Name: "Tất cả",
            Enabled: true,
          }, ...res?.Categories] : null);
        });
    }

    if (!theloaiId && !countryId) {
      setQuocGia(dataCinemaMaster?.Data?.Countries ? [{
        Id: 0,
        Name: "Tất cả",
        Enabled: true,
      }, ...dataCinemaMaster?.Data?.Countries] : null);

      setTheLoai(dataCinemaMaster?.Data?.Categories ? [{
        Id: 0,
        Name: "Tất cả",
        Enabled: true,
      }, ...dataCinemaMaster?.Data?.Categories] : null);
    }

    if (theloaiId && countryId) {
      setQuocGia(dataCinemaMaster?.Data?.Countries ? dataCinemaMaster?.Data?.Countries.filter((item) => item.Id == countryId) : null);
      setTheLoai(dataCinemaMaster?.Data?.Categories ? dataCinemaMaster?.Data?.Categories.filter((item) => item.Id == theloaiId) : null);
    }
    setSelectYear(filterYear[0]);

  }, []);

  useEffect(() => {
    if (theLoai) {
      setSelectTheLoai(theLoai[0]);
    }
  }, [theLoai]);

  useEffect(() => {
    if (quocGia) {
      setSelectQuocGia(quocGia[0]);
    }
  }, [quocGia]);

  useEffect(() => {
    if (theLoai && quocGia) {
      setSkip(false);
    }
  }, [theLoai, quocGia]);


  const { data: filmsData, error, isLoading } = useGetFilmsQuery({ "current": current - 1, "quocgia": selectQuocGia?.Id, "theloai": selectTheLoai?.Id, "query": stext, "year": selectYear?.Id }, { skip, });


  const fetchData = useDebouncedCallback(
    () => {
      setSkip(false);

      setLoading(false);
    },
    // delay in ms
    500
  );

  const handleSelectFilter = (item, type) => {

    setCurrent(1);

    if (type == "theloai") {
      setSelectTheLoai(item);
    }
    if (type == "quocgia") {
      setSelectQuocGia(item);
    }

    if (type == "year") {
      setSelectYear(item);
    }

    setLoading(true);
    setSkip(true);
    fetchData();
  }

  const onChange = (page) => {

    if (scrollRef.current) {
      scrollRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
    };

    setLoading(true);

    setSkip(true);
    fetchData();
    setCurrent(page);
  }

  const itemRender = (current, type, element) => {
    if (type === 'page') {
      return <button className="flex items-center justify-center w-8 h-8 rounded select-none focus:outline-none pointer-events-none bg-gray-100 border border-gray-300  text-gray-700 ">{current}</button>;
    }

    if (type === 'prev') {
      return <button className="flex items-center justify-center w-8 h-8 rounded select-none focus:outline-none pointer-events-none bg-gray-100 border border-gray-300 text-gray-700 shadow-sm">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
        </svg>
      </button>;
    }
    if (type === 'next') {
      return <button className="flex items-center justify-center w-8 h-8 rounded select-none focus:outline-none pointer-events-none bg-gray-100 border border-gray-300 text-gray-700 shadow-sm">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
        </svg>
      </button>;
    }

    if (type === 'jump-prev') {
      return <button className="flex items-center justify-center w-8 h-8 rounded select-none focus:outline-none pointer-events-none bg-gray-100 border border-gray-300 text-gray-500 shadow-sm">
        ...
      </button>;
    }

    if (type === 'jump-next') {
      return <button className="flex items-center justify-center w-8 h-8 rounded select-none focus:outline-none pointer-events-none bg-gray-100 border border-gray-300 text-gray-500 shadow-sm">
        ...
      </button>;
    }

    return element;
  };



  const debouncedSearchQuery = useDebouncedCallback(
    (value) => {
      setCurrent(1);
      setStext(value);
      setLoading(true);
      setSkip(true);
      fetchData();
    },
    600
  );



  return (
    <>
      <div className="lg:flex items-center mb-5  md:mb-5" >
        {headingContent && <div className="py-2 flex-1">
          <h2 className="text-2xl text-center lg:text-left lg:text-2xl font-bold  text-pink-600 flex-1">{headingContent}</h2>
        </div>}


        <div className="-mx-5 md:mx-0 py-0 md:py-2  shrink-0">

          <div className="flex items-center  justify-center  w-full   flex-wrap  md:pl-0 ">
            {!theloaiId &&
              <div className="relative p-1 md:p-2">
                <Menu>
                  <MenuButton className="flex items-center rounded px-2 md:px-3 py-1.5 text-left cursor-pointer focus:outline-none whitespace-nowrap leading-5 border-gray-300 border hover:bg-gray-50 active:bg-gray-100  transition-colors text-sm min-w-[120px]">


                    <span className="flex-1">
                      {selectTheLoai?.Id == 0 ? "Thể loại" : selectTheLoai?.Name}
                    </span>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-500 ml-2 icon transition-transform" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                    </svg>
                  </MenuButton>

                  <MenuPopover className="dropdown-filter-mobile fadeInDown  min-w-[375px] absolute z-10 mt-1 rounded-lg p-2 overflow-auto focus:outline-none shadow-level3 bg-white text-sm ">
                    <div className="arbitrary-element">
                      <MenuItems className="grid grid-cols-3 gap-x-2 gap-y-1">
                        {theLoai && theLoai.map((item, index) =>
                          <MenuItem key={item.Id} className={`cursor-pointer select-none relative py-1.5 pl-2 pr-8 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded ${item.Id == selectTheLoai?.Id ? "bg-gray-100 pointer-events-none" : ""}`}
                            onSelect={() => handleSelectFilter(item, "theloai")}>  {item.Name} <span className="absolute right-1">
                              {item.Id == selectTheLoai?.Id && <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-pink-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                              </svg>}

                            </span>
                          </MenuItem>
                        )}

                      </MenuItems>
                    </div>

                  </MenuPopover>


                </Menu>

              </div>}
            {!countryId && <div className="relative p-1 md:p-2">
              <Menu>
                <MenuButton className="flex items-center rounded px-2 md:px-3 py-1.5 text-left cursor-pointer focus:outline-none whitespace-nowrap leading-5 border-gray-300 border hover:bg-gray-50 active:bg-gray-100  transition-colors text-sm min-w-[120px]">


                  <span className="flex-1">
                    {selectQuocGia?.Id == 0 ? "Quốc gia" : selectQuocGia?.Name}
                  </span>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-500 ml-2 icon transition-transform" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                  </svg>
                </MenuButton>

                <MenuPopover className="fadeInDown dropdown-filter-mobile min-w-[375px] absolute z-10 mt-1 rounded-lg p-2 overflow-auto focus:outline-none shadow-level3 bg-white text-sm ">
                  <div className="arbitrary-element">
                    <MenuItems className="grid grid-cols-3 gap-x-2 gap-y-1">
                      {quocGia && quocGia.map((item, index) =>
                        <MenuItem key={item.Id} className={`cursor-pointer select-none relative py-1.5 pl-2 pr-8 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded ${item.Id == selectQuocGia?.Id ? "bg-gray-100 pointer-events-none" : ""}`}
                          onSelect={() => handleSelectFilter(item, "quocgia")}>  {item.Name} <span className="absolute right-1">
                            {item.Id == selectQuocGia?.Id && <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-pink-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                            </svg>}

                          </span>
                        </MenuItem>
                      )}

                    </MenuItems>
                  </div>

                </MenuPopover>


              </Menu>
            </div>
            }


            <div className="relative p-1 md:p-2">
              <Menu>
                <MenuButton className="flex items-center rounded px-2 md:px-3 py-1.5 text-left cursor-pointer focus:outline-none whitespace-nowrap leading-5 border-gray-300 border hover:bg-gray-50 active:bg-gray-100  transition-colors text-sm min-w-[120px]">


                  <span className="flex-1">
                    {selectYear?.Id == 0 ? "Năm" : selectYear?.Name}
                  </span>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-500 ml-2 icon transition-transform" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                  </svg>
                </MenuButton>

                <MenuPopover className="fadeInDown  min-w-[150px] absolute z-10 mt-1 rounded-lg p-2 overflow-auto focus:outline-none shadow-level3 bg-white text-sm ">
                  <div className="arbitrary-element">
                    <MenuItems className="grid grid-cols-1 gap-x-2 gap-y-1">
                      {filterYear.map((item, index) =>
                        <MenuItem key={item.Id} className={`cursor-pointer select-none relative py-1.5 pl-2 pr-8 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded ${item.Id == selectYear?.Id ? "bg-gray-100 pointer-events-none" : ""}`}
                          onSelect={() => handleSelectFilter(item, "year")}>  {item.Name} <span className="absolute right-1">
                            {item.Id == selectYear?.Id && <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-pink-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                            </svg>}

                          </span>
                        </MenuItem>
                      )}

                    </MenuItems>
                  </div>

                </MenuPopover>


              </Menu>
            </div>

            <div className="relative p-1 md:p-2 md:w-[190px]">
              <input
                type="text"
                // value={stext}
                defaultValue={""}
                // onBlur={closeSearch}
                onChange={(e) => debouncedSearchQuery(e.target.value)}
                placeholder="Tìm theo tên phim ..."
                className="items-center justify-center block px-2 md:px-3 py-1.5 text-sm bg-white border border-gray-300 rounded w-full"
              />

              <span
                className="absolute border-none outline-none opacity-50 right-3 top-3 md:right-4 md:top-4"
                aria-label="Search"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-4 h-4 "
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  />
                </svg>
              </span>
            </div>


            {/* <div className="w-1  ">&nbsp;</div> */}

          </div>
          {/* </CssCarousel> */}
        </div>
      </div>
      <div className="min-h-[400px] relative">
        {(loading || isLoading) && [1, 2, 3, 4].map((item, index) => <div className="absolute top-0 left-0 z-12 flex items-start justify-center w-full h-full pt-20 bg-white bg-opacity-30 fadeIn" key={index}>
          <Loader />
        </div>)
        }

        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-x-3 gap-y-5 md:gap-6  ">


          {error && <div className="text-center py-5">Có lỗi xảy ra </div>}

          {filmsData && filmsData?.Data?.Items.map((item, index) => (
            <div key={item.Id}>
              <MovieItem data={item} />
            </div>
          ))}
        </div>

        {filmsData && filmsData?.Data?.Items.length == 0 && <div> <div className="text-center mt-4 mb-7 md:mt-10 md:mb-20 w-full text-gray-400">
          Rất tiếc, không tìm thấy phim phù hợp với lựa chọn của bạn
        </div>

          <div>
            <h3 className="text-gray-500 font-semibold mb-4 uppercase text-sm">Gợi ý phim hay dành cho bạn</h3>

            <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-x-3 gap-y-5 md:gap-6  ">

              {dataFilmsSoon && dataFilmsSoon.map((item, index) => (
                <div key={item.Id}>
                  <MovieItem data={item} />
                </div>
              ))}

            </div>
          </div>

        </div>}
      </div>

      {filmsData?.Data?.Items.length != 0 && <div className={`text-center mt-10 ${loading ? "pointer-events-none opacity-80" : ""}`}>
        <Pagination
          onChange={onChange}
          current={current}
          total={filmsData?.Data?.TotalItems ?? 0}
          defaultPageSize={20}
          showLessItems
          itemRender={itemRender}
          showTitle={false}
          className=" justify-center flex flex-nowrap items-center space-x-2 text-sm"
        />
      </div>}
    </>
  );
};

export default MovieListFilter;

