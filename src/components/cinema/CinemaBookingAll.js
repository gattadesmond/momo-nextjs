import MovieBookingLocation from "@/components/cinema/MovieBookingLocation";
import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import MovieBookingList from "@/components/cinema/MovieBookingList";

import MovieBookingRapList from "@/components/cinema/MovieBookingRapList";
import MovieBookingRapSelect from "@/components/cinema/MovieBookingRapSelect";

import BookingRapSelectMobile from "@/components/cinema/BookingRapSelectMobile";

import { Media, MediaContextProvider } from "@/lib/mediaFresnel";

const CinemaBookingAll = ({ master, sAlone = false }) => {
  return (
    <>
      <MediaContextProvider>
        <div className=" md:shadow-soju1 rounded-lg border-gray-200  bg-white md:overflow-hidden md:border">
          <div className="relative z-10">
            <div className="flex items-center  pt-3 md:px-4 ">
              <div className="mr-3 hidden md:block">Vị trí</div>
              <MovieBookingLocation data={master.Cities} />
            </div>

            {!sAlone && (
              <Media greaterThan="sm">
                <div className="relative -mx-5 border-b  border-gray-200 bg-white md:top-0 md:mx-0">
                  <div className="pt-3 pb-2 ">
                    <MovieBookingRap data={master.Cineplexs} />
                  </div>
                </div>
              </Media>
            )}
          </div>
          {sAlone && <div className="border-b border-gray-200 py-1.5"></div>}
          <Media lessThan="md">
            <BookingRapSelectMobile master={master} sAlone={sAlone} />
          </Media>

          <div className="grid grid-cols-1 lg:grid-cols-3">
            <Media greaterThan="sm">
              <div className="cinema-list-height relative  mx-0 divide-y divide-gray-100 ">
                <MovieBookingRapList sAlone={sAlone}/>
              </div>
            </Media>

            <div className="cinema-list-height -mx-5 md:mx-0  md:border-l md:border-gray-200 lg:col-span-2 ">
              <Media
                greaterThan="sm"
                className="sticky top-0 z-20 border-b border-gray-200 shadow-sm"
              >
                <MovieBookingRapSelect />
              </Media>

              <MovieBookingList />
            </div>
          </div>
        </div>
      </MediaContextProvider>
      <style jsx>{`
        .box-nav {
          box-shadow: 0 2px 5px 0 rgb(0 0 0 / 5%);
        }

        .booking-list-height {
          min-height: 240px;
        }

        @media screen and (min-width: 767px) {
          .cinema-list-height {
            height: 500px;
            overflow-y: auto;
          }
        }
      `}</style>
    </>
  );
};
export default CinemaBookingAll;
