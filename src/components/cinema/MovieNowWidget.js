import Link from "next/link";

import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

import MovieItem2 from "./MovieItem2";

const MovieNowWidget = ({ data }) => {
  const [films, setFilms] = useState(null);

  useEffect(() => {
    axios
      .get(`/ci-film/loadMore?apiStatus=3&lastIndex=0&count=30&sortType=5&sortDir=1&apiCityId=50`)
      .then(function (response) {
        const res = response.Data ?? null;
        setFilms(res?.Items ?? null);
      });
  }, []);

  return (
    <div className="relative divide-y divide-gray-200 cinema ">
      {films && films.map((item, index) => (
        <div className="py-3 cinema__item" key={item.Id}>
          <MovieItem2 data={item}  isNumber={index + 1}/>
        </div>
      ))}

      <style jsx>{`
      
        .cinema::-webkit-scrollbar {
          display: none;
        }
      `}</style>
    </div>
  );
};

export default MovieNowWidget;
