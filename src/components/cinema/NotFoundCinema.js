import Link from "next/link";
function NotFoundCinema({
  title = "Không tìm thấy kết quả",
  content = "Bạn vui lòng tìm kiếm kết quả khác",
  link = null,
  button = null,
}) {
  return (
    <div className="py-5 text-center">
      <div>
        <img
          src="https://static.mservice.io/next-js/_next/static/public/cinema/not-found.svg"
          alt="Not found"
          className="block mx-auto"
          loading="lazy"
          width="120"
          height="120"
        />
      </div>

      <div className="mt-3 mb-0 text-lg font-semibold">{title}</div>
      <div className="text-sm text-gray-500">{content}</div>
      {button && (
        <div className="mt-4 text-sm text-center">
          <Link href={link}>
            <a className="btn-primary">{button}</a>
          </Link>
        </div>
      )}
    </div>
  );
}

export default NotFoundCinema;
