import { useEffect, useState } from "react";

import { useDebouncedCallback } from "use-debounce";

import { useSelector, useDispatch } from "react-redux";

import { axios } from "@/lib/index";

import RapRowItem from "@/components/cinema/RapRowItem";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import ContentLoader from "react-content-loader";

import Loader from "@/components/ui/Loader";

import { setRapId } from "@/slices/filmBooking";

import { useRouter } from "next/router";

const MovieBookingRapList = ({ sAlone = false, sRap = false }) => {
  const dispatch = useDispatch();
  const filmStore = useSelector((state) => state.filmBooking);
  const router = useRouter();

  const { cityId, isNearby, rapBrand, geo, rapId } = filmStore;

  const [rapList, setRapList] = useState(null);

  const [stext, setStext] = useState("");

  const [loading, setLoading] = useState(false);

  const [isMore, setIsMore] = useState(false);

  const [lastIndex, setLastIndex] = useState(0);

  const [isLoadingRapList, setIsLoadingRapList] = useState(false);

  const debouncedFetch = useDebouncedCallback((query, isMore) => {
    if (isMore) {
      query += `&lastIndex=${lastIndex}`;
    }
    axios
      .get(`/ci-cinema/loadMore?${query}`)
      .then(function (response) {
        setLoading(false);
        const res = response.Data?.Items;

        if (!response.Result || !res) {
          setLoading(false);
          setRapList(null);
          setNotFound(true);
          return;
        }

        const more =
          response?.Data?.LastIndex != null &&
          response?.Data?.LastIndex < response?.Data?.TotalItems
            ? true
            : false;
        setIsMore(more);

        let index =
          response.Data.LastIndex != null ? response.Data.LastIndex : 0;
        setLastIndex(index);

        let result = [];

        if (isMore) {
          result = [...rapList, ...res];
          setIsLoadingRapList(false);
        } else {
          result = res;
        }

        setRapList(result);
      })
      .catch(function (error) {
        return;
      });
  }, 500);

  const getQuery = () => {
    let query = "";
    let count = 7;
    query += `&count=${count}`;

    if (isNearby == true) {
      query += `&lat=${geo.lat}&lon=${geo.lng}&sortType=4&sortDir=1`;
    } else {
      query += `&sortType=1&sortDir=1`;
    }

    if (rapBrand != null) {
      query += `&cineplex=${rapBrand}`;
    }

    if (cityId != null) {
      query += `&apiCityId=${cityId}`;
    }

    if (stext != "") {
      query += `&query=${stext}`;
    }
    return query;
  };

  useEffect(() => {
    let query = getQuery();
    setLoading(true);
    debouncedFetch(query);
  }, [cityId, isNearby, rapBrand, geo, stext]);

  const handleInputSearch = (val) => {
    setStext(val);
    setLoading(true);
  };

  const handelLoadMore = () => {
    if (isLoadingRapList) return;

    if (isMore == true && lastIndex > 0) {
      setIsLoadingRapList(true);
      let query = getQuery();
      debouncedFetch(query, true);
    }
  };

  const handleSelectRap = (item) => {
    if (sRap == true) {
      router.push(item.Link);
    } else {
      dispatch(setRapId(item.ApiCinemaId || null));
    }
  };

  useEffect(() => {
    if (!rapId || sAlone) {
      if (rapList && rapList.length > 0) {
        dispatch(setRapId(rapList[0].ApiCinemaId || null));
      }
    }
  }, [rapList]);

  return (
    <>
      {loading && (
        <div className="z-12 fadeIn absolute top-0 left-0 flex h-full w-full items-start justify-center bg-white bg-opacity-80  pt-32">
          <Loader />
        </div>
      )}
      <div className="relative z-20 px-3 py-2">
        <input
          type="text"
          value={stext}
          // onBlur={closeSearch}
          onChange={(e) => handleInputSearch(e.target.value)}
          placeholder="Tìm theo tên rạp ..."
          className="block h-9 w-full items-center justify-center rounded border border-gray-200 bg-gray-50 py-1 pl-3 pr-10 text-sm"
        />

        <span
          className="absolute right-5 top-4 border-none opacity-50 outline-none"
          aria-label="Search"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-4 w-4 "
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
        </span>
      </div>
      {rapList &&
        rapList.map((item, index) => (
          <div
            key={item.Id}
            className={` ${
              item.ApiCinemaId == rapId && !sRap
                ? " bg-pink-50 opacity-100"
                : "cursor-pointer  md:hover:bg-gray-50"
            }`}
          >
            <RapRowItem
              data={item}
              type={sRap ? 1 : 3}
              onClick={() => handleSelectRap(item)}
            />
          </div>
        ))}

      {isLoadingRapList && (
        <div className="px-4 py-4 md:px-4">
          <ContentLoader
            viewBox="0 0 250 80"
            width={250}
            height={80}
            title="Loading news..."
          >
            <rect x="0" y="0" rx="5" ry="5" width="35" height="35" />

            <rect x="47" y="5" rx="0" ry="0" width="80" height="20" />
          </ContentLoader>
        </div>
      )}

      {isMore && (
        <div className="py-5 text-center">
          <button
            onClick={() => handelLoadMore()}
            disabled={isLoadingRapList ? true : false}
            type="button"
            className="btn inline-block rounded-3xl border border-pink-500 border-opacity-30 bg-pink-50 bg-opacity-50 px-4 py-1 text-center text-sm font-bold text-pink-600 text-opacity-90 transition-all hover:bg-pink-100"
          >
            Xem thêm
          </button>
        </div>
      )}

      {rapList != null && rapList.length == 0 && (
        <NotFoundCinema
          title=" Không tìm thấy cụm rạp nào. "
          content="Bạn hãy thử lại với phim khác hoặc rạp khác nha!"
        />
      )}

      <style jsx>{``}</style>
    </>
  );
};

export default MovieBookingRapList;
