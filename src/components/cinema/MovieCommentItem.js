import Link from "next/link";

import { useEffect, useState } from "react";

import { parseISO, format } from "date-fns";

import ReadMore from "@/components/ReadMore";

import UserAvatar from "@/components/cinema/UserAvatar";

const MovieCommentItem = ({ data }) => {


  //   cột Booking=1
  // là user có mua vé ở MoMo
  // Featured là nổi bật có dấu gim
  // Tags có chữ Khen
  // thì e gắn hình LIKE vô
  // chê thì ngược lại
  // hình nè


  return (
    <>
      <div className="flex flex-nowrap ">
        {data.Avatar && <UserAvatar avatarUrl={data.Avatar} showName={data.ShowName} />}


        <div className=" flex-1 ml-4  ">

          <div className="flex flex-nowrap items-center">


            <div className=" text-md font-bold flex-1">
              {data.ShowName}     {data.CreatedAt && (
                <>
                  <div className=" inline-block font-normal text-xs text-gray-400 ">
                    {" "}
                    ·{" "} {format(parseISO(data.CreatedAt), "dd/MM/yyyy")}
                  </div>
                </>
              )}


              {data.Booking == 1 && <div className="text-pink-500 text-xs mt-0 mb-1 font-normal">
                Đã mua vé trên MoMo
              </div>}


            </div>

            {data.Feature == 1 && <div className="text-pink-500 text-xs mt-0 mb-1 font-normal pl-3">
              <img src=" https://static.mservice.io/blogscontents/momo-upload-api-210915162244-637673197648216091.png" loading="lazy" className="w-6 mt-1" />
            </div>}
          </div>







          <div className=" whitespace-pre-wrap  text-md text-gray-700 mt-1 break-words">
            {data.Comment && <ReadMore maxLine="4" btnText="... Xem thêm" type="line">
              {data.Comment}
            </ReadMore>}
          </div>
        </div>
      </div>
      <style jsx>{``}</style>
    </>
  );
};

export default MovieCommentItem;
