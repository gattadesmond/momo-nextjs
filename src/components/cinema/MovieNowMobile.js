import Link from "next/link";

import { useEffect, useState } from "react";

import MovieItem from "./MovieItem";

const MovieNow = ({ data, isDark }) => {
  return (
    <div className="relative cinema">
      {data.map((item, index) => (
        <div className=" cinema__item" key={item.Id}>
          <MovieItem data={item} isDark={isDark} isNumber={index + 1} />
        </div>
      ))}

      <style jsx>{`
        .cinema {
          display: flex;
          overflow-x: auto;
          overflow-y: hidden;
          flex-flow: row nowrap;
          scroll-snap-type: x mandatory;
          scroll-padding: 0 0 0 16px;
          scroll-margin-left: 16px;
          padding-left: 16px;
          padding-right: 16px;
          -webkit-overflow-scrolling: touch;
        }
        .cinema__item {
          flex: 0 0 155px;
          margin-right: 16px;
        }

        @media screen and (max-width: 375px) {
          .cinema__item {
            flex: 0 0 145px;
          }
        }

        .cinema__item:last-child {
          margin-right: 0;
        }
        .cinema::-webkit-scrollbar {
          display: none;
        }
      `}</style>
    </div>
  );
};

export default MovieNow;
