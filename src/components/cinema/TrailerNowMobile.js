import Link from "next/link";

import { useEffect, useState } from "react";

import dynamic from "next/dynamic";
const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import lgVideo from "lightgallery/plugins/video";
import lgThumbnail from "lightgallery/plugins/thumbnail";

import TrailerItem from "./TrailerItem";

const TrailerNow = ({ data }) => {
  let lightGallery = null;
  const onInitGallery = (e) => {
    lightGallery = e.instance;
  };

  const onOpenGallery = (number) => {
    // const act = number - 1 < 0 ? 0 : number - 1;
    lightGallery.openGallery(number);
  };

  let video = [];
  data.map((item, index) => {
    let hmm = {
      video: {
        source: [
          {
            src: item.ApiAutoplayTrailer,
            type: "video/mp4",
          },
        ],
        attributes: {
          preload: true,
          controls: true,
        },
      },
      src:item.TrailerUrl,
      thumb: item.BannerUrl,
      poster: item.BannerUrl,
      subHtml: `<h4>${item.Title}</h4>`,
    };
    video.push(hmm);
  });

  return (
    <>
      <div className="relative cinema">
        {data.map((item, index) => (
          <div
            className=" cinema__item"
            key={item.Id}
            onClick={() => onOpenGallery(index)}
          >
            <TrailerItem data={item} />
          </div>
        ))}

        <style jsx>{`
          .cinema {
            display: flex;
            overflow-x: auto;
            flex-flow: row nowrap;
            overflow-y: hidden;
            scroll-snap-type: x mandatory;
            scroll-padding: 0 0 0 16px;
            scroll-margin-left: 16px;
            padding-left: 16px;
            padding-right: 16px;
            -webkit-overflow-scrolling: touch;
          }
          .cinema__item {
            flex: 0 0 250px;
            margin-right: 16px;
          }

          .cinema__item:last-child {
            margin-right: 0;
          }

          .cinema::-webkit-scrollbar {
            display: none;
          }
        `}</style>
      </div>

      <LightGallery
        speed={500}
        autoplayFirstVideo={true}
        onInit={onInitGallery}
        dynamic={true}
        dynamicEl={video}
        download={false}
        // onInit={onInitGallery}
        plugins={[lgVideo, lgThumbnail]}
      />
    </>
  );
};

export default TrailerNow;
