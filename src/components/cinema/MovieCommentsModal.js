import Link from "next/link";

import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

import { parseISO, format } from "date-fns";

import ReadMore from "@/components/ReadMore";


import Modal from "@/components/modal/Modal";

import ModalBody from "@/components/modal/ModalBody";


import ModalFooter from "@/components/modal/ModalFooter";

import CssCarousel from "@/components/CssCarousel";

import UserAvatar from "@/components/cinema/UserAvatar";

import { useGetFilmCommentsQuery } from '@/slices/services/getFilmComments';

import MovieCommentsVirtualList from "@/components/cinema/MovieCommentsVirtualList";


const MovieCommentsModal = ({ id, isOpen, onDismiss }) => {

  const { data: filmComments, error, isLoading } = useGetFilmCommentsQuery(id);

  const comments =  filmComments?.Data?.[0]?.Comment ?? null;
  const score = filmComments?.Data?.[0]?.Rating?.MedalRating ?? null;

  const listMedalRatings = score ? score.medalRatings.filter((item) => item.isShow == true)
    .sort((a, b) => (a.total > b.total ? -1 : 1)).slice(0, 4) : null;

  const [actNum, setActNum] = useState(0);

  useEffect(() => {
    setActNum(0);

    // axios.get(`/ci-film/rating/${id}`).then(function (response) {
    //   if (!response.Result) return;
    //   const res = response.Data[0];

    //   setComments(res.Comment);

    //   setScore(res.Rating.MedalRating);

    //   if (res.Rating.MedalRating && res.Rating.MedalRating.medalRatings && res.Rating.MedalRating.medalRatings.length > 0) {
    //     let list = res.Rating.MedalRating.medalRatings;
    //     list = list
    //       .filter((item) => item.isShow == true)
    //       .sort((a, b) => (a.total > b.total ? -1 : 1)).slice(0, 4);
    //     setListMedalRatings(list);
    //   }     
    // });
  }, [id]);

  return (
    <>
      <Modal isOpen={isOpen} onDismiss={onDismiss} isFull={true}>
        <header className="modal-header  pt-4 sm:pt-5 sm:pb-2 pl-6 pr-6  flex items-center">
          {score && (
            <div className="flex flex-nowrap space-x-6 items-center">
              <div className="text-center mr-2 px-1 ml-4 md:ml-0  ">
                <div className="grid  grid-cols-1 sm:grid-cols-2">
                  <img
                    src={score.medal.image}
                    className="block w-12 sm:w-16 mx-auto "
                  />
                  <div>
                    <div className="mt-2 text-lg sm:text-xl font-semibold whitespace-nowrap">
                      {score.medal.per}%
                    </div>

                    <div className="mt-0 text-gray-500 text-sm whitespace-nowrap">
                      {score.medal.total} đánh giá
                    </div>
                  </div>
                </div>
              </div>

              <div>
                <div className="grid grid-cols-1  sm:grid-cols-2 gap-y-1 md:gap-y-4 gap-x-5  text-sm md:text-md">
                  {listMedalRatings && listMedalRatings.map((item, index) =>
                    <div className="flex flex-nowrap space-x-2 items-center" key={index}>
                      <img src={item.image} alt={item.name} className="w-8 h-8" />
                      <div>{item.name}</div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}
        </header>

        <div className="modal-body  h-full overflow-hidden p-0 bg-white rounded relative">
          {comments && comments.length > 0 && (
            <>
              <div className="z-10 absolute left-2 right-2 top-0">
                <CssCarousel bgColor="255,255,255">
                  <div className="flex w-full bg-white   overflow-scroll overflow-y-hidden  scroll-hide border-b border-gray-300 ">
                    {comments.map((item, index) => (
                      <div
                        key={index}
                        className={`inline-block px-2 py-3 md:px-3 md:py-3 border-b-2 text-sm md:text-md  whitespace-nowrap  text-gray-500 cursor-pointer ${index == actNum
                          ? " border-pink-500 font-bold text-gray-900"
                          : "border-transparent"
                          }`}
                        onClick={() => setActNum(index)}
                      >
                        {item.TagName} ({item.Items.length})
                      </div>
                    ))}
                    <div className="w-1 ">&nbsp;</div>
                  </div>
                </CssCarousel>
              </div>

              <MovieCommentsVirtualList data={comments[actNum].Items} />

            </>
          )}
        </div>
        <ModalFooter className="flex">
          <div>
            <a
              onClick={onDismiss}
              className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default MovieCommentsModal;
