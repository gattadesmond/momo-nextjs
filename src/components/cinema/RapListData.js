import MovieBookingLocation from "@/components/cinema/MovieBookingLocation";
import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import MovieBookingList from "@/components/cinema/MovieBookingList";

import MovieBookingRapList from "@/components/cinema/MovieBookingRapList";
import MovieBookingRapSelect from "@/components/cinema/MovieBookingRapSelect";

import BookingRapSelectMobile from "@/components/cinema/BookingRapSelectMobile";


const RapListData = ({ master }) => {
  return (
    <>
        <div className=" md:shadow-soju1 rounded-lg border-gray-200  bg-white md:overflow-hidden md:border  max-w-3xl mx-auto">
          <div className="relative z-10">
            <div className="flex items-center  pt-3 md:px-4 ">
              <div className="mr-3 hidden md:block">Vị trí</div>
              <MovieBookingLocation data={master.Cities} />
            </div>

            <div className="relative -mx-5 border-b  border-gray-200 bg-white md:top-0 md:mx-0">
              <div className="pt-3 pb-2 ">
                <MovieBookingRap data={master.Cineplexs} />
              </div>
            </div>
          </div>

          <div className="grid grid-cols-1 ">
            <div className="cinema-list-height relative   divide-y divide-gray-100 -mx-5 md:mx-0 ">
              <MovieBookingRapList sRap={true} />
            </div>
          </div>
        </div>
      <style jsx>{`
        .box-nav {
          box-shadow: 0 2px 5px 0 rgb(0 0 0 / 5%);
        }

        .booking-list-height {
          min-height: 240px;
        }

        @media screen and (min-width: 767px) {
          .cinema-list-height {
            height: 500px;
            overflow-y: auto;
          }
        }
      `}</style>
    </>
  );
};
export default RapListData;
