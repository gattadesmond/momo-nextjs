import Link from "next/link";

import { useEffect, useState, useRef, Fragment } from "react";

import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import MovieBookingLocation from "@/components/cinema/MovieBookingLocation";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import RapRowItem from "@/components/cinema/RapRowItem";

import Loader from "@/components/ui/Loader";


import ContentLoader from "react-content-loader";

import { useDebouncedCallback } from "use-debounce";

import { axios } from "@/lib/index";

import { useMediaQuery } from "react-responsive";
import { LG } from "@/lib/breakpoint";

const CinemaSuggest = ({ master, sCineplex = "", sAlone = false }) => {
  const [rapBrand, setRapBrand] = useState("");

  const [cityId, setCityId] = useState("");

  const [rapList, setRapList] = useState(null);

  const [geo, setGeo] = useState({ lat: "", lng: "" });

  const [loading, setLoading] = useState(false);

  const [isMore, setIsMore] = useState(false);

  const [isNearby, setIsNearby] = useState(false);

  const [lastIndex, setLastIndex] = useState(0);

  const [isLoadingRapList, setIsLoadingRapList] = useState(false);

  const boxId = useRef(null);


  const [stext, setStext] = useState("");

  //Check co rap dc chon
  useEffect(() => {
    if (sCineplex != "") {
      setRapBrand(sCineplex);
    }
  }, []);

  const itemLoad = 5;
  const isLG = useMediaQuery(LG);

  const fetchData = useDebouncedCallback(
    // function
    (isLoadMore = false) => {
      let query = "";
      let last = 0;

      if (isNearby) {
        query = query.concat(
          `&lat=${geo.lat}&lon=${geo.lng}&sortType=4&sortDir=1`
        );
      } else {
        query = query.concat(`&sortType=5&sortDir=1`);
      }

      if (stext != "") {
        query = query.concat(`&query=${stext}`);
      }

      if (rapBrand != "") {
        query = query.concat(`&cineplex=${rapBrand}`);
      }
      if (cityId != "" ) {
        query = query.concat(`&apiCityId=${cityId}`);
      }

      if (isLoadMore) {
        last = lastIndex;
      }

      axios
        .get(`/ci-cinema/loadMore?lastIndex=${last}${query}&count=${itemLoad}`)
        .then(function (response) {

          setLoading(false);
          const res = response.Data?.Items;
          const more =
            response.Data.LastIndex != null &&
              response.Data.LastIndex < response.Data.TotalItems
              ? true
              : false;

          setIsMore(more);

          if (res) {
            let index =
              response.Data.LastIndex != null ? response.Data.LastIndex : 0;
            setLastIndex(index);

            let result = [];

            if (!isLoadMore) {
              result = res;
            } else {
              result = [...rapList, ...res];
              setIsLoadingRapList(false);
            }

         

            setRapList(result);
          }
        });
    },
    // delay in ms
    500
  );

  const handelLoadMore = () => {
    if (isLoadingRapList) return;
    setIsLoadingRapList(true);
    fetchData(true);
  };

  const handleSelectGeo = (lat, long, cityDetect) => {
    if (lat != "" || long != "") {
      setGeo({ lat: lat, lng: long });

      setLoading(true);
      setIsNearby(true);
      setCityId(cityDetect);
     
      fetchData();
    } else {
      alert(
        "Bạn vui lòng cho phép truy cập địa điểm để thực hiện chức năng này"
      );
    }
  };

  const handleSelectCity = (val) => {
    setCityId(val);
    // if (cityId == 0) return;
    setLoading(true);
    setIsNearby(false);
    fetchData();
  };

  const handleRapBrand = (val) => {
    setRapBrand(val);

    setLoading(true);
    fetchData();

    if (boxId.current) {
      boxId.current.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };


  const handleInputSearch = (val) => {
    setStext(val);
    setLoading(true);
    fetchData();

  };

  return (
    <>
      <div
        className="grid grid-cols-1 border-gray-200  rounded-lg md:border cinema-scroll-margin max-w-3xl mx-auto  bg-white md:shadow-soju1 md:overflow-hidden"
        ref={boxId}
      >
        <div className={` ${sAlone ? "sticky z-20 bg-white border-b border-gray-200 shadow-sm  -mx-5 md:mx-0 px-4 top-0 lg:px-0" : "relative z-10"}`}>
          <div className="h-full  py-2 md:px-4 md:pt-3 md:pb-2">
              <div className="w-full md:mt-1">
                <div className="flex items-center ">
                  <div className="hidden md:block mr-3">Vị trí</div>
                  <MovieBookingLocation
                    data={master.Cities}
                    isNearby={isNearby}
                    onSelect={(e) => handleSelectCity(e)}
                    onSelectGeo={(lat, lng) => handleSelectGeo(lat, lng)}
                  />

                  <div className="hidden md:block">
                    <div className="px-3 md:px-4 relative z-20 ">
                      <input
                        type="text"
                        value={stext}
                        // onBlur={closeSearch}
                        onChange={(e) => handleInputSearch(e.target.value)}
                        placeholder="Tìm theo tên rạp ..."
                        className="items-center justify-center block w-full pl-3 pr-10 py-1 bg-gray-50 border border-gray-200 rounded h-9 text-sm"
                      />

                      <span
                        className="absolute border-none outline-none opacity-50 right-5 top-2 md:right-6"
                        aria-label="Search"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="w-4 h-4 "
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                          />
                        </svg>
                      </span>

                    </div>

                  </div>
                </div>
              </div>
          </div>
        </div>

        {!sAlone && (
          <div className="sticky z-20 block pt-3 pb-2  -mx-5 bg-white border-b border-gray-200 shadow-sm lg:relative top-sticky-menu  md:mx-0 lg:top-0 ">
            <MovieBookingRap
              onSelect={(e) => handleRapBrand(e)}
              data={master.Cineplexs}
              sCineplex={sCineplex}
            />
          </div>
        )}

        <div className=" relative">
          {loading && (
            <div className="absolute top-0 left-0 z-12 flex items-start justify-center w-full h-full pt-20 bg-white bg-opacity-80 fadeIn">
              <Loader />
            </div>
          )}

          <div
            // option={{ wheelPropagation: false }}
            className=" cinema-list-height -mx-5 md:mx-0"
          >
            <div className="">
              <div className="divide-y divide-gray-100 ">
                <div className="px-3 md:px-4 py-2 relative z-15 md:hidden">
                  <input
                    type="text"
                    value={stext}
                    // onBlur={closeSearch}
                    onChange={(e) => handleInputSearch(e.target.value)}
                    placeholder="Tìm theo tên rạp ..."
                    className="items-center justify-center block w-full pl-3 pr-10 py-1 bg-white border border-gray-200 rounded h-9 text-sm"
                  />

                  <span
                    className="absolute border-none outline-none opacity-50 right-5 top-4 md:right-6"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-4 h-4 "
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      />
                    </svg>
                  </span>

                </div>


                {rapList != null &&
                  rapList.length > 0 &&
                  rapList.map((item, index) => (
                    <Fragment key={item.Id}>
                      <RapRowItem data={item} type={1} />
                    </Fragment>
                  ))}

                {isLoadingRapList && (
                  <div className=" px-4 py-4">
                    <ContentLoader
                      viewBox="0 0 250 80"
                      width={250}
                      height={80}
                      title="Loading news..."
                    >
                      <rect x="0" y="0" rx="5" ry="5" width="58" height="58" />

                      <rect x="75" y="5" rx="0" ry="0" width="80" height="20" />
                      <rect
                        x="75"
                        y="35"
                        rx="0"
                        ry="0"
                        width="220"
                        height="12"
                      />
                    </ContentLoader>
                  </div>
                )}

                {isMore && (
                  <div className="py-5 text-center">
                    <button
                      onClick={() => handelLoadMore()}
                      disabled={isLoadingRapList ? true : false}
                      type="button"
                      className="inline-block px-4 py-1 text-sm font-bold text-center text-pink-600 transition-all bg-opacity-50 border border-pink-500 rounded-3xl border-opacity-30 bg-pink-50 btn text-opacity-90 hover:bg-pink-100"
                    >
                      Xem thêm
                    </button>
                  </div>
                )}

                {rapList != null && rapList.length == 0 && (
                  <NotFoundCinema
                    title=" Không tìm thấy cụm rạp nào. "
                    content="Bạn hãy thử lại với phim khác hoặc rạp khác nha!"
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      {isLG && !sAlone ? (
        <style jsx global>{`
            .cinema-list-height {
              min-height: 400px;
              overflow-y: auto;
            }
        `}</style>
      ) : 
        <style jsx global>
        {`
            .cinema-list-height {
              min-height: 300px;
            }
        `}</style>
      }
    </>
  );
};

export default CinemaSuggest;
