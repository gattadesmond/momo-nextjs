import Link from "next/link";

import Image from "next/image";
import MapBox from "@/components/ui/MapBox";

const RapRowAvatar = ({ Logo, Name, Url = false }) => {
  return (
    <div className="flex-none overflow-hidden bg-white border border-gray-200 rounded w-9 h-9 flex items-center justify-center ">
      {Url ? (
        <Link href={Url}>
          <a className="flex" alt={Name}>
            <Image
              src={Logo}
              alt={Name}
              layout="intrinsic"
              width={32}
              height={32}
            />
          </a>
        </Link>
      ) : (
        <Image
          src={Logo}
          alt={Name}
          layout="intrinsic"
          width={32}
          height={32}
        />
      )}
    </div>
  );
};

const RapRowItem = ({ data, type = null, onClick = null }) => {
  const item = data;

  if (type === 4) {
    return (
      <>
        {onClick && (
          <div
            className="z-1 absolute inset-0 bg-transparent"
            onClick={onClick}
          ></div>
        )}

        <div className="flex items-center rap-detail flex-nowrap  w-full">
          <div className="flex-none hidden md:block">
            <RapRowAvatar Logo={item.Logo} Name={item.Name} Url={item.Link} />
          </div>

          <div className="flex-1 min-w-0 md:pl-3">
            <div className="mb-0 text-md font-semibold text-gray-800 leading-tight">
              {item.Link && type != 3 ? (
                <Link href={item.Link}>
                  <a
                    className="text-gray-800 hover:text-pink-500 "
                    alt={item.Name}
                  >
                    Lịch chiếu phim {item.Name}
                  </a>
                </Link>
              ) : (
                <span>{item.Name}</span>
              )}
            </div>
            {item.Address && (
              <div className="flex text-gray-500 text-tiny flex-nowrap items-center">
                <div className="truncate">
                  {item.Address}
                </div>

                <div className={`pl-2 ${type == 3 && "hidden md:block"}`}>
                  <MapBox
                    lat={item.Lat}
                    lon={item.Lon}
                    address={item.Address}
                    name={item.Name}
                  >
                    [ Bản đồ ]
                  </MapBox>
                </div>
              </div>
            )}

            {(item.FeaturedNote || item.CineplexFeaturedNote) && type != 2 && (
              <div className="mb-0 text-tiny text-pink-600 flex items-center mt-0">
                <div className="pr-1 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-3 w-3  rotate-90"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"
                    />
                  </svg>
                </div>
                <div className="leading-tight line-clamp-2">
                  {item.FeaturedNote
                    ? item.FeaturedNote
                    : item.CineplexFeaturedNote}
                </div>
              </div>
            )}
          </div>
        </div>
      </>
    );
  }

  if (type === 3) {
    return (
      <div className="block py-2 px-4   relative fwefwe">
        {onClick && (
          <div
            className="z-1 absolute inset-0 bg-transparent"
            onClick={onClick}
          ></div>
        )}

        <div className="flex items-center rap-detail flex-nowrap ">
          <RapRowAvatar Logo={item.Logo} Name={item.Name} />

          <div className="flex-1 min-w-0 pl-3 mb-0 text-md text-gray-800 leading-tight">
            <span>{item.Name}</span>
          </div>

          <div className="self-center flex-none pl-2 hidden md:block md:pl-5 ">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-4 h-4 text-gray-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M9 5l7 7-7 7"
              />
            </svg>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div
      className={`${type == 2 ? "" : ""} ${
        type == 1 ? "block py-3 px-4   relative" : ""
      } ${type == 3 ? "block py-3 px-4   relative" : ""}`}
    >
      {onClick && (
        <div
          className="z-1 absolute inset-0 bg-transparent"
          onClick={onClick}
        ></div>
      )}

      <div className="flex items-center rap-detail flex-nowrap ">
        <div className="flex-none ">
          <RapRowAvatar Logo={item.Logo} Name={item.Name} Url={item.Link} />
        </div>

        <div className="flex-1 min-w-0 pl-4">
          <div className="mb-0 text-md font-semibold text-gray-800 leading-tight">
            {item.Link && type != 3 ? (
              <Link href={item.Link}>
                <a
                  className="text-gray-800 hover:text-pink-500 "
                  alt={item.Name}
                >
                  {item.Name}
                </a>
              </Link>
            ) : (
              <span>{item.Name}</span>
            )}
          </div>
          {item.Address && (
            <div className="flex text-gray-500 text-tiny flex-nowrap items-center">
              <div className="truncate">
                {item.Address}
              </div>

              <div className={`pl-2 ${type == 3 && "hidden md:block"}`}>
                <MapBox
                  lat={item.Lat}
                  lon={item.Lon}
                  address={item.Address}
                  name={item.Name}
                >
                  [ Bản đồ ]
                </MapBox>
              </div>
            </div>
          )}

          {(item.FeaturedNote || item.CineplexFeaturedNote) && type != 2 && (
            <div className="mb-0 text-tiny text-pink-600 flex items-center mt-1">
              <div className="pr-1 ">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-3 w-3  rotate-90"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"
                  />
                </svg>
              </div>
              <div className="leading-tight line-clamp-2">
                {item.FeaturedNote
                  ? item.FeaturedNote
                  : item.CineplexFeaturedNote}
              </div>
            </div>
          )}

          {/* <div className="mb-0 text-sm text-pink-600 ">

        {item.CineplexFeaturedNote}
      </div> */}
        </div>

        <div className="self-center flex-none pl-2 hidden md:block md:pl-5 ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-4 h-4 text-gray-400 icon"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 5l7 7-7 7"
            />
          </svg>
        </div>


      </div>
    </div>
  );
};

export default RapRowItem;
