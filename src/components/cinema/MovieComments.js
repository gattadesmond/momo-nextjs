import Link from "next/link";

import { useEffect, useState } from "react";



import MovieCommentsModal from "@/components/cinema/MovieCommentsModal";

import MovieCommentItem from "@/components/cinema/MovieCommentItem";

import CssCarousel from "@/components/CssCarousel";

import { useGetFilmCommentsQuery } from '@/slices/services/getFilmComments'

const MovieComments = ({ id }) => {

  // const count = useSelector((state) => state.commentsFilm.value);

  // const dispatch = useDispatch();

  const { data: filmComments, error, isLoading } = useGetFilmCommentsQuery(id);



  const comments =  filmComments?.Data?.[0]?.Comment ?? null;

  const [actNum, setActNum] = useState(0);


  const [modal, setModal] = useState(false);
  const close = () => {
    setModal(false);
  };


  useEffect(() => {
    return function cleanup() {
      setActNum(0);
    };
  }, [id]);

  return (
    <>
      {comments && comments.length > 0 && (
        <>
          <CssCarousel bgColor="255,255,255">
            <div className="flex w-full  overflow-scroll overflow-y-hidden  scroll-hide border-b border-gray-300  ">
              {comments.map((item, index) => (
                <div
                  key={index}
                  className={`inline-block px-2 py-3 md:px-3 md:py-3 border-b-2 text-sm md:text-md  whitespace-nowrap  text-gray-500 cursor-pointer ${index == actNum
                    ? " border-pink-500 font-bold text-gray-900"
                    : "border-transparent"
                    }`}
                  onClick={() => setActNum(index)}
                >
                  {item.TagName} ({item.Items.length})
                </div>
              ))}
              <div className="w-1 ">&nbsp;</div>
            </div>
          </CssCarousel>

          <div className="grid grid-cols-1 gap-y-6 mt-4">
            {comments[actNum].Items.slice(0, 6).map((item, index) => (
              <div key={index} className="">
                <MovieCommentItem data={item} />
              </div>
            ))}
          </div>

          {comments[0].Items.length > 6 && (
            <div className="mt-5 text-center">
              <button
                className="border border-gray-400 px-5 font-bold  text-gray-700 py-2 text-md rounded-md inline-block hover:bg-gray-100"
                onClick={() => setModal(true)}
              >
                Hiển thị tất cả {comments[0].Items.length} bình luận
              </button>
            </div>
          )}

          {modal && (
            <MovieCommentsModal isOpen={modal} onDismiss={close} id={id} />
          )}
        </>
      )}

      {(!comments || comments.length == 0) && (
        <div className="text-gray-500 py-4">Chưa có bình luận</div>
      )}

      <style jsx>{``}</style>
    </>
  );
};

export default MovieComments;
