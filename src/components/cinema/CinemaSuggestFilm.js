import Link from "next/link";

import { useEffect, useState, useRef, Fragment } from "react";

import MovieBookingRap from "@/components/cinema/MovieBookingRap";

import MovieBookingLocation from "@/components/cinema/MovieBookingLocation";

import NotFoundCinema from "@/components/cinema/NotFoundCinema";

import RapRowItem from "@/components/cinema/RapRowItem";

import Loader from "@/components/ui/Loader";

import MovieBookingList from "@/components/cinema/MovieBookingList";


import ContentLoader from "react-content-loader";

import { useDebouncedCallback } from "use-debounce";

import { axios } from "@/lib/index";

import { useMediaQuery } from "react-responsive";
import { MD } from "@/lib/breakpoint";

import Image from "next/image";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

const CinemaSuggestFilm = ({
  master,
  isMobile,
  sCineplex = "",
  sAlone = false,
}) => {

  const [rapBrand, setRapBrand] = useState("");


  const [rapSelect, setRapSelect] = useState(null);


  const [rapList, setRapList] = useState(null);

  const [loading, setLoading] = useState(false);

  const [isMore, setIsMore] = useState(false);

  const [lastIndex, setLastIndex] = useState(0);

  const [isLoadingRapList, setIsLoadingRapList] = useState(false);


  const boxId = useRef(null);

  const [modal, setModal] = useState(false);


  const [stext, setStext] = useState("");

  const close = () => {
    setModal(false);
  };

  //Check co rap dc chon
  useEffect(() => {
    if (sCineplex != "") {
      setRapBrand(sCineplex);
    }
  }, []);

  const itemLoad = 7;
  const isMD = useMediaQuery(MD);


  const fetchData = useDebouncedCallback(
    // function
    (isLoadMore = false) => {
      let query = "";
      let last = 0;

      if (isNearby) {
        query = query.concat(
          `&lat=${geo.lat}&lon=${geo.lng}&sortType=4&sortDir=1`
        );
      } else {
        query = query.concat(`&sortType=5&sortDir=1`);
      }

      if (stext != "") {
        query = query.concat(`&query=${stext}`);
      }

      if (rapBrand != "") {
        query = query.concat(`&cineplex=${rapBrand}`);
      }
      if (cityId != "" ) {
        query = query.concat(`&apiCityId=${cityId}`);
      }

      if (isLoadMore) {
        last = lastIndex;
      }

      axios
        .get(`/ci-cinema/loadMore?lastIndex=${last}${query}&count=${itemLoad}`)
        .then(function (response) {
          setLoading(false);
          const res = response.Data?.Items;
          const more =
            response.Data.LastIndex != null &&
              response.Data.LastIndex < response.Data.TotalItems
              ? true
              : false;

          setIsMore(more);

          if (res) {
            let index =
              response.Data.LastIndex != null ? response.Data.LastIndex : 0;
              
            setLastIndex(index);

            let result = [];

            if (!isLoadMore) {
              result = res;
            } else {
              result = [...rapList, ...res];
              setIsLoadingRapList(false);
            }

    
            setRapList(result);
          }
        });
    },
    // delay in ms
    500
  );

  const handelLoadMore = () => {
    if (isLoadingRapList) return;
    setIsLoadingRapList(true);
    fetchData(true);
  };



  const handleSelectRap = (item) => {
    setRapSelect(item);
    setModal(false);

  };

  const handleInputSearch = (val) => {
    setStext(val);
    setLoading(true);
    fetchData();

  };

  useEffect(() => {
    if (rapList && rapList.length > 0) {
      setRapSelect(rapList[0]);
    }
  }, [rapList]);


  return (
    <>
      <div
        className=" border-gray-200  rounded-lg md:border cinema-scroll-margin   bg-white md:shadow-soju1 md:overflow-hidden"
        ref={boxId}
      >
        <div
          className={`relative z-10`}
        >
          <div className="h-full  py-2 md:px-4 md:pt-3 md:pb-2">
            <div className="w-full md:mt-1">
              <div className="flex items-center ">
                <div className="hidden md:block mr-3">Vị trí</div>
                <MovieBookingLocation
                  data={master.Cities}
                />
              </div>
            </div>
          </div>
        </div>


        {sAlone && <div className="h-2 border-b">&nbsp;</div>}

        {rapSelect &&
          <div className="border relative mt-3  cursor-pointer  border-pink-600  rounded md:hover:bg-gray-50  pt-2 pb-1 mb-2 lg:hidden  mx-0 md:mx-4  " onClick={() => setModal(true)}>
            <div className="mb-2 text-sm text-gray-600 absolute z-1 inline-block bg-white px-1 -top-3 left-1">Chọn rạp</div>

            <div
              className="flex items-center rap-detail flex-nowrap"
            >

              <div className="flex-none  w-9 h-9 mr-2 ml-1 bg-gray-50">
                <div className="aspect-w-1 aspect-h-1 ">
                  <Image
                    src={rapSelect.Logo}
                    alt={rapSelect.Name}
                    layout="fill"
                    objectFit="cover"
                  />
                </div>
              </div>


              <div className="flex-1 min-w-0 ">
                <div className="mb-0 text-md font-semibold text-gray-800 leading-tight">
                  <span>{rapSelect.Name}</span>
                </div>

              </div>

              <div className="flex-none pl-3 pr-3 ">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-4 h-4 opacity-50"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M19 9l-7 7-7-7"
                  />
                </svg>
              </div>

            </div>
          </div>
        }


        {!sAlone && <div className="sticky z-20  pt-3 pb-2  bg-white border-b border-gray-200 shadow-sm lg:relative top-sticky-menu md:top-0  -mx-5 md:mx-0  hidden lg:block">
          <MovieBookingRap
            data={master.Cineplexs}
            sCineplex={rapBrand}
          />
        </div>}


        {/* SUA TU DAY XUONG */}


        <div className="  relative ">
          {loading && (
            <div className="absolute top-0 left-0 z-12 flex items-start justify-center w-full h-full pt-20 bg-white bg-opacity-80 fadeIn">
              <Loader />
            </div>
          )}


          <div className="grid grid-cols-1 lg:grid-cols-3">
            <div
              className=" cinema-list-height hidden lg:block -mx-5 md:mx-0 divide-y divide-gray-100"
            >
              <div className="px-3 py-2 relative z-20">
                <input
                  type="text"
                  value={stext}
                  // onBlur={closeSearch}
                  onChange={(e) => handleInputSearch(e.target.value)}
                  placeholder="Tìm theo tên rạp ..."
                  className="items-center justify-center block w-full pl-3 pr-10 py-1 bg-gray-50 border border-gray-200 rounded h-9 text-sm"
                />

                <span
                  className="absolute border-none outline-none opacity-50 right-5 top-4"
                  aria-label="Search"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-4 h-4 "
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </span>

              </div>


              {rapSelect &&
                rapList.map((item, index) => (
                  <div
                    key={item.Id}
                    className={` ${item.ApiCinemaId == rapSelect.ApiCinemaId
                      ? " bg-pink-50 opacity-100"
                      : "cursor-pointer  md:hover:bg-gray-50"
                      }`}

                  >
                    <RapRowItem data={item} type={3} onClick={() => handleSelectRap(item)} />
                  </div>
                ))}



              {isLoadingRapList && (
                <div className=" md:px-4 py-4">
                  <ContentLoader
                    viewBox="0 0 250 80"
                    width={250}
                    height={80}
                    title="Loading news..."
                  >
                    <rect
                      x="0"
                      y="0"
                      rx="5"
                      ry="5"
                      width="58"
                      height="58"
                    />

                    <rect
                      x="75"
                      y="5"
                      rx="0"
                      ry="0"
                      width="80"
                      height="20"
                    />
                    <rect
                      x="75"
                      y="35"
                      rx="0"
                      ry="0"
                      width="220"
                      height="12"
                    />
                  </ContentLoader>
                </div>
              )}

              {isMore && (
                <div className="py-5 text-center">
                  <button
                    onClick={() => handelLoadMore()}
                    disabled={isLoadingRapList ? true : false}
                    type="button"
                    className="inline-block px-4 py-1 text-sm font-bold text-center text-pink-600 transition-all bg-opacity-50 border border-pink-500 rounded-3xl border-opacity-30 bg-pink-50 btn text-opacity-90 hover:bg-pink-100"
                  >
                    Xem thêm
                  </button>
                </div>
              )}

              {rapList != null && rapList.length == 0 && (
                <NotFoundCinema
                  title=" Không tìm thấy cụm rạp nào. "
                  content="Bạn hãy thử lại với phim khác hoặc rạp khác nha!"
                />
              )}
            </div>



            <div className="lg:col-span-2 md:border-l md:border-gray-200 cinema-list-height -mx-5 md:mx-0 ">

              {rapSelect &&
                <div className="border border-gray-200 rounded mx-5 my-1 md:mx-0 md:my-0 md:rounded-none md:border-0 md:border-b lg:border-gray-100 bg-gray-50 p-3 "
                >
                  <RapRowItem data={rapSelect} type={4} />
                </div>
              }

              {rapSelect &&
                <RapBooking data={rapSelect.ApiCinemaId} rapData={rapSelect} isMobile={isMobile} type={1} />
              }
            </div>

          </div>
        </div>

      </div>

      <Modal isOpen={modal} onDismiss={close} isFull={true}>
        <ModalHeader isBorder={true}>
          Danh sách rạp
        </ModalHeader>


        <ModalBody css="p-0 bg-white rounded relative">

          {!sAlone &&
            <div className="sticky z-30 block pt-3 pb-2  bg-white border-b border-gray-200 shadow-sm lg:relative top-0 ">
              <MovieBookingRap
                data={master.Cineplexs}
                sCineplex={rapBrand}
              />
            </div>}




          <div
            className=" cinema-list-height relative"
          >
            {loading && (
              <div className="absolute top-0 left-0 z-10 flex items-start justify-center w-full bottom-0 pt-20 bg-white bg-opacity-80 fadeIn">
                <Loader />
              </div>
            )}

            <div className="px-3 py-2 relative z-20">
              <input
                type="text"
                value={stext}
                // onBlur={closeSearch}
                onChange={(e) => handleInputSearch(e.target.value)}
                placeholder="Tìm theo tên rạp ..."
                className="items-center justify-center block w-full pl-3 pr-10 py-1 bg-gray-50 border border-gray-200 rounded h-9 "
              />

              <span
                className="absolute border-none outline-none opacity-50 right-5 top-4"
                aria-label="Search"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 "
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                  />
                </svg>
              </span>

            </div>

            <div className="divide-y divide-gray-100 ">
              {rapList != null && rapSelect &&
                rapList.length > 0 &&
                rapList.map((item, index) => (
                  <div
                    key={item.Id}
                    className={` ${item.ApiCinemaId == rapSelect.ApiCinemaId
                      ? " bg-pink-50 opacity-100"
                      : "cursor-pointer  md:hover:bg-gray-50"
                      }`}

                  >
                    <RapRowItem data={item} type={3} onClick={() => handleSelectRap(item)} />
                  </div>
                ))}

              {isLoadingRapList && (
                <div className=" md:px-4 py-4">
                  <ContentLoader
                    viewBox="0 0 250 80"
                    width={250}
                    height={80}
                    title="Loading news..."
                  >
                    <rect
                      x="0"
                      y="0"
                      rx="5"
                      ry="5"
                      width="58"
                      height="58"
                    />

                    <rect
                      x="75"
                      y="5"
                      rx="0"
                      ry="0"
                      width="80"
                      height="20"
                    />
                    <rect
                      x="75"
                      y="35"
                      rx="0"
                      ry="0"
                      width="220"
                      height="12"
                    />
                  </ContentLoader>
                </div>
              )}

              {isMore && (
                <div className="py-5 text-center">
                  <button
                    onClick={() => handelLoadMore()}
                    disabled={isLoadingRapList ? true : false}
                    type="button"
                    className="inline-block px-4 py-1 text-sm font-bold text-center text-pink-600 transition-all bg-opacity-50 border border-pink-500 rounded-3xl border-opacity-30 bg-pink-50 btn text-opacity-90 hover:bg-pink-100"
                  >
                    Xem thêm
                  </button>
                </div>
              )}

              {rapList != null && rapList.length == 0 && (
                <NotFoundCinema
                  title=" Không tìm thấy cụm rạp nào. "
                  content="Bạn hãy thử lại với phim khác hoặc rạp khác nha!"
                />
              )}
            </div>
          </div>


        </ModalBody>

        <ModalFooter className="flex">
          <div className="flex">
            <a
              onClick={close}
              className="flex items-center px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>

      </Modal>



      {isMD && (
        <style jsx global>{`
            .cinema-list-height {
              height: 500px;
              overflow-y: auto;
            }
        `}</style>
      )}

      {!isMD && (<style jsx global>{`
            .cinema-list-height {
              height: auto;
              min-height: 300px;
            }
        `}</style>)}
    </>
  );
};

export default CinemaSuggestFilm;
