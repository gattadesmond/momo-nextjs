import Link from "next/link";

import { useEffect, useState } from "react";

import dynamic from "next/dynamic";

import Image from "next/image";

import SojuSlider from "@/components/SojuSlider";

const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import lgThumbnail from "lightgallery/plugins/thumbnail";

const MovieGallery = ({ data }) => {
  const [isGallery, setIsGallery] = useState(false);

  let lightGallery = null;

  const onInitGallery = (e) => {
    lightGallery = e.instance;
  };

  const onOpenGallery = (number) => {
    lightGallery.openGallery(number);
  };

  let gallery = [];

  data.map((item, index) => {
    let hmm = {
      src: item,
      thumb: item,
    };
    gallery.push(hmm);
  });

  useEffect(() => {
    const timer = setTimeout(function () {
      setIsGallery(true);
    }, 200);
    return function cleanup() {
      clearTimeout(timer);
      setIsGallery(false);
    };
  }, [data]);

  return (
    <>
      <SojuSlider>
        <div className=" whitespace-nowrap block overflow-auto w-full mt-4">
          {data.map((item, index) => (
            <a
              key={index}
              data-lg-size="780-439"
              className=" inline-block gallery-item w-72 mx-1 movie-gallery-item cursor-pointer"
              data-src={item}
              onClick={() => onOpenGallery(index)}
            >
              <img className="img-responsive rounded" src={item} />
            </a>
          ))}
        </div>
      </SojuSlider>
      {isGallery && (
        <div className="movie-gallery hidden">
          <LightGallery
            speed={500}
            onInit={onInitGallery}
            dynamic={true}
            dynamicEl={gallery}
            plugins={[lgThumbnail]}
          />
        </div>
      )}
      <style jsx>{`
     
      `}</style>
    </>
  );
};

export default MovieGallery;
