import Link from "next/link";

import { useEffect, useState, useRef } from "react";

import { parseISO, format } from "date-fns";

import ReadMore from "@/components/ReadMore";

import UserAvatar from "@/components/cinema/UserAvatar";

import { useGetFilmCommentsQuery } from '@/slices/services/getFilmComments';

import InfiniteScroll from "react-infinite-scroll-component";

import MovieCommentItem from "@/components/cinema/MovieCommentItem";


const MovieCommentsVirtualList = ({ data }) => {

  const comments = data ?? null;

  const [items, setItems] = useState([]);
  const [start, setStart] = useState(1);
  const [hasMore, setHasMore] = useState(true);

  const timeout = useRef(null);

  useEffect(() => {
    if (comments) {
      setItems(comments.slice(0, 20))
    }
    return function clean() {
      setItems([]);
      setHasMore(true);
      setStart(1);
      clearTimeout(timeout.current);
    };
  }, [data])

  const fetchMoreData = () => {

    if (items.length == 0) return;
    if (items.length >= comments.length) {
      setHasMore(false);
      return;
    }
    setStart(start => start += 1);
    timeout.current = setTimeout(() => {
      setItems([...items, ...comments.slice(start * 20, start * 20 + 20)]);
    }, 1000);

  };

  return (
    <>
      {items && items.length > 0 && (
        <div className="  overflow-auto  px-4 md:px-6 pb-4 scrollable-div" id="scrollableDiv">
          <div className="grid grid-cols-1 gap-y-6 ">
            <InfiniteScroll
              dataLength={items.length}
              className="grid grid-cols-1 gap-y-6 mt-4"
              next={fetchMoreData}
              hasMore={hasMore}
              loader={<p className="py-4 text-gray-400 text-center">
                <span>Đang load ....</span>
              </p>}
              // height={500}
              endMessage={
                <p className="py-4 text-gray-400 text-center">
                  {/* <span>Yay! Đã hết</span> */}
                </p>
              }
              scrollableTarget="scrollableDiv"
            >
              {items.map((item, index) => (
                <div key={index} className="">

                  <MovieCommentItem data={item} />
                </div>
              ))}
            </InfiniteScroll>
          </div>
        </div>
      )}


      <style jsx>{`
        .scrollable-div{
          height: calc(100% - 44px);
          margin-top: 44px
        }
        `}</style>
    </>
  );
};

export default MovieCommentsVirtualList;
