import SwiperCore, { Navigation } from "swiper";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
// install Swiper modules
SwiperCore.use([Navigation]);

import { useEffect, useState } from "react";

import { useSelector, useDispatch } from "react-redux";
import { setRapBrand } from "@/slices/filmBooking";

const MovieBookingRap = ({ data }) => {
  const dispatch = useDispatch();


  const [swiperRap, setSwiperRap] = useState(null);

  const [selectedRap, setSelectedRap] = useState(0);

  const [selectedInt, setSelectedInt] = useState(0);

  //Check chon rap mac dinh
  useEffect(() => {
    return function cleanup() {
      dispatch(setRapBrand(null));
    };
  }, []);

  useEffect(() => {
    if (swiperRap != null) {
      swiperRap.slideTo(selectedInt - 1);
    }
  }, [selectedInt]);

  const handleSelectRap = (number, int) => {
    dispatch(setRapBrand(number || null));

    setSelectedRap(number);
    setSelectedInt(int);
  };

  return (
    <>
      <Swiper
        spaceBetween={2}
        slidesPerView={"auto"}
        onSwiper={setSwiperRap}
        className="px-5 lg:px-3"
        navigation={{
          nextEl: ".button-next",
          prevEl: ".button-prev",
        }}
      >
        <SwiperSlide
          className={`${
            selectedRap == 0 ? "is-active pointer-events-none" : ""
          } w-16`}
        >
          <div
            className={`cursor-pointer ${
              selectedRap == 0 ? "is-active " : ""
            }  `}
            onClick={() => handleSelectRap(0, 0)}
          >
            <div
              className={`relative mx-auto flex h-12 w-12 items-start justify-center rounded-lg border bg-white p-1 text-pink-500 ${
                selectedRap == 0 ? "border-pink-500 " : "border-gray-200"
              }`}
            >
              <Image
                src="https://static.mservice.io/next-js/_next/static/public/cinema/dexuat-icon.svg"
                alt="Đề xuất"
                layout="intrinsic"
                width={38}
                height={38}
              />
            </div>
            <div
              className={`mt-2  overflow-hidden text-ellipsis whitespace-nowrap text-center text-xs ${
                0 == selectedRap
                  ? "font-semibold text-pink-500 "
                  : "text-gray-500"
              }`}
            >
              Tất cả
            </div>
          </div>
        </SwiperSlide>

        {data.length > 0 &&
          data.map((item, index) => (
            <SwiperSlide
              className={`${
                item.Id == selectedRap ? "is-active pointer-events-none" : ""
              } w-16`}
              key={item.Id}
            >
              <div
                className={`relative cursor-pointer ${
                  item.Id == selectedRap ? "is-active" : ""
                }  `}
                onClick={() => handleSelectRap(item.Id, index)}
                key={item.Id}
              >
                <div
                  className={`mx-auto flex h-12 w-12 items-center justify-center overflow-hidden rounded-lg border bg-white relative ${
                    item.Id == selectedRap
                      ? "border-pink-500 "
                      : "border-gray-200"
                  }`}
                >
                  {item.FeaturedNote2 && (
                    <div className=" absolute h-3  right-1/4 top-1/4  translate-x-1/2  -translate-y-1/2 rotate-45  z-10 w-full   whitespace-nowrap bg-red-600/80 text-center text-[9px] font-semibold text-white/90">
                      {item.FeaturedNote2}
                    </div>
                  )}

                  <Image
                    src={item.Logo}
                    alt={item.Name}
                    layout="intrinsic"
                    width={38}
                    height={38}
                  />
                </div>

                <div
                  className={`mt-2 ml-1 overflow-hidden text-ellipsis whitespace-nowrap text-center text-xs ${
                    item.Id == selectedRap
                      ? "font-semibold text-pink-500 "
                      : "text-gray-500"
                  }`}
                >
                  {item.Name}
                </div>
              </div>
            </SwiperSlide>
          ))}

        <div className="button-next button-swiper absolute top-0 right-0 z-10 flex h-full w-12 cursor-pointer items-center justify-end pr-1 text-gray-500 ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 5l7 7-7 7"
            />
          </svg>
        </div>

        <div className="button-prev button-swiper absolute top-0 left-0 z-10 flex h-full w-12 cursor-pointer items-center pl-1 text-gray-500 ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 19l-7-7 7-7"
            />
          </svg>
        </div>
      </Swiper>

      <style jsx>{`
        .button-next {
          background: linear-gradient(
            90deg,
            rgba(255, 255, 255, 0) 0,
            rgba(255, 255, 255, 0.9) 30%,
            rgba(255, 255, 255, 1) 50%,
            #fff 100%
          );
        }

        .button-prev {
          background: linear-gradient(
            -90deg,
            rgba(255, 255, 255, 0) 0,
            rgba(255, 255, 255, 0.9) 30%,
            rgba(255, 255, 255, 1) 50%,
            #fff 100%
          );
        }
        .button-swiper.swiper-button-disabled {
          display: none;
        }
      `}</style>
    </>
  );
};

export default MovieBookingRap;
