import Link from "next/link";

import Image from "next/image";

import { useEffect, useState, useRef } from "react";

import dynamic from "next/dynamic";
const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import lgVideo from "lightgallery/plugins/video";

const MoviePoster = ({ img, name, trailer, banner, trailerYTB }) => {
  const [isGallery, setIsGallery] = useState(false);
  useEffect(() => {
    const timer = setTimeout(function () {
      setIsGallery(true);
    }, 200);
    return function cleanup() {
      clearTimeout(timer);
      setIsGallery(false);
    };
  }, [img]);

  return (
    <>
      <div className="overflow-hidden bg-gray-200 rounded shadow-md md:rounded-lg flex relative">
        <Image
          src={img}
          alt={name}
          layout="intrinsic"
          width={300}
          height={450}
          unoptimized={true}
        />

        {isGallery && (
          <LightGallery plugins={[lgVideo]} mode="lg-fade">
            <a
              data-lg-size="780-439"
              className="gallery-item"
              data-video={`{"source": [{"src":"${trailer}", "type":"video/mp4"}], "attributes": {"preload": false, "controls": true}}`}
              data-src={`${trailerYTB}`}
              data-thumb={`${banner}`}
              data-poster={`${banner}`}
              data-sub-html={`<h4>${name}</h4>`}
            >
              <div className="absolute z-10 w-10 h-10 pt-3 pb-3 pl-4 pr-3 transition-transform  -translate-x-1/2 -translate-y-1/2 bg-black border-2 border-white rounded-full cursor-pointer md:w-12 md:h-12 top-1/2 left-1/2 hover:scale-110 bg-opacity-40">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 25 31"
                  className="w-full h-full"
                  style={{ fill: "white" }}
                >
                  <path d="M22.79 17.02L3.934 29.436a2.327 2.327 0 01-3.607-1.944V2.662A2.327 2.327 0 013.934.718L22.79 13.133a2.327 2.327 0 010 3.888v-.001z" />
                </svg>
              </div>
            </a>
          </LightGallery>
        )}
      </div>

      <style jsx>{``}</style>
    </>
  );
};

export default MoviePoster;
