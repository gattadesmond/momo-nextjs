import Link from "next/link";

import { useEffect, useState } from "react";

import Image from "next/image";

import MovieLabel from "@/components/cinema/MovieLabel";

const MovieItem = ({ data, isDark, isNumber }) => {
  const {
    Id,
    url = data.Link,
    GraphicUrl,
    Title,
    TitleEn,
    ApiGenreName,
    averageScore = null,
    ApiRating,
    ApiSneakShowDate,
    ApiStatus,
    OpeningDate,
    ApiRatingPer,
  } = data;

  const dateNow = new Date();

  const openingDate = new Date(OpeningDate);

  const sneakShowDate =
    ApiSneakShowDate != null ? new Date(ApiSneakShowDate) : null;

  // const sneakShowDate  = ApiSneakShowDate != null ? new Date("2021-07-01 00:00:00") : null;

  const isBadgeSneakshow = !(
    ApiStatus == 3 &&
    sneakShowDate != null &&
    dateNow <= sneakShowDate
  )
    ? false
    : true;

  const isBook = !(
    ApiStatus == 3 &&
    sneakShowDate != null &&
    dateNow > sneakShowDate &&
    dateNow < openingDate
  )
    ? false
    : true;

  const convertGenre = ApiGenreName ? ApiGenreName.split(", ").slice(0, 2).join(", ") : null;
  return (
    <>
      <Link href={url}>
        <a className="block group relative">
          <div className="relative z-10 overflow-hidden rounded background-gray-100">
            <div className="absolute inset-0 z-20 px-2 py-2 bg-transparent overlay ">
              <div className="flex flex-row space-x-2 flex-nowrap ">
                <MovieLabel data={ApiRating} />

                {isBook && <img src="https://static.mservice.io/next-js/_next/static/public/cinema/icon-dattruoc.svg" />}

                {isBadgeSneakshow && <img src="https://static.mservice.io/next-js/_next/static/public/cinema/icon-sneakshow.svg" />}
              </div>
            </div>
            <div
              className={` flex  ${isDark ? "bg-black" : "bg-gray-200"
                }`}
            >
              <Image
                src={GraphicUrl}
                alt={TitleEn}
                layout="intrinsic"
                width={300}
                height={450}
                className="transition-transform duration-300  scale-100 group-hover:scale-105 "
              // unoptimized={true}
              />
            </div>
          </div>

          {isDark && isNumber && (
            <div
              className=" text-4xl md:text-5xl font-bold text-white font-inter absolute -bottom-2 left-0 text-opacity-90 z-10 drop-shadow"
            >
              {isNumber}
            </div>
          )}
        </a>
      </Link>

      <div className="mt-2 md:mt-4">
        <div
          className={`font-semibold leading-tight  ${isDark ? "text-white" : "text-gray-800"
            }`}
        >
          <Link href={url}>
            <a className="">{Title}</a>
          </Link>
        </div>
        <div className="mt-1 leading-tight text-gray-400 text-tiny">
          {convertGenre}
        </div>
        {ApiRatingPer != 0 && (
          <>
            <div className={`flex items-center mt-2  text-tiny ${isDark ? "text-gray-200" : "text-gray-600"}`}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-1 text-green-500"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5"
                />
              </svg>{" "}
              {ApiRatingPer}%
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default MovieItem;
