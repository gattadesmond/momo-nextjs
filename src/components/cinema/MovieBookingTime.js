import { useSelector, useDispatch } from "react-redux";

import { useEffect, useState } from "react";


import MovieBookingStep from "@/components/cinema/MovieBookingStep";


function movieTimeConvertZero(text) {
  const [start, end] = text.split(":");
  let q = "";
  if (start.length == 1) {
    q = q.concat(`0${start}:`);
  } else {
    q = q.concat(`${start}:`);
  }

  if (end.length == 1) {
    q = q.concat(`0${end}`);
  } else {
    q = q.concat(`${end}`);
  }

  return q;
}

const MovieBookingTime = ({ gioChieu }) => {
  const [modal, setModal] = useState(false);

  const [start, end] =gioChieu?.ShowTimeDuration?.split(" ~ ");


  const handleSelectTime = () => {
    setModal(true);
  };
  return (
    <>
      <div
        className="text-tiny group cursor-pointer whitespace-nowrap rounded-md border border-gray-200 bg-gray-50 px-2 py-1 text-center text-gray-400 hover:bg-white hover:text-pink-600"
        onClick={() => handleSelectTime()}
      >
        <strong className="text-md font-semibold text-gray-800 group-hover:text-pink-600">
          {movieTimeConvertZero(start)}
        </strong>{" "}
        ~ {movieTimeConvertZero(end)}
      </div>
      {modal && (
        <MovieBookingStep
          dissmiss={() => {
            setModal(false);
          }}
          isOpen={modal}
          gioChieu={gioChieu}
        />
      )}
    </>
  );
};

export default MovieBookingTime;
