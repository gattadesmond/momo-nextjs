import Link from "next/link";

import { useEffect, useState } from "react";

import Image from "next/image";
import MovieType from "@/components/cinema/MovieType";

import MovieBookingTime from "@/components/cinema/MovieBookingTime";

import RapRowItem from "@/components/cinema/RapRowItem";

import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
} from "@reach/accordion";

const RapBookingList = ({ rapList = null }) => {
  if (!rapList) return null;
  return (
    <div>
      <Accordion className="normal-accordion divide-y divide-gray-200">
        {rapList.map((item, index) => (
          <AccordionItem key={item.Id} className="">
            <div className="relative mx-0 block py-3 hover:bg-gray-50 md:px-4 ">
              <AccordionButton className="absolute inset-0 block w-full"></AccordionButton>

              <RapRowItem data={item} type={2} />
            </div>
            <AccordionPanel>
              <>
                {item.VersionsCaptions.map((child, index) => (
                  <div key={index} className="px-0 pb-5 md:px-4">
                    <div className="mb-2 text-sm font-bold ">
                      <MovieType
                        vtype={child.VersionType}
                        ctype={child.CaptionType}
                      />
                    </div>
                    <div className="grid grid-cols-3 gap-3 md:grid-cols-5 ">
                      {child.ShowTimes.map((i, index) => (
                        <MovieBookingTime
                          gioChieu={i}
                          key={index}
                        />
                      ))}
                    </div>
                  </div>
                ))}
              </>
            </AccordionPanel>
          </AccordionItem>
        ))}
      </Accordion>
    </div>
  );
};
export default RapBookingList;
