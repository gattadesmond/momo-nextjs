import Link from "next/link";

import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

import MovieCommentsModal from "@/components/cinema/MovieCommentsModal";

import { useGetFilmCommentsQuery } from '@/slices/services/getFilmComments'

const MovieRating = ({ id }) => {
  // const [rating, setRating] = useState(null);

  const { data: filmComments, error, isLoading } = useGetFilmCommentsQuery(id);

  // const comments = filmComments?.Data[0]?.Comment ?? null;

  const score = filmComments?.Data?.[0]?.Rating?.MedalRating ?? null;

  const listMedalRatings = score ? score.medalRatings.filter((item) => item.isShow == true)
    .sort((a, b) => (a.total > b.total ? -1 : 1)).slice(0, 4) : null;

  // const listMedalRatings = null;

  // const [score, setScore] = useState(null);

  const [modal, setModal] = useState(false);

  // const [listMedalRatings, setListMedalRatings] = useState(null);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  // useEffect(() => {
  //   axios.get(`/ci-film/rating/${id}`).then(function (response) {
  //     if (!response.Result) return;
  //     const res = response.Data[0];

  //     // setRating(res);
  //     setScore(res.Rating.MedalRating);

  //     if (res.Rating.MedalRating && res.Rating.MedalRating.medalRatings != null && res.Rating.MedalRating.medalRatings.length > 0) {
  //       let list = res.Rating.MedalRating.medalRatings;
  //       list = list
  //         .filter((item) => item.isShow == true)
  //         .sort((a, b) => (a.total > b.total ? -1 : 1)).slice(0, 4);

  //       setListMedalRatings(list);
  //     }
  //   });
  // }, [id]);

  return (
    <>
      {score && score.medal.total > 0 && (
        <div className="relative z-20 px-4 py-5 bg-white border border-gray-200 rounded-lg shadow-sm md:shadow-md">
          {/* <div className="mx-auto text-lg font-semibold text-center">
            Thang điểm đánh giá
          </div> */}

          <div className="flex flex-nowrap">
            <div className={"text-center mr-2 px-1 " + (listMedalRatings != null && listMedalRatings.length > 0 ? '' : 'w-full')}>
              <div className="">
                <img src={score.medal.image} className="block w-16 mx-auto " />
              </div>

              <div className="mt-2 text-xl font-semibold whitespace-nowrap">
                {score.medal.per}%
              </div>

              <div className="mt-0 text-gray-500 text-sm whitespace-nowrap">
                {score.medal.total} đánh giá
              </div>

              <div className="mt-2">
                <button className="inline-block px-2 py-1 text-sm font-bold  border border-pink-500 text-center  transition-all bg-white text-pink-600 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-100 whitespace-nowrap" onClick={() => setModal(true)}>
                  Xem đánh giá
                </button>
              </div>
            </div>

            <div className="grid grid-cols-1 gap-y-2 text-md">
              {listMedalRatings != null && listMedalRatings.map((item, index) =>
                <div className="flex flex-nowrap space-x-2 items-center" key={index}>
                  <img src={item.image} alt={item.name} className="w-8 h-8" />
                  <div>{item.name}</div>
                </div>
              )}
            </div>

            <style jsx>{``}</style>
          </div>
        </div>
      )}
      <MovieCommentsModal isOpen={modal} onDismiss={close} id={id} />
    </>
  );
};

export default MovieRating;
