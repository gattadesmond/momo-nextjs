import Link from "next/link";

import Image from "next/image";

const ActorItem = ({ data }) => {
  return (
    <>
      <div key={data.id} className="text-center ">
        <div className="w-20 h-20 mx-auto">
          <div className="overflow-hidden bg-gray-200 rounded-full aspect-w-2 aspect-h-2">
            {data.picture &&   <Image
              src={data.picture}
              alt={data.name}
              layout="fill"
              objectFit="cover"
            />}
          
          </div>
        </div>

        <div className="mt-1 mb-1 text-sm leading-tight ">{data.name}</div>
        <div className="text-xs text-gray-500">{data.character}</div>
      </div>
    </>
  );
};

export default ActorItem;
