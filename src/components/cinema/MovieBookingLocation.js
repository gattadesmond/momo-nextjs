import Link from "next/link";

import { useEffect, useState, memo } from "react";
import { axios } from "@/lib/index";
import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";
import Toast from "@/components/Toast";

import { useSelector, useDispatch } from "react-redux";
import { setCityId, setIsNearBy, setGeo } from "@/slices/filmBooking";

function removeAccents(str) {
  return str
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/đ/g, "d")
    .replace(/Đ/g, "D");
}

const MovieBookingLocation = ({ data }) => {
  const dispatch = useDispatch();

  const isNearby = useSelector((state) => state.filmBooking.isNearby);

  const [modal, setModal] = useState(false);

  const [loading, setLoading] = useState(false);

  const [city, setCity] = useState(null);

  const [cityName, setCityName] = useState(null);

  const [stext, setStext] = useState("");

  const [tinhthanh, setTinhthanh] = useState(data);

  const tinhthanhArray = data;

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  useEffect(() => {
    //Check neu chua co location thi chon 50 - TP HCM
    let rememberCityID = localStorage.getItem("rememberCityID");

    let rememberIsNearBy = localStorage.getItem("rememberIsNearBy");

    let rememberGeo = JSON.parse(localStorage.getItem("rememberGeo"));
    if (rememberIsNearBy == "true" && rememberGeo) {
      dispatch(setIsNearBy(true));
      dispatch(
        setGeo({
          lat: rememberGeo.lat,
          lng: rememberGeo.lng,
        })
      );
    }

    if (!rememberCityID) {
      localStorage.setItem("rememberCityID", 50);
      rememberCityID = 50;
    }

    setCity(rememberCityID);

    dispatch(setCityId(rememberCityID || null));

    //give parent location ID
  }, []);

  useEffect(() => {
    // function getCoords() {
    //   return new Promise((resolve, reject) =>
    //     navigator.permissions
    //       ? // Permission API is implemented
    //         navigator.permissions
    //           .query({
    //             name: "geolocation",
    //           })
    //           .then((permission) =>
    //             // is geolocation granted?
    //             permission.state === "granted"
    //               ? navigator.geolocation.getCurrentPosition((pos) =>
    //                   resolve(pos.coords)
    //                 )
    //               : resolve(null)
    //           )
    //       :
    //         reject(new Error("Permission API is not supported"))
    //   );
    // }
    // getCoords().then((coords) => {
    //   if (coords == null) return;
    //   selectGeoLocation();
    // });
  }, []);

  useEffect(() => {
    const hmm = tinhthanh.filter((item) => item.Id == city);
    if (hmm.length > 0) {
      setCityName(hmm[0].Name);
    }
  }, [city]);

  const handleSelectLocation = (val) => {
    setCity(val);

    dispatch(setIsNearBy(false));

    localStorage.setItem("rememberCityID", 50);

    localStorage.setItem("rememberIsNearBy", false);

    dispatch(setCityId(val || null));

    setModal(false);
  };

  const handleSearchCity = (val) => {
    setStext(val);

    if (val == "") {
      setTinhthanh(data);
    } else {
      let hmm = tinhthanhArray.filter((item, index) =>
        removeAccents(item.Name.toLowerCase()).includes(
          removeAccents(val.toLowerCase())
        )
      );
      setTinhthanh(hmm);
    }
  };

  //GEO location dang loi chua bit sua

  function selectGeoLocation() {
    setLoading(true);
    if (!navigator.geolocation) {
      console.log("Geolocation is not supported by your browser");
    } else {
      const onSuccess = (location) => {
        dispatch(setIsNearBy(true));
        localStorage.setItem("rememberIsNearBy", true);
        dispatch(
          setGeo({
            lat: location.coords.latitude,
            lng: location.coords.longitude,
          })
        );

        localStorage.setItem(
          "rememberGeo",
          JSON.stringify({
            lat: location.coords.latitude,
            lng: location.coords.longitude,
          })
        );

        axios
          .get(
            `/city/nearby?lat=${location.coords.latitude}&lon=${location.coords.longitude}&lastIndex=0&count=1&sortType=1&sortDir=1`
          )
          .then(function (response) {
            const res = response.Data ?? null;

            setCity(res?.Items[0].Id ?? 50);

            localStorage.setItem("rememberCityID", res?.Items[0].Id ?? 50);

            dispatch(setCityId(res?.Items[0].Id ?? 50));
          });

        setTimeout(function () {
          setLoading(false);
        }, 500);
      };

      const options = {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 1000 * 60 * 60,
      };

      const onError = (error) => {
        Toast(
          "Bạn cần cho phép website sử dụng thông tin vị trí để sử dụng tính năng này"
        );

        setTimeout(function () {
          setLoading(false);
        }, 500);
      };

      navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    }
  }

  return (
    <>
      <div className="flex items-center space-x-3">
        <div
          className={`relative flex h-9  w-40 flex-none cursor-pointer flex-nowrap items-center rounded border pr-4 pl-7 text-sm font-semibold hover:bg-gray-50 md:w-40 ${
            !isNearby
              ? "border-pink-600 text-pink-600"
              : "cursor-pointer border-gray-300 text-gray-600 hover:bg-gray-50"
          }`}
          onClick={() => setModal(true)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="absolute left-2 top-1 mt-1 h-4 w-4"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
            />
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
            />
          </svg>{" "}
          <span className="block min-w-0 overflow-hidden text-ellipsis whitespace-nowrap">
            {cityName}
          </span>
          <div className="absolute right-3 md:block">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4 opacity-50"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M19 9l-7 7-7-7"
              />
            </svg>
          </div>
        </div>
        <div className="flex-none">
          <div
            className={`relative flex h-9 w-28 flex-nowrap  items-center  rounded  border pl-7 text-sm font-semibold  ${
              isNearby
                ? "border-pink-600 text-pink-600 cursor-pointer"
                : "cursor-pointer border-gray-300 text-gray-800 hover:bg-gray-50"
            }`}
            onClick={() => !loading && selectGeoLocation()}
          >
            {!loading ? (
              <svg
                className="absolute left-2 top-1 mt-1 h-4 w-4"
                focusable="false"
                viewBox="0 0 24 24"
                aria-hidden="true"
                fill="currentColor"
                tabIndex={-1}
                title="MyLocation"
                data-ga-event-category="material-icons"
                data-ga-event-action="click"
                data-ga-event-label="MyLocation"
              >
                <path d="M12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm8.94 3c-.46-4.17-3.77-7.48-7.94-7.94V1h-2v2.06C6.83 3.52 3.52 6.83 3.06 11H1v2h2.06c.46 4.17 3.77 7.48 7.94 7.94V23h2v-2.06c4.17-.46 7.48-3.77 7.94-7.94H23v-2h-2.06zM12 19c-3.87 0-7-3.13-7-7s3.13-7 7-7 7 3.13 7 7-3.13 7-7 7z" />
              </svg>
            ) : (
              <div className="loader absolute left-2 top-1 mt-1 animate-spin"></div>
            )}
            Gần bạn
          </div>
        </div>
      </div>

      <Modal isOpen={modal} onDismiss={close} isFull={true}>
        <ModalHeader isBorder={false}> &nbsp; </ModalHeader>
        <ModalBody css="p-0 bg-white rounded">
          <div className="px-4 md:px-10">
            <div className="mb-6 flex items-center justify-between">
              <div className="mr-5 whitespace-nowrap text-xl font-bold text-gray-700">
                Chọn địa điểm
              </div>

              <div className="relative ">
                <input
                  type="text"
                  value={stext}
                  // onBlur={closeSearch}
                  onChange={(e) => handleSearchCity(e.target.value)}
                  placeholder="Tìm địa điểm ..."
                  className="block h-9 w-full items-center justify-center rounded border border-gray-300 bg-white px-3 py-1"
                />

                <button
                  type="submit"
                  className="absolute right-2 top-2 border-none opacity-50 outline-none"
                  aria-label="Search"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5 "
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </button>
              </div>
            </div>

            <div className="my-3 grid grid-cols-2 gap-1 md:my-5 md:grid-cols-4 md:gap-3 ">
              {tinhthanh.map((item, index) => (
                <div
                  className={`text-md  cursor-pointer whitespace-nowrap rounded-md border px-3 py-2 ${
                    item.Id == city
                      ? " border-pink-700 font-semibold text-pink-700"
                      : "border-transparent text-gray-700 hover:bg-gray-100"
                  }
                      `}
                  onClick={() => handleSelectLocation(item.Id)}
                  key={item.Id}
                >
                  {item.Name}
                </div>
              ))}
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn inline-block rounded-md bg-pink-700 px-4 py-2 text-center text-sm font-bold text-white text-opacity-90 transition-all hover:bg-pink-800"
            onClick={close}
          >
            Đóng
          </button>
        </ModalFooter>
      </Modal>

      <style jsx>{`
        .loader {
          border: 2px solid var(--gray-300); /* Light grey */
          border-top: 2px solid var(--pinkmomo); /* Blue */
          border-radius: 50%;
          width: 15px;
          height: 15px;
        }
      `}</style>
    </>
  );
};

export default memo(MovieBookingLocation);
