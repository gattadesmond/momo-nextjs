import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Image from "next/image";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

import StickyBox from "react-sticky-box";

import Container from "@/components/Container";
import Page from "@/components/page";

import ReplaceURL from "@/lib/replace-url";
import ViewFormat from "@/lib/view-format";

import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_QA from "@/components/LdJson_QA";
import ArticleRating from "@/components/blog/ArticleRating";

import ArticleContent from "@/components/article/ArticleContent";

import ArticleMenu from "@/components/article/ArticleMenu";
import PreferList from "@/components/article/PreferList";
import RelateList from "@/components/article/RelateList";

import CategoryInfo from "@/components/article/CategoryInfo";

import FooterCta from "@/components/FooterCta";

import ButtonShare from "@/components/ui/ButtonShare";

import ButtonAction from "@/components/ui/ButtonAction";
const HelpArticleGroup = dynamic(() =>
  import("@/components/article/Article/HelpArticleGroup")
);

function PageGuide({
  dataPostDetail,
  dataCatePost,
  listPrefer,
  pageMaster,
  isMobile,
}) {
  const data = dataPostDetail.Data;
  const dataCategories = dataCatePost.Data;

  const router = useRouter();

  const [isViewApp, setIsViewApp] = useState(false);

  useEffect(() => {
    var _isViewApp =
      data.ViewInApp && router?.query?.view == "app" ? true : false;
    if (_isViewApp) setIsViewApp(_isViewApp);
  }, [router]);

  useEffect(() => {
    try {
      window.ReactNativeWebView.postMessage("GetUserInfo");
      if (data.ViewInApp) setIsViewApp(true);
    } catch {}
  }, []);

  useEffect(() => {
    document.querySelector("body").classList.add("page-article");

    return function clean() {
      document.querySelector("body").classList.remove("page-article");
    };
  }, []);

  return (
    <>
      <Page
        className="article-detail"
        title={data.Meta?.Title}
        description={data.Meta?.Description}
        image={data.Meta?.Avatar}
        keywords={data.Meta?.Keywords}
        robots={data.Meta?.Robots}
        header={pageMaster.Data?.MenuHeaders}
        isMobile={isMobile}
        isViewApp={isViewApp}
        link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${data.Link}`}
        Trace={{ Id: data.Id, Type: "guide" }}
      >
        <LdJson_Meta_NewsArticle Meta={data.Meta} />
        <LdJson_QA QaData={data.QaData} QaGroupData={data.QaGroupData} />

        <ArticleMenu
          root={{
            link: "/huong-dan",
            title: "Hướng dẫn",
            icon: "https://static.mservice.io/next-js/_next/static/public/article/Tinmoi.svg",
          }}
          data={dataCategories}
          cat={data.Category}
        />

        <Container>
          <div className="pb-6">
            <div className="lg:grid lg:grid-flow-col lg:grid-cols-12 lg:gap-6 mt-5 lg:mt-6">
              <article className="w-full mx-auto lg:col-span-8">
                <h1

                  className={`mb-4 font-bold tracking-tight text-momo  md:leading-tight`}
                >
                  {data.Title}
                </h1>
                <div className="flex items-center mb-5">
                  <div className="flex-1 flex flex-col md:flex-row flex-wrap md:items-center text-gray-500 text-sm">
                    <>
                      <CategoryInfo
                        isViewApp={isViewApp}
                        catId={data.Category.Id}
                        catName={data.Category.Name}
                        catLink={data.Category.Link}
                        isViewApp={isViewApp}
                      />
                      {/* <span className="mx-1 hidden md:inline">·</span> */}
                    </>
                    <div className="flex items-center">
                      {/* <span className="relative">{data.PublishDate}</span> */}
                      {/* {data.TimeReadAvg && (
                        <>
                          <span className="mx-1">·</span>
                          <span>{data.TimeReadAvg} phút đọc</span>
                        </>
                      )} */}
                      {data.TotalViews > 500 && (
                        <>
                          <span className="mx-1">·</span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-4 w-4 mr-1 relative block md:hidden"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            style={{ top: "-1px" }}
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                            />
                          </svg>{" "}
                          <ViewFormat data={data.TotalViews} />
                          <span className="hidden md:block">
                            &nbsp;lượt xem
                          </span>
                        </>
                      )}
                    </div>
                  </div>
                  <div className=" shrink-0">
                    <ButtonShare
                      link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${data.Link}`}
                      isViewApp={isViewApp}
                    />
                  </div>
                </div>
                <p className="article-desc">{data.Short}</p>

                {data.Avatar && (
                  <div className="pointer-events-none select-none mt-4 mb-5">
                    <Image
                      src={data.Avatar}
                      alt={data.Title}
                      layout="responsive"
                      width={770}
                      height={370}
                      loading="eager"
                    />
                  </div>
                )}
                {data?.GuideGroupDataEx?.length > 0 && (
                  <div className="mt-4">
                    {data.GuideGroupDataEx.map((guide, idx) => {
                      return (
                        <HelpArticleGroup
                          key={idx}
                          data={guide}
                          isShowTitle={false}
                          isMobile={isMobile}
                          isMockup={data.IsMockup}
                        />
                      );
                    })}
                  </div>
                )}

                {data?.Ctas?.length > 0 && (
                  <div className="flex justify-center mt-10">
                    <div className="flex">
                      {data.Ctas.map((cta, idx) => {
                        return (
                          <ButtonAction
                            key={idx}
                            className="mx-auto"
                            cta={cta}
                            isMobile={isMobile}
                          />
                        );
                      })}
                    </div>
                  </div>
                )}

                <div className="article-content mt-4">
                  <ArticleContent
                    isMobile={isMobile}
                    data={data.Content}
                    ads={data.AdsData}
                    cta={data.CtaData}
                    qa={data.QaData}
                    qaGroup={data.QaGroupData}
                    guide={data.GuideData}
                    guideGroup={data.GuideGroupData}
                    isViewApp={isViewApp}
                  />
                </div>

                {data && data.Meta && (
                  <ArticleRating
                    Id={data.Id}
                    Type={5}
                    Count={data.Meta?.RatingCount}
                    Value={data.Meta?.RatingValue}
                  />
                )}
              </article>
              {!isViewApp && (
                <div className={`relative z-10 w-full lg:w-80`}>
                  <StickyBox offsetTop={60} offsetBottom={20}>
                    <div className="pt-5 mt-3 rounded-lg lg:mt-0 bg-white lg:px-0 lg:py-0">
                      <div className="border-l-0 lg:border-l border-gray-300 lg:pl-6">
                        {data.Relatings && (
                          <RelateList
                            title="Hướng dẫn liên quan"
                            isShowCat={false}
                            className=""
                            data={data.Relatings.Items}
                            linkViewMore={data.Category.Link}
                          />
                        )}
                      </div>
                    </div>
                  </StickyBox>
                </div>
              )}
            </div>
          </div>

          <div className="py-6 md:py-8">
            <PreferList className="" data={listPrefer.Data?.Items} type={1} />
          </div>
        </Container>

        {data?.Ctas?.length > 0 && (
          <FooterCta
            data={data.Ctas.map((x) => {
              return {
                TypeName: "CtaFooter",
                Template: 1,
                Cta: {
                  Link: x.Link,
                  Text: x.Text,
                  NewTab: x.NewTab,
                  RedirectUC: x.RedirectUC,
                  QrCodeId: x.QrCodeId,
                },
                Content: "",
              };
            })}
            isMobile={isMobile}
          />
        )}

        {isViewApp && data?.ViewInAppConfig?.Ctas?.length > 0 && (
          <FooterCta
            data={data.ViewInAppConfig.Ctas.map((x) => {
              return {
                TypeName: "CtaFooter",
                Template: 1,
                Cta: {
                  Link: x.Link,
                  Text: x.Text,
                  NewTab: x.NewTab,
                  RedirectUC: x.RedirectUC,
                  QrCodeId: x.QrCodeId,
                },
                Content: "",
              };
            })}
            isMobile={isMobile}
          />
        )}
      </Page>
    </>
  );
}

export default PageGuide;
