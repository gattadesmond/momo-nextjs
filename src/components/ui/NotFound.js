function NotFound({ children }) {
  return (
    <div className="w-full max-w-6xl px-5 mx-auto md:px-8 lg:px-8 ">
      {children}
    </div>
  );
}

export default NotFound;
