import Link from "next/link";
import Image from "next/image";

import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { axios } from "@/lib/index";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";
import ButtonAction from "@/components/ui/ButtonAction";

import ViewFormat from "@/lib/view-format";

import BlogAjaxLoad from "@/components/ui/BlogAjaxLoad";
import { appSettings } from "@/configs";

function BlogBox1({ data, isModal, isMobile = false }) {
  const { Url, Id } = data;

  const [article, setArticle] = useState(null);
  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  const handleShow = () => {
    setModal(true);
    setLoading(true);
    axios.get(`/blog/${Id}`).then(function (response) {
      const res = response.Data;
      setLoading(false);
      setArticle(res);

      var _isViewApp =
        res.ViewInApp && router?.query?.view == "app" ? true : false;
      if (_isViewApp) setIsViewApp(_isViewApp);

      var img = document.createElement("img");
      img.setAttribute(
        "src",
        `${appSettings.AppConfig.HOST_API}/trace/blog/${res.Id}`
      );
      img.setAttribute(
        "style",
        "position: absolute;display: block;z-index: -10000"
      );
      document.body.appendChild(img);

    });
  };

  function Content({ data }) {
    const { Avatar, Short, Title, DateShow, TotalViews } = data;
    return (
      <div className="flex flex-wrap pb-3 border-b border-gray-200 sm:border-none sm:pb-0 ">
        <div className="order-2 pt-3 sm:flex-auto sm:w-full w-28 sm:order-1 sm:pt-0">
          <div className="overflow-hidden bg-gray-100 rounded relative flex ">
            <Image
              src={Avatar}
              alt={Title}
              layout="intrinsic"
              width={462}
              height={222}
              className="w-full"
              loading="lazy"
            />
          </div>
        </div>

        <div className="flex-1 order-1 pr-6 sm:pr-0 sm:flex-auto sm:w-full sm:order-2">
          <div
            className="pt-3 pb-2 font-semibold leading-snug group-hover:text-pink-600"
            dangerouslySetInnerHTML={{ __html: Title }}
          ></div>

          <div className="text-xs text-gray-500">
            {/* {DateShow && <>{format(parseISO(DateShow), "dd/MM/yyyy")}</>} */}
            {TotalViews >= 500 && (
              <>
                {/* <span> - </span> */}
                <ViewFormat data={TotalViews} /> lượt xem
              </>
            )}
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className="h-full cursor-pointer group ">
        {isModal ? (
          <div onClick={handleShow}>
            <Content data={data} />
          </div>
        ) : (
          <Link href={Url}>
            <a>
              <Content data={data} />
            </a>
          </Link>
        )}
      </div>

      <Modal isOpen={modal} onDismiss={close} isFull={true} isBig={true}>
        <ModalHeader>Blog MoMo</ModalHeader>

        <ModalBody css="p-0 bg-white rounded">
          <div className="p-5 md:p-7">
            <BlogAjaxLoad
              isMobile={isMobile}
              article={article}
              loading={loading}
              isViewApp={isViewApp}
            />
          </div>
        </ModalBody>
        <ModalFooter
          className={`space-x-3 ${
            article?.ViewInApp && article?.ViewInAppConfig?.Ctas?.length > 0
              ? "justify-center"
              : ""
          }`}
        >
          {!loading &&
            (article?.ViewInApp &&
            article?.ViewInAppConfig?.Ctas?.length > 0 ? (
              article.ViewInAppConfig.Ctas.map((cta, idx) => {
                return (
                  <div
                    key={idx}
                    className="flex items-start justify-center flex-1 md:flex-none    "
                  >
                    <ButtonAction
                      className="w-full whitespace-nowrap"
                      isViewApp={isViewApp}
                      classNameRoot=" flex  md:grow-0"
                      cta={cta}
                    />
                  </div>
                );
              })
            ) : (
              <div className="flex items-start justify-end flex-1 md:flex-none  ">
                <a
                  onClick={close}
                  className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
                >
                  Đóng
                </a>
              </div>
            ))}
        </ModalFooter>
      </Modal>
    </>
  );
}

export default BlogBox1;
