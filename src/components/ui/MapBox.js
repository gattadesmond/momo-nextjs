

import dynamic from 'next/dynamic';
import { useState, useEffect, useCallback, useRef } from "react";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

const MapBoxLoad = dynamic(() => import('@/components/ui/MapBoxLoad'));


function GGMap({ children, lat, lon, address, name }) {
  if (!lat || !lon) {
    return <></>;
  }

  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
  };

  return (
    <>
      <a
        className="inline-block  text-blue-500 whitespace-nowrap relative z-10 hover:text-blue-700 cursor-pointer btn-gg"
        onClick={() => setModal(true)}
      >
        {children}
      </a>

      <Modal isOpen={modal} onDismiss={close} isFull={true}>
        <ModalHeader> Bản đồ </ModalHeader>

        <ModalBody css="p-0 bg-white rounded gg-map">
          <div className="flex h-full w-full relative">
            <MapBoxLoad lat={lat} lon={lon} address={address} name={name} />

            <div className="flex space-x-2 absolute bottom-2 right-2 z-10">
              {typeof navigator !== "undefined" &&
                navigator.userAgent.includes("Mac") && (
                  <a
                    className="shadow flex flex-nowrap items-center px-3 py-1 text-sm text-center  transition-all bg-blue-50 rounded-md cursor-pointer btn text-opacity-60 hover:bg-blue-100 "
                    target="_blank"
                    href={` http://maps.apple.com/?destination=${lat},${lon}`}
                  >
                    <img
                      src="https://static.mservice.io/next-js/_next/static/public/cinema/apple-map-icon.svg"
                      className="w-6 mr-2"
                    />{" "}
                    <div className="text-gray-500 text-left leading-4">
                      <div className="text-xs ">Tìm đường bằng</div>{" "}
                      <div className="text-gray-900"> Apple Map</div>
                    </div>
                  </a>
                )}

              <a
                className="shadow flex flex-nowrap items-center px-3 py-1 text-sm  text-center  transition-all bg-blue-50 rounded-md cursor-pointer btn text-opacity-60 hover:bg-blue-100  "
                target="_blank"
                href={`https://www.google.com/maps/dir/?api=1&destination=${lat},${lon}`}
              >
                <img src="https://static.mservice.io/next-js/_next/static/public/cinema/google-map-icon.svg" className="w-6 mr-2" />

                <div className="text-gray-500 text-left leading-4">
                  <div className="text-xs ">Tìm đường bằng</div>{" "}
                  <div className="text-gray-900">Google Map</div>
                </div>
              </a>
            </div>
          </div>
        </ModalBody>
        <ModalFooter className="flex">
          <div className="flex">
            <a
              onClick={close}
              className="flex items-center px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>
      </Modal>

      <style jsx global>{`
        .mapboxgl-popup {
          top: -40px;
        }
        .mapboxgl-popup-close-button {
          padding: 4px 8px;
        }

        .map-container {
          height: 100%;
          width: 100%;
        }

      `}</style>
    </>
  );
}

export default GGMap;
