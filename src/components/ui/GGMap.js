import Link from "next/link";
import Image from "next/image";

import { useState, useEffect, useCallback } from "react";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

import {
  GoogleMap,
  useJsApiLoader,
  Marker,
  InfoWindow,
} from "@react-google-maps/api";

function SojuMap({ center, address, name }) {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: "AIzaSyA7yM33-6kHWHpmrh1IlyOGP_Ha96Oe7hk",
  });

  const containerStyle = {
    width: "100%",
    height: "100%",
  };

  const [map, setMap] = useState(null);

  const onLoad = useCallback(function callback(map) {
    setMap(map);
  }, []);

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={16}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      <InfoWindow onLoad={onLoad} position={center}>
        <div className=" w-52 ">
          <div className="font-bold text-sm">{name}</div>
          <div className="mt-1 text-xs text-gray-500 overflow-hidden text-ellipsis whitespace-nowrap">
            {address}
          </div>
        </div>
      </InfoWindow>

      <Marker onLoad={onLoad} position={center} />
    </GoogleMap>
  ) : (
    <></>
  );
}

function GGMap({ children, lat, lon, address, name }) {
  if (!lat || !lon) {
    return <></>;
  }
  // return <></>;

  const center = {
    lat: lat,
    lng: lon,
  };

  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
  };

  return (
    <>
      <a
        className="inline-block  text-blue-500 whitespace-nowrap relative z-10 hover:text-blue-700 cursor-pointer btn-gg"
        onClick={() => setModal(true)}
      >
        {children}
      </a>

      <Modal isOpen={modal} onDismiss={close} isFull={true}>
        <ModalHeader> Bản đồ </ModalHeader>

        <ModalBody css="p-0 bg-white rounded gg-map">
          <div className="flex h-full w-full">
            <SojuMap center={center} address={address} name={name} />
          </div>
        </ModalBody>
        <ModalFooter className="flex">
          <div>
            <a
              onClick={close}
              className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>
      </Modal>

      <style jsx>{`
        :global(.gg-map .gm-style-iw-a) {
          transform: translateY(-50px);
        }

        .btn-gg::after {
          content: "";
          position: absolute;
          left: -15px;
          right: -15px;
          top: -10px;
          bottom: -10px;
        }
      `}</style>
    </>
  );
}

export default GGMap;
