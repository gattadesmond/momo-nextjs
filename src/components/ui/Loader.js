function Loader() {
  return (
    <>
      <div className="loader"></div>
      <style jsx>
        {`
          .loader {
            border: 4px solid  var(--gray-300); /* Light grey */
            border-top: 4px solid var(--pink); /* Blue */
            border-radius: 50%;
            width: 40px;
            height: 40px;
            animation: spin3 1.4s linear infinite;
          }
          @keyframes spin3 {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </>
  );
}

export default Loader;
