// import Link from "next/link";
// import Image from "next/image";

import { useState, useEffect } from "react";

import BlogBox1 from "@/components/ui/BlogBox1";
import { axios } from "@/lib/index";

function BlogListAjax({ item, isNumber, isMobile = false }) {
  const SortDir = item.SortDir;
  const SortBy = item.SortBy;
  const ProjectIds = item.ProjectIds;

  const CountItems = isNumber ? isNumber : item.Items.length;
  const Total = item.TotalItems;

  const firstList = item.Items.slice(0, CountItems);

  // const paramArray = firstList.reduce(function (stri, item) {
  //   if (!stri) {
  //     stri = item.Id.toString();
  //   } else {
  //     stri = stri.concat(",", item.Id);
  //   }
  //   return stri;
  // }, null);

  const projectIdsList = ProjectIds.reduce((result, item) => {
    if (result == "") {
      return result.concat(item.toString());
    }
    return result.concat(`,${item.toString()}`);
  }, "");

  const [article, setArticle] = useState(firstList);
  const [loading, setLoading] = useState(false);
  const [isMore, setIsMore] = useState(true);
  const [lastIndex, setLastIndex] = useState(CountItems);

  useEffect(() => {
    if (Total <= CountItems) {
      setIsMore(false);
    }
  }, []);

  const handelLoadMore = () => {
    setLoading(true);
    setTimeout(function () {
      axios
        .get(
          `/blog/listByProject?projectIds=${projectIdsList}&sortType=${SortBy}&sortDir=${SortDir}&count=${
            CountItems == 3 ? 3 : 4
          }&lastIdx=${lastIndex}`
        )
        .then((res) => {
          if (!res.Data) {
            return;
          }
          var data = res.Data;

          setArticle([...article, ...data.Items]);
          setLastIndex(data.LastIndex);
          setLoading(false);
          if (data.LastIndex < Total) {
            setIsMore(true);
          } else {
            setIsMore(false);
          }
        })
        .catch((e) => {});
    }, 300);
  };

  return (
    <>
      <div
        className={`grid grid-cols-1 sm:gap-6 sm:grid-cols-2 lg:grid-cols-${
          CountItems == 3 ? 3 : 4
        }`}
      >
        {article &&
          article.map((item, index) => (
            <BlogBox1
              key={index}
              data={item}
              isModal={true}
              isMobile={isMobile}
            />
          ))}
      </div>

      {isMore && (
        <div className="pt-6 text-center">
          <button
            type="button"
            disabled={loading ? true : false}
            className="py-1 pl-4 pr-6 font-semibold text-pink-700 transition-all border border-pink-600 rounded-full hover:text-pink-800 hover:bg-pink-50"
            onClick={handelLoadMore}
          >
            {loading ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-spin opacity-80"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-bounce opacity-80"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M16.707 10.293a1 1 0 010 1.414l-6 6a1 1 0 01-1.414 0l-6-6a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l4.293-4.293a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            )}
            Xem thêm
          </button>
        </div>
      )}
    </>
  );
}

export default BlogListAjax;
