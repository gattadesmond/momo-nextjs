
import { ImgComparisonSlider } from '@img-comparison-slider/react';
import Image from "next/image";

const ImageCompare = ({ img1, img2, alt1 = "", alt2 = "", width = 800, height = 450, desc="null" }) => {
  if (!img1 || !img2) return (<> </>);
  return (
    <>
      <ImgComparisonSlider className="img-comparison group">
        <div slot="first">
          <Image
            src={img1}
            layout="intrinsic"
            width={width}
            height={height}
            alt={alt1}
          />
        </div>

        <div slot="second">
          <Image
            src={img2}
            layout="intrinsic"
            width={width}
            height={height}
            alt={alt2 ?? alt1}
          />
        </div>

        <div slot="handle" className=" w-9 h-9 md:w-11 md:h-11 rounded-full transition-colors bg-pink-600 bg-opacity-90 flex items-center  justify-center text-white group-hover:bg-pink-500 ">
          <svg xmlns="http://www.w3.org/2000/svg" className=" h-8 w-8 md:h-10 md:w-10 opacity-70  rotate-90" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1} d="M8 9l4-4 4 4m0 6l-4 4-4-4" />
          </svg>
        </div>
    
      </ImgComparisonSlider>

      {desc && <div className="text-center mb-4 italic text-gray-600 text-md ">{desc}</div>} 
      
    </>
  );
};



export default ImageCompare;
