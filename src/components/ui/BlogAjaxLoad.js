import Link from "next/link";
import Image from "next/image";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();


// import ArticleContentConvert from "@/components/ui/ArticleContentConvert";
import ArticleContent from "@/components/blog/ArticleContent";

import ViewFormat from "@/lib/view-format";
import CatClassName from "@/lib/cat-class-name";
import { useEffect, useState } from "react";

import { axios } from "@/lib/index";
import Loader from "@/components/ui/Loader";
import ButtonShare from "./ButtonShare";

function BlogAjaxLoad({
  article,
  loading,
  idBlog = null,
  isViewApp = false,
  isMobile = false,
}) {
  const [articleDetail, setArticleDetail] = useState(null);
  const [loadingDetail, setLoadingDetail] = useState(true);

  useEffect(() => {
    setArticleDetail(article);
    setLoadingDetail(loading);
  }, [article, loading]);

  //Truong hop truyen vao ID blog le
  useEffect(() => {
    if (idBlog) {
      axios.get(`/blog/${idBlog}`).then(function (response) {
        const res = response.Data;
        setLoadingDetail(false);
        setArticleDetail(res);
      });
    }
  }, []);

  return (
    <>
      {loadingDetail && (
        <div className="py-20 flex justify-center">
          <Loader />
        </div>
      )}

      {articleDetail && (
        <>
          <div className="overflow-hidden rounded pointer-events-none select-none md:rounded-lg ">
            <Image
              src={articleDetail.Avatar}
              alt={articleDetail.Title}
              layout="responsive"
              loading="eager"
              width={1920}
              height={600}
            />
          </div>

          <div className="flex flex-row items-center mb-2 mt-5 md:px-8">
            <div className="flex-auto">
              {isViewApp ? (
                <a>
                  <div
                    className={` text-tiny font-semibold mb-0 transition-colors  inline-block ${CatClassName(
                      articleDetail.Category.Link
                    )}`}
                  >
                    {articleDetail.Category.Name}
                  </div>
                </a>
              ) : (
                <Link href={articleDetail.Category.Link}>
                  <a>
                    <div
                      className={` text-tiny font-semibold mb-0 transition-colors  inline-block ${CatClassName(
                        articleDetail.Category.Link
                      )}`}
                    >
                      {articleDetail.Category.Name}
                    </div>
                  </a>
                </Link>
              )}

              <div className="mt-0 text-gray-500 text-tiny first-letter:uppercase">
                <div
                  className={`flex flex-wrap items-center text-tiny text-gray-500`}
                >
                  {/* <div> {article.PublishDate}</div> */}
                  {articleDetail.TimeReadAvg && (
                    <>
                      {/* <div className="mx-1 text-base font-normal ">·</div> */}
                      <div>{articleDetail.TimeReadAvg} phút đọc</div>
                    </>
                  )}
                  {articleDetail.TotalViews >= 500 && (
                    <>
                      <div className="mx-1 text-base font-normal ">·</div>
                      <div>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-4 w-4 inline-block relative"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          style={{ top: "-1px" }}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                          />
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                          />
                        </svg>{" "}
                        <ViewFormat data={articleDetail.TotalViews} />{" "}
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
            <div className="flex-0 shrink-0  ">
              <ButtonShare
                isViewApp={isViewApp}
                link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${articleDetail.Link}`}
                // isViewApp={isViewApp}
              />
            </div>
          </div>

          <h3 className="mb-3 text-2xl font-bold tracking-tight text-black font-inter md:text-3xl  md:leading-tight md:px-8">
            {articleDetail.Title}{" "}
          </h3>

          <p className="italic leading-normal text-gray-600 md:px-8 ">
            {articleDetail.Short}
          </p>

          <hr className="post__sapo__hr font-inter" />
          <style jsx>{`
            .post__sapo__hr {
              font-style: normal;
              display: block;
              margin: 0.5em 0;
              border: 0;
              text-align: center;
              height: auto;
              color: var(--pinkmomo);
              font-weight: 300;
              font-family: system-ui, -apple-system, BlinkMacSystemFont,
                "Helvetica Neue", Helvetica, Arial, sans-serif;
            }
          `}</style>

          <div className="mt-3 soju__prose small font-inter md:px-8">
            <ArticleContent
              isMobile={isMobile}
              data={articleDetail.Content}
              ads={articleDetail.AdsData}
              cta={articleDetail.CtaData}
              qa={articleDetail.QaData}
              qaGroup={articleDetail.QaGroupData}
              guide={articleDetail.GuideData}
              guideGroup={articleDetail.GuideGroupData}
              isViewApp={isViewApp}
            />
            {/* <ArticleContentConvert data={articleDetail.Content} /> */}
          </div>
        </>
      )}
    </>
  );
}

export default BlogAjaxLoad;
