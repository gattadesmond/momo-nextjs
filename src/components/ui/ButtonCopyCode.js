import { useState, useEffect } from "react";
import useCopyToClipboard from "@/lib/useCopyToClipboard";


function ButtonCopyCode({ value, isViewApp = false }) {
  const [copied, copy] = useCopyToClipboard(value);

  function copyToClipboard() {
    copy();
  }

  return (
    <div className="relative text-center py-2">
      
      <div className=" text-sm text-gray-500 mb-1">
        Nhấn vào để copy mã
      </div>
      
      <button
        className="cursor-pointer text-lg text-pink-600 uppercase relative py-1 pl-5 pr-14  font-bold whitespace-nowrap  bg-pink-50 rounded-full border border-dashed border-pink-300 hover:bg-pink-100 "
        onClick={() => copyToClipboard()}
      >
        {" "}
        {value}{" "}
        <span className="absolute right-4 top-2">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"
            />
          </svg>
        </span>
      </button>

    </div>
  );
}

export default ButtonCopyCode;
