// import Link from "next/link";
// import Image from "next/image";
import dynamic from "next/dynamic";

import { useState, useEffect } from "react";

import AnchorLink from "react-anchor-link-smooth-scroll";
import parse from "html-react-parser";

import isMobile from "@/lib/isMobile";
import { clientUrlUc } from "@/lib/momo";
import { qrCodeModelOpen, qrCodeModelClose } from "@/lib/momo/tracking";
const QRModal = dynamic(() => import("@/components/QRModal"));

import { axios } from "@/lib/index";
import ScrollToElement from "@/components/ScrollToElement";

const ModalQrCode = ({
  qrId,
  text,
  link,
  newTab,
  uc,
  isViewApp = false,
  className,
}) => {
  const [modal, setModal] = useState(false);

  const [qr, setQr] = useState(null);

  const isMobileDevice = isMobile();

  const handleClose = () => {
    setModal(false);
    qrCodeModelClose();
  };

  const handleOpen = () => {
    setModal(true);
    qrCodeModelOpen(qrId);
  };
  useEffect(() => {
    axios
      .get(`/qrCode/ListByIds?ids=${qrId}`)
      .then(function (response) {
        const res = response.Data;
        if (res) {
          setQr(res[0]);
          setTimeout(() => {
            clientUrlUc();
          }, 1000);
        }
      });
  }, []);

  return (
    <>
      {qr && (
        <>
          {isMobileDevice &&
            (isViewApp && link ? (
              <a
                data-redirect-uc={uc}
                href={link}
                rel="noreferrer"
                target={newTab ? "_blank" : ""}
                className={`btn-primary ${className}`}
              >
                {text}
              </a>
            ) : (
              <a
                data-redirect-uc={uc}
                href={qr.QrLink}
                rel="noreferrer"
                target={newTab ? "_blank" : ""}
                className={`btn-primary ${className}`}
              >
                {text}
              </a>
            ))}

          {!isMobileDevice && (
            <>
              <button
                onClick={() => handleOpen()}
                className={`btn-primary ${className}`}
              >
                {text}
              </button>

              <QRModal
                isOpen={modal}
                onDismiss={handleClose}
                isDark={true}
                img={qr.QrImage}
                title={qr.Title}
                shortTitle={qr.ShortTitle}
              />
            </>
          )}
        </>
      )}
    </>
  );
};

const ButtonAction = ({
  cta,
  content,
  type = 0,
  isMobile,
  className = "",
  classNameRoot = "",
  isViewApp = false,
  ...rest
}) => {
  if (cta.QrCodeId) {
    return (
      <div
        {...rest}
        className={`qrtype-${cta.QrCodeId} w-full md:flex md:items-center ${classNameRoot}`}
        data-qr-id={cta.QrCodeId}
      >
        {content && <span className="mr-2 whitespace-nowrap hidden md:inline-block">{parse(content)}</span>}
        <ModalQrCode
          isViewApp={isViewApp}
          qrId={cta.QrCodeId}
          text={cta.Text}
          link={cta.Link}
          newTab={cta.NewTab}
          uc={cta.RedirectUC}
          className={className}
        />
      </div>
    );
  }

  // type 0 => btn-primary
  // type 1 => btn-outline
  const getCssClassByType = (type) => {
    if (type === 1) {
      return `btn-primary-outline ${className}`;
    } else {
      return `btn-primary ${className}`;
    }
  };

  useEffect(() => {
    if (!cta.QrCodeId) {
      clientUrlUc();
    }
  }, [])

  return (
    <>
      {content && <span className="mr-2 mt-1">{parse(content)}</span>}
      {cta.Link &&
        (cta.Link.startsWith("#") ? (
          <ScrollToElement href={cta.Link} rel="noreferrer" offset={50}>
            <span className={getCssClassByType(type)}>{cta.Text}</span>
          </ScrollToElement>
        ) : (
          <a
            data-redirect-uc={cta.RedirectUC}
            href={cta.Link}
            rel="noreferrer"
            target={cta.NewTab == true ? "_blank" : ""}
            className={
              getCssClassByType(type) +
              " " +
              (!content && isMobile ? "w-full" : "") +
              " " +
              classNameRoot
            }
          >
            {cta.Text}
          </a>
        ))}
    </>
  );
};

export default ButtonAction;
