import { clientUrlUc } from "@/lib/momo";
import { qrCodeModelOpen, qrCodeModelClose } from "@/lib/momo/tracking";
import { useState, useEffect } from "react";

import parse, { domToReact } from "html-react-parser";

import isMobile from "@/lib/isMobile";

import QRModal from "@/components/QRModal";

import { axios } from "@/lib/index";

const ModalQrCode = ({ qrId, text }) => {
  const [modal, setModal] = useState(false);

  const [qr, setQr] = useState(null);

  const isMobileDevice = isMobile();

  const handleClose = () => {
    setModal(false);
    qrCodeModelClose();
  };

  const handleOpen = () => {
    setModal(true);
    qrCodeModelOpen(qrId);
  };

  useEffect(() => {
    axios.get(`/qrCode/ListByIds?ids=${qrId}`).then(function (response) {
      const res = response.Data;
      if (res) {
        setQr(res[0]);
        setTimeout(() => {
          clientUrlUc();
        }, 1000);
      }
    });
  }, []);

  if (isMobileDevice) {
    return (
      <>
        {qr && (
          <a
            href={qr.QrLink}
            rel="noreferrer"
            target="_blank"
            className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md btn text-opacity-90 hover:bg-pink-800"
          >
            {text}
          </a>
        )}
      </>
    );
  }
  return (
    <>
      {qr && (
        <>
          <button
            onClick={() => handleOpen()}
            className="btn-primary"
          >
            {text}
          </button>

          <QRModal
            isOpen={modal}
            onDismiss={handleClose}
            isDark={true}
            img={qr.QrImage}
            title={qr.Title}
            shortTitle={qr.ShortTitle}
          />

        </>
      )}
    </>
  );
};

function ArticleContentConvert({ data, cta }) {
  // const isMobileDevice = isMobile();

  // const [qrId, setQrId] = useState(null);

  const options = {
    trim: true ,
    replace: ({ attribs, name, children }) => {
      if (name == "table") {
        return (
          <div className="table-responsive">
            <table className="table">{domToReact(children)}</table>
          </div>
        );
      }

      if (name == "a") {
        if (attribs["data-qrcode-id"] != undefined) {
          var qrId = attribs["data-qrcode-id"];

          if (!qrId) return;

          return (
            <div className={` qrtype-${qrId}`} data-qr-id={qrId}>
              <ModalQrCode qrId={qrId} text={children[0]?.data} />
            </div>
          );
        }
      }

      if (name == "img") {
        return (
          <img 
            alt={attribs["alt"]}
            src={attribs["src"]}
            loading="eager"
          />
        )
      }

      if (!attribs) {
        return;
      }
    },
  };

  return (
    <>
      {parse(data, options)}

      {/* {qrId && (
        <ModalQrCode qrActive={qrId} dissmiss={() => setQrId(null)} />
      )} */}
    </>
  );
}

export default ArticleContentConvert;
