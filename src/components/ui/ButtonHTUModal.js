import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";

import ModalHeader from "../modal/ModalHeader";
import HowToUse from "../htu/Desktop";

export default function ButtonHTUModal({ guideId, text, isMobile, className }) {
  if (!guideId) return <></>;

  const [modal, setModal] = useState(false);
  const [guideData, setGuideData] = useState(null);

  const handleClose = () => {
    setModal(false);
  };

  const handleOpen = () => {
    setModal(true);
  };

  useEffect(() => {
    axios.get(`/help-article-group/${guideId}`)
      .then(function (response) {
        const res = response.Data;
        if (res) {
          setGuideData(res);
        }
      });
  }, []);

  return (
    <>
      {guideData &&
        <>
          <button
            type="button"
            onClick={() => handleOpen()}
            className={`font-bold px-4 py-2 bg-gray-100 rounded-md border border-gray-500 ${className}`}
          >
            {text}
          </button>
          <Modal isOpen={modal} onDismiss={handleClose}>
            <ModalBody css="p-0 bg-white rounded">
              <div className="w-100 pt-6">
                <h5 className="text-center text-1.5xl font-bold">
                  {guideData.GroupItems[0].Title}
                </h5>
              </div>
              <div className="pl-8 pr-4 py-5">
                <HowToUse
                  data={guideData.GroupItems}
                  isMobile={isMobile}
                />
              </div>
            </ModalBody>
          </Modal>
        </>
      }
    </>
  )
}