import { useState, useEffect } from "react";
import useCopyToClipboard from "@/lib/useCopyToClipboard";
import MaxApi from "@momo-platform/max-api";
import { FacebookShareButton } from "react-share";

import {
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
} from "@reach/menu-button";
import { momoPlatformApi } from "@/lib/momo";

function ButtonShare({ children, link, isViewApp = false }) {
  const [copied, copy] = useCopyToClipboard(link);
  const [visible, setVisible] = useState(false);

  const { shareFacebookApp, shareOtherApp } = momoPlatformApi();

  function copyToClipboard() {
    copy();
  }

  function shareFacebook() {
    window.open(
      "https://www.facebook.com/sharer/sharer.php?u=" + link,
      "_blank",
      "width=560,height=340,toolbar=0,menubar=0,location=0"
    );
  }

  function shareTwitter() {
    window.open(
      "https://twitter.com/intent/tweet?url=" + link,
      "_blank",
      "width=560,height=340,toolbar=0,menubar=0,location=0"
    );
  }
  return (
    <div className="relative">
      <Menu>
        <MenuButton className="button-share">
          {children ? (
           children
          ) : (
            <div className="button-share-bg drop-shadow-sm flex space-x-1 items-center py-1 pl-2.5 pr-3 text-md rounded-3xl border border-gray-200 text-gray-600 hover:text-gray-800 hover:bg-gray-100 focus:text-gray-800 focus:bg-gray-100">
              <span>Chia sẻ</span>
              <svg
                width="24px"
                height="24px"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                className="h-4 w-4"
                fill="currentColor"
              >
                <g>
                  <path fill="none" d="M0 0h24v24H0z" />
                  <path d="M13 14h-2a8.999 8.999 0 0 0-7.968 4.81A10.136 10.136 0 0 1 3 18C3 12.477 7.477 8 13 8V3l10 8-10 8v-5z" />
                </g>
              </svg>
            </div>
          )}
        </MenuButton>

        <MenuPopover
          className="dropdown-filter-mobile fadeInDown w-[220px] z-10 mt-1 rounded-lg p-2 focus:outline-none shadow-level3 bg-white absolute top-full right-0"
          portal={false}
        >
          <div className="arbitrary-element">
            <MenuItems className="grid grid-cols-1 ">
              <MenuItem
                className="cursor-pointer select-none text-sm relative py-2 pl-9 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                onSelect={() => copyToClipboard()}
              >
                {" "}
                Sao chép liên kết{" "}
                <span className="absolute left-2 top-2">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M12.586 4.586a2 2 0 112.828 2.828l-3 3a2 2 0 01-2.828 0 1 1 0 00-1.414 1.414 4 4 0 005.656 0l3-3a4 4 0 00-5.656-5.656l-1.5 1.5a1 1 0 101.414 1.414l1.5-1.5zm-5 5a2 2 0 012.828 0 1 1 0 101.414-1.414 4 4 0 00-5.656 0l-3 3a4 4 0 105.656 5.656l1.5-1.5a1 1 0 10-1.414-1.414l-1.5 1.5a2 2 0 11-2.828-2.828l3-3z"
                      clipRule="evenodd"
                    />
                  </svg>
                </span>
              </MenuItem>
              {!isViewApp && (
                <>
                  <MenuItem
                    className="cursor-pointer select-none text-sm relative py-2 pl-9 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                    onSelect={() => shareFacebook()}
                  >
                    {" "}
                    Chia sẻ Facebook{" "}
                    <span className="absolute left-2 top-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        className="h-5 w-5"
                        fill="currentColor"
                      >
                        <path d="M15.12,5.32H17V2.14A26.11,26.11,0,0,0,14.26,2C11.54,2,9.68,3.66,9.68,6.7V9.32H6.61v3.56H9.68V22h3.68V12.88h3.06l.46-3.56H13.36V7.05C13.36,6,13.64,5.32,15.12,5.32Z" />
                      </svg>
                    </span>
                  </MenuItem>

                  <MenuItem
                    className="cursor-pointer select-none text-sm relative py-2 pl-9 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                    onSelect={() => shareTwitter()}
                  >
                    {" "}
                    Chia sẻ Twitter{" "}
                    <span className="absolute left-2 top-2">
                      <svg
                        fill="currentColor"
                        className="h-5 w-5"
                        viewBox="0 0 30 30"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="m28 6.937c-0.957 0.425-1.985 0.711-3.064 0.84 1.102-0.66 1.947-1.705 2.345-2.951-1.03 0.611-2.172 1.055-3.388 1.295-0.973-1.037-2.359-1.685-3.893-1.685-2.946 0-5.334 2.389-5.334 5.334 0 0.418 0.048 0.826 0.138 1.215-4.433-0.222-8.363-2.346-10.995-5.574-0.458 0.788-0.721 1.704-0.721 2.683 0 1.85 0.941 3.483 2.372 4.439-0.874-0.028-1.697-0.268-2.416-0.667v0.067c0 2.585 1.838 4.741 4.279 5.23-0.447 0.122-0.919 0.187-1.406 0.187-0.343 0-0.678-0.034-1.003-0.095 0.679 2.119 2.649 3.662 4.983 3.705-1.825 1.431-4.125 2.284-6.625 2.284-0.43 0-0.855-0.025-1.273-0.075 2.361 1.513 5.164 2.396 8.177 2.396 9.812 0 15.176-8.128 15.176-15.177 0-0.231-5e-3 -0.461-0.015-0.69 1.043-0.753 1.948-1.692 2.663-2.761z" />
                      </svg>
                    </span>
                  </MenuItem>
                </>
              )}

              {isViewApp && (
                <>
                  <MenuItem
                    className="cursor-pointer select-none text-sm relative py-2 pl-9 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                    onSelect={() => shareFacebookApp(link)}
                  >
                    {" "}
                    Chia sẻ Facebook{" "}
                    <span className="absolute left-2 top-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        className="h-5 w-5"
                        fill="currentColor"
                      >
                        <path d="M15.12,5.32H17V2.14A26.11,26.11,0,0,0,14.26,2C11.54,2,9.68,3.66,9.68,6.7V9.32H6.61v3.56H9.68V22h3.68V12.88h3.06l.46-3.56H13.36V7.05C13.36,6,13.64,5.32,15.12,5.32Z" />
                      </svg>
                    </span>
                  </MenuItem>
                  <MenuItem
                    className="cursor-pointer select-none text-sm relative py-2 pl-9 pr-2 text-gray-600 whitespace-nowrap hover:bg-gray-100 rounded"
                    onSelect={() => shareOtherApp(link)}
                  >
                    {" "}
                    Chia sẻ khác{" "}
                    <span className="absolute left-2 top-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"
                        />
                      </svg>
                    </span>
                  </MenuItem>
                </>
              )}
            </MenuItems>
          </div>
        </MenuPopover>
      </Menu>
      <style jsx>{`
        :global(.button-share[aria-expanded="true"] .button-share-bg) {
          background-color: var(--gray-200);
          color: var(--gray-800);
        }
      `}</style>
    </div>
  );
}

export default ButtonShare;
