import Link from "next/link";
import Image from "next/image";

import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { decode } from "html-entities";
import parse from "html-react-parser";
import { axios } from "@/lib/index";

import { parseISO, format } from "date-fns";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";
import ButtonAction from "@/components/ui/ButtonAction";

import ArticleAjaxLoad from "@/components/ui/ArticleAjaxLoad";
import { appSettings } from "@/configs";

function ArticleBox1({ data, isModal, isMobile = false }) {
  const { Url, Id } = data;

  const [article, setArticle] = useState(null);
  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };
  const handleShow = () => {
    setModal(true);
    setLoading(true);
    axios
      .get(`/article/${Id}?countRelating=0&countNewest=0`)
      .then(function (response) {
        const res = response.Data;
        setLoading(false);
        setArticle(res);

        var _isViewApp =
          res.ViewInApp && router?.query?.view == "app" ? true : false;
        if (_isViewApp) setIsViewApp(_isViewApp);

        var img = document.createElement("img");
        img.setAttribute(
          "src",
          `${appSettings.AppConfig.HOST_API}/trace/article/${res.Id}`
        );
        img.setAttribute(
          "style",
          "position: absolute;display: block;z-index: -10000"
        );
        document.body.appendChild(img);

      });
  };

  function Content({ data }) {
    const { Avatar, Short, Title, TotalViews } = data;
    let DateShow = data.DateShow;
    if (!DateShow && data.Date) {
      DateShow = data.Date;
    }
    if (!Avatar && !Title) {
      return <></>;
    }
    return (
      <div className="flex flex-wrap pb-3 border-b border-gray-200 sm:border-none sm:pb-0 ">
        <div className="order-2 pt-3 sm:flex-auto sm:w-full w-28 sm:order-1 sm:pt-0">
          <div className="overflow-hidden bg-gray-100 rounded flex md:rounded-none relative">
            {Avatar && <Image
              src={Avatar}
              alt={Title}
              layout="intrinsic"
              width={508}
              height={244}
              loading="lazy"
            />}
          </div>
        </div>

        <div className="flex-1 order-1 pr-6 sm:pr-0 sm:flex-auto sm:w-full sm:order-2 md:pb-2 md:px-4">
          <div className="pt-3 pb-2 font-semibold leading-snug group-hover:text-pink-600 text-md">
            {parse(decode(Title))}
          </div>

          <div className="text-xs text-gray-500">
            {DateShow && <>{format(parseISO(DateShow), "dd/MM/yyyy")}</>}
          </div>
        </div>
      </div>
    );
  }
  // <div className="h-full overflow-hidden transition-shadow bg-white rounded-lg shadow-md cursor-pointer group hover:shadow-lg flex flex-wrap">
  return (
    <>
      <div className="h-full group transition-shadow cursor-pointer  md:overflow-hidden md:bg-white md:rounded-lg md:shadow-md md:hover:shadow-lg">
        {isModal ? (
          <div onClick={handleShow}>
            <Content data={data} />
          </div>
        ) : (
          <Link href={Url}>
            <a>
              <Content data={data} />
            </a>
          </Link>
        )}
      </div>

      <Modal isOpen={modal} onDismiss={close} isFull={true} isBig={false}>
        <ModalHeader> Tin tức </ModalHeader>

        <ModalBody css="p-0 bg-white rounded">
          <div className="p-4 md:p-7">
            <ArticleAjaxLoad
              isMobile={isMobile}
              article={article}
              loading={loading}
              isViewApp={isViewApp}
            />
          </div>
        </ModalBody>
        <ModalFooter
          className={`space-x-3 ${
            article?.ViewInApp && article?.ViewInAppConfig?.Ctas?.length > 0
              ? "justify-center"
              : ""
          }`}
        >
          {!loading &&
            (article?.ViewInApp &&
            article?.ViewInAppConfig?.Ctas?.length > 0 ? (
              article.ViewInAppConfig.Ctas.map((cta, idx) => {
                return (
                  <div
                    key={idx}
                    className="flex items-start justify-center flex-1 md:flex-none    "
                  >
                    <ButtonAction
                      className="w-full whitespace-nowrap"
                      isViewApp={isViewApp}
                      classNameRoot=" flex  md:grow-0"
                      cta={cta}
                    />
                  </div>
                );
              })
            ) : (
              <div className="flex items-start justify-end flex-1 md:flex-none  ">
                <a
                  onClick={close}
                  className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
                >
                  Đóng
                </a>
              </div>
            ))}
        </ModalFooter>
      </Modal>
    </>
  );
}

export default ArticleBox1;
