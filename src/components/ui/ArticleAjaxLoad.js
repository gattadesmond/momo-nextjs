import Link from "next/link";
import Image from "next/image";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

import { useState, useEffect } from "react";

import ButtonCopyCode from "@/components/ui/ButtonCopyCode";
// import ArticleContentConvert from "@/components/ui/ArticleContentConvert";
import ArticleContent from "@/components/article/ArticleContent";
import format from "date-fns/format";

import Loader from "@/components/ui/Loader";
import NewDateOnSafari from "@/lib/newDateOnSafari";
import ButtonShare from "@/components/ui/ButtonShare";
import { ArticleCateColor } from "@/components/article/CategoryInfo";
import { MoneyFormat } from "@/lib/int-format";

function ArticleAjaxLoad({
  article,
  loading,
  isViewApp = false,
  isMobile = false,
}) {
  const [codes, setCodes] = useState([]);
  useEffect(() => {
    if (article && article.Code) {
      var split = article.Code.split(",");
      var codes = [];
      for (var idx = 0; idx < split.length; idx++) {
        if (split[idx]) codes.push(split[idx]);
      }
      setCodes(codes);
    }
  }, [article]);
  return (
    <>
      {loading && (
        <div className="py-20 flex justify-center">
          <Loader />
        </div>
      )}

      {article && (
        <>
          {article.ViewInApp && (
            <div className="overflow-hidden rounded pointer-events-none select-none md:rounded-lg mb-3">
              <Image
                src={article.Avatar}
                alt={article.Title}
                layout="responsive"
                width={770}
                height={370}
              />
            </div>
          )}

          <h3 className="mb-2 text-lg font-bold text-pink-600 md:text-2xl">
            {article.Title}
          </h3>
          <div className="flex flex-row items-center mb-2">
            <div className="flex-auto">
              {isViewApp ? (
                <a className="block">
                  <span
                    className={`text-tiny font-semibold mb-0 transition-colors inline-block`}
                    style={{ color: ArticleCateColor(article.Category.Id) }}
                  >
                    {article.Category.Name}
                  </span>
                </a>
              ) : (
                <Link href={article.Category.Link}>
                  <a className="block">
                    <span
                      className={`text-tiny font-semibold mb-0 transition-colors inline-block`}
                      style={{ color: ArticleCateColor(article.Category.Id) }}
                    >
                      {article.Category.Name}
                    </span>
                  </a>
                </Link>
              )}

              <div className="mt-0 text-gray-500 text-tiny first-letter:uppercase flex flex-wrap items-center ">
                {article.DateStart && article.DateEnd ? (
                  <>
                    Từ&nbsp;
                    <span>
                      {format(NewDateOnSafari(article.DateStart), "dd/MM/yyyy")}
                    </span>
                    &nbsp;đến&nbsp;
                    <span>
                      {format(NewDateOnSafari(article.DateEnd), "dd/MM/yyyy")}
                    </span>
                  </>
                ) : (
                  <span>{article.PublishDate}</span>
                )}

                {article.TotalViews > 500 && (
                  <>
                    &nbsp;-&nbsp;<span>{article.TotalViewsFormat}</span>
                    &nbsp;lượt xem
                  </>
                )}
              </div>
            </div>
            <div className="flex-0 shrink-0">
              <ButtonShare
                isViewApp={isViewApp}
                link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${article.Link}`}
              />
            </div>
          </div>

          <div className="mt-5 soju__prose small">
            <p className="article-desc">{article.Short}</p>
            {codes &&
              codes.map((code, idx) => {
                return (
                  <p className="m-2">
                    <ButtonCopyCode value={code} key={idx} />
                  </p>
                );
              })}
            <ArticleContent
              isMobile={isMobile}
              data={article.Content}
              ads={article.AdsData}
              cta={article.CtaData}
              qa={article.QaData}
              qaGroup={article.QaGroupData}
              guide={article.GuideData}
              guideGroup={article.GuideGroupData}
              isViewApp={isViewApp}
            />
            {/* <ArticleContentConvert data={article.Content} /> */}
          </div>
        </>
      )}
      <style jsx>
        {`
          .article-desc {
            font-weight: bold;
            color: #828282;
            font-size: 18px;
            line-height: 160%;
          }
        `}
      </style>
    </>
  );
}

export default ArticleAjaxLoad;
