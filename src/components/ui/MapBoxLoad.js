

import { useState, useEffect, useCallback, useRef } from "react";


import 'mapbox-gl/dist/mapbox-gl.css';

import mapboxgl from "mapbox-gl";


mapboxgl.accessToken =
  "pk.eyJ1IjoiZ2F0dGFkZXNtb25kIiwiYSI6ImNrcjI3ajN3bzE3YXkydW1ueGJnMTVkbTUifQ.MHe4DsdVGivVpbABXitZog";

const MapBoxLoad = ({ lat, lon, name, address }) => {
  const mapContainerRef = useRef(null);

  const [lngX, setLngX] = useState(lon);
  const [latX, setLatX] = useState(lat);
  const [zoom, setZoom] = useState(16);

  // Initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lngX, latX],
      zoom: zoom,
    });

    var popup = new mapboxgl.Popup({ closeOnClick: false })
      .setLngLat([lngX, latX])
      .setHTML(
        `<div class=" w-52 ">
      <div class="font-bold text-sm">${name}</div>
      <div class="mt-1 text-xs text-gray-500 overflow-hidden text-ellipsis whitespace-nowrap">
        ${address}
      </div>
    </div>`
      )
      .addTo(map);

    var marker1 = new mapboxgl.Marker({
      color: "#f45197",
    })
      .setLngLat([lngX, latX])
      .addTo(map);

    // Add navigation control (the +/- zoom buttons)
    // map.addControl(new mapboxgl.NavigationControl(), "top-right");

    // map.on("move", () => {
    //   setLng(map.getCenter().lng.toFixed(4));
    //   setLat(map.getCenter().lat.toFixed(4));
    //   setZoom(map.getZoom().toFixed(2));
    // });

    // Clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return <div className="map-container" ref={mapContainerRef} />;
};


export default MapBoxLoad;
