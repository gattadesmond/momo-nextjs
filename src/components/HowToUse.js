import dynamic from "next/dynamic";

const Desktop = dynamic(() => import("../components/htu/Desktop"));
const Mobile = dynamic(() => import("../components/htu/Mobile"));

const HowToUse = ({ isMobile, data, title, isMockup }) => {
  return (
    <>
      {isMobile == false ? (
        <Desktop data={data} title={title} isMockup={isMockup} />
      ) : (
        <Mobile data={data} title={title} />
      )}
    </>
  );
};

export default HowToUse;
