
import { memo } from "react";

import dynamic from "next/dynamic";

const Desktop = dynamic(() => import("../components/header/HeaderDesktop"));
const Mobile = dynamic(() => import("../components/header/HeaderMobile"));

const Header = (props) => {

  return ( <>
    <Mobile data={props.data} />
    <Desktop data={props.data} />
  </>
  );
};

export default memo(Header);
