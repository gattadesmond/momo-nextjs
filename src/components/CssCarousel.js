// import Link from "next/link";
// import { useRouter } from "next/router";

import { useEffect, useRef } from "react";

import usePosition from "@/lib/usePosition";

//https://css-tricks.com/a-super-flexible-css-carousel-enhanced-with-javascript-navigation/#top-of-site

const ArrowLeft = () => (
  <div className="w-6 h-6 text-gray-600  rounded-full border border-gray-300 flex items-center justify-center">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="h-4 w-4 "
      viewBox="0 0 20 20"
      fill="currentColor"
    >
      <path
        fillRule="evenodd"
        d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z"
        clipRule="evenodd"
      />
    </svg>
  </div>
);

// ArrowRight
const ArrowRight = () => (
  <div className="w-6 h-6 text-gray-600  rounded-full border border-gray-300 flex items-center justify-center">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="h-4 w-4 "
      viewBox="0 0 20 20"
      fill="currentColor"
    >
      <path
        fillRule="evenodd"
        d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
        clipRule="evenodd"
      />
    </svg>
  </div>
);

const CssCarousel = ({ className, children, bgColor = null, activeId = null }) => {
  const ref = useRef();

  const { hasItemsOnLeft, hasItemsOnRight, scrollRight, scrollLeft, scrollToElementNow } =
    usePosition(ref);

  useEffect(() => {
    scrollToElementNow(ref);
  }, [activeId]);

  const gradBgColor = bgColor ? bgColor : "var(--grad-rgb-color)";


  return (
    <div className={`relative soju-carousel ${className ? className : ''}`} ref={ref}>
      {children}

      {hasItemsOnRight && (
        <div
          onClick={scrollRight}
          className="absolute right-0 top-0  h-full  pl-10 pr-1 flex items-center  grad cursor-pointer z-10"
        >
          <ArrowRight />
        </div>
      )}

      {hasItemsOnLeft && (
        <div
          onClick={scrollLeft}
          className="absolute left-0 top-0  h-full  pr-5 md:pr-10 pl-1  items-center  grad-left cursor-pointer  flex z-10"
        >
          <div className=" hidden md:block"> <ArrowLeft /></div>

        </div>
      )}

      <style jsx>{`
        .grad {
          background: linear-gradient(
            to right,
            rgba(255, 255, 255, 0),
            rgb(${gradBgColor}),
            rgb(${gradBgColor})
          );
        }

        .grad-left {
          background: linear-gradient(
            to left,
            rgba(255, 255, 255, 0),
            rgb(${gradBgColor}),
            rgb(${gradBgColor})
          );
        }
      `}</style>
    </div>
  );
};

export default CssCarousel;
