import { useEffect } from "react";

//https://css-tricks.com/the-trick-to-viewport-units-on-mobile/

export default function ViewportHeight() {
  useEffect(() => {


    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);

    // window.addEventListener("resize", () => {
    //   // We execute the same script as before
    //   let vh = window.innerHeight * 0.01;
    //   document.documentElement.style.setProperty("--vh", `${vh}px`);
    // });
  }, []);
  return (
    <style jsx global>
      {`
        :global(.viewport-height) {
          height: 100vh; /* Fallback for browsers that do not support Custom Properties */
          // height: calc(var(--vh, 1vh) * 100);
        }

        :global(.viewport-max-height) {
          max-height: 100vh; /* Fallback for browsers that do not support Custom Properties */
          // max-height: calc(var(--vh, 1vh) * 100);
        }
      `}
    </style>
  );
}
