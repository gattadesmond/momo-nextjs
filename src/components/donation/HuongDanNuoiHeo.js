import { useState, useEffect, useRef } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import parse from "html-react-parser";

import CssCarousel from "@/components/CssCarousel";
import Heading from "../Heading";

const HuongDanNuoiHeo = () => {
  const ref = useRef();

  const [tabIndex, setTabIndex] = useState(0);

  useEffect(() => {
    const callback = (elementRef) => {
      if (elementRef[0].isIntersecting) {
        videoPlay(tabIndex);
      } else {
        videoPaused(tabIndex);
      }
    };

    const observer = new IntersectionObserver(callback, {
      rootMargin: "0px 0px -80% 0px",
    });

    observer.observe(ref.current);
    return () => observer.disconnect();
  })

  const tabs = [
    "Tạo Heo Vàng", "Kiếm thức ăn", "Lên cấp", "Quyên góp Heo Vàng"
  ];

  const videoStopAll = (index) => {
    try {
      const video = document.querySelector(`#huongdan video#tab-${index}`);
      video.pause();
      video.currentTime = 0;
    } catch (e) {
      console.error(e);
    }
  }
  const videoPaused = (index) => {
    try {
      document.querySelector(`#huongdan video#tab-${index}`).pause();
    } catch(e) {
      console.error(e);
    }
  }

  const videoPlay = (index) => {
    document.querySelector(`#huongdan video#tab-${index}`).play(0);
  }

  const onChangeTabIndex = (index) => {
    videoStopAll(tabIndex);
    setTabIndex(index);
    videoPlay(index);
  }

  const tabData = [
    {
      Title: 'Tạo Heo Vàng',
      Video: 'https://momo.vn/momo2020/img/donation/heodat/Block 3-thu-hoach-cho-an.mp4',
      Poster: 'https://momo.vn/momo2020/img/donation/heart/df-video.jpg',
      Content: `
        <p>
          Bạn có thể nhận được Heo Vàng khi cho Heo ăn. Sau khi Heo ăn xong, 1 Heo Vàng sẽ được tự động chuyển vào hũ.
        </p>
        <p>
          Bạn bè cũng có thể ghé thăm nhà và cho Heo của bạn ăn, khi đó Heo Vàng sẽ rớt ở dưới đất. Nếu thấy Heo Vàng dưới đất thì hãy bấm ngay để thu vào hũ bạn nhé.
        </p>
      `
    },
    {
      Title: 'Kiếm thức ăn',
      Video: 'https://momo.vn/momo2020/img/donation/heodat/Block 3-nhiem-vu.mp4',
      Poster: 'https://momo.vn/momo2020/img/donation/heart/df-video.jpg',
      Content: `
        <p>
          Bạn có thể nhận được thức ăn khi thực hiện các công việc được liệt kê trong danh sách nhiệm vụ. Các công việc này bao gồm: Cho Heo đi học, Quyên góp tiền mặt (Trái tim MoMo), hoặc sử dụng các dịch vụ thanh toán và chuyển tiền của MoMo...
        </p>
      `
    },
    {
      Title: 'Lên cấp',
      Video: 'https://momo.vn/momo2020/img/donation/heodat/Block 3-len-cap.mp4',
      Poster: 'https://momo.vn/momo2020/img/donation/heart/df-video.jpg',
      Content: `
        <p>
          Heo sẽ lên cấp khi đạt đủ “Điểm sống tốt”. Bạn sẽ nhận được “Điểm sống tốt” nếu chăm chỉ thực hiện các nhiệm vụ sống tốt mỗi ngày như: <strong> Dậy sớm, Quyên góp, Đi bộ cùng MoMo đủ 4000 bước… </strong> Khi lên cấp, Heo sẽ mọc cánh, ăn nhanh hơn và túi thức ăn cũng sẽ to ra.
        </p>
      `
    },
    {
      Title: 'Quyên góp Heo vàng',
      Video: 'https://momo.vn/momo2020/img/donation/heodat/Block 3-quyen-gop.mp4',
      Poster: 'https://momo.vn/momo2020/img/donation/heart/df-video.jpg',
      Content: `
        <p>
          Hãy quyên góp Heo Vàng mỗi ngày để Heo mau lên cấp và mang lại tương lai tươi sáng hơn cho trẻ em trên khắp đất nước.
        </p>
        <p>
          Bấm vào nút Quyên góp để xem danh sách các hoàn cảnh đang cần Heo Vàng bạn nhé.
        </p>
      `
    },

  ]

  return (
    <div id="huongdan" ref={ref}>
      <div className="mb-5 text-center md:mb-8" id="section-help">
        <Heading title={"Hướng dẫn nuôi Heo"} color="pink" />
      </div>
      <Tabs index={tabIndex} onChange={(index) => onChangeTabIndex(index)}>
        <CssCarousel className="-mx-5">
          <TabList className="flex items-center md:justify-center space-x-3 md:space-x-6 flex-nowrap overflow-x-auto pl-5 md:pl-0">
            {tabs.map((tab, index) => (
              <Tab
                key={index}
                className={`block px-2 py-2.5 whitespace-nowrap text-lg font-semibold outline-none ring-0 border-b-2  ${index == tabIndex
                  ? "text-gray-800 border-pink-600 is-active"
                  : "text-gray-500 border-transparent"
                  }`}
              >
                {tab}
              </Tab>
            ))}
            <div className="w-2">&nbsp;</div>
          </TabList>
          <div className="block md:hidden h-px w-6"></div>
        </CssCarousel>
        <TabPanels>
          {tabData.length > 0 &&
            tabData.map((item, index) => (
              <TabPanel key={index}>
                <div className="pt-5 grid grid-cols-1 md:grid-cols-2 justify-center items-center">
                  <div className="pb-5 md:pl-24">
                    <div className="mx-auto max-w-[280px] md:max-w-[340px]">
                      <video
                        id={`tab-${index}`}
                        className=""
                        height="auto"
                        width="100%"
                        poster={item.Poster}
                        loop muted playsInline
                      >
                        <source src={item.Video} type="video/mp4" />
                      </video>
                    </div>
                  </div>
                  <div className="md:px-4 md:max-w-sm">
                    <h4 className="text-xl md:text-1.5xl font-semibold mb-2 md:mb-6">{item.Title}</h4>
                    <div className="text-lg text-gray-600 text-opacity-80 space-y-4">
                      {parse(item.Content)}
                    </div>
                  </div>
                </div>
              </TabPanel>
            ))
          }
        </TabPanels>
      </Tabs>
    </div>
  )
}
export default HuongDanNuoiHeo;