import { memo } from "react";

import isMobile from "@/lib/isMobile";
import { getRootSlug } from "@/lib/url";

import ButtonAction from "../ui/ButtonAction";
import ButtonHTUModal from "../ui/ButtonHTUModal";
import { IconHeoVang } from "./IconsDonation";



const DonationDetail = ({ className, data, isShowButtonDonation }) => {
  if (!data) {
    return null;
  }

  const renderProgressBar = (finishPercent, isEndCampaign) => {
    if (finishPercent >= 100) {
      return (
        <div className="dn-progress-bar done h-1.5 rounded-lg bg-finish w-full">
        </div>
      )
    }
    else if (finishPercent < 100 && isEndCampaign) {
      return (
        <div
          className="dn-progress-bar end-time h-1.5 bg-gray-500 rounded-lg"
          style={{ width: `${finishPercent}%` }}
        >
        </div>
      )
    }
    else {
      return (
        <div
          className="dn-progress-bar h-1.5 bg-momo rounded-lg"
          style={{ width: `${finishPercent}%` }}
        >
        </div>
      )
    }
  }

  const renderClockInfo = (finishPercent, isEndCampaign, isContDonate, timeToEndDate) => {
    if (finishPercent >= 100) {
      if (!isEndCampaign && isContDonate) {
        return <h4>Tiếp tục nhận <br />Quyên góp</h4>;
      } else {
        return <h4>Đã đạt mục tiêu</h4>;
      }
    }
    else if (finishPercent < 100 && isEndCampaign) {
      return <h4>Đã hết thời hạn</h4>;
    }
    else {
      return (
        <>
          <h4 className="">Thời hạn còn</h4>
          <p className="text-gray-900 font-bold">{timeToEndDate}</p>
        </>
      );
    }
  }

  return (
    <div className={`dn-detail`}>
      <div className="dn-info flex items-end mb-2">
        <strong className="flex item-end text-xl font-bold text-gray-700 dn-value">
          {data.TotalMoney}
          {data.Type === 1 && 'đ'}
          {data.Type === 2 &&
            <IconHeoVang className="w-6 h-6 ml-1 top-1" />
          }
        </strong>
        <span className="pl-2 text-sm text-gray-500 flex items-end">
          quyên góp / {data.ExpectedValueFormat}
          {data.Type === 1 && 'đ'}
          {data.Type === 2 &&
            <IconHeoVang className="w-5 h-5 ml-1 -top-px" />
          }
        </span>
      </div>
      <div className="dn-progress flex overflow-hidden h-1.5 w-full bg-gray-200 rounded-lg my-1">
        {renderProgressBar(data.FinishPercent, data.IsEndCampaign)}
      </div>
      <div className={`dn-time flex justify-between items-start mt-2 text-gray-400`}>
        <div className="">
          <h4 className="text-gray-400">Lượt quyên góp</h4>
          <p className="text-gray-900 font-bold">{data.TotalTrans}</p>
        </div>
        <div className="">
          <h4 className="text-gray-400">Đạt được</h4>
          <p className="text-gray-900 font-bold">{data.FinishPercent}%</p>
        </div>
        <div className={`${data.FinishPercent >= 100 ? 'text-finish' : ''}`}>
          {renderClockInfo(
            data.FinishPercent,
            data.IsEndCampaign,
            data.IsContDonate,
            data.TimeToEndDate
          )}
        </div>
      </div>
      <div className="mt-6 space-y-3">
        {isShowButtonDonation &&
          <ButtonAction
            className="w-full !text-lg"
            cta={{
              QrCodeId: data.CtaQrCodeId,
              NewTab: data.CtaNewTab,
              Link: data.CtaLink,
              Text: "Quyên góp"
            }}
            isMobile={isMobile}
          />
        }
        <ButtonHTUModal
          className="w-full text-lg hidden md:inline-block hover:bg-gray-50"
          text="Hướng dẫn Quyên góp"
          guideId={data.DataJson?.GuideId}
        />
      </div>
      {(data.CategoryName && data.PartnerImage) &&
        <>
          <hr className="mt-6 mb-5 opacity-10" />
          <div className="dn-partner">
            <div className="text-gray-500 text-md mb-2">Đồng hành cùng dự án</div>
            <div className="flex items-center relative">
              {data.CategoryIsShowDetail &&
                <a href={getRootSlug(data.CategorySlug)} className="stretched-link">
                  <span className="invisible absolute">{data.CategoryName}</span>
                </a>
              }
              {data.PartnerImage &&
                <div className="pr-3">
                  <div className="relative w-10 ">
                    <img
                      alt={data.CategoryName}
                      src={data.PartnerImage}
                      loading="lazy"
                      className="max-w-full w-full max-h-full"
                    />
                  </div>
                </div>
              }
              <div className="text-md text-gray-800">
                <strong>{data.CategoryName}</strong>
                {data.CategoryIsShowDetail &&
                  <div className="text-blue-500"><i>Tìm hiểu thêm &gt;&gt;</i></div>
                }
              </div>
            </div>
          </div>
        </>
      }
       {(data.ListPartner && data.ListPartner.length > 0) &&
        <>
          <hr className="mt-6 mb-5 opacity-10" />
          <div className="dn-partner">
            <div className="text-gray-500 text-md mb-2">Nhà tài trợ</div>
            {data.ListPartner.map((item, index) => (
               <div className="flex items-center relative" index={index}>
                  {item.IsShowDetail &&
                    <a href={getRootSlug(item.UrlRewrite)} className="stretched-link">
                      <span className="invisible absolute">{item.Name}</span>
                    </a>
                  }

                  {item.Avatar &&
                    <div className="pr-3">
                      <div className="relative w-10 ">
                        <img
                          alt={item.Name}
                          src={item.Avatar}
                          loading="lazy"
                          className="max-w-full w-full max-h-full"
                        />
                      </div>
                    </div>
                  }

                  <div className="text-md text-gray-800">
                    <strong>{item.Name}</strong>
                    {item.IsShowDetail &&
                      <div className="text-blue-500"><i>Tìm hiểu thêm &gt;&gt;</i></div>
                    }
                  </div>
             </div>
            ))}          
          </div>
        </>
      }
       {(data.ListSponsor && data.ListSponsor.length > 0) &&
        <>
          <hr className="mt-6 mb-5 opacity-10" />
          <div className="dn-partner">
            <div className="text-gray-500 text-md mb-2">Đối tác đồng hành</div>
            {data.ListSponsor.map((item, index) => (
               <div className="flex items-center relative" index={index}>
                  {item.IsShowDetail &&
                    <a href={getRootSlug(item.UrlRewrite)} className="stretched-link">
                      <span className="invisible absolute">{item.Name}</span>
                    </a>
                  }

                  {item.Avatar &&
                    <div className="pr-3">
                      <div className="relative w-10 ">
                        <img
                          alt={item.Name}
                          src={item.Avatar}
                          loading="lazy"
                          className="max-w-full w-full max-h-full"
                        />
                      </div>
                    </div>
                  }

                  <div className="text-md text-gray-800">
                    <strong>{item.Name}</strong>
                    {item.IsShowDetail &&
                      <div className="text-blue-500"><i>Tìm hiểu thêm &gt;&gt;</i></div>
                    }
                  </div>
             </div>
            ))}          
          </div>
        </>
      }
      <style jsx>{`
        .dn-value {
          font-family: -apple-system,BlinkMacSystemFont,Roboto,"Helvetica Neue",sans-serif;
        }
        .text-finish {
          color: #7eb54a;
        }
        .dn-progress :global(.bg-finish) {
          background-color: #7eb54a;
        }
      `}</style>
    </div>
  );
};
export default memo(DonationDetail);