import { MoneyFormat } from "@/lib/int-format";
import { useState, useEffect, useRef } from "react";
import dynamic from "next/dynamic";
import { IconHeoVang } from "./IconsDonation";

const InfiniteScroll = dynamic(
  () => import("react-infinite-scroll-component"),
  {
    ssr: false,
  }
);

import { axios } from "@/lib/index";

const Modal = dynamic(() => import("@/components/modal/Modal"));
const ModalBody = dynamic(() => import("@/components/modal/ModalBody"));
const ModalHeader = dynamic(() => import("@/components/modal/ModalHeader"));
const ModalFooter = dynamic(() => import("@/components/modal/ModalFooter"));

const RenderTransItem = ({ item, type }) => {
  return (
    <div
      key={`trans-${item.Rank}`}
      className={`flex items-center justify-between text-sm text-gray-700 font-semibold py-2 border-b border-gray-100 last:border-0`}
    >
      <div className="w-12 mr-1 flex items-center ">
        <div className="w-9 h-9 text-sm font-bold text-gray-500 bg-gray-300 rounded-full flex justify-center items-center">
          {item.SortCusName}
        </div>
      </div>
      <div className="flex-1 text-left">
        <div className="pt-1 mb-1">{item.CusName}</div>
        <div className="text-xs text-gray-400">{item.PhoneNumber}</div>
      </div>
      <div className="flex item-end justify-end">
        {MoneyFormat(item.Amount)}
        {type === 1 && "đ"}
        {type === 2 && <IconHeoVang className="w-6 h-6 ml-1 -top-px" />}
      </div>
    </div>
  );
};

const ModalListTrans = ({ donationId, data, donationType }) => {
  const [items, setItems] = useState([]);
  const [lastIndex, setLastIndex] = useState(0);
  const [hasMore, setHasMore] = useState(true);

  const [modal, setModal] = useState(false);

  const timeout = useRef(null);
  const total = data.TotalItems;

  useEffect(() => {
    setItems([...data.Items]);
    setLastIndex(data.LastIndex);
    setHasMore(data.LastIndex < total ? true : false);

    return () => {
      setItems([]);
      setHasMore(true);

      clearTimeout(timeout.current);
    };
  }, [data]);

  const openModal = () => {
    setModal(true);
  };
  const closeModal = () => {
    setModal(false);
  };

  const fetchMoreData = () => {
    timeout.current = setTimeout(function () {
      axios
        .get(
          `/donation/listTrans?donationId=${donationId}&lastIdx=${lastIndex}&count=10`
        )
        .then((res) => {
          if (!res.Data) {
            return;
          }

          setItems([...items, ...res.Data.Items]);
          setLastIndex(res.Data.LastIndex);
          setHasMore(res.Data.LastIndex < total ? true : false);
        });
    }, 500);
  };

  return (
    <>
      {hasMore && (
        <div className="text-center">
          <button
            type="button"
            onClick={openModal}
            className="py-1 px-6 font-semibold text-gray-700 transition-all border border-gray-600 rounded-full hover:text-gray-800 hover:bg-gray-50 mt-4"
          >
            Xem tất cả
          </button>
        </div>
      )}

      <Modal isOpen={modal} onDismiss={closeModal}>
        <ModalHeader>Danh sách nhà hảo tâm mới nhất </ModalHeader>
        <ModalBody css="p-0 overflow-hidden bg-white">
          <div
            id="scrollableDiv"
            className="px-4 md:px-6 overflow-y-auto h-[80vh] md:h-[70vh]"
          >
            <InfiniteScroll
              className=""
              dataLength={items.length}
              next={fetchMoreData}
              hasMore={hasMore}
              loader={
                <p className="py-4 text-gray-400 text-center">
                  <span>Đang load ....</span>
                </p>
              }
              scrollableTarget="scrollableDiv"
            >
              {items.map((item, index) => (
                <RenderTransItem key={index} item={item} type={donationType} />
              ))}
            </InfiniteScroll>
          </div>
        </ModalBody>
        <ModalFooter className="flex">
          <button
            type="button"
            onClick={closeModal}
            className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
          >
            Đóng
          </button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default function ListTrans({ donationId, data, donationType }) {
  return (
    <>
      <div className="border border-gray-200 rounded px-4 pt-1.5 pb-3">
        {data.Items.map((item, index) => (
          <RenderTransItem
            key={`trans-${index}`}
            item={item}
            type={donationType}
          />
        ))}
      </div>
      <ModalListTrans
        donationId={donationId}
        data={data}
        donationType={donationType}
      />
    </>
  );
}
