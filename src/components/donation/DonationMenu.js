import { getRootSlug } from "@/lib/url";
import BreadcrumbContainer, { BreadcrumbItem } from "../BreadcrumbContainer";

const DonationMenu = ({ title, icon, url }) => {
  if (!title || !url) {
    return <></>;
  }

  return (
    <BreadcrumbContainer>
      <BreadcrumbItem url="https://momo.vn/" />
      <BreadcrumbItem title="Sống tốt" url={getRootSlug(url)} />
      <BreadcrumbItem icon={icon} title={title} url={getRootSlug(url)} />
    </BreadcrumbContainer>
  );
};

export default DonationMenu;
