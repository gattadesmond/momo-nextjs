import DonationItem from "./DonationItem";

const ListDonation = ({data, type = 1}) => {
  // type = 1: full width
  // type = 2: cột bên tay phải
  return (
    <div className={`grid ${type === 1 ? "grid-cols-1 md:grid-cols-2 lg:grid-cols-3 md:grid-rows-1" : "grid-cols-1 md:grid-cols-2 lg:grid-cols-1"} gap-6 mb-6 mt-2`}>
      {data.Items
        .map((item, index) => (
        <DonationItem key={item.Id} data={item} />
      ))}
    </div>
  );
};

export default ListDonation;
