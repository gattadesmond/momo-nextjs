import Link from "next/link";
import Image from "next/image";

import useAppContext from "@/lib/appContext";

import { IconClock, IconHeoVang, IconUser } from "./IconsDonation";

const DonationItem = ({
  className,
  data,
  isInline = false
}) => {
  if (!data) {
    return null;
  }
  const { isViewApp } = useAppContext();

  const renderProgressBar = (finishPercent, isEndCampaign) => {
    if (finishPercent >= 100) {
      return (
        <div className="dn-progress-bar done h-1.5 bg-green-500 rounded-lg bg-finish w-full"></div>
      );
    } else if (finishPercent < 100 && isEndCampaign) {
      return (
        <div
          className="dn-progress-bar end-time h-1.5 bg-gray-500 rounded-lg"
          style={{ width: `${finishPercent}%` }}
        ></div>
      );
    } else {
      return (
        <div
          className="dn-progress-bar h-1.5 bg-momo rounded-lg"
          style={{ width: `${finishPercent}%` }}
        ></div>
      );
    }
  };

  // const renderClockInfo = (
  //   finishPercent,
  //   isEndCampaign,
  //   isContDonate,
  //   timeToEndDate
  // ) => {
  //   return <span className="lowercase">Còn {timeToEndDate}</span>;
  // };

  const renderClockInfo = (
    finishPercent,
    isEndCampaign,
    isContDonate,
    timeToEndDate
  ) => {
    if (finishPercent < 100 && !isEndCampaign) {
      return (
        <>
          <span className="text-xs px-2 py-1 rounded-3xl badge-date-left text-orange-400">
            Còn {timeToEndDate}
          </span>
          <style jsx>{`
            .badge-date-left {
              background: rgba(252, 100, 45, 0.15);
            }
          `}</style>
        </>
      );
    } else {
      return <></>;
    }
  };

  const renderCTA = (
    finishPercent,
    isEndCampaign,
    isContDonate,
    timeToEndDate
  ) => {
    if (finishPercent >= 100) {
      if (!isEndCampaign && isContDonate) {
        return (
          <span 
            className="text-xs text-pink-600 border border-pink-600
            rounded-md flex items-center justify-center font-bold h-7 px-3 md:group-hover:bg-pink-50"
          >
            Quyên góp
          </span>
        );
      } else {
        return (
          <span
            className="text-xs text-gray-400 border border-gray-400 
            rounded-md flex items-center justify-center font-bold h-7 px-3 md:group-hover:bg-gray-50"
          >
            Đạt mục tiêu
          </span>
        );
      }
    } else if (finishPercent < 100 && isEndCampaign) {
      return (
        <span
          className="text-xs text-gray-400 border border-gray-400
          rounded-md flex items-center justify-center font-bold h-7 px-3 md:group-hover:bg-gray-50"
        >
          Đã hết thời hạn
        </span>
      );
    } else {
      return (
        <span className="text-xs text-pink-600 border border-pink-600 
          rounded-md flex items-center justify-center font-bold h-7 px-3 md:group-hover:bg-pink-50"
        >
          Quyên góp
        </span>
      );
    }
  };

  const getViewAppLink = (type, url) => {
    if (type === 1) {
      return `momo://?refId=https://m.momo.vn/quyen-gop/${url}?webToken=donation`;
    } else {
      return `momo://?refId=https://m.momo.vn/quyen-gop-heovang/${url}?webToken=donation`;
    }
  }

  return (
    <>
      <div
        className={`donation-item relative overflow-hidden flex flex-col flex-nowrap text-gray-700 md:hover:text-momo 
          bg-white transition border border-gray-200 rounded-xl group
          ${!isInline ? "shadow-sm hover:shadow-xl" : ""} 
          ${className ? className : ""}
        `}
      >
        <div className="dn-img flex aspect-w-15 aspect-h-8">
          <Image
            alt={data.Title}
            src={data.Avatar}
            layout="fill"
            objectFit="cover"
            loading={isInline ? "eager" : "lazy"}
            priority={isInline ? true : false}
          />
        </div>
        <div className={`dn-body pt-4 pb-3 flex-1 min-h-1 px-4`}>
          <h5 className="dn-title text-lg transition font-bold leading-snug ">
            {data.Title}
          </h5>
          {/* <p className="dn-summary text-md text-gray-500 text-truncate-row text-rows-2">{data.Short}</p> */}
        </div>

        <div className={`dn-footer pt-0 mb-4 px-4`}>
          <div className=" flex flex-nowrap space-x-2 items-center mb-3">
            {data.PartnerImage ? (
              <>
                <div className="shrink-0">
                  <div className="rounded-full overflow-hidden border border-gray-200 p-1">
                    <div className="w-11 h-11 md:w-7 md:h-7 relative">
                      <Image
                        alt={data.CategoryName}
                        src={data.PartnerImage}
                        layout="fill"
                        objectFit="contain"
                      />
                    </div>
                  </div>
                </div>
                {data.CategoryName &&
                  <div className="flex-1 text-xs md:text-sm text-gray-600 leading-4">
                    {data.CategoryName}
                  </div>
                }
              </>
            ) : (
              <div className="flex-1 text-sm text-gray-500 leading-4">
                &nbsp;
              </div>
            )
            }


            <div className="shrink-0 ">
              {renderClockInfo(
                data.FinishPercent,
                data.IsEndCampaign,
                data.IsContDonate,
                data.TimeToEndDate
              )}
            </div>
          </div>

          <div className="dn-money flex items-end mb-2">
            <strong className="flex item-end text-gray-700 leading-5">
              {data.TotalMoney}
              {data.Type === 1 && "đ"}
              {data.Type === 2 && (
                <IconHeoVang className="ml-1 inline-block top-px" />
              )}
            </strong>

            <span className="pl-2 text-xs md:text-sm text-gray-500">
              / {data.ExpectedValueFormat}
              {data.Type === 1 && "đ"}
              {data.Type === 2 && <span className="font-bold"> Heo vàng</span>}
            </span>
          </div>
          <div className="dn-progress flex overflow-hidden h-1.5 w-full bg-gray-200 rounded-lg my-1">
            {renderProgressBar(data.FinishPercent, data.IsEndCampaign)}
          </div>

          <div
            className={` flex flex-nowrap justify-between space-x-2  md:space-x-3 items-center mt-3 
              ${data.FinishPercent >= 100 ? "text-finish" : ""}
            `}
          >
            <div className="grow ">
              <div className=" text-xs text-gray-500">Lượt quyên góp</div>
              <div className=" text-sm font-bold text-gray-600">
                {data.TotalTrans}
              </div>
            </div>

            <div className="grow">
              <div className=" text-xs text-gray-500">Đạt được</div>
              <div className=" text-sm font-bold text-gray-600">
                {data.FinishPercent}%
              </div>
            </div>

            <div className="grow flex items-center justify-end">
              {!isViewApp && (
                <Link href={data.Link}>
                  <a className="stretched-link">
                    {renderCTA(
                      data.FinishPercent,
                      data.IsEndCampaign,
                      data.IsContDonate,
                      data.TimeToEndDate
                    )}
                  </a>
                </Link>
              )}
              {isViewApp &&
                <a href={getViewAppLink(data.Type, data.UrlRewrite)}
                  className="stretched-link">
                  {renderCTA(
                    data.FinishPercent,
                    data.IsEndCampaign,
                    data.IsContDonate,
                    data.TimeToEndDate
                  )}
                </a>
              }
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .donation-item:hover .dn-title {
          color: ;
        }
        .text-finish {
          color: #7eb54a;
        }
        .dn-progress :global(.bg-finish) {
          background-color: #7eb54a;
        }
      `}</style>
    </>
  );
};
export default DonationItem;
