import { useEffect, useRef, useState } from "react";

import AnchorLink from "react-anchor-link-smooth-scroll";

import Container from "@/components/Container";
import CssCarousel from "@/components/CssCarousel";
import ScrollToElement from "@/components/ScrollToElement";

//https://www.emgoto.com/react-table-of-contents/
const useIntersectionObserver = (setActiveId, data) => {
  const headingElementsRef = useRef({});
  useEffect(() => {
    const callback = (headings) => {
      headingElementsRef.current = headings.reduce((map, headingElement) => {
        map[headingElement.target.id] = headingElement;
        return map;
      }, headingElementsRef.current);

      //Get all headings that are currently visible on the page
      const visibleHeadings = [];
      Object.keys(headingElementsRef.current).forEach((key) => {
        const headingElement = headingElementsRef.current[key];
        if (headingElement.isIntersecting) visibleHeadings.push(headingElement);
      });

      //if there is only one visible heading, this is our "active" heading
      if (visibleHeadings.length >= 1) {
        setActiveId(visibleHeadings[0].target.id);
      }
    };

    const observer = new IntersectionObserver(callback, {
      rootMargin: "-60px 0px 0px 0px",
    });

    data.forEach(
      (element) =>
        document.getElementById(element.Id) != null &&
        observer.observe(document.getElementById(element.Id))
    );
    return () => observer.disconnect();
  }, [setActiveId]);
};

export default function StickyMenuDetail({ menuSticky }) {
  const ref = useRef();

  const [stick, setStick] = useState(false);
  const [activeId, setActiveId] = useState(null);
  const [visible, setVisible] = useState(false);

  useIntersectionObserver(setActiveId, menuSticky);

  useEffect(() => {
    const element = ref.current;

    const update = () => {
      const rect = element.getBoundingClientRect().y;

      if (rect <= 1) {
        setStick(true);
        setVisible(window.pageYOffset > 200 ? true : false);
      } else {
        setStick(false);
      }
    };
    update();

    document.addEventListener("scroll", update, { passive: true });
    return () => {
      setStick(false);
      document.removeEventListener("scroll", update);
    };
  }, []);

  return (
    <div ref={ref} 
      className={`fixed w-screen max-w-full md:sticky -top-px z-10 md:z-20 bg-white transition duration-200 border-b 
        ${visible ? "opacity-100" : "opacity-0 md:opacity-100"}
        ${stick ? "border-gray-200" : "border-transparent"}
      `}>
      <Container className="px-0">
        <CssCarousel activeId={activeId}>
          <div className={`breadcrumb flex overflow-scroll w-full pl-5 md:pl-0 border-b ${!stick ? "border-gray-200" : "border-transparent"}`}>
            {menuSticky.map((item, index) => (
              <ScrollToElement 
                key={`menu-${item.Id}`}
                className={`cursor-pointer whitespace-nowrap text-md pt-4 pb-3 mr-5 md:mr-8 transition border-b-2
                  ${item.Id === activeId ?  "is-active text-gray-700 border-gray-700 font-extrabold" : "text-gray-500 font-normal border-transparent md:hover:border-gray-400"}
                `}
                href={`#${item.Id}`}
                offset={60}
              >
                <span className="">{item.Name}</span>
              </ScrollToElement>
            ))}
            <div className="w-1">&nbsp;</div>
          </div>
        </CssCarousel>
      </Container>
    </div>
  )
}