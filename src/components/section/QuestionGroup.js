import { useState } from "react";
import { decode } from "html-entities";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

import QuestionList from "./QuestionList";
import CssCarousel from "../CssCarousel";

const ArticleQuestionGroup = ({ data, isMobile }) => {
  if (!data || !data.Data || data.Data.length === 0) return null;

  const Groups = data.Data.sort((a, b) => a.DisplayOrder - b.DisplayOrder);

  const [tabIndex, setTabIndex] = useState(0);
  const handleTabsChange = (index) => setTabIndex(index);

  return (
    Groups[0] && (
      <>
        <div className="mt-4 md:mt-8">
          <h2 className="text-center text-momo">{data.Title}</h2>
          <Tabs index={tabIndex} onChange={handleTabsChange}>
            {Groups?.length > 1 && (
              <CssCarousel className="-mx-5">
                <TabList className="flex items-center md:justify-center space-x-6 flex-nowrap overflow-x-auto pl-5 md:pl-0">
                  {Groups.map((item, index) => (
                    <Tab
                      key={index}
                      className={`block px-2 py-1  font-semibold outline-none ring-0 border-b-2  ${
                        index == tabIndex
                          ? "text-gray-800 border-pink-600 is-active"
                          : "text-gray-500 border-transparent"
                      }`}
                    >
                      {item.Name}
                    </Tab>
                  ))}
                  <div className="w-1">&nbsp;</div>
                </TabList>
              </CssCarousel>
            )}

            <TabPanels>
              {Groups.map((group, index) => (
                <TabPanel key={group.Id}>
                  <Accordion
                    collapsible
                    className="divide-y divide-gray-200 article-question"
                  >
                    <QuestionList data={group.Items} />
                  </Accordion>
                </TabPanel>
              ))}
            </TabPanels>
          </Tabs>
        </div>

        <style jsx>
          {`
            :global(.article-question
                [data-reach-accordion-item][data-state="open"]
                .icon) {
              transform: rotate(180deg);
            }
          `}
        </style>
      </>
    )
  );
};

export default ArticleQuestionGroup;
