import AnchorLink from "react-anchor-link-smooth-scroll";
import ScrollToElement from "@/components/ScrollToElement";

export default function MenuSticky({ data, isMobile }) {
  // if(data.Projects[0] == undefined) {
  //   return null
  // }
  // const List = data.Projects[0].Items;


  const items = data.Items ?? null;

  return (
    <>
      {items && <nav className="fixed left-0 right-0 bg-white z-20 flex items-center  menu-bottom border-t border-gray-200 md:hidden bg-opacity-95 backdrop-blur-sm" aria-hidden="false">
        <div className="w-full max-w-sm mx-auto flex items-center justify-around">
          {items.map((item, index) => (
            <ScrollToElement href={`#${item.Target ?? "home"}`} offset={52} className="flex-0 group" key={index}>
              <div className="px-2  text-center">
                {item.Icon &&
                  <img className="w-5 h-5  inline-block" src={item.Icon ?? ""} width="20" height="20" loading="lazy" />
                }
                <div className="mt-1 text-xs font-semibold text-gray-800 no-underline group-focus:text-pink-600 whitespace-nowrap">{item.Name ?? ""}</div>
              </div>
            </ScrollToElement>
          ))}


          {/* 
          <AnchorLink href="#cumrap" offset={0} className="flex-0 group">
            <div className="px-2  text-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 text-pink-600  inline-block"
                viewBox="0 0 16 16"
              >
                <path
                  d="M13.06 14.84H2.94a1.79 1.79 0 01-1.79-1.79v-8.9a1.79 1.79 0 011.79-1.79H3v.73a.83.83 0 001.65 0v-.73h2.52v.73a.83.83 0 001.66 0v-.73h2.51v.73a.83.83 0 001.65 0v-.73h.07a1.79 1.79 0 011.79 1.79v8.9a1.79 1.79 0 01-1.79 1.79zm.64-9.71H2.3v7.78a.77.77 0 00.78.77h9.84a.77.77 0 00.78-.77zm-1.54-1.58a.47.47 0 01-.46-.46V1.62a.47.47 0 01.46-.46.46.46 0 01.46.46v1.47a.46.46 0 01-.46.46zM8 3.55a.47.47 0 01-.46-.46V1.62a.46.46 0 01.92 0v1.47a.47.47 0 01-.46.46zm-4.16 0a.46.46 0 01-.46-.46V1.62a.46.46 0 01.46-.46.47.47 0 01.46.46v1.47a.47.47 0 01-.46.46z"
                  id="m2"
                  fill="currentColor"
                />
              </svg>
              <div className="mt-1 text-xs font-semibold text-gray-800 no-underline group-focus:text-pink-600 whitespace-nowrap">Lịch chiếu</div>
            </div>
          </AnchorLink>

*/}

        </div>
      </nav>

      }

   

      <style jsx>{`
      .menu-bottom{
          margin-bottom: env(safe-area-inset-bottom) !important;
          padding-left: env(safe-area-inset-left) !important;
          padding-right: env(safe-area-inset-right) !important;

          height: 120px;
          bottom: -60px;
          padding-bottom: 60px;
        }

        .menu-bottom + :global(section){
          display:none;
        }
        
        `}</style>


    </>
  );
}
