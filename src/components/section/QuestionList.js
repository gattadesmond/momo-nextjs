// import { useState, useEffect } from "react";
import { decode } from "html-entities";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";

import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

export default function QuestionList({ data, title = "", isMobile }) {
  if (!data) return null;

  return (
    <>
      {title &&
        <h2 className="text-center text-momo mb-0">
          {title}
        </h2>
      }
      {data.length > 0 &&
        <>
          <Accordion
            collapsible
            className="divide-y divide-gray-200 article-question"
          >
            {data.map((item, index) => (
              <AccordionItem className="block " key={item.Id}>
                <AccordionButton className="question-title relative block w-full text-left py-3 pl-0 pr-5 md:py-4 text-lg font-semibold text-pine-500 hover:text-opacity-70 cursor-pointer focus:outline-none">
                  <div className="">
                    {decode(item.Title)}
                  </div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="absolute right-0 inline w-5 h-5 -mt-2 text-gray-500  icon top-1/2"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </AccordionButton>

                <AccordionPanel className="pb-5 text-base soju__prose small text-gray-600">
                  <ArticleContentConvert data={decode(item.Content)} />
                </AccordionPanel>
              </AccordionItem>
            ))}
          </Accordion>

          <style jsx>{`
            :global(.article-question [data-reach-accordion-item][data-state="open"] .icon) {
              transform: rotate(180deg);
            }
          `}</style>
        </>
      }
    </>
  );
}
