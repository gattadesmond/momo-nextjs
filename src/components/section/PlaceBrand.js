import Image from "next/image";
import Link from "next/link";

import { Swiper, SwiperSlide } from 'swiper/react';
// import Swiper core and required modules
import SwiperCore, { Navigation } from 'swiper/core';
// install Swiper modules
SwiperCore.use([Navigation]);

import { getRootSlug } from "@/lib/url";


const ItemPlaceBrand = ({ brand }) => {
  const checkImgSrc = (url) => {
    const arrayUrl = url.split('.');
    const fileType = arrayUrl[arrayUrl.length - 1];
    if (["jpg", "png", "jpeg"].includes(fileType)) {
      return true;
    } else {
      return false;
    }
  }
  return (
    <div key={brand.Id}
      className="partner relative bg-white border border-gray-200 rounded transition hover:shadow-lg"
    >
      {(brand.Url || brand.Link) &&
        <Link href={getRootSlug(brand.Url || brand.Link)}>
          <a target={brand.IsNewTab ? "_blank" : "_self"} className="stretched-link">
            <span className="hidden">{brand.Name}</span>
          </a>
        </Link>
      }
      <div className="grid grid-cols-12 items-center">
        <div className="col-span-3 p-3">
          {brand.Logo && (
            checkImgSrc(brand.Logo)
              ?
              <Image
                className="max-w-full"
                alt={brand.Name}
                src={brand.Logo}
                width={58}
                height={58}
                layout="responsive"
                loading="lazy"
              />
              :
              <img
                className="max-w-full"
                alt={brand.Name}
                src={brand.Logo}
                width={58}
                height={58}
                loading="lazy"
              />
          )
          }
        </div>
        <div className="col-span-9 px-3.5 py-2">
          <h3 className="text-base text-momo font-bold" style={{ color: '#e81f76' }}>{brand.Name}</h3>
          <p className="text-xs text-gray-600 mb-0">{brand.Description || brand.Short}</p>
        </div>
      </div>
    </div>
  );
}

const PlaceBrand = ({ data, ...rest }) => {
  let items = [...data?.Items] || [];
  items = items
    .filter(a => !!a.Name)
    .sort((a, b) => a.DisplayOrder - b.DisplayOrder);

  const countItemOfPage = 3;
  const page = parseInt(items.length / countItemOfPage) + (items.length % countItemOfPage);
  const arrayPage = [...Array(page)].fill(null);

  return (
    <>
      {items.length <= 9 &&
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-x-6 gap-y-4">
          {items
            .map((brand, index) => (
              <ItemPlaceBrand key={brand.Id} brand={brand} />
            ))
          }
        </div>
      }
      {items.length > 9 &&
        <div className="swp-brand">
          <Swiper
            className=""
            slidesPerView={1}
            spaceBetween={10}
            slidesPerGroup={1}
            // autoplay={{
            //   "delay": 5000,
            //   "disableOnInteraction": false
            // }}
            // speed={800}
            preloadImages={false}
            lazy={true}
            // pagination={{
            //   "clickable": true
            // }}
            navigation={true}
            breakpoints={{
              // when window width is >= 728px
              768: {
                slidesPerView: 2,
                slidesPerGroup: 1,
                spaceBetween: 20
              },
              // when window width is >= 1024px
              1024: {
                slidesPerView: 3,
                slidesPerGroup: 1,
                spaceBetween: 30
              },
            }}
          >
            {arrayPage.map((v, index) => (
              <SwiperSlide key={index} className="grid grid-cols-1 gap-x-6 gap-y-4">
                {items
                  .filter((a, i) => i >= (index * countItemOfPage) && i < (index * countItemOfPage + countItemOfPage))
                  .map((brand) => (
                    <ItemPlaceBrand key={brand.Id} brand={brand} />
                  ))
                }
              </SwiperSlide>
            ))}
          </Swiper>
          <style jsx>{`
            .swp-brand :global(.swiper-button-prev),
            .swp-brand :global(.swiper-button-next) {
              top: 64%;
            }
          `}</style>
        </div>
      }
    </>
  )
};

export default PlaceBrand;