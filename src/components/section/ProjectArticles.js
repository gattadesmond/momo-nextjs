import { useState, useEffect } from "react";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

import ArticleListAjax from "@/components/ui/ArticleListAjax";

import Link from "next/link";
import CssCarousel from "@/components/CssCarousel";

export default function ProjectArticles({
  data,
  isMobile = false,
  isNumber = null,
}) {
  if (data.Projects[0] == undefined) {
    return null;
  }

  const Cat = data.Projects.length > 1 ? data.Projects : null;

  let List = null;
  if (!Cat) {
    List = data.Projects[0];
  }

  const [tabIndex, setTabIndex] = useState(0);

  return (
    <>
      {Cat && (
        <Tabs index={tabIndex} onChange={(index) => setTabIndex(index)}>
          <CssCarousel className="-mx-5">
            <TabList className="flex items-center md:justify-center space-x-3 md:space-x-6 overflow-scroll pl-5 md:pl-0">
              {Cat.map((item, index) => (
                <Tab
                  key={index}
                  className={`block px-2 py-1 whitespace-nowrap font-semibold outline-none ring-0 border-b-2 ${
                    index == tabIndex
                      ? "text-gray-800 border-pink-600 is-active"
                      : "text-gray-500 border-transparent"
                  }`}
                >
                  {item.Title}
                </Tab>
              ))}
              <div className="w-1">&nbsp;</div>
            </TabList>
          </CssCarousel>
          <TabPanels>
            {Cat.map((item, index) => (
              <TabPanel key={index} className="pt-5">
                <ArticleListAjax
                  item={item}
                  isNumber={isNumber}
                  isMobile={isMobile}
                />
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      )}

      {!Cat && (
        <ArticleListAjax item={List} isNumber={isNumber} isMobile={isMobile} />
      )}

      <style jsx>{``}</style>
    </>
  );
}
