import { useState, useEffect } from "react";

import ArticleBox1 from "@/components/ui/ArticleBox1";

export default function GroupPromotionArticles({ data, isMobile = false }) {
  const List = data.Groups[0].Items;
  // const ListName = data.Items[0].Name;

  return (
    <>
      {/* <div className="mb-1 text-sm font-semibold text-gray-400 uppercase">
        {ListName}
      </div> */}
      <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-4">
        {List &&
          List.map((item, index) => (
            <ArticleBox1
              data={item}
              key={index}
              isModal={true}
              isMobile={isMobile}
            />
          ))}
      </div>

      <style jsx>{``}</style>
    </>
  );
}
