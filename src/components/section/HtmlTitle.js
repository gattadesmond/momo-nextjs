import ReadMore from "@/components/ReadMore";

export default function HtmlTitle({ data, isMobile, isReadMore }) {
  // if(data.Projects[0] == undefined) {
  //   return null
  // }
  // const List = data.Projects[0].Items;

  return (
    <>
      <div className="mx-auto max-w-4xl">
        <div className="mt-5 soju__prose small">
          {data && <ReadMore maxLine="4" btnText="... Xem thêm" type="html" isReadMore={isReadMore}>
            {data}
          </ReadMore>}

        </div>
      </div>

      <style jsx>{``}</style>
    </>
  );
}
