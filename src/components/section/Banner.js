import { useEffect, useState } from "react";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";

import SwiperCore, { Navigation, Pagination } from "swiper/core";
SwiperCore.use([Navigation, Pagination]);

const PictureItem = ({ item }) => {
  return (
    <div className="relative aspect-[5/6] overflow-hidden sm:aspect-[96/37]">
      {item.Url ? (
        <Link href={item.Url}>
          <a target={item.IsNewTab ? "_blank" : "_self"}>
            <picture>
              <source
                media="(max-width: 639px)"
                srcSet={item.AvatarMobile}
              />
              <img
                src={item.Avatar}
                loading="lazy"
                className="absolute inset-0 w-full "
                alt={item.Name}
              />
            </picture>
          </a>
        </Link>
      ) : (
        <picture>
          <source
            media="(max-width: 639px)"
            srcSet={item.AvatarMobile}
          />
          <img
            src={item.Avatar}
            loading="lazy"
            className="absolute inset-0 w-full "
            alt={item.Name}
          />
        </picture>
      )}
    </div>
  );
};

const Banner = ({ data }) => {
  const Images = data.Data.Items || [];
  if (Images.length <= 0) {
    return null;
  }

  const [swiperObject, setSwiperObject] = useState(null);
  const [swiperLeft, setSwiperLeft] = useState(false);
  const [swiperRight, setSwiperRight] = useState(false);

  const clickRight = () => {
    swiperObject.slideNext();
  };
  const clickLeft = () => {
    swiperObject.slidePrev();
  };

  const slideChange = () => {
    swiperObject.isEnd ? setSwiperRight(false) : setSwiperRight(true);
    swiperObject.isBeginning ? setSwiperLeft(false) : setSwiperLeft(true);
  };

  useEffect(() => {
    if (swiperObject) {
      slideChange();
    }
  }, [swiperObject]);


  return (
    <div className="swiper-hero relative -mb-3 max-w-full sm:-mb-4 md:-mb-6 lg:-mb-7">
      {Images.length <= 1 ? (
        <PictureItem item={Images[0]} />
      ) : (
        <>
          <Swiper
            slidesPerView={1}
            spaceBetween={0}
            autoplay={{
              delay: 5000,
              disableOnInteraction: false,
            }}
            speed={800}
            pagination={{
              clickable: true,
            }}
            onSwiper={setSwiperObject}
            onSlideChange={() => slideChange()}
          >
            {Images.sort((a, b) => b.DisplayOrder - a.DisplayOrder).map(
              (item, index) => (
                <SwiperSlide className="" key={"banner" + index}>
                  <PictureItem item={item} />
                </SwiperSlide>
              )
            )}
          </Swiper>

          <div className="absolute left-0 right-0 top-1/2 mx-auto w-full max-w-6xl px-5 md:px-8 lg:px-8">
            {swiperObject && swiperRight && (
              <div
                onClick={() => clickRight()}
                className="button-next button-swiper absolute right-4 top-1/2 z-20 flex  h-9 w-9 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full border border-gray-200 bg-white text-black shadow-md transition-all hover:opacity-90 md:h-11 md:w-11"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className=" h-6 w-6 md:h-7 md:w-7"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M9 5l7 7-7 7"
                  />
                </svg>
              </div>
            )}
            {swiperObject && swiperLeft && (
              <div
                onClick={() => clickLeft()}
                className="button-next button-swiper absolute left-4 top-1/2 z-20 flex h-9 w-9  -translate-y-1/2 cursor-pointer items-center justify-center rounded-full border border-gray-200 bg-white text-gray-700 shadow-md transition-all hover:opacity-90 md:h-11 md:w-11"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className=" h-6 w-6 md:h-7 md:w-7"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15 19l-7-7 7-7"
                  />
                </svg>
              </div>
            )}
          </div>
        </>
      )}

      <svg className="absolute h-0 w-0">
        <defs>
          <clipPath id="my-clip-path" clipPathUnits="objectBoundingBox">
            <path d="M0,0 H1 V1 C1,1,0.823,0.96,0.5,0.96 C0.177,0.96,0,1,0,1 V0" />
          </clipPath>
        </defs>
      </svg>

      <svg className="absolute h-0 w-0">
        <clipPath id="my-clip-path2" clipPathUnits="objectBoundingBox">
          <path d="M0,0 H1 V1 C1,1,0.821,0.984,0.5,0.984 C0.179,0.984,0,1,0,1 V0"></path>
        </clipPath>
      </svg>
      <style jsx>{`
        .swiper-hero :global(.swiper-pagination) {
          bottom: 40px;
          z-index: 20;
        }

        .swiper-hero {
          -webkit-clip-path: url(#my-clip-path);
          clip-path: url(#my-clip-path);
        }

        @media (max-width: 769px) {
          .swiper-hero {
            -webkit-clip-path: url(#my-clip-path2);
            clip-path: url(#my-clip-path2);
          }

          .swiper-hero :global(.swiper-pagination) {
            bottom: 20px;
          }
        }

        .swiper-hero :global(.swiper-button-prev) {
        }
      `}</style>
    </div>
  );
};
export default Banner;
