
import { useState } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

import Heading from "@/components/Heading";
import CssCarousel from "../CssCarousel";

const InformationSecurity = ({ className, data }) => {
  const [tabIndex, setTabIndex] = useState(0);
  const handleTabsChange = (index) => setTabIndex(index);

  const contents = [
    {
      Title: 'Bảo chứng ngân hàng',
      Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-210330101137-637526958979794431.jpg",
      Content: [
        "Tiền của bạn trong Ví MoMo là TIỀN THẬT và được bảo chứng 100% bởi các NGÂN HÀNG đang hợp tác với Ví MoMo.",
        "Người dùng MoMo có thể Nạp/Rút tiền từ ngân hàng liên kết bất kỳ lúc nào.",
        "MoMo đã liên kết trực tiếp với 25 ngân hàng lớn và các tổ chức thẻ quốc tế Visa/Master/JCB."
      ]
    },
    {
      Title: 'Bảo mật đa tầng',
      Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-201210133205-637432039255627068.jpg",
      Content: [
        "Để đăng nhập, cần nhập mật khẩu 6 số và mã OTP được gửi đến điện thoại. Mỗi lần thanh toán đều cần nhập lại mật khẩu.",
        "Ứng dụng tự khóa sau 5 phút nếu không sử dụng.",
      ]
    },
    {
      Title: 'Đạt chuẩn quốc tế',
      Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-201210133226-637432039467379120.jpg",
      Content: [
        "Công nghệ bảo mật của Ví MoMo đáp ứng các tiêu chuẩn quốc tế khắt khe nhất trong ngành tài chính ngân hàng, đạt chứng nhận bảo mật toàn cầu PCI DSS cấp độ cao nhất.",
        "Công nghệ mã hóa đường truyền SSL/TLS đạt chứng nhận quốc tế GlobalSign.",
      ]
    },
  ]
  return (
    <>
      <div className="mb-5 text-center md:mb-8" id="section-html">
        <Heading title="Thanh toán an toàn - Bảo mật tuyệt đối" color="pink" />
      </div>
      <div className="">
        <div className="warp-scroll">
          <CssCarousel>
            <div className="flex overflow-scroll w-full pl-5 md:pl-0 md:justify-center">
              {contents.map((item, index) => (
                <div key={index}
                  className={`flex whitespace-nowrap cursor-pointer py-3 mx-4 transition font-semibold text-gray-700 hover:text-momo border-b-2 
                    ${tabIndex === index ? "border-momo" : " border-transparent"}`}
                  onClick={() => handleTabsChange(index)}
                >
                  {item.Title}
                </div>
              ))}
            </div>
          </CssCarousel>
        </div>
        <Tabs index={tabIndex} onChange={handleTabsChange}>
          <TabPanels className="pt-8 md:pt-4">
            {contents.map((item, index) => (
              <TabPanel key={index}>
                <div className="grid md:grid-cols-2 items-center">
                  <div className="order-0 md:order-2">
                    <img
                      className="img-fluid block mx-auto px-3 px-md-0"
                      alt={item.Title}
                      src={item.Avatar}
                      width={500}
                      height={434}
                      loading="lazy"
                    />
                  </div>
                  <div className="order-1 pt-8">
                    {item.Content.map((content, cIndex) => (
                      <div key={cIndex} className="mb-3 lg:mb-4 mr-4 lg:ml-0 lg:mr-4">
                        <div className="flex items-center">
                          <div className="rounded-circle bg-success-alt">
                            <svg className="injected-svg m-2 icon icon-xs bg-success" fill="none" height="24" viewBox="0 0 24 24" width="24">
                              <path d="M20 6L9 17L4 12" stroke="#2C3038" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path>
                            </svg>
                          </div>

                          <div className="mb-0 ml-3">{content}</div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </div>
      <style jsx>{`
        @media (max-width: 767px) {
          .warp-scroll {
            margin-left: -20px;
            margin-right: -20px;
          }
        }
        .bg-success-alt {
            background-color: rgba(40,167,69,.1);
        }
        .rounded-circle {
            border-radius: 50%!important;
        }
        .icon.icon-xs {
          height: 1rem;
          flex-shrink: 0;
          width: auto;
          vertical-align: middle;
        }
        .icon.icon-xs path[stroke] {
          stroke-width: 3px;
        }
        svg.bg-success [stroke]:not([stroke=none]) {
          stroke: #28a745;
        }
      `}</style>
    </>
  )
};

export default InformationSecurity;