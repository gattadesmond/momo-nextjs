import ProjectArticles from "./ProjectArticles";

import {  Fragment} from "react";

import HtmlTitle from "./HtmlTitle";

import ProjectBlogs from "./ProjectBlogs";

export default function SectionCinemaDetail({
  data,
  isMobile,
  isPink = false,
}) {
  return (
    <>
      {data
        .filter(
          (item) =>
            item.TypeName != "CtaFooter" && item.TypeName != "BreadCrumb"
        )
        .map((item, index) => (
          <Fragment key={item.Id}>
            {item.TypeName == "ProjectBlogs" && (
              <>
                <section className="py-8 border-t border-gray-200">
                  <h3 className="text-xl font-semibold mb-4">{item.Title}</h3>
                  <ProjectBlogs
                    template={item.Template}
                    data={item.Data}
                    isMobile={isMobile}
                    isNumber="3"
                  />
                </section>
              </>
            )}

            {item.TypeName == "ProjectArticles" && (
              <>
                <section className="py-8 border-t border-gray-200">
                  <h3 className="text-xl font-semibold mb-4">{item.Title}</h3>
                  <ProjectArticles
                    template={item.Template}
                    data={item.Data}
                    isMobile={isMobile}
                    isNumber="3"
                  />
                </section>
              </>
            )}

            {item.TypeName == "HtmlTitle" && (
              <>
                <section className="py-8 border-t border-gray-200">
                  <h3 className="text-xl font-semibold mb-4">{item.Title}</h3>
                  <HtmlTitle
                    template={item.Template}
                    data={item.Content}
                    isMobile={isMobile}
                  
                  />
                </section>
              </>
            )}
          </Fragment>
        ))}
    </>
  );
}
