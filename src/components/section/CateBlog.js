import { useState, useEffect } from "react";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import BlogListAjax from "@/components/ui/BlogListAjax";

// import Link from "next/link";

export default function CateBlog({ data, isMobile = false }) {
  const Cat = data.Cates;

  const [tabIndex, setTabIndex] = useState(0);
  // const ListName = data.Items[0].Name;

  return (
    <>
      <Tabs index={tabIndex} onChange={(index) => setTabIndex(index)}>
        <TabList className="flex items-center justify-center space-x-6 flex-nowrap">
          {Cat.map((item, index) => (
            <Tab
              key={index}
              className={`block px-2 py-1  font-semibold outline-none ring-0 border-b-2  ${
                index == tabIndex
                  ? "text-gray-800 border-pink-600"
                  : "text-gray-500 border-transparent"
              }`}
            >
              {item.Title}
            </Tab>
          ))}
        </TabList>
        <TabPanels>
          {Cat.map((item, index) => (
            <TabPanel key={index} className="pt-5">
              <BlogListAjax item={item} isMobile={isMobile} />
            </TabPanel>
          ))}
        </TabPanels>
      </Tabs>

      <style jsx>{``}</style>
    </>
  );
}
