import { getRootSlug } from "@/lib/url";
import Image from "next/image";
import Link from "next/link";

import Container from "../Container";

const ChildLinks = ({ data }) => {
  if (!data.Items || data.Items.length <= 0) {
    return null;
  }

  return (
    <>
      <section className="sc-menu">
        <Container className="flex justify-center">
          <div className="flex flex-row flex-wrap justify-center">
            {data.Items.length > 0 &&
              data.Items
                .sort((a,b) => a.Id - b.Id)
                .map((item, index) => (
                  <div key={item.Id} className={`service-item relative w-[100px] h-auto rounded py-2.5 px-3 m-2.5
                bg-white border border-gray-200 transition shadow hover:shadow-lg text-gray-500 hover:text-momo`}>
                    {item.Url &&
                      <Link href={getRootSlug(item.Url)}>
                        <a className="stretched-link">
                          <span className="hidden">{item.Name}</span>
                        </a>
                      </Link>
                    }
                    <div className="rounded-full service-icon w-11 h-11 p-2 mx-auto" style={{ backgroundColor: item.BgColor }}>
                      <Image
                        className="brightness-0 invert"
                        alt={item.Name}
                        src={item.Logo}
                        layout="intrinsic"
                        width={37}
                        height={37}
                        loading="eager"
                        priority={true}
                      />
                    </div>
                    <div className="pt-2 text-xs font-medium text-center">
                      {item.Name}
                    </div>
                  </div>
                ))
            }
          </div>
        </Container>
      </section>
      <style jsx>{`
        .sc-menu {
          padding: 0;
          display: block;
          position: relative;
          margin-top: -35px;
          z-index: 10;
        }
        @media (min-width: 576px) {
          .sc-menu {
            margin-top: -50px;
          }
        }
        .service-item:hover {
          transform:translate3d(0,-3px,0);
        }
      `}</style>
    </>
  )
};
export default ChildLinks;