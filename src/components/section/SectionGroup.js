import dynamic from "next/dynamic";

// import { useState, useEffect } from "react";
import SectionSpace from "@/components/SectionSpace";
import Heading from "@/components/Heading";
import ContentTab from "./ContentTab";
import parse, { domToReact } from "html-react-parser";

const LdJson_QA = dynamic(() => import("@/components/LdJson_QA"));
const HelpArticleGroup = dynamic(() => import("./HelpArticleGroup"));
const QuestionList = dynamic(() => import("./QuestionList"));
const ProjectArticles = dynamic(() => import("./ProjectArticles"));
const HtmlTitle = dynamic(() => import("./HtmlTitle"));
const AboutH1 = dynamic(() => import("./AboutH1"));
const GroupPromotionArticles = dynamic(() =>
  import("./GroupPromotionArticles")
);
const CateBlog = dynamic(() => import("./CateBlog"));
const ProjectBlogs = dynamic(() => import("./ProjectBlogs"));
const PlaceBrand = dynamic(() => import("./PlaceBrand"));
const ItemsContent = dynamic(() => import("./ItemsContent"));
const Usp = dynamic(() => import("./Usp"));
const InformationSecurity = dynamic(() => import("./InformationSecurity"));
const MenuSticky = dynamic(() => import("./MenuSticky"));
const Banner = dynamic(() => import("./Banner"));
const ChildLinks = dynamic(() => import("./ChildLinks"));
const ProjectDonations = dynamic(() => import("./ProjectDonations"));
const GoogleForm = dynamic(() => import("./GoogleForm"));

export default function SectionGroup({
  data,
  isMobile,
  isPink,
  startType = false,
  genLdJson = true,
  isViewApp = false,
}) {
  // 'bg-gray-50': type == 1,
  // 'bg-white': type == 2,
  // 'bg-pink-50': type == 3,
  const getType = (index) => {
    let startIndex = index;

    if (startType != false) {
      startIndex = startType - 1 + index;
    }

    if (data.findIndex((x) => x.TypeName === "ChildLinks") > -1) {
      startIndex = startIndex + 1;
    }

    return (startIndex % 3) + 1;
  };

  return (
    <>
      {data
        .filter(
          (item) =>
            ["CtaFooter", "BreadCrumb", "Html"].includes(item.TypeName) ===
            false
        )
        .map((item, index) => (
          <div
            key={item.Id}
            id={`block-${item.Id || -1}`}
            data-index={index}
            data-type={getType(index)}
          >
            {item.TypeName == "Banner" && (
              <Banner
                template={item.Template}
                data={item}
                isMobile={isMobile}
              />
            )}
            {item.TypeName == "ChildLinks" && (
              <ChildLinks
                template={item.Template}
                data={item.Data}
                isMobile={isMobile}
              />
            )}

            {item.TypeName == "AboutH1" && (
              <SectionSpace type={getType(index)}>
                <AboutH1
                  template={item.Template}
                  data={item.Data}
                  config={item.Config}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "HelpArticleGroup" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-help">
                  <Heading title={item.Title} color="pink" />
                </div>
                <div>
                  <HelpArticleGroup
                    template={item.Template}
                    data={item.Data}
                    isShowTitle={false}
                    isMobile={isMobile}
                  />
                </div>
              </SectionSpace>
            )}

            {item.TypeName == "QuestionGroup" && (
              <SectionSpace type={getType(index)}>
                <div
                  className="grid grid-cols-1 md:grid-cols-3 md:gap-6"
                  id="section-question"
                >
                  <div className="">
                    <div className="mb-5 text-center md:mt-5 md:text-left">
                      <Heading
                        title={item.Title}
                        color="pink"
                        subtitle={item.Short}
                      />
                      {!isMobile && item.Data.BtnMoreContent && (
                        <div className="small mx-auto mt-2 max-w-3xl text-gray-500">
                          {parse(item.Data.BtnMoreContent)}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="md:col-span-2 md:pl-5">
                    {genLdJson && <LdJson_QA QaData={item.Data} />}

                    <QuestionList
                      template={item.Template}
                      data={item.Data.Items[0].ListQuestions}
                      isShowTitle={false}
                      isMobile={isMobile}
                    />
                  </div>
                  {isMobile && item.Data.BtnMoreContent && (
                    <div className="small mx-auto mt-2 max-w-3xl text-gray-500">
                      {parse(item.Data.BtnMoreContent)}
                    </div>
                  )}
                </div>
              </SectionSpace>
            )}

            {item.TypeName == "GroupPromotionArticles" && (
              <SectionSpace type={getType(index)}>
                <div
                  className="mb-5 text-center md:mb-8"
                  id="section-promotion"
                >
                  <Heading
                    title={item.Title}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <GroupPromotionArticles
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "CateBlog" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-catblog">
                  <Heading
                    title={item.Title}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <CateBlog
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "ProjectBlogs" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-blog">
                  <Heading
                    title={item.Title}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <ProjectBlogs
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "ProjectArticles" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-article">
                  <Heading
                    title={item.Title}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <ProjectArticles
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "HtmlTitle" && (
              <SectionSpace type={null} className="bg-white ">
                <div className="mb-5 text-center md:mb-8" id="section-html">
                  <Heading title={item.Title} color="pink" />
                </div>
                <HtmlTitle
                  template={item.Template}
                  data={item.Content}
                  isMobile={isMobile}
                  isReadMore={item.BtnMoreEnabled}
                />
              </SectionSpace>
            )}

            {item.TypeName == "Usp" && (
              <Usp
                data={item}
                isMobile={isMobile}
                type={getType(index)}
                config={item.Config}
              />
            )}

            {["PlaceBrand", "OtaMerchant"].includes(item.TypeName) && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-brand">
                  <Heading
                    title={item.Title || ""}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <PlaceBrand data={item.Data} isMobile={isMobile} />
              </SectionSpace>
            )}

            {item.TypeName == "ItemsContent" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-brand">
                  <Heading
                    title={item.Data.Title || ""}
                    subtitle={item.Data.Description}
                    color="pink"
                  />
                </div>
                <ItemsContent
                  data={item.Data}
                  template={item.Template}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "Securities" && (
              <SectionSpace type={2}>
                <InformationSecurity />
              </SectionSpace>
            )}

            {item.TypeName == "MenuSticky" && (
              <>
                <MenuSticky
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </>
            )}

            {item.TypeName == "ProjectDonations" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-article">
                  <Heading
                    title={item.Title}
                    subtitle={item.Short}
                    color="pink"
                  />
                </div>
                <ProjectDonations
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "ContentTabs" && (
              <SectionSpace type={getType(index)}>
                <div className="mb-5 text-center md:mb-8" id="section-article">
                  <Heading
                    title={item.Title || ""}
                    subtitle={item.Short || ""}
                    color="pink"
                  />
                </div>
                <ContentTab
                  template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}

            {item.TypeName == "GoogleForm" && (
              <SectionSpace type={getType(index)}>
                <div
                  className="mb-5 text-center md:mb-8"
                  id="section-googleform"
                >
                  <Heading
                    title={item.Data.Title || ""}
                    subtitle={item.Data.ShortTitle || ""}
                    color="pink"
                  />
                </div>
                <GoogleForm
                  // template={item.Template}
                  data={item.Data}
                  isMobile={isMobile}
                />
              </SectionSpace>
            )}
          </div>
        ))}
    </>
  );
}
