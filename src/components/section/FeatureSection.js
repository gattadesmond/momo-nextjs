import { useState, useEffect } from "react";

import Container from "@/components/Container";

import SectionSpace from "@/components/SectionSpace";

import Heading from "@/components/Heading";

import Image from "next/image";

import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

export default function FeatureSection({ data, isMobile }) {
  var hmm = `<div class="">
  <a class="" ${data.Ctas[0].QrCodeId ? "data-qrcode-id='2' " : ""}  > ${data.Ctas[0].Text}</a>
  `;
  return (
    <>
      <SectionSpace type={3}>
        <div className="grid items-center grid-cols-1 gap-6 sm:grid-cols-2">
          <div className="order-2 sm:order-1 sm:col-start-1 ">
            <div className="mb-5 text-left md:mb-8">
              {data.Title && (
                <h1 className={`text-2xl lg:text-3xl font-bold text-pink-600`}>
                  {data.Title}
                </h1>
              )}

              {data.Description && (
                <p className="text-lg">{data.Description}</p>
              )}
            </div>

            <div className="grid grid-cols-1 gap-y-3">
              {data.Items &&
                data.Items.map((item, index) => (
                  <div className="flex items-center flex-nowrap" key={index}>
                    <img src="https://static.mservice.io/img/momo-upload-api-210724114227-637627237476783614.png" />
                    <div className="pl-2 ">{item.Title}</div>
                  </div>
                ))}
            </div>

            <div className="mt-6 text-center md:text-left">
              <ArticleContentConvert data={hmm} />
            </div>
          </div>
          <div className="order-1 sm:order-2 sm:col-start-2 ">
            <div className="w-full overflow-hidden bg-gray-200 rounded shadow aspect-w-5 aspect-h-3">
              <Image
                src={data.Avatar}
                alt={data.Title}
                layout="fill"
                objectFit="cover"
                className=""
              />
            </div>
          </div>
        </div>
      </SectionSpace>
      <style jsx>{``}</style>
    </>
  );
}
