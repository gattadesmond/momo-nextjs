import { useState, useEffect } from "react";
import HowToUse from "@/components/HowToUse";

export default function HelpArticleGroup({ data, isMobile }) {

  if (data.Maps[0] == undefined) {
    return null;
  }

  return (
    <div>
      {data.Maps[0].GroupItems && (
        <HowToUse
          isMobile={isMobile}
          data={data.Maps[0].GroupItems}
          title={data.Maps[0].Title}
        />
      )}
    </div>
  );
}
