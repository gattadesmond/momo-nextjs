import { useState, useEffect, useLayoutEffect, useRef, memo } from "react";

// import AnchorLink from "react-anchor-link-smooth-scroll";

import Image from "next/image";
import parse from "html-react-parser";
import CountUp from "react-countup";

import ButtonAction from "@/components/ui/ButtonAction";

import dynamic from "next/dynamic";
const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

import lgVideo from "lightgallery/plugins/video";
import useAppContext from "@/lib/appContext";
import useIsOnscreen from "@/lib/useIsOnscreen";

function AboutH1({ data, template, isMobile, config }) {
  const [isGallery, setIsGallery] = useState(false);

  const [youtubeLink, setYoutubeLink] = useState(null);

  const { isViewApp } = useAppContext();

  const ref = useRef();
  const isOnscreen = useIsOnscreen(ref);
  const [isEnabledCount, setIsEnabledCount] = useState(false);

  useEffect(() => {
    if (data.YoutubeCode) {
      setYoutubeLink(`https://www.youtube.com/watch?v=${data.YoutubeCode}`);
    }

    const timer = setTimeout(function () {
      setIsGallery(true);
    }, 200);

    return function cleanup() {
      clearTimeout(timer);
      setIsGallery(false);
    };
  }, [data]);

  useEffect(() => {
    if (isOnscreen && !isEnabledCount) {
      setIsEnabledCount(true);
    }
  }, [isOnscreen]);

  let dataCount = [];
  if ([3, 4].includes(template)) {
    dataCount = data?.Items.map((el, index) => {
      // replace(/\'/g, '').split(/(\d+)/).filter(Boolean)
      let prefix = null;
      let count = null;
      let suffix = null;

      const splitString = el.Title.replace(/\'/g, "")
        .split(/(\d+)/)
        .filter(Boolean);

      if (splitString.length == 1) {
        if (isNaN(splitString[0])) {
          prefix = splitString[0];
        } else {
          count = splitString[0];
        }
      }

      if (splitString.length == 2) {
        if (isNaN(splitString[0])) {
          prefix = splitString[0];
          count = splitString[1];
        } else {
          suffix = splitString[1];
          count = splitString[0];
        }
      }

      if (splitString.length == 3) {
        prefix = splitString[0];
        count = splitString[1];
        suffix = splitString[2];
      }
      if (splitString.length > 3) {
        prefix = splitString.join();
      }

      // const blankIndex = el.Title.indexOf(' ');

      // const value = blankIndex >= 0 ? el.Title.substr(0, blankIndex) : el.Title;
      // const suffix = blankIndex >= 0 ? el.Title.substr(blankIndex + 1) : null;
      // const isNumber = +value === +value;

      return {
        ...el,
        Suffix: suffix,
        Count: count,
        Prefix: prefix,
      };
    });
  }

  return (
    <>
      <div className="grid grid-cols-1 items-center gap-6 sm:grid-cols-2">
        <div className="order-2 sm:order-1 sm:col-start-1 ">
          <div className="mb-5 text-left">
            {data.Title && (
              <>
                {config != null && config.TitleHtmlElement == 3 ? (
                  <h3
                    className={`mb-6 text-2xl font-bold text-pink-600 md:text-3xl lg:text-4xl`}
                  >
                    {parse(data.Title)}
                  </h3>
                ) : config != null && config.TitleHtmlElement == 2 ? (
                  <h2
                    className={`mb-6 text-2xl font-bold text-pink-600 md:text-3xl lg:text-4xl`}
                  >
                    {parse(data.Title)}
                  </h2>
                ) : (
                  <h1
                    className={`mb-6 text-2xl font-bold text-pink-600 md:text-3xl lg:text-4xl`}
                  >
                    {parse(data.Title)}
                  </h1>
                )}
              </>
            )}
            {data.Description && (
              <p className="text-lg font-light text-gray-600">
                {parse(data.Description)}
              </p>
            )}
          </div>

          {[3, 4].includes(template) && (
            <div className="flex space-x-3" ref={ref}>
              {dataCount &&
                dataCount.map((item, index) => (
                  <div key={index} className="sl-item max-w-[170px] md:pl-3">
                    <h4 className="pb-1 pl-3 text-lg font-bold text-gray-700 md:pl-0">
                      {item.Prefix && <span>{item.Prefix}</span>}
                      {item.Count &&
                        (isEnabledCount ? (
                          <CountUp
                            start={0}
                            end={item.Count}
                            duration={1.5}
                            separator="."
                          />
                        ) : (
                          0
                        ))}
                      {item.Suffix && <span>{item.Suffix}</span>}
                    </h4>
                    <p className="md:text-tiny text-xs text-gray-500">
                      {item.Short}
                    </p>
                  </div>
                ))}
            </div>
          )}
          {template !== 3 && template !== 4 && (
            <div className="grid grid-cols-1 gap-y-3 md:pt-3">
              {data.Items &&
                data.Items.map((item, index) => (
                  <div className="flex flex-nowrap items-center" key={index}>
                    <svg
                      width={24}
                      height={24}
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="block shrink-0"
                    >
                      <circle
                        opacity="0.1"
                        cx={12}
                        cy={12}
                        r={12}
                        fill="#A50064"
                      />
                      <path
                        d="M17.3332 8L9.99984 15.3333L6.6665 12"
                        stroke="#A50064"
                        strokeWidth={2}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>

                    <div className="note text-md pl-2 text-gray-600">
                      {parse(item.Title)}
                    </div>
                  </div>
                ))}
            </div>
          )}
          {(data.Short || data.Ctas) && (
            <div className="mt-7">
              {data.Short && (
                <div className="mb-4 text-gray-600">{parse(data.Short)}</div>
              )}
              {data.Ctas && (
                <div className="space-x-3 text-center md:text-left">
                  {data.Ctas.map((btn, index) => (
                    <ButtonAction
                      key={index}
                      cta={btn}
                      type={index % 2}
                      isViewApp={isViewApp}
                    />
                  ))}
                </div>
              )}
            </div>
          )}
        </div>

        {template === 4 && (
          <div className="order-3 sm:order-2 sm:col-start-2">
            <div className="aspect-w-16 aspect-h-9 relative w-full overflow-hidden">
              <video
                autoPlay={true}
                className="block"
                height="auto"
                width="100%"
                poster={data.Avatar}
                loop
                muted
                playsInline
              >
                <source src={data.VideoUrl} type="video/mp4" />
              </video>
            </div>
          </div>
        )}
        <div
          className={`order-1 sm:order-2 sm:col-start-2 ${
            template === 4 ? "hidden" : ""
          }`}
        >
          {template === 3 && (
            <div className="aspect-w-5 aspect-h-4 relative hidden md:block">
              <Image
                src={data.Avatar}
                alt={data.Title}
                loading="lazy"
                layout="fill"
                objectFit="contain"
              />
            </div>
          )}
          {template !== 3 && template !== 4 && (
            <div className="aspect-w-16 aspect-h-9 relative w-full overflow-hidden rounded bg-gray-200 shadow">
              {!youtubeLink && (
                <Image
                  src={data.Avatar}
                  alt={data.Title}
                  layout="fill"
                  objectFit="cover"
                  loading="lazy"
                />
              )}

              {isGallery && youtubeLink && !isMobile && (
                <>
                  <Image
                    src={data.Avatar}
                    alt={data.Title}
                    layout="fill"
                    objectFit="cover"
                    loading="lazy"
                  />

                  <LightGallery plugins={[lgVideo]} mode="lg-fade">
                    <a
                      data-lg-size="780-439"
                      className="gallery-item"
                      data-src={`${youtubeLink}`}
                      data-thumb={`${data.Avatar}`}
                      data-poster={`${data.Avatar}`}
                      data-sub-html={`<h4>${data.Title}</h4>`}
                    >
                      <div className="absolute top-1/2 left-1/2 z-10 h-12 w-12 -translate-x-1/2 -translate-y-1/2 cursor-pointer  rounded-full border-2 border-white bg-black bg-opacity-40 pt-3 pb-3 pl-4 pr-3 transition-transform hover:scale-110 md:h-16 md:w-16 md:pl-5 md:pr-4">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 25 31"
                          className="h-full w-full"
                          style={{ fill: "white" }}
                        >
                          <path d="M22.79 17.02L3.934 29.436a2.327 2.327 0 01-3.607-1.944V2.662A2.327 2.327 0 013.934.718L22.79 13.133a2.327 2.327 0 010 3.888v-.001z" />
                        </svg>
                      </div>
                    </a>
                  </LightGallery>
                </>
              )}

              {/* https://web.dev/iframe-lazy-loading/ */}
              {youtubeLink && isMobile && (
                <>
                  <iframe
                    allowFullScreen=""
                    className=""
                    src={`https://www.youtube.com/embed/${data.YoutubeCode}?controls=0`}
                    loading="lazy"
                  />
                </>
              )}
            </div>
          )}
        </div>
      </div>

      <style jsx>{`
        .sl-item {
          position: relative;
        }
        .sl-item:before {
          content: "";
          height: 18px;
          width: 2px;
          background-color: #f45197;
          position: absolute;
          left: 0;
          top: 4px;
        }
      `}</style>
    </>
  );
}
export default memo(AboutH1);
