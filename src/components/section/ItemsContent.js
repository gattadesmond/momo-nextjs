import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

const Item = ({ data }) => {
  const checkImgSrc = (url) => {
    const arrayUrl = url.split(".");
    const fileType = arrayUrl[arrayUrl.length - 1];
    if (["jpg", "png", "jpeg"].includes(fileType)) {
      return true;
    } else {
      return false;
    }
  };
  return (
    <div
      className={`relative rounded-md border border-gray-200 bg-white px-3  py-3  transition-shadow ${
        data.IsOpenUrl ? "hover:shadow-md" : ""
      }`}
    >
      <div className=" flex flex-nowrap items-center">
        <div className=" w-14 shrink-0">
          {data.Avatar &&
            (checkImgSrc(data.Avatar) ? (
              <Image
                className="max-w-full"
                alt={data.Title}
                src={data.Avatar}
                width={58}
                height={58}
                layout="responsive"
                loading="lazy"
              />
            ) : (
              <img
                className="max-w-full"
                alt={data.Title}
                src={data.Avatar}
                width={58}
                height={58}
                loading="lazy"
              />
            ))}
        </div>

        <div className=" flex-1 pl-4">
          <h4 className=" text-md font-bold leading-5 text-pink-500">
            {data.IsOpenUrl && data.Url ? (
              <Link href={data.Url} target={data.IsNewTab ? "_blank" : ""}>
                <a className="stretched-link">{data.Title}</a>
              </Link>
            ) : (
              data.Title
            )}
          </h4>

          <p className="mb-0 mt-0.5 text-xs text-gray-500">{data.Short}</p>
        </div>
      </div>
    </div>
  );
};

const Item2 = ({ data }) => {
  return (
    <figure className="mb-12 flex px-6 text-left">
      <div
        className="flex h-16 w-16 items-center justify-center rounded"
        style={{
          flex: "0 0 64px",
          boxShadow: "0 0 0 1px rgb(0 0 0 / 3%), 0 1px 6px 0 rgb(0 0 0 / 10%)",
        }}
      >
        <img
          className="max-w-full"
          alt={data.Title}
          src={data.Avatar}
          width={60}
          height={60}
          loading="lazy"
        />
      </div>
      <figcaption className="mt-0 flex-auto pl-4">
        <h4 className="text-gray-7000 mb-2 text-2xl font-bold">{data.Title}</h4>
        <p className="text-md my-1.5 text-gray-500">{data.Short}</p>
        {data.Url && (
          <Link href={data.Url}>
            <a className="text-momo text-md flex items-center font-bold hover:text-pink-400">
              Xem thêm
              <svg
                className="svg-icon ml-1"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M13 7l5 5m0 0l-5 5m5-5H6"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                ></path>
              </svg>
            </a>
          </Link>
        )}
      </figcaption>
    </figure>
  );
};

const RenderItem = ({ template, ...rest }) => {
  switch (template) {
    case 2:
      return <Item2 {...rest} />;
    default:
      return <Item {...rest} />;
  }
};

const ItemsContent = ({ data, template = 1, ...rest }) => {
  const [swiperObject, setSwiperObject] = useState(null);
  const [swiperLeft, setSwiperLeft] = useState(false);
  const [swiperRight, setSwiperRight] = useState(false);

  const clickRight = () => {
    swiperObject.slideNext();
  };
  const clickLeft = () => {
    swiperObject.slidePrev();
  };

  const slideChange = () => {
    swiperObject.isEnd ? setSwiperRight(false) : setSwiperRight(true);
    swiperObject.isBeginning ? setSwiperLeft(false) : setSwiperLeft(true);
  };

  useEffect(() => {
    if (swiperObject) {
      slideChange();
    }
  }, [swiperObject]);

  let items = [...data?.Items] || [];
  items = items
    .filter((a) => !!a.Title)
    .sort((a, b) => a.DisplayOrder - b.DisplayOrder);

  const countItemOfPage = 3;
  const page =
    parseInt(items.length / countItemOfPage) + (items.length % countItemOfPage);
  const arrayPage = [...Array(page)].fill(null);

  const getListCss = (template) => {
    switch (template) {
      case 2:
        return "grid md:grid-cols-2 gap-x-5 gap-y-5";
      default:
        return "grid md:grid-cols-2 lg:grid-cols-3 gap-x-5 gap-y-5";
    }
  };

  return (
    <>
      {items.length < 3 ? (
        <div className={"flex items-center justify-center space-x-3"}>
          {items.map((data, idx) => (
            <RenderItem key={idx} data={data} template={template} />
          ))}
        </div>
      ) : items.length <= 9 ? (
        <div className={getListCss(template)}>
          {items.map((data, idx) => (
            <RenderItem key={idx} data={data} template={template} />
          ))}
        </div>
      ) : (
        <div className="swp-brand relative">
          <Swiper
            slidesPerView="auto"
            spaceBetween={20}
            speed={700}
            threshold="10px"
            slidesPerGroup={1}
            onSwiper={setSwiperObject}
            onSlideChange={() => slideChange()}
            breakpoints={{
              // when window width is >= 728px
              768: {
                slidesPerView: 2,
                slidesPerGroup: 1,
                spaceBetween: 20,
              },
              // when window width is >= 1024px
              1024: {
                slidesPerView: 3,
                slidesPerGroup: 1,
                spaceBetween: 20,
              },
            }}
          >
            {arrayPage.map((v, idx) => (
              <SwiperSlide key={idx} className="grid grid-cols-1 gap-y-4">
                {items
                  .filter(
                    (a, i) =>
                      i >= idx * countItemOfPage &&
                      i < idx * countItemOfPage + countItemOfPage
                  )
                  .map((data, idx2) => (
                    <RenderItem key={idx2} data={data} template={template} />
                  ))}
              </SwiperSlide>
            ))}
          </Swiper>

          {swiperObject && swiperRight && (
            <div
              onClick={() => clickRight()}
              className="button-next button-swiper absolute -right-4 top-1/2 z-20 flex  h-8 w-8 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full border border-gray-200 bg-white text-black shadow-md transition-all hover:opacity-90"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M9 5l7 7-7 7"
                />
              </svg>
            </div>
          )}
          {swiperObject && swiperLeft && (
            <div
              onClick={() => clickLeft()}
              className="button-next button-swiper absolute -left-4 top-1/2 z-20 flex  h-8 w-8 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full border border-gray-200 bg-white text-black shadow-md transition-all hover:opacity-90"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M15 19l-7-7 7-7"
                />
              </svg>
            </div>
          )}

          <style jsx>{`
            .swp-brand :global(.swiper-button-prev),
            .swp-brand :global(.swiper-button-next) {
              top: 40%;
            }
          `}</style>
        </div>
      )}
    </>
  );
};

export default ItemsContent;
