import { useState, useEffect } from "react";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import parse from "html-react-parser";
import Image from "next/image";

import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper/core";
// install Swiper modules
SwiperCore.use([Navigation]);

export default function ContentTab({
  data,
  template,
  isMobile
}) {
  if (!data) return <></>;

  const [tabIndex, setTabIndex] = useState(0);
  const [tabs, setTabs] = useState(data.Tabs);
  const [swiperObject, setSwiperObject] = useState(null);

  return (
    <Tabs index={tabIndex} onChange={(index) => setTabIndex(index)}>
      <Swiper
        slidesPerView={"auto"}
        spaceBetween={10}
        threshold={8}
        centerInsufficientSlides={true}
        onSwiper={setSwiperObject}
        className="grid grid-nowrap grid-flow-col items-center z-20"
      >
        {tabs.map((item, index) => (
          <SwiperSlide
            key={"tabList-" + index}
            className={`grow shrink outline-none border-0 min-w-[160px] h-full pb-2.5 cursor-pointer
              ${index == tabIndex ? "is-active relative z-40" : ""}
            `}
            onClick={() => {
              if (isMobile) {
                if (tabIndex < index) {
                  swiperObject.slideTo(index);
                } else {
                  swiperObject.slideTo(index - 1);
                }
              } else {
                if (tabIndex < index) {
                  swiperObject.slideTo(index - 2);
                } else {
                  swiperObject.slideTo(index - 1);
                }
              }
              setTabIndex(index);
            }}
          >
            <span
              className={`flex items-center text-tiny space-x-2 text-left  px-2.5 py-1.5 rounded border border-gray-200 transition relative bg-white
                ${index == tabIndex ? 'translate-y-1.5 border-b-0 rounded-br-none rounded-bl-none z-30 pb-2.5 h-[120%]' : 'h-full shadow-md'}
              `}
            >
              <img
                alt={item.Title}
                src={item.Icon}
                width={30}
                height={30}
                loading="lazy"
              />
              <span className="text-truncate-row text-rows-2">{item.Title}</span>
            </span>
          </SwiperSlide>
        ))}
      </Swiper>

      <TabPanels
        className="bg-white border border-gray-200 shadow-md relative z-10 -mt-px"
      >
        {tabs.map((item, index) => (
          <TabPanel key={index} className="py-5 px-3">
            <div className="grid md:grid-cols-2 content-start">
              <div className="soju__prose block px-3 md:mt-0">
                {parse(item.Content)}
              </div>
              <div className="relative mt-4 md:mt-0 mx-3 aspect-w-16 aspect-h-9">
                <Image
                  src={item.Avatar}
                  alt={item.Title}
                  loading="lazy"
                  layout="fill"
                  objectFit="cover"
                />
              </div>
            </div>
          </TabPanel>
        ))}
      </TabPanels>
      <style jsx global>{`
      `}</style>
    </Tabs>
  );
}
