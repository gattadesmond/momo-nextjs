// import { useState, useEffect } from "react";

// import QRModal from "@/components/QRModal";

import ButtonAction from "@/components/ui/ButtonAction";

// import isMobile from "@/lib/isMobile";

export default function CtaFooter({
  content,
  cta,
  isMobile,
  isViewApp = false,
}) {
  return (
    <div className="flex items-start justify-center flex-1 md:flex-none">
      <ButtonAction
        className="mx-auto w-full h-full !inline-flex !justify-center !items-center"
        classNameRoot="h-full"
        content={content}
        cta={cta}
        isMobile={isMobile}
        isViewApp={isViewApp}
      />
    </div>
  );
}
