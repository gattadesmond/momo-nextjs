export default function GoogleForm({ data, isMobile }) {
  return (
    data && (
      <>
        <div className="mx-auto max-w-4xl">
          <div className="soju__prose small">
            <iframe
              height={data.Height}
              frameBorder="0"
              marginHeight="0"
              marginWidth="0"
              src={`https://docs.google.com/forms/d/e/${data.Code}/viewform?embedded=true`}
              width="100%"
            >
              Loading…
            </iframe>
          </div>
        </div>

        <style jsx>{``}</style>
      </>
    )
  );
}
