import { useState, useEffect, useRef } from "react";
import { generateId, random, range, sample, clamp } from "@/lib/utils";

import useRandomInterval from "@/lib/useRandomInterval";

import useIsOnscreen from "@/lib/useIsOnscreen";

const defaultGeneratePosition = (size) => {
  let style = {};
  style.left = random(0, 100) + "%";
  style.zIndex = sample([1, 3])[0];

  if (Math.random() > 0.5) {
    style.top = size * 0.5;
  } else {
    style.bottom = -size * 0.5;
  }

  return style;
};

const generateSparkle = (colors, minSize, maxSize, generatePosition) => {
  const size = random(minSize, maxSize);

  const sparkle = {
    id: generateId(),
    color: sample(colors)[0],
    size,
    numOfPoints: 4,
    createdAt: Date.now(),
    style: generatePosition(size),
  };

  return sparkle;
};

const Sparkles = ({
  rate = 250,
  variance = 200,
  minSize = 10,
  maxSize = 20,
  colors = ["#FFC700"],
  children,
  style = {},
  generatePosition = defaultGeneratePosition,
  delayBy = 0,
  ...delegated
}) => {
  const [sparkles, setSparkles] = useState(() => []);
  const [hasDelayElapsed, setHasDelayElapsed] = useState(delayBy === 0);

  const [isEnabled, setIsEnabled] = useState(true);

  const ref = useRef();
  const isOnscreen = useIsOnscreen(ref);

  useRandomInterval(
    () => {
      if (!isOnscreen || !isEnabled) {
        return;
      }

      const sparkle = generateSparkle(
        colors,
        minSize,
        maxSize,
        generatePosition
      );
      const now = Date.now();

      const nextSparkles = sparkles.filter((sp) => {
        const delta = now - sp.createdAt;
        return delta < 1000;
      });

      nextSparkles.push(sparkle);

      setSparkles(nextSparkles);
    },
    rate - variance,
    rate + variance
  );

  useEffect(() => {
    if (!delayBy) {
      return;
    }

    const timeoutId = window.setTimeout(
      () => setHasDelayElapsed(true),
      delayBy
    );

    return () => {
      window.clearTimeout(timeoutId);
    };
  }, []);

  return (
    <div
      className="inline-block relative"
      ref={ref}
      style={{
        ...style,
      }}
      {...delegated}
    >
      {sparkles.map((sparkle) => (
        <Sparkle
          key={sparkle.id}
          color={sparkle.color}
          size={sparkle.size}
          numOfPoints={sparkle.numOfPoints}
          style={sparkle.style}
        />
      ))}
      <strong className="relative z-2 font-bold">{children}</strong>
    </div>
  );
};

const Sparkle = ({ size, color, style, numOfPoints = 4 }) => {
  const path =
    numOfPoints === 4
      ? "M92 0C92 0 96 63.4731 108.263 75.7365C120.527 88 184 92 184 92C184 92 118.527 98 108.263 108.263C98 118.527 92 184 92 184C92 184 86.4731 119 75.7365 108.263C65 97.5269 0 92 0 92C0 92 63.9731 87.5 75.7365 75.7365C87.5 63.9731 92 0 92 0Z"
      : "M34 0C34 0 33.4886 20.0074 41.7749 26.3376C50.0612 32.6678 68 25.9737 68 25.9737C68 25.9737 49.7451 31.6449 46.58 41.8873C43.4149 52.1298 55.0132 68 55.0132 68C55.0132 68 44.2424 51.4976 34 51.4976C23.7576 51.4976 12.9868 68 12.9868 68C12.9868 68 24.5851 52.1298 21.42 41.8873C18.2549 31.6449 0 25.9737 0 25.9737C0 25.9737 17.9388 32.6678 26.2251 26.3376C34.5114 20.0074 34 0 34 0Z";

  return (
    <>
      <span className="absolute block cont" style={style}>
        <svg
          className="block star"
          width={size}
          height={size}
          viewBox="0 0 184 184"
          fill="none"
        >
          <path d={path} fill={color} />
        </svg>
      </span>
      <style jsx>{`
        @keyframes comeInOut {
          0% {
            transform: translate3d(-50%, -50%, 0) scale(0);
          }
          50% {
            transform: translate3d(-50%, -50%, 0) scale(1);
          }
          100% {
            transform: translate3d(-50%, -50%, 0) scale(0);
          }
        }

        @keyframes spin2 {
          from {
            transform: rotate(0deg);
          }
          to {
            transform: rotate(180deg);
          }
        }

        .cont {
          animation: comeInOut 900ms forwards;
        }

        .star {
          animation: spin2 1000ms linear;
        }
      `}</style>
    </>
  );
};


export default Sparkles;
