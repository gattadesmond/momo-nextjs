import Link from "next/link";
import { useRouter } from "next/router";

import { useState, useEffect, useRef } from "react";

import usePosition from "@/lib/usePosition";

//https://css-tricks.com/a-super-flexible-css-carousel-enhanced-with-javascript-navigation/#top-of-site

const ArrowLeft = () => (
  <div className="flex items-center justify-center text-gray-600 bg-white border border-gray-200 rounded-full shadow-sm cursor-pointer w-9 h-9 hover:opacity-90 ">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="w-5 h-5"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M15 19l-7-7 7-7"
      />
    </svg>
  </div>
);

// ArrowRight
const ArrowRight = () => (
  <div className="flex items-center justify-center text-gray-600 bg-white border border-gray-200 rounded-full shadow-sm cursor-pointer w-9 h-9 hover:opacity-90 ">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="w-5 h-5"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M9 5l7 7-7 7"
      />
    </svg>
  </div>
);

const SojuSlider = ({ children }) => {
  const ref = useRef();

  const { hasItemsOnLeft, hasItemsOnRight, scrollRight, scrollLeft } =
    usePosition(ref);

  return (
    <div className="relative soju-slider " ref={ref}>
      {children}
      {hasItemsOnRight && (
        <div
          onClick={scrollRight}
          className="absolute top-0 items-center hidden h-full -right-6 md:flex "
        >
          <ArrowRight />
        </div>
      )}

      {hasItemsOnLeft && (
        <div
          onClick={scrollLeft}
          className="absolute top-0 items-center hidden h-full -left-6 md:flex "
        >
          <ArrowLeft />
        </div>
      )}

      <style jsx global>{`
        
        .soju-slider > * {
          overflow-x: auto;
          scroll-snap-type: x mandatory;
          -ms-overflow-style: none;
          scrollbar-width: none;}

        `}</style>
    </div>
  );
};

export default SojuSlider;
