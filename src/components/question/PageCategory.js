import Link from "next/link";
import parse from "html-react-parser";

import Page from "@/components/page";
import Container from "@/components/Container";

import QuestionMenu from "./QuestionMenu";
import BlockDetailCategory from "./BlockDetailCategory";
import ArticleRating from "../blog/ArticleRating";
import { SvgStart } from "./QuestionIcons";
import QuestionSearch from "./QuestionSearch";

export default function PageCategory({
  data,
  dataCategories,
  pageMaster,
  isMobile
}) {
  const metaData = data.Data.Meta;
  const listQuestionArticleCategoryGroup = data.Data.ListQuestionArticleCategoryGroup;

  let pageTitle = data.Data.MainCategory.Name;
  let pageDescription = data.Data.MainCategory.Description;
  if (data.Data.SubCategory.Id > 0 && !!data.Data.SubCategory.Name) {
    pageTitle = data.Data.SubCategory.Name;
    pageDescription = data.Data.SubCategory.Description || "";
  }

  return (
    <Page
      className=""
      title={metaData?.Title}
      description={metaData?.Description}
      image={metaData?.Avatar}
      keywords={metaData?.Keywords}
      robots={metaData?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <QuestionMenu
        category={data.Data.MainCategory}
        title={data.Data.Title}
      />
      <Container>
        <div
          className="grid grid-cols-1 lg:grid-cols-12 -mx-5 md:mx-0 lg:border lg:border-gray-200 mb-8 mt-5"
        >
          <div
            className="lg:col-span-3 lg:border-r border-gray-200"
          >
            <BlockDetailCategory
              data={dataCategories.Data}
              cateIdActive={data.Data.MainCategory.Id}
              subIdActive={data.Data.SubCategory.Id}
            />
          </div>
          <div
            className="lg:col-span-9"
          >
            <div className="bg-gray-200 justify-between px-10 py-2 hidden lg:flex">
              <div></div>
              <QuestionSearch
                className="w-2/5 flex-row-reverse pl-4"
                PlaceHolder="Tìm theo câu hỏi, chủ đề"
              />
            </div>
            <div className="py-8 px-4 md:px-10 lg:px-10 mx-1">
              <div className="hidden lg:block">
                <h1
                  className={`text-xl lg:text-2xl font-bold`}
                >
                  {parse(pageTitle)}
                </h1>
                <p className="max-w-3xl mx-auto mt-1 text-md text-gray-500">
                  {parse(pageDescription)}
                </p>
                <hr className="my-2.5 bg-gray-200" />
              </div>
              <div className="mt-5">
                {listQuestionArticleCategoryGroup &&
                  listQuestionArticleCategoryGroup.map((group, gIndex) => (
                    <div key={'group' + group.Id} className="mb-5 lg:mb-10">
                      {data.Data.SubCategory.Id != group.Id &&
                        <h3 className="md:text-lg font-bold text-gray-600 mb-1 lg:mb-2">{group.Name}</h3>
                      }
                      <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-8">
                        {group.ListQuestionArticle &&
                          group.ListQuestionArticle.map((question, qIndex) => (
                            <div
                              key={'question' + question.Id}
                              className="border-b border-gray-100 pb-2 mb-2 lg:pb-2.5 lg:mb-2.5"
                            >
                              <Link href={question.Link}>
                                <a className="text-sm lg:text-md font-medium text-gray-600 text-opacity-80 hover:text-momo">
                                  {qIndex == 0 &&
                                    <SvgStart className="w-3 h-3 relative top-1 mr-2 float-left" />
                                  }
                                  <span>{question.Title}</span>
                                </a>
                              </Link>
                            </div>
                          ))
                        }
                      </div>
                    </div>
                  ))
                }
                {data && metaData && (
                  <ArticleRating
                    Id={data.Data.MainCategory.Id}
                    Type={3}
                    Count={metaData?.RatingCount}
                    Value={metaData?.RatingValue}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    </Page>
  )
}
