import Head from 'next/head';
import Script from 'next/script';

const LdJson_QA = (props) => {
  const { data } = props;
  var ldJson =
    `
      {
        "@type": "Question",
        "url": "https://momo.vn/hoi-dap/${data.UrlRewrite}",
        "name": "${data.Title}",
        "acceptedAnswer":
        {
            "@type": "Answer",
            "text": "${data.Content}"
        }
      }
    `;
  if (data.ListRelated && data.ListRelated.length > 0) {
    data.ListRelated.forEach(question => {
      ldJson +=
        (ldJson ? "," : "") +
        `
            {
              "@type": "Question",
              "url": "https://momo.vn${question.Link}",
              "name": "${question.Title}",
              "acceptedAnswer":
              {
                  "@type": "Answer",
                  "text": "${question.Content}"
              }
            }
          `;
    });
  }

  return ldJson ? (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `{
              "@context": "http://schema.org",
              "@type": "FAQPage",
              "mainEntity":
              [
              ${ldJson}
              ]
          }`,
        }}
      ></script>
    </Head>
  ) : (
    <></>
  );
};

export default LdJson_QA;