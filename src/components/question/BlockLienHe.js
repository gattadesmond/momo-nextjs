import { useState } from "react";
import Link from "next/link";

import Heading from "@/components/Heading";


export default function BlockLienHe() {

  const [guide, setGuide] = useState(null);
  const [loading, setLoading] = useState(true);
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
  };

  const openGuideModal = () => {
    setModal(true);
    setLoading(true);
  }

  return (
    <>
      <div
        className="flex flex-wrap md:flex-nowrap justify-around items-center mt-6 border border-gray-200 bg-gradient-to-r from-white to-blue-50 py-5 px-4 md:px-6 md:py-2"
      >
        <div
          className="flex-auto w-full lg:w-3/4"
        >
       
           <h2 className="font-bold  text-pink-600 md:text-xl">
           LIÊN HỆ VỚI VÍ MOMO
          </h2>

          <p className="my-4 text-gray-600">
            Bạn đang gặp khó khăn cần được hỗ trợ hay cần đóng góp ý kiến cho bộ phận khách hàng?
            Hãy liên hệ với Ví MoMo qua bộ phận chăm sóc khách hàng.
            Chúng tôi sẽ giải quyết vấn đề của bạn nhanh nhất có thể !
          </p>
          <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-4 text-gray-600">
            <div className="md:pr-4 md:border-r border-gray-200">
              Hướng dẫn trợ giúp trên
              <div className="mt-1">
                <Link href="/huong-dan/huong-dan-gui-yeu-cau-ho-tro-bang-tinh-nang-tro-giup">
                  <a
                    className="btn-primary-outline !rounded-full items-center hover:text-white hover:bg-pink-400 !font-normal !inline-flex"
                  >
                    <svg className="w-5 h-5 mr-1.5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" viewBox="0 0 345.1 512">
                      <g>
                        <path d="M279.4,23.7H30.8C14.5,23.7,0,38.2,0,56.3v401.8c0,16.3,14.5,30.8,30.8,30.8H76h23.8L76,449.4H34.5V96.2h243.1v152l34.5,22
                            V56.3C312,38.2,297.5,23.7,279.4,23.7z M226.8,77.1H86.1c-8.1,0-13.5-5.4-13.5-13.5c0-8.1,5.4-13.5,13.5-13.5h140.8
                            c5.4,0,10.8,5.4,10.8,13.5C237.7,71.7,232.3,77.1,226.8,77.1z"></path>
                        <path d="M189.4,200.7c-14.4,0-25.9,11.6-25.9,25.9v155.7l-17.3-34.6c-14.2-26.3-28.1-23.6-38.9-17.3c-12.5,8.3-17.2,17-8.6,38.9
                            c19.6,48.2,49.8,105.6,82.2,142.7h116.7c41-30.4,74-175,17.3-181.6c-5.2,0-13.5,0.8-17.3,4.3c0-17.3-15.1-21.7-21.6-21.6
                            c-7.5,0.1-13,4.3-17.3,13c0-17.3-14.1-21.6-21.6-21.6c-8.3,0-17.9,5.2-21.6,13v-90.8C215.4,212.3,203.8,200.7,189.4,200.7z"></path>
                      </g>
                    </svg>
                    <span>Ứng dụng Ví MoMo</span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="md:pr-4 md:border-r border-gray-200">
              <span>Hotline CSKH <strong>24/07</strong> (1000 đ/phút)</span>
              <div className="mt-1">
                <a href="tel:1900545441" className="btn-primary-outline !text-blue-600 !border-blue-600 !rounded-full hover:!text-white hover:bg-blue-600 !font-normal">
                  <span className="flex items-center">
                    <svg className="w-4 h-4 mr-1.5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" viewBox="0 0 512 512">
                      <path d="M0,48c0,256.5,207.9,464,464,464c11.3,0,20.9-7.8,23.4-18.6l24-104c2.6-11.3-3.3-22.9-14-27.6l-112-48
                          c-9.8-4.2-21.2-1.4-28,6.9l-49.6,60.6c-78.3-36.7-141.2-100.5-177.2-177.2l60.6-49.6c8.3-6.7,11.1-18.2,6.9-28l-48-112
                          C145.5,3.9,133.9-2,122.6,0.6l-104,24C7.7,27.1,0,36.8,0,48z"></path>
                    </svg>
                    <span>GỌI NGAY</span>
                  </span>
                </a>
              </div>
            </div>
            <div>
              <span>Email hỗ trợ</span>
              <div className="mt-1">
                <a href="mailto:%20hotro@momo.vn" className="text-blue-500">
                  hotro@momo.vn
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="flex-initial"
        >
          <img
            src="https://static.mservice.io/blogscontents/momo-upload-api-220215160839-637805381193475792.png"
          />
        </div>
      </div>

    </>
  )
}
