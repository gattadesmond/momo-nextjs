import Link from "next/link";

export default function BlockBotQuestion({
  data
}) {
  if (!data) return <></>;

  //https://static.mservice.io/img/momo-upload-api-220215115723-637805230439126190.png
  return (
    <div
      className="grid grid-cols-1 md:grid-cols-3 gap-4"
    >
      {data.ListQuestionArticle.map((question, index) => (
        <div
          key={'bot-question-' + index}
          className="relative  px-5 md:px-6 pt-4 pb-5 border border-gray-200 group"
        >
          <Link href={question.Link}>
            <a className="stretched-link">
            </a>
          </Link>
          <div
            className="mb-2.5 text-center"
          >
            <img
              className="inline-block"
              alt="Câu hỏi"
              src="https://static.mservice.io/img/momo-upload-api-220215115723-637805230439126190.png"
              width={36}
              height={39}
              loading="lazy"
            />
          </div>
          <div>
            <h4
              className="mb-4 text-base md:text-xl font-bold text-gray-700 group-hover:text-momo md:leading-tight text-center"
            >
              {question.Title}
            </h4>
            <p
              className=" opacity-70 soju__prose small text-justify"
            >
              {question.ShortContent}
            </p>
          </div>
        </div>
      ))}
    </div>
  )
}
