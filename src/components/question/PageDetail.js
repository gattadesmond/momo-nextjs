import Link from "next/link";
import parse from "html-react-parser";

import Page from "@/components/page";
import Container from "@/components/Container";
import ArticleRating from "@/components/blog/ArticleRating";
import ArticleContent from "@/components/article/ArticleContent";

import QuestionMenu from "./QuestionMenu";
import BlockDetailCategory from "./BlockDetailCategory";
import { SvgAngleUp } from "./QuestionIcons";
import QuestionSearch from "./QuestionSearch";
import BlockRatingHelpful from "./BlockRatingHelpful";
import HowToUse from "@/components/HowToUse";
import LdJson_QA from "./LdJson_QA";

export default function PageDetail({
  data,
  dataCategories,
  pageMaster,
  isMobile
}) {
  const metaData = data.Data.Meta;

  return (
    <Page
      className=""
      title={metaData?.Title}
      description={metaData?.Description}
      image={metaData?.Avatar}
      keywords={metaData?.Keywords}
      robots={metaData?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <LdJson_QA data={data.Data} />
      <QuestionMenu
        category={data.Data.MainCategory}
        title={data.Data.Title}
      />
      <Container>
      <div
          className="grid grid-cols-1 lg:grid-cols-12 -mx-5 md:mx-0 lg:border lg:border-gray-200 mb-8 mt-5"
        >
          <div
            className="lg:col-span-3 border-r border-gray-200"
          >
            <BlockDetailCategory
              data={dataCategories.Data}
              cateIdActive={data.Data.MainCategory.Id}
              subIdActive={data.Data.Value}
            />
          </div>
          <div
            className="lg:col-span-9"
          >
            <div className="bg-gray-200 justify-between px-10 py-2 hidden lg:flex">
              <Link href={data.Data.MainCategory.Url}>
                <a className="flex items-center text-md font-medium text-gray-500">
                  <SvgAngleUp className="w-2.5 h-2.5 mr-1 -rotate-90" />
                  Quay lại
                </a>
              </Link>
              <QuestionSearch
                className="w-2/5 flex-row-reverse pl-4"
                PlaceHolder="Tìm theo câu hỏi, chủ đề"
              />
            </div>
            <div className="py-8 px-4 md:px-10 lg:px-12 mx-1">
              <h1
                className={`text-xl lg:text-2xl font-bold`}
              >
                {parse(data.Data.Title)}
              </h1>
              <hr className="my-2.5 bg-gray-200" />
              <div className="soju__prose small leading-normal md:leading-relaxed">
                {/* {parse(decode(data.Data.Content))} */}
                <ArticleContent
                  data={data.Data.Content}
                  guide={data.Data.GuideData}
                  guideGroup={data.Data.GuideGroupData}
                  isMobile={isMobile}
                />
              </div>
              {data.Data.ListHelpArticle && data.Data.ListHelpArticle &&
                data.Data.ListHelpArticle.map((item, index) => (
                  <div key={'guide-' + item.Id}>
                    {item.Title &&
                      <div className="mb-5 text-center md:mb-8">
                        <h2 className="text-lg font-bold text-center text-momo">{item.Title}</h2>
                      </div>
                    }
                    <p>{isMobile}</p>
                    <HowToUse
                      isMobile={isMobile}
                      title={item.Title}
                      data={[{
                        Id: item.Id,
                        Title: item.Title,
                        Items: [item]
                      }]}
                      isMockup={data.Data.IsMockup}
                    />
                  </div>
                ))
              }
              {data && metaData && (
                <ArticleRating
                  Id={data.Data.Id}
                  Type={3}
                  Count={metaData?.RatingCount}
                  Value={metaData?.RatingValue}
                />
              )}
              {data &&
                <BlockRatingHelpful
                  Id={data.Data.Id}
                />
              }
              <div className="my-2.5 flex justify-end">
                <Link href={data.Data.MainCategory.Url}>
                  <a className="flex items-center text-md font-medium text-pink-500">
                    <SvgAngleUp className="w-2.5 h-2.5 mr-1 -rotate-90" />
                    Quay lại
                  </a>
                </Link>
              </div>
              {data && data.Data.ListRelated && data.Data.ListRelated.length > 0 &&
                <>
                  <h4 className="mb-2  font-bold text-gray-600">
                    Bài viết liên quan :
                  </h4>
                  <div className="grid text-sm">
                    {data.Data.ListRelated.map((item, index) => (
                      <Link key={'link' + item.Id} href={item.Link}>
                        <a className="py-1.5 text-blue-600 text-opacity-90 hover:text-blue-500">- {item.Title}</a>
                      </Link>
                    ))}
                  </div>
                </>
              }
            </div>
          </div>
        </div>
      </Container>
    </Page>
  )
}
