import { useState } from "react";
import { useRouter } from "next/router";

export default function QuestionSearch({
  className = '',
  PlaceHolder = 'Chào bạn, Ví MoMo có thể giúp gì được cho bạn?'
}) {
  const router = useRouter();

  const [query, setQuery] = useState(router.query.q || "");

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    router.push('/hoi-dap/tim-kiem?q=' + query);
  }

  const handleSearchChange = (e) => {
    setQuery(e.target.value);
  }

  const handleSearchKeyDown = (e) => {
    if (e.key === "Enter") {
      handleSearchSubmit(e);
    }
  }
  return (
    <form
      onSubmit={handleSearchSubmit}
      className={`bg-white rounded-full border border-white overflow-hidden flex items-center
        ${className ? className : ''}
      `}
    >
      <button
        type="submit"
        className="flex-initial px-5 py-1.5 h-full"
      >
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-5 h-5">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
        </svg>
      </button>
      <input
        type="text"
        className="flex-auto py-2 lg:py-1 text-sm text-gray-500 bg-transparent"
        placeholder={PlaceHolder}
        value={query}
        onChange={handleSearchChange}
        onKeyDown={handleSearchKeyDown}
      />
    </form>
  )
}
