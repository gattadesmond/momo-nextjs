import Head from 'next/head';

export default function LdJson_QA_Home() {
  return (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html:
          `{
          "@@context": "https://schema.org",
          "@@type": "FAQPage",
          "mainEntity": [{
          "@@type": "Question",
          "name": "MoMo là gì ?",
          "url": "https://momo.vn/hoi-dap/momo-la-gi",
          "acceptedAnswer": {
          "@@type": "Answer",
          "url": "https://momo.vn/hoi-dap/momo-la-gi",
          "text": "Ứng dụng Ví điện tử MoMo là sản phẩm của Công ty Cổ phần Dịch vụ Di động Trực tuyến (M_Service). Ứng dụng cho phép bạn tạo và nạp tiền vào tài khoản MoMo để thanh toán cho hơn 200 dịch vụ như nạp tiền điện thoại, thanh toán điện nước, thanh toán vay tiêu dùng, v.v..."
          }
          },{
          "@@type": "Question",
          "name": "Lợi ích khi liên kết MoMo với tài khoản ngân hàng",
          "url": "https://momo.vn/hoi-dap/loi-ich-khi-lien-ket-momo-voi-tai-khoan-ngan-hang",
          "acceptedAnswer": {
          "@@type": "Answer",
          "url": "https://momo.vn/hoi-dap/loi-ich-khi-lien-ket-momo-voi-tai-khoan-ngan-hang",
          "text": "Liên kết MoMo với tài khoản ngân hàng để:
          <br>
          <br> 1. Xác thực tài khoản MoMo và nâng hạn mức thanh toán hoặc rút tiền lên đến 20.000.000đ mỗi ngày.
          <br>
          <br> 2. Không cần phải nhập lại thông tin tài khoản ngân hàng mỗi lần giao dịch.
          <br>
          <br> 3. Nạp tiền vào MoMo từ tài khoản ngân hàng hoàn toàn miễn phí.
          <br>
          <br> 4. Rút tiền từ MoMo về tài khoản ngân hàng dễ dàng, ngay lập tức.
          <br>
          <br> 5. Nhận ngay quà tặng 100.000đ để trải nghiệm các dịch vụ trên MoMo"
          }
          },{
          "@@type": "Question",
          "name": "Sử dụng MoMo có an toàn?",
          "url": "https://momo.vn/hoi-dap/su-dung-momo-co-an-toan",
          "acceptedAnswer": {
          "@@type": "Answer",
          "url": "https://momo.vn/hoi-dap/su-dung-momo-co-an-toan",
          "text": "Bạn hoàn toàn yên tâm khi sử dụng MoMo, vì 4 lý do:
          <br>
          <br> 1. Bảo chứng bởi ngân hàng: Tiền trong tài khoản MoMo được bảo chứng bởi Vietcombank và ngân hàng liên kết với tài khoản của bạn. MoMo được Ngân hàng Nhà nước cấp giấy phép và quản lý.
          <br>
          <br> 2. Mạng lưới bảo mật đa tầng: Mỗi bước giao dịch của bạn đều được MoMo mã hóa và bảo vệ ngay tức thì. MoMo tự khóa ứng dụng nếu không được sử dụng trong vòng 5 phút và sử dụng công nghệ thông minh để tự động nhận diện và ngăn chặn giao dịch đáng ngờ.
          <br>
          <br>
          <a href='https://momo.vn/hoi-dap/su-dung-momo-co-an-toan'>Xem thêm</a>."
          }
          },{
          "@@type": "Question",
          "name": "Vì sao cần xác thực tài khoản?",
          "url": "https://momo.vn/hoi-dap/vi-sao-can-xac-thuc-tai-khoan",
          "acceptedAnswer": {
          "@@type": "Answer",
          "url": "https://momo.vn/hoi-dap/vi-sao-can-xac-thuc-tai-khoan",
          "text": "Đây là biện pháp được yêu cầu bởi pháp luật để bảo vệ tài khoản MoMo của bạn và tiền trong tài khoản.
          <br>
          <br>- Tài khoản chưa xác thực: là tài khoản chưa được xác thực thông tin chủ tài khoản (CMND), chỉ được thực hiện tối đa 15 giao dịch và giá trị giao dịch không quá 500.000đ/ngày.
          <br>
          <br>- Tài khoản đã xác thực: là tài khoản đã được xác thực thông tin chủ tài khoản (CMND), không hạn chế số lượng giao dịch và giá trị giao dịch lên tới 20.000.000đ/ngày.
          <br>
          <br>Để xác thực tài khoản, bạn vui lòng liên kết MoMo với tài khoản ngân hàng. Xem hướng dẫn cách liên kết tài khoản
          <a href='https://momo.vn/huong-dan/lien-ket-ngan-hang-ctgr67'>tại đây</a>"
          }
          }]
          }`,
        }}>
      </script>
    </Head>
  )
}
