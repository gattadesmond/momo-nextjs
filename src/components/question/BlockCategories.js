import Link from "next/link";

export default function BlockCategories({ data }) {
  if (!data) return <></>;


  return (
    <div className="flex flex-wrap -mx-5 md:mx-0 -mt-5">
      {data && data.map((cate, index) => (
        <div 
          key={"q-cate-" + index}
          className="w-1/2 lg:w-1/4 px-1.5 md:px-2.5 pt-4 md:pt-7 flex group"
        >
          <div
            className="flex flex-col justify-around relative bg-white text-center shadow-lg mt-7 md:mt-10 w-full px-4 pb-5"
          >
            <div
              className="flex-initial block relative -mt-7 md:-mt-10 mb-4"
            >
              <img 
                className="inline-block w-14 h-14 md:w-20 md:h-20"
                alt={cate.Name}
                src={cate.Avatar}
                width={89}
                height={89}
                loading="lazy"
              />
            </div>
            <div
              className="flex-auto mb-5 md:mb-6"
            >
              <h4
                className="text-base md:text-xl font-bold text-gray-700 mb-4"
              >
                {cate.Name}
              </h4>
              <p
                className="text-md text-gray-700"
              >
                {cate.Description}
              </p>
            </div>
            <div
              className="flex-initial"
            >
              <Link href={cate.Url}>
                <a className="btn-primary-outline !rounded-full hover:bg-pink-400 hover:text-white w-3/4">XEM NGAY</a>
              </Link>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}
