import { useState, useEffect } from "react";

import Link from "next/link";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import parse from "html-react-parser";
import { decode } from "html-entities";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";

import CssCarousel from "@/components/CssCarousel";
import SectionSpace from "../SectionSpace";

const BlockTabs = ({ data }) => {
  if (!data || data.length === 0) {
    return <></>;
  }

  const [tabIndex, setTabIndex] = useState(0);
  const [questionIndex, setQuestionIndex] = useState(0);

  const onChangeTab = (index) => {
    setTabIndex(index);
    setQuestionIndex(0);
  };

  return (
    <>
      <Tabs index={tabIndex} onChange={onChangeTab}>
        <CssCarousel className="relative">
          <TabList className="flex flex-nowrap items-center overflow-scroll pt-0 pb-3 md:justify-center md:space-x-3 md:pt-2 md:pb-2">
            {data.map((tab, index) => (
              <Tab
                key={"tab-name-" + index}
                className={`whitespace-nowrap border-b-2 py-1 px-2 text-sm font-medium text-gray-500 md:text-lg lg:px-3
                  ${
                    tabIndex === index
                      ? "border-pink-600 text-gray-800"
                      : "border-transparent"
                  }
                `}
              >
                {tab.Name}
              </Tab>
            ))}
            <div className="block w-2">&nbsp;</div>
          </TabList>
        </CssCarousel>
        <div className=" mx-auto mt-5 max-w-3xl">
          <TabPanels>
            {data.map((tab, index) => (
              <TabPanel key={"tab-panel" + index}>
                <Accordion
                  index={questionIndex}
                  onChange={(index) => setQuestionIndex(index)}
                  className={"grid grid-cols-1 gap-3 article-question"}
                >
                  {tab.ListQuestionArticle &&
                    tab.ListQuestionArticle.map((question, qindex) => (
                      <AccordionItem
                        key={"tab-" + index + "-question-" + qindex}
                        className={`border border-gray-200  leading-normal shadow-sm md:leading-relaxed
                          ${
                            questionIndex === qindex
                              ? "bg-gray-100/60"
                              : "bg-white"
                          }
                        `}
                      >
                        <AccordionButton
                          className={`relative block w-full py-3 pl-4 pr-6 cursor-pointer  text-left text-base font-semibold hover:text-opacity-70 focus:outline-none md:text-lg
                      
                        `}
                        >
                          <p className=" ">{decode(question.Title)}</p>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="icon absolute right-2 top-1/2 -mt-2 inline h-5 w-5 text-gray-500 transition-transform"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                          >
                            <path
                              fillRule="evenodd"
                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </AccordionButton>

                        <AccordionPanel className="soju__prose small pr-6  pl-4  ">
                          <div className="text-gray-600">
                            {parse(question.ShortContent || "")}
                          </div>
                          {question.Link && (
                            <Link href={question.Link}>
                              <a className="text-momo block py-2.5 text-xs md:text-sm">
                                &gt; Xem chi tiết
                              </a>
                            </Link>
                          )}
                        </AccordionPanel>
                      </AccordionItem>
                    ))}
                </Accordion>
              </TabPanel>
            ))}
          </TabPanels>
        </div>
      </Tabs>

      <style jsx>{`
        :global(.article-question
            [data-reach-accordion-item][data-state="open"]
            .icon) {
          transform: rotate(180deg);
        }
      `}</style>
    </>
  );
};

export default BlockTabs;
