import Link from "next/link";
import { useEffect, useState } from "react";
import { SvgAngleUp, SvgCart, SvgCloud, SvgDollarCircle, SvgGame, SvgHandShake, SvgMapPin, SvgMenuGrid, SvgMomoLock, SvgMomoMoneybox, SvgMoneyQuestion, SvgMoneyTransfer, SvgNote, SvgPlayButton } from './QuestionIcons';
import QuestionSearch from "./QuestionSearch";

const MenuSubItem = ({ urlParent, data, isActive = false }) => (
  <Link href={data.Url ?? ""}>
    <a className={`block text-tiny py-2  ml-10 pl-3 mr-6 pr-2 rounded-xl
      ${isActive ? 'bg-gray-100 text-momo' : 'text-gray-600 hover:text-momo'}
    `}>
      {data.Name}
    </a>
  </Link>
)

const MenuItem = ({ data, cateIdActive, subIdActive }) => {
  const [isExpand, setIsExpand] = useState(false);
  const [hasChild, setHasChild] = useState(data.ListSubs.length > 0);

  useEffect(() => {
    setIsExpand(data.Id == cateIdActive)
  }, [])

  const iconClassName = "w-full max-w-full";
  const renderIcon = (value) => {
    switch (value) {
      case "svg-cloud":
        return <SvgCloud className={iconClassName} />
      case "svg-note":
        return <SvgNote className={iconClassName} />
      case "svg-momo-moneybox":
        return <SvgMomoMoneybox className={iconClassName} />
      case "svg-cart":
        return <SvgCart className={iconClassName} />
      case "svg-money-transfer":
        return <SvgMoneyTransfer className={iconClassName} />
      case "svg-map-pin":
        return <SvgMapPin className={iconClassName} />
      case "svg-menu-grid":
        return <SvgMenuGrid className={iconClassName} />
      case "svg-play-button":
        return <SvgPlayButton className={iconClassName} />
      case "svg-money-question":
        return <SvgMoneyQuestion className={iconClassName} />
      case "svg-momo-lock":
        return <SvgMomoLock className={iconClassName} />
      case "svg-handshake":
        return <SvgHandShake className={iconClassName} />
      case "svg-game":
        return <SvgGame className={iconClassName} />
      case "svg-dollar-circle":
        return <SvgDollarCircle className={iconClassName} />

      default:
        return <></>;
    }
  };

  return (
    <div>
      <div className="relative px-4 py-2 text-sm font-medium text-gray-600 hover:text-momo flex items-center justify-start leading-tight">
        <span className="flex-initial w-[22px] h-[22px] text-gray-500 mr-4">
          {renderIcon(data.IconCss)}
        </span>
        {!hasChild ?
          <Link href={data.Url ?? ""}>
            <a className={`flex-auto ${isExpand ? 'text-pink-600' : ''}`}>{data.Name}</a>
          </Link>
          :
          <a 
            className={`flex-auto cursor-pointer ${isExpand ? 'text-pink-600' : ''}`}
            onClick={() => setIsExpand(value => !value)}
          >
            {data.Name}
          </a>
        }
        {hasChild &&
          <button
            type="button"
            className="p-2"
            onClick={() => setIsExpand(value => !value)}
          >
            <SvgAngleUp className={`flex-initial w-2 h-2 transition
                ${isExpand ? 'text-pink-500' : 'rotate-180'}
              `} />
          </button>
        }
      </div>
      <div className={`w-full overflow-hidden transition-all
        ${isExpand ? 'h-auto' : 'h-0'}
      `}>
        {hasChild &&
          data.ListSubs.map((item, index) => (
            <MenuSubItem
              key={"menu-sub-item-" + index}
              data={item}
              urlParent={data.Url}
              isActive={item.Id == subIdActive}
            />
          ))
        }
      </div>
    </div>
  )
}

export default function BlockDetailCategory({
  data,
  cateIdActive = null,
  subIdActive = null
}) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div
      className="relative rounded border boder-gray-200 lg:border-none mx-4 lg:mx-0"
    >
      <div className="flex lg:hidden items-center justify-between border-b border-gray-200 px-4 lg:px-0 py-2.5">
        <button
          type="button"
          onClick={() => setIsOpen(value => !value)}
        >
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false">
            <path stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeMiterlimit="10" d="M4 7h22M4 15h22M4 23h22"></path>
          </svg>
        </button>
        <QuestionSearch
          className="flex-auto bg-gray-200 bg-opacity-70 flex-row-reverse ml-6 md:ml-0 pl-4 w-3/4 md:w-2/5"
          PlaceHolder="Tìm theo câu hỏi, chủ đề"
        />
      </div>
      <div className={`overflow-hidden lg:h-auto transition-all duration-500
        ${isOpen ? 'h-auto' : 'h-0'}
      `}>
        <div className="py-2.5">
          <h2
            className="text-xs uppercase font-bold text-gray-500 px-4 py-2.5"
          >
            Câu hỏi theo chủ đề
          </h2>
          <div className="grid">
            {data && data.map((item, index) => (
              <MenuItem
                key={'menu-item-' + index}
                data={item}
                cateIdActive={cateIdActive}
                subIdActive={subIdActive}
              />
            ))}
          </div>
        </div>
        <div className="border-t border-gray-200 py-2.5 px-4">
          <h2 className="font-bold text-xs text-gray-500 py-2.5 mb-1.5 uppercase text-opacity-90">Liên hệ với Ví Momo</h2>
          <div className="flex flex-col space-y-4 text-md text-gray-600">
            <div className="">
              Hướng dẫn trợ giúp trên
              <div className="mt-1">
                <Link href="/huong-dan/huong-dan-gui-yeu-cau-ho-tro-bang-tinh-nang-tro-giup">
                  <a
                    className="btn-primary-outline !rounded-full !flex items-center hover:text-white hover:bg-pink-400 !font-normal"
                  // onClick={openGuideModal}
                  >
                    <svg className="w-5 h-5 mr-1.5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" viewBox="0 0 345.1 512">
                      <g>
                        <path d="M279.4,23.7H30.8C14.5,23.7,0,38.2,0,56.3v401.8c0,16.3,14.5,30.8,30.8,30.8H76h23.8L76,449.4H34.5V96.2h243.1v152l34.5,22
                          V56.3C312,38.2,297.5,23.7,279.4,23.7z M226.8,77.1H86.1c-8.1,0-13.5-5.4-13.5-13.5c0-8.1,5.4-13.5,13.5-13.5h140.8
                          c5.4,0,10.8,5.4,10.8,13.5C237.7,71.7,232.3,77.1,226.8,77.1z"></path>
                        <path d="M189.4,200.7c-14.4,0-25.9,11.6-25.9,25.9v155.7l-17.3-34.6c-14.2-26.3-28.1-23.6-38.9-17.3c-12.5,8.3-17.2,17-8.6,38.9
                          c19.6,48.2,49.8,105.6,82.2,142.7h116.7c41-30.4,74-175,17.3-181.6c-5.2,0-13.5,0.8-17.3,4.3c0-17.3-15.1-21.7-21.6-21.6
                          c-7.5,0.1-13,4.3-17.3,13c0-17.3-14.1-21.6-21.6-21.6c-8.3,0-17.9,5.2-21.6,13v-90.8C215.4,212.3,203.8,200.7,189.4,200.7z"></path>
                      </g>
                    </svg>
                    <span>Ứng dụng Ví MoMo</span>
                  </a>
                </Link>
              </div>
            </div>
            <div className="">
              <span>Hotline CSKH <strong>24/07</strong> (1000 đ/phút)</span>
              <div className="mt-1">
                <a href="tel:1900545441" className="btn-primary-outline !text-blue-600 !border-blue-600 !rounded-full hover:!text-white hover:bg-blue-600 !font-normal">
                  <span className="flex items-center">
                    <svg className="w-4 h-4 mr-1.5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" viewBox="0 0 512 512">
                      <path d="M0,48c0,256.5,207.9,464,464,464c11.3,0,20.9-7.8,23.4-18.6l24-104c2.6-11.3-3.3-22.9-14-27.6l-112-48
                          c-9.8-4.2-21.2-1.4-28,6.9l-49.6,60.6c-78.3-36.7-141.2-100.5-177.2-177.2l60.6-49.6c8.3-6.7,11.1-18.2,6.9-28l-48-112
                          C145.5,3.9,133.9-2,122.6,0.6l-104,24C7.7,27.1,0,36.8,0,48z"></path>
                    </svg>
                    <span>GỌI NGAY</span>
                  </span>
                </a>
              </div>
            </div>
            <div>
              <span>Email hỗ trợ</span>
              <div className="mt-1">
                <a href="mailto:%20hotro@momo.vn" className="text-blue-500">
                  hotro@momo.vn
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
