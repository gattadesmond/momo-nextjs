import { useState } from "react";

import { axios } from "@/lib/index";

export default function BlockRatingHelpful({
  Id
}) {
  const [isLoading, setIsLoading] = useState(false);
  const [isComplete, setIsComplete] = useState(false);

  const handleRating = (isGood) => {
    setIsLoading(true);
    axios
      .post('/question-article/rating-helpful', {
        data: {
          id: Id,
          isGood: isGood
        }
      })
      .then((res) => {
        if (!res || !res.Success) return;
        setIsComplete(true);
      })
  }

  return (
    <div className="mt-6 lg:mt-8 text-sm flex flex-nowrap items-center text-gray-700 rating">
      <div className="mr-2">Bài viết này có hữu ích với bạn? </div>
      {isComplete
        ?
        <div className="px-3 py-1 border border-yellow-300  text-yellow-800 bg-yellow-50 rounded inline-block">
          Cảm ơn bạn đã phản hồi!
        </div>
        :
        <div className="flex items-center justify-between space-x-6 ml-4">
          <button
            type="button"
            className="btn-primary-outline !border-2 !border-pink-400 !rounded-full hover:!text-white hover:!bg-gradient-to-b from-[#fd64a9] to-[#cb6a9f]"
            onClick={() => handleRating(true)}
            disabled={isLoading}
          >
            Có
          </button>
          <button
            type="button"
            className="btn-primary-outline !border-2 !border-blue-800 !text-blue-800 !rounded-full hover:!text-white hover:!bg-blue-800"
            onClick={() => handleRating(false)}
            disabled={isLoading}
          >
            Không
          </button>

        </div>
      }
    </div>
  )
}
