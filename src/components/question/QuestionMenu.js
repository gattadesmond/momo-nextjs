import { getRootSlug } from "@/lib/url";
import BreadcrumbContainer, { BreadcrumbItem } from "../BreadcrumbContainer";

export default function QuestionMenu({ category, title }) {
  return (
    <BreadcrumbContainer key={title}>
      <BreadcrumbItem url="https://momo.vn/" />
      <BreadcrumbItem title="Hỏi đáp" url="/hoi-dap" />
      {category &&
        <BreadcrumbItem title={category.Name} url={getRootSlug(category.Url)} isActive={!title} />
      }
      {title &&
        <BreadcrumbItem title={title} isActive={true} />
      }
    </BreadcrumbContainer>
  )
}
