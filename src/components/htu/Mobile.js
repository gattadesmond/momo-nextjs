import { useState, useMemo, useCallback, useEffect } from "react";

import SwiperCore, { Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

import parse from "html-react-parser";

SwiperCore.use([Pagination]);

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";

import Image from 'next/image';

const HowToUseItem = ({ data, single = false }) => {
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  const htuList = data.Items[0].Blocks;

  return (
    <>
      {single && (
        <>
          <div className="relative m-0 mockup">
            {htuList.slice(0, 3).map((item, index) => (
              <div
                className="block bg-gray-200 shadow-md mockup-item "
                key={item.Id}
              >
                {/* <img src={item.Image} className="block w-full" /> */}

                <Image
                  alt={`step-demo-${index}`}
                  src={item.Image ?? ''}
                  width={540}
                  height={960}
                  loading="lazy"
                  className="block w-full"
                />
              </div>
            ))}
          </div>
          <div>
            <div className="mt-6 text-center">
              <button
                className="inline-block px-4 py-2 text-sm font-bold text-center text-white transition-all bg-pink-700 rounded-md btn text-opacity-90 hover:bg-pink-800"
                onClick={() => setModal(true)}
              >
                Xem chi tiết
              </button>
            </div>
          </div>
        </>
      )}

      {!single && (
        <div
          className="flex flex-nowrap items-center htu-mb-item relative cursor-pointer"
          onClick={() => setModal(true)}
        >
          <div className=" w-20 rounded h-20 shrink-0 bg-gray-200 px-2 pt-2 overflow-hidden">
            <Image
              alt="step"
              src={data.Items[0].Blocks[0].Image}
              width={540}
              height={960}
              loading="lazy"
              className=" rounded shadow-sm w-full"
            />
          </div>
          <div className="pl-4 flex-1">
            <div className="font-bold">{data.Title}</div>
            <div className="text-gray-500 text-sm">
              {data.Items[0].Description}
            </div>
          </div>

          <div className="  shrink-0 text-gray-400 pl-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M9 5l7 7-7 7"
              />
            </svg>
          </div>
        </div>
      )}

      <Modal isOpen={modal} onDismiss={close} isFull={true}>
        <ModalHeader isBorder={true}> {data.Title} </ModalHeader>
        <ModalBody css="bg-white modal-htu">
          <Swiper
            spaceBetween={0}
            slidesPerView={1}
            pagination
          // pagination={{
          //   dynamicBullets: true,
          // }}
          >
            {htuList.map((item, index) => (
              <SwiperSlide key={item.Id}>
                <div className="flex items-center justify-center bg-gray-200 mockup-modal-bg">
                  <div className="mockup-modal max-w-full max-h-full ">
                    <div className=" w-full h-full relative">

                      <Image
                        alt={`step ${index + 1}`}
                        src={item.Image ?? ''}
                        layout="fill"
                        loading="lazy"
                        className="block "
                      />

                    </div>
                  </div>
                </div>
                <div className="px-6 pt-7 pb-14">
                  <div className="text-lg font-semibold">
                    {" "}
                    Bước {index + 1}: {item.Title}
                  </div>
                  {item.Content && (
                    <div className="mt-2  text-gray-600">
                      {parse(item.Content)}
                    </div>
                  )}
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </ModalBody>
      </Modal>

      <style jsx>
        {`

          .htu-mb-item {
            position: relative:
          }
          .htu-mb-item:not(:last-child):after{
            content: "";
            position: absolute;
            width: calc(100% - 95px);
            right: 0;
            bottom: -10px;
            height: 1px;
            background: var(--gray-300);
          }

          
          .mockup {
            height: 320px;
            width: 100%;
          }
          .mockup .mockup-item {
          }
          .mockup .mockup-item:nth-child(1) {
            width: 180px;
            height: 320px;
            position: relative;
            margin: 0 auto;
            z-index: 3;
          }

          .mockup .mockup-item:nth-child(2) {
            width: 120px;
            height: 213px;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate3d(-50%, -50%, 0);
            margin-left: -80px;
          }

          .mockup .mockup-item:nth-child(3) {
            width: 120px;
            height: 213px;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate3d(-50%, -50%, 0);
            margin-left: 80px;
          }
          .mockup-modal-bg {
            height: 380px;
          }

         .mockup-modal {
            box-shadow: inset 0 0 3px 0 rgba(0, 0, 0, 0.1),
              0 0 0 1px var(--gray-700), 0 0 10px 0px rgba(0, 0, 0, 0.4);
            background: linear-gradient(
              135deg,
              var(--gray-900) 0%,
              var(--gray-800) 50%,
              var(--gray-800) 69%,
              var(--gray-700) 100%
            );
              width: 216px;

           
            position: relative;
            margin: 0px auto;
            height: 100%;
            padding: 4px 10px;
          }


          .mockup-modal-border{
          

          }

          :global(.modal-htu .swiper-pagination-bullets) {
            position: fixed;
            bottom: 15px;
            padding: 0px 12px 1px;
            background: rgba(0, 0, 0, 0.4);
            border-radius: 20px;
            color: white;
            left: 50%;
            width: auto;
            transform: translateX(-50%);
          }
          :global(.modal-htu
              .swiper-pagination-bullets
              .swiper-pagination-bullet) {
            background: white;
          }
        `}
      </style>
    </>
  );
};

export default function HowToUseMobile({ data, title }) {
  //1 active Id to rule them all

  if (!data) {
    return null;
  }

  // const htuList = data[0].Items[0].Blocks;

  const htuList = data;

  const htuListLength = htuList.htuListLength;

  return (
    <div className="max-w-3xl mx-auto ">
      <div className="grid grid-cols-1 gap-y-4  htu-mb">
        {htuList &&
          htuList.length > 1 &&
          htuList.map((item, index) => (
            <HowToUseItem key={index} data={item} />
          ))}

        {htuList && htuList.length == 1 && (
          <HowToUseItem data={htuList[0]} single={true} />
        )}
      </div>
    </div>
  );
}
