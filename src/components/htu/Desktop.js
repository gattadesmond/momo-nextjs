import { useState, useMemo, useCallback, useEffect } from "react";

import SwiperCore, { Navigation, Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

import parse from "html-react-parser";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

import Image from "next/image";

SwiperCore.use([Navigation]);

const HowToUseItem = ({ data, isMockup }) => {
  const htuList = data.Blocks;
  const [activeId, setActiveId] = useState(0);
  const [swiperHtu, setSwiperHtu] = useState(null);

  const IsMockup =
    isMockup == null || isMockup == undefined
      ? data.Type == 2
        ? false
        : true
      : isMockup;
      
  const handleActiveId = (id) => {
    setActiveId(id);
    if (swiperHtu != null) {
      swiperHtu.slideTo(id);
    }
  };

  const handleSwiperHtuChange = () => {
    setActiveId(swiperHtu.activeIndex);
  };

  return (
    <>
      <div className="flex justify-center flex-nowrap max-w-3xl mx-auto ">
        <div className="flex-none w-72">
          <div className={`iphone ${IsMockup ? "mockup" : ""}`}>
            <div className="iphone-screen">
              <Swiper
                className="absolute inset-0 max-w-[235px]"
                spaceBetween={0}
                slidesPerView={1}
                slidesPerGroup={1}
                preloadImages={false}
                lazy={true}
                onSwiper={setSwiperHtu}
                onSlideChange={() => handleSwiperHtuChange()}
                navigation={{
                  nextEl: ".button-next",
                  prevEl: ".button-prev",
                }}
              >
                {htuList.map((item, index) => (
                  <SwiperSlide key={item.Id}>
                    <Image
                      src={item.Image ?? ""}
                      alt={`step${index}`}
                      layout="intrinsic"
                      width={234}
                      height={415}
                    />
                  </SwiperSlide>
                ))}

                <div className="absolute z-10 flex items-center justify-center w-8 h-8 text-white  -translate-y-1/2 bg-black rounded-full cursor-pointer button-next button-swiper bg-opacity-30 right-1 top-1/2 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M9 5l7 7-7 7"
                    />
                  </svg>
                </div>

                <div className="absolute z-10 flex items-center justify-center w-8 h-8 text-white  -translate-y-1/2 bg-black rounded-full cursor-pointer button-prev button-swiper bg-opacity-30 left-1 top-1/2 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M15 19l-7-7 7-7"
                    />
                  </svg>
                </div>
              </Swiper>
            </div>

            {/* <div className="iphone-momo">MoMo</div> */}
          </div>
        </div>
        <div className="flex-auto pt-4 pl-10 ">
          <div className=" htu-list">
            {htuList.map((item, index) => (
              <div
                className="relative pl-12 cursor-pointer htu-list-item"
                onClick={() => handleActiveId(index)}
                key={item.Id}
              >
                <div
                  className={`absolute top-0 left-0 w-8 h-8  transition-colors text-md font-semibold rounded-full  flex items-center justify-center ${
                    activeId == index
                      ? "bg-pink-600 text-white"
                      : "bg-gray-200 text-gray-700"
                  }`}
                >
                  {index + 1}
                </div>
                <div
                  className={`pt-1 transition-colors  text-md ${
                    activeId == index
                      ? "text-pink-600 font-semibold"
                      : "text-gray-700 font-medium"
                  }`}
                >
                  {item.Title}
                </div>
                {item.Content && (
                  <div
                    className={`text-sm text-gray-500 mt-2 text-ani-fade ${
                      activeId == index ? "" : "hidden"
                    }`}
                  >
                    {parse(item.Content)}
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>
      </div>

      <style jsx>
        {`
          .button-swiper.swiper-button-disabled {
            display: none;
          }
          .iphone {
            padding: 0;
            border-radius: 26px;

            height: 465px;
            width: 250px;

            position: relative;
            margin: 0px auto;
          }
          .mockup {
            box-shadow: inset 0 0 3px 0 rgba(0, 0, 0, 0.1),
              0 0 0 1px var(--gray-700), 0 0 10px 0px rgba(0, 0, 0, 0.4);

            background: linear-gradient(
              135deg,
              var(--gray-900) 0%,
              var(--gray-800) 50%,
              var(--gray-800) 69%,
              var(--gray-700) 100%
            );
          }
          .iphone-momo {
            color: rgba(255, 255, 255, 0.5);
            font-size: 12px;
            position: absolute;
            left: 50%;
            bottom: 5px;
          }

          .iphone-screen {
            background: black;
            position: absolute;
            border-radius: 6px;
            left: 8px;
            top: 25px;
            right: 8px;
            bottom: 25px;
            overflow: hidden;
          }

          .htu-list {
            position: relative;
            z-index: 1;
          }

          .htu-list-item {
            min-height: 80px;
          }

          .htu-list .htu-list-item:not(:last-child) {
            padding-bottom: 30px;
          }
          .htu-list .htu-list-item:not(:last-child):after {
            content: "";
            position: absolute;
            height: 100%;
            left: 15px;
            z-index: -1;
            width: 0px;
            top: 0;
            border-left: 1px dashed var(--gray-300);
          }
        `}
      </style>
    </>
  );
};

export default function HowToUse({ data, isMockup }) {
  if (!data) {
    return null;
  }
  const Cat = data.length > 1 ? data : null;
  const [cat, setCat] = useState(null);
  const [tabIndex, setTabIndex] = useState(0);

  useEffect(() => {
    if (data.length > 1) {
      setCat(data);
      setTabIndex(0);
    }
  }, [data]);

  return (
    <div className="max-w-4xl mx-auto ">
      {Cat && (
        <Tabs index={tabIndex} onChange={(index) => setTabIndex(index)}>
          <TabList className="flex items-center justify-center space-x-4  leading-snug flex-nowrap mb-4">
            {Cat.map((item, index) => (
              <Tab
                key={item.Id}
                className={`block px-2 py-1  font-semibold outline-none ring-0 border-b-2  ${
                  index == tabIndex
                    ? "text-gray-800 border-pink-600"
                    : "text-gray-500 border-transparent"
                }`}
              >
                {item.Title}
              </Tab>
            ))}
          </TabList>

          <TabPanels>
            {Cat.map((item, index) => (
              <TabPanel key={item.Id} className="pt-5">
                <HowToUseItem
                  key={item.Id}
                  data={item.Items[0]}
                  isMockup={isMockup}
                />
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      )}

      {!Cat && <HowToUseItem data={data[0].Items[0]} isMockup={isMockup} />}
    </div>
  );
}
