import Link from "next/link";

import classNames from "classnames";

// import DateFormat from "@/lib/date-format";
import ViewFormat from "@/lib/view-format";
import CatClassName from "@/lib/cat-class-name";

const ArticleInfo = (props) => {
  const {
    title,
    sapo,
    slug,
    catName,
    catLink,
    catId,
    timeRead,
    date,
    totalViews,
    totalViewsFormat,
    type = "normal",
    children,
    ...rest
  } = props;
  
  return (
    <div className=" max-w-2xl">
      <header {...rest}>
        {type == "popular" && (
          <>
            <div className="hidden md:block w-8 h-1 border-t border-gray-300"></div>
          </>
        )}

        {catLink &&
          <Link href={catLink}>
            <a className={`${CatClassName(catLink)}`}>
              <div className=" font-semibold text-tiny mb-1  inline-block">
                {catName}
              </div>
            </a>
          </Link>
        }

        <Link href={slug}  >
          <a>
            <h3
              className={classNames(
                "font-semibold text-gray-800 hover:underline leading-tight",
                {
                  "text-xl md:text-2xl lg:text-3xl": type == "big",
                  "text-xl md:text-lg lg:text-xl md:leading-tight lg:leading-tight": type == "normal",
                  "text-base md:text-lg md:leading-tight": type == "small",
                  "text-sm md:text-lg lg:text-sm": type == "popular",
                }
              )}
            >
              {title}
            </h3>
          </a>
        </Link>

        <div
          className={`flex flex-wrap items-center  mt-2 text-xs md:text-tiny  text-gray-600`}
        >
          {/* <div className="">
            {date}
          </div>
          {timeRead && <div className="text-base font-normal mx-1">·</div>} */}
          {/* {timeRead && <div>{timeRead} phút đọc</div>} */}
          {type != "popular" && totalViews >= 500 && (
            <>
              {/* <div className="text-base font-normal mx-1">·</div> */}
              <div>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-4 w-4 inline-block relative lg:hidden"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  style={{ top: "-1px" }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>{" "}
                {/* <ViewFormat data={totalView} /> */}
                {totalViewsFormat}
                <span className="hidden lg:inline">&nbsp;lượt xem</span>
              </div>
            </>
          )}
        </div>
      </header>

      {type != "popular" && (
        <div
          className={classNames("text-gray-600 mt-2 text-sm lg:text-sm", {
            "md:line-clamp-2 hidden": type == "small",
            "line-clamp-2": type == "normal",
            "line-clamp-2 lg:line-clamp-4": type == "big",
          })}
        >
          {sapo}
        </div>
      )}
    </div>
  );
};

export default ArticleInfo;
