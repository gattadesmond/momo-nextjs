import Link from "next/link";

import classNames from "classnames";

import DateFormat from "@/lib/date-format";

const ArticleInfo = (props) => {
  const {
    title,
    sapo,
    slug,
    catName,
    catLink,
    catId,
    timeRead,
    totalView,
    date,
    type = "normal",
    children,
    ...rest
  } = props;

  return (
    <div className="">
      <header {...rest}>
        {type == "popular" && (
          <>
            <div class="w-8 h-1 border-t border-gray-300"></div>
          </>
        )}

        <Link href={catLink}>
          <a>
            <div className=" uppercase text-tiny mb-1 transition-colors text-indigo-600 hover:text-indigo-500 inline-block">
              {catName}
            </div>
          </a>
        </Link>

        <Link href={slug}>
          <a>
            <h2
              className={classNames(
                "font-semibold text-gray-800 hover:underline leading-tight",
                {
                  "text-xl md:text-3xl": type == "normal",
                  "text-lg": type == "small",
                  "text-sm": type == "popular",
                }
              )}
            >
              {title}
            </h2>
          </a>
        </Link>

        <div className={`flex flex-wrap items-center  mt-2 text-tiny text-gray-500`}>
          <div>
            <DateFormat date={date} />
          </div>
          <div class="text-base font-normal mx-2">·</div>
          <div>{timeRead} phút đọc</div>
          {type != "popular" && (
            <>
              <div class="text-base font-normal mx-2">·</div>
              <div>{totalView} lượt xem</div>
            </>
          )}
        </div>

      </header>
      {type != "popular" && (
        <div
          className={classNames("text-gray-600 mt-3 text-sm md:text-base", {
            "line-clamp-2": type == "small",
            "line-clamp-4": type == "normal",
          })}
        >
          {sapo}
        </div>
      )}
    </div>
  );
};

export default ArticleInfo;
