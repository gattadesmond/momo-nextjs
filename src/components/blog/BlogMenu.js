import Link from "next/link";

import { useState, useEffect, useRef } from "react";

import classNames from "classnames";

import CssCarousel from "@/components/CssCarousel";

import CatClassName from "@/lib/cat-class-name";

const BlogMenu = ({ data, cat }) => {
  const ref = useRef();

  const [stick, setStick] = useState(false);

  useEffect(() => {
    const element = ref.current;
    const update = () => {
      const rect = element.getBoundingClientRect().y;

      if (rect <= 1) {
        setStick(true);
      } else {
        setStick(false);
      }
    };
    update();

    document.addEventListener("scroll", update, { passive: true });
    return () => {
      setStick(false);
      document.removeEventListener("scroll", update);
    };
  });

  return (
    <div
      className={`-mx-5 md:mx-0  sticky md:relative top-0 z-10 mb-8 ${
        stick ? "bg-white shadow-sm" : ""
      }`}
      ref={ref}
    >
      <CssCarousel>
        <div className="flex w-full pl-5 overflow-scroll text-sm border-b md:pl-0 md:text-base">
          
          <div
            className={classNames("mr-5   border-pink-600", {
              "is-active": !cat,
            })}
          >
            <Link href="/blog" >
              <a className="block px-1 py-3 text-gray-700 whitespace-nowrap hover:text-gray-900 md:px-2">
                Mới nhất
              </a>
            </Link>
          </div>

          {data.map((item, index) => (
            <div
              key={item.Id}
              className={` mr-5 ${CatClassName(item.Link)} ${
                item.Id === cat?.Id ? "is-active" : ""
              }`}
            >
              <Link href={item.Link}>
                <a
                  className={`py-3 px-1 md:px-2 block whitespace-nowrap text-gray-700 hover:text-gray-900`}
                >
                  {item.Name}
                </a>
              </Link>
            </div>
          ))}

          <div className="w-1 ">&nbsp;</div>
        </div>
      </CssCarousel>

      <style jsx>{`
        .is-active {
          position: relative;
          font-weight: 600;
        }
        .is-active:after {
          content: "";
          position: absolute;
          width: 100%;
          bottom: 0px;
          height: 0;
          border-bottom: 2px;
          border-style: solid;
          left: 0;
          border-color: inherit;
          border-top-left-radius: 3px;
          border-top-right-radius: 3px;
        }
      `}</style>
    </div>
  );
};

export default BlogMenu;

