import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

const SectionFeature = ({ data }) => {
  let itemHighLight, itemList;
  if (data.length > 3) {
    itemHighLight = data[0];
    itemList = data.slice(1);
  } else {
    itemHighLight = null;
    itemList = data;
  }
  return (
    <div className="">
      {!!itemHighLight && 
          <article className="mt-6 md:mt-8">
            <div className="grid grid-cols-1 md:grid-cols-2 gap-2 pb-4 md:gap-8 ">
              <ArticleThumbnail
                avatar={itemHighLight.Avatar}
                title={itemHighLight.Title}
                slug={itemHighLight.Link}
              />
              <ArticleInfo
                title={itemHighLight.Title}
                slug={itemHighLight.Link}
                sapo={itemHighLight.ShortContent}
                catName={itemHighLight.CategoryName}
                catLink={itemHighLight.CategoryLink}
                catId={itemHighLight.CategoryId}
                timeRead={itemHighLight.TimeReadAvg}
                totalViews={itemHighLight.TotalViews}
                totalViewsFormat={itemHighLight.TotalViewsFormat}
                date={itemHighLight.PublicDate}
                type="big"
              />
            </div>
          </article>
        }

      <div className="grid grid-cols-1 md:grid-cols-3 gap-10 md:gap-6 mt-6 md:mt-3">
        {itemList &&
          itemList.map((item) => (
            <article className="" key={item.Id}>
              <ArticleThumbnail
                avatar={item.Avatar}
                title={item.Title}
                slug={item.Link}
              />
              <div className="mt-2">
                <ArticleInfo
                  title={item.Title}
                  slug={item.Link}
                  sapo={item.ShortContent}
                  catName={item.CategoryName}
                  catLink={item.CategoryLink}
                  catId={item.CategoryId}
                  timeRead={item.TimeReadAvg}
                  totalViews={item.TotalViews}
                  totalViewsFormat={item.TotalViewsFormat}
                  date={item.PublicDate}
                  type="normal"
                />
              </div>
            </article>
          ))}
      </div>
    </div>
  );
};

export default SectionFeature;
