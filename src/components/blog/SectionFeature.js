import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

const SectionFeature = ({ data }) => {
  console.log(data);
  return (
    <div className="">
      {data &&
        data.slice(0, 1).map((item, index) => (
          <article className="mt-10" key={item.Id}>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-8 ">
              <ArticleThumbnail
                avatar={item.Avatar}
                title={item.Title}
                slug={item.Link}
              />
              <ArticleInfo
                title={item.Title}
                slug={item.Link}
                sapo={item.ShortContent}
                catName={item.CategoryName}
                catLink={item.CategoryLink}
                catId={item.CategoryId}
                timeRead={item.TimeReadAvg}
                totalView={item.TotalView}
                date={item.Date}
              />
              {/* <ArticleInfo {...item} /> */}
            </div>
          </article>
        ))}

      <div className="grid grid-cols-1 md:grid-cols-3 gap-6 mt-8">
        {data &&
          data.slice(1).map((item, index) => (
            <article className="">
              <ArticleThumbnail
                avatar={item.Avatar}
                title={item.Title}
                slug={item.Link}
              />
              <div className="mt-3">
                <ArticleInfo
                  title={item.Title}
                  slug={item.Link}
                  sapo={item.ShortContent}
                  catName={item.CategoryName}
                  catLink={item.CategoryLink}
                  catId={item.CategoryId}
                  timeRead={item.TimeReadAvg}
                  totalView={item.TotalView}
                  date={item.Date}
                  type="small"
                />
              </div>
            </article>
          ))}
      </div>
    </div>
  );
};

export default SectionFeature;
