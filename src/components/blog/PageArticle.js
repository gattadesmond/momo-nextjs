import { useRouter } from "next/router";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();
import { useState, useEffect } from "react";

import Container from "@/components/Container";
import Page from "@/components/page";

import ArticleContent from "@/components/blog/ArticleContent";

import ArticleTOC from "@/components/blog/ArticleTOC";

import ArticleRating from "@/components/blog/ArticleRating";
import ArticleRelatingConfig from "@/components/blog/ArticleRelatingConfig";

import Breadcrumb from "@/components/Breadcrumb";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import LdJson_BreadCrumbs from "@/components/LdJson_BreadCrumbs";
import LdJson_QAContent from "@/components/LdJson_QAContent";
// import ArticleShare from "@/components/blog/ArticleShare";

import BlogList from "@/components/blog/BlogList";
import ButtonShare from "@/components/ui/ButtonShare";

import Link from "next/link";
import Image from "next/image";

import ViewFormat from "@/lib/view-format";
import CatClassName from "@/lib/cat-class-name";
import FooterCta from "@/components/FooterCta";

import ReplaceURL from "@/lib/replace-url";

function PageArticle({ dataPostDetail, pageMaster, isMobile }) {
  const data = dataPostDetail.Data;

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);

  useEffect(() => {
    var _isViewApp =
      data.ViewInApp && router?.query?.view == "app" ? true : false;
    if (_isViewApp) setIsViewApp(_isViewApp);
  }, [router]);

  useEffect(() => {
    try {
      window.ReactNativeWebView.postMessage("GetUserInfo");
      if (data.ViewInApp) setIsViewApp(true);
    } catch {}

    // ReplaceURL(data.Link);

    document.querySelector("body").classList.add("page-blog");
    return function clean() {
      document.querySelector("body").classList.remove("page-blog");
    };
  }, []);

  return (
    <>
      <Page
        className="font-inter"
        title={data.Meta?.Title}
        description={data.Meta?.Description}
        image={data.Meta?.Avatar}
        keywords={data.Meta?.Keywords}
        robots={data.Meta?.Robots}
        header={pageMaster.Data?.MenuHeaders}
        isMobile={isMobile}
        isViewApp={isViewApp}
        link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${data.Link}`}
        Trace={{ Id: data.Id, Type: "blog" }}
        GTM={{
          Disabled: false,
          SwapIdCase: isViewApp ? 2 : 1,
        }}
      >
        <LdJson_Meta_NewsArticle Meta={data.Meta} />
        <LdJson_QAContent QaData={data.QaData} QaGroupData={data.QaGroupData} />

        {isViewApp && (
          <div className="pointer-events-none select-none">
            <Image
              src={data.Avatar}
              alt={data.Title}
              layout="responsive"
              width={1920}
              height={600}
            />
          </div>
        )}

        <Container>
          {data.BreadCrumbs && (
            <>
              <div className="mt-10 breadcrumb ">
                <Breadcrumb data={data.BreadCrumbs} />
              </div>

              <LdJson_BreadCrumbs BreadCrumbs={data.BreadCrumbs} />
            </>
          )}

          {!isViewApp && (
            <div className="overflow-hidden rounded pointer-events-none select-none md:rounded-lg mt-6 md:mt-10">
              <Image
                src={data.Avatar}
                alt={data.Title}
                layout="responsive"
                width={1920}
                height={600}
              />
            </div>
          )}

          <article className=" pb-10 md:pb-12  lg:px-10">
            <div className="lg:grid justify-center grid-cols-1 mt-4 lg:grid-cols-none lg:grid-flow-col lg:gap-6 lg:mt-5">
              {data.Type == 1 && (
                <div className="relative z-10 w-full lg:w-60 lg:col-start-2 lg:pl-4 ">
                  <ArticleTOC data={data.MenuItems} />
                </div>
              )}

              <div className="w-full max-w-3xl mx-auto lg:col-start-1">
                <div className="flex flex-row items-center mb-2">
                  <div className="flex-auto">
                    {isViewApp ? (
                      <span>
                        <div
                          className={` text-tiny font-semibold mb-0 transition-colors  inline-block ${CatClassName(
                            data.Category.Link
                          )}`}
                        >
                          {data.Category.Name}
                        </div>
                      </span>
                    ) : (
                      <Link href={data.Category.Link}>
                        <a>
                          <div
                            className={` text-tiny font-semibold mb-0 transition-colors  inline-block ${CatClassName(
                              data.Category.Link
                            )}`}
                          >
                            {data.Category.Name}
                          </div>
                        </a>
                      </Link>
                    )}

                    <div className="mt-0 text-gray-500 text-tiny first-letter:uppercase">
                      <div
                        className={`flex flex-wrap items-center text-tiny text-gray-500`}
                      >
                        {/* <div> {data.PublishDate}</div> */}
                        {data.TimeReadAvg && (
                          <>
                            {/* <div className="mx-1 text-base font-normal ">·</div> */}
                            <div>{data.TimeReadAvg} phút đọc</div>
                          </>
                        )}
                        {data.TotalViews >= 500 && (
                          <>
                            <div className="mx-1 text-base font-normal ">·</div>
                            <div>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-4 w-4 inline-block relative"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                style={{ top: "-1px" }}
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth={2}
                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                />
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth={2}
                                  d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                                />
                              </svg>{" "}
                              <ViewFormat data={data.TotalViews} />{" "}
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="flex-0 shrink-0  ">
                    <ButtonShare
                      link={`${publicRuntimeConfig.REACT_APP_FRONT_END}${data.Link}`}
                      isViewApp={isViewApp}
                    />
                  </div>
                </div>

                <h1 className="mb-3 text-2xl font-bold tracking-tight text-black font-inter md:text-4xl  md:leading-tight">
                  {data.Title}
                </h1>
                <p className="italic leading-normal text-gray-600  lg:text-lg ">
                  {data.Short}
                </p>
                <hr className="post__sapo__hr" />
                <style jsx>{`
                  .post__sapo__hr {
                    font-style: normal;
                    display: block;
                    margin: 1.5em 0;
                    border: 0;
                    text-align: center;
                    height: auto;
                    color: var(--pinkmomo);
                    font-weight: 300;
                    font-family: system-ui, -apple-system, BlinkMacSystemFont,
                      "Helvetica Neue", Helvetica, Arial, sans-serif;
                  }
                `}</style>

                {/* <div
                  className="w-full max-w-full leading-normal prose soju__prose lg:prose-lg lg:leading-relaxed"
                  dangerouslySetInnerHTML={{ __html: data.Content }}
                ></div> */}
                <div className="mx-auto leading-normal tracking-tight md:text-lg soju__prose lg:mx-0 lg:w-full lg:max-w-full md:leading-relaxed mb-4">
                  <ArticleRelatingConfig
                    Data={data.RelatingConfig.ProjectTop}
                    isViewApp={isViewApp}
                  />
                </div>
                <ArticleContent
                  data={data.Content}
                  ads={data.AdsData}
                  cta={data.CtaData}
                  qa={data.QaData}
                  qaGroup={data.QaGroupData}
                  isViewApp={isViewApp}
                />

                <div className="mx-auto leading-normal tracking-tight md:text-lg soju__prose lg:mx-0 lg:w-full lg:max-w-full md:leading-relaxed">
                  <ArticleRelatingConfig
                    Data={data.RelatingConfig.ProjectBottom}
                    isViewApp={isViewApp}
                  />
                </div>
                {data && data.Meta && (
                  <ArticleRating
                    Id={data.Id}
                    Count={data.Meta.RatingCount}
                    Value={data.Meta.RatingValue}
                  />
                )}
              </div>
            </div>
          </article>

          <div className="mt-4 mb-10 lg:px-10 blogs-relative">
            <h2 className="mb-0 text-xl font-semibold text-gray-800">
              Bài viết liên quan
            </h2>

            <div className="grid grid-cols-1 divide-y divide-gray-300 ">
              <BlogList data={data.RelatingBlogs} cat={null} isMore={false} />

              <div className="py-8 text-center">
                <Link href={data.Category.Link}>
                  <a className="py-1 pl-4 pr-6 font-semibold text-pink-700 transition-all border border-pink-600 rounded-full focus:outline-none hover:outline-none hover:text-pink-800 hover:bg-pink-50 ">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="inline-block w-4 h-4 mr-2 animate-bounce opacity-80"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M16.707 10.293a1 1 0 010 1.414l-6 6a1 1 0 01-1.414 0l-6-6a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l4.293-4.293a1 1 0 011.414 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                    Xem thêm
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </Container>

        {data.ViewInApp && data?.ViewInAppConfig?.Ctas?.length > 0 && (
          <FooterCta
            data={data.ViewInAppConfig.Ctas.map((x) => {
              return {
                TypeName: "CtaFooter",
                Template: 1,
                Cta: {
                  Link: x.Link,
                  Text: x.Text,
                  NewTab: x.NewTab,
                  RedirectUC: x.RedirectUC,
                  QrCodeId: x.QrCodeId,
                },
                Content: "",
              };
            })}
            isViewApp={isViewApp}
            isMobile={isMobile}
          />
        )}
      </Page>
    </>
  );
}

export default PageArticle;
