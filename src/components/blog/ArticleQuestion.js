import {decode} from 'html-entities';

const ArticleQuestion = ({ data, questId }) => {
  const List = data.filter((item) => item.Id == questId)[0];
  return (
    <div className="mt-4 md:mt-8">
      <div className="bg-yellow-50  border border-yellow-200 bg-opacity-50 px-4 py-3 md:px-6 md:py-4 rounded relative">
        <div className="h-8 w-8 flex items-center justify-center absolute -top-3 -left-4 bg-white border-2 border-yellow-200 rounded-full">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-7 w-7 text-yellow-500 block "
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
              clipRule="evenodd"
            />
          </svg>
        </div>

        <div className="text-lg relative font-semibold text-gray-800 ">
          {List.Data.Title}
        </div>
        <div
          className=" text-base md:text-md opacity-80 soju__prose mt-4"
          dangerouslySetInnerHTML={{ __html:decode(List.Data.Content) }}
        ></div>
        <style jsx>{``}</style>
      </div>
    </div>
  );
};

export default ArticleQuestion;
