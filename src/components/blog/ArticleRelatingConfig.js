import { useState, useEffect } from "react";

function ArticleRelatingConfig({ Data, isViewApp }) {
  return (
    Data &&
    Data.Enabled &&
    Data.Items.length > 0 && (
      <>
        <p>
          <strong>{Data.Title}</strong>
        </p>
        <ul>
          {Data.Items.map((x, i) => {
            var urlOut = x.Link;
            if (urlOut && urlOut.indexOf("http") == -1) {
              urlOut = `https://momo.vn${urlOut}`;
            }
            return (
              <li key={i}>
                {x.ViewInApp && isViewApp ? (
                  <a href={x.Link}>{x.Title}</a>
                ) : (
                  <a
                    target="_blank"
                    href={
                      isViewApp ? `momo://?refId=browser|${urlOut}` : x.Link
                    }
                  >
                    {x.Title}
                  </a>
                )}
              </li>
            );
          })}
        </ul>
      </>
    )
  );
}

export default ArticleRelatingConfig;
