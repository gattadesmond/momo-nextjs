import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

import ContentLoader from "react-content-loader";

import { useEffect, useState } from "react";

import { axios } from "@/lib/index";

const BlogList = ({ data, cat, isMore =true }) => {

  const initModel = {
    CateId: null,
    Items: [],
    TotalItems: 0,
    LastIdx: 0,
    Count: 10,
    ExcludeIds: null,
  };

  const [model, setModel] = useState(initModel);

  useEffect(() => {
    setModel({
      ...model,
      SortType: 1,
      CateId: cat ? cat.Id : null,
      Items: data.Items,
      TotalItems: data.TotalItems,
      LastIdx: data.LastIdx,
      Count: data.Count,
      ExcludeIds: data.ExcludeIds,
      IsLoading: false,
    });
    //setLastIdx(lastI);

    return function cleanup() {
      setModel(initModel);
    };
  }, [data, cat]);

  //Check get data
  // const [isFetching, setIsFetching] = useState(false);
  //Check has more
  // const [hasMore, setHasMore] = useState(true);

  // useEffect(() => {
  //   setHasMore(true);
  // }, [list]);

  const paramEmptyNull = (value) => {
    if (value && value != "null" && value != "undefined") {
      return value;
    }
    return "";
  };
  const paramArray = (array) => {
    if (array) return array.toString();

    return "";
  };

  const handelLoadMore = () => {
    setModel({
      ...model,
      IsLoading: true,
    });
    setTimeout(function () {
      axios
        .get(
          `/blog/list?cateId=${paramEmptyNull(
            model.CateId
          )}&excludeIds=${paramArray(model.ExcludeIds)}&sortType=${
            model.SortType
          }&count=${paramEmptyNull(model.Count)}&lastIdx=${paramEmptyNull(
            model.LastIdx
          )}`
        )
        .then((res) => {
          if (!res.Result || !res.Data) {
            setModel({
              ...model,
              IsLoading: false,
            });
            return;
          }
          var data = res.Data;
          setModel({
            ...model,
            Items: [...new Set([...model.Items, ...data.Items])],
            TotalItems: data.TotalItems,
            LastIdx: data.LastIdx,
            IsLoading: false,
          });

          // setIsFetching(false);

          // setItems((prevTitles) => {
          //   return [...new Set([...prevTitles, ...res.Data.Items])];
          // });
          // setLastIdx(res.Data.LastIdx);
          // setIsFetching(false);

          // //Check co load more ko
          // if (model.LastIdx < model.TotalItems) {
          //   setHasMore(false);
          // }
        })
        .catch((e) => {
          
        });
    }, 1000);
  };

  return (
    <>
      {model.Items && 
        model.Items.map((item, index) => (
          <article
            className={`py-3 ${index == 0 ? "md:pb-5" : "md:py-5"}`}
            key={item.Id}
          >
            <div className="flex flex-nowrap ">
              <div className="flex-1 md:order-2 md:pl-5 ">
                <ArticleInfo
                  type="small"
                  title={item.Title}
                  slug={item.Link}
                  sapo={item.ShortContent}
                  catName={item.CategoryName}
                  catLink={item.CategoryLink}
                  catId={item.CategoryId}
                  timeRead={item.TimeReadAvg}
                  date={item.PublicDate}
                  totalViews={item.TotalViews}
                  totalViewsFormat={item.TotalViewsFormat}
                />
              </div>

              <div className="flex-initial pt-3 pb-2 pl-3 w-28 md:pt-2 md:w-52 md:pl-0 md:order-1">
                <ArticleThumbnail
                  avatar={item.Avatar}
                  title={item.Title}
                  slug={item.Link}
                />
              </div>
            </div>
          </article>
        ))}

      {model.IsLoading && (
        <>
          <article className="hidden py-3 border-b md:py-5 md:block">
            <ContentLoader
              viewBox="0 0 400 150"
              width={400}
              height={150}
              title="Loading news..."
            >
              <rect x="0" y="0" rx="5" ry="5" width="208" height="143" />

              <rect x="230" y="2" rx="0" ry="0" width="80" height="12" />
              <rect x="230" y="30" rx="0" ry="0" width="220" height="20" />
            </ContentLoader>
          </article>

          <article className="py-3 border-b md:py-5 md:hidden">
            <ContentLoader
              viewBox="0 0 300 80"
              width={300}
              height={80}
              title="Loading news..."
            >
              <rect x="0" y="15" rx="0" ry="0" width="80" height="12" />
              <rect x="0" y="40" rx="0" ry="0" width="220" height="20" />
            </ContentLoader>
          </article>
        </>
      )}

      {model.LastIdx < model.TotalItems && isMore && (
        <div className="py-5 text-center md:py-10">
          <button
            type="button"
            disabled={model.IsLoading ? true : false}
            className="py-1 pl-4 pr-6 font-semibold text-pink-700 transition-all border border-pink-600 rounded-full hover:text-pink-800 hover:bg-pink-50"
            onClick={handelLoadMore}
          >
            {model.IsLoading ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-spin opacity-80"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-bounce opacity-80"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M16.707 10.293a1 1 0 010 1.414l-6 6a1 1 0 01-1.414 0l-6-6a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l4.293-4.293a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            )}
            Xem thêm
          </button>
        </div>
      )}
    </>
  );
};

export default BlogList;
