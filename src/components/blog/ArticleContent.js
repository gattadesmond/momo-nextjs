import Link from "next/link";
import { useState, useEffect } from "react";
import parse, { domToReact } from "html-react-parser";
import dynamic from "next/dynamic";

import { clientUrlUc } from "@/lib/momo";
import { qrCodeModelOpen, qrCodeModelClose } from "@/lib/momo/tracking";
import { random, sampleOne } from "@/lib/utils";

import isMobile from "@/lib/isMobile";

import QRModal from "@/components/QRModal";

import ArticleQuestion from "@/components/blog/ArticleQuestion";
import ArticleQuestionGroup from "@/components/blog/ArticleQuestionGroup";

import ImageCompare from "@/components/ui/ImageCompare";

import Sparkles from "@/components/Sparkles";

import { axios } from "@/lib/index";

const LightGallery = dynamic(() => import("lightgallery/react"), {
  ssr: false,
});

const getPerimeterPosition = (size) => {
  let style = {};
  style.zIndex = 3;

  const side = sampleOne(["top", "left", "right", "bottom"]);

  if (side === "top") {
    style.top = 0;
    style.left = `${random(0, 100)}%`;
  } else if (side === "left") {
    style.left = 0;
    style.top = `${random(0, 100)}%`;
  } else if (side === "right") {
    style.left = "100%";
    style.top = `${random(0, 100)}%`;
  } else {
    style.top = "100%";
    style.left = `${random(0, 100)}%`;
  }

  return style;
};

import lgThumbnail from "lightgallery/plugins/thumbnail";

const ModalQrCode = ({ qrId, text }) => {
  const [modal, setModal] = useState(false);

  const [qr, setQr] = useState(null);

  const isMobileDevice = isMobile();

  const handleClose = () => {
    setModal(false);
    qrCodeModelClose();
  };

  const handleOpen = () => {
    setModal(true);
    qrCodeModelOpen(qrId);
  };

  useEffect(() => {
    axios.get(`/qrCode/ListByIds?ids=${qrId}`).then(function (response) {
      const res = response.Data;
      if (res) {
        setQr(res[0]);
        setTimeout(() => {
          clientUrlUc();
        }, 1000);
      }
    });
  }, []);

  if (isMobileDevice) {
    return (
      <>
        {qr && (
          <a
            href={qr.QrLink}
            rel="noreferrer"
            target="_blank"
            className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md btn text-opacity-90 hover:bg-pink-800"
          >
            {text}
          </a>
        )}
      </>
    );
  }
  return (
    <>
      {qr && (
        <>
          <button onClick={() => handleOpen()} className="btn-primary">
            {text}
          </button>

          <QRModal
            isOpen={modal}
            onDismiss={handleClose}
            isDark={true}
            img={qr.QrImage}
            title={qr.Title}
            shortTitle={qr.ShortTitle}
          />
        </>
      )}
    </>
  );
};

const ArticleContent = (props) => {
  const {
    data,
    ads,
    cta,
    qa,
    qaGroup,
    children,
    isViewApp = false,
    ...rest
  } = props;

  const [isGallery, setIsGallery] = useState(false);

  useEffect(() => {
    const timer = setTimeout(function () {
      setIsGallery(true);
    }, 200);
    return function cleanup() {
      clearTimeout(timer);
      setIsGallery(false);
    };
  }, [data]);

  let imgList = [];
  let imgListIndex = 0;

  let lightGallery = null;

  const onInitGallery = (e) => {
    lightGallery = e.instance;
  };

  let qaSingle = [];
  let qaList = [];
  if (qa.length > 0) {
    qaSingle = qa;
  }
  if (qaGroup.length > 0) {
    qaList = qaGroup;
  }

  function openLightboxOnSlide(number) {
    const act = number - 1 < 0 ? 0 : number - 1;
    lightGallery.openGallery(act);
  }

  function isValid(string) {
    var re = /^[a-zA-Z0-9-]{0,}$/g; // regex here
    return re.test(string);
  }

  const options = {
    replace: ({ attribs, name, children }) => {
      if (name == "div") {
        if (attribs.class != undefined && attribs.class.includes("ckemm_ads")) {
          var adsId = attribs["data-ads-id"];
          if (!adsId) return;
          var data = ads.filter((x) => x.Id == adsId)[0];
          if (!data) return;

          var themeType = attribs["data-ads-type"];

          return (
            <div
              className={`ckemm_ads type${themeType}`}
              data-ads-id={adsId}
              data-ads-type={themeType}
            >
              <div className="relative -mx-5 post__adv md:mx-0">
                <div
                  className={` px-4 py-3 md:px-6 md:py-4 md:rounded my-6 flex items-center flex-nowrap ${
                    themeType == "1" ? "bg-pink-100" : "bg-gray-100"
                  }`}
                >
                  <div className="flex-1 pr-4 ">
                    <div className="mt-1 text-xs font-semibold opacity-80">
                      {data.Name}
                    </div>
                    <div className="mt-1 text-sm font-semibold text-gray-800 md:text-base lg:text-lg">
                      {parse(data.Content)}
                    </div>

                    <div className="mt-3">
                      <Sparkles
                        generatePosition={getPerimeterPosition}
                        minSize={20}
                        maxSize={30}
                        rate={600}
                      >
                        <a
                          href={data.BtnLink}
                          rel="noreferrer"
                          className="inline-block px-4 py-2 text-sm font-bold text-center text-white transition-all bg-pink-700 rounded-md btn text-opacity-90 hover:bg-pink-800"
                          target="_blank"
                        >
                          {data.BtnName}
                        </a>
                      </Sparkles>
                    </div>
                  </div>

                  <div className="flex-initial w-32 md:w-48 lg:w-52 ">
                    <img
                      src={data.Avatar}
                      className="w-full "
                      loading="lazy"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          );
        }
      }

      if (name == "question-article") {
        if (
          attribs["data-type"] == undefined ||
          attribs["data-questid"] == undefined
        )
          return;

        const questID = attribs["data-questid"];

        //group
        if (attribs["data-type"] == 1) {
          return <ArticleQuestionGroup data={qaGroup} questId={questID} />;
        }
        if (attribs["data-type"] == 2) {
          return <ArticleQuestion data={qa} questId={questID} />;
        }
      }

      if (name == "img") {
        let hmm = {
          src: attribs.src,
          subHtml: `${attribs.alt ? attribs.alt : ""}<div></div>`,
          thumb: attribs.src,
        };
        imgList.push(hmm);
        imgListIndex++;
        let gg = imgListIndex;
        return (
          <>
            <span
              className="block cursor-pointer "
              onClick={() => openLightboxOnSlide(gg)}
            >
              <img
                src={attribs.src}
                className="mx-auto mt-8 mb-2"
                alt={attribs.alt}
                loading="lazy"
              />
            </span>
          </>
        );
      }

      if (name == "compare-image") {
        if (!attribs["data-img1"] || !attribs["data-img2"]) return;

        const img1 = attribs["data-img1"];
        const img2 = attribs["data-img2"];

        const alt1 = attribs["data-alt1"] ?? "";
        const alt2 = attribs["data-alt2"] ?? "";

        const desc = attribs["data-desc"] ?? "";
        const height = attribs["data-height"] ?? 800;
        const width = attribs["data-width"] ?? 450;

        return (
          <div className={`my-3 md:my-4 `}>
            <ImageCompare
              img1={img1}
              img2={img2}
              alt1={alt1}
              alt2={alt2}
              width={width}
              height={height}
              desc={desc}
            />
          </div>
        );
      }

      if (name == "a") {
        if (isViewApp) {
          if (attribs.class && attribs.class.includes("btn-primary")) {
            return <></>;
          }
          if (attribs.class && attribs.class.includes("cke-inapp-link")) {
            var href = attribs["href"];
            attribs["href"] = "momo://?refId=browser|" + href;
          } else {
            attribs["data-href"] = attribs["href"];
            attribs["href"] = ""; //"javascript:void(0)";
            attribs.onClick = (e) => {
              e.preventDefault();
            };
            if (attribs["target"]) attribs["target"] = "";
          }
        } else if (attribs["data-qrcode-id"] != undefined) {
          var qrId = attribs["data-qrcode-id"];

          if (!qrId) return;

          return (
            <div className={`py-4 qrtype-${qrId}`} data-qr-id={qrId}>
              <ModalQrCode qrId={qrId} text={children[0]?.data} />
            </div>
          );
        }
      }

      if (name == "table") {
        return (
          <div className="table-responsive">
            <table className="table">{domToReact(children, options)}</table>
          </div>
        );
      }

      if (name && !isValid(name)) return <></>;

      if (!attribs) {
        return;
      }
    },
  };
  return (
    <>
      <div className="mx-auto leading-normal tracking-tight md:text-lg soju__prose lg:mx-0 lg:w-full lg:max-w-full md:leading-relaxed">
        {parse(data, options)}
      </div>
      {isGallery && (
        <LightGallery
          speed={500}
          onInit={onInitGallery}
          dynamic={true}
          dynamicEl={imgList}
          plugins={[lgThumbnail]}
        />
      )}
    </>
  );
};

export default ArticleContent;
