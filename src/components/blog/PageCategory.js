import Link from "next/link";

import { useEffect } from "react";

import StickyBox from "react-sticky-box";

import Page from "@/components/page";
import Container from "@/components/Container";
import SectionFeature from "@/components/blog/SectionFeature";
import BlogList from "@/components/blog/BlogList";
import PopularList from "@/components/blog/PopularList";

import BlogMenu from "@/components/blog/BlogMenu";

import ReplaceURL from "@/lib/replace-url";

function PageCategory({ categoryObj, dataHomePost, pageMaster, isMobile }) {
  const data = dataHomePost.Data;

  useEffect(() => {
    document.querySelector("body").classList.add("page-blog");
    if(dataHomePost.isHome == true) {
      ReplaceURL("/blog");
    }
    return function clean() {
      document.querySelector("body").classList.remove("page-blog");
    };
  }, []);

  return (
    <Page
      className=" font-inter"
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      robots={data.Meta.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isMobile={isMobile}
    >
      <Container>
        <div className="pt-2 my-6 md:my-8 md:pt-8">
          <h1 className="mb-1 text-3xl font-bold text-gray-900 md:text-5xl md:mb-4">
            {data.Category ? data.Category.Name : "Blog MoMo"}
          </h1>
          <h2 className="max-w-3xl text-gray-500 text-md md:text-lg">
            {data.Category
              ? data.Category.Description
              : "Nơi chia sẻ những thông tin, kiến thức hữu ích và giá trị dành cho bạn"}
          </h2>
        </div>

        <BlogMenu data={data.Categories} cat={categoryObj} />

        <SectionFeature data={data.ListBlogFeatured.Items} />

        <div className="pt-6 mt-6 border-t border-gray-200" />

        <div className="grid grid-cols-1 pb-10 lg:grid-cols-3 lg:gap-6">
          <div className="col-span-2 md:pr-4">
            {categoryObj && (
              <h2 className="mb-0 text-lg font-semibold">
                Bài viết mới nhất
              </h2>
            )}

            <div className="grid grid-cols-1 divide-y divide-gray-300 ">
              <BlogList
                data={data.ListBlogs}
                cat={data.Category ? data.Category : null}
              />
            </div>
          </div>

          <div className="">
            <StickyBox offsetTop={40} offsetBottom={20}>
              <div className="px-4 py-5 mt-8 rounded-lg bg-pink-50 lg:mt-0 lg:bg-white lg:px-0 lg:py-0">
                <h2 className="mb-0 text-lg font-semibold">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="inline w-5 h-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M12 7a1 1 0 110-2h5a1 1 0 011 1v5a1 1 0 11-2 0V8.414l-4.293 4.293a1 1 0 01-1.414 0L8 10.414l-4.293 4.293a1 1 0 01-1.414-1.414l5-5a1 1 0 011.414 0L11 10.586 14.586 7H12z"
                      clipRule="evenodd"
                    />
                  </svg>{" "}
                  Xem nhiều nhất
                </h2>
                <div className="grid grid-cols-1 ">
                  <PopularList data={data.ListBlogTopView.Items} />
                </div>
              </div>
            </StickyBox>
          </div>
        </div>

        <style jsx>{`
          .is-active {
            position: relative;
            font-weight: 600;
          }
          .is-active:after {
            content: "";
            position: absolute;
            width: 100%;
            bottom: 0px;
            height: 0;
            border-bottom: 2px;
            border-style: solid;
            left: 0;
            border-color: inherit;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
          }
        `}</style>
      </Container>
    </Page>
  );
}

export default PageCategory;
