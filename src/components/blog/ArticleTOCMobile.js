import Link from "next/link";

import { useState, useEffect, useRef, useCallback } from "react";

import StickyBox from "react-sticky-box";

//https://www.emgoto.com/react-table-of-contents/

const useIntersectionObserver = (setActiveId, data) => {
  const headingElementsRef = useRef({});
  useEffect(() => {
    const callback = (headings) => {
      headingElementsRef.current = headings.reduce((map, headingElement) => {
        map[headingElement.target.id] = headingElement;
        return map;
      }, headingElementsRef.current);

      //Get all headings that are currently visible on the page
      const visibleHeadings = [];
      Object.keys(headingElementsRef.current).forEach((key) => {
        const headingElement = headingElementsRef.current[key];
        if (headingElement.isIntersecting) visibleHeadings.push(headingElement);
      });

      //if there is only one visible heading, this is our "active" heading
      if (visibleHeadings.length >= 1) {
        setActiveId(visibleHeadings[0].target.id);
      }
    };

    const observer = new IntersectionObserver(callback, {
      rootMargin: "0px 0px -80% 0px",
    });

    data.forEach(
      (element) =>
        document.getElementById(element.Id) != null &&
        observer.observe(document.getElementById(element.Id))
    );
    return () => observer.disconnect();
  }, [setActiveId]);
};

const ArticleTOC = (props) => {
  const { data, ...rest } = props;
  const [activeId, setActiveId] = useState(null);

  useIntersectionObserver(setActiveId, data);

  const [isOpen, setIsOpen] = useState(false);

  const [tocName, setTocName] = useState("Danh mục");
  useEffect(() => {
    if (activeId != null) {
      const name = data.find((item) => item.Id == activeId);
      setTocName(name.Title);
    }
  }, [activeId]);

  const [stick, setStick] = useState(true);

  useEffect(() => {
    const update = () => {
      const last = window.scrollY;
      if (last >= 400) {
        setStick(true);
      } else {
        setStick(false);
        setIsOpen(false);
      }
    };
    update();

    document.addEventListener("scroll", update, { passive: true });
    return () => {
      setStick(false);
      document.removeEventListener("scroll", update);
    };
  });

  const toggleOpen = useCallback(() => setIsOpen((state) => !state), []);

  return (
    <>
      <div
        className={`toc-mobile top-0 fixed  left-0 right-0 z-2  transition-all  ${
          stick ? " opacity-1 visible " : "opacity-0 invisible "
        }`}
      >
        <div
          className=" bg-white backdrop-blur-sm bg-opacity-90 px-4 md:px-6 toc-mobile-label text-sm  top-0 h-12 border-b border-t border-gray-200 shadow-sm flex flex-nowrap relative items-center font-semibold z-0"
          onClick={toggleOpen}
        >
          <div className="block text-ellipsis whitespace-nowrap overflow-hidden pr-5">{tocName}</div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 absolute top-3 right-4 opacity-60"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M19 9l-7 7-7-7"
            />
          </svg>
        </div>

        <ul
          className={`toc-mobile-content py-2 px-6 border-b z-10 relative border-gray-200 shadow  bg-white  backdrop-blur-sm bg-opacity-90 ${
            isOpen ? "block" : "hidden"
          }`}
          onClick={() => setIsOpen(false)}
        >
          {data.map((item, index) => (
            <li data-soju={item.Id} key={item.Id}>
              <span
                className={`py-2 text-sm  block cursor-pointer  ${
                  activeId === item.Id
                    ? "text-pink-600  font-semibold "
                    : "text-gray-500 hover:text-gray-900"
                }`}
                onClick={(e) => {
                  e.preventDefault();
                  document.getElementById(`${item.Id}`).scrollIntoView({
                    behavior: "smooth",
                  });
                }}
              >
                {item.Title}
              </span>
            </li>
          ))}
        </ul>

        <div
          className={`bg-transparent fixed top-0 left-0 right-0 bottom-0 z-0 ${
            isOpen ? "block" : "hidden"
          }`}
          onClick={() => setIsOpen(false)}
        ></div>
      </div>

      <style jsx>{`
     
        .toc-mobile-content {
          max-height: 70vh;
          overflow-y: scroll;
          -webkit-overflow-scrolling: auto;
        }
      `}</style>
    </>
  );
};

export default ArticleTOC;
