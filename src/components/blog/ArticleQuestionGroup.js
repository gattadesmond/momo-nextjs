import { decode } from "html-entities";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";

import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

const ArticleQuestionGroup = ({ data, questId }) => {
  const List = data.filter((item) => item.Id == questId)[0];
  return List.Data[0] ? (
    <div>
      <div className="mt-4 md:mt-8">
        <div className="mb-1 text-sm font-semibold text-gray-400 uppercase">
          {List.Data[0].Name}
        </div>
        <Accordion
          collapsible
          className="bg-white divide-y divide-gray-200 article-question"
        >
          {List.Data[0].Items.map((item, index) => (
            <AccordionItem className="block " key={item.Id}>
              <AccordionButton className="block w-full text-left cursor-pointer focus:outline-none">
                <div className="relative block py-4 pl-0 pr-5 font-semibold text-gray-800 text-md hover:text-pink-600 ">
                  {item.Title}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="absolute right-0 inline w-5 h-5 -mt-2 text-gray-500  icon top-1/2"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </div>
              </AccordionButton>

              <AccordionPanel>
                <div className="pb-4 text-sm text-gray-600 soju__prose">
                  <div className="text-gray-600">
                    <ArticleContentConvert data={decode(item.Short)} />
                  </div>
                </div>
              </AccordionPanel>
            </AccordionItem>
          ))}
        </Accordion>
      </div>

      <style jsx>
        {`
          :global(.article-question
              [data-reach-accordion-item][data-state="open"]
              .icon) {
            transform: rotate(180deg);
          }
        `}
      </style>
    </div>
  ) : (
    <></>
  );
};

export default ArticleQuestionGroup;
