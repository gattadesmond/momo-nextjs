import Link from "next/link";

import { useState, useEffect, useRef, useCallback } from "react";

import StickyBox from "react-sticky-box";

//https://www.emgoto.com/react-table-of-contents/

const useIntersectionObserver = (setActiveId, data) => {
  const headingElementsRef = useRef({});
  useEffect(() => {
    const callback = (headings) => {
      headingElementsRef.current = headings.reduce((map, headingElement) => {
        map[headingElement.target.id] = headingElement;
        return map;
      }, headingElementsRef.current);

      //Get all headings that are currently visible on the page
      const visibleHeadings = [];
      Object.keys(headingElementsRef.current).forEach((key) => {
        const headingElement = headingElementsRef.current[key];
        if (headingElement.isIntersecting) visibleHeadings.push(headingElement);
      });

      //if there is only one visible heading, this is our "active" heading
      if (visibleHeadings.length >= 1) {
        setActiveId(visibleHeadings[0].target.id);
      }
    };

    const observer = new IntersectionObserver(callback, {
      rootMargin: "0px 0px -80% 0px",
    });

    data.forEach(
      (element) =>
        document.getElementById(element.Id) != null &&
        observer.observe(document.getElementById(element.Id))
    );
    return () => observer.disconnect();
  }, [setActiveId]);
};

const ArticleTOC = (props) => {
  const { data, ...rest } = props;
  const [activeId, setActiveId] = useState(null);

  useIntersectionObserver(setActiveId, data);

  const [isOpen, setIsOpen] = useState(false);

  const [tocName, setTocName] = useState("Danh mục");
  useEffect(() => {
    if (activeId != null) {
      const name = data.find((item) => item.Id == activeId);
      if(name?.Title) {
        setTocName(name.Title);
      }
    }
  }, [activeId]);

  const [stick, setStick] = useState(true);

  useEffect(() => {
    const update = () => {
      const last = window.scrollY;
      if (last >= 400) {
        setStick(true);
      } else {
        setStick(false);
        setIsOpen(false);
      }
    };
    update();

    document.addEventListener("scroll", update, { passive: true });
    return () => {
      setStick(false);
      document.removeEventListener("scroll", update);
    };
  });

  const toggleOpen = useCallback(() => setIsOpen((state) => !state), []);

  return (
    <>
      <StickyBox offsetTop={30} offsetBottom={20} className="hidden lg:block">
        <h4 className=" uppercase text-gray-500 mt-3  tracking-wider text-tiny font-semibold mb-5">
          MỤC LỤC
        </h4>
        <ul className="">
          {data.map((item, index) => (
            <li
              data-soju={item.Id}
              key={item.Id}
              className={`pl-4 border-l  ${
                activeId === item.Id
                  ? " border-pink-500 bg-pink-50 bg-opacity-60"
                  : "border-gray-200"
              }`}
            >
              <span
                className={`py-2 text-sm block cursor-pointer  ${
                  activeId === item.Id
                    ? "text-pink-600 "
                    : "text-gray-500 hover:text-gray-900"
                }`}
                // href={`#${item.Id}`}
                onClick={(e) => {
                  e.preventDefault();
                  document.getElementById(`${item.Id}`).scrollIntoView({
                    behavior: "smooth",
                  });
                }}
              >
                {item.Title}
              </span>
            </li>
          ))}
        </ul>
      </StickyBox>

     
      <style jsx>{`
     
        .toc-mobile-content {
          max-height: 70vh;
          overflow-y: scroll;
          -webkit-overflow-scrolling: auto;
        }
      `}</style>
    </>
  );
};

export default ArticleTOC;
