import Link from "next/link";

import { useState, useEffect, useRef, useCallback } from "react";

import Rating from "react-rating";

import { axios } from "@/lib/index";

const ArticleRating = (props) => {
  const { Count, Value, Id, Type = 1, children, ...rest } = props;

  // Type = 1 -- Blog
  // Type = 4 -- Article
  // Type = 3 -- Question
  
  const [countArticle, setCountArticle] = useState(null);
  useEffect(() => {
    setCountArticle(Count);
  }, [Count])

  const [complete, setComplete] = useState(false);

  // useEffect(() => {
  //   const rememberRating = localStorage.getItem('rememberRating');
  //   if(rememberRating == Id) {
  //     setComplete(true);
  //   } else{
  //     setComplete(false);
  //   }
  // } , [])

  const handleRating = (stars) => {
    axios
      .post(`/common/rating`, {
        data: {
          Type: Type,
          Id: Id,
          Stars: stars,
        },
      })
      .then((res) => {
        if (!res || !res.Result) return;

        // localStorage.setItem('rememberRating', Id);

        setCountArticle(parseInt(countArticle) + 1);
        setComplete(true);
      });
  };

  return (
    <>
      <div className="mt-6 lg:mt-8 text-sm flex flex-nowrap items-center text-gray-700 rating">
        <div className="mr-2">Đánh giá : </div>{" "}
        <div className="font-semibold text-pink-600 mr-1 text-base">
          {Value ? Value : 0}
        </div>
        <div className="text-xs mr-3 pt-1">
          /{countArticle ? countArticle : 0}
        </div>
        {!complete ? (
          <div className="pt-1">
            <Rating
              onClick={handleRating}
              initialRating={Value}
              emptySymbol={
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5 text-gray-500"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}
                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
                  />
                </svg>
              }
              fullSymbol={
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5 text-gray-500"
                  fill="#FCD34D"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}
                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"
                  />
                </svg>
              }
            />
          </div>
        ) : (
          <div className="px-3 py-1 border border-yellow-300  text-yellow-800 bg-yellow-50 rounded inline-block">
            Cám ơn bạn đã đánh giá 😘
          </div>
        )}
      </div>
    </>
  );
};

export default ArticleRating;
