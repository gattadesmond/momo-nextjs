import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

const PopularList = ({ data }) => {
  return (
    <>
      {data &&
        data.map((item, index) => (
          <article className="py-3 md:py-5" key={item.Id}>
            <div className="grid grid-flow-col gap-4 md:gap-6 ">
              <div className="md:col-start-2">
                <ArticleInfo
                  type="small"
                  title={item.Title}
                  slug={item.Link}
                  sapo={item.ShortContent}
                  catName={item.CategoryName}
                  catLink={item.CategoryLink}
                  catId={item.CategoryId}
                  timeRead={item.TimeReadAvg}
                  totalView={item.TotalView}
                  date={item.Date}
                />
              </div>

              <div className="w-28 md:w-52 pt-2 md:col-start-1">
                <ArticleThumbnail
                  type="square"
                  avatar={item.Avatar}
                  title={item.Title}
                  slug={item.Link}
                />
              </div>
            </div>
          </article>
        ))}
    </>
  );
};

export default PopularList;
