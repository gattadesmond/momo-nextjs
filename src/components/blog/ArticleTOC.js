import Link from "next/link";


import dynamic from "next/dynamic";

import { useMediaQuery } from "react-responsive";
import { LG } from "@/lib/breakpoint";


const Desktop = dynamic(() =>
  import("../../components/blog/ArticleTOCDesktop")
);
const Mobile = dynamic(() => import("../../components/blog/ArticleTOCMobile"));

const ArticleTOC = (props) => {
  const { children, ...rest } = props;

  const isLG = useMediaQuery(LG);


  return (
    <>{!isLG ? <Mobile data={props.data} /> : <Desktop data={props.data} />}</>
  );
};

export default ArticleTOC;
