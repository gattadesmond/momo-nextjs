import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

const BlogList = ({ data }) => {
  return (
    <>
      {data &&
        data.map((item, index) => (
          <article className=" py-2 md:py-3" key={item.Id}>
            <div className="flex flex-nowrap ">
           
              <div className=" w-28 pt-2 md:w-36 flex-initial md:order-1 relative">
                <ArticleThumbnail
                  avatar={item.Avatar}
                  title={item.Title}
                  slug={item.Link}
                >
                  <div className="p-3 text-3xl font-semibold absolute left-0 top-0 w-full h-full z-10  overflow-hidden text-white grad-over-popular flex items-end ">
                    0{index + 1}
                  </div>
                </ArticleThumbnail>
              </div>

              <div className="flex-1 md:order-2 pl-4 md:pl-5  ">
                <ArticleInfo
                  type="popular"
                  title={item.Title}
                  slug={item.Link}
                  sapo={item.ShortContent}
                  catName={item.CategoryName}
                  catLink={item.CategoryLink}
                  catId={item.CategoryId}
                  timeRead={item.TimeReadAvg}
                  totalView={item.TotalView}
                  date={item.PublicDate}
                />
              </div>

            </div>

            <style jsx>{`
              .grad-over-popular {
                background: linear-gradient(
                  -125deg,
                  transparent 54%,
                  rgba(0, 0, 0, 0.5) 100%
                );
                text-shadow: 0 2px 4px rgb(0, 0, 0, 57%);
              }
            `}</style>
          </article>
        ))}
    </>
  );
};

export default BlogList;
