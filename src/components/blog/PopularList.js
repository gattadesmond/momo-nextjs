import Link from "next/link";

import classNames from "classnames";

import ArticleInfo from "@/components/blog/ArticleInfo";
import ArticleThumbnail from "@/components/blog/ArticleThumbnail";

const SectionList = ({ data }) => {
  return (
    <>
      {data &&
        data.map((item, index) => (
          <article className="py-3" key={item.Id}>
            <div className="grid grid-flow-col gap-4 ">
              <div className="w-40 col-start-1 row-start-1 relative">
                <ArticleThumbnail
                  type="square"
                  avatar={item.Avatar}
                  title={item.Title}
                  slug={item.Link}
                >
                  <div className="p-3 text-3xl font-semibold absolute left-0 top-0 w-full h-full z-10  overflow-hidden text-white grad-over-popular flex items-end ">
                    0{index + 1}
                  </div>
                </ArticleThumbnail>
              </div>

              <ArticleInfo
                type="popular"
                title={item.Title}
                slug={item.Link}
                sapo={item.ShortContent}
                catName={item.CategoryName}
                catLink={item.CategoryLink}
                catId={item.CategoryId}
                timeRead={item.TimeReadAvg}
                totalView={item.TotalView}
                date={item.Date}
              />
            </div>

            <style jsx>{`
              .grad-over-popular {
                background: linear-gradient(
                  -125deg,
                  transparent 54%,
                  rgba(0, 0, 0, 0.5) 100%
                );
                text-shadow: 0 2px 4px rgb(0, 0, 0, 57%);
              }
            `}</style>
          </article>
        ))}
    </>
  );
};

export default SectionList;
