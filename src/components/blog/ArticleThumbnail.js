import Link from "next/link";
import Image from "next/image";

import classNames from "classnames";

const ArticleThumbnail = ({ avatar, title, slug, children, type = "normal", objectFit = "cover", ...rest }) => {
  return (
    slug
      ? <Link href={slug}>
        <a>
          <div
            className={classNames("bg-gray-100  overflow-hidden", {
              "aspect-w-16 aspect-h-9 rounded-md": type == "normal"
            })}
          >
            <Image
              src={avatar}
              alt={title}
              layout="fill"
              objectFit={objectFit}
              
              loading="lazy"
            />
            {children}
          </div>
        </a>
      </Link>
      : <div
        className={classNames("bg-gray-100  overflow-hidden", {
          "aspect-w-16 aspect-h-9 rounded-md": type == "normal"
        })}
      >
        <Image
          src={avatar}
          alt={title}
          layout="fill"
          objectFit={objectFit}
          
          loading="lazy"
        />
        {children}
      </div>
  );
};

export default ArticleThumbnail;

