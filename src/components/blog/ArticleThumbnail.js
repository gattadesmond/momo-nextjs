import Link from "next/link";
import Image from "next/image";

import classNames from "classnames";

const ArticleThumbnail = ({ avatar, title, slug, children, type = "normal", ...rest }) => {
  return (
    <Link href={slug}>
      <a>
        <div
          className={classNames("bg-gray-100 rounded-md overflow-hidden", {
            "aspect-w-16 aspect-h-9": type == "normal",
            "aspect-w-16 aspect-h-11": type == "square",
          })}
        >
          <Image
            src={avatar}
            alt={title}
            layout="fill"
            objectFit="cover"
          />
          {children}
        </div>
      </a>
    </Link>
  );
};

export default ArticleThumbnail;
