const LdJson_BreadCrumbs = (props) => {
  const { BreadCrumbs } = props;

  var ldJson = "";
  if (BreadCrumbs && BreadCrumbs.length > 0) {
    for (var idx = 0; idx < BreadCrumbs.length; idx++) {
      var x = BreadCrumbs[idx];

      ldJson += (ldJson?',':'')+ `
          {
            "@type": "ListItem",
            "position": ${idx + 1},
            "item":
            {
                "@id": "${x.Url}",
                "name": "${x.Title}"
            }
          }
          `;
    }
  }

  return (
    <>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement":
            [
            ${ldJson}
            ]
        }`,
        }}
      ></script>
    </>
  );
};

export default LdJson_BreadCrumbs;
