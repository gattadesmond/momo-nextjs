const LdJson_Meta_Movie = (props) => {
  var {
    Title,
    Description,
    Keywords,
    Avatar,
    RatingCount,
    RatingValue,
    Robots,
    Scripts,
    Url,
    Date
  } = props.Meta;
  var {
    Film
  } = props;

  var ldJsonDirector = "";
  if (Film.ApiCasts && Film.ApiCasts.length > 0) {
    Film.ApiCasts.forEach((x) => {
      ldJsonDirector +=
        (ldJsonDirector ? "," : "") +
        `
          {
            "@type": "Person",
            "name": "${x.name}"
          }
          `;
    });
  }
  return (
    <>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: `
        {
          "@context": "http://schema.org",
          "@type": "Movie",
          "@id": "${Film.Id}",
          "sameAs": "${Url}",
          "url": "${Url}",
          "name": "${Title}",
          "description": "${Description}",
          "duration": "${Film.Duration} phút",
          "image": {
            "@type": "ImageObject",
            "url": "${Avatar}",
            "width": "400",
            "height": "600"
          },
          "datePublished": "${Date}",
          "genre": "${Film.ApiGenreName}",
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": ${RatingValue},
            "ratingCount": ${RatingCount},
            "bestRating": 5
          },
          "director": [
            ${ldJsonDirector}
          ]
        }`,
        }}
      ></script>
    </>
  );
};

export default LdJson_Meta_Movie;
