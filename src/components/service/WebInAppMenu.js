import { useContext, useEffect, useState } from "react";

import { momoPlatformApi } from "@/lib/momo";
import useAppContext from "@/lib/appContext";

import ButtonShare from "@/components/ui/ButtonShare";


export default function WebInAppMenu({ link }) {

  const { isViewApp } = useAppContext();
  // const { exitApp } = momoPlatformApi();

  const [visible, setVisible] = useState(true);

  let scrolling = false,
    previousTop = 0,
    scrollDelta = 10,
    scrollOffset = 150;

  function autoHideMenu() {
    const currentTop = window.pageYOffset;
    if (previousTop - currentTop > scrollDelta) {
      setVisible(true);
    } else if (
      currentTop - previousTop > scrollDelta &&
      currentTop > scrollOffset
    ) {
      setVisible(false)
    }

    previousTop = currentTop;
    scrolling = false;
  }

  function eventScrolling () {
    if (!scrolling) {
      scrolling = true;
      !window.requestAnimationFrame
      ? setTimeout(autoHideMenu, 250)
      : requestAnimationFrame(autoHideMenu);
    }
  }

  // useEffect(() => {
  //   window.addEventListener("scroll", eventScrolling, { passive: true });
  //   () => {
  //     window.removeEventListener("scroll", eventScrolling);
  //   };
  // }, []);

  return (
    <div
      id="webInAppMenu"
      className={`webInAppMenu fixed z-30 top-0 left-0 w-full flex justify-between py-3 px-4 transition
        ${visible ? "translate-y-0" : "-translate-y-full"}
      `}
    >
      <div></div>
      <ButtonShare 
        link={link} 
        isViewApp={isViewApp} 
      />
      <style jsx>{`
        .webInAppMenu :global(.button-share-bg) {
          background-color: white;
          color: var(--pinkmomo);
        }
      `}</style>
    </div>
  )
}