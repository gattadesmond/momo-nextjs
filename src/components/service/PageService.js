import { useEffect, useLayoutEffect, useState } from "react";
import dynamic from "next/dynamic";

import { getRootSlug } from "@/lib/url";
import { appSettings } from "@/configs";
import useAppContext from "@/lib/appContext";
import useBrowserLayoutEffect from "@/lib/useBrowserLayoutEffect";

import Page from "@/components/page";
import LdJson_QA from "@/components/LdJson_QA";
import LdJson_Meta_NewsArticle from "@/components/LdJson_Meta_NewsArticle";
import SectionGroup from "@/components/section/SectionGroup";
import WebInAppMenu from "./WebInAppMenu";

const ServiceBreadCrumb = dynamic(() => import("./ServiceBreadCrumb"));
const ServiceMenu = dynamic(() => import("./ServiceMenu"));
const FooterCta = dynamic(() => import("@/components/FooterCta"));

function PageService({
  dataServiceBreadcrumbs,
  dataServiceDetail,
  dataServiceListBlock,
  pageMaster,
  isMobile,
}) {
  const metaData = dataServiceDetail.Data;

  const breadCrumbBlock =
    dataServiceListBlock.Data.filter((x) => x.TypeName === "BreadCrumb")[0] ||
    null;
  const ctaFooter = dataServiceListBlock.Data.filter(
    (x) => x.TypeName === "CtaFooter"
  );
  const listBlock = dataServiceListBlock.Data.filter(
    (x) => !["CtaFooter"].includes(x.TypeName)
  );

  const isMenuSticky =
    listBlock.findIndex((x) => x.TypeName === "MenuSticky") > -1 ? true : false;

  const { isViewApp, setIsViewAppFromDataApi } = useAppContext();

  const [isRenderCtaFooter, setIsRenderCtaFooter] = useState(false);

  useBrowserLayoutEffect(() => {
    setIsViewAppFromDataApi(metaData?.ViewInApp);

    document.querySelector("body").classList.add("fix-header");
    return function clean() {
      document.querySelector("body").classList.remove("fix-header");
    };
  }, []);

  useEffect(() => {
    setIsRenderCtaFooter(checkCtaFooter());
  }, [isViewApp]);

  const checkCtaFooter = () => {
    if (isViewApp) return false;
    if (ctaFooter.length === 0) return false;
    if (isMobile && isMenuSticky) return false;
    return true;
  };
  
  return (
    <Page
      className=""
      title={metaData.Meta?.Title}
      description={metaData.Meta?.Description}
      image={metaData.Meta?.Avatar}
      keywords={metaData.Meta?.Keywords}
      robots={metaData.Meta?.Robots}
      header={pageMaster.Data?.MenuHeaders}
      isShowHeader={metaData.HeaderType == 1}
      isShowFooter={metaData.ShowFooter}
      isMobile={isMobile}
      isViewApp={isViewApp}
    >
      <LdJson_Meta_NewsArticle Meta={metaData.Meta} />
      {metaData.QaData && <LdJson_QA QaData={metaData.QaData} />}
      {isViewApp && (
        <WebInAppMenu
          link={`${appSettings.AppConfig.FRONT_END}${getRootSlug(
            metaData.Link
          )}`}
        />
      )}
      {!isViewApp &&
        (breadCrumbBlock ? (
          <ServiceBreadCrumb
            isStickyMobile={!isMenuSticky}
            data={breadCrumbBlock}
          />
        ) : (
          <ServiceMenu
            isStickyMobile={!isMenuSticky}
            data={dataServiceBreadcrumbs}
            pageId={metaData.Id}
            isMobile={isMobile}
          />
        ))}
      <SectionGroup data={listBlock} isMobile={isMobile} startType={2} />
      {isRenderCtaFooter && <FooterCta data={ctaFooter} isMobile={isMobile} />}
    </Page>
  );
}

export default PageService;
