import BreadcrumbContainer, { BreadcrumbItem } from "../BreadcrumbContainer";

import { getUrlHasRoot } from "@/lib/url";
const ServiceMenu = ({ className, data, pageId, isMobile, isStickyMobile = true }) => {
  if (!data) return <></>;
  const menuData = data?.Data || null;
  if (!menuData) return;

  let navItems = [...menuData.ListChilds];

  return (
    <BreadcrumbContainer
      id={pageId}
      className={`${className} ${isStickyMobile ? "sticky" : "block"} md:sticky z-30 -top-px`}
    >
      <BreadcrumbItem url="https://momo.vn/" />

      {menuData.ShowBreadcrumb &&
        <BreadcrumbItem
          icon={menuData.Icon}
          title={menuData.Title}
          url={menuData.Url}
          isShowIcon={false}
          isActive={menuData.Id === pageId ? true : false}
        />
      }

      {navItems
        .filter(x => x.ShowBreadcrumb)
        .sort((a, b) => a.Id - b.Id)
        .sort((a, b) => a.Id === pageId ? -1 : 1)
        .map((item, index) => (
          <BreadcrumbItem
            key={index}
            icon={item.Icon}
            title={item.Title}
            url={getUrlHasRoot('/'+item.Url)}
            // url={item.Url}
            isActive={item.Id === pageId}
            isCatItem={true}
          />
        ))}
    </BreadcrumbContainer>
  );
};

export default ServiceMenu;
