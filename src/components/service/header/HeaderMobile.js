import { useRouter } from "next/router";

import dynamic from "next/dynamic";

import Link from "next/link";

import { useState, useEffect, lazy, Suspense } from "react";

import Search from "@/components/Search";
const HeaderSubMenuMobile = dynamic(() => import("./HeaderSubMenuMobile"));
import { getUrlHasRoot } from "@/lib/url";

const HeaderMobile = ({ data }) => {
  const [isOpen, setIsOpen] = useState(false);

  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url, { shallow }) => {
      setIsOpen(false);
    };

    router.events.on("routeChangeStart", handleRouteChange);

    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
    };
  }, []);

  const openMenuMobile = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <nav
        className={`mobile-menu header-mobile z-60 sticky top-0 left-0 right-0  border-b border-gray-200 bg-white bg-opacity-95 backdrop-blur-sm lg:hidden ${
          isOpen ? "active" : ""
        }`}
      >
        <div className="relative z-20 flex items-center pt-1 pb-1 pl-4 pr-1">
          <div className="flex flex-none items-center justify-center space-x-3 ">
            <a
              href={getUrlHasRoot("/")}
              className="block transition-opacity hover:opacity-90"
              aria-label="MoMo"
            >
              <svg
                width={72}
                height={72}
                viewBox="0 0 72 72"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="mx-auto block h-9 w-9"
              >
                <path
                  d="M0 8C0 3.58172 3.58172 0 8 0H64C68.4183 0 72 3.58172 72 8V64C72 68.4183 68.4183 72 64 72H8C3.58172 72 0 68.4183 0 64V8Z"
                  fill="#A50064"
                />
                <path
                  d="M51.859 10C45.6394 10 40.5057 15.0349 40.5057 21.3533C40.5057 27.5729 45.5407 32.7065 51.859 32.7065C58.0786 32.7065 63.2123 27.6716 63.2123 21.3533C63.2123 15.1337 58.1774 10 51.859 10ZM51.859 26.1908C49.1935 26.1908 47.0215 24.0188 47.0215 21.3533C47.0215 18.6877 49.1935 16.5158 51.859 16.5158C54.5246 16.5158 56.6965 18.6877 56.6965 21.3533C56.6965 24.0188 54.5246 26.1908 51.859 26.1908Z"
                  fill="white"
                />
                <path
                  d="M28.7576 10C26.8818 10 25.1048 10.5923 23.6239 11.6783C22.2418 10.5923 20.4648 10 18.4903 10C13.7515 10 10 13.8502 10 18.4903V32.7065H16.5158V18.4903C16.5158 17.4043 17.4043 16.6145 18.3915 16.6145C19.4775 16.6145 20.2673 17.503 20.2673 18.4903V32.7065H26.7831V18.4903C26.7831 17.4043 27.6716 16.6145 28.6589 16.6145C29.7448 16.6145 30.5346 17.503 30.5346 18.4903V32.7065H37.0504V18.589C37.2479 13.8502 33.4963 10 28.7576 10Z"
                  fill="white"
                />
                <path
                  d="M51.859 37.6427C45.6394 37.6427 40.5057 42.6776 40.5057 48.996C40.5057 55.2156 45.5407 60.3492 51.859 60.3492C58.0786 60.3492 63.2123 55.3143 63.2123 48.996C63.2123 42.6776 58.1774 37.6427 51.859 37.6427ZM51.859 53.7347C49.1935 53.7347 47.0215 51.5628 47.0215 48.8972C47.0215 46.2317 49.1935 44.0598 51.859 44.0598C54.5246 44.0598 56.6965 46.2317 56.6965 48.8972C56.6965 51.6615 54.5246 53.7347 51.859 53.7347Z"
                  fill="white"
                />
                <path
                  d="M28.7576 37.6427C26.8818 37.6427 25.1048 38.235 23.6239 39.321C22.2418 38.235 20.4648 37.6427 18.4903 37.6427C13.7515 37.6427 10 41.4929 10 46.133V60.3492H16.5158V46.0342C16.5158 44.9483 17.4043 44.1585 18.3915 44.1585C19.4775 44.1585 20.2673 45.047 20.2673 46.0342V60.2505H26.7831V46.0342C26.7831 44.9483 27.6716 44.1585 28.6589 44.1585C29.7448 44.1585 30.5346 45.047 30.5346 46.0342V60.2505H37.0504V46.133C37.2479 41.3942 33.4963 37.6427 28.7576 37.6427Z"
                  fill="white"
                />
              </svg>
            </a>

            {data.Root &&
              (data.Root.Url ? (
                data.Root.Icon ? (
                  <Link href={getUrlHasRoot(data.Root.Url)}>
                    <a className=" absolute left-1/2  block -translate-x-1/2 ">
                      <img
                        style={{"height":"42px"}}
                        src={data.Root.Icon}
                        className="mr-3  block  max-h-10"
                      />
                    </a>
                  </Link>
                ) : (
                  data.Root.Name && (
                    <Link href={getUrlHasRoot(data.Root.Url)}>
                      <a
                        target={data.Root.NewTab ? "blank" : ""}
                        className=" leading-none text-gray-800 hover:text-pink-600"
                      >
                        {data.Root.Name && (
                          <span className="block  pt-1  font-bold leading-tight">
                            {data.Root.Name}
                          </span>
                        )}
                        {data.Root.Description && (
                          <span className="text-sm text-gray-500">
                            {data.Root.Description}
                          </span>
                        )}
                      </a>
                    </Link>
                  )
                )
              ) : data.Root.Icon ? (
                <a className=" absolute left-1/2  block -translate-x-1/2 ">
                  <img
                    style={{"height":"42px"}}
                    src={data.Root.Icon}
                    className="mr-3  block  max-h-10"
                  />
                </a>
              ) : (
                data.Root.Name && (
                  <a className=" leading-none text-gray-800 hover:text-pink-600">
                    {data.Root.Name && (
                      <span className="block  pt-1  font-bold leading-tight">
                        {data.Root.Name}
                      </span>
                    )}
                    {data.Root.Description && (
                      <span className="text-sm text-gray-500">
                        {data.Root.Description}
                      </span>
                    )}
                  </a>
                )
              ))}
          </div>
          <div className="flex-1 pl-4 pr-1"> &nbsp;</div>
          <div className="flex-none">
            <div
              className="mobile-menu__button flex h-11 w-12 cursor-pointer items-center justify-center"
              onClick={() => openMenuMobile()}
            >
              {isOpen ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-7 w-7"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-7 w-7"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M4 6h16M4 12h16M4 18h16"
                  />
                </svg>
              )}
            </div>
          </div>
        </div>
      </nav>

      {isOpen && (
        <Suspense fallback={``}>
          <HeaderSubMenuMobile menu={data} />
        </Suspense>
      )}

      {/* <div
        className={`overlay js-overlay z-20 ${isOpen ? "is-active" : ""}`}
      ></div> */}

      <style jsx>{`
        .overlay {
          background-color: rgba(255, 255, 255, 0.65);
          -webkit-backdrop-filter: blur(10px);
          backdrop-filter: blur(10px);
        }

        .overlay {
          position: fixed;
          width: 100%;
          height: 100%;
          height: 100vh;
          // height: -webkit-fill-available;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          padding: 0;
          overflow-y: scroll;
          opacity: 0;
          pointer-events: none;
          visibility: hidden;
          transition-duration: 350ms;
          transition-property: opacity, visibility;
          background-color: rgba(255, 255, 255, 0.85);
        }

        .overlay.is-active {
          opacity: 1;
          visibility: visible;
          pointer-events: all;
          transition-timing-function: cubic-bezier(0.165, 0.84, 0.44, 1);
        }
      `}</style>
    </>
  );
};

export default HeaderMobile;
