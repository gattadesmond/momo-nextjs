import Link from "next/link";
import dynamic from "next/dynamic";
import Image from "next/image";

import { useState, useEffect, lazy, Suspense } from "react";

import Search from "@/components/Search";

import { getUrlHasRoot } from "@/lib/url";

const HeaderSubMenu = ({ data }) => {
  return (
    <>
      {data.length > 0 && (
        <div
          className="submenu fadeInUp absolute opacity-0"
          style={{ width: "340px", opacity: "0" }}
        >
          <div className="grid grid-cols-1 gap-2 rounded border border-gray-200 bg-white p-3 shadow-xl">
            {data.map((child, index) => (
              <Link href={getUrlHasRoot(child.Url)} key={child.Id}>
                <a
                  // href={getUrlHasRoot(child.Url)}
                  // key={child.Id}
                  className="group flex flex-row flex-nowrap items-center rounded-md px-2 py-2 hover:bg-pink-100 hover:bg-opacity-50 "
                >
                  <div className="flex-none pr-3 ">
                    <img
                      src={child.Icon}
                      className="h-9 w-9 object-contain"
                      loading="lazy"
                    />
                  </div>

                  <div className="flex-1 ">
                    <div className="whitespace-nowrap font-bold text-gray-700 group-hover:text-pink-700 ">
                      {child.Name}
                    </div>
                    <div className="text-xs opacity-50 ">
                      {child.Description}
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

const HeaderDesktop = ({ data }) => {
  const menu = data;

  const [activeMenu, setActiveMenu] = useState(false);

  const [timeoutId, setTimeoutId] = useState(false);

  // let timeoutId = null;

  const handleMouseEnter = (tar) => {
    let timeout = window.setTimeout(() => {
      setActiveMenu(tar);
    }, 200);
    setTimeoutId(timeout);
  };

  const handleMouseLeave = (tar) => {
    window.clearTimeout(timeoutId);
    setActiveMenu(false);
  };

  const getMenuItem = (tar) =>
    menu.Items?.filter((item) => item.ParentId == tar).sort((a, b) =>
      a.OrderView > b.OrderView ? -1 : 1
    );

  const menuParent = getMenuItem(null);

  return (
    <>
      <nav className="header-desktop main-menu sticky top-0 z-40 hidden border-b border-gray-200   bg-white/95 shadow-sm  backdrop-blur transition-transform lg:block">
        <div className="text-md mx-auto flex w-full max-w-6xl items-center justify-between px-6 lg:px-8">
          <div className="flex flex-none items-center space-x-3">
            <a
              href={getUrlHasRoot("/")}
              className="block transition-opacity hover:opacity-90"
              aria-label="MoMo"
            >
              <svg
                width={72}
                height={72}
                viewBox="0 0 72 72"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="mx-auto block h-10 w-10"
              >
                <path
                  d="M0 8C0 3.58172 3.58172 0 8 0H64C68.4183 0 72 3.58172 72 8V64C72 68.4183 68.4183 72 64 72H8C3.58172 72 0 68.4183 0 64V8Z"
                  fill="#A50064"
                />
                <path
                  d="M51.859 10C45.6394 10 40.5057 15.0349 40.5057 21.3533C40.5057 27.5729 45.5407 32.7065 51.859 32.7065C58.0786 32.7065 63.2123 27.6716 63.2123 21.3533C63.2123 15.1337 58.1774 10 51.859 10ZM51.859 26.1908C49.1935 26.1908 47.0215 24.0188 47.0215 21.3533C47.0215 18.6877 49.1935 16.5158 51.859 16.5158C54.5246 16.5158 56.6965 18.6877 56.6965 21.3533C56.6965 24.0188 54.5246 26.1908 51.859 26.1908Z"
                  fill="white"
                />
                <path
                  d="M28.7576 10C26.8818 10 25.1048 10.5923 23.6239 11.6783C22.2418 10.5923 20.4648 10 18.4903 10C13.7515 10 10 13.8502 10 18.4903V32.7065H16.5158V18.4903C16.5158 17.4043 17.4043 16.6145 18.3915 16.6145C19.4775 16.6145 20.2673 17.503 20.2673 18.4903V32.7065H26.7831V18.4903C26.7831 17.4043 27.6716 16.6145 28.6589 16.6145C29.7448 16.6145 30.5346 17.503 30.5346 18.4903V32.7065H37.0504V18.589C37.2479 13.8502 33.4963 10 28.7576 10Z"
                  fill="white"
                />
                <path
                  d="M51.859 37.6427C45.6394 37.6427 40.5057 42.6776 40.5057 48.996C40.5057 55.2156 45.5407 60.3492 51.859 60.3492C58.0786 60.3492 63.2123 55.3143 63.2123 48.996C63.2123 42.6776 58.1774 37.6427 51.859 37.6427ZM51.859 53.7347C49.1935 53.7347 47.0215 51.5628 47.0215 48.8972C47.0215 46.2317 49.1935 44.0598 51.859 44.0598C54.5246 44.0598 56.6965 46.2317 56.6965 48.8972C56.6965 51.6615 54.5246 53.7347 51.859 53.7347Z"
                  fill="white"
                />
                <path
                  d="M28.7576 37.6427C26.8818 37.6427 25.1048 38.235 23.6239 39.321C22.2418 38.235 20.4648 37.6427 18.4903 37.6427C13.7515 37.6427 10 41.4929 10 46.133V60.3492H16.5158V46.0342C16.5158 44.9483 17.4043 44.1585 18.3915 44.1585C19.4775 44.1585 20.2673 45.047 20.2673 46.0342V60.2505H26.7831V46.0342C26.7831 44.9483 27.6716 44.1585 28.6589 44.1585C29.7448 44.1585 30.5346 45.047 30.5346 46.0342V60.2505H37.0504V46.133C37.2479 41.3942 33.4963 37.6427 28.7576 37.6427Z"
                  fill="white"
                />
              </svg>
            </a>

            {menu.Root &&
              (menu.Root.Url ? (
                <>
                  {menu.Root.Icon && (
                    <Link href={getUrlHasRoot(menu.Root.Url)}>
                      <a className="block">
                        <img
                          style={{"height":"42px"}}
                          src={menu.Root.Icon}
                          className="block  max-h-10 "
                        />
                      </a>
                    </Link>
                  )}
                  {menu.Root.Name && (
                    <Link href={getUrlHasRoot(menu.Root.Url)}>
                      <a
                        target={menu.Root.NewTab ? "blank" : ""}
                        className=" leading-none text-gray-800 hover:text-pink-600"
                      >
                        {menu.Root.Name && (
                          <span className="block  pt-1  font-bold leading-tight">
                            {menu.Root.Name}
                          </span>
                        )}
                        {menu.Root.Description && (
                          <span className="text-sm text-gray-500">
                            {menu.Root.Description}
                          </span>
                        )}
                      </a>
                    </Link>
                  )}
                </>
              ) : (
                <>
                  {menu.Root.Icon && (
                    <a className="block">
                      <img
                        style={{"height":"42px"}}
                        src={menu.Root.Icon}
                        className="block  max-h-10 "
                      />
                    </a>
                  )}
                  {menu.Root.Name && (
                    <a className=" leading-none text-gray-800 hover:text-pink-600">
                      {menu.Root.Name && (
                        <span className="block  pt-1  font-bold leading-tight">
                          {menu.Root.Name}
                        </span>
                      )}
                      {menu.Root.Description && (
                        <span className="text-sm text-gray-500">
                          {menu.Root.Description}
                        </span>
                      )}
                    </a>
                  )}
                </>
              ))}
          </div>

          <div className="flex-1 px-3">
            <div className="flex flex-row flex-nowrap justify-end">
              {menuParent &&
                menuParent.map((item, index) => {
                  var childs = getMenuItem(item.Id);
                  return (
                    <div
                      key={item.Id}
                      className={`menu relative ${
                        activeMenu == item.Id ? "is-active" : ""
                      }`}
                      onMouseEnter={() => handleMouseEnter(item.Id)}
                      onMouseLeave={() => handleMouseLeave(item.Id)}
                    >
                      {item.Url != "" ? (
                        <Link href={getUrlHasRoot(item.Url)}>
                          <a //href={getUrlHasRoot(item.Url)}
                            className="block cursor-pointer whitespace-nowrap px-2 py-5 font-bold text-gray-600 transition-all hover:text-gray-900"
                          >
                            {item.Name}
                            {childs.length > 0 && (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="icon relative ml-1 inline h-4 w-4  text-gray-500 "
                                viewBox="0 0 20 20"
                                fill="currentColor"
                              >
                                <path
                                  fillRule="evenodd"
                                  d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                  clipRule="evenodd"
                                />
                              </svg>
                            )}
                          </a>
                        </Link>
                      ) : (
                        <div className="block cursor-pointer whitespace-nowrap px-2 py-5 font-bold  text-gray-600 hover:text-gray-900">
                          {item.Name}
                          {childs.length > 0 && (
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="icon relative ml-1 inline h-4 w-4 text-gray-500  transition-transform "
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path
                                fillRule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clipRule="evenodd"
                              />
                            </svg>
                          )}
                        </div>
                      )}

                      {activeMenu == item.Id && (
                        <Suspense fallback={``}>
                          <HeaderSubMenu data={childs} />
                        </Suspense>
                      )}
                    </div>
                  );
                })}
            </div>
          </div>

          {/* <div className="flex-none">
            <Search />
          </div> */}
        </div>
      </nav>

      <style jsx global>{`
        .submenu {
          visibility: hidden;
          opacity: 0;
          padding-top: 15px;
        }
        .submenu:before {
          content: "";
          width: 0;
          height: 0;
          border-left: 15px solid transparent;
          border-right: 15px solid transparent;
          border-bottom: 15px solid #e0e0e0;
          position: absolute;
          top: 2px;
          left: 34px;
          margin-left: -5px;
          pointer-events: none;
          transition: opacity 0.2s ease-in;
        }
        .submenu:after {
          content: "";
          width: 0;
          height: 0;
          border-left: 14px solid transparent;
          border-right: 14px solid transparent;
          border-bottom: 14px solid #fff;
          position: absolute;
          top: 3px;
          left: 35px;
          margin-left: -5px;
          pointer-events: none;
          transition: opacity 0.2s ease-in;
        }

        .menu.is-active .submenu {
          opacity: 1 !important;
          transform: translate3d(0, 0, 0);
          visibility: visible;
        }
        .menu.is-active .icon {
          transform: rotate(180deg);
        }
      `}</style>
    </>
  );
};

export default HeaderDesktop;
