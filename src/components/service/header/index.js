
import { memo } from "react";

import dynamic from "next/dynamic";



const Desktop = dynamic(() => import("./HeaderDesktop"));
const Mobile = dynamic(() => import("./HeaderMobile"));

const Header = (props) => {

  return ( <>
    <Mobile data={props.data} />
    <Desktop data={props.data} />
  </>
  );
};

export default memo(Header);
