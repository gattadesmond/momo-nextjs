import Link from "next/link";
import { appSettings } from "@/configs";
import Image from "next/image";

import classNames from "classnames";

import LogoMomo from "@/components/layout/logoMomo";
import Container from "@/components/Container";
import { getUrlHasRoot } from "@/lib/url";

const FooterContent = ({ menuServices }) => {
  const momo = {
    Name: "Công Ty Cổ Phần Dịch Vụ Di Động Trực Tuyến (viết tắt M_Service)",
    Address:
      "Lầu 6, Toà nhà Phú Mỹ Hưng, số 8 Hoàng Văn Thái, khu phố 1,  Phường Tân Phú, Quận 7, Thành phố Hồ Chí Minh",
  };
  const support = {
    Address:
      "Tầng M, Tòa nhà Petroland, Số 12 Tân Trào, Phường Tân Phú, Quận 7, Thành phố Hồ Chí Minh",
  };
  const listLink = [
    { Name: "Về MoMo", Href: "/gioi-thieu/gioi-thieu-chung#footer" },
    { Name: "Blog", Href: "/lienhe#footer" },
    { Name: "Liên hệ", Href: "/tuyen-dung#footer" },
    { Name: "Tuyển dụng", Href: "/tuyen-dung#footer" },
    { Name: "Điều khoản điều lệ", Href: "/dieu-khoan-dieu-le#footer" },
    {
      Name: "Chính sách quyền riêng tư",
      Href: "/chinh-sach-quyen-rieng-tu#footer",
    },
  ];

  const socialItem = [
    {
      Name: "Facebook",
      Image:
        "https://static.mservice.io/styles/desktop/images/social/facebook.svg",
      Link: "https://www.facebook.com/vimomo",
    },
    {
      Name: "Linkedin",
      Image:
        "https://static.mservice.io/styles/desktop/images/social/linkedin.svg",
      Link: "https://www.linkedin.com/company/momo-mservice/",
    },
    {
      Name: "Youtube",
      Image:
        "https://static.mservice.io/styles/desktop/images/social/youtube.svg",
      Link: "https://www.youtube.com/channel/UCKHHW-qL2JoZqcSNm1jPlqw",
    },
  ];

  const appDownload = [
    {
      Name: "App Store",
      Image:
        "https://static.mservice.io/img/momo-upload-api-210724113855-637627235353131497.jpg",
      Link: "https://itunes.apple.com/vn/app/id918751511?utm_source=website-momo&amp;utm_medium=download&amp;utm_campaign=momo-1dong",
    },
    {
      Name: "Google Play",
      Image:
        "https://static.mservice.io/img/momo-upload-api-210724113959-637627235994410679.jpg",
      Link: "https://play.google.com/store/apps/details?id=com.mservice.momotransfer&utm_source=website-momo&utm_medium=download&utm_campaign=momo-1dong",
    },
  ];

  const services = [
    {
      Name: "Chuyển tiền",
      Href: "/chuyen-tien#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201217140851.svg",
    },
    {
      Name: "Vé xe khách",
      Href: "/ve-xe#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201214120331.svg",
    },
    {
      Name: "Vé tàu hỏa",
      Href: "/ve-tau-hoa#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201214120427.svg",
    },
    {
      Name: "Vé máy bay",
      Href: "/ve-may-bay#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201210145350.svg",
    },
    {
      Name: "Khách sạn",
      Href: "/dat-khach-san#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-210315154805.svg",
    },
    {
      Name: "Trái Tim MoMo",
      Href: "/trai-tim-momo#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201210105714.svg",
    },
    {
      Name: "Heo Đất MoMo",
      Href: "/heo-dat-momo#footer",
      Image:
        "https://static.mservice.io/fileuploads/svg/momo-file-201210105632.svg",
    },
  ];

  return (
    <Container>
      <div className="grid grid-cols-1 gap-x-1 gap-y-0 md:grid-cols-2 lg:grid-cols-4">
        <div className="clear-both mb-4 lg:mb-0">
          <div className="flex">
            <div className="float-left mr-1 md:float-none">
              <LogoMomo />
            </div>
            <div
              className="mb:mb-3 mt-0 mb-0 font-semibold leading-5 text-white lg:mt-1 lg:mb-0"
              style={{ "font-size": "0.85em" }}
            >
              {momo.Name}
            </div>
          </div>
          <div className="mt-2 text-sm leading-5 text-white text-opacity-60">
            {momo.Address}
          </div>
          <div className="mt-5">
            <a
              rel="noopener noreferrer"
              href="http://online.gov.vn/Home/AppDetails/163"
              target="_blank"
            >
              <Image
                alt="chứng nhận"
                src="https://static.mservice.io/blogscontents/momo-upload-api-210629153623-637605777831780706.png"
                width={119}
                height={45}
              />
            </a>
          </div>
        </div>
        <div className="mb-4 hidden text-sm md:block lg:mb-0 lg:block">
          {listLink &&
            listLink.map((item, index) => (
              <div className="mt-1" key={index}>
                <a
                  className="text-white text-opacity-80 hover:text-opacity-100"
                  href={getUrlHasRoot(item.Href)}
                >
                  {item.Name}
                </a>
              </div>
            ))}
        </div>
        <div className="mb-4 text-sm lg:mb-0">
          <h4 className="text-sm uppercase text-white text-opacity-60">
            CHĂM SÓC KHÁCH HÀNG
          </h4>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">
              Địa chỉ:{" "}
            </span>
            <span className="inline text-white text-opacity-80 hover:text-opacity-100">
              {support.Address}
            </span>
          </div>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">
              Hotline :{" "}
            </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`tel:${appSettings.MoMoText.Support.Hotline}`}
            >
              {appSettings.MoMoText.Support.Hotline_Format}
            </a>
            <small className="text-xs italic text-white text-opacity-60">
              {" "}
              (1000 đ/phút)
            </small>
          </div>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">Email : </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`mailto:${appSettings.MoMoText.Support.Email}`}
            >
              {appSettings.MoMoText.Support.Email}
            </a>
          </div>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">
              Tổng đài gọi ra :{" "}
            </span>
            <br />
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`tel:${appSettings.MoMoText.Support.Phone_Ext_1}`}
            >
              {appSettings.MoMoText.Support.Phone_Ext_1_Format}
            </a>
            <span className="text-white text-opacity-60"> - </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`tel:${appSettings.MoMoText.Support.Phone_Ext_2}`}
            >
              {appSettings.MoMoText.Support.Phone_Ext_2_Format}
            </a>
          </div>
          <div className="my-3 inline-block">
            <a
              href="https://momo.vn/huong-dan/huong-dan-gui-yeu-cau-ho-tro-bang-tinh-nang-tro-giup"
              rel="noreferrer"
              target="_blank"
            >
              <div className="relative inline-block overflow-hidden rounded border border-gray-300 bg-black py-1 pl-10 pr-2 ">
                <div
                  className="absolute left-1 top-1 "
                  style={{ paddingTop: "3px" }}
                >
                  <svg
                    className="h-7 w-7 text-gray-100 "
                    fill="currentColor"
                    stroke="currentColor"
                    viewBox="0 0 345.1 512"
                  >
                    <g>
                      <title>Asset 1</title>
                      <path
                        d="M279.4,23.7H30.8C14.5,23.7,0,38.2,0,56.3v401.8c0,16.3,14.5,30.8,30.8,30.8H76h23.8L76,449.4H34.5V96.2h243.1v152l34.5,22
                          V56.3C312,38.2,297.5,23.7,279.4,23.7z M226.8,77.1H86.1c-8.1,0-13.5-5.4-13.5-13.5c0-8.1,5.4-13.5,13.5-13.5h140.8
                          c5.4,0,10.8,5.4,10.8,13.5C237.7,71.7,232.3,77.1,226.8,77.1z"
                      ></path>
                      <path
                        d="M189.4,200.7c-14.4,0-25.9,11.6-25.9,25.9v155.7l-17.3-34.6c-14.2-26.3-28.1-23.6-38.9-17.3c-12.5,8.3-17.2,17-8.6,38.9
                          c19.6,48.2,49.8,105.6,82.2,142.7h116.7c41-30.4,74-175,17.3-181.6c-5.2,0-13.5,0.8-17.3,4.3c0-17.3-15.1-21.7-21.6-21.6
                          c-7.5,0.1-13,4.3-17.3,13c0-17.3-14.1-21.6-21.6-21.6c-8.3,0-17.9,5.2-21.6,13v-90.8C215.4,212.3,203.8,200.7,189.4,200.7z"
                      ></path>
                    </g>
                  </svg>
                </div>
                <div className="text-xs text-white text-opacity-70">
                  Hướng dẫn trợ giúp trên
                </div>
                <div className="text-tiny font-semibold text-white text-opacity-90 ">
                  Ứng dụng Ví MoMo
                </div>
              </div>
            </a>
          </div>
        </div>
        <div className="mb-4 text-sm lg:mb-0">
          <h4 className="text-sm uppercase text-white text-opacity-60">
            HỢP TÁC DOANH NGHIỆP
          </h4>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">
              Hotline :{" "}
            </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`tel:${appSettings.MoMoText.Business.Phome}`}
            >
              {appSettings.MoMoText.Business.Phome_Format}
            </a>
          </div>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">Email : </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href={`mailto:${appSettings.MoMoText.Business.Email}`}
            >
              {appSettings.MoMoText.Business.Email}
            </a>
          </div>
          <div className="mt-1">
            <span className="text-sm text-white text-opacity-60">
              Website :{" "}
            </span>
            <a
              className="inline text-white text-opacity-80 hover:text-opacity-100"
              href="https://business.momo.vn/"
              aria-label="MoMo Business"
              rel="noreferrer"
              target="_blank"
            >
              business.momo.vn
            </a>
          </div>

          <div className="my-3 inline-block">
            <a
              href="https://business.momo.vn/#menutop"
              rel="noreferrer"
              target="_blank"
            >
              <div className="relative inline-block overflow-hidden rounded border border-gray-300 bg-black py-1 pl-12 pr-2 ">
                <div className="absolute left-1 top-1 ">
                  <img
                    src="https://static.mservice.io/fileuploads/svg/momo-file-210724114053.svg"
                    className="w-10  brightness-200 grayscale "
                    alt="Hợp tác doanh nghiệp"
                    width={29}
                    height={20}
                    loading="lazy"
                  />
                </div>
                <div className="text-xs text-white text-opacity-70">
                  Hợp tác doanh nghiệp
                </div>
                <div className="text-tiny font-semibold text-white text-opacity-90 ">
                  Đăng ký hợp tác
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
      {menuServices?.Items?.length > 0 ? (
        <div className="mt-5 overflow-hidden">
          <div className="scroll-hide flex flex-nowrap overflow-x-auto whitespace-nowrap py-2">
            <div className="flex ">
              <span className="flex content-center text-sm text-white text-opacity-80">
                {menuServices.Title}
              </span>
            </div>
            {menuServices.Items.map((item, index) => (
              <div
                key={index}
                className={classNames("flex px-3", {
                  "border-r": index !== menuServices?.Items?.length - 1,
                })}
              >
                <a
                  href={getUrlHasRoot(item.Url)}
                  target="_blank"
                  rel="noreferrer"
                  className="flex content-center text-sm text-white text-opacity-80 hover:text-opacity-100"
                >
                  <Image
                    alt={item.Name}
                    src={item.Icon}
                    layout="fixed"
                    loading="lazy"
                    width="19.5"
                    height="19.5"
                  />
                  <span className="pl-2">{item.Name}</span>
                </a>
              </div>
            ))}
          </div>
        </div>
      ) : (
        <div className="mt-5 overflow-hidden">
          <div className="scroll-hide flex flex-nowrap overflow-x-auto whitespace-nowrap py-2">
            <div className="flex ">
              <span className="flex content-center text-sm text-white text-opacity-80">
                Các dịch vụ nổi bật :
              </span>
            </div>
            {services &&
              services.map((item, index) => (
                <div
                  key={index}
                  className={classNames("flex px-3", {
                    "border-r": index !== services.length - 1,
                  })}
                >
                  <a
                    href={getUrlHasRoot(item.Href)}
                    target="_blank"
                    rel="noreferrer"
                    className="flex content-center text-sm text-white text-opacity-80 hover:text-opacity-100"
                  >
                    <Image
                      alt={item.Name}
                      src={item.Image}
                      layout="fixed"
                      loading="lazy"
                      width="19.5"
                      height="19.5"
                    />
                    <span className="pl-2">{item.Name}</span>
                  </a>
                </div>
              ))}
          </div>
          {/* <div className="wrap__scroll wrap__scroll__service__footer">
          <nav aria-label="breadcrumb" className="breadcrumb-container">
            <ol className="breadcrumb align-items-center wrap__scroll__target">
              <li className="breadcrumb-item">
                <span>Các dịch vụ nổi bật :</span>
              </li>
              <li className="breadcrumb-item">
                <a href="/heo-dat-momo#footer" target="_blank">
                  <img src="https://static.mservice.io/fileuploads/svg/momo-file-201210105632.svg" className="img-fluid img-icon" alt="" />
                                        Heo Đất MoMo
                                    </a>
              </li>

              <li className="d-block" style="height: 1px">
                <span style="width: 30px; display: block"></span>
              </li>
            </ol>
          </nav>

          <div className="wrap__scroll__next">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-arrow-right svg-icon"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>
          </div>
        </div> */}
        </div>
      )}
      <div className="flex flex-col-reverse content-center justify-center border-t border-white border-opacity-10 pt-5 md:flex-row">
        <div className="copyright mb-5 flex w-full grow items-center justify-center text-xs text-white text-opacity-50 md:mb-0 md:w-1/3 md:justify-start">
          ©Copyright M_Service 2020
        </div>
        <div className="mb-5 flex w-full grow items-center justify-center md:mb-0 md:w-1/3">
          <ul className="flex list-none flex-wrap content-center justify-center">
            {socialItem &&
              socialItem.map((item, index) => (
                <li className="inline-block px-2" key={index}>
                  <a
                    href={getUrlHasRoot(item.Link)}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      alt={item.Name}
                      src={item.Image}
                      loading="lazy"
                      width="26"
                      height="26"
                    />
                  </a>
                </li>
              ))}
          </ul>
        </div>
        <div className="mb-5 flex w-full grow items-center justify-center md:mb-0 md:w-1/3 md:justify-end">
          {appDownload &&
            appDownload.map((item, index) => (
              <div
                key={index}
                className={classNames(
                  "flex-initial text-center md:text-right",
                  {
                    "mr-3": index === 0,
                  }
                )}
              >
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={getUrlHasRoot(item.Link)}
                >
                  <Image
                    alt={item.Name}
                    src={item.Image}
                    loading="lazy"
                    width="130"
                    height="39"
                  />
                </a>
              </div>
            ))}
        </div>
      </div>
    </Container>
  );
};

export default FooterContent;
