import { memo } from "react";

import dynamic from "next/dynamic";
import useInView from "react-cool-inview";

const FooterContent = dynamic(() => import("./FooterContent"));

const Footer = ({ menuServices }) => {
  const { observe, inView } = useInView({
    threshold: 0.25,
    onEnter: ({ unobserve }) => unobserve(),
  });

  return (
    <footer
      className="min-h-[834px] bg-gray-800 px-0 py-5 md:min-h-[367px] md:px-5"
      ref={observe}
    >
      {inView && <FooterContent menuServices={menuServices} />}
    </footer>
  );
};

export default memo(Footer);
