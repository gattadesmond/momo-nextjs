import BreadcrumbContainer, { BreadcrumbItem } from "../BreadcrumbContainer";

import { getUrlHasRoot } from "@/lib/url";
const ServiceBreadCrumb = ({ className, data, isStickyMobile = true }) => {
  const menuData = data.Data || null;
  if (!menuData) return;

  return (
    <BreadcrumbContainer
      id={menuData.Id}
      className={`${className} ${isStickyMobile ? "sticky" : "block"} md:sticky z-30 -top-px`}
    >
      {menuData.Items.map((item, index) => (
        <BreadcrumbItem
          key={index}
          icon={item.Icon}
          title={item.Title}
          url={item.Url}
          isActive={menuData.Items.length - 1 === index}
        />
      ))}
    </BreadcrumbContainer>
  )
};

export default ServiceBreadCrumb;