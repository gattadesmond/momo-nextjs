import { useState, useEffect, useMemo, useRef, Fragment } from "react";

import { axios } from "@/lib/index";

import { Dialog } from "@reach/dialog";

import { MoneyFormat } from "@/lib/int-format";

import ReCAPTCHA from "react-google-recaptcha";

import { useForm } from "react-hook-form";

import Loader from "@/components/ui/Loader";

import Confetti from "react-confetti";

const ListCustomer = () => {
  ///page state

  const [result, setResult] = useState(null);

  const [modal, setModal] = useState(false);
  const handleClose = () => {
    setModal(false);
  };

  const recaptchaRef = useRef(null);

  const [loading, setLoading] = useState(false);

  const [captcha, setCaptcha] = useState("");

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    mode: "onBlur", // "onChange"
  });

  const onChangeCaptcha = (value) => {
    setCaptcha(value || null);
  };

  const onSubmit = (data) => {
    if (loading == true) return;

    if (captcha == "") {
      setCaptcha(null);
      return;
    }

    if (captcha == null) return;

    setLoading(true);

    const { userId, cmnd } = data;
    axios
      .get(
        `/lixi/2022/search-player?userId=${userId}&cmnd=${cmnd}&captcha=${captcha}`
      )
      .then(function (response) {
        setLoading(false);
        const res = response;
        const dataResult = res?.Data?.Result;
        const data = res?.Data?.Data || null;
        const dataCode = res?.Data?.Error?.Code;

        if (res.Result == false) {
          if (res.Error.Code == 1) {
            setResult({
              data: "Lỗi hệ thống, bạn vui lòng thử lại",
              code: 100,
            });
          }

          if (res.Error.Code == 2) {
            setResult({
              data: "Lỗi Captcha hết thời hạn, bạn vui lòng thử lại",
              code: 100,
            });
          }

          if (res.Error.Code == 3) {
            setResult({
              data: "Hệ thống đang quá tải, vui lòng thử lại sau",
              code: 100,
            });
          }

          // alert("Xảy ra lỗi, xin bạn vui lòng thử lại");         setLoading(false);
          // return;
        }

        if (dataResult == false) {
          if (dataCode == 502) {
            setResult({
              data: null,
              code: 502,
            });
          }

          if (dataCode == 501) {
            setResult({
              data: null,
              code: 501,
              number: userId,
            });
          }

          if (dataCode == 503) {
            if (data.currentChallenge < 5) {
              setResult({
                data: data,
                code: 504,
              });
            } else {
              setResult({
                data: data,
                code: 503,
                message: res?.Data?.Error?.Message,
              });
            }
          }

          if (dataCode == 504) {
            setResult({
              data: data,
              code: 504,
            });
          }
        }

        if (dataResult == true) {
          setResult({
            data: data,
            code: 200,
          });
        }

        setModal(true);

        recaptchaRef.current.reset();
        setCaptcha(null);
      })
      .catch(function (error) {
        setLoading(false);
      });
  };
  const ResultModal = ({ isOpen, onDismiss }) => {
    return (
      <Dialog
        isOpen={isOpen}
        onDismiss={onDismiss}
        className="modal fixed inset-0 flex max-h-full flex-row items-center justify-center bg-transparent p-3 text-gray-800  md:p-10 "
        aria-labelledby="modal-title"
        role="dialog"
        aria-modal="true"
      >
        <div className="modalFadeUp relative z-10   h-auto w-full    max-w-2xl rounded-xl bg-gradient-to-b from-[#B72121] to-[#800404] shadow-xl ">
          <button
            className={`absolute top-2 left-auto right-2 z-20 flex h-12 w-12  cursor-pointer items-center justify-center rounded-full text-white text-opacity-70 hover:bg-gray-50 hover:bg-opacity-20`}
            onClick={onDismiss}
          >
            <img
              src="https://static.mservice.io/fileuploads/svg/momo-file-220108135005.svg"
              className="block w-8"
            />
          </button>

          <div
            className="absolute inset-0 -z-10 mix-blend-overlay "
            style={{
              backgroundImage:
                "url(https://static.mservice.io/img/momo-upload-api-220217015503-637806597036398858.png)",
            }}
          ></div>
          <img
            src="https://static.mservice.io/img/momo-upload-api-220217020630-637806603905333794.png"
            loading="lazy"
            className=" absolute -left-3 top-2 w-32 md:left-0 md:w-40"
          />

          <div className="p-4 md:p-7">
            {result && result.code == 0 ? (
              <img
                src="https://static.mservice.io/img/momo-upload-api-220217023801-637806622810987398.png
                "
                loading="lazy"
                className=" mx-auto block "
                width={176}
                height={176}
              />
            ) : (
              <img
                src="https://static.mservice.io/img/momo-upload-api-220217020935-637806605758597004.png"
                loading="lazy"
                className=" mx-auto block "
                width={176}
                height={175}
              />
            )}

            {result && result.code == 502 && (
              <div className="pb-4 text-center">
                <div className="font-lx heading-lx-2 mt-2 text-lg text-white/90">
                  Thông tin CMND/CCCD/Hộ chiếu không chính xác, <br /> vui lòng kiểm tra
                  lại.
                </div>
              </div>
            )}

            {result && result.code == 501 && (
              <div className="pb-4 text-center">
            

                <div className="font-lx heading-lx-2 mt-2 text-lg text-white/90">
                  Không tìm thấy kết quả cho số điện thoại{" "}
                </div>

                <div className="text-lx font-lx heading-lx-2 mt-1 text-xl md:text-2xl">
                  {result.number || "này"}
                </div>
              </div>
            )}

            {result && result.code == 504 && (
              <div className="pb-4 text-center">
                <div className="text-lx font-lx heading-lx-2 text-xl md:text-2xl">
                  {result.data.username}
                </div>
                <div className="font-lx heading-lx-2 text-lg text-white/90">
                  ({result.data.userId})
                </div>

                <div className="heading-lx-2 font-lx mt-4  text-lg text-white/90">
                  Bạn chưa vượt qua <strong>5 thử thách thần tài!</strong>
                </div>
              </div>
            )}

            {result && result.code == 503 && (
              <div className="pb-4 text-center">
                <div className="text-lx font-lx heading-lx-2 text-xl md:text-2xl">
                  {result.data.username}
                </div>
                <div className="font-lx heading-lx-2 text-lg text-white/90">
                  ({result.data.userId})
                </div>
                {result.message && (
                  <div className="heading-lx-2 font-lx mt-4  text-lg text-white/90">
                    {result.message}
                  </div>
                )}

                {/* <div className="heading-lx-2 font-lx mt-4  text-lg text-white/90">
                  Bạn không đủ điều kiện nhận thưởng từ{" "}
                  <strong>Thử thách thần tài </strong>
                  {result.message && (
                    <>
                      vì{" "}
                      <strong className="block text-yellow-200">
                        {result.message}
                      </strong>{" "}
                    </>
                  )}
                </div> */}
              </div>
            )}

            {result && result.code == 200 && (
              <div className="relative pb-4 text-center">
                <div className="confetti absolute top-1/2 left-1/2">
                  <Confetti
                    width="400px"
                    height="400px"
                    recycle={false}
                    numberOfPieces={20}
                    initialVelocityX={5}
                    initialVelocityY={10}
                    confettiSource={{ x: 150, y: 150, w: 100, h: 100 }}
                    colors={[
                      "#EF8E99",
                      "#F4B43F",
                      "#458B8B",
                      "#a50064",
                      "#8a0000",
                      "#a10203",
                    ]}
                  />
                </div>

                <div className="text-lx font-lx heading-lx-2 text-xl md:text-2xl">
                  {result.data.username}
                </div>
                <div className="font-lx heading-lx-2 text-lg text-white/90">
                  ({result.data.userId})
                </div>
                <div className="font-lx pt-4 pb-5 text-lg text-white/90">
                  Chúc mừng! Bạn đã vượt qua <br /> 5 Thử thách thần tài và nhận
                  được{" "}
                </div>

                <div className=" font-lx heading-lx-2 text-3xl text-white">
                  <strong>{MoneyFormat(result.data.balanceMoney)}</strong> xu{" "}
                </div>
              </div>
            )}

            {result && result.code == 100 && (
              <div className="pb-4 text-center">
                <div className="text-lx font-lx heading-lx-2 text-xl md:text-2xl">
                  {result.data}
                </div>
              </div>
            )}

            {!result && (
              <div className="font-lx heading-lx-2 text-center text-lg text-white/90">
                Có lỗi xảy ra, bạn vui lòng thử lại nhé.
              </div>
            )}
          </div>
        </div>

        <div
          className="modal-overlay fixed inset-0 overflow-y-auto bg-black bg-opacity-50"
          onClick={onDismiss}
        ></div>
      </Dialog>
    );
  };

  return (
    <>
      <div className=" box-lx relative mx-auto mt-10 max-w-4xl rounded-md p-6 sm:px-10">
        <div className="grid grid-cols-1 items-center gap-8 sm:grid-cols-2 sm:gap-10">
          <div className=" max-w-md  ">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="mb-5">
                <label
                  htmlFor="sodienthoai"
                  className="mb-1 block  font-bold text-gray-900"
                >
                  Số điện thoại
                </label>
                <input
                  type="text"
                  id="sodienthoai"
                  autoComplete="off"
                  inputMode="numeric"
                  maxLength="11"
                  className="input-lx block w-full  rounded-lg p-2.5 px-4 text-lg "
                  placeholder="Nhập số điện thoại của bạn"
                  {...register("userId", {
                    required: "Vui lòng nhập số điện thoại của bạn",
                    pattern: {
                      value: /^\+?[0-9]{10,11}$/,
                      message: "Vui lòng nhập số điện thoại 10 hoặc 11 số",
                    },
                  })}
                />

                {errors.userId && (
                  <span className="text-sm italic text-red-600">
                    {errors.userId.message}
                  </span>
                )}
              </div>

              <div className="mb-5 ">
                <label
                  htmlFor="socuoi"
                  className="mb-1 block font-bold text-gray-900 "
                >
                  Nhập 3 số cuối CMND/CCCD/Hộ chiếu liên kết Ví MoMo
                </label>
                <input
                  type="text"
                  id="socuoi"
                  autoComplete="off"
                  inputMode="numeric"
                  maxLength="3"
                  className="input-lx block w-full  max-w-[200px] rounded-lg p-2.5 px-4 text-lg "
                  placeholder="Nhập 3 số cuối"
                  {...register("cmnd", {
                    required:
                      "Vui lòng nhập 3 số cuối CMND/CCCD/Hộ chiếu của bạn để xác thực",
                    pattern: {
                      value: /^\+?[0-9]{3}$/,
                      message: "Số CMND/CCCD/Hộ chiếu chiếu không hợp lệ.",
                    },
                  })}
                />

                {errors.cmnd && (
                  <span className="text-sm italic text-red-600">
                    {errors.cmnd.message}
                  </span>
                )}
              </div>

              <div className="mb-6">
                {/* 6LeIpIEeAAAAADRZ73qloSgVoUF8nvJE7ML7HPXj */}
                {/* 6LdYldkUAAAAAJ2S95ckvgvdEtxidBCRgTM9nJtl */}
                <ReCAPTCHA
                  sitekey="6LejXIMeAAAAAKvXH3WIoST0qD6pHbo0kLGySyIX"
                  ref={recaptchaRef}
                  onChange={onChangeCaptcha}
                />

                {captcha == null && (
                  <span className="text-sm italic text-red-600">
                    Vui lòng chọn Tôi không phải là người máy
                  </span>
                )}
              </div>

              <button
                type="submit"
                disabled={loading}
                className=" font-lx bg-grad-1 w-full rounded-md px-6 py-2.5 text-lg text-yellow-100 hover:opacity-80"
              >
                TÌM KIẾM
              </button>
            </form>
          </div>

          <div className="">
            <img
              src="https://static.mservice.io/img/momo-upload-api-220216161403-637806248435886450.png"
              className="mx-auto block px-6 sm:px-0"
              loading="lazy"
            />
          </div>
        </div>

        {loading && (
          <div className="z-12 fadeIn absolute top-0 left-0 flex h-full w-full items-center justify-center rounded-md bg-black bg-opacity-30">
            <Loader />
          </div>
        )}
      </div>

      <ResultModal isOpen={modal} onDismiss={handleClose} />

      <style jsx>{`
        .input-lx {
          border-radius: 8px;
          box-shadow: inset 0 1px 3px 1.5px rgba(177, 106, 95, 0.48);
          border: solid 1px #fcc;
          background-color: #fff;
          color: #8a0000;
          font-weight: bold;
          letter-spacing: 0.05rem;
        }
        .input-lx::placeholder {
          font-weight: normal;
          letter-spacing: 0;
        }
      `}</style>
    </>
  );
};

export default ListCustomer;
