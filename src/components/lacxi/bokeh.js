import { useState, useMemo, useCallback, useEffect } from "react";

export default function Bokeh({ data, title }) {
  return (
    <div className="cs hidden md:block">
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>
      <div className="c"></div>

      <style jsx global>
        {`
          .cs{
            user-select: none;
            pointer-events: none;
            position: absolute;
            top:0;
            left: 0;
            right: 0;
            bottom: 0;
          }
          .c {
            position: absolute;
            border-radius: 50%;
            box-shadow: 0 0 10px #fff;
            mix-blend-mode: overlay;
          }
          .c:nth-child(1) {
            background: rgba(255, 255, 255, 0.4);
            top: 35%;
            left: 71%;
            width: 51px;
            height: 51px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float1 17s infinite linear;
          }
          .c:nth-child(2) {
            background: rgba(255, 255, 255, 0.4);
            top: 66%;
            left: 38%;
            width: 25px;
            height: 25px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float2 17s infinite linear;
          }
          .c:nth-child(3) {
            background: rgba(255, 255, 255, 0.4);
            top: 33%;
            left: 22%;
            width: 37px;
            height: 37px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float3 17s infinite linear;
          }
          .c:nth-child(4) {
            background: rgba(255, 255, 255, 0.4);
            top: 70%;
            left: 42%;
            width: 47px;
            height: 47px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float4 17s infinite linear;
          }
          .c:nth-child(5) {
            background: rgba(255, 255, 255, 0.4);
            top: 22%;
            left: 83%;
            width: 36px;
            height: 36px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float5 17s infinite linear;
          }
          .c:nth-child(6) {
            background: rgba(255, 255, 255, 0.4);
            top: 49%;
            left: 5%;
            width: 58px;
            height: 58px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float6 17s infinite linear;
          }
          .c:nth-child(7) {
            background: rgba(255, 255, 255, 0.4);
            top: 68%;
            left: 32%;
            width: 42px;
            height: 42px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float7 17s infinite linear;
          }
          .c:nth-child(8) {
            background: rgba(255, 255, 255, 0.4);
            top: 38%;
            left: 76%;
            width: 8px;
            height: 8px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float8 17s infinite linear;
          }
          .c:nth-child(9) {
            background: rgba(255, 255, 255, 0.4);
            top: 13%;
            left: 22%;
            width: 46px;
            height: 46px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float9 17s infinite linear;
          }
          .c:nth-child(10) {
            background: rgba(255, 255, 255, 0.4);
            top: 96%;
            left: 88%;
            width: 31px;
            height: 31px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float10 17s infinite linear;
          }
          .c:nth-child(11) {
            background: rgba(255, 255, 255, 0.4);
            top: 6%;
            left: 32%;
            width: 40px;
            height: 40px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float11 17s infinite linear;
          }
          .c:nth-child(12) {
            background: rgba(255, 255, 255, 0.4);
            top: 58%;
            left: 82%;
            width: 45px;
            height: 45px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float12 17s infinite linear;
          }
          .c:nth-child(13) {
            background: rgba(255, 255, 255, 0.4);
            top: 39%;
            left: 56%;
            width: 20px;
            height: 20px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float13 17s infinite linear;
          }
          .c:nth-child(14) {
            background: rgba(255, 255, 255, 0.4);
            top: 90%;
            left: 22%;
            width: 55px;
            height: 55px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float14 17s infinite linear;
          }
          .c:nth-child(15) {
            background: rgba(255, 255, 255, 0.4);
            top: 7%;
            left: 50%;
            width: 45px;
            height: 45px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float15 17s infinite linear;
          }
          .c:nth-child(16) {
            background: rgba(255, 255, 255, 0.4);
            top: 16%;
            left: 39%;
            width: 39px;
            height: 39px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float16 17s infinite linear;
          }
          .c:nth-child(17) {
            background: rgba(255, 255, 255, 0.4);
            top: 95%;
            left: 70%;
            width: 34px;
            height: 34px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float17 17s infinite linear;
          }
          .c:nth-child(18) {
            background: rgba(255, 255, 255, 0.4);
            top: 8%;
            left: 86%;
            width: 2px;
            height: 2px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float18 17s infinite linear;
          }
          .c:nth-child(19) {
            background: rgba(255, 255, 255, 0.4);
            top: 58%;
            left: 19%;
            width: 60px;
            height: 60px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float19 17s infinite linear;
          }
          .c:nth-child(20) {
            background: rgba(255, 255, 255, 0.4);
            top: 63%;
            left: 11%;
            width: 12px;
            height: 12px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float20 17s infinite linear;
          }
          .c:nth-child(21) {
            background: rgba(255, 255, 255, 0.4);
            top: 30%;
            left: 88%;
            width: 33px;
            height: 33px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float21 17s infinite linear;
          }
          .c:nth-child(22) {
            background: rgba(255, 255, 255, 0.4);
            top: 17%;
            left: 68%;
            width: 2px;
            height: 2px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float22 17s infinite linear;
          }
          .c:nth-child(23) {
            background: rgba(255, 255, 255, 0.4);
            top: 68%;
            left: 53%;
            width: 52px;
            height: 52px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float23 17s infinite linear;
          }
          .c:nth-child(24) {
            background: rgba(255, 255, 255, 0.4);
            top: 84%;
            left: 93%;
            width: 40px;
            height: 40px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float24 17s infinite linear;
          }
          .c:nth-child(25) {
            background: rgba(255, 255, 255, 0.4);
            top: 69%;
            left: 2%;
            width: 49px;
            height: 49px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float25 17s infinite linear;
          }
          .c:nth-child(26) {
            background: rgba(255, 255, 255, 0.4);
            top: 1%;
            left: 47%;
            width: 46px;
            height: 46px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float26 17s infinite linear;
          }
          .c:nth-child(27) {
            background: rgba(255, 255, 255, 0.4);
            top: 76%;
            left: 51%;
            width: 50px;
            height: 50px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float27 17s infinite linear;
          }
          .c:nth-child(28) {
            background: rgba(255, 255, 255, 0.4);
            top: 88%;
            left: 36%;
            width: 43px;
            height: 43px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float28 17s infinite linear;
          }
          .c:nth-child(29) {
            background: rgba(255, 255, 255, 0.4);
            top: 71%;
            left: 49%;
            width: 55px;
            height: 55px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float29 17s infinite linear;
          }
          .c:nth-child(30) {
            background: rgba(255, 255, 255, 0.4);
            top: 51%;
            left: 53%;
            width: 35px;
            height: 35px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float30 17s infinite linear;
          }
          .c:nth-child(31) {
            background: rgba(255, 255, 255, 0.4);
            top: 44%;
            left: 27%;
            width: 11px;
            height: 11px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float31 17s infinite linear;
          }
          .c:nth-child(32) {
            background: rgba(255, 255, 255, 0.4);
            top: 50%;
            left: 13%;
            width: 38px;
            height: 38px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float32 17s infinite linear;
          }
          .c:nth-child(33) {
            background: rgba(255, 255, 255, 0.4);
            top: 66%;
            left: 38%;
            width: 15px;
            height: 15px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float33 17s infinite linear;
          }
          .c:nth-child(34) {
            background: rgba(255, 255, 255, 0.4);
            top: 68%;
            left: 30%;
            width: 29px;
            height: 29px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float34 17s infinite linear;
          }
          .c:nth-child(35) {
            background: rgba(255, 255, 255, 0.4);
            top: 69%;
            left: 23%;
            width: 31px;
            height: 31px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float35 17s infinite linear;
          }
          .c:nth-child(36) {
            background: rgba(255, 255, 255, 0.4);
            top: 63%;
            left: 33%;
            width: 40px;
            height: 40px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float36 17s infinite linear;
          }
          .c:nth-child(37) {
            background: rgba(255, 255, 255, 0.4);
            top: 42%;
            left: 37%;
            width: 1px;
            height: 1px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float37 17s infinite linear;
          }
          .c:nth-child(38) {
            background: rgba(255, 255, 255, 0.4);
            top: 96%;
            left: 83%;
            width: 32px;
            height: 32px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float38 17s infinite linear;
          }
          .c:nth-child(39) {
            background: rgba(255, 255, 255, 0.4);
            top: 2%;
            left: 14%;
            width: 53px;
            height: 53px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float39 17s infinite linear;
          }
          .c:nth-child(40) {
            background: rgba(255, 255, 255, 0.4);
            top: 66%;
            left: 87%;
            width: 36px;
            height: 36px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float40 17s infinite linear;
          }
          .c:nth-child(41) {
            background: rgba(255, 255, 255, 0.4);
            top: 59%;
            left: 74%;
            width: 28px;
            height: 28px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float41 17s infinite linear;
          }
          .c:nth-child(42) {
            background: rgba(255, 255, 255, 0.4);
            top: 57%;
            left: 41%;
            width: 6px;
            height: 6px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float42 17s infinite linear;
          }
          .c:nth-child(43) {
            background: rgba(255, 255, 255, 0.4);
            top: 9%;
            left: 35%;
            width: 49px;
            height: 49px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float43 17s infinite linear;
          }
          .c:nth-child(44) {
            background: rgba(255, 255, 255, 0.4);
            top: 85%;
            left: 98%;
            width: 25px;
            height: 25px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float44 17s infinite linear;
          }
          .c:nth-child(45) {
            background: rgba(255, 255, 255, 0.4);
            top: 33%;
            left: 59%;
            width: 4px;
            height: 4px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float45 17s infinite linear;
          }
          .c:nth-child(46) {
            background: rgba(255, 255, 255, 0.4);
            top: 72%;
            left: 92%;
            width: 26px;
            height: 26px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float46 17s infinite linear;
          }
          .c:nth-child(47) {
            background: rgba(255, 255, 255, 0.4);
            top: 81%;
            left: 86%;
            width: 6px;
            height: 6px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float47 17s infinite linear;
          }
          .c:nth-child(48) {
            background: rgba(255, 255, 255, 0.4);
            top: 19%;
            left: 39%;
            width: 5px;
            height: 5px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float48 17s infinite linear;
          }
          .c:nth-child(49) {
            background: rgba(255, 255, 255, 0.4);
            top: 74%;
            left: 43%;
            width: 30px;
            height: 30px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float49 17s infinite linear;
          }
          .c:nth-child(50) {
            background: rgba(255, 255, 255, 0.4);
            top: 48%;
            left: 58%;
            width: 6px;
            height: 6px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float50 17s infinite linear;
          }
          .c:nth-child(51) {
            background: rgba(255, 255, 255, 0.4);
            top: 79%;
            left: 42%;
            width: 18px;
            height: 18px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float51 17s infinite linear;
          }
          .c:nth-child(52) {
            background: rgba(255, 255, 255, 0.4);
            top: 51%;
            left: 13%;
            width: 23px;
            height: 23px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float52 17s infinite linear;
          }
          .c:nth-child(53) {
            background: rgba(255, 255, 255, 0.4);
            top: 26%;
            left: 72%;
            width: 48px;
            height: 48px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float53 17s infinite linear;
          }
          .c:nth-child(54) {
            background: rgba(255, 255, 255, 0.4);
            top: 68%;
            left: 75%;
            width: 55px;
            height: 55px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float54 17s infinite linear;
          }
          .c:nth-child(55) {
            background: rgba(255, 255, 255, 0.4);
            top: 3%;
            left: 24%;
            width: 42px;
            height: 42px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float55 17s infinite linear;
          }
          .c:nth-child(56) {
            background: rgba(255, 255, 255, 0.4);
            top: 72%;
            left: 84%;
            width: 29px;
            height: 29px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float56 17s infinite linear;
          }
          .c:nth-child(57) {
            background: rgba(255, 255, 255, 0.4);
            top: 74%;
            left: 70%;
            width: 47px;
            height: 47px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float57 17s infinite linear;
          }
          .c:nth-child(58) {
            background: rgba(255, 255, 255, 0.4);
            top: 26%;
            left: 32%;
            width: 14px;
            height: 14px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float58 17s infinite linear;
          }
          .c:nth-child(59) {
            background: rgba(255, 255, 255, 0.4);
            top: 26%;
            left: 74%;
            width: 7px;
            height: 7px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float59 17s infinite linear;
          }
          .c:nth-child(60) {
            background: rgba(255, 255, 255, 0.4);
            top: 80%;
            left: 98%;
            width: 10px;
            height: 10px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float60 17s infinite linear;
          }
          .c:nth-child(61) {
            background: rgba(255, 255, 255, 0.4);
            top: 85%;
            left: 38%;
            width: 25px;
            height: 25px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float61 17s infinite linear;
          }
          .c:nth-child(62) {
            background: rgba(255, 255, 255, 0.4);
            top: 93%;
            left: 5%;
            width: 46px;
            height: 46px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float62 17s infinite linear;
          }
          .c:nth-child(63) {
            background: rgba(255, 255, 255, 0.4);
            top: 38%;
            left: 95%;
            width: 20px;
            height: 20px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float63 17s infinite linear;
          }
          .c:nth-child(64) {
            background: rgba(255, 255, 255, 0.4);
            top: 67%;
            left: 1%;
            width: 21px;
            height: 21px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float64 17s infinite linear;
          }
          .c:nth-child(65) {
            background: rgba(255, 255, 255, 0.4);
            top: 39%;
            left: 42%;
            width: 57px;
            height: 57px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float65 17s infinite linear;
          }
          .c:nth-child(66) {
            background: rgba(255, 255, 255, 0.4);
            top: 87%;
            left: 26%;
            width: 43px;
            height: 43px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float66 17s infinite linear;
          }
          .c:nth-child(67) {
            background: rgba(255, 255, 255, 0.4);
            top: 71%;
            left: 35%;
            width: 51px;
            height: 51px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float67 17s infinite linear;
          }
          .c:nth-child(68) {
            background: rgba(255, 255, 255, 0.4);
            top: 72%;
            left: 30%;
            width: 41px;
            height: 41px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float68 17s infinite linear;
          }
          .c:nth-child(69) {
            background: rgba(255, 255, 255, 0.4);
            top: 19%;
            left: 92%;
            width: 15px;
            height: 15px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float69 17s infinite linear;
          }
          .c:nth-child(70) {
            background: rgba(255, 255, 255, 0.4);
            top: 76%;
            left: 85%;
            width: 29px;
            height: 29px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float70 17s infinite linear;
          }
          .c:nth-child(71) {
            background: rgba(255, 255, 255, 0.4);
            top: 84%;
            left: 71%;
            width: 13px;
            height: 13px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float71 17s infinite linear;
          }
          .c:nth-child(72) {
            background: rgba(255, 255, 255, 0.4);
            top: 42%;
            left: 79%;
            width: 15px;
            height: 15px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float72 17s infinite linear;
          }
          .c:nth-child(73) {
            background: rgba(255, 255, 255, 0.4);
            top: 80%;
            left: 21%;
            width: 18px;
            height: 18px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float73 17s infinite linear;
          }
          .c:nth-child(74) {
            background: rgba(255, 255, 255, 0.4);
            top: 19%;
            left: 34%;
            width: 12px;
            height: 12px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float74 17s infinite linear;
          }
          .c:nth-child(75) {
            background: rgba(255, 255, 255, 0.4);
            top: 51%;
            left: 83%;
            width: 58px;
            height: 58px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float75 17s infinite linear;
          }
          .c:nth-child(76) {
            background: rgba(255, 255, 255, 0.4);
            top: 53%;
            left: 88%;
            width: 36px;
            height: 36px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float76 17s infinite linear;
          }
          .c:nth-child(77) {
            background: rgba(255, 255, 255, 0.4);
            top: 67%;
            left: 32%;
            width: 58px;
            height: 58px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float77 17s infinite linear;
          }
          .c:nth-child(78) {
            background: rgba(255, 255, 255, 0.4);
            top: 73%;
            left: 27%;
            width: 14px;
            height: 14px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float78 17s infinite linear;
          }
          .c:nth-child(79) {
            background: rgba(255, 255, 255, 0.4);
            top: 100%;
            left: 72%;
            width: 49px;
            height: 49px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float79 17s infinite linear;
          }
          .c:nth-child(80) {
            background: rgba(255, 255, 255, 0.4);
            top: 29%;
            left: 94%;
            width: 59px;
            height: 59px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float80 17s infinite linear;
          }
          .c:nth-child(81) {
            background: rgba(255, 255, 255, 0.4);
            top: 20%;
            left: 85%;
            width: 20px;
            height: 20px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float81 17s infinite linear;
          }
          .c:nth-child(82) {
            background: rgba(255, 255, 255, 0.4);
            top: 84%;
            left: 27%;
            width: 60px;
            height: 60px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float82 17s infinite linear;
          }
          .c:nth-child(83) {
            background: rgba(255, 255, 255, 0.4);
            top: 76%;
            left: 61%;
            width: 19px;
            height: 19px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float83 17s infinite linear;
          }
          .c:nth-child(84) {
            background: rgba(255, 255, 255, 0.4);
            top: 63%;
            left: 85%;
            width: 46px;
            height: 46px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float84 17s infinite linear;
          }
          .c:nth-child(85) {
            background: rgba(255, 255, 255, 0.4);
            top: 82%;
            left: 86%;
            width: 46px;
            height: 46px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float85 17s infinite linear;
          }
          .c:nth-child(86) {
            background: rgba(255, 255, 255, 0.4);
            top: 84%;
            left: 9%;
            width: 2px;
            height: 2px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float86 17s infinite linear;
          }
          .c:nth-child(87) {
            background: rgba(255, 255, 255, 0.4);
            top: 11%;
            left: 76%;
            width: 24px;
            height: 24px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float87 17s infinite linear;
          }
          .c:nth-child(88) {
            background: rgba(255, 255, 255, 0.4);
            top: 22%;
            left: 69%;
            width: 43px;
            height: 43px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float88 17s infinite linear;
          }
          .c:nth-child(89) {
            background: rgba(255, 255, 255, 0.4);
            top: 34%;
            left: 73%;
            width: 42px;
            height: 42px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float89 17s infinite linear;
          }
          .c:nth-child(90) {
            background: rgba(255, 255, 255, 0.4);
            top: 9%;
            left: 35%;
            width: 28px;
            height: 28px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float90 17s infinite linear;
          }
          .c:nth-child(91) {
            background: rgba(255, 255, 255, 0.4);
            top: 16%;
            left: 40%;
            width: 18px;
            height: 18px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float91 17s infinite linear;
          }
          .c:nth-child(92) {
            background: rgba(255, 255, 255, 0.4);
            top: 79%;
            left: 59%;
            width: 45px;
            height: 45px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float92 17s infinite linear;
          }
          .c:nth-child(93) {
            background: rgba(255, 255, 255, 0.4);
            top: 66%;
            left: 71%;
            width: 21px;
            height: 21px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float93 17s infinite linear;
          }
          .c:nth-child(94) {
            background: rgba(255, 255, 255, 0.4);
            top: 70%;
            left: 41%;
            width: 8px;
            height: 8px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float94 17s infinite linear;
          }
          .c:nth-child(95) {
            background: rgba(255, 255, 255, 0.4);
            top: 58%;
            left: 89%;
            width: 34px;
            height: 34px;
            opacity: calc(0.3 + random(100) / 100);
            animation: float95 17s infinite linear;
          }
          @keyframes float1 {
            40% {
              transform: translateX(1px) translateY(27px);
            }
            80% {
              transform: translateX(58px) translateY(-18px);
            }
          }
          @keyframes float2 {
            40% {
              transform: translateX(79px) translateY(45px);
            }
            80% {
              transform: translateX(-7px) translateY(242px);
            }
          }
          @keyframes float3 {
            40% {
              transform: translateX(32px) translateY(-13px);
            }
            80% {
              transform: translateX(-27px) translateY(193px);
            }
          }
          @keyframes float4 {
            40% {
              transform: translateX(-19px) translateY(-161px);
            }
            80% {
              transform: translateX(143px) translateY(-19px);
            }
          }
          @keyframes float5 {
            40% {
              transform: translateX(54px) translateY(-78px);
            }
            80% {
              transform: translateX(-40px) translateY(108px);
            }
          }
          @keyframes float6 {
            40% {
              transform: translateX(27px) translateY(76px);
            }
            80% {
              transform: translateX(-38px) translateY(79px);
            }
          }
          @keyframes float7 {
            40% {
              transform: translateX(12px) translateY(-82px);
            }
            80% {
              transform: translateX(111px) translateY(206px);
            }
          }
          @keyframes float8 {
            40% {
              transform: translateX(31px) translateY(-30px);
            }
            80% {
              transform: translateX(70px) translateY(129px);
            }
          }
          @keyframes float9 {
            40% {
              transform: translateX(53px) translateY(-50px);
            }
            80% {
              transform: translateX(43px) translateY(-47px);
            }
          }
          @keyframes float10 {
            40% {
              transform: translateX(-4px) translateY(-125px);
            }
            80% {
              transform: translateX(69px) translateY(69px);
            }
          }
          @keyframes float11 {
            40% {
              transform: translateX(23px) translateY(87px);
            }
            80% {
              transform: translateX(98px) translateY(111px);
            }
          }
          @keyframes float12 {
            40% {
              transform: translateX(1px) translateY(-50px);
            }
            80% {
              transform: translateX(-8px) translateY(-3px);
            }
          }
          @keyframes float13 {
            40% {
              transform: translateX(-11px) translateY(-55px);
            }
            80% {
              transform: translateX(126px) translateY(21px);
            }
          }
          @keyframes float14 {
            40% {
              transform: translateX(34px) translateY(-123px);
            }
            80% {
              transform: translateX(120px) translateY(291px);
            }
          }
          @keyframes float15 {
            40% {
              transform: translateX(-6px) translateY(74px);
            }
            80% {
              transform: translateX(-41px) translateY(-18px);
            }
          }
          @keyframes float16 {
            40% {
              transform: translateX(-2px) translateY(-85px);
            }
            80% {
              transform: translateX(-19px) translateY(207px);
            }
          }
          @keyframes float17 {
            40% {
              transform: translateX(50px) translateY(15px);
            }
            80% {
              transform: translateX(57px) translateY(-61px);
            }
          }
          @keyframes float18 {
            40% {
              transform: translateX(68px) translateY(50px);
            }
            80% {
              transform: translateX(7px) translateY(58px);
            }
          }
          @keyframes float19 {
            40% {
              transform: translateX(54px) translateY(-208px);
            }
            80% {
              transform: translateX(-7px) translateY(117px);
            }
          }
          @keyframes float20 {
            40% {
              transform: translateX(73px) translateY(-72px);
            }
            80% {
              transform: translateX(34px) translateY(194px);
            }
          }
          @keyframes float21 {
            40% {
              transform: translateX(78px) translateY(64px);
            }
            80% {
              transform: translateX(10px) translateY(15px);
            }
          }
          @keyframes float22 {
            40% {
              transform: translateX(47px) translateY(96px);
            }
            80% {
              transform: translateX(136px) translateY(154px);
            }
          }
          @keyframes float23 {
            40% {
              transform: translateX(65px) translateY(-143px);
            }
            80% {
              transform: translateX(110px) translateY(38px);
            }
          }
          @keyframes float24 {
            40% {
              transform: translateX(38px) translateY(84px);
            }
            80% {
              transform: translateX(10px) translateY(177px);
            }
          }
          @keyframes float25 {
            40% {
              transform: translateX(59px) translateY(-92px);
            }
            80% {
              transform: translateX(92px) translateY(58px);
            }
          }
          @keyframes float26 {
            40% {
              transform: translateX(56px) translateY(-8px);
            }
            80% {
              transform: translateX(-3px) translateY(262px);
            }
          }
          @keyframes float27 {
            40% {
              transform: translateX(7px) translateY(6px);
            }
            80% {
              transform: translateX(117px) translateY(-67px);
            }
          }
          @keyframes float28 {
            40% {
              transform: translateX(40px) translateY(-191px);
            }
            80% {
              transform: translateX(-16px) translateY(5px);
            }
          }
          @keyframes float29 {
            40% {
              transform: translateX(24px) translateY(-79px);
            }
            80% {
              transform: translateX(75px) translateY(248px);
            }
          }
          @keyframes float30 {
            40% {
              transform: translateX(46px) translateY(-172px);
            }
            80% {
              transform: translateX(118px) translateY(-73px);
            }
          }
          @keyframes float31 {
            40% {
              transform: translateX(41px) translateY(72px);
            }
            80% {
              transform: translateX(150px) translateY(39px);
            }
          }
          @keyframes float32 {
            40% {
              transform: translateX(32px) translateY(-118px);
            }
            80% {
              transform: translateX(117px) translateY(117px);
            }
          }
          @keyframes float33 {
            40% {
              transform: translateX(-8px) translateY(18px);
            }
            80% {
              transform: translateX(97px) translateY(27px);
            }
          }
          @keyframes float34 {
            40% {
              transform: translateX(4px) translateY(-76px);
            }
            80% {
              transform: translateX(107px) translateY(-49px);
            }
          }
          @keyframes float35 {
            40% {
              transform: translateX(50px) translateY(-176px);
            }
            80% {
              transform: translateX(-25px) translateY(-3px);
            }
          }
          @keyframes float36 {
            40% {
              transform: translateX(77px) translateY(-53px);
            }
            80% {
              transform: translateX(52px) translateY(210px);
            }
          }
          @keyframes float37 {
            40% {
              transform: translateX(9px) translateY(-31px);
            }
            80% {
              transform: translateX(105px) translateY(-87px);
            }
          }
          @keyframes float38 {
            40% {
              transform: translateX(30px) translateY(-281px);
            }
            80% {
              transform: translateX(-6px) translateY(209px);
            }
          }
          @keyframes float39 {
            40% {
              transform: translateX(-11px) translateY(-257px);
            }
            80% {
              transform: translateX(-19px) translateY(264px);
            }
          }
          @keyframes float40 {
            40% {
              transform: translateX(45px) translateY(-78px);
            }
            80% {
              transform: translateX(133px) translateY(13px);
            }
          }
          @keyframes float41 {
            40% {
              transform: translateX(78px) translateY(-138px);
            }
            80% {
              transform: translateX(114px) translateY(229px);
            }
          }
          @keyframes float42 {
            40% {
              transform: translateX(16px) translateY(-234px);
            }
            80% {
              transform: translateX(85px) translateY(36px);
            }
          }
          @keyframes float43 {
            40% {
              transform: translateX(53px) translateY(-259px);
            }
            80% {
              transform: translateX(88px) translateY(78px);
            }
          }
          @keyframes float44 {
            40% {
              transform: translateX(22px) translateY(-154px);
            }
            80% {
              transform: translateX(19px) translateY(-9px);
            }
          }
          @keyframes float45 {
            40% {
              transform: translateX(26px) translateY(-295px);
            }
            80% {
              transform: translateX(-37px) translateY(-66px);
            }
          }
          @keyframes float46 {
            40% {
              transform: translateX(-7px) translateY(41px);
            }
            80% {
              transform: translateX(36px) translateY(91px);
            }
          }
          @keyframes float47 {
            40% {
              transform: translateX(62px) translateY(-275px);
            }
            80% {
              transform: translateX(-24px) translateY(268px);
            }
          }
          @keyframes float48 {
            40% {
              transform: translateX(38px) translateY(-78px);
            }
            80% {
              transform: translateX(-47px) translateY(284px);
            }
          }
          @keyframes float49 {
            40% {
              transform: translateX(0) translateY(-5px);
            }
            80% {
              transform: translateX(115px) translateY(-59px);
            }
          }
          @keyframes float50 {
            40% {
              transform: translateX(11px) translateY(86px);
            }
            80% {
              transform: translateX(73px) translateY(194px);
            }
          }
          @keyframes float51 {
            40% {
              transform: translateX(75px) translateY(1px);
            }
            80% {
              transform: translateX(-11px) translateY(281px);
            }
          }
          @keyframes float52 {
            40% {
              transform: translateX(39px) translateY(-255px);
            }
            80% {
              transform: translateX(133px) translateY(-83px);
            }
          }
          @keyframes float53 {
            40% {
              transform: translateX(16px) translateY(41px);
            }
            80% {
              transform: translateX(-29px) translateY(123px);
            }
          }
          @keyframes float54 {
            40% {
              transform: translateX(-12px) translateY(13px);
            }
            80% {
              transform: translateX(32px) translateY(254px);
            }
          }
          @keyframes float55 {
            40% {
              transform: translateX(8px) translateY(-279px);
            }
            80% {
              transform: translateX(69px) translateY(165px);
            }
          }
          @keyframes float56 {
            40% {
              transform: translateX(72px) translateY(48px);
            }
            80% {
              transform: translateX(-38px) translateY(165px);
            }
          }
          @keyframes float57 {
            40% {
              transform: translateX(21px) translateY(89px);
            }
            80% {
              transform: translateX(52px) translateY(28px);
            }
          }
          @keyframes float58 {
            40% {
              transform: translateX(65px) translateY(-40px);
            }
            80% {
              transform: translateX(123px) translateY(199px);
            }
          }
          @keyframes float59 {
            40% {
              transform: translateX(25px) translateY(34px);
            }
            80% {
              transform: translateX(144px) translateY(-52px);
            }
          }
          @keyframes float60 {
            40% {
              transform: translateX(50px) translateY(-263px);
            }
            80% {
              transform: translateX(93px) translateY(-80px);
            }
          }
          @keyframes float61 {
            40% {
              transform: translateX(65px) translateY(-89px);
            }
            80% {
              transform: translateX(46px) translateY(70px);
            }
          }
          @keyframes float62 {
            40% {
              transform: translateX(37px) translateY(-167px);
            }
            80% {
              transform: translateX(116px) translateY(-98px);
            }
          }
          @keyframes float63 {
            40% {
              transform: translateX(3px) translateY(-140px);
            }
            80% {
              transform: translateX(-19px) translateY(58px);
            }
          }
          @keyframes float64 {
            40% {
              transform: translateX(-6px) translateY(-257px);
            }
            80% {
              transform: translateX(136px) translateY(-92px);
            }
          }
          @keyframes float65 {
            40% {
              transform: translateX(5px) translateY(-90px);
            }
            80% {
              transform: translateX(-30px) translateY(36px);
            }
          }
          @keyframes float66 {
            40% {
              transform: translateX(77px) translateY(98px);
            }
            80% {
              transform: translateX(114px) translateY(249px);
            }
          }
          @keyframes float67 {
            40% {
              transform: translateX(-5px) translateY(-186px);
            }
            80% {
              transform: translateX(5px) translateY(-83px);
            }
          }
          @keyframes float68 {
            40% {
              transform: translateX(34px) translateY(-292px);
            }
            80% {
              transform: translateX(-40px) translateY(-82px);
            }
          }
          @keyframes float69 {
            40% {
              transform: translateX(-10px) translateY(-55px);
            }
            80% {
              transform: translateX(91px) translateY(172px);
            }
          }
          @keyframes float70 {
            40% {
              transform: translateX(19px) translateY(-84px);
            }
            80% {
              transform: translateX(77px) translateY(225px);
            }
          }
          @keyframes float71 {
            40% {
              transform: translateX(21px) translateY(58px);
            }
            80% {
              transform: translateX(77px) translateY(-41px);
            }
          }
          @keyframes float72 {
            40% {
              transform: translateX(61px) translateY(-101px);
            }
            80% {
              transform: translateX(113px) translateY(-66px);
            }
          }
          @keyframes float73 {
            40% {
              transform: translateX(69px) translateY(-243px);
            }
            80% {
              transform: translateX(-8px) translateY(-85px);
            }
          }
          @keyframes float74 {
            40% {
              transform: translateX(32px) translateY(-137px);
            }
            80% {
              transform: translateX(-6px) translateY(298px);
            }
          }
          @keyframes float75 {
            40% {
              transform: translateX(20px) translateY(23px);
            }
            80% {
              transform: translateX(32px) translateY(195px);
            }
          }
          @keyframes float76 {
            40% {
              transform: translateX(71px) translateY(5px);
            }
            80% {
              transform: translateX(52px) translateY(52px);
            }
          }
          @keyframes float77 {
            40% {
              transform: translateX(37px) translateY(3px);
            }
            80% {
              transform: translateX(60px) translateY(-57px);
            }
          }
          @keyframes float78 {
            40% {
              transform: translateX(-6px) translateY(65px);
            }
            80% {
              transform: translateX(77px) translateY(284px);
            }
          }
          @keyframes float79 {
            40% {
              transform: translateX(68px) translateY(-178px);
            }
            80% {
              transform: translateX(150px) translateY(-77px);
            }
          }
          @keyframes float80 {
            40% {
              transform: translateX(45px) translateY(-198px);
            }
            80% {
              transform: translateX(55px) translateY(149px);
            }
          }
          @keyframes float81 {
            40% {
              transform: translateX(30px) translateY(-33px);
            }
            80% {
              transform: translateX(-13px) translateY(-82px);
            }
          }
          @keyframes float82 {
            40% {
              transform: translateX(62px) translateY(-71px);
            }
            80% {
              transform: translateX(11px) translateY(254px);
            }
          }
          @keyframes float83 {
            40% {
              transform: translateX(1px) translateY(-181px);
            }
            80% {
              transform: translateX(-17px) translateY(300px);
            }
          }
          @keyframes float84 {
            40% {
              transform: translateX(45px) translateY(-235px);
            }
            80% {
              transform: translateX(82px) translateY(-56px);
            }
          }
          @keyframes float85 {
            40% {
              transform: translateX(41px) translateY(2px);
            }
            80% {
              transform: translateX(16px) translateY(247px);
            }
          }
          @keyframes float86 {
            40% {
              transform: translateX(72px) translateY(97px);
            }
            80% {
              transform: translateX(62px) translateY(168px);
            }
          }
          @keyframes float87 {
            40% {
              transform: translateX(17px) translateY(-106px);
            }
            80% {
              transform: translateX(114px) translateY(133px);
            }
          }
          @keyframes float88 {
            40% {
              transform: translateX(4px) translateY(-171px);
            }
            80% {
              transform: translateX(137px) translateY(131px);
            }
          }
          @keyframes float89 {
            40% {
              transform: translateX(51px) translateY(-48px);
            }
            80% {
              transform: translateX(126px) translateY(100px);
            }
          }
          @keyframes float90 {
            40% {
              transform: translateX(47px) translateY(-116px);
            }
            80% {
              transform: translateX(104px) translateY(-48px);
            }
          }
          @keyframes float91 {
            40% {
              transform: translateX(61px) translateY(-53px);
            }
            80% {
              transform: translateX(136px) translateY(111px);
            }
          }
          @keyframes float92 {
            40% {
              transform: translateX(47px) translateY(-66px);
            }
            80% {
              transform: translateX(-36px) translateY(-29px);
            }
          }
          @keyframes float93 {
            40% {
              transform: translateX(35px) translateY(-188px);
            }
            80% {
              transform: translateX(124px) translateY(202px);
            }
          }
          @keyframes float94 {
            40% {
              transform: translateX(0) translateY(-217px);
            }
            80% {
              transform: translateX(-8px) translateY(127px);
            }
          }
          @keyframes float95 {
            40% {
              transform: translateX(27px) translateY(-153px);
            }
            80% {
              transform: translateX(-2px) translateY(-42px);
            }
          }
          @keyframes spin {
            from {
              transform: rotate(0);
              transform-origin: 50% 50%;
            }
            to {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </div>
  );
}
