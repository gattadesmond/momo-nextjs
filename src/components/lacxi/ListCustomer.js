import { useState, useEffect, useMemo, Fragment } from "react";

import { MoneyFormat } from "@/lib/int-format";

// import { Media, MediaContextProvider } from "@/lib/mediaFresnel";

const ListCustomer = ({ topList, itemMax = 10 }) => {
  ///page state

  if (!topList || topList.Result == false) {
    return <div className="py-6 px-5 text-center">Có lỗi xảy ra</div>;
  }

  const listData = topList?.Data?.Items || null;

  const [page, setPage] = useState(1);
  const [maxPage, setMaxPage] = useState(1);

  useEffect(() => {
    setPage(1);
    // setMaxPage(1);
    if (listData && listData.length > 0) {
      let extraPages = 1;
      if (listData.length % itemMax === 0) {
        extraPages = 0;
      }
      setMaxPage(Math.floor(listData.length / itemMax) + extraPages);
    }
    // axios.get(`/lixi/2022/list-player?count=100`).then(function (response) {
    //   const res = response.Data ?? null;
    //   setList(res?.Items ?? null);
    // });
  }, [listData]);

  const ListFilter = useMemo(() => {
    return listData && listData.slice(itemMax * (page - 1), page * itemMax);
  }, [listData, itemMax, page]);

  const ListItem = ({ item, index }) => {
    return (
      <div className="grid grid-cols-[40px_3fr_80px] gap-4  md:grid-cols-[75px_4fr_2fr_150px] ">
        <div className="flex items-center  justify-center  font-bold">
          {item.Index == 1 && (
            <img
              src="https://static.mservice.io/img/momo-upload-api-220218151842-637807943225398483.png"
              className=" w-[31px]"
              loading="lazy"
            />
          )}

          {item.Index == 2 && (
            <img
              src="https://static.mservice.io/img/momo-upload-api-220218151927-637807943672908831.png"
              className=" w-[31px]"
              loading="lazy"
            />
          )}

          {item.Index == 3 && (
            <img
              src="https://static.mservice.io/img/momo-upload-api-220218151936-637807943761259457.png"
              className=" w-[31px]"
              loading="lazy"
            />
          )}

          {item.Index >3  && item.Index}
        </div>
        <div className="relative pt-1">
          <div className=" text-sm  font-bold md:text-base uppercase">
            {item.userName}
          </div>
          <div className="text-sm text-gray-500 md:hidden">{item.userId}</div>
        </div>

        <div className="relative hidden pt-1 md:block">
          {/* <div className=" font-bold">{item.userName}</div> */}
          <div className="text-gray-800">{item.userId}</div>
        </div>

        <div className="text-lx-2 flex  items-center font-bold  ">
          {" "}
          {MoneyFormat(item.coin)}
        </div>
      </div>
    );
  };

  const Pagination = () => {
    return (
      <div className="flex items-center justify-center  space-x-2 ">
        <div
          className={`flex h-9 w-9  select-none items-center justify-center ${
            page === 1 ? "opacity-50" : "cursor-pointer"
          }`}
          onClick={() => setPage(page === 1 ? page : page - 1)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="text-lx-2 h-5 w-5"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M10 19l-7-7m0 0l7-7m-7 7h18"
            />
          </svg>
        </div>

        <div className=""> {"Trang " + page + " / " + maxPage} </div>

        <div
          className={`flex h-9 w-9  select-none items-center  justify-center ${
            page === maxPage ? "opacity-50" : "cursor-pointer"
          }`}
          onClick={() => setPage(page === maxPage ? page : page + 1)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="text-lx-2 h-5  w-5"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M14 5l7 7m0 0l-7 7m7-7H3"
            />
          </svg>
        </div>
      </div>
    );
  };

  const Divider = () => {
    return <div className=" h-[1px] w-full bg-[#fac7c0]"></div>;
  };

  const LineLX = () => {
    return (
      <div className="  w-full pl-5">
        <div className="relative h-3 overflow-hidden">
          <img
            src="https://static.mservice.io/fileuploads/svg/momo-file-220218142348.svg"
            className="absolute  left-0 block w-auto max-w-none "
          />
        </div>
      </div>
    );
  };

  return (
    <>
      <h4 className="  mb-5 max-w-4xl text-center text-gray-700 mx-auto sm:text-lg">
        Bảng xếp hạng 100 người chơi kiếm được nhiều xu nhất  <i className="opacity-70">(mỗi xu tương ứng 1 VNĐ)</i>
      </h4>

      <div className="  border-lx-sm  mx-auto  grid max-w-4xl grid-flow-row  auto-rows-auto gap-y-2 rounded-md px-2  py-1 sm:px-4  sm:py-3 md:gap-y-3  ">
        <div className="grid grid-cols-[40px_3fr_80px] gap-4  border-b-2 border-[#fac7c0] pb-3 md:grid-cols-[75px_4fr_2fr_150px]">
          <div className=" text-lx-2 flex items-center font-bold leading-tight">
            Thứ hạng
          </div>

          <div className="relative flex items-center leading-tight">
            <div className=" text-lx-2 font-bold">Người chơi</div>
          </div>

          <div className="relative hidden leading-tight md:block">
            <div className="  text-lx-2 font-bold">Số điện thoại</div>
          </div>

          <div className="text-lx-2  font-bold leading-tight">
            Số xu kiếm được
          </div>
        </div>

        {ListFilter &&
          ListFilter.map((item, index) => {
            return (
              <Fragment key={index}>
                <ListItem key={index} index={index} item={item} />
                <Divider />
              </Fragment>
            );
          })}

        <Pagination />
      </div>
    </>
  );
};

export default ListCustomer;
