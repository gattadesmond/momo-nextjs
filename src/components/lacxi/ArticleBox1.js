import Link from "next/link";
import Image from "next/image";

import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { decode } from "html-entities";
import parse from "html-react-parser";
import { axios } from "@/lib/index";

import { parseISO, format } from "date-fns";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";
import ButtonAction from "@/components/ui/ButtonAction";

import ArticleAjaxLoad from "@/components/ui/ArticleAjaxLoad";
import { appSettings } from "@/configs";

function ArticleBox1({ data, isModal, isMobile = false }) {
  const { Url, Id } = data;

  const [article, setArticle] = useState(null);
  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };
  const handleShow = () => {
    setModal(true);
    setLoading(true);
    axios
      .get(`/article/${Id}?countRelating=0&countNewest=0`)
      .then(function (response) {
        const res = response.Data;
        setLoading(false);
        setArticle(res);

        var _isViewApp =
          res?.ViewInApp && router?.query?.view == "app" ? true : false;
        if (_isViewApp) setIsViewApp(_isViewApp);

        var img = document.createElement("img");
        img.setAttribute(
          "src",
          `${appSettings.AppConfig.HOST_API}/trace/article/${res.Id}`
        );
        img.setAttribute(
          "style",
          "position: absolute;display: block;z-index: -10000"
        );
        document.body.appendChild(img);
      });
  };

  function Content({ data }) {
    const { Avatar, Short, Title, DateShow, TotalViews } = data;
    return (
      <>
        <div className="img-mask-container">
          <div className=" max-w-full img-mask border-2  rounded-xl  border-[#B0091B] flex overflow-hidden">
            {Avatar && (
              <Image src={Avatar} alt={Title} width={508} height={244} />
            )}
          </div>
        </div>
        <h3 className="font-lx text-lx-2  mt-4  text-xl md:text-lg leading-snug  md:leading-snug">
          {parse(decode(Title))}
        </h3>

        <div className="mt-3 text-md ">{parse(decode(Short))}</div>

        <div className="mt-5 text-center absolute bottom-0 left-0 right-0">
          <button
            onClick={handleShow}
            className=" rounded-md  px-6 py-1.5  text-yellow-100 font-lx bg-grad-1  hover:opacity-80"
          >
            Xem chi tiết
          </button>
        </div>
      </>
    );
  }
  // <div className="h-full overflow-hidden transition-shadow bg-white rounded-lg shadow-md cursor-pointer group hover:shadow-lg flex flex-wrap">
  return (
    <>
      <div className="snap-start first:pl-3 sm:first:pl-0 shrink-0 cursor-pointer   w-[270px] sm:w-auto">
        {isModal ? (
          <div onClick={handleShow} className="relative h-full pb-12">
            <Content data={data} />
          </div>
        ) : (
          <Link href={Url}>
            <a className="relative block h-full pb-12">
              <Content data={data} />
            </a>
          </Link>
        )}
      </div>

      <Modal isOpen={modal} onDismiss={close} isFull={true} isBig={false}>
        <ModalHeader> Sự kiện </ModalHeader>

        <ModalBody css="p-0 bg-white rounded">
          <div className="p-4 md:p-7">
            <ArticleAjaxLoad
              isMobile={isMobile}
              article={article}
              loading={loading}
              isViewApp={isViewApp}
            />
          </div>
        </ModalBody>
        <ModalFooter
          className={`space-x-3 ${
            article?.ViewInApp && article?.ViewInAppConfig?.Ctas?.length > 0
              ? "justify-center"
              : ""
          }`}
        >
          {!loading &&
            (article?.ViewInApp &&
            article?.ViewInAppConfig?.Ctas?.length > 0 ? (
              article.ViewInAppConfig.Ctas.map((cta, idx) => {
                return (
                  <div
                    key={idx}
                    className="flex items-start justify-center flex-1 md:flex-none    "
                  >
                    <ButtonAction
                      className="w-full whitespace-nowrap"
                      isViewApp={isViewApp}
                      classNameRoot=" flex  md:grow-0"
                      cta={cta}
                    />
                  </div>
                );
              })
            ) : (
              <div className="flex items-start justify-end flex-1 md:flex-none  ">
                <a
                  onClick={close}
                  className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
                >
                  Đóng
                </a>
              </div>
            ))}
        </ModalFooter>
      </Modal>
    </>
  );
}

export default ArticleBox1;
