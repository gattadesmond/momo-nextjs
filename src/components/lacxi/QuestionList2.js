import { useState, useEffect } from "react";

import { decode } from "html-entities";

import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

const QuestionItem = ({ data }) => {
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  const handleShow = () => {
    setModal(true);
  };
  return (
    <>
      <div
        className="question-title relative block w-full text-left py-3 pl-0 pr-5 md:py-4 md:text-lg font-semibold text-lx-2 hover:opacity-70 cursor-pointer focus:outline-none"
        onClick={handleShow}
      >
        <div className="relative pl-11">
          {" "}
          <img
            src="https://static.mservice.io/img/momo-upload-api-220117190530-637780431304219676.png"
            className="w-8 absolute left-0 -top-1"
          />
          {decode(data.Title)}
        </div>
      </div>

      <Modal isOpen={modal} onDismiss={close} isFullMobile={true} isBig={false}>
        <ModalHeader> Câu hỏi thường gặp </ModalHeader>

        <ModalBody css="p-0 bg-white rounded">
          <div className="p-4 md:p-7">
            <h3 className="text-xl sm:text-2xl mb-4 font-bold mt-1  text-lx-2">
              {" "}
              {decode(data.Title)}
            </h3>
            <div className=" soju__prose small">
              <ArticleContentConvert data={decode(data.Content)} />
            </div>
          </div>
        </ModalBody>

        <ModalFooter className={`space-x-3 `}>
          <div className="flex items-start justify-end flex-1 md:flex-none  ">
            <a
              onClick={close}
              className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default function QuestionList({ data, title = "", isMobile }) {
  if (!data) return null;
  // .Data.Items[0].ListQuestions
  const Groups = data.Data.Items.sort(
    (a, b) => a.DisplayOrder - b.DisplayOrder
  );

  const [tabIndex, setTabIndex] = useState(0);
  const handleTabsChange = (index) => setTabIndex(index);

  return (
    <>
      {Groups[0] && (
        <Tabs index={tabIndex} onChange={handleTabsChange}>
          {Groups?.length > 1 && (
            <TabList className="flex items-center justify-center space-x-6 flex-nowrap ">
              {Groups.map((item, index) => (
                <Tab
                  key={index}
                  className={`block px-2 py-1  font-semibold outline-none ring-0 border-b-2  ${
                    index == tabIndex
                      ? "text-lx-2 border-pink-600 is-active"
                      : "text-gray-800 border-transparent"
                  }`}
                >
                  {item.Name}
                </Tab>
              ))}
              <div className="w-1">&nbsp;</div>
            </TabList>
          )}

          <TabPanels className=" mt-4 sm:mt-5">
            {Groups.map((group, index) => (
              <TabPanel key={group.Id}>
                <div className="grid grid-cols-1 md:grid-cols-2 md:gap-16">
                  <div className="divide-y divide-[#F2C9C2] article-question">
                    {group.ListQuestions.slice(
                      0,
                      group.ListQuestions.length / 2
                    ).map((item, index) => (
                      <QuestionItem data={item} key={index}/>
                    ))}
                  </div>

                  <div className="bg-[#F2C9C2] h-[1px] w-full md:hidden"></div>

                  <div className="divide-y divide-[#F2C9C2] article-question">
                    {group.ListQuestions.slice(
                      group.ListQuestions.length / 2
                    ).map((item, index) => (
                      <QuestionItem data={item} key={index} />
                    ))}
                  </div>
                </div>
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      )}
    </>
  );
}
