import { useState, useEffect } from "react";

import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import Image from "next/image";

import { decode } from "html-entities";
import parse from "html-react-parser";

import { parseISO, format } from "date-fns";

import Link from "next/link";
import CssCarousel from "@/components/CssCarousel";
import ArticleBox1 from "@/components/lacxi/ArticleBox1";

const LineLX = () => {
  return (
    <div className=" overflow-hidden relative h-3 max-w-6xl mx-auto w-full px-5 md:px-8 lg:px-8">
      <img
        src="https://static.mservice.io/fileuploads/svg/momo-file-220117161804.svg"
        className="block mx-auto w-auto max-w-none absolute left-1/2 -translate-x-1/2"
      />
    </div>
  );
};
export default function ProjectArticles({
  data,
  isMobile = false,
  isNumber = null,
}) {
  if (data.Projects[0] == undefined) {
    return null;
  }

  const Cat = data.Projects.length > 0 ? data.Projects : null;
  let List = null;
  if (!Cat) {
    List = data.Projects[0];
  }

  const [tabIndex, setTabIndex] = useState(1);

  return (
    <>
      {Cat && (
        <Tabs index={tabIndex} onChange={(index) => setTabIndex(index)}>
          <CssCarousel className="-mx-5 mb-3 mt-1">
            <TabList className="flex items-center md:justify-center space-x-3 md:space-x-6 overflow-scroll pl-5 md:pl-0">
              {Cat.map((item, index) => (
                <Tab
                  key={index}
                  className={`block px-5 py-1.5 sm:px-6 sm:py-2 whitespace-nowrap outline-none ring-0 rounded-full  border-2 ${
                    index == tabIndex
                      ? "text-lx-2 bg-[#FDF1BB] font-bold  border-[#F3B27D]  is-active"
                      : "text-lx-2 bg-[#F8D8CE] border-[#F8D8CE]"
                  }`}
                >
                  {item.Title}
                </Tab>
              ))}
              <div className="w-1">&nbsp;</div>
            </TabList>
          </CssCarousel>

          <LineLX />

          <TabPanels>
            {Cat.map((item, index) => (
              <TabPanel key={index} className=" pt-2 sm:pt-5 -mx-3 sm:mx-0">
                <div className="  w-full flex sm:flex-none sm:grid sm:grid-cols-3 gap-3 sm:gap-6 snap-x scroll-pl-3  overflow-x-auto py-2">

                  {item.Items.map((item, index) => (
                    <ArticleBox1
                      key={index}
                      data={item}
                      isModal={true}
                      isMobile={isMobile}
                    />
                  ))}


                  <div className="snap-start w-4 sm:hidden">&nbsp;</div>
                </div>
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      )}

      {/* {!Cat && (
        <ArticleListAjax item={List} isNumber={isNumber} isMobile={isMobile} />
      )} */}

      <style jsx>{``}</style>
    </>
  );
}
