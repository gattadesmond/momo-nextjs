import { useState, useEffect } from "react";
import { decode } from "html-entities";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
} from "@reach/accordion";

import ArticleContentConvert from "@/components/ui/ArticleContentConvert";

import Modal from "@/components/modal/Modal";
import ModalBody from "@/components/modal/ModalBody";
import ModalHeader from "@/components/modal/ModalHeader";
import ModalFooter from "@/components/modal/ModalFooter";

const TheleItem = ({ data }) => {
  const [modal, setModal] = useState(false);

  const close = () => {
    setModal(false);
    // dissmiss();
  };

  const handleShow = () => {
    setModal(true);
  };
  return (
    <>
      <div
        className="question-title relative block w-full text-left py-3 pl-0 pr-5 md:py-4 text-lg font-semibold text-lx-2 hover:text-opacity-70 cursor-pointer focus:outline-none"
        onClick={handleShow}
      >
        <div className="relative pl-11">
          {" "}
          <img
            src="https://static.mservice.io/img/momo-upload-api-220117190530-637780431304219676.png"
            className="w-8 absolute left-0 -top-1"
          />
          {decode(data.Title)}
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="absolute right-0 inline w-5 h-5 -mt-2  text-lx-2  icon top-1/2"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      </div>

      <Modal isOpen={modal} onDismiss={close} isFullMobile={true} isBig={false}>
        <ModalHeader> Thể lệ</ModalHeader>

        <ModalBody css="p-0 bg-white rounded">
          <div className="p-4 md:p-7">
            <h3 className="text-xl sm:text-2xl mb-4 font-bold mt-1  text-lx-2">
              {" "}
              {decode(data.Title)}
            </h3>
            <div className=" soju__prose small">
              <ArticleContentConvert data={decode(data.Content)} />
            </div>
          </div>
        </ModalBody>

        <ModalFooter className={`space-x-3 `}>
          <div className="flex items-start justify-end flex-1 md:flex-none  ">
            <a
              onClick={close}
              className="inline-block px-5 py-2 text-sm font-bold text-center text-white uppercase transition-all bg-pink-700 rounded-md cursor-pointer btn text-opacity-90 hover:bg-pink-800"
            >
              Đóng
            </a>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default function QuestionList({ data, title = "", isMobile }) {
  if (!data) return null;

  return (
    <>
      {data.length > 0 && (
        <>
          <div className="divide-y divide-[#F2C9C2] article-question">
            {data.map((item, index) => (
              <TheleItem data={item} key={index} />
            ))}
          </div>
        </>
      )}
    </>
  );
}
