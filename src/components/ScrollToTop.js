import { useState, useEffect } from "react";

import { useSpring, animated } from "react-spring";

const ScrollToTop = () => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    function setScrollVisible() {
      window.pageYOffset > 500 ? setVisible(true) : setVisible(false);
    }

    window.addEventListener("scroll", setScrollVisible, { passive: true });
    return function clean() {
      window.removeEventListener("scroll", setScrollVisible);
    };
  }, []);

  const stylesSpring = useSpring({
    opacity: visible ? 1 : 0,
    y: visible ? 0 : 20,
    config: { duration: 200 },
  });

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <>
      <animated.div
        id="scrollToTop"
        style={{
          opacity: stylesSpring.opacity,
          transform: stylesSpring.y.to((y) => `translateY(${y}px)`),
        }}
        onClick={() => scrollToTop()}
        className={`fixed z-30  right-3 bottom-16 sm:bottom-10 totop-btn sm:right-8 ${visible ? "" : " select-none pointer-events-none "
          }`}
      >
        <div className=" flex items-center justify-center text-gray-500 transition bg-white border rounded-full shadow-sm cursor-pointer w-11 h-11 hover:text-gray-800 hover:bg-gray-50">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-5 h-5 "
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M3.293 9.707a1 1 0 010-1.414l6-6a1 1 0 011.414 0l6 6a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L4.707 9.707a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            />
          </svg>
        </div>

        <style jsx>{`
          
        .totop-btn{
          margin-bottom: env(safe-area-inset-bottom) !important;
        }
        `}</style>
      </animated.div>
    </>
  );
};

export default ScrollToTop;
