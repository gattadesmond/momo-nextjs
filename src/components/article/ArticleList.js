import Link from "next/link";

import classNames from "classnames";

import ArticleTitle from "./ArticleItem/ArticleTitle";
import ArticleThumbnail from "./ArticleItem/ArticleThumbnail";

import ContentLoader from "react-content-loader";

import { useEffect, useState } from "react";

import { axios } from "@/lib/index";
import ArticleItem from "./ArticleItem/ArticleItem";

const ArticleList = ({ data, excludeIds = [], cat, isMore = true, isHome = false }) => {
  const initModel = {
    CateId: null,
    Items: [],
    TotalItems: 0,
    LastIdx: 0,
    Count: 10,
    ExcludeIds: [],
  };

  const [model, setModel] = useState(initModel);

  useEffect(() => {
    setModel({
      ...model,
      SortType: 1,
      CateId: cat ? cat.Id : null,
      Items: data.Items,
      TotalItems: data.TotalItems,
      LastIdx: data.LastIdx,
      Count: data.Count,
      ExcludeIds: excludeIds,
      IsLoading: false,
    });
    //setLastIdx(lastI);

    // return function cleanup() {
    //   setModel(initModel);
    // };
  }, [data, cat]);

  const paramEmptyNull = (value) => {
    if (value && value != "null" && value != "undefined") {
      return value;
    }
    return "";
  };
  
  const getExcludeIds = () => {
   return  model.ExcludeIds? model.ExcludeIds.join(','):'';
    // return [].concat(
    //   model.ExcludeIds,
    //   model.Items.map(x => x.Id)
    // ).join(',');
  }

  const handelLoadMore = () => {
    setModel({
      ...model,
      IsLoading: true,
    });
    setTimeout(function () {
      axios
        .get(
          `/article/list?cateId=${paramEmptyNull(
            model.CateId
          )}&excludeIds=${getExcludeIds(model.ExcludeIds)}&sortType=${model.SortType
          }&count=${paramEmptyNull(model.Count)}&lastIdx=${paramEmptyNull(
            model.LastIdx
          )}`
        )
        .then((res) => {
          if (!res.Result || !res.Data) {
            setModel({
              ...model,
              IsLoading: false,
            });
            return;
          }
          var data = res.Data;
          setModel({
            ...model,
            Items: [...new Set([...model.Items, ...data.Items])],
            TotalItems: data.TotalItems,
            LastIdx: data.LastIdx,
            IsLoading: false,
          });

          // setIsFetching(false);

          // setItems((prevTitles) => {
          //   return [...new Set([...prevTitles, ...res.Data.Items])];
          // });
          // setLastIdx(res.Data.LastIdx);
          // setIsFetching(false);

          // //Check co load more ko
          // if (model.LastIdx < model.TotalItems) {
          //   setHasMore(false);
          // }
        })
        .catch((e) => {
          
        });
    }, 500);
  };

  return (
    <>
      {model.Items &&
        model.Items.map((item, index) => (
          <ArticleItem
            className={`${index == (model.Items.length- 1) ? "border-b-0" : "border-b border-gray-200 md:border-b-0 pb-2 md:pb-0"}`}
            key={item.Id}
            type="popular"
            data={item}
            isShowCat={isHome}
          />
        ))}

      {model.IsLoading && (
        <>
          <article className="hidden md:block">
            <ContentLoader
              viewBox="0 0 350 256"
              title="Loading news..."
            >
              <rect x="0" y="0" rx="5" ry="5" width="350" height="168" />
              <rect x="0" y="180" rx="0" ry="0" width="350" height="76" />
            </ContentLoader>
          </article>
          <article className="hidden md:block">
            <ContentLoader
              viewBox="0 0 350 256"
              title="Loading news..."
            >
              <rect x="0" y="0" rx="5" ry="5" width="350" height="168" />
              <rect x="0" y="180" rx="0" ry="0" width="350" height="76" />
            </ContentLoader>
          </article>
          <article className="md:hidden">
            <ContentLoader
              viewBox="0 0 335 90"
              title="Loading news..."
            >
              <rect x="0" y="0" rx="5" ry="5" width="335" height="90" />
            </ContentLoader>
          </article>
        </>
      )}

      {model.LastIdx < model.TotalItems && isMore && (
        <div className="py-5 text-center md:py-8 md:col-span-2">
          <button
            type="button"
            disabled={model.IsLoading ? "true" : ""}
            className="py-1 pl-4 pr-6 font-semibold text-blue-500 transition-all border border-blue-600 rounded-full hover:text-blue-800 hover:bg-blue-50"
            onClick={handelLoadMore}
          >
            {model.IsLoading ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-spin opacity-80"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="inline-block w-4 h-4 mr-2 animate-bounce opacity-80"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M16.707 10.293a1 1 0 010 1.414l-6 6a1 1 0 01-1.414 0l-6-6a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l4.293-4.293a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            )}
            Xem thêm
          </button>
        </div>
      )}
      {(model.LastIdx >= model.TotalItems || !isMore) &&
        <div className="pb-5 md:pb-8 md:col-span-2 h-1"></div>
      }
    </>
  );
};

export default ArticleList;
