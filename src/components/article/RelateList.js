import Link from "next/link";
import ArticleTitle from "./ArticleItem/ArticleTitle";
import ArticleThumbnail from "./ArticleItem/ArticleThumbnail";
import ArticleInfo from "./ArticleItem/ArticleInfo";

const RelateList = ({
  className,
  data = [],
  linkViewMore = "",
  title = "Tin tức liên quan",
  isShowCat = true,
}) => {
  return (
    <div className={`${className ? className : ""}`}>
      <div className="flex justify-between items-center">
        <h2 className="mb-0 text-xl font-semibold text-momo flex-auto">
          {title}
        </h2>
        <Link href={linkViewMore}>
          <a className="text-base text-blue-500 items-center flex">
            Xem thêm
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M9 5l7 7-7 7"
              />
            </svg>
          </a>
        </Link>
      </div>
      <div className="grid gap-4 pt-4">
        {data.length > 0 &&
          data.map((item) => (
            <article
              key={item.Id}
              className="article-type-relate grid grid-cols-12 grid-rows-1 gap-4"
            >
              <div className="col-span-5 pt-1">
                <ArticleThumbnail
                  avatar={item.Avatar}
                  title={item.Title}
                  slug={item.Link}
                  objectFit="contain"
                />
              </div>
              <div className={`col-span-7`}>
                <ArticleTitle
                  type="normal"
                  title={item.Title}
                  slug={item.Link}
                />
                <ArticleInfo
                  isShowCat={isShowCat}
                  className={`mt-2`}
                  catId={item.CategoryId}
                  catLink={item.CategoryLink}
                  catName={item.CategoryName}
                  date={item.PublicDate}
                />
              </div>
            </article>
          ))}
      </div>
    </div>
  );
};

export default RelateList;
