import { appSettings } from "@/configs";


const ArticleFacebookLikeShare = (props) => {
  const { className, title, url, ...rest } = props;

  const Fb_App_Id = appSettings.AppConfig?.FACEBOOK_APP_ID || '';

  return (
    <div
      className={`${props.className ? prop.className : ''} share-fb`}
    >
      {/* https://web.dev/iframe-lazy-loading/ */}
      <iframe
        src={`https://www.facebook.com/plugins/like.php?href=${url}&width=130&layout=button&action=like&size=small&share=true&height=20&lazy=true${Fb_App_Id ? "&appId=" + Fb_App_Id : ''}`}
        width="130" height="20"
        scrolling="no"
        frameBorder="0"
        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
        loading="lazy"
      />
    </div>
  )
}

export default ArticleFacebookLikeShare;