import { useState } from "react";

import ArticleItem from "./ArticleItem/ArticleItem";

function ArticleFeature({ className, data,isShowCat=true }) {
  let listFeature = [...data].filter((x, i) => i < 3);

  const getTypeOfArticle = (index, listData) => {
    if (index === 0) return "big-feature";
    if (index === 1 && listData.length === 2) return "big-feature";
    return "feature";
  };

  return (
    <div
      className={`${
        className && className
      } article-feature grid grid-cols-1 md:grid-cols-2 gap-4 content-center pb-7`}
    >
      {listFeature &&
        listFeature.map((item, index) => (
          <div
            key={item.Id}
            className={`col-span-1 
          ${index === 0 && "md:col-span-2 md:mb-3"}
          ${index === 1 && listFeature.length === 2 && "md:col-span-2"}
        `}
          >
            <ArticleItem
              data={item}
              type={getTypeOfArticle(index, listFeature)}
              isShowCat={isShowCat}
              isPriority={index < 2 ? true : false}
            />
          </div>
        ))}
    </div>
  );
}

export default ArticleFeature;
