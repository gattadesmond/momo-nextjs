import { useEffect } from "react";
import StickyBox from "react-sticky-box";
// import ContentLoader from "react-content-loader";

import Page from "@/components/page";
import Container from "@/components/Container";

import ArticleFeature from "./ArticleFeature";
import ArticleMenu from "./ArticleMenu";
import ArticleList from "./ArticleList";
import CategoryTile from "./CategoryTitle";
import PreferList from "./PreferList";
import ListDonation from "../donation/ListDonation";

function PageArticleCategory({ categoryObj, dataCategory, dataHomePost, listPrefer, listDonation, isCateDonation, pageMaster, isMobile }) {
  const data = dataHomePost.Data;

  useEffect(() => {
    document.querySelector("body").classList.add("page-article");

    return function clean() {
      document.querySelector("body").classList.remove("page-article");
    };
  }, []);

  return (
    <Page
      className="page-article"
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      header={pageMaster.Data.MenuHeaders}
      isMobile={isMobile}
    >
      <ArticleMenu data={dataCategory.Data} cat={categoryObj} root={{
              link:"/tin-tuc",
              title:"Tin tức",
              icon:"https://static.mservice.io/next-js/_next/static/public/article/Tinmoi.svg"
            }} />
      <Container>
        <div className="grid grid-cols-1 pb-10 lg:grid-cols-3 lg:gap-6 mt-5 lg:mt-6">
          <div className="col-span-2">
            <CategoryTile category={data.Category} />
            {
              data.ListFeatured?.Items?.length > 0 &&
              <ArticleFeature data={data.ListFeatured.Items} isHome={false} />
            }
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <ArticleList
                excludeIds={data.ListFeatured.Items.map(x => x.Id)}
                data={data.Lists}
                cat={data.Category ? data.Category : null}
                isHome={true}
              />
            </div>
          </div>

          <div className="border-t lg:border-t-0 lg:border-l border-gray-300 lg:pl-6 pt-3 md:pt-0">
            <StickyBox offsetTop={60} offsetBottom={20}>
              <div className="pb-4 md:pt-5 mt-1 md:mt-3 rounded-lg lg:mt-0 lg:pt-1">
                {!isCateDonation &&
                  <PreferList className="" data={listPrefer.Data?.Items} type={2} />
                }
                {isCateDonation && (listDonation?.TraiTim?.Data || listDonation?.HeoDat?.Data) &&
                  <>
                    <div className="mb-6 text-center lg:text-left lg:mb-0">
                      <h2 className="text-xl font-semibold text-momo">
                        Chương trình quyên góp mới nhất
                      </h2>
                    </div>
                    {listDonation?.TraiTim?.Data &&
                      <ListDonation limit={2} data={listDonation.TraiTim.Data} type={2} />
                    }
                    {listDonation?.HeoDat?.Data &&
                      <ListDonation limit={2} data={listDonation.HeoDat.Data} type={2} />
                    }
                  </>
                }
              </div>
            </StickyBox>
          </div>
        </div>
      </Container>
    </Page>
  )
};

export default PageArticleCategory;