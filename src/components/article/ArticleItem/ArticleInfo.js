import ViewFormat from "@/lib/view-format";
import CategoryInfo from "../CategoryInfo";

const ArticleInfo = ({
  className,
  type,
  isShowCat = true,
  catId,
  catName,
  catLink,
  date,
  totalViews,
  totalViewsFormat,
}) => {
  return (
    <div
      className={`${
        className || ""
      } flex flex-wrap items-center text-xs text-gray-500 font-semibold`}
    >
      {isShowCat && catLink && (
        <>
          <CategoryInfo catId={catId} catName={catName} catLink={catLink} />
        </>
      )}
      {date && (
        <>
          <div className="font-normal mx-1">·</div>
          <div>{date}</div>
        </>
      )}
      {totalViews > 500 && (type == "popular" || type == "big-feature") && (
        <>
          {isShowCat && catLink && <div className="font-normal mx-1">·</div>}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-4 w-4 inline-block relative md:hidden"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            style={{ top: "-1px" }}
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
            />
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
            />
          </svg>
          &nbsp;
          {totalViewsFormat}
          {/* <ViewFormat data={totalView} /> */}
          <span className="hidden md:inline-block">&nbsp;lượt xem</span>
        </>
      )}
    </div>
  );
};
export default ArticleInfo;
