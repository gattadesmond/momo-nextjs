import Link from "next/link";

import ArticleTitle from "./ArticleTitle";
import ArticleThumbnail from "./ArticleThumbnail";
import CategoryInfo from "../CategoryInfo";
import ArticleInfo from "./ArticleInfo";

const ArticleItem = ({
  className,
  data,
  type,
  isShowCat = true,
  isPriority = false
}) => {
  const listClassName = {
    article: "",
    thumb: "",
    info: "",
    title: "",
  };

  return (
    <article
      className={`${
        className ? className : ""
      } article-new relative article-type-${type} ${listClassName.article}
      grid gap-x-4 gap-y-2 md:gap-1 content-start 
      ${type === "normal" ? "grid-cols-12 md:grid-cols-1" : ""}
    `}
    >
      <div
        className={`
          ${
            type === "normal"
              ? "col-span-5 md:col-span-full order-2 md:order-1"
              : ""
          }
        `}
      >
        <ArticleThumbnail
          avatar={data.Avatar}
          title={data.Title}
          slug={data.Link}
          objectFit="contain"
          isPriority={isPriority}
        />
      </div>

      <ArticleInfo
        className={`md:pt-1
          ${type.includes("feature") ? "pt-2" : ""}
          ${type === "normal" ? "col-span-full order-3 md:order-2" : ""}
        `}
        type={type}
        isShowCat={isShowCat}
        catId={data.CategoryId}
        catName={data.CategoryName}
        catLink={data.CategoryLink}
        date={data.PublicDate}
        totalViews={data.TotalViews}
        totalViewsFormat={data.TotalViewsFormat}
      />

      <ArticleTitle
        className={`
          ${
            type === "normal"
              ? "col-span-7 md:col-span-full order-1 md:order-3"
              : ""
          }
        `}
        type={type}
        title={data.Title}
        slug={data.Link}
      />
    </article>
  );
};

export default ArticleItem;
