import Link from "next/link";
import Image from "next/image";

const ArticleThumbnail = ({ className, avatar, title, slug, objectFit = "cover", isPriority = false }) => {
  return (
    <div className={`article-thumb bg-gray-100 relative overflow-hidden rounded-md ${className ? className : ''}`}>
      {slug &&
        <Link href={slug}>
          <a className="stretched-link"></a>
        </Link>
      }
      <div className="aspect-w-16 aspect-h-9 aspect-77-37">
        <Image
          src={avatar}
          alt={title || ""}
          layout="fill"
          objectFit={objectFit}
          // unoptimized={true}
          loading={isPriority ? "eager" : "lazy"}
          priority={isPriority}
        />
      </div>
   
    </div>
  );
};

export default ArticleThumbnail;

