import Link from "next/link";

const ArticleTitle = (props) => {
  const {
    className,
    title,
    slug,
    type = "normal",
    ...rest
  } = props;

  let classTitle = '';
  switch (type) {
    case 'big-feature':
      classTitle += 'text-lg md:text-1.5xl';
      break;
    case 'feature':
      classTitle += 'text-lg';
      break;
    case 'full':
      classTitle += 'text-xl';
      break;
    default:
      classTitle += 'text-base';
      break;
  }

  return (
    <header className={`${className || ''}`}>
      <h3>
        <Link href={slug} >
          <a className={`article-title text-truncate-row font-bold text-gray-800 hover:underline leading-tight" ${classTitle}`}>
            {title}
          </a>
        </Link>
      </h3>
    </header>
  );
};

export default ArticleTitle;
