import Link from "next/link";

import { getArticleCategoryIcon } from "@/lib/article-category-icon";

import ArticleItem from "./ArticleItem/ArticleItem";

const CategoryListForHome = ({ data }) => {
  const articles = data.Articles;
  const icon = getArticleCategoryIcon(data.Id);
  return (
    <div className="pt-3 pb-3 md:pt-0 border-t lg:border-t-0 border-gray-300">
      <div className="flex justify-between items-center">
        <h2 className="mb-0 flex items-center text-xl font-semibold text-momo">
          <div className="pr-2">
            <img
              alt="Tin mới"
              src={icon.icon}
              loading="lazy"
              width={icon.width}
              height={icon.height}
            />
          </div>
          {data.Name}
        </h2>
        <Link href={data.Link}>
          <a className="text-base text-blue-500 flex items-center">
            Xem thêm
            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" />
            </svg>

          </a>
        </Link>
      </div>
      <div className="grid md:grid-rows-1 grid-flow-row md:grid-flow-col gap-4 lg:gap-6 mt-3">
        {articles.Items &&
          articles.Items.filter((item, index) => index < 3).map((item, index) => (
            <ArticleItem key={item.Id} data={item}
              className={`
                ${index === 0 ? 'md:row-span-2 md:col-span-2' : 'col-auto'}
              `}
              type={index === 0 ? "full" : "normal"}
            />
          ))
        }
      </div>
    </div>
  )
};
export default CategoryListForHome;