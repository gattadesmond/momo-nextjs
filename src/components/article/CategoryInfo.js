import Link from "next/link";

export const ArticleCateColor = (catId = 17) => {
  const catColor = {
    17: "#EB5757",
    18: "#27AE60",
    19: "#2F80ED",
    20: "#9B51E0",
    118: "#a50064",

    67: "#EB5757",
    69: "#27AE60",
    86: "#2F80ED",
    68: "#9B51E0",
    93: "#a50064",
  };

  return catColor[catId];
};

const CategoryInfo = ({ catId, catName, catLink, isViewApp }) => {
  return (
    <>
      {catId &&
        catLink &&
        (isViewApp ? (
          <span
            className="uppercase article-cate"
            style={{ color: ArticleCateColor(catId) }}
          >
            {catName}
          </span>
        ) : (
          <Link href={catLink}>
            <a
              className="uppercase article-cate"
              style={{ color: ArticleCateColor(catId) }}
            >
              {catName}
            </a>
          </Link>
        ))}
    </>
  );
};
export default CategoryInfo;
