import Link from "next/link";
import Image from "next/image";
import { useState, useEffect, useRef } from "react";

import CssCarousel from "@/components/CssCarousel";
import Container from "@/components/Container";

const ArticleMenu = ({ className, data, cat, root = {} }) => {
  const ref = useRef();
  //link="/tin-tuc",title="Tin tức",icon="https://static.mservice.io/next-js/_next/static/public/article/Tinmoi.svg"
  const [stick, setStick] = useState(false);

  useEffect(() => {
    const element = ref.current;
    const update = () => {
      const rect = element.getBoundingClientRect().y;

      if (rect <= 1) {
        setStick(true);
      } else {
        setStick(false);
      }
    };
    update();

    document.addEventListener("scroll", update, { passive: true });
    return () => {
      setStick(false);
      document.removeEventListener("scroll", update);
    };
  });

  return (
    <div
      className={`${
        className ? className : ""
      } sticky top-0 z-30 bg-white shadow`}
      ref={ref}
    >
      <Container className="px-0">
        <CssCarousel>
          <div
            key={`${cat?.Id || 0}`}
            id="article_menu"
            className="flex overflow-scroll w-full py-3 pl-5 md:pl-0 text-sm"
          >
            <div className="nav-item">
              <a
                href="https://momo.vn"
                className={`text-gray-800 whitespace-nowrap hover:text-pink-700 w-8 pr-4 md:pr-4
                flex content-center justify-around`}
              >
                <Image
                  className="img-black"
                  alt="Trang chủ"
                  src="https://static.mservice.io/next-js/_next/static/public/article/Home.svg"
                  loading="eager"
                  priority={true}
                  layout="fixed"
                  width={15}
                  height={19}
                />
              </a>
            </div>
            {root && (
              <div className={`nav-item ${!cat && "is-active"}`}>
                <Link href={root.link}>
                  <a
                    className={`pl-3 pr-4 whitespace-nowrap flex content-center justify-around transition-all duration-150 ease-in-out
                    ${
                      !cat
                        ? "text-pink-700 font-bold"
                        : "text-gray-800 hover:text-pink-700"
                    }`}
                  >
                    {root.icon && (
                      <Image
                        className="img-black"
                        alt={root.title}
                        src={root.icon}
                        loading="eager"
                        priority={true}
                        layout="fixed"
                        width={12}
                        height={20}
                      />
                    )}

                    <span className="ml-1" style={{ paddingTop: "1px" }}>
                      {root.title}
                    </span>
                  </a>
                </Link>
              </div>
            )}

            {data
              .sort((a, b) => a.Id - b.Id)
              .sort((a, b) => (a.Id === cat?.Id ? -1 : 1))
              .map((item, index) => (
                <div
                  key={item.Id}
                  className={`nav-item cat-item ${
                    item.Id === cat?.Id ? "is-active" : ""
                  }`}
                >
                  <Link href={item.Link}>
                    <a
                      className={`px-3 whitespace-nowrap flex content-center justify-around transition-all duration-150 ease-in-out
                    ${
                      item.Id === cat?.Id
                        ? "text-pink-700 font-bold"
                        : "text-gray-800 hover:text-pink-700"
                    }`}
                    >
                      {item.Icon && (
                        <Image
                          className="img-black"
                          alt={item.Name}
                          src={item.Icon || ""}
                          loading="eager"
                          priority={true}
                          layout="fixed"
                          width={20}
                          height={20}
                        />
                      )}

                      <span className="pl-1" style={{ paddingTop: "1px" }}>
                        {item.Name.replace("Tin Tức -", "").trim()}
                      </span>
                    </a>
                  </Link>
                </div>
              ))}
            <div className="w-1">&nbsp;</div>
          </div>
        </CssCarousel>
      </Container>

      <style jsx>{``}</style>
    </div>
  );
};

export default ArticleMenu;
