import { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";

import { axios } from "@/lib/index";

import ContentLoader from "react-content-loader";

import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, {
  Navigation, Pagination, Autoplay
} from 'swiper/core';
// install Swiper modules
SwiperCore.use([Navigation, Pagination, Autoplay]);

const ArticleBanner = ({ className, codeBanner = 'ARTICLE_RIGHT', handleShowBanner }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [listBanner, setlistBanner] = useState([]);

  useEffect(() => {
    getListBanner();
  }, [codeBanner]);

  const getListBanner = () => {
    setIsLoading(true);
    axios
      .get(`banner/listByGroup?groupCode=${codeBanner}`)
      .then((res) => {
        setIsLoading(false);
        if (!res || !res.Result) return;
        if (!res.Data) return;
        
        setlistBanner([...res.Data]);
        onShowBanner(res.Data.length > 0 ? true: false);
      })
      .catch(() => {
        setIsLoading(false);
        setlistBanner([]);
      })
  }
  const onShowBanner = (isShow) => {
    handleShowBanner && handleShowBanner(isShow);
  }

  return (
    <div className={`overflow-hidden article-banner ${listBanner.length === 0 ? "hidden" : null}`}>
      {isLoading &&
        <ContentLoader
          viewBox="0 0 363 450"
          title="Loading news..."
        >
          <rect x="0" y="0" rx="5" ry="5" width="363" height="450" />
        </ContentLoader>
      }
      {!isLoading &&
        listBanner.length === 0
        ?
          <Image
            className="hidden"
            src="https://static.mservice.io/next-js/_next/static/public/article/SlideBanner.png"
            alt="Slide Banner"
            loading="lazy"
            width={363}
            height={458}
          />
        :
          <Swiper className="swiper-banner"
            slidesPerView={1}
            spaceBetween={0}
            pagination
            navigation
            autoplay={{
              "delay": 4000,
              "disableOnInteraction": false
            }}
          >
            {
              listBanner.map((item, index) => (
                <SwiperSlide key={item.Id}>
                  <Link href={item.Link ? item.Link : "#"}>
                    <a className="block bg-gray-100 overflow-hidden aspect-w-4 aspect-h-3 aspect-363-458">
                      <Image
                        className="hidden md:block"
                        src={item.Avatar}
                        alt={item.Name}
                        layout="fill"
                        unoptimized={true}
                        loading="lazy"
                        objectFit="contain"
                      />
                      <Image
                        className="block md:hidden"
                        src={item.AvatarMobile}
                        alt={item.Name}
                        layout="fill"
                        unoptimized={true}
                        loading="lazy"
                        objectFit="contain"
                      />
                    </a>
                  </Link>
                </SwiperSlide>
              ))
            }
          </Swiper>
        }
      <style jsx>{`
          .article-banner {
            width: 100%;
          }
          .article-banner .swiper-banner {
            width: 100%;
          }
          .article-banner .aspect-363-458 {
            --tw-aspect-w: 363;
            --tw-aspect-h: 458;
          }
      `}</style>
    </div>
  );
};

export default ArticleBanner;