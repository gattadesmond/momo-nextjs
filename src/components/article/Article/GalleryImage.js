import { useState, useEffect } from "react";
import parse, { domToReact } from "html-react-parser";

import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, {
  Navigation, Thumbs
} from 'swiper/core';
// install Swiper modules
SwiperCore.use([Navigation, Thumbs]);

const GalleryIamge = ({ data }) => {
  if (!data || data.length === 0) return null;

  // store thumbs swiper instance
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  const sWrapper = data.find(x => x.attribs?.class === "swiper-wrapper");
  const sSlider = sWrapper.children.filter(x => x.type === "tag");

  const optionsOfSlider = {
    trim: true,
    replace: ({ attribs, name, children }) => {
      if (name == "div") {
        if (attribs.class.includes('swiper-lazy-preloader')) {
          return <></>;
        }
      }
      if (name == "img") {
        return (
          <img
            src={attribs.src || attribs["data-src"]}
            className="img-fluid"
            alt={attribs.alt}
            loading="lazy"
          />
        );
      }
      if (!attribs) {
        return;
      }
    }
  }

  return (
    <>
      <Swiper className="gallery-top"
        slidesPerView={1}
        spaceBetween={0}
        autoplay={{
          "delay": 4000,
          "disableOnInteraction": false
        }}
        pagination={{
          "clickable": true
        }}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
      >
        {
          sSlider.map((item, index) => (
            <SwiperSlide key={index} className="flex justify-center items-center bg-gray-100">
              {domToReact(item.children, optionsOfSlider)}
            </SwiperSlide>
          ))
        }
      </Swiper>
      <Swiper className="gallery-thumbs hidden md:block mt-4"
        slidesPerView={4}
        spaceBetween={15}
        onSwiper={setThumbsSwiper}
        watchSlidesVisibility
        watchSlidesProgress
      >
        {
          sSlider.map((item, index) => (
            <SwiperSlide key={index} className="flex justify-center bg-gray-100">
              {domToReact(item.children, optionsOfSlider)}
            </SwiperSlide>
          ))
        }
      </Swiper>
    </>
  );
};
export default GalleryIamge;