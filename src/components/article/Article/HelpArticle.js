import { decode } from "html-entities";

import HowToUse from "@/components/HowToUse";
import Container from "@/components/Container";

const HelpArticle = ({
  className,
  data,
  isShowTitle = true,
  isMobile,
}) => {
  if (!data || !data.Data) return null;

  const { Data, ...rest } = data;
  const htwData = {
    Items: [Data],
    ...rest
  };
  htwData.Items.forEach(item => {
    item.Blocks.forEach(block => {
      if (!block.Image) {
        block.Image = block.Avatar;
      }
      block.Content = decode(block.Content);
    });
  });

  return (
    <Container className={`article-guide ${className && className}`}>
      {isShowTitle &&
        <div className="mb-5 text-center md:mb-8">
          <h2 className="text-center text-momo">{data.Title}</h2>
        </div>
      }
      <div>
        {data.Data && (
          <HowToUse
            isMobile={isMobile}
            data={[htwData]}
            title={data.Data.Title}
          />
        )}
      </div>
    </Container>
  );
};

export default HelpArticle;
