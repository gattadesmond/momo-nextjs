import { decode } from "html-entities";

import HowToUse from "@/components/HowToUse";
import Container from "@/components/Container";

const HelpArticleGroup = ({
  className,
  data,
  isShowTitle = true,
  isMobile,
  isMockup,
  ...rest
}) => {
  if (!data || !data.Data || !data.Data.Groups || data.Data.Groups.length === 0)
    return <></>;
    
  data.Data.Groups.forEach((group) => {
    group.Items.forEach((item) => {
      group.Title = group.Name;
      item.Blocks.forEach((block) => {
        if (!block.Image) {
          block.Image = block.Avatar;
        }
        block.Content = decode(block.Content);
      });
    });
  });

  return (
    <Container {...rest} className={`${className ? className : ""}`}>
      {isShowTitle && (
        <div className="mb-5 text-center md:mb-8">
          <h2 className="text-center text-momo">{data.Title}</h2>
        </div>
      )}
      {data.Data?.Groups && (
        <HowToUse
          isMockup={isMockup}
          isMobile={isMobile}
          data={data.Data.Groups || []}
          title={data.Title}
        />
      )}
    </Container>
  );
};
export default HelpArticleGroup;
