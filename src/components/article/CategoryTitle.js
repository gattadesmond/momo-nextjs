const CategoryTile = ({ category }) => {

  return (
    <div className="">
      <h1 className="text-2xl font-bold mb-3 text-momo">
        {category ? category.Name : "Tin tức"}
      </h1>
      <div className="text-base text-gray-500 max-w-3xl mb-5 hidden">
        {category
          ? category.Description ? category.Description : <span>&nbsp;</span>
          : "Chia sẻ thông tin, kiến thức có giá trị dành cho bạn"}
      </div>
    </div>
  );
}

export default CategoryTile;