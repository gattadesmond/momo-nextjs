import Link from "next/link";

import { getArticleCategoryIcon } from "@/lib/article-category-icon";

import ArticleThumbnail from "./ArticleItem/ArticleThumbnail";
import { getUrlHasRoot } from "@/lib/url";
const PreferList = ({ className, data, type = 0 }) => {
  // type = 0 : show 1 item 1 row
  // type = 1 : show 3 item 1 row
  const getOffDate = (offdate) => {
    if (!offdate) return;

    const DateTimeNow = new Date();
    const DateTimeOffDate = new Date((offdate || "").replace(/-/g, "/"));
    const OffDateString = `${DateTimeOffDate.getDate()}/${DateTimeOffDate.getMonth() + 1}/${DateTimeOffDate.getFullYear()}`;

    if (DateTimeOffDate.getTime() > DateTimeNow.getTime()) {
      return (
        <>
          <span>Ngày hết hạn:</span>&nbsp;<strong>{OffDateString}</strong>
        </>
      );
    }

    return;
  }

  return (
    <div className={`${className && className} pb-4`}>
      <div className={`flex ${type === 1 ? "justify-start pb-4 md:pb-6" : "pb-4 md:pd-2"}`}>
        <h2 className="flex items-center text-xl font-semibold text-momo">
            <img
              className="img-black mr-2"
              alt="Ưu đãi nổi bật"
              src={getArticleCategoryIcon(null).icon}
              loading="lazy"
              width={getArticleCategoryIcon(null).width}
              height={getArticleCategoryIcon(null).height}
            />
          Ưu đãi nổi bật
        </h2>
      </div>
      <div className={`grid grid-cols-1 gap-6 md:grid-cols-2 ${type === 1 ? "lg:grid-cols-3" : 'lg:grid-cols-1'}`}>
        {data &&
          data.map((item, index) => (
            <article key={index} className="article-type-prefer font-nunito overflow-hidden shadow-lg rounded-xl" >
              <div className="flex flex-col flex-nowrap h-full">
                <div className="article-top">
                  <ArticleThumbnail
                    className="rounded-b-none"
                    avatar={item.Avatar}
                    title={item.Title}
                    slug={getUrlHasRoot(item.Link)}
                    objectFit="contain"
                  />
                </div>
                <div className="article-header grow p-4">
                  <h5 className="article-title mb-2">
                    <Link href={getUrlHasRoot(item.Link)}>
                      <a>
                        {item.Title}
                      </a>
                    </Link>
                  </h5>
                  <p className="article-short">{item.Description}</p>
                </div>
                <div className="article-footer p-4">
                  <div className="article-offdate">{getOffDate(item.OffDate)}</div>
                  <div className="article-xemchitiet text-right">
                    <Link href={getUrlHasRoot(item.Link)}>
                      <a className="">Xem chi tiết</a>
                    </Link>
                  </div>
                </div>
              </div>
            </article>
          ))}
      </div>
    </div>
  );
};

export default PreferList;
