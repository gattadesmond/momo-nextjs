import ArticleTitle from "./ArticleItem/ArticleTitle";

const PopularList = ({ className, data }) => {
  return (
    <div className={`${className}`}>
      <div className="flex justify-between items-center mt-2">
        <h2 className="mb-0 flex items-center text-xl font-semibold text-momo">
          <div className="pr-2">
            <img
              className="img-black"
              alt="Tin xem nhiều"
              src="https://static.mservice.io/next-js/_next/static/public/article/Popular.svg"
              loading="lazy"
              width={19}
              height={19}
            />
          </div>
          Tin xem nhiều
        </h2>
      </div>
      <div className="">
        {data &&
          data.sort((a,b) => b.TotalView - a.TotalView).map((item, index) => (
            <article className="pt-2 md:pt-3 article-type-popular" key={item.Id}>
              <div className="flex flex-nowrap ">
                <div className="pr-3 text-2xl font-bold text-pink-400">
                  #{index + 1}
                </div>

                <div className="flex-1 md:order-2 border-b border-gray-300 md:pt-2 lg:pt-0">
                  <ArticleTitle
                    type="popular"
                    title={item.Title}
                    slug={item.Link}
                    totalView={item?.TotalView}
                    date={item?.PublicDate}
                  />
                </div>

              </div>
            </article>
          ))}
      </div>
    </div>
  );
};

export default PopularList;
