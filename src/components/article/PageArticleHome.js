import { useEffect, useState } from "react";
import StickyBox from "react-sticky-box";
// import { axios } from "@/lib/index";

import Page from "@/components/page";
import Container from "@/components/Container";

import ArticleMenu from "./ArticleMenu";
import ArticleFeature from "./ArticleFeature";
import CategoryTile from "./CategoryTitle";
import PreferList from "./PreferList";
// import PopularList from "./PopularList";
import ArticleList from "./ArticleList";

const PageArticleHome = ({
  categoryObj,
  dataCategory,
  dataHomePost,
  listArticle,
  listPrefer,
  pageMaster,
  isMobile,
}) => {
  const data = dataHomePost.Data;

  useEffect(() => {
    document.querySelector("body").classList.add("page-article");

    return function clean() {
      document.querySelector("body").classList.remove("page-article");
    };
  }, []);

  return (
    <Page
      className="page-article"
      title={data.Meta.Title}
      description={data.Meta.Description}
      image={data.Meta.Avatar}
      keywords={data.Meta.Keywords}
      header={pageMaster.Data.MenuHeaders}
      isMobile={isMobile}
    >
      <ArticleMenu
        data={dataCategory.Data}
        cat={categoryObj}
        root={{
          link: "/tin-tuc",
          title: "Tin tức",
          icon: "https://static.mservice.io/next-js/_next/static/public/article/Tinmoi.svg",
        }}
      />
      <Container>
        <div className="grid grid-cols-1 pb-10 lg:grid-cols-3 lg:gap-6 mt-5 lg:mt-6">
          <div className="col-span-2">
            <CategoryTile category={data.Category} />
            {data.ListFeatured?.Items?.length > 0 && (
              <ArticleFeature data={data.ListFeatured.Items} isHome={true} />
            )}
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <ArticleList
                excludeIds={data.ListFeatured.Items.map((x) => x.Id)}
                data={listArticle.Data}
                cat={data.Category ? data.Category : null}
                isHome={true}
              />
            </div>
          </div>
          <div className="border-t lg:border-t-0 lg:border-l border-gray-300 lg:pl-6 pt-3 md:pt-0">
            <StickyBox offsetTop={60} offsetBottom={20}>
              <div className="pb-4 md:pt-5 mt-1 md:mt-3 rounded-lg lg:mt-0 lg:pt-1">
                <PreferList
                  className=""
                  data={listPrefer.Data?.Items}
                  type={2}
                />
                {/* <PopularList className="mt-4" data={topViewArticle.Data?.Items} /> */}
              </div>
            </StickyBox>
          </div>
        </div>
      </Container>
    </Page>
  );
};

export default PageArticleHome;
