import Link from "next/link";
import { useRouter } from "next/router";
import { useState, useEffect, useCallback } from "react";

import debounce from "@/lib/debounce";

import { axios } from "@/lib/index";

const Search = ({ data }) => {
  const router = useRouter();

  const [query, setQuery] = useState("");

  const [kq, setKq] = useState(null);

  const [loading, setLoading] = useState(false);

  const [focused, setFocused] = useState(false);

  const IconMap = (name) => {
    const iconList = {
      Article: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z"
          />
        </svg>
      ),
      Blog: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
          />
        </svg>
      ),
      GuideArticles: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z"
          />
        </svg>
      ),
      MomoPage: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14"
          />
        </svg>
      ),

      Promotions: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7"
          />
        </svg>
      ),
      QAs: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
          />
        </svg>
      ),
      Services: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-4 h-4 opacity-80"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z"
          />
        </svg>
      ),
    };

    return iconList[name];
  };

  function closeSearch() {
    setTimeout(function () {
      setFocused(false);
    }, 250);
  }

  function openSearch() {
    setTimeout(function () {
      setFocused(true);
    }, 250);
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (query == "" || query == null) {
      return;
    }
    window.location.href = `https://momo.vn/tim-kiem?q=${query}`;
  }

  function filterData(res) {
    let result = [];
    Object.keys(res).map((item, index) => {
      if (
        res[item].Items.length == 0 ||
        item == "PlaceBrand" ||
        item == "PlaceCategory" ||
        item == "QAs"
      )
        return;
      let ad = res[item].Items;
      ad.type = item;
      result = [...result, ...ad];
    });
    setKq(result.slice(0, 8));
  }

  const fetchData = useCallback(
    debounce((q) => {
      const query = q;
      axios
        .post(`/search/suggest`, {
          data: {
            q: query,
            t: 0,
            s: 0,
            pi: 0,
            c: 3,
          },
        })
        .then((res) => {
          setLoading(false);
          if (!res || !res.Result) return;
          filterData(res.Data);
        });
    }, 500),
    []
  );

  // useEffect(() => {
  //   if (query != null && query != "" && query.length > 2) {
  //     fetchData(query);
  //     setLoading(true);
  //   } else {
  //     setKq(null);
  //   }
  // }, [query]);

  return (
    <>
      <form onSubmit={handleSubmit} className="relative">
        <div className="relative ">
          <input
            type="text"
            value={query}
            // onFocus={openSearch}
            // onBlur={closeSearch}

            onChange={(e) => setQuery(e.target.value)}
            placeholder="Bạn tìm gì ..."
            className="items-center justify-center block w-full px-3 py-1 bg-white border border-gray-300 rounded h-9"
          />
          {kq && kq.length > 0 && focused && (
            <div className="absolute left-0 right-0 px-1 py-3 mt-1 -ml-10 -mr-10 bg-white border border-gray-200 rounded shadow-sm lg:left-auto lg:ml-0 lg:mr-0 lg:w-96 top-full ">
              {kq.map(
                (item, index) =>
                  item.UrlRewrite && (
                    <Link
                      href={`https://momo.vn${item.UrlRewrite}`}
                      key={item.Id}
                    >
                      <a
                        target="_blank"
                        className="text-gray-800 hover:text-pink-600"
                      >
                        <div
                          className="flex flex-row items-center px-3 py-2 text-sm flex-nowrap"
                          key={item.Id}
                        >
                          <div className="flex-none pr-3 ">
                            {IconMap(item.Type)}
                          </div>

                          <div className="flex-1 min-w-0 overflow-hidden whitespace-nowrap text-ellipsis">
                            {item.Name}
                          </div>
                        </div>
                      </a>
                    </Link>
                  )
              )}
            </div>
          )}
          {!loading ? (
            <button
              type="submit"
              className="absolute border-none outline-none opacity-50 right-2 top-2"
              aria-label="Search"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 "
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                />
              </svg>
            </button>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="absolute right-0 w-5 h-5 mr-2 opacity-50 animate-spin top-2"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
              />
            </svg>
          )}
        </div>
      </form>

      <style jsx>{``}</style>
    </>
  );
};

export default Search;
