import CtaFooter from "@/components/section/CtaFooter";

import { useSpring, animated } from "react-spring";

import { useState, useEffect } from "react";

export default function FooterCta({ data, isMobile, isViewApp = false }) {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    function setScrollVisible() {
      window.pageYOffset > 400 ? setVisible(true) : setVisible(false);
    }

    window.addEventListener("scroll", setScrollVisible, { passive: true });
    return function clean() {
      window.removeEventListener("scroll", setScrollVisible);
    };
  }, []);

  const stylesSpring = useSpring({
    opacity: visible ? 1 : 0,
    config: { duration: 200 },
  });

  const getData = data.filter((item) => item.TypeName == "CtaFooter");
  return (
    <>
      {getData && getData.length > 0 && (
        <animated.div
          className={`footer-cta sticky bottom-0 z-30 bg-pink-100 bg-opacity-90 flex  justify-center space-x-3 px-3 py-2 ${
            visible ? "" : " select-none pointer-events-none "
          } `}
          style={{
            opacity: stylesSpring.opacity,
          }}
        >
          {getData.map((item, index) => (
            <CtaFooter
              key={index}
              template={item.Template}
              cta={item.Cta}
              content={item.Content}
              isMobile={isMobile}
              isViewApp={isViewApp}
            />
          ))}
        </animated.div>
      )}
{/* 
.footer-cta {
          box-sizing: border-box;
          margin-bottom: env(safe-area-inset-bottom) !important;
          padding-left: env(safe-area-inset-left) !important;
          padding-right: env(safe-area-inset-right) !important;
          height: 114px;
          bottom: -60px;
          padding-bottom: 60px;
        } */}

      <style jsx global>{`
     

   
      `}</style>
    </>
  );
}
