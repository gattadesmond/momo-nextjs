import css from "styled-jsx/css";

// Use styled-jsx instead of global CSS because global CSS
export default css.global`
  :root {
    /* Spacing variables */
    --pinkmomo: #a50064;
    --pink: #f45197;
    --pinklight: #f9b5c3;
    --gray-900: #171717;
    --gray-800: #424242;
    --gray-700: #616161;
    --gray-600: #757575;
    --gray-500: #9e9e9e;
    --gray-400: #bdbdbd;
    --gray-300: #e0e0e0;
    --gray-200: #eeeeee;
    --gray-100: #f5f5f5;
  }
`;
