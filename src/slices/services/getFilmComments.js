import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { appSettings } from "@/configs";


export const filmCommentsApi = createApi({
    reducerPath: 'filmComments',
    baseQuery: fetchBaseQuery({ baseUrl: `${appSettings.AppConfig.HOST_API}/ci-film/` }),
    endpoints: (builder) => ({
        getFilmComments: builder.query({ 
            query: (arg) => `rating/${arg}`, 
        }),
    }),
})

export const { useGetFilmCommentsQuery } = filmCommentsApi