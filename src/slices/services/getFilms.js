import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { appSettings } from "@/configs";

const convertQueryFilter = (query) => {

    let convert = "";

    if (query.query != "") {
        convert += `&query=${query.query}`
    }

    if (query.quocgia > 0) {
        convert += `&countryId=${query.quocgia}`
    }

    if (query.theloai > 0) {
        convert += `&projectIds=${query.theloai}`
    }

    if (query.year > 0) {
        convert += `&year=${query.year}`
    }
    return convert;
}

export const filmsApi = createApi({
    reducerPath: 'filmsApi',
    baseQuery: fetchBaseQuery({ baseUrl: `${appSettings.AppConfig.HOST_API}/ci-film/` }),
    endpoints: (builder) => ({
        getFilms: builder.query({ query: (arg) => `paging?pageIndex=${arg.current}${convertQueryFilter(arg)}&pageSize=20&sortType=2&sortDir=1`, }),
    }),
})


export const { useGetFilmsQuery } = filmsApi