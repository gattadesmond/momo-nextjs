import { combineReducers } from 'redux'

import { filmsApi } from './services/getFilms'
import { filmCommentsApi } from './services/getFilmComments'

//Add ...reducer name
import filmBookingReducer from './filmBooking'


const rootReducer = combineReducers({
    filmBooking: filmBookingReducer,

    [filmsApi.reducerPath]: filmsApi.reducer,
    [filmCommentsApi.reducerPath]: filmCommentsApi.reducer,
})

export default rootReducer;