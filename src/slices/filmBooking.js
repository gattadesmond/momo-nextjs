import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  filmID: null, //apiFilmId
  rapBrand: null, //cineplex
  rapId: null, //cineplex
  cityId: null, //apiCityId
  showDay: null, //date - today
  isNearby: false,
  geo: { lat: null, lng: null }, //lat lon
};
//name : filmBooking
export const filmBookingSlice = createSlice({
  name: "filmBooking",
  initialState,
  reducers: {
    setRapBrand: (state, action) => {
      state.rapBrand = action.payload;
    },
    setRapId: (state, action) => {
      state.rapId = action.payload;
    },
    setCityId: (state, action) => {
      state.cityId = action.payload;
    },
    setShowDay: (state, action) => {
      state.showDay = action.payload;
    },
    setIsNearBy: (state, action) => {
      state.isNearby = action.payload;
    },
    setGeo: (state, action) => {
      state.geo = action.payload;
    },
  },
});

// Extract the action creators object and the reducer
const { actions, reducer } = filmBookingSlice;
// Extract and export each action creator by name
export const { setRapBrand,setRapId, setCityId, setShowDay, setIsNearBy, setGeo } =
  actions;
// Export the reducer, either as a default or named export
export default reducer;
