import React, { useCallback, useEffect, useState } from "react";

function getPrevElement(list) {
  const sibling = list[0].previousElementSibling;
  if (sibling instanceof HTMLElement) {
    return sibling;
  }
  return sibling;
}

function getNextElement(list) {
  const sibling = list[list.length - 1].nextElementSibling;

  if (sibling instanceof HTMLElement) {
    return sibling;
  }

  return sibling;
}

export default function usePosition(ref) {
  const [prevElement, setPrevElement] = useState(null);
  const [nextElement, setNextElement] = useState(null);

  const offsetSoju = 20;

  useEffect(() => {
    // Our scrollable container
    const element = ref.current.children[0];

    const update = () => {
      const rect = element.getBoundingClientRect();
      const visibleElements = Array.from(element.children).filter((child) => {
        const childReact = child.getBoundingClientRect();

        return (
          rect.left <= childReact.left &&
          rect.right >= childReact.right - offsetSoju
        );
      });

      if (visibleElements.length > 0) {
        setPrevElement(getPrevElement(visibleElements));
        setNextElement(getNextElement(visibleElements));
      }
    };

    update();

    element.addEventListener("scroll", update, { passive: true });

    return () => {
      element.removeEventListener("scroll", update);
    };
  }, [ref]);

  const scrollToElement = useCallback(
    (element) => {
      const currentNode = ref.current.children[0];

      if (!currentNode || !element) return;

      let newScrollPosition;

      newScrollPosition =
        element.offsetLeft +
        element.getBoundingClientRect().width / 2 -
        currentNode.getBoundingClientRect().width / 2;

      currentNode.scroll({
        left: newScrollPosition,
        behavior: "smooth",
      });
    },
    [ref]
  );

  useEffect(() => {
    scrollToElementNow(ref);
  }, []);

  const scrollToElementNow = (ref = ref) => {
    const element = ref.current.children[0];

    if (!element) return;

    const activeElements = Array.from(element.children).filter((child) => {
      return child.classList.contains("is-active");
    });

    scrollToElement(activeElements[0]);
  };

  const scrollRight = useCallback(() => {
    scrollToElement(nextElement);
  }, [scrollToElement, nextElement]);

  const scrollLeft = useCallback(
    () => scrollToElement(prevElement),
    [scrollToElement, prevElement]
  );

  return {
    hasItemsOnLeft: prevElement !== null,
    hasItemsOnRight: nextElement !== null,
    scrollRight,
    scrollLeft,
    scrollToElementNow,
  };
}
