import * as Parser from "ua-parser-js";

const screens = {
  sm: "640px",
  // => @media (min-width: 640px) { ... }

  md: "768px",
  // => @media (min-width: 768px) { ... }

  lg: "1024px",
  // => @media (min-width: 1024px) { ... }

  xl: "1280px",
  // => @media (min-width: 1280px) { ... }

  "2xl": "1536px",
  // => @media (min-width: 1536px) { ... }
};

export const SM = {
  minWidth: screens["sm"],
};

export const MD = {
  minWidth: screens["md"],
};

export const LG = {
  minWidth: screens["lg"],
};

export const XL = {
  minWidth: screens["xl"],
};
