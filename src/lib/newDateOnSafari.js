export default function NewDateOnSafari(value) {
  if (!value) return null;
  return new Date(value.replace(/-/g, "/"));
};