function ViewFormat(props) {
  const { data } = props;

  if (data >= 0 && data < 1000) {
    return `${data}`;
  }

  const convertNumber = parseFloat(data / 1000).toFixed(1);
  return `${convertNumber}k`;
}

export default ViewFormat;
