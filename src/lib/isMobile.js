import * as Parser from "ua-parser-js";

//https://www.youtube.com/watch?v=K7g8X_VRDy8&t=357s
export default function isMobile(req) {
  let userAgent;

  if (req) {
    //SSR request is present
    userAgent = Parser(req.headers["user-agent"] || "");
  } else {
    //check only on client, no request present
    userAgent = new Parser().getResult();
  }

  if (
    userAgent?.device?.type == "mobile" 
  )
    return true;

  return false;
}
