export default function CatClassName(url) {

  const prefix = "/blog/";
  const slug = url.slice(prefix.length);
  return `soju-${slug}`;

//   switch (slug) {
//     case "du-hi":
//       return `cat-${slug}`;
//     case "di-quay":
//       return "text-red-500 hover:text-red-800";
//     case "them-an":
//       return "text-green-500 hover:text-green-800";
//     case "mua-gi":
//       return "text-yellow-500 hover:text-yellow-800";
//     case "chon-momo":
//       return "text-indigo-500 hover:text-indigo-800";
//     default:
//       return "text-green-500 hover:text-green-800";
//   }
}
