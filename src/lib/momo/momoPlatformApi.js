import { useEffect } from "react";

import MaxApi from "@momo-platform/max-api";

export default function momoPlatformApi() {
  useEffect(() => {
    MaxApi.init({
      appId: "vn.momo.web.quyengop",
      name: "vn.momo.web.quyengop",
      displayName: "Quyên Góp",
      client: {
        web: {
          hostId: "vn.momo.web.quyengop",
          accessToken:
            "U2FsdGVkX1/GbSqeoCZwUA4RlSDjsqVFWWtqL6Re9Rxc1LEGiZHMg7VgjxY89CBf3KsQngY3gW0fU7fQMJsmiNK2fNu60l6rxnNrGtAuE3s=",
        },
      },
      configuration_version: 1,
    });
  }, []);

  const shareFacebookApp = (link) => {
    if (!link) return;
    try {
      MaxApi.shareFacebook({ link: link });
      var objShare = { action: "shareFacebookUrl", value: link };
      window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
    } catch (error) { }
  };

  const shareOtherApp = (link) => {
    if (!link) return;
    try {
      MaxApi.share({ message: link });
      var objShare = { action: "shareContent", value: link };
      window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
    } catch (error) { }
  };

  const exitApp = () => {
    try {
      MaxApi.goBack();
    } catch (error) { }
  };

  return {
    shareFacebookApp,
    shareOtherApp,
    exitApp
  };
}