
import { reBuildUrl } from "@/lib/momo/tracking";

const qrCodeModelOpen = (qrCodeId) => {
  var btnQrCodes = document.querySelectorAll(`*[data-qr-id="${qrCodeId}"]`);
  if (btnQrCodes.length > 0) {
    var uc = btnQrCodes[0].getAttribute("data-redirect-uc");

    var args = [
      { key: "popup-qrcode", val: qrCodeId },
      { key: "popup-uc", val: uc },
    ];
    var pushUrl = reBuildUrl(args);
    window.history.pushState({}, "", pushUrl);
  }
};

export default qrCodeModelOpen;
