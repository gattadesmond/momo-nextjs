const reBuildUrl = (args) => {
  function getItem(key, array) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].key == key) return array[i];
    }

    return null;
  }
  var argsBind = [];
  var rs = location.origin + location.pathname;
  var params = "";
  var url = new URL(window.location);
  url.searchParams.forEach(function (val, key) {
    var arg = getItem(key, argsBind);
    if (!arg) {
      argsBind.push({ key: key, val: val });
    }
  });

  args.forEach(function (data, idx) {
    var arg = getItem(data.key, argsBind);
    if (!arg) {
      argsBind.push({ key: data.key, val: data.val });
    } else {
      arg.val = data.val;
    }
  });

  argsBind.forEach(function (data, idx) {
    if (data.key && data.val)
      params += (params ? "&" : "") + (data.key + "=" + data.val);
  });

  rs += (params ? "?" : "") + params;
  rs += location.hash;
  return rs;
};
export default reBuildUrl;
