import { reBuildUrl } from "@/lib/momo/tracking";

const qrCodeModelClose = () => {
  var args = [
    { key: "popup-qrcode", val: null },
    { key: "popup-uc", val: null },
  ];
  var pushUrl = reBuildUrl(args);
  window.history.pushState({}, "", pushUrl);
};

export default qrCodeModelClose;
