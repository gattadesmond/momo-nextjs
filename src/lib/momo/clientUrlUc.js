const clientUrlUc = () => {
  var btnRedirectCta = document.querySelectorAll("a[data-redirect-uc]");

  btnRedirectCta.forEach((item, i) => {
    
    var uc = item.getAttribute("data-redirect-uc");
    if (!uc) return;
    if (item.classList.contains("data-redirect-uc")) return;
    var to = item.getAttribute("href");

    var from = location.href;
    var subPar = from.indexOf("?");

    if (subPar > -1) {
      subPar = from.substring(subPar + 1);
    } else {
      subPar = "";
    }
    if (!to || to == "javascript:void(0)") return;
    uc = encodeURIComponent(uc);
    to = encodeURIComponent(to);
    from = encodeURIComponent(from);

    var urlRedirect =
      "/redirect-cta?uc=" +
      uc +
      "&to=" +
      to +
      "&from=" +
      from +
      (subPar ? "&" + subPar : "");

    item.setAttribute("href", urlRedirect);
    item.classList.add("data-redirect-uc");
  });
};

export default clientUrlUc;
