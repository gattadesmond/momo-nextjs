import { createMedia } from "@artsy/fresnel"

const MoMoAppMedia = createMedia({
  breakpoints: {
    xs: 0,
    sm: 640,
    md: 768,
    lg: 1024,
    xl: 1280,
  },
})

// Make styles for injection into the header of the page
export const mediaStyles = MoMoAppMedia.createMediaStyle()

export const { Media, MediaContextProvider } = MoMoAppMedia