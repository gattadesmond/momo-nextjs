//Xu ly File va Thu muc
import fs from "fs";

//YAML
import matter from "gray-matter";

import mdxPrism from "mdx-prism";
import path from "path";

import readingTime from "reading-time";

//Load MDX content not through an import, through getStaticProps, getServerProps
import renderToString from "next-mdx-remote/render-to-string";

import MDXComponents from "@/components/MDXComponents"; //MDX components

const root = process.cwd(); //current working directory


const PermalinkIcon = () => (
  <span>
    <svg viewBox="0 0 16 16" width="16" height="16">
      <g strokeWidth="1" fill="#fff" stroke="#fff">
        <path
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          d="M8.995,7.005 L8.995,7.005c1.374,1.374,1.374,3.601,0,4.975l-1.99,1.99c-1.374,1.374-3.601,1.374-4.975,0l0,0c-1.374-1.374-1.374-3.601,0-4.975 l1.748-1.698"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          d="M7.005,8.995 L7.005,8.995c-1.374-1.374-1.374-3.601,0-4.975l1.99-1.99c1.374-1.374,3.601-1.374,4.975,0l0,0c1.374,1.374,1.374,3.601,0,4.975 l-1.748,1.698"
        />
      </g>
    </svg>
  </span>
);


export async function getFiles(type) {
  //reads the contents of the directory.
  return fs.readdirSync(path.join(root, "data", type)); // data/blog
  //Example var x = path.join('Users', 'Refsnes', 'demo_path.js'); Users\Refsnes\demo_path.js
}

export async function getFileBySlug(type, slug) {
  const source = slug
    ? fs.readFileSync(path.join(root, "data", type, `${slug}.mdx`), "utf8")
    : fs.readFileSync(path.join(root, "data", `${type}.mdx`), "utf8");

  //Chac la lay YAML
  const { data, content } = matter(source);

  const mdxSource = await renderToString(content, {
    components: MDXComponents,
    mdxOptions: {
      remarkPlugins: [
        require("remark-slug"),
        [
          require("remark-autolink-headings"),
          {
            content: {
              type: "element",
              tagName: "span",
              properties: { className: ["icon ", "soju-icon-autolink"] },
            },
          },
        ],
        require("remark-code-titles"),
      ],
      rehypePlugins: [mdxPrism],
    },
  });

  return {
    mdxSource,
    frontMatter: {
      wordCount: content.split(/\s+/gu).length,
      readingTime: readingTime(content),
      slug: slug || null,
      ...data,
    },
  };
}

export async function getAllFilesFrontMatter(type) {
  const files = fs.readdirSync(path.join(root, "data", type));

  //reduce(total = [], currentValue) method reduces the array to a single value.
  return files.reduce((allPosts, postSlug) => {
    //postSlug la ten file
    const source = fs.readFileSync(
      path.join(root, "data", type, postSlug),
      "utf8"
    );

    //Loc YAML content
    const { data } = matter(source);

    return [
      {
        ...data,
        slug: postSlug.replace(".mdx", ""),
      },
      ...allPosts,
    ];
  }, []);
}
