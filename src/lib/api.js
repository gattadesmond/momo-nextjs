export async function getAllPostsForHome(preview) {
  const params = {
    limit: "all",
    include: "authors",
    order: "published_at DESC",
    ...(preview && { status: "all" }),
  };
  const posts = await api.posts.browse(params);
  return posts;
}
