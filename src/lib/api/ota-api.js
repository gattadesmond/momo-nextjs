import { axios } from "@/lib/index";

const otaFlightParams = {
  //Mã nơi đi
  deptCode: '',
  //Mã nơi đến
  arriCode: '',
  //Ngày đi (dd-MM-yyyy) 
  departureTime: '',
  //Ngày về (dd-MM-yyyy)
  returnTime: '',
  //Danh sách mã hãng bay ["vna","bamboo","vietjet"]
  airlineCodes: null,
  //Số người lớn
  adult: 1,
  //Số trẻ em
  child: 0,
  //Số em bé
  infant: 0,
  //Index trang 
  pageIndex: 0,
  //Số lượng kết quả trong 1 trang
  pageSize: 10,
  //Giá thấp nhất
  priceMin: null,
  //Giá cao nhất
  priceMax: null,
  //Kiểu sắp xếp.
  //1=Giá vé tăng dần, 2=Giá vé giảm dần, 3=Giờ khởi hành sớm nhất, 4=Giờ khởi hành muộn nhất, 5=Thời gian bay nhanh nhất (Api chưa support)
  sortType: 1,
  //Giờ cất cánh sớm nhất
  takeOffTimeRangeStart: null,
  //Giờ cất cánh trễ nhất
  takeOffTimeRangeEnd: null,
  //Giờ hạ cánh sớm nhất
  landingTimeRangeStart: null,
  //Giờ hạ cánh trễ nhất
  landingTimeRangeEnd: null
}
const convertObjectToParamString = (object) => 
  Object.keys(object)
    .filter((key) => object[key] != null && object[key] != undefined)
    .map((key) => `${key}=${object[key]}`)
    .join('&');

const searchFlightsOut = (params) => {
  const urlParameter = convertObjectToParamString(params);
  return axios.get(`/ota/searchFlightsOut?${urlParameter}`);
}

const searchFlightsIn = (params) => {
  const urlParameter = convertObjectToParamString(params);
  return axios.get(`/ota/searchFlightsIn?${urlParameter}`);
};

const getServicePageId = async (servicePageType) => {
  const otaCommon = await axios.get(`/common/ota`, null, true);
  if (!otaCommon || !otaCommon.Result || !otaCommon.Data.ServicePageIds) {
    return null;
  }

  return otaCommon.Data.ServicePageIds[servicePageType];
}

const getLocation = () => axios.getSSR('/ota-location/loadMore?count=9999&lastIndex=0&status=1');
const getCheapFlights = () => axios.getSSR('/ota/getCheapFlights');

const submitFlight = (requestId, outbound, inbound) => axios.post(`/ota/submit`, { data: { requestId, outbound, inbound } });
const getBFM = (outbound) => axios.post(`/ota/getBFM`, {data: {outbound}});

const otaApi = {
  ssr: {
    getServicePageId,
    getLocation,
    getCheapFlights
  },
  csr: {
    searchFlightsIn,
    searchFlightsOut,
    submitFlight,
    getBFM
  },
  otaFlightParams
};

export default otaApi;