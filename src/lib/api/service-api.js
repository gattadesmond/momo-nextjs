import { axios } from "@/lib/index";

const getDataDetail = (servicePageId) => axios.getSSR(`/service-page/detail?id=${servicePageId}`);
const getBreadcrumb = (pageId, parentId) => axios.getSSR(`/service-page/listBreadcrumbs?id=${pageId}${parentId ? '&parentId=' + parentId : ''}`);
const getDataBlocks = (servicePageId) => axios.getSSR(`/service-page/listBlocks?pageId=${servicePageId}`);

const serviceApi = {
  ssr: {
    getDataDetail,
    getBreadcrumb,
    getDataBlocks
  },
  csr: {

  }
};

export default serviceApi;