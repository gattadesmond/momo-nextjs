function ReplaceURL(url) {
  if (window.history.replaceState) {
    //prevents browser from storing history with each change:
    let urlCont = `${url}${window.location.search ? window.location.search : ''}${window.location.hash ? window.location.hash : ''}`
    window.history.replaceState("", "", urlCont);
  }
}

export default ReplaceURL;
