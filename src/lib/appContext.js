import { createContext, useContext, useEffect, useLayoutEffect, useState } from "react";
import { useRouter } from "next/router";
import useBrowserLayoutEffect from "./useBrowserLayoutEffect";

const AppContext = createContext({
  isMobile: false,
  isViewApp: false,
  setIsViewApp: () => { },
  setIsViewAppFromDataApi: () => { }
});

export function AppProvider({ isMobile = false, children }) {
  const router = useRouter();
  const [isViewApp, setIsViewApp] = useState(false);

  useBrowserLayoutEffect(() => {
    setIsViewApp(router?.query?.view == "app");
  }, [router]);

  useBrowserLayoutEffect(() => {
    try {
      window.ReactNativeWebView.postMessage("GetUserInfo");
      setIsViewApp(true);
    } catch { }
  }, []);


  const setIsViewAppFromDataApi = (ViewInApp) => {
    setIsViewApp((_isViewApp) => {
      if (_isViewApp && ViewInApp) {
        return true;
      } else {
        return false;
      }
    })
  }

  return (
    <AppContext.Provider
      value={{
        isMobile,
        isViewApp,
        setIsViewApp,
        setIsViewAppFromDataApi
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export default function useAppContext() {
  return useContext(AppContext);
};