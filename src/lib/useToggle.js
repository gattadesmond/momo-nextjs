import { useState, useCallback } from "react";

// Got from https://usehooks.com/
export default function useToggle(initialState = false) {
  //Initialize the state

  const [state, setState] = useState(initialState);

  //Defind and memorize toggler function in case we pass down the component,
  //This funciton change the boolean value to it's opposite value

  const toggle = useCallback(() => setState((state) => !state), []);

  return [state, toggle];
}
