import mousetrap from './mousestrap/mousetrap';
import './mousestrap/mousetrap-global-bind';

import { useEffect,  useRef } from "react";

export default function useKey(targetKey, action) {
  const actionRef = useRef(null);
  actionRef.current = action;

  useEffect(() => {
    mousetrap.bindGlobal(targetKey, function (event) {
      event.preventDefault()
      if (actionRef.current) {
        actionRef.current()
      }
    })

    return () => mousetrap.unbind(targetKey)
  }, [targetKey])
}


