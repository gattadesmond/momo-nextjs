import { appSettings } from "@/configs";
const getUrlHasRoot = (url) => {
  if (!url) return url;
  if (
    url.indexOf("https://") == 0 ||
    url.indexOf("http://") == 0 ||
    url.indexOf("//") == 0
  )
    return url;

  var rs = `${appSettings.AppConfig.FRONT_END}${url}`;
  return rs;
};

export default getUrlHasRoot;
