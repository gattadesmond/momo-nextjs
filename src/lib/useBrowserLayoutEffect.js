import { useLayoutEffect, useEffect } from 'react'

// fix bug useLayoutEffect error in server side render
// https://reactjs.org/link/uselayouteffect-ssr
// https://gist.github.com/gaearon/e7d97cdf38a2907924ea12e4ebdf3c85
const useBrowserLayoutEffect = typeof window !== 'undefined' ? useLayoutEffect : useEffect;

export default useBrowserLayoutEffect;