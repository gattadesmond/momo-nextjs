import axios from "axios";
import { appSettings } from "@/configs";

const instanceAxios = axios.create({
  baseURL: `${appSettings.AppConfig.HOST_API}`,
  timeout: 20000,
});
const instanceAxios_NextJs = axios.create({
  baseURL: `${appSettings.AppConfig.HOST_API_NEXTJS}`,
  timeout: 160000,
});
//  export default axios.create();
const _fetchJson = (source, options = {}, useNextJs) => {
  options = mergeConfig(
    {
      // headers: {
      //   "x-client": 6,
      //   "x-authen": "123",
      //   "x-timestamp": _getTimestamp(),
      // },
    },
    options
  );

  return (useNextJs ? instanceAxios_NextJs : instanceAxios)
    .request(`${source}`, options)
    .then((res) => {
      if (res.status !== 200) {
        if (res.data && res.data.Error) {
          if (res.data.Error.Code === 1001) {
            // authenProvider.RemoveAuthenInfo();
            // global.AdminContext.Message.Error(`Authen failed: ${res.data.Error.Code}`);
            return { Authen: false };
          }
        }
      }
      return res.data;
    })
    .catch((error) => {
      if (error.response && error.response.status === 401) {
        // authenProvider.RemoveAuthenInfo();
        // global.AdminContext.Message.Error(`Authen failed - status: ${error.response.status}`);
        return { Authen: false };
      }
      if (error.message) {
        // global.AdminContext.Message.Error(error.message);
      }
      return { Error: { Message: error.message } };
    });
};

const _getTimestamp = () => {
  var date = new Date();
  return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
};

const mergeConfig = (config1, config2) => {
  // eslint-disable-next-line no-param-reassign
  config1 = config1 || {};
  config2 = config2 || {};
  var config = {};
  for (var prop1 in config1) {
    config[prop1] = config1[prop1];
  }
  for (var prop2 in config2) {
    config[prop2] = config2[prop2];
  }
  return config;
};

const requestAxios = () => {
  const _getSSR = (source, options = null) => {
    options = mergeConfig(
      {
        method: "get",
      },
      options
    );

    return _fetchJson(source, options, true);
  };
  var _get = (source, options, useNextJs) => {
    options = mergeConfig(
      {
        method: "get",
      },
      options
    );

    return _fetchJson(source, options, useNextJs);
  };
  var _post = (source, options) => {
    options = mergeConfig(
      {
        method: "post",
      },
      options
    );

    return _fetchJson(source, options);
  };
  var _put = (source, options) => {
    options = mergeConfig(
      {
        method: "put",
      },
      options
    );

    return _fetchJson(source, options);
  };
  var _delete = (source, options) => {
    options = mergeConfig(
      {
        method: "delete",
      },
      options
    );

    return _fetchJson(source, options);
  };
  return {
    getSSR: _getSSR,
    get: _get,
    post: _post,
    put: _put,
    delete: _delete,
    cancelToken: axios.CancelToken.source()
  };
};

export default requestAxios();
