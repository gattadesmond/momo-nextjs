export const articleCategoryIcon = [
  { id: null, icon: 'https://static.mservice.io/next-js/_next/static/public/article/Tinmoi.svg', width: 12, height: 13 },
  { id: 17, icon: 'https://static.mservice.io/next-js/_next/static/public/article/Sukien.svg', width: 14, height: 20 },
  { id: 18, icon: 'https://static.mservice.io/next-js/_next/static/public/article/Khuyenmai.svg', width: 13, height: 13 },
  { id: 19, icon: 'https://static.mservice.io/next-js/_next/static/public/article/Thongcaobaochi.svg', width: 10, height: 13 },
  { id: 20, icon: 'https://static.mservice.io/next-js/_next/static/public/article/Video.svg', width: 16, height: 20 },
];
export const getArticleCategoryIcon = (id) => {
  let result = articleCategoryIcon.find(a => a.id === id);
  return result ? result : articleCategoryIcon[0];
};