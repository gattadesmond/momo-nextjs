import { appSettings } from "@/configs";

// remove first character "/" on page link
export const getPureSlug = (url) =>
  url ? url.split("/").slice(-1).pop() : null;

// return url include character "/" root
export const getRootSlug = (url = "") => {
  if (url.substr(0, 4) === "http") return url;

  return url[0] === "/" ? url : "/" + url;
};

// remove domain of link
export const getRemoveDomainSlug = (url) => url.replace(/^.*\/\/[^\/]+/, "");

// get url without url parameter
export const getUrlWithoutParameter = (url) => {
  if (url.indexOf("?") === -1) {
    return url;
  } else {
    return url.split("?")[0];
  }
};
export const getUrlHasRoot = (url) => {
  if (!url) return url;
  if (
    url.indexOf("https://") == 0 ||
    url.indexOf("http://") == 0 ||
    url.indexOf("//") == 0
  )
    return url;

  var rs = `${appSettings.AppConfig.FRONT_END}${url}`;
  // var fontEnd = appSettings.AppConfig.FRONT_END;
  // switch (fontEnd) {
  //   default:
  //   case "https://momo.vn":
  //     var rs = `${"https://momo.vn"}${url}`;
  //     break;
  //   case "http://localhost:3000":
  //     var rs = `${"http://localhost:3000"}${url}`;
  //     break;
  //   case "http://dev1.momo.vn:8000":
  //     var rs = `${"http://dev1.momo.vn:8000"}${url}`;
  //     break;
  // }

  return rs;
};
export const getUrlParams = (url) => {
  if (!url) return;
  if (url.indexOf('?') === -1) return;

  const urlQuery = url.split('?')[1];
  const urlSearchParams = new URLSearchParams(urlQuery);
  return Object.fromEntries(urlSearchParams.entries());
}
